<?php

namespace App\Services\Access\Traits;

use App\Http\Requests\User\ChangePasswordRequest;

/**
 * Class ChangePasswords
 * @package App\Services\Access\Traits
 */
trait ChangePasswords
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        return view('frontend.auth.passwords.change');
    }

    /**
     * @param ChangePasswordRequest $request
     * @return mixed
     */
    public function changePassword(ChangePasswordRequest $request) {

        $user = $this->user->find(access()->id());

        if (!$user->validarClavesAnteriores($request->get('password'))) {

            notify()->flash('La contraseña no puede ser igual a las 5 últimas contraseñas utilizadas', 'warning', [
              'timer' => 5000,
              'text' => ''
            ]);

            return redirect()->back();
        }

        $this->user->changePassword($user, $request->all());
        
        notify()->flash('Contraseña actualizada', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);

        return redirect()->route('frontend.user.perfil')->withFlashSuccess(trans('strings.frontend.user.password_updated'));
    }
}
