<?php

namespace App\Services\Carpetas;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Monitoreo\FlujoTipo;

class Carpetas
{
    public static function filesystem()
    {
        $raiz = ParametroSistema::select('valor')
        ->where('id', 1)
        ->first()
        ->valor;

        $local = new Local($raiz);

        return new Filesystem($local);
    }

    public static function crearCarpetas()
    {
        $filesystem = self::filesystem();

        $bancos = Banco::select('codigo')
        ->where('estatus', 'ACTIVO')
        ->get();

        $tipos = FlujoTipo::select('nombre')
        ->get();

        // crear carpetas para bancos
        foreach ($bancos as $banco) {
            $carpeta_banco = 'V' . $banco->codigo . 'V01';

            $filesystem->createDir($carpeta_banco);

            foreach ($tipos as $tipo) {
                $filesystem->createDir($carpeta_banco . DIRECTORY_SEPARATOR . $tipo->nombre);
            }
        }

        $carpetas = ['ALCMD','INCMD','OUTCMD','RICMD','ROCMD','SOCMD','TMP','VARECV00','VNCCEV00','VSCOPV00','VSICCV00'];

        foreach ($carpetas as $carpeta) {
            $filesystem->createDir($carpeta);
        }
    }

    public static function borrarCarpetas()
    {
        $filesystem = self::filesystem();

        $carpetas = $filesystem->listContents();

        foreach ($carpetas as $carpeta) {
            $filesystem->deleteDir($carpeta['path']);
        }
    }

    public static function validarCarpetas()
    {
        $filesystem = self::filesystem();

        $bancos = Banco::select('codigo')
        ->where('estatus', 'ACTIVO')
        ->get();

        $tipos = FlujoTipo::select('nombre')
        ->whereIn('tipo', ['STDOUT', 'STDIN', 'ROUOUT', 'ROUIN'])
        ->get();

        // crear carpetas para bancos
        foreach ($bancos as $banco) {
            $carpeta_banco = 'V' . $banco->codigo . 'V01';

            if (!$filesystem->has($carpeta_banco)) {
                return false;
            }

            foreach ($tipos as $tipo) {
                if (!$filesystem->has($carpeta_banco . DIRECTORY_SEPARATOR . $tipo->nombre)) {
                    return false;
                }
            }
        }

        $carpetas = ['ALCMD','INCMD','OUTCMD','RICMD','ROCMD','SOCMD','TMP','VARECV00','VNCCEV00','VSCOPV00','VSICCV00'];

        foreach ($carpetas as $carpeta) {
            if (!$filesystem->has($carpeta)) {
                return false;
            }
        }

        return true;
    }
}