<?php

namespace App\Services\ParametrosSistema;

use App\Models\Configuracion\ParametroSistema\ParametroSistema;

class Parametro
{
    static $sistema_cerrado;

    public static function valor($id)
    {
        return ParametroSistema::find($id)->valor;
    }

    public static function sistemaIniciado()
    {
        if (self::valor(23) == '0' &&
            self::valor(24)== '0' &&
            self::valor(25) == '1' &&
            self::valor(26) == '1' &&
            self::valor(27) == '1' &&
            self::valor(28) == '1' &&
            self::valor(29) == '1' &&
            self::valor(31) == '1' &&
            self::valor(32) == '1' &&
            self::valor(33) == '1') {return false;
        }else {
            return true;
        }
    }
}
