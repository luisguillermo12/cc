<?php

namespace App\Services\Helpers;

use Carbon\Carbon;
use DB;

use App\Models\Configuracion\SesionCompensacion\Periodo;

class HelperService
{
    //retorna la fecha actual de base de datos en formato Carbon
    public function actual_date()
    {
      $query = 'SELECT SYSDATE as fecha FROM DUAL';
      $fecha_actual = DB::select($query)[0]->fecha;
      $fecha_actual = Carbon::parse("{$fecha_actual}");
      return $fecha_actual;
    }

    //retorna un arreglo con las horas para un periodo completo de compensacion -> ["19:00","19:30","20:00",...]
    //$diferencia = cantidad de minutos de diferencia entre una hora y la otra
    public function generateHoras($diferenciaMinutos){

      // se obtiene la menor hora de inicio para un periodo, de modo que sea la base para todos los demas
      $menorInicio = Periodo::select(DB::raw('MIN(hora_inicio) as hora_inicio'))->where('dia_inicio','D-1')->first();
      $menorInicio = isset($menorInicio) ? $menorInicio->hora_inicio : '19:00';

      $i = 0; //contador para las posiciones del vector de horas
      $horaInicial = Carbon::parse("{$menorInicio}"); //se transforma la hora en un objeto carbon para añadir los minutos
      $horaInicialBase = $horaInicial->copy(); //para mantener la hora base con quien comparar la hora final del ciclo
      do { //se van a sumar los minutos indicados hasta que la diferencia en dias sea de 1
        $rangosHora[$i++] = $horaInicial->format('H:i');
      } while($horaInicialBase->diffInDays($horaInicial->addMinutes($diferenciaMinutos)) < 1);

      return $rangosHora;
    }
}
