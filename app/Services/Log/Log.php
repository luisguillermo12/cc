<?php

namespace App\Services\Log;

use Carbon\Carbon;
use Storage;

class Log
{
    public $ruta = [];

    public function __construct()
    {
        $fecha = Carbon::now()->format('Y-m-d');

        $this->ruta['inicio'] = "log-inicio-{$fecha}.log";
        $this->ruta['cierre'] = "log-cierre-{$fecha}.log";
        $this->ruta['respaldo_archivos'] = "RESPALDO-{$fecha}.log";
        $this->ruta['eps'] = "log-eps-{$fecha}.log";
    }

    /*
     * Escribe en el archivo de log.
     */
    public function escribir($tipo, $nivel, $mensaje)
    {
        // Agregar fecha
        $contenido = '['.Carbon::now()->format('Y-m-d H:i:s').'] ';

        // Agregar nivel
        $contenido .= "local.{$nivel}: ";

        // Agregar mensaje
        $contenido .= $mensaje;

        Storage::disk('log')->append($this->ruta[$tipo], $contenido);
    }

    /*
     * Escribe en el archivo de log.
     */
    public function respaldo($mensaje)
    {
        // Agregar fecha
        $contenido = '['.Carbon::now()->format('Y-m-d H:i:s').'] ';

        // Nombre del archivo
        $fecha = Carbon::now()->format('Ymdhis');
        $nombre = "RESPALDO_{$fecha}.log";

        // Agregar mensaje
        $contenido .= implode("\r\n", $mensaje);

        Storage::disk('eps_archivos')->append("ROOT_EPS_RESPALDO\\{$nombre}", $contenido);
    }

    public function info($tipo, $mensaje)
    {
        $this->escribir($tipo, 'INFO', $mensaje);
    }

    public function alert($tipo, $mensaje)
    {
        $this->escribir($tipo, 'ALERT', $mensaje);
    }

    public function warning($tipo, $mensaje)
    {
        $this->escribir($tipo, 'WARNING', $mensaje);
    }

    public function inicio($nivel, $mensaje)
    {
        $this->escribir('inicio', $nivel, $mensaje);
    }

    public function cierre($nivel, $mensaje)
    {
        $this->escribir('cierre', $nivel, $mensaje);
    }

    public function leer($tipo)
    {
        $contenido = null;

        if (Storage::disk('log')->has($this->ruta[$tipo])) {
            $contenido = Storage::disk('log')->get($this->ruta[$tipo]);
        }

        return $contenido;
    }

    public function lineas($tipo)
    {
        if ($this->leer($tipo) != null) {
            return array_reverse(explode("\r\n", $this->leer($tipo)));
        }

        return [];
    }

    public function mostrar($tipo)
    {
        $lineas = $this->lineas($tipo);

        $salida = [];

        for ($x = 0; $x < count($lineas); $x++) {

            if ($lineas[$x]) {

                $linea = explode(" ", $lineas[$x], 4);

                $salida[$x] = (object) [
                    'fecha' => Carbon::parse(
                        str_replace([ '[', ']' ], '', $linea[0].$linea[1])
                    ),
                    'nivel' => str_replace(['local.', ':'], '', $linea[2]),
                    'contenido' => $linea[3],
                ];
            }
        }

        return  collect($salida);
    }
}
