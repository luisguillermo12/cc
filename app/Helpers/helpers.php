<?php
setlocale(LC_ALL, 'es');

if(!function_exists('routeIs')) {
    function routeIs($url, $class = 'menu-open')
    {
        if (is_array($url)) {
            foreach ($url as $url) {
    	       if (Request::is($url))
                return $class;
            }
            return '';
        }
        return Request::is($url) ? $class : '';
    }
}

if(!function_exists('fechaBd')) {
    function fechaBd()
    {
        // Selecciona la fecha actual desde la base de datos.
        $sysdate = \DB::select('SELECT sysdate FROM dual')[0]->sysdate;

        // Transforma la fecha actual en un objeto de carbon.
        return \Carbon\Carbon::parse($sysdate);
    }
}

if(!function_exists('compensacion')) {
    function compensacion()
    {
        // Selecciona la fecha de compensacion.
        $compensacion = \DB::select('SELECT  valor FROM t_parametros_sistema WHERE id = 2')[0]->valor;

        // Transforma la fecha de compensación en un objeto de carbon.
        return \Carbon\Carbon::parse($compensacion);
    }
}

if(!function_exists('version')) {
    function version()
    {
    	return DB::select('SELECT valor FROM t_parametros_sistema WHERE id = 108')[0]->valor;
    }
}

