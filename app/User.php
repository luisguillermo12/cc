<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use  App\Models\Seguridad\Role;
use App\Models\Traits\PermisionModule;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\PasswordChange\PasswordChange;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use PermisionModule;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'status_description', 'confirmation_code', 'confirmed'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
        {
            return $this->belongsToMany(Role::class, 'assigned_roles')
             ->withPivot('role_id');
        }

    public function password_changes()
    {
        return $this->hasMany(PasswordChange::class);
    }

    public function isNew()
    {
        // Comentado hasta que se resuleva los requerimientos de seguridad.
        //return $this->password_changes->count() == 0;
        return false;
    }

}
