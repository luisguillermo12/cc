<?php

namespace App\Models\Reportes;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class RangoHora extends Model
{
    //use SoftDeletes;
    protected $table= 't_rangos_horas';
    protected $primarykey = 'id';
    protected $fillable = ['hora','nombre'];
    //protected $dates = ['deleted_at'];
}
