<?php

namespace App\Models\Reportes;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Grafica extends Model
{
    //use SoftDeletes;
    protected $table= 'grafica';
    protected $primarykey = 'id';
    protected $fillable = ['numero_click','numero_vista','fecha'];
    //protected $dates = ['deleted_at'];
}
