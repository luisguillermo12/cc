<?php

namespace App\Models\Reportes\Transaccional\TiempoLiquidacion;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;

class TiempoLiquidacion extends Model
{
    protected $table = 'v_tiempo_liquidacion';
    protected $fillable = ['num_liquidaciones', 'hora_archivo_stmtr', 'hora_archivo_syntr', 'hora_archivo_stmta', 'estatus', 'minutos'];

}
