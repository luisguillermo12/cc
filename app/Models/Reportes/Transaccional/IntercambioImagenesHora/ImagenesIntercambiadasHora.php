<?php

namespace App\Models\Reportes\Transaccional\IntercambioImagenesHora;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;

class ImagenesIntercambiadasHora extends Model
{
    protected $table = 'v_img_intercambiadas_hora';
    protected $connection = 'oraclecon';

    //protected $connection = 'oracle_2';


    protected $fillable = [
        'cod_banco_emisor', 'cod_banco_pagador', 'hora_id', 'hora', 'archivos_img_env', 'cant_operaciones_env', 'cant_img_env', 'cant_img_concilia_env', 'archivos_img_rec', 'bilat_cant_operaciones_rec', 'cant_img_rec', 'cant_img_concilia_rec'
    ];

     public function banco_emisor()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_emisor', 'codigo');
    }

      public function banco_receptor()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_pagador', 'codigo');
    }

}
