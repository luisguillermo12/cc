<?php

namespace App\Models\Reportes\tiempo_liquidacion_historico;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;

class t_histo_tiempo_liquidacion extends Model
{
    protected $table = 't_histo_tiempo_liquidacion';
    protected $connection = 'oracle_his';
    protected $fillable = ['histo_id','num_liquidaciones', 'hora_archivo_stmtr', 'hora_archivo_syntr', 'hora_archivo_stmta', 'estatus', 'minutos', 'fecha_dt'];

}
