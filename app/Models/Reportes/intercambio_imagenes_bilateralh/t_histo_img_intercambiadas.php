<?php

namespace App\Models\Reportes\intercambio_imagenes_bilateralh;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;

class t_histo_img_intercambiadas extends Model
{
    protected $table = 't_histo_img_intercambiadas';

    protected $connection = 'oraclecon_his';

    protected $fillable = [
        'histo_id' ,'cod_banco_emisor', 'cod_banco_pagador', 'archivos_img_env', 'cant_operaciones_env', 'cant_img_env', 'cant_img_concilia_env', 'archivos_img_rec', 'bilat_cant_operaciones_rec', 'cant_img_rec', 'cant_img_concilia_rec', 'fecha_dt'
    ];

     public function banco_emisor()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_emisor', 'codigo');
    }

      public function banco_receptor()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_pagador', 'codigo');
    }

}
