<?php

namespace App\Models\Reportes;


use Illuminate\Database\Eloquent\Model;

class EscalaMontos extends Model
{
    protected $table ="t_escala_montos";
    protected $primarykey = 'id';
    protected $fillable = ['mon_desde','mon_hasta'];
    }
