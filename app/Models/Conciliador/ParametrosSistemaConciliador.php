<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;

class ParametrosSistemaConciliador extends Model
{
  protected $table = 've_fbs_conciliador.t_parametros_sistema';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'nombre', 'valor', 'created_at', 'updated_at'
  ];
}
