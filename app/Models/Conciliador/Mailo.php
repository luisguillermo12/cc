<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;

class Mailo extends Model
{
  protected $table = 't_mailo';
  protected $primarykey = 'id';
    protected $connection = 'oraclecon';
  public $timestamps = true;

  protected $fillable = [
    'id', 'num_ibrn', 'num_cheque','cod_banco_pagador', 'sucursal_pagador', 'num_cuenta_pagador', 'cod_producto', 'cod_banco_emisor', 'sucursal_emisora', 'monto', 'longitud_anverso', 'longitud_reverso', 'longitud_firma_anverso', 'longitud_firma_reverso', 'serial_certificado', 'fecha_creacion', 'estatus', 'transferencia_archivo_id'
  ];
}
