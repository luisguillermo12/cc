<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;

class Operaciones extends Model
{
  protected $table = 've_fbs_conciliador.t_operaciones';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'num_ibrn', 'archivo_scs_id', 'num_cheque', 'cod_banco_emisor', 'cod_banco_receptor', 'num_digitos_control', 'num_lote', 'monto', 'num_cuenta', 'num_referencia', 'nom_pagador', 'hora', 'estatus_conciliacion'
  ];
}
