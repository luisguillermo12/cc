<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;
use App\Models\Conciliador\Imagenes;

class TransferenciasArchivos extends Model
{
  protected $table = 't_transferencias_archivos';
  protected $connection = 'oraclecon';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'ruta', 'nombre', 'tipo', 'cft_part_codigo', 'estatus', 'inicio_dt', 'fin_dt'
  ];

  public function imagenes()
  {
  	return $this->hasMany(Imagenes::class, 'transferencia_archivo_id');
  }

  public function cantImagenes()
  {
  	return count($this->imagenes);
  }

  
}
