<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;

class ArchivosSCS extends Model
{
  protected $table = 've_fbs_conciliador.t_archivos_scs';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'nombre', 'numero', 'cantidad_registros', 'tipo', 'monto', 'cod_banco_emisor', 'cod_banco_receptor', 'fecha_compensacion', 'hora', 'estatus'
  ];
}
