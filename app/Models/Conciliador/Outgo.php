<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;

class Outgo extends Model
{
  protected $table = 't_outgo';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'num_ibrn', 'num_cheque', 'num_referencia_interna', 'cod_banco_emisor', 'cod_banco_receptor', 'sucursal_emisora', 'cod_banco_pagador', 'sucursal_pagador', 'num_cuenta_pagador', 'cod_producto', 'monto', 'cod_moneda', 'archivo_entrada', 'num_lote', 'fecha_compensacion', 'fecha_liquidacion', 'fecha_creacion', 'estatus', 'transferencia_archivo_id' 
  ];

  protected $connection = 'oraclecon';

}
