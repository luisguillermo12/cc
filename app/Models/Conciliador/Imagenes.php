<?php

namespace App\Models\Conciliador;

use Illuminate\Database\Eloquent\Model;

class Imagenes extends Model
{
  protected $table = 't_imagenes';
  protected $primarykey = 'id';
  protected $connection = 'oraclecon';
  public $timestamps = true;

  protected $fillable = [
    'id', 'num_ibrn', 'archivo_scs_id', 'ruta_anverso', 'ruta_reverso', 'contenido_anverso', 'contenido_reverso', 'medida_anverso', 'medida_reverso', 'hora', 'estatus_conciliacion'
  ];
}
