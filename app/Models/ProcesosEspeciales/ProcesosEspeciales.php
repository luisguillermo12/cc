<?php

namespace App\Models\ProcesosEspeciales;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use Carbon\Carbon;
use App\Models\Access\User\User;
use App\Models\EjecucionAdministrativa\Proceso;


class ProcesosEspeciales extends Model //implements AuditableContract
{
    //use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 't_procesos_especiales';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'descripcion',
      
    ];
}