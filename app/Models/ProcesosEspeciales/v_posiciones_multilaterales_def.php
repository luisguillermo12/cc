<?php



namespace App\Models\ProcesosEspeciales;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;



class V_posiciones_multilaterales_def extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'V_posiciones_multilaterales_def';

    /**
     * The primary key of the table.
     *
     * @var string
     */
   // protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod_banco_emisor',
        'nombre',
        'posicion'
    ];
}