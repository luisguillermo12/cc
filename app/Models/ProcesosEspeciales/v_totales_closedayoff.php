<?php



namespace App\Models\ProcesosEspeciales;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;



class V_TOTALES_CLOSEDAYOFF extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'V_TOTALES_CLOSEDAYOFF';

    /**
     * The primary key of the table.
     *
     * @var string
     */
   // protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'total_operaciones',
        'credito_por_liquidar',
        'debito_por_liquidar',
        'total_por_liquidar',
        'credito_liquidado',
        'debito_liquidado',
        'total_liquidado'     
    ];
}