<?php

namespace App\Models\ProcesosEspeciales;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\Alarmas;
use Carbon\Carbon;
use App\Models\Access\User\User;
use App\Models\ProcesosEspeciales\ProcesosEspeciales;
use App\Models\Configuracion\Calendario\Calendario;


class v_procesos_especiales_exclusion extends Model implements AuditableContract
{
    use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'v_procesos_especiales_exclusion';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod_operacion',
        'id_procesos_especiales',
        'nombre_proceso',
        'user_id',
        'nombre_usuario',
        'estatus',
        'ip_conexion',
        'fecha',
        'cod_banco',
        'descripcion',
        'observaciones',
        'cargar',
        'validar',
        'tratar',
        'responder',
        'distribuir',
        'id_t_calendario'
    ];

    public $timestamps = false;


    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function proceso_especial()
    {
        return $this->belongsTo(ProcesosEspeciales::class, 'id_procesos_especiales');
    }

    public function calendario()
    {
        return $this->belongsTo(Calendario::class, 'id_t_calendario');
    }
}