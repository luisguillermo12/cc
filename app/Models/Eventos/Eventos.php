<?php

namespace App\Models\Eventos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Role\Role;

class Eventos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_eventos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod_evento',
        'nombre',
        'descripcion',
        'asunto',
        'mensaje',
        'destinatarios',
        'estatus',
        'correo',
        'sms',
    ];

    protected $append = [
        'destinatarios',
    ];

    public $timestamps = false;

    public function getDestinatariosAttribute($value)
    {
        return (object) json_decode($value, true);
    }
}