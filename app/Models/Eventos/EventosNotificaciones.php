<?php

namespace App\Models\Eventos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eventos\Eventos;
use App\Models\Access\User\User;
use App\Models\Access\Role\Role;

class EventosNotificaciones extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_eventos_notificaciones';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_evento',
        'fecha',
        'envio_email',
        'envio_sms',
    ];

    public $timestamps = false;

    public function evento()
    {
        return $this->belongsTo(Eventos::class, 'id_evento');
    }

    public function roles()
    {
        $roles_id = $this->evento->destinatarios->roles;

        if (is_null($roles_id)) {
            return [];
        }

        return Role::select('name')
        ->whereIn('id', $roles_id)
        ->get()
        ->pluck('name')
        ->toArray();
    }

    public function usuarios()
    {
        $users_id = $this->evento->destinatarios->usuarios;

        if (is_null($users_id)) {
            return [];
        }

        return User::select('name')
        ->whereIn('id', $users_id)
        ->get()
        ->pluck('name')
        ->toArray();
    }

    public function correos()
    {
        return $this->evento->destinatarios->correos;
    }
}