<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class EstadoGeneralPar extends Model
{
    protected $table = 'v_participante_general';

    //protected $connection = 'oracle_2';

    protected $primarykey = 'id';

    protected $fillable = [
        'cod_banco', 'nombre_banco', 'cod_tipo_operacion', 'fecha_inicio',
    ];
}
