<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class ResultadosCodigos extends Model
{
  protected $table = 't_resultados_codigos';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'app_codigo', 'cod_nivel', 'codigo', 'descripcion'
  ];
}
