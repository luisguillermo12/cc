<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Conciliador\Imagenes;

class TransferenciaArchivo extends Model
{
    protected $table = 't_transferencias_archivos';

    protected $primarykey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'id', 'ruta', 'nombre', 'tipo', 'cft_part_codigo', 'estatus', 'inicio_dt', 'fin_dt'
    ];

    /*
     * @var array Esta variable permite diferenciar si se trata de un
     *            archivo de entrada o de salida.
     */
    protected static $direccion = [
        'INITR' => 'in',
        'ICOM1' => 'in',
        'ICOM2' => 'in',
        'ICOMA' => 'out',
        'OUTGO' => 'out',
        'MAILI' => 'in',
        'MAILA' => 'out',
        'MAILO' => 'out',
    ];

    public static function totalArchivos($tipo = null, $emisor = null, $receptor = null, $moneda = null, $cod_resultado = null) 
    {
        return self::
        /* Realiza el join de acuerdo al tipo de archivo si es de entrada o de salida. */
        leftJoin('t_flujos_info', function ($join) use ($tipo) {
            if (isset($tipo) && $tipo != '*' && self::$direccion[$tipo] == 'in') {
                $join->on('t_flujos_info.transferencia_archivo_in_id', '=', 't_transferencias_archivos.id');
            } else {
                $join->on('t_flujos_info.transferencia_archivo_out_id', '=', 't_transferencias_archivos.id');
            }
        })
        /* Filtra por tipo de archivo. */
        ->where(function ($query) use ($tipo) {
            if (isset($tipo) && $tipo != '*') {
                $query->where('t_flujos_info.cod_flujo_tipo', '=', $tipo);
            }
        })
        /* Filtra por moneda. */
        ->where(function ($query) use ($moneda) {
            if (isset($moneda) && $moneda != '*') {
                $query->where('t_flujos_info.cod_moneda', $moneda);
            }
        })
        /* Filtra por banco emisor. */
        ->where(function ($query) use ($emisor, $tipo) {
            if (isset($emisor) && $emisor != '*') {
                if (self::$direccion[$tipo] == 'in') {
                    $query->where('t_flujos_info.enc_banco_emisor', '=', $emisor);
                } else {
                    $query->where('t_flujos_info.enc_banco_receptor', '=', $emisor);
                }
            }
        })
        /* Filtra por banco receptor. */
        ->where(function ($query) use ($receptor, $tipo) {
            if ($receptor && $receptor != '*') {
                $query->where('substr(t_transferencias_archivos.nombre, 6, 4)', '=', $receptor);
            }
        })
        /* Filtra por codigo de resultado. */
        ->where(function ($query) use ($cod_resultado) {
            if (isset($cod_resultado)) {
                if ($cod_resultado == 'parcial') {
                   $query->where('t_flujos_info.cod_resultado', '<>', '00');
                   $query->where('t_flujos_info.cod_resultado', '<>', '99');
                } elseif ($cod_resultado != '*') {
                   $query->where('t_flujos_info.cod_resultado', '=', $cod_resultado);
                }
            }
        })
        ->count();
    }

    public static function totalRecibidos($tipo = null, $emisor = null, $receptor = null, $cod_moneda = null)
    {
        return self::totalArchivos($tipo, $emisor, $receptor, $cod_moneda);
    }

    public static function totalAceptados($tipo = null, $emisor = null, $receptor = null, $cod_moneda = null)
    {
        return self::totalArchivos($tipo, $emisor, $receptor, $cod_moneda, '00');
    }

    public static function totalParcialmenteAceptados($tipo = null, $emisor = null, $receptor = null, $cod_moneda = null)
    {
        return self::totalArchivos($tipo, $emisor, $receptor, $cod_moneda, 'parcial');
    }

    public static function totalRechazados($tipo = null, $emisor = null, $receptor = null, $cod_moneda = null)
    {
        return self::totalArchivos($tipo, $emisor, $receptor, $cod_moneda, '98');
    }

    public static function archivosRecibidos($tipo, $banco = null, $moneda = null, $cod_resultado = null, $fecha = null)
    {
        return self::select(
            't_flujos_info.enc_banco_emisor',
            't_flujos_info.cod_moneda',
            't_flujos_info.enc_num_archivo',
            't_transferencias_archivos.inicio_dt',
            'SUM(t_flujos_lote.enc_num_transaccion) as total_operaciones',
            't_flujos_info.cod_resultado',
            't_validacion_detalle.codigo_descripcion',
            't_flujos_info.cod_flujo_info',
            't_transferencias_archivos.tipo'
        )
        ->where(function ($query) use ($tipo) {
            if (isset($tipo)) {
                if ($tipo == '*') {
                    $query->whereIn('t_transferencias_archivos.tipo', ['ICOM1', 'ICOM2']);
                    $query->whereIn('t_flujos_info.enc_flujo_tipo', ['ICOM1', 'ICOM2']);
                } else {
                    $query->where('t_transferencias_archivos.tipo', $tipo);
                    $query->where('t_flujos_info.enc_flujo_tipo', $tipo);
                }
            }
        })
        ->where(function ($query) use ($banco) {
            if (isset($banco) && $banco != '*') {
                $query->where('t_flujos_info.enc_banco_emisor', $banco);
            }
        })
        ->where(function ($query) use ($moneda) {
            if (isset($moneda) && $moneda != '*') {
                $query->where('t_flujos_info.cod_moneda', $moneda);
            }
        })
        ->where(function ($query) use ($cod_resultado) {
            if (isset($cod_resultado) && $cod_resultado != '*') {
                $query->where('t_flujos_info.cod_resultado', $cod_resultado);
            }
        })
        ->where(function ($query) use ($fecha) {
            if (isset($fecha) && $fecha != '*') {
                $query->where('SUBSTR(t_transferencias_archivos.inicio_dt,0,8)', $fecha);
            }
        })
        ->join('t_flujos_info', 't_flujos_info.transferencia_archivo_in_id', '=', 't_transferencias_archivos.id')
        ->leftJoin('t_validacion_detalle', 't_validacion_detalle.respuesta_codigo', '=', 't_flujos_info.cod_resultado')
        ->join('t_flujos_lote', 't_flujos_lote.cod_flujo_info', '=', 't_flujos_info.cod_flujo_info')
        ->groupBy(
            't_flujos_info.enc_banco_emisor',
            't_flujos_info.cod_moneda',
            't_flujos_info.enc_num_archivo',
            't_transferencias_archivos.inicio_dt',
            't_flujos_info.cod_resultado',
            't_validacion_detalle.codigo_descripcion',
            't_flujos_info.cod_flujo_info',
            't_transferencias_archivos.tipo'
        )
        ->orderBy('t_transferencias_archivos.inicio_dt', 'DESC')
        ->get();
    }

    public function imagenes()
    {
        return $this->hasMany(Imagenes::class, 'transferencia_archivo_id');
    }

    public function cantImagenes()
    {
        return count($this->imagenes);
    }
}
