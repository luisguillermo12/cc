<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Monitoreo\ValidacionDetalle;
use App\Models\MensajesInformativos\MensajesInformativos;

class Alarmas extends Model
{
  protected $table = 't_alarmas';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'transferencia_archivo_id', 'codigos_validaciones_eps_id', 'fecha_hora', 'estatus'
  ];
  
  protected $appends = [
    'descipcion'
  ];

    public function getDescipcionAttribute()
    {
        return $this->validacion()->descripcion;  
    }

    public function validacion()
    {
        return $this->belongsTo(ValidacionDetalle::class, 'codigos_validaciones_eps_id');  
    }

    public function mensajes()
    {
        return $this->belongsTo(MensajesInformativos::class, 'transferencia_archivo_id');  
    }
}
