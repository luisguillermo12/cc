<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class TransaccionCreditoLote extends Model
{
  protected $table = 't_trans_creditos_lotes';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'cod_flujo_lote', 'cod_trans_cre'
  ];
}
