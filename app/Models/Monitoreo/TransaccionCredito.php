<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class TransaccionCredito extends Model
{
  protected $table = 't_transacciones_creditos';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id',
    'flujo_lote_id',
    'rel_id',
    'tipo_operacion',
    'banco_emisor',
    'banco_destinatario',
    'moneda',
    'razon_devolucion',
    'archivo_entrada',
    'numero_lote',
    'fecha_compensacion',
    'numero_referencia',
    'monto',
    'fecha_intercambio',
    'fecha_liquidacion',
    'estatus',
    'codigo_rechazo',
    'subtipo_credito',
    'cuenta_origen',
    'cuenta_destino',
    'codigo_interno',
    'tipo_emisor',
    'identificacion_emisor',
    'nombre_emisor',
    'tipo_beneficiario',
    'identificacion_beneficiario',
    'nombre_beneficiario',
    'codigo_seguridad_tarjeta',
    'concepto',
    'fecha_creacion'
  ];
}
