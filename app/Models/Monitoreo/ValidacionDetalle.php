<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class ValidacionDetalle extends Model
{
    protected $table = 't_validacion_detalle';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'id', 'cod_tipo_operacion', 'codigo', 'cod_nivel', 'prioridad', 'tipo_campo', 'clas_campo', 'descripcion', 'valores_esperados',
    ];

    public static function descripcion($nivel, $codigo)
    {
        // Se valida por acá porque en la table t_validacion_detalle no existen los codigos 00 y 000
        if ($codigo == '00' || $codigo == '000') {
            return 'VALIDACIÓN COMPLETA';
        }

        $descripcion = self::select('codigo_descripcion')
        ->where('nivel', $nivel)
        ->where('respuesta_codigo', $codigo)
        ->first();

        return $descripcion != null ? $descripcion->codigo_descripcion : null;
    }
}
