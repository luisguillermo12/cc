<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class TransaccionDevDomiciliacionLote extends Model
{
  protected $table = 't_trans_dev_domiciliaciones_lotes';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'cod_flujo_lote', 'cod_trans_dev_dom'
  ];
}
