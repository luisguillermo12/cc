<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class FlujoTipo extends Model
{
  protected $table = 't_flujos_tipo';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
  	//
  ];
}
