<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;


class OperacionesValidadas2 extends Model
{
  protected $table = 'v_operaciones_validadas_2';



  protected $fillable = [
    'cod_tipo_operacion', 'num_transaccion_validadas', 'monto_validadas', 'num_transaccion_distribuidas', 'monto_distribuidas'
  ];
  


}
