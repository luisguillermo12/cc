<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class InicioOperacion extends Model
{
  protected $table = 't_inicio_operaciones';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'cod_banco_emisor', 'cod_moneda'
  ];
}
