<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class ArchivosRechazados extends Model
{
  protected $table = 't_archivos_rechazados';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
    'id', 'transferencia_archivo_id', 'estatus', 'cod_resultado', 'descripcion'
  ];
}
