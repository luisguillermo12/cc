<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class FlujoInfo extends Model
{
  protected $table = 't_flujos_info';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'cod_flujo_info', 'fecha_intercambio', 'cod_banco_emisor', 'num_archivo', 'cod_moneda', 'transferencia_archivo_in_id', 'transferencia_archivo_out_id', 'cod_flujo_tipo', 'enc_num_archivo', 'enc_banco_receptor', 'enc_fecha_intercambio', 'enc_banco_emisor', 'enc_moneda', 'enc_flujo_tipo', 'num_lote', 'cod_resultado', 'num_liquidacion', 'num_franja'
  ];
}
