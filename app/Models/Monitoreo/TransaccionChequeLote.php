<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class TransaccionChequeLote extends Model
{
  protected $table = 't_trans_cheques_lotes';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'cod_flujo_lote', 'cod_trans_che'
  ];
}
