<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class LiquidacionArchivos extends Model
{
  protected $table = 't_liquidaciones_archivos';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
    'id', 'fecha_hora_ejecucion', 'critico_ejecucion', 'estatus', 'num_liquidaciones'
  ];
}
