<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class DetallesAlarmas extends Model
{
  protected $table = 't_alarmas_detalle';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'alarma_id', 'usuario_id', 'observacion', 'descripcion'
  ];

}
