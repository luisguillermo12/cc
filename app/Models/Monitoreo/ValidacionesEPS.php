<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class ValidacionesEPS extends Model
{
    protected $table = 't_codigos_validaciones_eps';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'id', 'cod_tipo_operacion', 'codigo', 'cod_nivel', 'prioridad', 'tipo_campo', 'clas_campo', 'descripcion', 'valores_esperados',
    ];

    public static function descripcion($cod_tipo_operacion, $codigo)
    {
        $cod_tipo_operacion = substr($cod_tipo_operacion, 1);

        $descripcion = self::select('descripcion')
        ->where('cod_tipo_operacion', '0'.$cod_tipo_operacion)
        ->where('codigo', $codigo)
        ->first();

        return $descripcion != null ? $descripcion->descripcion : null;
    }
}
