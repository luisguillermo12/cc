<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class FlujoLote extends Model
{
  protected $table = 't_flujos_lote';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'cod_flujo_lote', 'cod_flujo_info', 'num_lote', 'num_ref_lote', 'cod_tipo_operacion', 'cod_banco_destinatario', 'enc_monto', 'enc_num_lote', 'monto', 'num_transaccion', 'cod_resultado'
  ];
}
