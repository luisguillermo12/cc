<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class LiquidacionArchivosDetalle extends Model
{
  protected $table = 't_liquidaciones_archivos_detalle';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'liquidacion_archivo_id', 'producto_id', 'cod_tipo_operacion', 'num_liquidacion', 'num_franja'
  ];
}
