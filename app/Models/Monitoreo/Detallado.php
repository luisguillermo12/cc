<?php

namespace App\Models\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class Detallado extends Model
{

    public static function TotalCantOperaciones($cod_banco = null, $operacion = null, $fecha = null)
    {

        $suma = self::from('t_flujos_info')
        ->whereIn('cod_flujo_tipo', ['ICOM1', 'ICOM2'])
        ->where(function($query) use ($cod_banco, $fecha) {
            if ($cod_banco != '*') {
                $query->where('cod_banco_emisor', $cod_banco);
            };
            if ($fecha != '*') {
                $query->where('fecha_intercambio', $fecha);
            };
        })
        ->leftJoin('t_flujos_lote', 't_flujos_info.cod_flujo_info', '=', 't_flujos_lote.cod_flujo_info')
        ->where(function($query) use ($operacion) {
            if ($operacion != null && is_array($operacion)) {
                $query->whereIn('t_flujos_lote.cod_tipo_operacion', $operacion);
            } elseif($operacion != null) {
                $query->where('t_flujos_lote.cod_tipo_operacion', $operacion);
            };
        })
        ->sum('num_transaccion');

        $total = ($suma != null) ? $suma : 0;

        return $total;
    }

    public static function TotalOperaciones($cod_banco = null, $operacion = null, $fecha = null)
    {

        $suma = self::from('t_flujos_info')
        ->whereIn('cod_flujo_tipo', ['ICOM1', 'ICOM2'])
        ->where(function($query) use ($cod_banco, $fecha) {
            if ($cod_banco != '*') {
                $query->where('cod_banco_emisor', $cod_banco);
            };
            if ($fecha != '*') {
                $query->where('fecha_intercambio', $fecha);
            };
        })
        ->leftJoin('t_flujos_lote', 't_flujos_info.cod_flujo_info', '=', 't_flujos_lote.cod_flujo_info')
        ->where(function($query) use ($operacion) {
            if ($operacion != null || !is_array($operacion)) {
                $query->where('t_flujos_lote.cod_tipo_operacion', $operacion);
            } else {
                $query->whereIn('t_flujos_lote.cod_tipo_operacion', $operacion);
            };
        })
        ->sum('monto');

        $total = ($suma != null) ? $suma : 0;

        return $total;
    }
}
