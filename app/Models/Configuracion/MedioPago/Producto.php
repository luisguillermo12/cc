<?php

namespace App\Models\Configuracion\MedioPago;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Producto extends Model //implements AuditableContract
{

    //use Auditable;

    protected $table = 't_productos';
    protected $primarykey = 'id';

    protected $fillable = [
     'nombre','estatus', 'limite_financiero','codigo','limite_bajo_valor'
    ];

    public function sub_producto() {

      // belong to -- pertenece
      return $this->hasMany(SubProducto::class, 'producto_id');
    }

    public function tipo_operacion() {
      // belong to -- pertenece
      return $this->hasMany(TipoOperacion::class);
    }

    public static function producto($signo)
    {
        if ($signo != '*')
        {
          return Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
          ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
          ->where('t_tipos_operaciones.signo', $signo)
          ->where('t_productos.estatus', 'ACTIVO')
          ->orderBy('t_productos.codigo', 'asc')
          ->distinct()
          ->get();
        }
        else
        {
          return Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
          ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
          ->where('t_productos.estatus', 'ACTIVO')
          ->orderBy('t_productos.codigo', 'asc')
          ->distinct()
          ->get();
        }

    }
}
