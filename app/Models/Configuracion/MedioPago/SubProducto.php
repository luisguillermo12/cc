<?php

namespace App\Models\Configuracion\MedioPago;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SubProducto extends Model //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_sub_productos';
    protected $primarykey = 'id';

    protected $fillable = [
      'id','producto_id','codigo','nombre','estatus','exonerado'
    ];

    protected $appends = [
        'nombre_completo',
    ];

    public function producto() {
      // hasmany - tiene muchas
      return $this->belongsTo(Producto::class);
    }

    public function getNombreCompletoAttribute()
    {
      return $this->codigo . ' - ' . $this->nombre;
    }
}
