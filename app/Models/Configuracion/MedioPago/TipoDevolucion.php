<?php

namespace App\Models\Configuracion\MedioPago;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TipoDevolucion extends Model //implements AuditableContract
{
    //use Auditable;

  protected $table = 't_tipos_devoluciones';
  protected $primarykey = 'id';

  protected $fillable = [
    'id','tipo_operacion_id','codigo','nombre','estatus', 'producto_id'
  ];

  public function tipo_operacion() {
    // hasmany - tiene muchas
    return $this->belongsTo(TipoOperacion::class);
  }
}
