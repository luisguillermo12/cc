<?php

namespace App\Models\Configuracion\MedioPago;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Moneda\MonedaOperacion;

class TipoOperacion extends Model
{
  protected $table = 't_tipos_operaciones';
  protected $primarykey = 'id';

  protected $fillable = [
    'id','producto_id','codigo','signo','nombre','direccion','estatus'
  ];

  public function producto() {
    // hasmany - tiene muchas
    return $this->belongsTo(Producto::class, 'producto_id' );
  }

  public function razon_devolucion() {
    // belong to -- pertenece
    return $this->belongsto(RazonDevolucion::class);
  }

  public function moneda_operacion() {
    // hasmany - tiene muchas
    return $this->hasmany(MonedaOperacion::class);
  }

  public static function operacion($signo)
  {
      return TipoOperacion::select('codigo','nombre')
      ->where('signo','=', $signo)
      ->orderBy('codigo', 'asc')
      ->get();
  }
}
