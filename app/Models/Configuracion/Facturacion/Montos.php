<?php

namespace App\Models\Configuracion\Facturacion;

use Illuminate\Database\Eloquent\Model;

class Montos extends Model
{
  protected $table = 't_facturacion_montos';
  protected $primarykey = 'id';
  protected $fillable = [
    'id', 'codigo_iso','nombre','num_decimales','limite'
  ];
}
