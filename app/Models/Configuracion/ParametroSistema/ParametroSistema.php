<?php

namespace App\Models\Configuracion\ParametroSistema;

use Illuminate\Database\Eloquent\Model;

class ParametroSistema extends Model
{
  protected $table = 't_parametros_sistema';
  protected $primarykey = 'id';
  protected $fillable = [
    'id', 'nombre', 'valor'
  ];
}
