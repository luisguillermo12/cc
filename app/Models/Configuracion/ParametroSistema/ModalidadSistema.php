<?php

namespace App\Models\Configuracion\ParametroSistema;

use Illuminate\Database\Eloquent\Model;

class ModalidadSistema extends Model
{
  protected $table = 't_modalidad_sistema';
  protected $primarykey = 'id';
  protected $fillable = [
    'id', 'nombre', 'valor'
  ];
}
