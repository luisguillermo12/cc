<?php

namespace App\Models\Configuracion\SemanaCompensacion;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SemanaCompensacion extends Model  //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_semanas';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
      'id', 'dia', 'estatus', 'validacion'
    ];
}
