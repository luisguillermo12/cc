<?php

namespace App\Models\Configuracion\EmpresasOrdenantes;

use Illuminate\Database\Eloquent\Model;



class EmpresaOrdenante extends Model
{
    protected $table = 't_empresas_ordenantes';
    protected $primarykey = 'id';

    protected $fillable = [
      'id','rif','nombre','estatus','cod_banco_asociado'
    ];

}
