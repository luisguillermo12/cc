<?php

namespace App\Models\Configuracion\Calendario;

use Illuminate\Database\Eloquent\Model;
//use OwenIt\Auditing\Auditable;
//use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;

class Calendario extends Model //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_calendario';
    protected $primarykey = 'id';
    protected $fillable = [
      'id', 'fecha', 'fecha_validacion','estatus',
    ];

    public function day_off()
    {
        return $this->hasMany(RegistroProcesosEspeciales::class, 'id_t_calendario');
    }
}
