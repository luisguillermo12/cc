<?php

namespace App\Models\Configuracion\Calendario;

use Illuminate\Database\Eloquent\Model;
//use OwenIt\Auditing\Auditable;
//use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;
use Carbon\Carbon;

class FechasCalendario extends Model //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_fechas_calendario';
    protected $primarykey = 'id';
    protected $fillable = [
      'id', 'fecha', 'fecha_validacion', 'observacion', 'estatus', 'created_at', 'updated_at'
    ];

    public function getFechaValidacionAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getDescripcionAttribute($value)
    {
        return $this->registro->descripcion;
    }

    public function registro()
    {
        return $this->hasOne(RegistroProcesosEspeciales::class, 'id_t_calendario');
    }
}
