<?php

namespace App\Models\Configuracion\ValidacionesSistema;

use Illuminate\Database\Eloquent\Model;

class ValidacionesSistema extends Model
{
  protected $table = 't_validacion_detalle';
  public $timestamps = false;
  protected $primarykey = 'id';
  protected $fillable = [
    'id', 'codigo_interno_error', 'nivel', 'respuesta_accion' , 'respuesta_codigo',
    'codigo_descripcion', 'causa', 'archivos_aplica'
  ];
}
