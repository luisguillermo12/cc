<?php

namespace App\Models\Configuracion\Moneda;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CodigoIso extends Model //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_codigo_iso';
    protected $primarykey = 'id';

    protected $fillable = [
      'id', 'codigo_iso', 'nombre', 'num_decimales', 'limite_financiero', 'monto_max_bv', 'estatus'
    ];

    protected $appends = [
      'nombre_completo'
    ];

    public function monedaoperacion() {
      // hasmany - tiene muchas
      return $this->hasmany(MonedaOperacion::class);
    }

    public function getNombreCompletoAttribute() {
      return "{$this->codigo_iso} - {$this->nombre}";
    }
}
