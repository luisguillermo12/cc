<?php

namespace App\Models\Configuracion\Moneda;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\MedioPago\TipoOperacion;

class MonedaOperacion extends Model
{
  protected $table = 't_monedas_operaciones';
  protected $primarykey = 'id';

  protected $fillable = [
    'id', 'moneda_id', 'tipo_operacion_id', 'limite_financiero'
  ];

  public function moneda() {
    // belong to -- pertenece
    return $this->belongsto(Moneda::class);
  }

  public function tipo_operacion() {
    // belong to -- pertenece
    return $this->belongsto(TipoOperacion::class);
  }
}
