<?php

namespace App\Models\Configuracion\Banco;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;
//use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Banco extends Model //implements AuditableContract
{
    //use Auditable;
    //use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_bancos';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['codigo','nombre','estatus','liquidacion_idt','fecha_ingreso',
                           'modalidad_participacion','banco_representante_id','banco_principal_id','observaciones','fecha_absorsion', 'deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha_ingreso',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        'nombre_completo',
    ];


    /**
     * Buscar IB Activas
     *
     * @param  string|array $prepend valor para agregar al inicio de la lista.
     * @param  boolean $chequearIB determina si se debe verificar si el usuario actual es una IB.
     * @return array
     */
    public static function buscarTodos($prepend = ['*', '*'], $chequearIB = true)
    {
        /* Variable que debe comprobar si el rol de usuario actual es una IB. */
        /* Por ahora sólo devuelve false */
        $esIB = !$chequearIB ? false : false;

        /* Variable que debe tomar el código de la IB conectada. */
        /* Por ahora sólo devuelve "0105" */
        $cod_banco = !$esIB ?: '0105';

        $bancos = self::select('codigo', 'nombre')
        ->where('estatus','ACTIVO')
        ->where(function($query) use ($esIB) {
            // Si el rol de usuario actual es una IB
            // filtrar por el codigo bancario de IB
            if ($esIB) {
                $query->where('codigo', $cod_banco);
            };
        })
        ->orderBy('codigo', 'asc')
        ->get();

        $bancos = $bancos->keyBy('codigo')
        ->map(function ($item, $key) {
            return $item->nombre_completo;
        });

        // Si el rol de usuario actual es una IB
        // no agregar $prepend al inicio de la lista
        if (!$esIB && is_array($prepend)) {

            $bancos->prepend($prepend[0], $prepend[1]);

        } elseif(!$esIB && $prepend) {

            $bancos->prepend($prepend, '');
        };

        return $bancos;
    }

    public static function buscaruno($cod_banco)
    {
        $banco = self::select('codigo', 'nombre')
        
        ->where('codigo',$cod_banco)
        ->first();

        return $banco;
    }


    public function getNombreCompletoAttribute()
    {
        return $this->codigo.' - '.$this->nombre;
    }

    public function getProcesoExclusionAttribute()
    {
        return  RegistroProcesosEspeciales::select('estatus')
        ->where('cod_banco', $this->codigo)
        ->orderBy('id', 'DESC')
        ->first();
    }
}
