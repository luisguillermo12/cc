<?php

namespace App\Models\Configuracion\Banco;

use Illuminate\Database\Eloquent\Model;

class EstadoBanco extends Model
{

  protected $table= 't_historico_estado_banco';
  protected $primarykey = 'id';

  protected $fillable = ['id','estatus_anterior','estatus_actual','codigo_banco'];
}
