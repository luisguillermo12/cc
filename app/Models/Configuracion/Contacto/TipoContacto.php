<?php

namespace App\Models\Configuracion\Contacto;

use Illuminate\Database\Eloquent\Model;
//use OwenIt\Auditing\Auditable;
//use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TipoContacto extends Model //implements AuditableContract
{

  //use Auditable;

  protected $table = 't_tipos_contactos';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'nombre', 'estatus'
  ];
}
