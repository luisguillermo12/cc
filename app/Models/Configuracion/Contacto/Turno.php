<?php

namespace App\Models\Configuracion\Contacto;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Turno extends Model
{
    use Auditable;

    protected $table = 't_turnos_contactos';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
      'id', 'turno'
    ];
}
