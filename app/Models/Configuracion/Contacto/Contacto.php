<?php

namespace App\Models\Configuracion\Contacto;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Contacto extends Model //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_contactos';
    protected $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
      'id', 'cedula', 'nombre', 'cargo','telefono_celular','email','estatus','telefono_local'
    ];

    public function contactobancario() {
      // hasmany - tiene muchas
      return $this->hasmany(ContactoBancario::class);
    }

    public function contactoente() {
      // hasmany - tiene muchas
      return $this->hasmany(ContactoEnte::class);
    }
}
