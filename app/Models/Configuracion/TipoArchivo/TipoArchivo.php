<?php

namespace App\Models\Configuracion\TipoArchivo;

use Illuminate\Database\Eloquent\Model;

class TipoArchivo extends Model
{
  protected $table = 't_flujos_tipo';
  protected $primarykey = 'id';
  public $timestamps = true;

  protected $fillable = [
    'id', 'codigo', 'nombre', 'tipo', 'descripcion'
  ];
}
