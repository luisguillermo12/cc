<?php

namespace App\Models\Configuracion\SesionCompensacion;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Carbon\Carbon;

class Periodo extends Model //implements AuditableContract
{
    // use Auditable;

    protected $table = 't_periodos';
    protected $primarykey = 'id';

    protected $fillable = [
      'id','producto_id','dia_inicio','hora_inicio','dia_fin','hora_fin','num_liquidacion','num_franja','estatus'
    ];

    public function producto() {
      // hasmany - tiene muchas
      return $this->hasmany(Producto::class);
    }

    public function intervalo_liquidacion() {
      // hasmany - tiene muchas
      return $this->hasmany(IntervaloLiquidacion::class);
    }

    // public function getHoraInicioAttribute($value)
    // {
    //     return Carbon::createFromFormat('H:i', $value);
    // }

}
