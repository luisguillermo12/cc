<?php

namespace App\Models\Configuracion\SesionCompensacion;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\MedioPago\TipoOperacion;
//use OwenIt\Auditing\Auditable;
//use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class IntervaloLiquidacion extends Model //implements AuditableContract
{
    //use Auditable;

    protected $table = 't_intervalos_liquidacion';
    protected $primarykey = 'id';

    protected $fillable = [
      'id','periodo_id','tipo_operacion_id','dia_inicio','hora_inicio','dia_fin','hora_fin','num_liquidacion', 'num_cmpsa', 'num_franja'
    ];

    public function tipo_operacion() {
      // hasmany - tiene muchas
      return $this->hasmany(TipoOperacion::class);
    }
}
