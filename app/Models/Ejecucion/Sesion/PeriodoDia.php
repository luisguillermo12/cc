<?php

namespace App\Models\Ejecucion\Sesion;

use Illuminate\Database\Eloquent\Model;

class PeriodoDia extends Model
{
  protected $table = 't_periodos_dia';
  protected $primarykey = 'id';

  protected $fillable = [
    'id','producto_id','fecha_compensacion','hora_inicio','hora_fin','nro_sesion','nro_franja'
  ];

  public function producto() {
    // hasmany - tiene muchas
    return $this->hasmany(Producto::class);
  }

  public function sesion_franja() {
    // hasmany - tiene muchas
    return $this->hasmany(SesionFranja::class);
  }
}
