<?php

namespace App\Models\Ejecucion\Sesion;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\MedioPago\TipoOperacion;

class SesionFranjaDia extends Model
{
  protected $table = 't_sesiones_franjas_dia';
  protected $primarykey = 'id';

  protected $fillable = [
    'id','periodo_id','tipo_operacion_id','fechahora_inicio','fechahora_fin','critico_inicio_dt','critico_fin_dt','nro_sesion','nro_franja'
  ];

  public function tipo_operacion() {
    // hasmany - tiene muchas
    return $this->hasmany(TipoOperacion::class);
  }
}
