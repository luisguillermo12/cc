<?php

namespace App\Models\MensajesInformativos;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\Alarmas;
use Carbon\Carbon;
use App\Models\Access\User\User;
use App\Models\EjecucionAdministrativa\Proceso;


class MensajesInformativos extends Model //implements AuditableContract
{
    //use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_mensaje_informativo';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        't_transferencia_archivo_id',
        'codigo_participante_emisor',
        'id_participante_emisor',
        'codigo_participante_receptor',
        'id_participante_receptor',
        'codigo_iso_moneda',
        't_moneda_id',
        'numero_archivo',
        'fecha_archivo',
        'fecha intercambio',
        'mensaje',
        'usuario_id',
        'tipo_mensaje',
        'cargar',
        'respuesta',
        'tratar',
        'distribuir',
    ];

    public $timestamps = false;

    public function getFechaIntercambioAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function emisor()
    {
        return $this->belongsTo(Banco::class, 'id_participante_emisor');
    }

    public function receptor()
    {
        return $this->belongsTo(Banco::class, 'id_participante_receptor');
    }

    public function moneda()
    {
        return $this->belongsTo(Moneda::class, 't_moneda_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public static function alarmas()
    {
        return self::where('t_mensaje_informativo.estatus', 'REJECTED')
        ->join('t_alarmas', 't_mensaje_informativo.id', '=', 't_alarmas.transferencia_archivo_id')
        ->join('t_alarmas', 2, '=', 't_alarmas.cod_tipo_proc_rechazo')
        ->join('t_validacion_detalle', 't_alarmas.codigos_validaciones_eps_id', '=', 't_validacion_detalle.id')
        ->select(
            't_mensaje_informativo.nombre_archivo_entrada',
            't_alarmas.id',
            't_alarmas.estatus',
            't_alarmas.fecha_hora',
            't_validacion_detalle.respuesta_codigo',
            't_validacion_detalle.codigo_descripcion'
        )
        ->get();
    }

    public static function alarma($id)
    {
        return self::where('t_alarmas.id', $id)
        ->join('t_alarmas', 't_mensaje_informativo.id', '=', 't_alarmas.transferencia_archivo_id')
        ->join('t_validacion_detalle', 't_alarmas.codigos_validaciones_eps_id', '=', 't_validacion_detalle.id')
        ->select(
            't_mensaje_informativo.nombre_archivo_entrada',
            't_mensaje_informativo.codigo_participante_emisor as participante_emisor',
            't_alarmas.id',
            't_alarmas.estatus',
            't_alarmas.fecha_hora',
            't_validacion_detalle.respuesta_codigo as codigo',
            't_validacion_detalle.codigo_descripcion as descripcion'
        )
        ->get();
    }
}