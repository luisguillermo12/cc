<?php
namespace App\Models\Seguridad\PortalParticipantes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Seguridad\PortalParticipantes\Role;
use App\Models\Traits\PermisionModule;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use PermisionModule;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'status_description', 'confirmation_code', 'confirmed'];
    protected $connection = 'oraclepar';



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
        {
            return $this->belongsToMany(Role::class, 'assigned_roles')
             ->withPivot('role_id');
        }

}

