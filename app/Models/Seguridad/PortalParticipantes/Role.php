<?php

namespace App\Models\Seguridad\PortalParticipantes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Seguridad\PermissionP;
use App\User;
/**
 * Class Role
 * @package App\Models\Access\Role
 */
class Role extends Model
{

    protected $table;

    protected $fillable = ['name', 'todos', 'SORT'];

    protected $connection = 'oraclepar';


    public function permissions()
        {
            return $this->belongsToMany(Permission::class, 'permission_role')
             ->withPivot('permission_id');
        }

     public function users()
        {
            return $this->belongsToMany(user::class, 'assigned_roles')
             ->withPivot('user_id');
        }
}
