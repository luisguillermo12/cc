<?php

namespace App\Models\Cobranza;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;

class TotalAltoValor extends Model implements AuditableContract
{
    use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_histo_totales_alto_valor';

    protected $connection = 'oracle_his';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fecha_compensacion', 'monto_bajo_valor', 'cod_banco_emisor', 'cod_tipo_operacion', 'cantidad_total', 'monto_total'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        //'fecha'
    ];

    public function banco()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_emisor', 'codigo');
    }

    public static function consulta($banco = null, $operacion = null, $desde = null, $hasta = null)
    {


        if ($desde == '*') {
            $desde = $hasta;
        } elseif ($hasta == '*') {
            $hasta = $desde;
        }

        return self::where(function($query) use ($banco) {
            if ($banco != '*') {
                $query->where('cod_banco_emisor', $banco);
            }

        })
        ->where(function($query) use ($operacion) {
            if ($operacion != '*') {
                $query->where('cod_tipo_operacion', $operacion);
            }

        })
        ->where(function($query) use ($desde, $hasta) {
            if ($desde != '*') {
                $query->whereRaw("FECHA_COMPENSACION_DT BETWEEN TO_DATE ({$desde}, 'YYYYMMDD') AND TO_DATE ({$hasta}, 'YYYYMMDD')");
            }

        })
        ->get()
        ->sortByDesc('fecha')
        ->sortBy('cod_tipo_operacion')
        ->sortBy('cod_banco_emisor')
        ->groupBy('cod_banco_emisor')
        ;

    }

       public static function totales_alto_valor($banco = null, $operacion = null, $desde = null, $hasta = null)
    {


        if ($desde == '*') {
            $desde = $hasta;
        } elseif ($hasta == '*') {
            $hasta = $desde;
        }

        return self::where(function($query) use ($banco) {
            if ($banco != '*') {
                $query->where('cod_banco_emisor', $banco);
            }

        })
        ->where(function($query) use ($operacion) {
            if ($operacion != '*') {
                $query->where('cod_tipo_operacion', $operacion);
            }

        })
        ->where(function($query) use ($desde, $hasta) {
            if ($desde != '*') {
                $query->whereRaw("FECHA_COMPENSACION_DT BETWEEN TO_DATE ({$desde}, 'YYYYMMDD') AND TO_DATE ({$hasta}, 'YYYYMMDD')");
            }

        })
        ->get()
        ->sortByDesc('fecha')
        ->sortBy('cod_tipo_operacion')
        ->sortBy('cod_banco_emisor')
        ->groupBy('cod_banco_emisor')
        ;

    }
}
