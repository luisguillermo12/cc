<?php

namespace App\Models\Cobranza;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;

class ParticipacionBanco extends Model implements AuditableContract
{
    use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_histo_participacion_bancos';

    protected $connection = 'oracle_his';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fecha_compensacion', 'cod_banco_emisor', 'tipo_operacion', 'cant_operaciones', 'monto'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha',
    ];

    public function banco()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_emisor', 'codigo');
    }

    public static function operaciones($banco = null, $operacion = null, $desde = null, $hasta = null)
    {
        if ($desde == '*') {
            $desde = $hasta;
        } elseif ($hasta == '*') {
            $hasta = $desde;
        }

        return self::with('banco')
        ->where(function($query) use ($banco) {
            if ($banco != '*') {
                $query->where('cod_banco_emisor', $banco);
            }

        })
        ->where(function($query) use ($operacion) {
            if ($operacion != '*') {
                $query->where('tipo_operacion', $operacion);
            }

        })
        ->where(function($query) use ($desde, $hasta) {
            if ($desde != '*') {
                $query->whereRaw("TO_DATE(fecha_compensacion,'yyyy/mm/dd') BETWEEN TO_DATE ({$desde}, 'yyyy/mm/dd') AND TO_DATE ({$hasta}, 'yyyy/mm/dd')");
            }

        })
        ->get();
    }

    public static function consulta($banco = null, $operacion = null, $desde = null, $hasta = null)
    {
        return self::operaciones($banco, $operacion, $desde, $hasta)
        ->sortByDesc('fecha_compensacion')
        ->groupBy('cod_banco_emisor');
        //->sort();
    }
}
