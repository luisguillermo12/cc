<?php

namespace App\Models\Cobranza;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configuracion\Banco\Banco;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;

class OperacionesExoneradas extends Model implements AuditableContract
{
    use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 'v_operaciones_exoneradas';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cod_tipo_operacion','fecha_compensacion','cod_banco_emisor', 'cod_banco_receptor', 'sub_tipo_producto', 'monto', 'estatus'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        //'fecha'
    ];

    public function banco_emisor()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_emisor', 'codigo');
    }

     public function banco_receptor()
    {
        return $this->belongsTo(Banco::class, 'cod_banco_receptor', 'codigo');
    }

    public static function consulta($banco = null, $banco_r = null, $operacion = null, $desde = null, $hasta = null)
    {


        if ($desde == '*') {
            $desde = $hasta;
        } elseif ($hasta == '*') {
            $hasta = $desde;
        }

        return self::where(function($query) use ($banco) {
            if ($banco != '*') {
                $query->where('cod_banco_emisor', $banco);
            }

        })
        ->where(function($query) use ($banco_r) {
            if ($banco_r != '*') {
                $query->where('cod_banco_receptor', $banco_r);
            }

        })
        ->where(function($query) use ($operacion) {
            if ($operacion != '*') {
                $query->where('cod_tipo_operacion', $operacion);
            }

        })
        ->where(function($query) use ($desde, $hasta) {
            if ($desde != '*') {
                $query->whereRaw("FECHA_COMPENSACION_DT BETWEEN TO_DATE ({$desde}, 'YYYYMMDD') AND TO_DATE ({$hasta}, 'YYYYMMDD')");
            }

        })
        ->get()
        //->sortBy('sub_tipo_producto')
        ->sortBy('cod_banco_emisor')
        ->sortByDesc('cod_tipo_operacion')
        ->sortBy('fecha_compensacion');
        //->groupBy('cod_banco_emisor');
       // ->sortBy('tipo_operacion');

    }

    //permite busqueda por agrupacion de fechas, consolidado por el rango seleccionado
    public static function consulta_consolidado($banco = null, $banco_r = null, $operacion = null, $desde = null, $hasta = null)
    {
        return self::select('cod_tipo_operacion','cod_banco_emisor','cod_banco_receptor','sub_tipo_producto', 'estatus', DB::raw("sum(monto) as monto"))
          ->whereRaw("(fecha_compensacion_dt BETWEEN TO_DATE ({$desde}, 'YYYYMMDD') AND TO_DATE ({$hasta}, 'YYYYMMDD'))")
          ->whereRaw("(cod_banco_emisor = '{$banco}' OR '{$banco}' = '*')")
          ->whereRaw("(cod_banco_receptor = '{$banco_r}' OR '{$banco_r}' = '*')")
          ->whereRaw("(cod_tipo_operacion = '{$operacion}' OR '{$operacion}' = '*')")
          ->orderBy('cod_tipo_operacion', 'asc')
          ->orderBy('cod_banco_emisor', 'asc')
          ->orderBy('cod_banco_receptor', 'asc')
          ->groupBy('cod_tipo_operacion')
          ->groupBy('cod_banco_emisor')
          ->groupBy('cod_banco_receptor')
          ->groupBy('sub_tipo_producto')
          ->groupBy('estatus')
          ->get();
    }

/*       public static function totales_alto_valor($banco = null, $operacion = null, $desde = null, $hasta = null)
    {


        if ($desde == '*') {
            $desde = $hasta;
        } elseif ($hasta == '*') {
            $hasta = $desde;
        }

        return self::where(function($query) use ($banco) {
            if ($banco != '*') {
                $query->where('cod_banco_emisor', $banco);
            }

        })
        ->where(function($query) use ($operacion) {
            if ($operacion != '*') {
                $query->where('cod_tipo_operacion', $operacion);
            }

        })
        ->where(function($query) use ($desde, $hasta) {
            if ($desde != '*') {
                $query->whereRaw("TO_DATE(FECHA_COMPENSACION,'yyyy/mm/dd') BETWEEN TO_DATE ({$desde}, 'yyyy/mm/dd') AND TO_DATE ({$hasta}, 'yyyy/mm/dd')");
            }

        })
        ->get()
        ->sortByDesc('fecha')
        ->sortBy('cod_banco_emisor')
        ->groupBy('cod_banco_emisor');
        //->sortBy('cod_tipo_operacion');

    } */
}
