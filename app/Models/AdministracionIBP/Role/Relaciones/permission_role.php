<?php

namespace App\Models\AdministracionIBP\Role\Relaciones;

use App\Models\AdministracionIBP\User\user;
use App\Models\AdministracionIBP\Role\Role;
//use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use App\Models\AdministracionIBP\Role\Relaciones\Permission;

class permission_role extends Model
{
     protected $table= 'permission_role';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['permission_id', 'role_id'];
    protected $connection = 'oraclepar';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $primarykey = 'id';
    public $timestamps = false;
    /**
     * @var array
     */

    public function permission()
    {
        return $this->belongsToMany(Permission::class,'id', 'permission_id' );
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class,'id', 'role_id' );
    }
}
