<?php

namespace App\Models\AdministracionIBP\Role;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Role\Traits\RoleAccess;
use App\Models\Access\Role\Traits\Attribute\RoleAttribute;
//use App\Models\Access\Role\Traits\Relationship\RoleRelationship;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\AdministracionIBP\Role\Relaciones\Permission;
use App\Models\AdministracionIBP\User\user;
/**
 * Class Role
 * @package App\Models\Access\Role
 */
class Role extends Model 
{
   // use RoleAccess, RoleAttribute, RoleRelationship, Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;
    protected $connection = 'oraclepar';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'todos', 'SORT'];

    /**
     * @param array $attributes
     */
  /*  public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.roles_table');
    }*/

    public function permissions()
        {
            return $this->belongsToMany(Permission::class, 'permission_role')
             ->withPivot('permission_id');
        }

     public function users()
        {
            return $this->belongsToMany(user::class, 'assigned_roles')
             ->withPivot('user_id');
        }
}
