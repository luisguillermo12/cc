<?php

namespace App\Models\AdministracionIBP\User;

use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Models\Access\User\Traits\UserPasswordChanges;
use App\Models\Access\User\Traits\PermisionModule;

use App\Models\AdministracionIBP\Role\Role;
use App\Models\Configuracion\Banco\Banco;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable implements AuditableContract, UserResolver
{

     use SoftDeletes,
        UserAccess,
        UserAttribute,
        UserRelationship,
        Auditable,
        UserPasswordChanges,
        PermisionModule;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'status_description', 'confirmation_code', 'confirmed', 'cod_banco'];
    protected $connection = 'oraclepar';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at','last_login','updated_at'];

    //protected $auditExclude = [
    //'remember_token',
    //'confirmed',
    //'confirmation_code',
  //];

  public static function resolveId()
  {
      return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
  }

 public function roles()
        {
            return $this->belongsToMany(Role::class, 'assigned_roles')
             ->withPivot('role_id');
        }
public function banco()
    {
        return $this->hasOne(Banco::class, 'codigo', 'cod_banco');
    }

}
