<?php

namespace App\Models\AdministracionIBP\User\Relaciones;

use App\Models\AdministracionIBP\User\user;
use App\Models\AdministracionIBP\Role\Role;
//use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;

class assigned_roles extends Model
{
     protected $table= 'assigned_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'role_id'];
    protected $connection = 'oraclepar';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $primarykey = 'id';
    public $timestamps = false;
    /**
     * @var array
     */

    public function users()
    {
        return $this->belongsToMany(User::class,'id', 'user_id' );
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class,'id', 'role_id' );
    }
}
