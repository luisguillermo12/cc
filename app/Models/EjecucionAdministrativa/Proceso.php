<?php

namespace App\Models\EjecucionAdministrativa;

use Illuminate\Database\Eloquent\Model;


class Proceso extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table= 't_procesos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['descripcion', 'estatus', 'fecha_inicio', 'fecha_fin'];

    public $timestamps = false;
}