<?php

namespace App\Models\EjecucionAdministrativa\MonitoreoSistema;

use Illuminate\Database\Eloquent\Model;

class ObjetosInvalidos extends Model
{
  protected $table = 'v_objetos_invalidos';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
  	//
  ];
}