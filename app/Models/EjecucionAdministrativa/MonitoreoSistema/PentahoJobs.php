<?php

namespace App\Models\EjecucionAdministrativa\MonitoreoSistema;

use Illuminate\Database\Eloquent\Model;

class PentahoJobs extends Model
{
  protected $table = 't_pentaho_jobs';
  protected $primarykey = 'id';
  public $timestamps = false;

  protected $fillable = [
    'id', 'codigo_job', 'nombre_job', 'ruta_job', 'descripcion_job'];
}