<?php

namespace App\Models\EjecucionAdministrativa\RecepcionArchivos;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Carbon\Carbon;

class RecepcionArchivos extends Model// implements AuditableContract
{
  // use Auditable;

  protected $table = 't_horarios_recepcion_archivos';
  protected $primarykey = 'id';
  protected $fillable = [
  'id', 'periodo_id', 'fecha_hora_inicio', 'fecha_hora_fin', 'critico_inicio_dt', 'critico_fin_dt', 'num_liquidacion', 'num_franja','estatus_corte'
   ];

    public $timestamps = false;

    public function getFechaHoraInicioAttribute($value)
    {
        return Carbon::createFromFormat('YmdHis', $value);
    }

    public function getFechaHoraFinAttribute($value)
    {
        return Carbon::createFromFormat('YmdHis', $value);
    }

    public function getCriticoInicioDtAttribute($value)
    {
        return Carbon::createFromFormat('YmdHis', $value);
    }

    public function getCriticoFinDtAttribute($value)
    {
        return Carbon::createFromFormat('YmdHis', $value);
    }
}
