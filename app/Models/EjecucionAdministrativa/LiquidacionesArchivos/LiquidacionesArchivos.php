<?php

namespace App\Models\EjecucionAdministrativa\LiquidacionesArchivos;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
//use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class LiquidacionesArchivos extends Model implements AuditableContract
{
  use Auditable;

  protected $table = 't_liquidaciones_archivos';
  protected $primarykey = 'id';
  protected $fillable = [
  'id', 'fecha_hora_ejecucion', 'critico_ejecucion', 'estatus', 'num_liquidaciones'
   ];

    public $timestamps = false;
}
