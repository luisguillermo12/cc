<?php

namespace App\Models\EjecucionAdministrativa\CortesArchivos;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CortesArchivos extends Model implements AuditableContract
{
    use Auditable;

    protected $table = 't_cortes_archivos';
    protected $primarykey = 'id';
    protected $fillable = [
        'id', 'fecha_hora_ejecucion', 'critico_ejecucion', 'estatus', 'num_corte'
    ];

    public $timestamps = false;
}
