<?php

namespace App\Models\EjecucionAdministrativa\ExtensionFranja;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ExtensionFranja extends Model// implements AuditableContract
{
    // use Auditable;

    protected $table = 't_extension_franja';

    protected $primarykey = 'id';

    protected $fillable = [
        'id', 'id_usuario', 'cod_banco', 'cod_tipo_operacion', 'tiempo_extension', 'fecha', 'descripcion', 'status'
    ];

    public $timestamps = false;
}
