<?php

namespace App\Http\Requests\Configuracion\ValidacionSistema;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class ValidacionSistemaUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'codigo_interno_error'=> 'required|max:3',
        'nivel'=> 'required|max:1',
        'respuesta_accion'=> 'required|max:99',
        'respuesta_codigo'=> 'required|max:3',
        'archivos_aplica'=> 'required|max:99',
      ];
    }

    public function messages()
    {
      return [
        'codigo_interno_error.required' => 'El código interno es requerido.',
        'codigo_interno_error.max' => 'El código interno no debe ser mayor a 3 caracteres',
        'nivel.required' => 'El nivel es requerido.',
        'nivel.max' => 'El nivel no debe ser mayor a 3 caracteres',
        'respuesta_accion.required' => 'El respuesta es requerido.',
        'respuesta_accion.max' => 'El respuesta no debe ser mayor a 3 caracteres',
        'respuesta_codigo.required' => 'El código de respuesta es requerido.',
        'respuesta_codigo.max' => 'El código de respuesta no debe ser mayor a 3 caracteres',
        'archivos_aplica.required' => 'El archivos que aplición es requerido.',
        'archivos_aplica.max' => 'El archivos que aplicación no debe ser mayor a 3 caracteres',
      ];
    }
}

