<?php

namespace App\Http\Requests\Configuracion\Participantes;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class ParticipanteUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function __construct(Route $route)
    {
        $this->route= $route;
    }


    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo' => 'required|unique:t_bancos,codigo,'.$this->get('id').'|digits:4',
            'nombre'=> 'required|unique:t_bancos,nombre,'.$this->get('id').'|max:100'
 
        ];
    }
    public function messages()
    {
        return [
          'codigo.required' => 'El Código es requerido',
          'codigo.unique' => 'El Código ya se encuentra registrado',
          'codigo.digits_between' => 'El Código desde estar comprendido entre 4 y 6 dígitos',
         
          'nombre.required' => 'El Nombre es requerido',
          'nombre.unique' => 'El Nombre ya se encuentra registrado',
          'nombre.max' => 'El Nombre no debe ser mayor a 100 caracteres'
       ,
        ];
    }
}
