<?php

namespace App\Http\Requests\Configuracion\participantes;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class ParticipanteCreateRequest extends Request
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'codigo' => 'required|unique:t_bancos,codigo|digits:4',
            'nombre'=> 'required|unique:t_bancos,nombre|max:100',
         
        ];
    }
    public function messages()
    {
        return [
            'codigo.required' => 'El Código es requerido',
            'codigo.unique' => 'El Código ya se encuentra registrado',
            'codigo.digits' => 'El Código desde estar comprendido entre 4 dígitos',
			
            'nombre.required' => 'El Nombre es requerido',
            'nombre.unique' => 'El Nombre ya se encuentra registrado',
            'nombre.max' => 'El Nombre no debe ser mayor a 100 caracteres',
     
        ];
    }
}
