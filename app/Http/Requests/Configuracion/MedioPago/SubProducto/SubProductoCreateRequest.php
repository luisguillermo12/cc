<?php

namespace App\Http\Requests\Configuracion\MedioPago\SubProducto;

use App\Http\Requests\Request;

class SubProductoCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'producto_id' => 'required|not_in:0',
        'codigo' => 'required|digits:3|max:3|unique:t_sub_productos',
        'nombre'=> 'required|max:50|unique:t_sub_productos',
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
       return [
         'producto_id.not_in' => 'El producto es requerido.',
         'codigo.required' => 'El código es requerido.',
		  //'codigo.numeric'  =>  'el codigo debe ser numerico',
         'codigo.digits' => 'El código debe ser de 3 dígitos.',
         'codigo.unique' => 'El código ya se encuentra registrado',
         'codigo.max' => 'El código no debe ser mayor a 3 caracteres',
         'nombre.required' => 'El nombre es requerido.',
         'nombre.max' => 'El nombre no debe ser mayor a 50 caracteres',
         'nombre.unique' => 'El nombre ya se encuentra registrado.',
         'estatus.required' => 'El estado es requerido.',
         'estatus.max' => 'El estado no debe ser mayor a 15 caracteres',
       ];
    }
}
