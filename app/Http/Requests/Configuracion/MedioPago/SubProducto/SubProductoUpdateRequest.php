<?php

namespace App\Http\Requests\Configuracion\MedioPago\SubProducto;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class SubProductoUpdateRequest extends Request
{
    /**
    * Constructor
    */
    public function __construct(Route $route)
    {
        $this->route= $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'producto_id' => 'required|not_in:0',
        'nombre'=> 'required|max:50|unique:t_sub_productos,nombre,'.$this->route->parameter('SubProductos'),
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
       return [
           'producto_id.not_in' => 'El producto es requerido',
           'nombre.required' => 'El nombre es requerido.',
           'nombre.max' => 'El nombre no debe ser mayor a 50 caracteres',
           'nombre.unique' => 'El nombre ya se encuentra registrado.',
           'estatus.required' => 'El estado es requerido.',
           'estatus.max' => 'El estado no debe ser mayor a 15 caracteres',
       ];
    }
}
