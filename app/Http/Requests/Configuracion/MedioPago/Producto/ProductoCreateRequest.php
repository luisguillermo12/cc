<?php

namespace App\Http\Requests\Configuracion\MedioPago\Producto;

use App\Http\Requests\Request;

class ProductoCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'codigo_presentada'=> 'required|max:3|unique:t_tipos_operaciones,codigo|different:codigo_devuelta',
        'codigo_devuelta' => 'required|max:3|digits:3|unique:t_tipos_operaciones,codigo|different:codigo_presentada',
        'nombre'=> 'required|max:50|unique:t_productos,nombre',
        'estatus'=> 'required|max:15',
        'signo'=> 'required|max:1|not_in:0',
        'direccion'=> 'not_in:0',
        'limite_financiero'=> 'required|digits_between:0,16|numeric',
      ];
    }

    public function messages()
    {
      return [
        //CODIGO PRESENTADA
        'codigo_presentada.required' => 'El código de presentada es requerido.',
        'codigo_presentada.max' => 'El Código de presentada no debe ser mayor a 3 digitos',
        'codigo_presentada.digits' => 'El código de presentada debe ser de 3 dígitos.',
        'codigo_presentada.unique' => 'El código de presentada ya se encuentra registrado.',
        'codigo_presentada.different' => 'El código de presentada debe ser diferente al código de devuelta.',

        //CODIGO DEVUELTA
        'codigo_devuelta.required' => 'El código devuelta es requerido.',
        'codigo_devuelta.max' => 'El código devuelta no debe ser mayor a 3 digitos',
        'codigo_devuelta.digits' => 'El código devuelta debe ser de 3 dígitos.',
        'codigo_devuelta.unique' => 'El código devuelta ya se encuentra registrado',
        'codigo_presentada.different' => 'El código de devuelta debe ser diferente al código de presentada.',

        //LOS OTROS CAMPOS
        'nombre.required' => 'El nombre de la operación es requerido.',
        'nombre.max' => 'El nombre de la operación no debe ser mayor a 50 caracteres',
        'nombre.unique' => 'El nombre del producto ya se encuentra registrado',
        'estatus.required' => 'El estado es requerido.',
        'estatus.max' => 'El estado no debe ser mayor a 15 caracteres',
        'direccion.not_in' => 'La dirección es requerida.',
        'signo.not_in' => 'El signo es requerido.',
        'limite_financiero.required' => 'El límite financiero es requerido.',
        'limite_financiero.digits' => 'La cantidad de digitos no debe ser mayor a 16 digitos.',
        'limite_financiero.numeric' => 'El límite financiero debe ser un número.',
     ];
    }
}
