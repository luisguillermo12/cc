<?php

namespace App\Http\Requests\Configuracion\MedioPago\Producto;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class ProductoUpdateRequest extends Request
{
    /**
    * Constructor
    */
    public function __construct(Route $route)
    {
        $this->route= $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $producto = $this->route('productos');
      $id = (int) $producto;
      return [
          /*'codigo_producto' => 'required|max:3|digits:3|unique:t_productos,codigo,'.$this->route->getparameter('productos'),
          'nombre_producto'=> 'required|max:50|unique:t_productos,nombre,'.$this->route->getparameter('productos'),
          'estatus'=> 'required|max:15',*/
          'producto_id'=> 'not_in:0',
          //'codigo'=> 'required|max:3|digits:3|unique:t_tipos_operaciones,codigo,'.$this->route->getparameter('productos'),
          /*'codigo_producto'=> 'required|max:3|digits:3,'.$this->route->getparameter('productos'),
          'signo'=> 'required|max:1|not_in:0',*/
          /*'nombre'=> 'required|max:50|unique:t_tipos_operaciones,nombre,'.$id.',id,producto_id,'.$this->producto_id.'',*/
          'nombre'=> 'required|max:50|unique:t_productos,nombre,'.$this->id.'',
          'estatus'=> 'required|max:15',
          'direccion'=> 'not_in:0',
          'limite_financiero'=> 'required|digits_between:0,16|numeric',
      ];
    }

    public function messages()
    {
      return [
        /*'codigo.required' => 'El Código es requerido.',
        'codigo.max' => 'El Código no debe ser mayor a 3 caracteres.',
        'codigo.digits' => 'El Código debe ser de 3 dígitos.',
        'codigo.unique' => 'El Código ya se encuentra registrado.',*/
        'nombre.required' => 'El nombre es requerido.',
        'nombre.max' => 'El nombre no debe ser mayor a 50 caracteres.',
        'nombre.unique' => 'El nombre ya se encuentra registrado.',
        'estatus.required' => 'El estado es requerido.',
        'estatus.max' => 'El estado no debe ser mayor a 15 caracteres.',
        'limite_financiero.required' => 'El límite financiero es requerido.',
        'limite_financiero.digits' => 'La cantidad de digitos no debe ser mayor a 16 digitos.',
        'limite_financiero.numeric' => 'El límite financiero debe ser un número.',
     ];
    }
}
