<?php

namespace App\Http\Requests\Configuracion\MedioPago\TipoDevolucion;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class TipoDevolucionUpdateRequest extends Request
{
    /**
    * Constructor
    */
    public function __construct(Route $route)
    {
        $this->route= $route;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {      return [
        'codigo'=> 'required|max:2|digits:2|unique:t_tipos_devoluciones,codigo,'.$this->route->parameter('razones_devolucion'),
        'nombre'=> 'required|max:50|unique:t_tipos_devoluciones,nombre,'.$this->route->parameter('razones_devolucion'),
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [
         'codigo.required' => 'El Código es requerido.',
         'codigo.max' => 'El Código no debe ser mayor a 2 caracteres.',
         'codigo.digits' => 'El Código debe ser de 2 dígitos.',
         'codigo.unique' => 'El Código ya se encuentra registrado.',
         'nombre.required' => 'La Razón es requerida.',
         'nombre.max' => 'La Razón no debe ser mayor a 50 caracteres.',
         'nombre.unique' => 'La Razón ya se encuentra registrada para el tipo de operación seleccionada.',
         'estatus.required' => 'El Estado es requerido.',
         'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres.',
      ];
    }
}
