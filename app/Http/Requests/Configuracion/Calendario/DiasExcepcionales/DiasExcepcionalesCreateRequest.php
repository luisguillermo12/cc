<?php

namespace App\Http\Requests\Configuracion\Calendario\DiasExcepcionales;

use App\Http\Requests\Request;

class DiasExcepcionalesCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'fecha'=> 'required|unique:t_dias_excepcionales,fecha',
        'observaciones'=> 'required|max:250',
        //'day'=> 'required|after:now|unique:t_calendario,fecha',
      ];
    }

    public function messages()
    {
      return [
        'fecha.required'=> 'La Fecha es requerida.',
        //'fecha.after'=> 'La Fecha debe ser después del día de hoy.',
        'fecha.unique'=> 'La Fecha ya se encuentra registrada.',

        'day.unique'=> 'La Fecha ya se encuentra registrada.',
        'observaciones.required'=> 'La Observación es requerida.',
        'observaciones.max' => 'La Observación no debe ser mayor a 250 caracteres',
      ];
    }
}
