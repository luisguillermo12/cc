<?php

namespace App\Http\Requests\Configuracion\Calendario;

use App\Http\Requests\Request;

class CalendarioCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'fecha'=> 'required|unique:t_calendario,fecha',
        'estatus'=> 'required',
        //'day'=> 'required|after:now|unique:t_calendario,fecha',
      ];
    }

    public function messages()
    {
      return [
        'fecha.required'=> 'La Fecha es requerida.',
        //'fecha.after'=> 'La Fecha debe ser después del día de hoy.',
        'fecha.unique'=> 'La Fecha ya se encuentra registrada.',
        'estatus.required'=> 'El estado es requerido.',
        //'day.after'=> 'La Fecha debe ser después del día de hoy.',
        'day.unique'=> 'La Fecha ya se encuentra registrada.',
      ];
    }
}
