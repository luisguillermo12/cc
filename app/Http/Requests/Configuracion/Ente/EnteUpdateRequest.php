<?php

namespace App\Http\Requests\Configuracion\Ente;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class EnteUpdateRequest extends Request
{
	/**
    * Constructor
    */
    public function __construct(Route $route)
    {
        $this->route= $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
				'rif' => 'required|max:10|min:10|unique:t_entes,rif,'.$this->route->getparameter('entes'),
        'nombre'=> 'required|max:150|unique:t_entes,nombre,'.$this->route->getparameter('entes'),
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [
		'rif.required' => 'El RIF es requerido.',
        'rif.max' => 'El RIF no debe ser mayor a 10 caracteres.',
        'rif.unique' => 'El RIF ya se encuentra registrado.',
        'rif.min' => 'El RIF debe tener 10 caracteres.',
        'nombre.required' => 'El Nombre es requerido.',
        'nombre.max' => 'El Nombre no debe ser mayor a 150 caracteres',
        'nombre.unique' => 'El Nombre ya se encuentra registrado.',
        'estatus.required' => 'El Estado es requerido.',
        'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres',
     ];
    }
}
