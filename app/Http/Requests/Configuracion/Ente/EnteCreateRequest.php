<?php

namespace App\Http\Requests\Configuracion\Ente;

use App\Http\Requests\Request;

class EnteCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
		'rif' => 'required|max:9|min:9|unique:t_empresas_ordenantes,rif',
        'nombre'=> 'required|max:150|unique:t_empresas_ordenantes,nombre',
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [
		'rif.required' => 'El RIF es requerido.',
        'rif.max' => 'El RIF no debe ser mayor a 9 caracteres.',
        'rif.unique' => 'El RIF ya se encuentra registrado.',
        'rif.min' => 'El RIF debe tener 9 caracteres.',
        'nombre.required' => 'El Nombre es requerido.',
        'nombre.max' => 'El Nombre no debe ser mayor a 150 caracteres',
        'nombre.unique' => 'El Nombre ya se encuentra registrado',
        'estatus.required' => 'El Estado es requerido.',
        'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres',
     ];
    }
}
