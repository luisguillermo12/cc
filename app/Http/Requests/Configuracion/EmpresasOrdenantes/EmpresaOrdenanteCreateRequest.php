<?php

namespace App\Http\Requests\Configuracion\EmpresasOrdenantes;

use App\Http\Requests\Request;

class EmpresaOrdenanteCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


      return [

        'rifd' => 'unique:t_empresas_ordenantes,rif',
        'rif' => 'required|max:9|min:9|',
        'nombre'=> 'required|max:50|unique:t_empresas_ordenantes,nombre',
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [

        'rifd.unique' => 'El RIF ya se encuentra registrado.',
        'rif.required' => 'El RIF es requerido.',
        'rif.max' => 'El RIF no debe ser mayor a 9 caracteres.',
        'rif.min' => 'El RIF debe tener 9 caracteres.',
        'nombre.required' => 'El Nombre es requerido.',
        'nombre.unique' => 'El nombre ya se encuentra registrado.',
        'nombre.max' => 'El Nombre no debe ser mayor a 50 caracteres.',
        'nombre.unique' => 'El Nombre de la empresa ya se encuentra registrado.',
        'estatus.required' => 'El Estado es requerido.',
        'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres.',
     ];
    }
}
