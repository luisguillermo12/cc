<?php

namespace App\Http\Requests\Configuracion\EmpresasOrdenantes;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class EmpresaOrdenanteUpdateRequest extends Request
{
    /**
    * Constructor
    */
    public function __construct(Route $route)
    {
        $this->route= $route;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'rif' => 'required|max:10|min:10|unique:t_empresas_ordenantes,rif,'.$this->route->Parameter('empresas_ordenantes'),
        'nombre'=> 'required|max:50',
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [
        'rif.required' => 'El RIF es requerido.',
        'rif.max' => 'El RIF no debe ser mayor a 10 caracteres.',
        'rif.unique' => 'El RIF ya se encuentra registrado.',
        'rif.min' => 'El RIF debe tener 10 caracteres.',
        'nombre.required' => 'El Nombre es requerido.',
        'nombre.max' => 'El Nombre no debe ser mayor a 50 caracteres.',
        'estatus.required' => 'El Estado es requerido.',
        'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres.',
     ];
    }
}
