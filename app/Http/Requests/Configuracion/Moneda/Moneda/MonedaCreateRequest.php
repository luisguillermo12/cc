<?php

namespace App\Http\Requests\Configuracion\Moneda\Moneda;

use App\Http\Requests\Request;

class MonedaCreateRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'codigo_iso'=> 'required|unique:t_monedas,codigo_iso',
      'estatus'=> 'required',
      /*'nombre'=> 'required|max:50|unique:t_monedas,nombre',
      'num_decimales'=> 'required|max:1',
      'estatus'=> 'required|max:15',*/
      //'limite_financiero'=> 'required|max:16',
      //'monto_max_bv'=> 'required|max:16',
    ];
  }

  public function messages()
  {
    return [
      'codigo_iso.required'=> 'El Código ISO es requerido.',
      'estatus.required' => 'El Estado es requerido.',
      /*'codigo_iso.max' => 'El Código ISO no debe ser mayor a 3 caracteres.',
      'codigo_iso.unique' => 'El Código ISO ya se encuentra registrado.',
      'nombre.required'=> 'El Nombre es requerido.',
      'nombre.max' => 'El Nombre no debe ser mayor a 50 caracteres.',
      'nombre.unique' => 'El Nombre ya se encuentra registrado.',
      'num_decimales.required' => 'La Cantidad De Decimales es requerido.',
      'num_decimales.max' => 'La Cantidad De Decimales no debe ser mayor a 1 digito.',
      'estatus.required' => 'El Estado es requerido.',
      'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres',
      //'limite_financiero.required' => 'El Límite Financiero es requerido.',
      //'limite_financiero.max' => 'La Cantidad De Decimales no debe ser mayor a 16 digitos.',
      /*'monto_max_bv.required' => 'El Monto De Máximo Bajo Valor es requerido.',
      'monto_max_bv.max' => 'El Monto De Máximo Bajo Valor no debe ser mayor a 16 digitos.',*/
    ];
  }
}
