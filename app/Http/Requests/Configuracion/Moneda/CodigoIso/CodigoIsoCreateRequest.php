<?php

namespace App\Http\Requests\Configuracion\Moneda\CodigoIso;

use App\Http\Requests\Request;

class CodigoIsoCreateRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'codigo_iso'=> 'required|max:3|unique:t_codigo_iso,codigo_iso',
      'nombre'=> 'required|max:50|unique:t_codigo_iso,nombre',
      'num_decimales'=> 'required|max:1',
      'separador_miles'=> 'required|max:1',
      'separador_decimales'=> 'required|max:1',
      'estatus'=> 'required|max:15',
    ];
  }

  public function messages()
  {
    return [
      'codigo_iso.required'=> 'El Código ISO es requerido.',
      'codigo_iso.max' => 'El Código ISO no debe ser mayor a 3 caracteres.',
      'codigo_iso.unique' => 'El Código ISO ya se encuentra registrado.',
      'nombre.required'=> 'El Nombre es requerido.',
      'nombre.max' => 'El Nombre no debe ser mayor a 50 caracteres.',
      'nombre.unique' => 'El Nombre ya se encuentra registrado.',
      'num_decimales.required' => 'La Cantidad De Decimales es requerido.',
      'num_decimales.max' => 'La Cantidad De Decimales no debe ser mayor a 1 digito.',
      'separador_decimales.required' => 'La Separación De Decimales es requerido.',
      'separador_decimales.max' => 'La Separación De Decimales no debe ser mayor a 1 digito.',
      'separador_miles.required' => 'La Separación De Miles es requerido.',
      'separador_miles.max' => 'La Separación De Miles no debe ser mayor a 1 digito.',
      'estatus.required' => 'El Estado es requerido.',
      'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres',
    ];
  }
}
