<?php

namespace App\Http\Requests\Configuracion\Moneda\MonedaOperacion;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class MonedaOperacionCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'moneda_id'=> 'required|not_in:0|unique:t_monedas_operaciones,moneda_id,NULL,id,tipo_operacion_id,'.$this->tipo_operacion_id.'',
        'tipo_operacion_id'=> 'required|not_in:0',
        'limite_financiero'=> 'required|max:16',
      ];
    }

    public function messages()
    {
       return [
         'moneda_id.not_in' => 'La Moneda es requerida.',
         'moneda_id.required' => 'La Moneda es requerida.',
         'moneda_id.unique' => 'La Moneda y el Tipo de Operación ya se encuentran registrada.',
         'tipo_operacion_id.not_in' => 'El Tipo de Operación es requerido.',
         'tipo_operacion_id.required' => 'El Tipo de Operación es requerido.',
         'limite_financiero.required' => 'El Límite Financiero es requerido.',
         'limite_financiero.max' => 'El Límite Financiero no debe ser mayor a 16 dígitos.',
       ];
    }
}
