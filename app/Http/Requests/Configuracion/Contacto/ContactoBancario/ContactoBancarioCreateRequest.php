<?php

namespace App\Http\Requests\Configuracion\Contacto\ContactoBancario;

use App\Http\Requests\Request;

class ContactoBancarioCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'banco_id' => 'required|not_in:0',
        'cedula'=> 'required|numeric|unique:t_contactos,cedula',
        'nombre'=> 'required|max:50',
        'cargo'=> 'required|max:50',
        'telefono_celular'=> 'required|max:16',
        'telefono_local'=> 'required|max:16',
        'email'=> 'required|max:100|email|unique:t_contactos,email',
        'tipo_contacto_id' => 'required|not_in:0',
        'turno_id' => 'required|not_in:0',
        'posicion_escalamiento_id' => 'required|not_in:0',
        'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [
        'banco_id.not_in' => 'El Banco es requerido.',
        'cedula.required' => 'La Cédula es requerida.',
        'cedula.numeric' => 'La Cédula debe ser un número.',
        'nombre.required' => 'El Nombre y Apellido es requerido.',
        'nombre.max' => 'El Nombre Y Apellido no debe ser mayor a 50 caracteres',
        'cargo.required' => 'El Cargo es requerido.',
        'cargo.max' => 'El Cargo no debe ser mayor a 50 caracteres',
        'telefono_celular.required' => 'El Teléfono es requerido.',
        'telefono_celular.max' => 'El Teléfono no debe ser mayor a 16 caracteres',
        'telefono_local.required' => 'El Teléfono es requerido.',
        'telefono_local.max' => 'El Teléfono no debe ser mayor a 16 caracteres',
        'email.required' => 'El Correo Electrónico es requerido.',
        'email.max' => 'El Correo Electrónico no debe ser mayor a 100 caracteres',
        'email.email' => 'El Correo Electrónico debe ser una dirección válida.',
        'email.unique' => 'El Correo Electrónico ya se encuentra registrado.',
        'tipo_contacto_id.not_in' => 'El Tipo De Contacto es requerido.',
        'turno_id.not_in' => 'El Turno es requerido.',
        'posicion_escalamiento_id.not_in' => 'La Posición de Escalamiento es requerido.',
        'estatus.required' => 'El Estado es requerido.',
        'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres',
     ];
    }
}
