<?php

namespace App\Http\Requests\Configuracion\Contacto\TipoContacto;

use App\Http\Requests\Request;

class TipoContactoCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'nombre'=> 'required|max:50',
        //'estatus'=> 'required|max:15',
      ];
    }

    public function messages()
    {
      return [
        'nombre.required' => 'El Nombre es requerido.',
        'nombre.max' => 'El Nombre no debe ser mayor a 50 caracteres',
        /*'estatus.required' => 'El Estado es requerido.',
        'estatus.max' => 'El Estado no debe ser mayor a 15 caracteres',*/
     ];
    }
}
