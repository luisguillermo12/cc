<?php

namespace App\Http\Requests\Configuracion\ParametroSistema;

use App\Http\Requests\Request;

class ParametroSistemaUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'id'=>'required',
        'nombre'=> 'required|max:99',
        'valor'=> 'required|max:199'
      ];
    }

    public function messages()
    {
      return [
        'nombre.required'=> 'El Nombre es requerido.',
        'nombre.max' => 'El Nombre no debe ser mayor a 99 digitos.',
        'valor.required'=> 'El Valor es requerido.',
        'valor.max' => 'El Valor no debe ser mayor a 199 digitos.',
      ];
    }

}
