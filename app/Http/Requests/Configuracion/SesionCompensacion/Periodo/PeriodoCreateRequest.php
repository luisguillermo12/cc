<?php

namespace App\Http\Requests\Configuracion\SesionCompensacion\Periodo;

use App\Http\Requests\Request;

class PeriodoCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        /*'producto_id'=> 'required|not_in:0',
        'dia_inicio'=> 'required',
        'hora_inicio'=> 'required',
        'dia_fin'=> 'required',
        'hora_fin'=> 'required',
        'num_liquidacion'=> 'required',
        'num_franja'=> 'required',*/
      ];
    }

    public function messages()
    {
       return [
         /*'producto_id.not_in' => 'El Producto es requerido.',
         'dia_inicio.required' => 'El Día de Inicio es requerido.',
         'hora_inicio.required' => 'La Hora de Inicio es requerida.',
         'dia_fin.required' => 'El Día de Fin es requerido.',
         'hora_fin.required' => 'La Hora de Fin es requerida.',
         'num_liquidacion.required' => 'El Número de Intervalos de Liquidación es requerido.',
         'num_franja.required' => 'El Número de Franja es requerido.',*/
       ];
    }
}
