<?php

namespace App\Http\Requests\Configuracion\SesionCompensacion\IntervaloLiquidacion;

use App\Http\Requests\Request;

class IntervaloLiquidacionUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'dia_inicio.*'=> 'required',
          'hora_inicio.*'=> 'required',
          'dia_fin.*'=> 'required',
          'hora_fin.*'=> 'required',
        ];
    }

    public function messages()
    {
      $messages = [];

      foreach($this->request->get('dia_inicio') as $key => $val)
      {
        $messages['dia_inicio.'.$key.'.required'] = 'El Dia de Inicio de la Fila '.$key.' es requerido.';
      }

      foreach($this->request->get('hora_inicio') as $key => $val)
      {
        $messages['hora_inicio.'.$key.'.required'] = 'La Hora de Inicio de la Fila '.$key.' es requerida.';
      }

      foreach($this->request->get('dia_fin') as $key => $val)
      {
        $messages['dia_fin.'.$key.'.required'] = 'El Dia de Inicio de la Fila '.$key.' es requerido.';
      }

      foreach($this->request->get('hora_fin') as $key => $val)
      {
        $messages['hora_fin.'.$key.'.required'] = 'La Hora de Fin de la Fila '.$key.' es requerida.';
      }

      return $messages;
    }
}
