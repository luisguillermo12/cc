<?php

namespace App\Http\Requests\Seguridad;

use App\Http\Requests\Request;

class StoreUserRequest extends Request
{

    public function authorize()
    {
     return true;
    }



    public function rules()
    {
        return [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users',
            'password'              => 'required',
            'password_confirmation' => 'required',
            'assignees_roles'       => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido.',
            'email.required' => 'El usuario es requerido.',
            'email.unique' => 'El usuario ya se encuentra registrado.',
            'password.required' => 'La contraseña es requerida.',
            'password_confirmation.required' => 'La contraseña de confirmación es requerida.',
            'assignees_roles.required' => 'El usuario debe tener al menos un rol asociado.',

        ];
    }
}
