<?php
namespace App\Http\Requests\Seguridad;

use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{

    public function authorize()
    {
     return true;
    }

    public function rules()
    {
        return [
            'password'              => 'required',
            'password_confirmation' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'La Contraseña es requerida.',
            'password_confirmation.required' => 'La Contraseña de Confirmación es requerida.'
        ];
    }
}

