<?php

namespace App\Http\Controllers\Configuracion\Empresas\Registro;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use App\Models\Configuracion\Moneda\Moneda;
//use App\Models\MensajesInformativos\MensajesInformativos;
use App\Models\Configuracion\EmpresaOrdenante\EmpresaOrdenante;
use App\Http\Requests\Configuracion\EmpresaOrdenante\EmpresaOrdenanteCreateRequest;
use Illuminate\Support\Facades\Log;


class RegistroEmpresasController extends Controller
{


    public function getIndex(Request $request)
    {
        $bancos = Banco::buscarTodos();
        return view('configuracion.empresas.registro.em01_i_registroempresas')
        ->with('bancos', $bancos);

    }
     public function store(EmpresaOrdenanteCreateRequest $request)
  {
   //return $request->all();
    // EmpresaOrdenante
    $empresaOrdenante = new EmpresaOrdenante();
    $empresaOrdenante->rif = $request->input('rifAux').$request->input('rif');
    $empresaOrdenante->nombre = $request->input('nombre');
    $empresaOrdenante->estatus = $request->input('estatus');
    $empresaOrdenante->cod_banco_asociado = $request->input('cod_banco');
    $result = $empresaOrdenante->save();

    notify()->flash('Empresa Ordenante Creada', 'success', [
      'timer' => 3000,
      'text' => ''
    ]);

     return redirect('Configuracion/EmpresasOrdenantes');

  }


public function cargamasiva(Request $request)
  {

    $file = $request->file('file');
    try
    {
      $empresaOrdenante = new EmpresaOrdenante();
      foreach(file($file) as $line)
      {
        $porciones = explode("|", $line);
        $rif = $porciones[0]; // rif
        $descripcion = $porciones[1]; // descripcion
        $estatus = $porciones[2]; // estatus
        $cod_banco_asociado=$porciones[3]; //banco asociado
        //Consultar RIF
        $consul = $empresaOrdenante::where('rif',$rif)->first();
        $consul = $empresaOrdenante::where('nombre',$descripcion)->first();
        if($consul->rif == "" && $consul->nombre == ""){
            $empresaOrdenante->rif = $porciones[0];
            $empresaOrdenante->nombre = $porciones[1];
            $empresaOrdenante->estatus = $porciones[2];
            $empresaOrdenante->cod_banco_asociado = $porciones[3];
            $result = $empresaOrdenante->save();
        }else{
          $this->write_log("\n REQUEST_EXPLORADOR:".$_SERVER['HTTP_USER_AGENT']." ".
          " \r\n REQUEST_URI: ".
          $_SERVER['REQUEST_URI']," INFO ")."\r\n";
          $this->write_log("RIF YA REGISTRADO ".$rif." ".$descripcion," INFO ");
        }

      }
    }catch (Illuminate\Database\QueryException $e){
      $errorCode = $e->errorInfo[1];
      if($errorCode == 1){
        Log::error('RIF YA SE ENCUENTRA REGISTRADO');
      }
    }

    notify()->flash('se proceso la carga masiva de Empresas Ordenantes', 'success', [
      'timer' => 3000,
      'text' => ''
    ]);
    return redirect('EmpresasOrdenantes/Registro');
    }

public function write_log($cadena,$tipo)
    {
        $app_path = storage_path('logs');
        $arch = fopen($app_path."/log_empresa_ord".date("Y-m-d").".txt", "a+");
        fwrite($arch, "[".date("Y-m-d H:i:s.u")." ".$_SERVER['REMOTE_ADDR']." ".$tipo." ] ".$cadena."\n");
        fwrite($arch, "\r\n");
        fclose($arch);
    }

}
