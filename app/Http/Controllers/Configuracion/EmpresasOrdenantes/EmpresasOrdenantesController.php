<?php

namespace App\Http\Controllers\Configuracion\EmpresasOrdenantes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use Session;
//use DB;
//use Illuminate\Database\Connection;
//use Yajra\Datatables\Facades\Datatables;
//use Validator;
//use App\Models\Configuracion\Moneda\Moneda;
//use App\Models\MensajesInformativos\MensajesInformativos;
use App\Models\Configuracion\EmpresasOrdenantes\EmpresaOrdenante;
use App\Http\Requests\Configuracion\EmpresasOrdenantes\EmpresaOrdenanteCreateRequest;
use App\Http\Requests\Configuracion\EmpresasOrdenantes\EmpresaOrdenanteUpdateRequest;

class EmpresasOrdenantesController extends Controller
{
    public function index(Request $request){
        if($request->get('rif') != null || $request->get('nombre') != null){
                    $rifselec = $request->get('rif');
                    $empresaselec = $request->get('nombre');
                    $empresasOrdenantes = EmpresaOrdenante::select('t_empresas_ordenantes.*')
                    ->where('rif', 'like', '%'.$rifselec.'%')
                    ->where('nombre', 'like', '%'.$empresaselec.'%')
                    ->orWhere('nombre',$empresaselec)
                    ->paginate(15);
        }else{
                    $empresasOrdenantes = EmpresaOrdenante::paginate(15);
        }
        return view('configuracion.empresas_ordenantes.co_eo_01_i_empresas_ordenantes')
        ->with('empresasOrdenantes', $empresasOrdenantes);
    }
    public function create(Request $request){
        $bancos = Banco::buscarTodos();
        return view('configuracion.empresas_ordenantes.co_eo_02_c_empresas_ordenantes')
                    ->with('bancos', $bancos);
    }
    public function store(EmpresaOrdenanteCreateRequest $request){
        $empresaOrdenante = new EmpresaOrdenante();
        $empresaOrdenante->rif = $request->input('rifAux').$request->input('rif');
        $empresaOrdenante->nombre = $request->input('nombre');
        $empresaOrdenante->estatus = $request->input('estatus');
        //$empresaOrdenante->cod_banco_asociado = $request->input('cod_banco');
        $result = $empresaOrdenante->save();
                alert()->success('Empresa ordenante creada satisfactoriamente');
                return redirect('Configuracion/EmpresasOrdenantes');
    }
    public function edit($id){
        $empresa_ordenante = EmpresaOrdenante::find($id);
        return view('configuracion.empresas_ordenantes.co_eo_03_e_empresas_ordenantes')
        ->with('empresa_ordenante', $empresa_ordenante);
  }
    public function update(EmpresaOrdenanteUpdateRequest $request, $id){
        $empresa_ordenante = EmpresaOrdenante::FindOrFail($id);
        $empresa_ordenante->rif = $request->input('rifAux').$request->input('rif');
        $empresa_ordenante->nombre = $request->input('nombre');
        $empresa_ordenante->estatus = $request->input('estatus');
        $empresa_ordenante->save();

        alert()->success('Empresa ordenante Actualizada satisfactoriamente');
        return redirect('Configuracion/EmpresasOrdenantes');
    }
    public function destroy($id){
        $empresa_ordenante = EmpresaOrdenante::FindOrFail($id);
        $result = $empresa_ordenante->delete();
            if ($result){
                return response()->json(['success'=>'true']);
            }else{
                    return response()->json(['success'=> 'false']);
            }
    }
}
