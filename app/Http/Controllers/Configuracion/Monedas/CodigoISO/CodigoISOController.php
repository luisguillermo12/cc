<?php

namespace App\Http\Controllers\Configuracion\Monedas\CodigoISO;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\CodigoIso;
use App\Models\Configuracion\Moneda\MonedaOperacion;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Http\Requests\Configuracion\Moneda\CodigoIso\CodigoIsoCreateRequest;
use App\Http\Requests\Configuracion\Moneda\CodigoIso\CodigoIsoUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;

class CodigoISOController extends Controller
{

  public function index()
  {
    $tipos_operaciones = TipoOperacion::pluck('nombre','id')
    ->prepend('Seleccione Tipo Operación', 0);

    $codigo_iso = CodigoIso::orderBy('nombre', 'asc')
    ->get();

        return View('configuracion.monedas.codigo_iso.co_ci_01_i_inicio')
        ->with('tipos_operaciones', $tipos_operaciones)
        ->with('codigo_iso', $codigo_iso);
  }

  public function create()
  {
    return View('configuracion.monedas.codigo_iso.co_ci_02_c_crear');
  }

  public function store(CodigoIsoCreateRequest $request)
  {

    $codigo_iso = new CodigoIso();
    $codigo_iso->codigo_iso = $request->input('codigo_iso');
    $codigo_iso->nombre = $request->input('nombre');
    $codigo_iso->num_decimales = $request->input('num_decimales');
    $codigo_iso->simbolo = $request->input('simbolo');
    $codigo_iso->separador_miles = $request->input('separador_miles');
    $codigo_iso->separador_decimales = $request->input('separador_decimales');
    $codigo_iso->estatus = $request->input('estatus');
    $result = $codigo_iso->save();

    notify()->flash('Codigo ISO creada', 'success', [
      'timer' => 3000,
      'text' => ''
    ]);

    return redirect()->route('CodigoISO.index');
  }

  public function show($id)
  {
    $monedaMostrar = CodigoIso::FindOrFail($id);

    return view('configuracion.monedas.codigo_iso.co_ci_04_d_detalle')
    ->with('monedaMostrar',$monedaMostrar);
  }

  public function edit($id)
  {
    $codigoiso = CodigoIso::find($id);
    return View('configuracion.monedas.codigo_iso.co_ci_03_e_editar')
    ->with('codigoiso', $codigoiso);
  }

  public function update(CodigoIsoUpdateRequest $request, $id)
  {
    $rules = [];
    $messages = [];
    $validator = Validator::make($request->all(), $rules, $messages);
    
    /*if($request->input('monto_max_bv') > $request->input('limite_financiero')) {
      $validator->getMessageBag()->add('limiteFinanciero', 'El Monto Máximo de Bajo Valor no debería ser superior al Límite Financiero.');
      return back()
      ->withInput()
      ->withErrors($validator);
    }*/

    $codigoiso = CodigoIso::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
    $result = $codigoiso->fill($input)->save(); /*Se guarda la información*/

    notify()->flash('Codigo ISO actualizado', 'success', [
      'timer' => 3000,
      'text' => ''
    ]);

    return redirect()->route('CodigoISO.index');
  }

  public function destroy($id)
  { 
    $moneda = CodigoIso::FindOrFail($id);
    $result = $moneda->delete();

    if ($result)
    {
      return response()->json(['success'=>'true']);
    }
    else
    {
      return response()->json(['success'=> 'false']);
    }
  }
}
