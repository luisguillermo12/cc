<?php

namespace App\Http\Controllers\Configuracion\Monedas\Tipos;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\CodigoIso;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\Moneda\MonedaOperacion;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Http\Requests\Configuracion\Moneda\Moneda\MonedaCreateRequest;
use App\Http\Requests\Configuracion\Moneda\Moneda\MonedaUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;

class TiposController extends Controller
{

  public function index()
  {
    $tipos_operaciones = TipoOperacion::pluck('nombre','id')
    ->prepend('Seleccione Tipo Operación', 0);

    $codigo_iso = CodigoIso::select('t_codigo_iso.*')
    ->where('estatus','ACTIVO')
    ->orderBy('codigo_iso', 'asc')
    ->get();

    $monedas = Moneda::orderBy('nombre', 'asc')
    ->where('estatus','<>','RETIRADO')
    ->get();
        return View('configuracion.monedas.tipos.co_ti_01_i_inicio')
        ->with('tipos_operaciones', $tipos_operaciones)
        ->with('codigo_iso', $codigo_iso)
        ->with('monedas', $monedas);
  }

  public function create()
  {
    $codigo_iso = CodigoIso::select('codigo_iso', 'nombre')
    ->where('estatus','ACTIVO')
    ->orderBy('codigo_iso', 'asc')
    ->get()
    ->pluck('nombre_completo','codigo_iso')
    ->prepend('Seleccione código ISO', '');

    return View('configuracion.monedas.tipos.co_ti_02_c_crear')
    ->with('codigo_iso', $codigo_iso);
  }

  public function store(MonedaCreateRequest $request)
  {

    $rules = [];
    $messages = [];
    $validator = Validator::make($request->all(), $rules, $messages);

    $codigo_iso = CodigoIso::where('codigo_iso', $request->input('codigo_iso'))
    ->first();

      $moneda = new Moneda();
      $moneda->codigo_iso = $codigo_iso->codigo_iso;
      $moneda->nombre = $codigo_iso->nombre;
      $moneda->num_decimales = $codigo_iso->num_decimales;
      $moneda->estatus = $request->input('estatus');
      $moneda->simbolo = $codigo_iso->simbolo;
      $moneda->separador_miles = $codigo_iso->separador_miles;
      $moneda->separador_decimales = $codigo_iso->separador_decimales;
      $result = $moneda->save();



    notify()->flash('Moneda creada', 'success', [
      'timer' => 3000,
      'text' => ''
    ]);

    return redirect()->route('Monedas.Tipos.index');
  }

  public function show($id)
  {
    $monedaMostrar = Moneda::FindOrFail($id);
    foreach ($monedaMostrar->monedaoperacion as $monedaope) {
      $monedaope->tipo_operacion->nombre;
    }

    return view('configuracion.monedas.tipos.co_ti_04_d_detalle')
    ->with('monedaMostrar',$monedaMostrar);
  }

  public function edit($id)
  {
    $moneda = Moneda::find($id);
    return View('configuracion.monedas.tipos.co_ti_03_e_editar')
    ->with('moneda', $moneda);
  }

  public function update(MonedaUpdateRequest $request, $id)
  {

    $rules = [];
    $messages = [];
    $validator = Validator::make($request->all(), $rules, $messages);

    $moneda = Moneda::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
    $result = $moneda->fill($input)->save(); /*Se guarda la información*/

    notify()->flash('Moneda actualizada', 'success', [
      'timer' => 3000,
      'text' => ''
    ]);

    return redirect()->route('Monedas.Tipos.index');
  }

  public function destroy($id)
  {
    $moneda = Moneda::FindOrFail($id);
    $result = $moneda->delete();

    if ($result)
    {
      return response()->json(['success'=>'true']);
    }
    else
    {
      return response()->json(['success'=> 'false']);
    }
  }
}
