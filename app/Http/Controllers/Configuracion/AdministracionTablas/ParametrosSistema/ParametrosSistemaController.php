<?php

namespace App\Http\Controllers\Configuracion\AdministracionTablas\ParametrosSistema;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Http\Requests\Configuracion\ParametroSistema\ParametroSistemaUpdateRequest;
use Alert;

class ParametrosSistemaController extends Controller
{
  public function Index(Request $request)
  {
    if($request->get('idt') != null)
    {
      $id = $request->get('idt');
      $nombre = $request->get('nombre');
      $parametros = ParametroSistema::select('t_parametros_sistema.*')
      ->where('id', $id)
      ->paginate(30);
    }
    else{
      $parametros = ParametroSistema::orderBy('id', 'asc')
      ->paginate(30);
    }

    return view('configuracion.administracion_tablas.parametros_sistema.co_pa_01_i_parametros')
     ->with('parametros', $parametros);
  }

  public function edit(Request $request,$id)
  {
    
    $parametro = ParametroSistema::where('id',$id )->first();

    return view('configuracion.administracion_tablas.parametros_sistema.co_pa_02_e_editar')
      ->with('parametro_sistema', $parametro);
  }

  public function update(ParametroSistemaUpdateRequest $request, $id)
  {
    
    $parametro = ParametroSistema::FindOrFail($id);
    $parametro->nombre = $request->get('nombre');  
    $parametro->valor = $request->get('valor'); 
    $parametro->comentario = $request->get('comentario'); 
    $parametro->update();

    Alert::success('!','Parámetro del Sistema actualizado');

    return redirect()->route('ParametrosSolucion.index');  
  }

}