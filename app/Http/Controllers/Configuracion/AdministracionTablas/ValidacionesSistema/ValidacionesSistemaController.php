<?php

namespace App\Http\Controllers\Configuracion\AdministracionTablas\ValidacionesSistema;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use App\Models\Configuracion\ValidacionesSistema\ValidacionesSistema;
use App\Http\Requests\Configuracion\ValidacionSistema\ValidacionSistemaUpdateRequest;

class ValidacionesSistemaController extends Controller
{
    public function Index(Request $request)
    {

    $nivel = $request->get('nivel'); 
    //dd($request->get('nivel'));
    $codinter = $request->get('codit');
    $codresp = $request->get('codrt'); 

    $validaciones = ValidacionesSistema::orderBy('id', 'asc')

    ->where(function($query) use ($nivel, $codinter, $codresp) {

      if ($nivel != null) {
        $query->where('nivel',$nivel);
      };

      if ($codinter != null) {
        $query->where('codigo_interno_error',$codinter);
      };

      if ($codresp != null) {
        $query->where('respuesta_codigo',$codresp);
      };

    })
    
    ->paginate(15);

    return view('configuracion.administracion_tablas.validaciones_sistema.co_ad_01_i_validacionessis')
    ->with('validaciones', $validaciones);
    
    }
    public function edit(Request $request,$id)
  {
     
  $validacion = ValidacionesSistema::
  where('id',$id )
  ->first();

   return view('configuracion.administracion_tablas.validaciones_sistema.co_ad_02_e_validacionessis')
   ->with('validacion', $validacion);

  }

 public function update(ValidacionSistemaUpdateRequest $request, $id)
  {
    
    $validacion = ValidacionesSistema::FindOrFail($id);
    $validacion->codigo_interno_error = $request->get('codigo_interno_error'); 
    $validacion->nivel = $request->get('nivel');  
    $validacion->respuesta_accion = $request->get('respuesta_accion');  
    $validacion->respuesta_codigo = $request->get('respuesta_codigo');   
    $validacion->codigo_descripcion = $request->get('codigo_descripcion');  
    $validacion->causa = $request->get('causa');  
    $validacion->archivos_aplica = $request->get('archivos_aplica'); 
    $validacion->save();


    alert()->success('!', 'Validacion del sistema Actualizada Satisfactoriamente');

  return redirect()->route('ValidacionesSolucion.index');  
 
  }

}