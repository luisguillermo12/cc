<?php

namespace App\Http\Controllers\Configuracion\Contacto;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Contacto\Contacto;
use App\Models\Configuracion\Contacto\ContactoBancario;
use App\Models\Configuracion\Contacto\TipoContacto;
use App\Models\Configuracion\Contacto\Turno;
use App\Models\Configuracion\Contacto\PosicionEscalamiento;
use App\Http\Requests\Configuracion\Contacto\ContactoBancario\ContactoBancarioCreateRequest;
use App\Http\Requests\Configuracion\Contacto\ContactoBancario\ContactoBancarioUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Log;

class ParticipantesController extends Controller
{

  public function getIndex(Request $request)
  {

    try {

      Log::info('Ingreso a la opción Contacto participante');

      // BANCOS
      $bancos = Banco::buscarTodos(null, false)
      ->prepend('*', '');;

      $nivel = PosicionEscalamiento::select('id','posicion')
      ->orderBy('posicion', 'asc')
      ->pluck('posicion','id')
      ->prepend('*', '');

      if($request->get('cod_banco') != null || $request->get('cod_nivel') != null)
      {
        $cod_nivel = $request->get('cod_nivel');
        $cod_banco = $request->get('cod_banco');

        $contactosBancarios = ContactoBancario::select('t_contactos.*','t_bancos.nombre as banco','t_turnos_contactos.turno as turno', 't_tipos_contactos.nombre as tipo_contacto','t_posicion_escalamiento_contacto.posicion as posicion','t_bancos.codigo')
        ->join('t_bancos','t_contactos_bancarios.banco_id','=','t_bancos.id')
        ->join('t_tipos_contactos','t_contactos_bancarios.tipo_contacto_id','=','t_tipos_contactos.id')
        ->join('t_contactos','t_contactos_bancarios.contacto_id','=','t_contactos.id')
        ->join('t_turnos_contactos','t_contactos.turno_id','=','t_turnos_contactos.id')
        ->join('t_posicion_escalamiento_contacto','t_contactos.posicion_escalamiento_id','=','t_posicion_escalamiento_contacto.id')
        ->orderBy('posicion', 'asc')
        ->where('t_bancos.codigo', $cod_banco)
        ->when($cod_nivel, function ($query) use ($cod_nivel) {
          return $query->where('t_posicion_escalamiento_contacto.id', $cod_nivel);
        })
        ->get();
      }
      else
      {
        $cod_banco = '*';
        $cod_nivel = '*';

        $contactosBancarios = ContactoBancario::select('t_contactos.*','t_bancos.nombre as banco','t_turnos_contactos.turno as turno', 't_tipos_contactos.nombre as tipo_contacto','t_posicion_escalamiento_contacto.posicion as posicion','t_bancos.codigo')
        ->join('t_bancos','t_contactos_bancarios.banco_id','=','t_bancos.id')
        ->join('t_tipos_contactos','t_contactos_bancarios.tipo_contacto_id','=','t_tipos_contactos.id')
        ->join('t_contactos','t_contactos_bancarios.contacto_id','=','t_contactos.id')
        ->join('t_turnos_contactos','t_contactos.turno_id','=','t_turnos_contactos.id')
        ->join('t_posicion_escalamiento_contacto','t_contactos.posicion_escalamiento_id','=','t_posicion_escalamiento_contacto.id')
        ->orderBy('posicion', 'asc')
        ->get();
      }

      return View('configuracion.contactos.participantes.co_par_01_i_participantes')
      ->with('bancos', $bancos)
      ->with('nivel', $nivel)
      ->with('cod_nivel', $cod_nivel)
      ->with('cod_banco', $cod_banco)
      ->with('contactosBancarios', $contactosBancarios);

    } catch (QueryException $ex){
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      throw new Exception($ex);
    } catch (Exception $ex) {
      throw new Exception($ex);
    }
  }


  public function create()
  {

        $bancos = Banco::select('id', 'codigo', 'nombre')
        ->where('estatus','ACTIVO')
        ->get();

        $bancos = $bancos->keyBy('id')
        ->map(function ($item, $key) {
            return $item->nombre_completo;
        })->prepend('Seleccione', 0);

    $tipos_contactos = TipoContacto::where('estatus','ACTIVO')
    ->orderBy('nombre', 'asc')
    ->pluck('nombre','id')
    ->prepend('Seleccione Tipo de Contacto', 0);

    $list_turnos = Turno::select('t_turnos_contactos.id','t_turnos_contactos.turno')
    ->orderBy('id', 'asc')
    ->pluck('turno','id')
    ->prepend('Seleccione turno', 0);

    $list_posicion = PosicionEscalamiento::select('t_posicion_escalamiento_contacto.*')
    ->orderBy('id', 'asc')
    ->pluck('posicion','id')
    ->prepend('Seleccione posición de escalamiento', 0);

    return View('configuracion.contactos.participantes.co_par_02_c_crearpar')
    ->with('bancos', $bancos)
    ->with('list_turnos', $list_turnos)
    ->with('list_posicion', $list_posicion)
    ->with('tipos_contactos', $tipos_contactos);
  }

  public function store(ContactoBancarioCreateRequest $request)
  {
    DB::beginTransaction();

    try {

      // Contacto
      $contacto = new Contacto();
      $contactoBancario = new ContactoBancario();
      $contacto->cedula = $request->input('cedula');
      $contacto->nombre = $request->input('nombre');
      $contacto->cargo = $request->input('cargo');
      $contacto->telefono_celular = $request->input('telefono_celular');
      $contacto->telefono_local = $request->input('telefono_local');
      $contacto->email = $request->input('email');
      $contacto->estatus = $request->input('estatus');
      $contacto->turno_id = $request->input('turno_id');
      $contacto->posicion_escalamiento_id = $request->input('posicion_escalamiento_id');
      $resultContacto = $contacto->save();
    
      $contactoBancario->banco_id = $request->input('banco_id');
      $contactoBancario->tipo_contacto_id = $request->input('tipo_contacto_id');
      $resultContactoBancario = $contacto->contactobancario()->save($contactoBancario);

      DB::commit();

      alert()->success('!', 'Contacto participante creado');


      return redirect()->route('ContactosParticipantes');

    } catch (QueryException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }

  public function show(Request $request, $id)
  {
    try {
      Log::info('Ingreso a la opción Contacto participante');
      $contactoBancario = ContactoBancario::
      select('t_contactos.*','t_bancos.nombre as banco', 't_turnos_contactos.turno as turno','t_tipos_contactos.nombre as tipo_contacto','t_posicion_escalamiento_contacto.posicion as posicion')
      ->join('t_contactos','t_contactos.id','=','t_contactos_bancarios.contacto_id')
      ->join('t_turnos_contactos','t_turnos_contactos.id','=','t_contactos.turno_id')
      ->join('t_posicion_escalamiento_contacto','t_posicion_escalamiento_contacto.id','=','t_contactos.posicion_escalamiento_id')
      ->join('t_bancos','t_bancos.id','=','t_contactos_bancarios.banco_id')
      ->join('t_tipos_contactos','t_tipos_contactos.id','=','t_contactos_bancarios.tipo_contacto_id')
      ->where('t_contactos_bancarios.contacto_id','=', $id)
      ->get()
      ->first();

      $bancos = Banco::buscarTodos('Seleccione Banco', 0);


      if($request->get('cod_banco') !=null)
      {
        // TIPOS DE CONTACTOS
        $cod_banco = $request->get('cod_banco');
        $contactosBancarios = ContactoBancario::select('t_contactos.*','t_turnos_contactos.turno', 't_tipos_contactos.nombre as tipo_contacto','t_posicion_escalamiento_contacto.posicion')
        ->join('t_bancos','t_contactos_bancarios.banco_id','=','t_bancos.id')
        ->join('t_tipos_contactos','t_contactos_bancarios.tipo_contacto_id','=','t_tipos_contactos.id')
        ->join('t_contactos','t_contactos_bancarios.contacto_id','=','t_contactos.id')
        ->join('t_turnos_contactos','t_contactos.turno_id','=','t_turnos_contactos.id')
        ->join('t_posicion_escalamiento_contacto','t_contactos.posicion_escalamiento_id','=','t_posicion_escalamiento_contacto.id')
        ->orderBy('posicion', 'asc')
        ->where('t_bancos.id',$cod_banco)
        ->get();
        //dd($contactosBancarios);

        $tipos_contactos = TipoContacto::where('estatus','ACTIVO')
        ->orderBy('nombre', 'asc')
        ->pluck('nombre','id')
        ->prepend('Seleccione Tipo de Contacto', 0);
        return view('configuracion.contactos.contactos_bancarios.show')
        ->with('contactoBancario',$contactoBancario);
      }
      else
      {
        $tipos_contactos = null;
        $contactosBancarios = null;
        $cod_banco = 0;//seleccion del periodo
      }

    } catch (QueryException $ex){
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      throw new Exception($ex);
    } catch (Exception $ex) {
      throw new Exception($ex);
    }

    return view('configuracion.contactos.participantes.co_par_04_s_detallespar')
    ->with('bancos',$bancos)
    ->with('contactosBancarios',$contactosBancarios)
    ->with('tipos_contactos',$tipos_contactos)
    ->with('cod_banco', $cod_banco)
    ->with('contactoBancario',$contactoBancario);

  }


  public function edit($id)
  {

    try {

      $bancos = Banco::buscarTodos('Seleccione Banco', 0);

      $tipos_contactos = TipoContacto::where('estatus','ACTIVO')
      ->orderBy('nombre', 'asc')
      ->pluck('nombre','id')
      ->prepend('Seleccione Tipo de Contacto', 0);

      $list_turnos = Turno::select('t_turnos_contactos.id','t_turnos_contactos.turno')
      ->orderBy('turno', 'asc')
      ->pluck('turno','id')
      ->prepend('Seleccione turno', 0);

      $list_posicion = PosicionEscalamiento::select('t_posicion_escalamiento_contacto.*')
      ->orderBy('id', 'asc')
      ->pluck('posicion','id')
      ->prepend('Seleccione posición de escalamiento', 0);

      $contacto_bancario = ContactoBancario::
      select('t_contactos.*','t_contactos_bancarios.banco_id','t_contactos_bancarios.tipo_contacto_id')
      ->join('t_contactos','t_contactos.id','=','t_contactos_bancarios.contacto_id')
      ->join('t_turnos_contactos','t_turnos_contactos.id','=','t_contactos.turno_id')
      ->join('t_posicion_escalamiento_contacto','t_posicion_escalamiento_contacto.id','=','t_contactos.posicion_escalamiento_id')
      ->join('t_bancos','t_bancos.id','=','t_contactos_bancarios.banco_id')
      ->join('t_tipos_contactos','t_tipos_contactos.id','=','t_contactos_bancarios.tipo_contacto_id')
      ->where('t_contactos_bancarios.contacto_id','=', $id)
      ->get()
      ->first();

      return view('configuracion.contactos.participantes.co_par_03_e_editarpar')
      ->with('bancos',$bancos)
      ->with('tipos_contactos',$tipos_contactos)
      ->with('list_turnos',$list_turnos)
      ->with('list_posicion',$list_posicion)
      ->with('contacto_bancario',$contacto_bancario);

    } catch (QueryException $ex) {
      throw new Exception($ex);
    } catch (PDOException $ex) {
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      throw new Exception($ex);
    } catch (Exception $ex) {
      throw new Exception($ex);
    }
  }

  public function update(ContactoBancarioUpdateRequest $request, $id)
  {
    DB::beginTransaction();

    try {

      // Contacto
      $contacto = Contacto::FindOrFail($id);
      $contacto->cedula = $request->input('cedula');
      $contacto->nombre = $request->input('nombre');
      $contacto->cargo = $request->input('cargo');
      $contacto->telefono_celular = $request->input('telefono_celular');
      $contacto->telefono_local = $request->input('telefono_local');
      $contacto->email = $request->input('email');
      $contacto->estatus = $request->input('estatus');
      $contacto->turno_id = $request->input('turno_id');
      $contacto->posicion_escalamiento_id = $request->input('posicion_escalamiento_id');
      $resultContacto = $contacto->update();

      // Contacto participante
      $contactoBancario = ContactoBancario::where('contacto_id', $id)->first();
      $contactoBancario->banco_id = $request->input('banco_id');
      $contactoBancario->tipo_contacto_id = $request->input('tipo_contacto_id');
      $resultContactoBancario = $contactoBancario->update();

      DB::commit();

      alert()->success('!', 'Contacto participante actualizado');



      return redirect()->route('ContactosParticipantes');

    } catch (QueryException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }

  public function destroy($id)
  {

    DB::beginTransaction();

    try {

      $contacto = Contacto::FindOrFail($id);
      $contacto_bancario = ContactoBancario::where('contacto_id',$id);
      $resultContactoBancario = $contacto_bancario->delete();
      $resultContacto = $contacto->delete();

      if ($resultContactoBancario && $resultContacto)
      {

        alert()->success('!', 'Contacto participante eliminado');


        DB::commit();

        //return redirect()->route('Periodos');
        return response()->json(['success'=>'true']);

      }
      else
      {
        DB::rollback();

        //return redirect()->route('Periodos');
        return response()->json(['success'=> 'false']);
      }

    } catch (QueryException $ex){
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }
}
