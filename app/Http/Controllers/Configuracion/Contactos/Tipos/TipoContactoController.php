<?php

namespace App\Http\Controllers\Configuracion\Contacto;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Contacto\TipoContacto;
use App\Http\Requests\Configuracion\Contacto\TipoContacto\TipoContactoCreateRequest;
use App\Http\Requests\Configuracion\Contacto\TipoContacto\TipoContactoUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Alert;

class TipoContactoController extends Controller
{

  public function getIndex()
  {
    $tiposContactos = TipoContacto::orderBy('nombre', 'asc')->get();

    return View('configuracion.contactos.tipos_contactos.co_ti_01_i_tipos')
      ->with('tiposContactos', $tiposContactos);
  }

  public function create()
  {
    return View('configuracion.contactos.tipos_contactos.co_ti_03_c_crear');
  }

  public function store(TipoContactoCreateRequest $request)
  {
    //$result = TipoContacto::create($request->all());
    $newTipoContacto = new TipoContacto;
    $newTipoContacto->nombre = $request->nombre;
    $newTipoContacto->estatus = 'ACTIVO';
    $newTipoContacto->save();

    Alert::success('!','Tipo de Contacto creado');

    return redirect()->route('TiposContactos');
  }

  public function show($id)
  {

    $tipo_contacto = TipoContacto::FindOrFail($id);

    return response()->json($tipo_contacto); /*Devuelve un array con los datos del registro*/
  }

  public function edit($id)
  {

    $tipo_contacto = TipoContacto::find($id);

    return view('configuracion.contactos.tipos_contactos.co_ti_02_e_editar')
      ->with('tipo_contacto',$tipo_contacto);
  }

  public function update(TipoContactoUpdateRequest $request, $id)
  {

    $tipo_contacto = TipoContacto::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
    $result = $tipo_contacto->fill($input)->save(); /*Se guarda la información*/

    Alert::success('!','Tipo de Contacto actualizado');

    return redirect()->route('TiposContactos');
  }

  public function destroy($id)
  {
    $tipo_contacto = TipoContacto::FindOrFail($id);
    $result = $tipo_contacto->delete();

    if ($result)
    {

      return response()->json(['success'=>'true']);
    }
    else
    {

      return response()->json(['success'=> 'false']);
    }
  }
}
