<?php

namespace App\Http\Controllers\Configuracion\Cobranza\RecuperacionCostos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\MedioPago\Producto;

class RecuperacionCostosController extends Controller
{
    public function index(Request $request){
        $tipos_operaciones = TipoOperacion::where('t_tipos_operaciones.estatus', 'ACTIVO')
        ->orderBy('codigo', 'asc')
        ->get();
                $total_operaciones = $tipos_operaciones->count();
                $total_operaciones_facturables =$tipos_operaciones->where('factura', '1')->count();
                $total_operaciones_devueltas = $tipos_operaciones->where('direccion', 'D')->count();
                $total_operaciones_presentadas = $tipos_operaciones->where('direccion', 'P')->count();
                $presentadas_facturables = $tipos_operaciones->where('direccion', 'P')->where('factura', '1')->count();
                $devueltas_facturables =  $tipos_operaciones->where('direccion', 'D')->where('factura', '1')->count();
                $presentadas=0;
                $devueltas =0;
                $todas =0;
                $presentadas=($total_operaciones_presentadas == $presentadas_facturables)? 1 : 0;
                $devueltas= ($total_operaciones_devueltas == $devueltas_facturables)? 1 : 0;
                        if ($total_operaciones == $total_operaciones_facturables){$todas=1; $presentadas =0;  $devueltas=0; }else { $todas=0;}
        return view('configuracion.cobranza.recuperacion_costos.co_re_01_i_recuperacion_costos')
        ->with('presentadas', $presentadas)
        ->with('devueltas', $devueltas)
        ->with('todas', $todas)
        ->with('tipos_operaciones', $tipos_operaciones);
    }
    public function edit(Request $request, $id){
    	$operacion = TipoOperacion::findOrFail($id);
    	return view('configuracion.cobranza.recuperacion_costos.co_re_02_e_editar')
        ->with('operacion', $operacion);
    }
    public function update(Request $request, $id){
    	$operacion = TipoOperacion::findOrFail($id);
    	$operacion->factura = $request->get('factura');
    	$operacion->save();
		    alert()->success('Recuperacion de costos  Actualizada  exitosamente');
    	return redirect()->route('RecuperacionCostos.index');
    }
    public function ajaxActualizar($direccion){
    	    if ($direccion == 'A') {
    		        $operaciones = TipoOperacion::query()
    		        ->update(['factura' => 1]);
    	    } elseif ($direccion != null) {
    		$operaciones = TipoOperacion::where('direccion', $direccion)
    	            ->update(['factura' => 1]);
    		$operaciones = TipoOperacion::where('direccion', '<>', $direccion)
    	            ->update(['factura' => 0]);
    	     }
    	return response()->json(['success' => true]);
    }
}
