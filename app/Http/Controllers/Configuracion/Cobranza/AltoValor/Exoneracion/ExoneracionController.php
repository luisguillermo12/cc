<?php

namespace App\Http\Controllers\Configuracion\Cobranza\AltoValor\Exoneracion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\MedioPago\SubProducto;
use App\Http\Requests\Configuracion\MedioPago\SubProducto\SubProductoCreateRequest;
use App\Http\Requests\Configuracion\MedioPago\SubProducto\SubProductoUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;

class ExoneracionController extends Controller
{
    //
    public function Index(Request $request)
    {

        $producto = $request->get('producto');
    
        $subproductos = SubProducto::select('t_sub_productos.*','t_productos.nombre as producto','t_productos.codigo as producto_codigo')
        ->join('t_productos','t_productos.id','=','t_sub_productos.producto_id')
        ->orderBy('t_productos.nombre', 'asc')
        ->where('t_sub_productos.estatus','<>','RETIRADO')
        ->where(function ($query) use ($producto) {
            if ($producto != null) {
                $query->where('t_sub_productos.producto_id', $producto);
            }

        })
        ->get();

        $productos = Producto::where('estatus', 'ACTIVO')
        ->orderBy('codigo')
        ->pluck('nombre', 'id')
        ->prepend('*', '');

        return view('configuracion.cobranza.altovalor.exoneraciones.co_ex_01_i_exoneracion')
        ->with('subproductos', $subproductos)
        ->with('productos', $productos)
        ->with('producto', $producto);
    }

    public function edit($id)
    {
    

    $subproducto = SubProducto::find($id);

     $productos = Producto::select('t_productos.nombre','t_tipos_operaciones.signo')
       ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
       ->where('t_productos.id',$subproducto->producto_id)
       ->get()
       ->first();

    //dd($subproducto);
     return view('configuracion.cobranza.altovalor.exoneraciones.co_ex_02_e_editar')
    ->with('productos', $productos)
    ->with('subproducto', $subproducto);
  }

 public function update(Request $request, $id)
  {

    $subproducto = SubProducto::FindOrFail($id);
    $subproducto->exonerado = $request->get('exonerado');  
    $subproducto->save();

    alert()->success('Exoneracion  actualizada  exitosamente');

   return redirect()->route('Exoneracion.index');  
  }

}
