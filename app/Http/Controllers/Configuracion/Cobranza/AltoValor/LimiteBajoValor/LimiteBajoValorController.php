<?php
namespace App\Http\Controllers\Configuracion\Cobranza\AltoValor\LimiteBajoValor;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\MedioPago\SubProducto;
use App\Models\Configuracion\MedioPago\TipoDevolucion;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Monitoreo\FlujoLote;
use App\Http\Requests\Configuracion\MedioPago\Producto\ProductoCreateRequest;
use App\Http\Requests\Configuracion\MedioPago\Producto\ProductoUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use App\Models\Configuracion\Moneda\Moneda;


class LimiteBajoValorController extends Controller
{
    public function Index(Request $request){
    	        $moneda_activa = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
                                        ->where('t_monedas.estatus','ACTIVO')
                                        ->first();

                $productos = Producto::select('t_productos.id', 't_productos.nombre', 't_productos.estatus','t_productos.limite_bajo_valor', 't_tipos_operaciones.codigo', 't_tipos_operaciones.signo')
                                        ->leftJoin('t_tipos_operaciones', 't_tipos_operaciones.producto_id', '=', 't_productos.id')
                                        ->where('t_tipos_operaciones.estatus', '<>', 'RETIRADO')
                                        ->orderBy('t_tipos_operaciones.codigo', 'asc')
                                        ->get()
                                        ->groupBy('nombre');

        return view('configuracion.cobranza.altovalor.limite_bajo_valor.co_lb_01_i_limitebajovalor')
        ->with('moneda_activa', $moneda_activa)
        ->with('productos', $productos);
    }
    public function edit($id){
                $producto = Producto::select('t_productos.*','t_tipos_operaciones.signo')
                ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
                ->where('t_productos.id',$id)
                ->get()
                ->first();
         return view('configuracion.cobranza.altovalor.limite_bajo_valor.co_lb_02_e_editar_limite_bajo_valor')
         ->with('producto', $producto);
    }
    public function update(Request $request, $id){
                $producto = Producto::findOrFail($id);
                $producto->limite_bajo_valor = str_replace('.', '', $request->get('limite_bajo_valor'));
                $producto->save();
                alert()->success('Limite de bajo valor  actualizado  exitosamente');
                return redirect()->route('LimiteBajoValor.index');
    }




}
