<?php

namespace App\Http\Controllers\Configuracion\MedioPago;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\MedioPago\SubProducto;
use App\Http\Requests\Configuracion\MedioPago\SubProducto\SubProductoCreateRequest;
use App\Http\Requests\Configuracion\MedioPago\SubProducto\SubProductoUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Alert;

class SubProductoController extends Controller
{
    public function index()
    {
        $subproductos = SubProducto::select('t_sub_productos.*','t_productos.nombre as producto','t_productos.codigo as producto_codigo')
        ->join('t_productos','t_productos.id','=','t_sub_productos.producto_id')
        ->orderBy('t_productos.nombre', 'asc')
        ->where('t_sub_productos.estatus','<>','RETIRADO')
        ->paginate();

        return View('configuracion.medios_pagos.sub_productos.co_sp_01_i_inicio')
        ->with('subproductos', $subproductos);
    }

    public function create()
    {
        $productos = Producto::select('id', 'nombre')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo', 'asc')
        ->pluck('nombre','id')
        ->prepend('Seleccione Producto', 0);

        return View('configuracion.medios_pagos.sub_productos.co_sp_02_c_crear')
        ->with('productos', $productos);
    }

  public function store(SubProductoCreateRequest $request)
  {
    $result = SubProducto::create($request->all());

    Alert::success('!','Sub-Producto creado');

    return redirect()->route('SubProductos.index');
  }

  public function show($id)
  {

  }

    public function edit($id)
    {
        $productos = Producto::select('id', 'nombre')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo', 'asc')
        ->pluck('nombre','id')
        ->prepend('Seleccione Producto', 0);

        $subproducto = SubProducto::find($id);

        return View('configuracion.medios_pagos.sub_productos.co_sp_03_e_editar')
        ->with('productos', $productos)
        ->with('subproducto', $subproducto);
    }

  public function update(SubProductoUpdateRequest $request, $id)
  {
    $subproducto = SubProducto::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
    $result = $subproducto->fill($input)->save(); /*Se guarda la información*/

    Alert::success('!','Sub-Producto actualizado');

    return redirect()->route('SubProductos.index');
  }

  public function destroy($id)
  { 
    $subproducto = SubProducto::FindOrFail($id);
    $result = $subproducto->delete();

    if ($result)
    {
        return response()->json(['success'=>'true']);
    }
    else
    {
        return response()->json(['success'=> 'false']);
    }
  }
}
