<?php

namespace App\Http\Controllers\Configuracion\MedioPago;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\MedioPago\SubProducto;
use App\Models\Configuracion\MedioPago\TipoDevolucion;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Monitoreo\FlujoLote;
use App\Http\Requests\Configuracion\MedioPago\Producto\ProductoCreateRequest;
use App\Http\Requests\Configuracion\MedioPago\Producto\ProductoUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use Alert;

class ProductoController extends Controller
{
    public function index(Request $request)
    {

        /* Recibe los parámetros de los filtros */
        $estatus = $request->get('estatus');
        $codigo = $request->get('codigo');
        $producto = $request->get('producto');

        /* Selecciona los datos para la tabla */
        $productos = Producto::select('t_productos.id', 't_productos.nombre', 't_productos.estatus', 't_productos.codigo', 't_tipos_operaciones.codigo as codigo_intervalo', 't_tipos_operaciones.signo')
        ->leftJoin('t_tipos_operaciones', 't_tipos_operaciones.producto_id', '=', 't_productos.id')
        ->where(function($query) use ($estatus, $codigo, $producto) {
            if ($estatus != null) {
                $query->where('t_tipos_operaciones.estatus', $estatus);
            } else {
                $query->where('t_tipos_operaciones.estatus', '<>', 'RETIRADO');
            };
            if ($codigo != null) {
                $query->where('t_tipos_operaciones.codigo', 'LIKE', '%'.substr($codigo, 1));
            };
            if ($producto != null) {
                $query->where('t_productos.id', $producto);
            };
        })
        ->orderBy('t_tipos_operaciones.codigo', 'asc')
        ->get()
        ->groupBy('nombre');

        /* Lista los códigos para los tipos de operaciones */
        $codigos = TipoOperacion::select('codigo')
        ->orderBy('codigo')
        ->get()
        ->pluck('codigo', 'codigo')
        ->prepend('*', '');

        /* Lista los productos */
        $listadoProductos = Producto::select('id', 'nombre')
        ->orderBy('id')
        ->get()
        ->pluck('nombre', 'id')
        ->prepend('*', '');

        return view('configuracion.medios_pagos.productos.co_pr_01_i_inicio')
        ->with('estatus', $estatus)
        ->with('codigos', $codigos)
        ->with('codigo', $codigo)
        ->with('listadoProductos', $listadoProductos)
        ->with('producto', $producto)
        ->with('productos', $productos);
    }

  public function anyData()
  {
    $productos = Producto::select('t_productos.*');
    return Datatables::of($productos)
    ->orderBy('codigo', 'asc')
    ->addColumn('action', function ($productos) {
        return '<a title="Ver Banco" href="#" OnClick="verBanco('.$productos->id.')" class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal-banco-view"><i class="fa fa-eye"></i></a>
                <a title="Editar Banco" href="#" OnClick="editarBanco('.$productos->id.')" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal-banco-edit"><i class="fa fa-edit"></i></a>
                <a title="Eliminar Banco" href="#" OnClick="eliminarBanco('.$productos->id.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';
    })
    ->editColumn('id', 'ID: {{$id}}')
    ->make(true);
  }

  public function create()
  {
    return view('configuracion.medios_pagos.productos.co_pr_02_c_crear');
  }

  public function store(ProductoCreateRequest $request)
  {
    DB::beginTransaction();

    try {
      $newProducto = new Producto;
      $newProducto->codigo = $request->codigo;
      $newProducto->nombre = $request->nombre;
      $newProducto->estatus = $request->estatus;
      $newProducto->limite_financiero = $request->limite_financiero;
     // dd($newProducto);
      $newProducto->save();
      //$valor = substr($newProducto->codigo,0,1);

      //Operaciones presentadas
      $OperacionPresentadas = new TipoOperacion;
      $OperacionPresentadas->producto_id = $newProducto->id;
      $OperacionPresentadas->codigo = $request->codigo_presentada;
      $OperacionPresentadas->signo = $request->signo;
      $OperacionPresentadas->nombre = $request->nombre;
      $OperacionPresentadas->estatus = $request->estatus;
      $OperacionPresentadas->direccion = 'P';
      $OperacionPresentadas->save();

      //Operaciones devuelta
      $OperacionDevuelta = new TipoOperacion;
      $OperacionDevuelta->producto_id =$newProducto->id;
      $OperacionDevuelta->codigo = $request->codigo_devuelta;
      $OperacionDevuelta->signo = $request->signo;
      $OperacionDevuelta->nombre = 'DEVOLUCION DE '.$request->nombre;
      $OperacionDevuelta->estatus = $request->estatus;
      $OperacionDevuelta->direccion = 'D';
      $OperacionDevuelta->save();


      DB::commit();

      Alert::success('!','Producto creado');

      return redirect()->route('Productos.index');

    } catch (QueryException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }

  public function show($id)
  {

  }

  public function edit($id)
  {
    $producto = Producto::select('t_productos.*','t_tipos_operaciones.signo')
    ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
    ->where('t_productos.id',$id)
    ->get()
    ->first();

    return View('configuracion.medios_pagos.productos.co_pr_03_e_editar')
    ->with('producto', $producto);
  }

  public function update(ProductoUpdateRequest $request, $id)
  {
    DB::beginTransaction();

    try {
      $producto = Producto::FindOrFail($id);
      $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
      $result = $producto->fill($input)->save(); /*Se guarda la información*/


      $tipoOperacion = TipoOperacion::
      where('producto_id', $producto->id)
      ->get();


      foreach($tipoOperacion as $tipooperacion) {
        //$producto->codigo = $request->input('codigo_producto');
        $tipooperacion->signo = $request->input('signo');
        $tipooperacion->estatus = $request->input('estatus');
        $tipooperacion->save();
      }

      /*$productos = Producto::
      where('id', $tipoOperacion->producto_id)
      ->get();


      foreach($productos as $producto) {
        //$producto->codigo = $request->input('codigo_producto');
        $producto->nombre = $request->input('nombre_producto');
        $producto->estatus = $request->input('estatus');
        $producto->limite_financiero = $request->input('limite_financiero');
        $producto->save();
      }*/
      //dd($producto);
      //BUSCAR SUBPRODUCTOS POR PRODUCTO
      $subProductos = SubProducto::
      where('producto_id', $producto->id)
      ->get();

      foreach($subProductos as $subProducto) {
        $subProducto->estatus = $request->input('estatus');
        $subProducto->save();
      }

      //BUSCAR TIPOS DE OPERACIONES POR PRODUCTO
    /*  $tiposOperaciones = TipoOperacion::
      where('producto_id', $id)
      ->get();

      foreach($tiposOperaciones as $tipoOperacion) {
        $tipoOperacion->estatus = $request->input('estatus');
        $tipoOperacion->save();*/

        //BUSCAR SUBPRODUCTOS POR PRODUCTO
        $tiposDevoluciones = TipoDevolucion::
        where('tipo_operacion_id', $producto->id)
        ->get();

        foreach($tiposDevoluciones as $tipoDevolucion) {
          $tipoDevolucion->estatus = $request->input('estatus');
          $tipoDevolucion->save();
        }
      //}



      DB::commit();

      Alert::success('!','Producto actualizado');

      return redirect()->route('Productos.index');

    } catch (QueryException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }

  public function destroy($id)
  {
    /*$producto = Producto::FindOrFail($id);
    $result = $producto->delete();

    if ($result)
    {
        return response()->json(['success'=>'true']);
    }
    else
    {
        return response()->json(['success'=> 'false']);
    }*/
    DB::beginTransaction();

    try {

      $producto = Producto::FindOrFail($id);

      $tipoOperacion = TipoOperacion::where('producto_id', $id);

      $resultTipoOperacion = $tipoOperacion->delete();
      $resultProducto = $producto->delete();

      if ($resultTipoOperacion && $resultProducto)
      {

        notify()->flash('Producto eliminado', 'success', [
          'timer' => 3000,
          'text' => ''
        ]);

        DB::commit();

        //return redirect()->route('Periodos');
        return response()->json(['success'=>'true']);

      }
      else
      {
        DB::rollback();

        //return redirect()->route('Periodos');
        return response()->json(['success'=> 'false']);
      }

    } catch (QueryException $ex){
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }
}
