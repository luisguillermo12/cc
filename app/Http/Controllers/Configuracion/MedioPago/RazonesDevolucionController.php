<?php

namespace App\Http\Controllers\Configuracion\MedioPago;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Configuracion\MedioPago\TipoDevolucion;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Http\Requests\Configuracion\MedioPago\TipoDevolucion\TipoDevolucionCreateRequest;
use App\Http\Requests\Configuracion\MedioPago\TipoDevolucion\TipoDevolucionUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Input;
use Response;
use App\Models\Configuracion\MedioPago\Producto;
use Alert;

class RazonesDevolucionController extends Controller
{
  public function index(Request $request)
  {
      $operacion = $request->get('producto');
      $estatus = $request->get('estatus');
      $codigo = $request->get('codigo');

      $tipos_devoluciones = TipoDevolucion::select('t_tipos_devoluciones.id','t_tipos_devoluciones.tipo_operacion_id','t_tipos_devoluciones.codigo','t_tipos_devoluciones.nombre','t_tipos_devoluciones.estatus','t_tipos_operaciones.producto_id','t_productos.nombre as producto')
      ->where(function ($query) use ($operacion, $estatus, $codigo) {
          if ($estatus != null) {
              $query->where('estatus', $estatus);
          }
          if ($codigo != null) {
              $query->where('codigo', $codigo);
          }
          if ($operacion != null) {
              $query->where('tipo_operacion_id', $operacion);
          }
      })
      ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_tipos_devoluciones.tipo_operacion_id')
      ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
      ->paginate();

      $operaciones = TipoOperacion::select('t_tipos_operaciones.id','t_tipos_operaciones.producto_id','t_tipos_operaciones.codigo','t_tipos_operaciones.nombre','t_tipos_operaciones.direccion','t_tipos_operaciones.estatus','t_productos.nombre' )
      ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
      ->where('t_tipos_operaciones.estatus', 'ACTIVO')
      ->where('t_tipos_operaciones.direccion', 'D')
      ->orderBy('t_tipos_operaciones.id')
      ->pluck('t_productos.nombre', 't_tipos_operaciones.id')
      ->prepend('*', '');


      $codigos = TipoDevolucion::select('codigo')
      ->selectRaw("CONCAT(codigo, CONCAT(' - ', nombre)) nombre")
      ->orderBy('codigo')
      ->pluck('nombre', 'codigo')
      ->prepend('*', '');

      return View('configuracion.medios_pagos.razones_devoluciones.co_rd_01_i_inicio')
      ->with('operacion', $operacion)
      ->with('operaciones', $operaciones)
      ->with('estatus', $estatus)
      ->with('codigo', $codigo)
      ->with('codigos', $codigos)
      ->with('tipos_devoluciones', $tipos_devoluciones);
  }
  public function autocomplete()
  {

    $queries = TipoDevolucion::where(function($query)
    {
      $term = Input::get('term');
      $query->where('nombre', 'like', '%'.$term.'%');
    })->take(6)->get();
    foreach ($queries as $query)
    {
      $results[] = [ 'id' => $query->id, 'value' => $query->nombre];
    }
    return Response::json($results);
  }
  public function autocompleteCodigo()
  {

    $queries = TipoDevolucion::where(function($query)
    {
      $term = Input::get('term');
      $query->where('codigo', 'like', '%'.$term.'%');
    })->take(6)->get();
    foreach ($queries as $query)
    {
      $results[] = [ 'id' => $query->id, 'value' => $query->codigo];
    }
    return Response::json($results);
  }

  public function create()
  {
    $tipos_operaciones = TipoOperacion::select('t_tipos_operaciones.id','t_tipos_operaciones.producto_id','t_tipos_operaciones.codigo','t_tipos_operaciones.nombre','t_tipos_operaciones.direccion','t_tipos_operaciones.estatus','t_productos.nombre', DB::raw("CONCAT(CONCAT(t_tipos_operaciones.codigo,' - '),t_productos.nombre) AS nombre_completo"))
      ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
      ->where('t_tipos_operaciones.estatus', 'ACTIVO')
      ->where('t_tipos_operaciones.direccion', 'D')
      ->orderBy('t_tipos_operaciones.codigo')
      ->pluck('nombre_completo', 't_tipos_operaciones.id')
      ->prepend('Seleccione Tipo producto', 0);

     // dd($tipos_operaciones,$operaciones);

    return View('configuracion.medios_pagos.razones_devoluciones.co_rd_02_c_crear')
    ->with('tipos_operaciones', $tipos_operaciones);
  }

  public function store(TipoDevolucionCreateRequest $request)
  {
    
    $razon  = new TipoDevolucion ();
    $razon->tipo_operacion_id = $request->get('tipo_operacion_id');
    $razon->codigo = $request->get('codigo');
    $producto_id = TipoOperacion::where('id',$request->get('tipo_operacion_id'))->first()->producto_id;
    $razon->nombre = $request->get('nombre');
    $razon->estatus = $request->get('estatus');
    $razon->producto_id = $producto_id;
    $result = $razon->save();

    Alert::success('!','Razón de Devolución creado');

    return redirect()->route('RazonesDevolucion.index');
  }

  public function show($id)
  {
    $tipo_devolucion = TipoDevolucion::
    select('t_tipos_devoluciones.*','t_tipos_operaciones.nombre as tipo_operacion')
    ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_tipos_devoluciones.tipo_operacion_id')
    ->FindOrFail($id);
    return response()->json($tipo_devolucion); /*Devuelve un array con los datos del registro*/
  }

  public function edit($id)
  {
    $tipos_operaciones = TipoOperacion::select('t_tipos_operaciones.id','t_tipos_operaciones.producto_id','t_tipos_operaciones.codigo','t_tipos_operaciones.nombre','t_tipos_operaciones.direccion','t_tipos_operaciones.estatus','t_productos.nombre', DB::raw("CONCAT(CONCAT(t_tipos_operaciones.codigo,' - '),t_productos.nombre) AS nombre_completo"))
      ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
      ->where('t_tipos_operaciones.estatus', 'ACTIVO')
      ->where('t_tipos_operaciones.direccion', 'D')
      ->orderBy('t_tipos_operaciones.codigo')
      ->pluck('nombre_completo', 't_tipos_operaciones.id')
      ->prepend('Seleccione Tipo producto', 0);

    $tipoDevolucion = TipoDevolucion::
    select('t_tipos_devoluciones.*','t_tipos_operaciones.nombre as tipo_operacion')
    ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_tipos_devoluciones.tipo_operacion_id')
    ->find($id);

    return View('configuracion.medios_pagos.razones_devoluciones.co_rd_03_e_editar')
    ->with('tipoDevolucion', $tipoDevolucion)
    ->with('tipos_operaciones', $tipos_operaciones);
  }

  public function update(TipoDevolucionUpdateRequest $request, $id)
  {
    $tipo_devolucion = TipoDevolucion::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
    $result = $tipo_devolucion->fill($input)->save(); /*Se guarda la información*/

    Alert::success('!','Razón de Devolución actualizado');

    return redirect()->route('RazonesDevolucion.index');
  }

  public function destroy($id)
  {
    $tipo_devolucion = TipoDevolucion::FindOrFail($id);
    $result = $tipo_devolucion->delete();

    if ($result)
    {
      return response()->json(['success'=>'true']);
    }
    else
    {
      return response()->json(['success'=> 'false']);
    }
  }
}
