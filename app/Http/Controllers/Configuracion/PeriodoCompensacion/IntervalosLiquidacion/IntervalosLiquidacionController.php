<?php

namespace App\Http\Controllers\Configuracion\PeriodoCompensacion\IntervalosLiquidacion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\SesionCompensacion\Periodo;
use App\Models\Configuracion\SesionCompensacion\IntervaloLiquidacion;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Http\Requests\Configuracion\SesionCompensacion\IntervaloLiquidacion\IntervaloLiquidacionCreateRequest;
use App\Http\Requests\Configuracion\SesionCompensacion\IntervaloLiquidacion\IntervaloLiquidacionUpdateRequest;
use Validator;
use Session;
use Log;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use App\Services\Helpers\HelperService;

class IntervalosLiquidacionController extends Controller
{

  protected $helperService;
  protected $rangosHora;

  public function __construct(HelperService $helperService)
  {
      $this->helperService = $helperService;
      $this->rangosHora = $this->helperService->generateHoras(1);
  }

  public function getIndex()
  {
    $intervalos_liquidacion = IntervaloLiquidacion::
    select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
    ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
    ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
    ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
    ->orderBy('t_intervalos_liquidacion.id', 'asc')
    ->get();

    //20181107. información general para las graficas
    $operaciones = $this->generateInfoOperacionesJson($intervalos_liquidacion,$this->rangosHora);

    return View('configuracion.periodo_compensacion.intervalos_liquidacion.co_pc_01_i_intervalosliquidacion')
    ->with('intervalos_liquidacion', $intervalos_liquidacion)
    ->with('rangos_hora', $this->rangosHora)
    ->with('operaciones', $operaciones);
  }

  //para buscar la posicion de la hora en el arreglo de horas
  private function findHora($hora,$rangos_hora){
    $hora_pos = null;
    for ($i=0; $i < count($rangos_hora); $i++) {
      if($rangos_hora[$i] == $hora){
        $hora_pos = $i;
        break;
      }
    }
    return $hora_pos == null ? 0 : $hora_pos;
  }

  //permite generar el objeto json para las graficas referentes a las franjas de operaciones
  private function generateInfoOperacionesJson($intervalos_liquidacion,$rangos_hora){
    $operaciones[] = null;
    $franjas_operaciones[] = null;
    $producto_id = '';
    $NombreOperacion = '';
    $i = 0; //para recorrer el objeto de franjas
    $j = 0; //para recorrer el objeto de operaciones
    foreach ($intervalos_liquidacion as $registro) {
      if(isset($producto_id) && $producto_id != $registro['producto_id']){
        //si se comienza con una nueva operacion
        if(isset($franjas_operaciones[0])){ //se guarda lo ya recorrido con otras operaciones
          $operaciones[$j] = new \stdClass();
          $operaciones[$j]->descripcion = $NombreOperacion->nombre_completo;
          $operaciones[$j]->datos = json_encode($franjas_operaciones); //se completa la informacion con datos en json
          $franjas_operaciones = null;
          $i = 0;
          $j++;
        }

        $producto_id = $registro['producto_id'];
        //se busca la informacion de la operacion para complementar el grafico
        $NombreOperacion = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
        ->where('estatus','ACTIVO')
        ->where('producto_id',$registro['producto_id'])
        ->orderBy('codigo', 'asc')
        ->first();
        if($i == 0){ //si se esta comenzando
          $franjas_operaciones[$i] = new \stdClass();
          $franjas_operaciones[$i]->x = count($rangos_hora)-1; //cantidad de posiciones que contiene el arreglo de horas
          $franjas_operaciones[$i]->x2 = count($rangos_hora)-1; //cantidad de posiciones que contiene el arreglo de horas
          $franjas_operaciones[$i]->y = 0; //base para indicar el intervalo de liquidacion completo
          $franjas_operaciones[$i]->partialFill = 1; //se rellena este campo para indicar que el grafico debe abarcar completamente el rango de horas
          $i++;
          //se busca la informacion del periodo
          $periodo = Periodo::
            select('t_periodos.*','t_productos.nombre as producto')
            ->join('t_productos','t_productos.id','=','t_periodos.producto_id')
            ->where('t_productos.id',$registro['producto_id'])
            ->first();
          //descripcion de la informacion del intervalo
          $franjas_operaciones[$i] = new \stdClass();
          $franjas_operaciones[$i]->x = $this->findHora($periodo['hora_inicio'],$rangos_hora);
          $franjas_operaciones[$i]->x2 = $this->findHora($periodo['hora_fin'],$rangos_hora);
          $franjas_operaciones[$i]->y = 0; //posicion en el grafico para el intervalo de liquidacion completo
          $i++;
        }
      }
      if($producto_id == $registro['producto_id']){ //si se sigue en el mismo producto, se acumulan las franjas
        $franjas_operaciones[$i] = new \stdClass();
        $franjas_operaciones[$i]->x = $this->findHora(substr($registro['hora_inicio'], 0, 5),$rangos_hora);
        $franjas_operaciones[$i]->x2 = $this->findHora(substr($registro['hora_fin'], 0, 5),$rangos_hora);
        $franjas_operaciones[$i]->y = intVal($registro['num_franja']);
        $i++;
      }
    }
    //para la ultima operacion
    $operaciones[$j] = new \stdClass();
    $operaciones[$j]->descripcion = $NombreOperacion->nombre_completo;
    $operaciones[$j]->datos = json_encode($franjas_operaciones); //se completa la informacion con datos en json

    return $operaciones;
  }

  public function create()
  {
    return View('configuracion.periodo_compensacion.intervalos_liquidacion.create');
  }

  public function store(IntervaloLiquidacionCreateRequest $request)
  {

  }

  public function show($id)
  {
    $intervalo_liquidacion = IntervaloLiquidacion::
    select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion')
    ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
    ->FindOrFail($id);
    return response()->json($intervalo_liquidacion); /*Devuelve un array con los datos del registro*/
  }

  public function edit($id)
  {

    $intervalos_liquidacion = IntervaloLiquidacion::
    where('periodo_id', $id)
    ->orderBy('id', 'asc')
    ->get();

    $periodo = Periodo::
    select('t_periodos.*','t_productos.nombre as producto')
    ->join('t_productos','t_productos.id','=','t_periodos.producto_id')
    ->find($id);

    //20181107. información general para las graficas
    $operaciones = $this->generateInfoOperacionesJson($intervalos_liquidacion,$this->rangosHora);

    $tipos_operaciones = TipoOperacion::select('id', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))->where('estatus','ACTIVO')->orderBy('codigo', 'asc')->pluck('nombre_completo','id')->prepend('Seleccione Tipo de Operación', 0);

    return view('configuracion.periodo_compensacion.intervalos_liquidacion.co_pc_02_e_editarintervalo',
      array(
        'intervalos_liquidacion'=>$intervalos_liquidacion,
        'tipos_operaciones'=>$tipos_operaciones,
        'periodo'=>$periodo,
        'rangos_hora' => $this->rangosHora,
        'operaciones' => $operaciones
      )
    );
  }

  public function update(IntervaloLiquidacionUpdateRequest $request, $id)
  {

    $parametros = ParametroSistema::whereIn('id', [39, 2])
        ->orderBy('id', 'DESC')
        ->get(['valor']);


    $periodo = Periodo::
    select('t_periodos.*','t_productos.nombre as producto')
    ->join('t_productos','t_productos.id','=','t_periodos.producto_id')
    ->find($id);                                                          // busca el periodo completo para el tipo de producto

    $intervalos_liquidacion = IntervaloLiquidacion::
    where('producto_id', $periodo->producto_id)
    ->where('periodo_id', $periodo->id)
    ->orderBy('id', 'asc')
    ->get();

//variables del periodo general
  $hora_fin_periodo_general=0;
  $hora_inicio_periodo_general=0;
  $dia_inicio_periodo_general=0;
  $dia_fin_periodo_general=0;
  (trim($periodo->dia_inicio) == 'D-1') ? $dia_inicio_periodo_general=$parametros[0]->valor : $dia_inicio_periodo_general=$parametros[1]->valor;
  (trim($periodo->dia_fin) == 'D-1') ? $dia_fin_periodo_general=$parametros[0]->valor : $dia_fin_periodo_general=$parametros[1]->valor;
  $periodo_general_inicio=Carbon::parse($dia_inicio_periodo_general.str_replace ( ':' , '' ,$periodo->hora_inicio));
  $periodo_general_fin=Carbon::parse($dia_fin_periodo_general.str_replace ( ':' , '' ,$periodo->hora_fin));
// fin variables del periodo general

  $tiempoMinimoDeFranja = ParametroSistema::find(4);   // busca en t_parametro de sistema el id (4) el mismo contiene el tiempo minimo en q puede terminar la franja de presentados y la de devueltas
  $tiempoDiferenciaDeFranjas = ParametroSistema::find(3)->valor;  // tiempo de diferencia entre franjas
// validaciones de campos requeridos
//dd($request->all());
    $rules = [
      'dia_inicio.*'=> 'required',
      'hora_inicio.*'=> 'required',
      'dia_fin.*'=> 'required',
      'hora_fin.*'=> 'required',
      'num_liquidacion.*'=> 'required',
      'num_cmpsa.*'=> 'required|unique_num_cmpsa:'.implode(',', $request->input('intervalo_liquidacion')),
    ];

    $messages = [];

    foreach($request->input('dia_inicio') as $key => $val)
    {
      $messages['dia_inicio.'.$key.'.required'] = 'El Dia de Inicio de la Fila '.$key.' es requerido.';
    }

    foreach($request->input('hora_inicio') as $key => $val)
    {
      $messages['hora_inicio.'.$key.'.required'] = 'La Hora de Inicio de la Fila '.$key.' es requerida.';
    }

    foreach($request->input('dia_fin') as $key => $val)
    {
      $messages['dia_fin.'.$key.'.required'] = 'El Dia de Inicio de la Fila '.$key.' es requerido.';
    }

    foreach($request->input('hora_fin') as $key => $val)
    {
      $messages['hora_fin.'.$key.'.required'] = 'La Hora de Fin de la Fila '.$key.' es requerida.';
    }

    foreach($request->input('num_liquidacion') as $key => $val)
    {
      $messages['num_liquidacion.'.$key.'.required'] = 'El número de liquidación '.$key.' es requerido.';
    }

    foreach($request->input('num_cmpsa') as $key => $val)
    {
      $messages['num_cmpsa.'.$key.'.required'] = 'El orden CMPSA '.$key.' es requerido.';
      $messages['num_cmpsa.'.$key.'.unique_num_cmpsa'] = 'El orden CMPSA '.$key.' ya existe.';
    }

    // Validación para que el num_cmpsa sea único.
    Validator::extend('unique_num_cmpsa', function ($attribute, $value, $parameters) {
        // Se obtiene el número de fila del campo
        $fila = explode('.', $attribute)[1];

        // Se obtiene el id del intérvalo al que corresponde esta fila
        $id = $parameters[$fila];

        // Se consulta si el num_cmpsa ya está siendo utilizado por otro id que no sea el actual.
        $intervalo = IntervaloLiquidacion::select('id')
        ->where('num_cmpsa', $value)
        ->where('id', '<>', $id)
        ->first();

        // Si se encuentra algún otro intérvalo que contenga el mismo num_cmpsa se devuelve false (que la validación no pasa)
        // De lo contrario se devuelve true (pasa la validacion)
        if (!is_null($intervalo)) {
          return false;
        }
        return true;
    });

    $validator = Validator::make($request->all(), $rules, $messages);
    // fin  validaciones de campos requeridos

    // verifica si el parametro de tiempo minimo de franja esta configurado

    if(is_null($tiempoMinimoDeFranja)) {
      $validator->getMessageBag()->add('intervaloliquidacionConfigurarTiempoMinimoDeFranja', 'El Tiempo Mínimo de Franja no esta configurado. Verifique que este se encuentre en Parámetros del Sistema.');
      return back()
      ->withInput()
      ->withErrors($validator);
    }
// fin verifica si el parametro de tiempo minimo de franja esta configurado

// variables de las nuevas horas a configurar para la franja de presentadas y devueltas

    $dias_inicio = $request->input('dia_inicio');
    $horas_inicio = $request->input('hora_inicio');
    $dias_fin = $request->input('dia_fin');
    $horas_fin = $request->input('hora_fin');
    // $dia_inicio_presentada='0';
    // $dia_inicio_devuelta='0';
    // $dia_fin_presentada='0';
    // $dia_fin_devuelta='0';
    $dia_inicio_presentada=[];
    $dia_inicio_devuelta=[];
    $dia_fin_presentada=[];
    $dia_fin_devuelta=[];
    //20181105. adaptacion a multiples liquidaciones - presentadas
    for ($h=0; $h < count($dias_inicio); $h++) {
      (trim($dias_inicio[$h]) == 'D-1') ? $dia_inicio_presentada[]=$parametros[0]->valor : $dia_inicio_presentada[]=$parametros[1]->valor; $h++;
    }
    for ($h=1; $h < count($dias_inicio); $h++) {
      (trim($dias_inicio[$h]) == 'D-1') ? $dia_inicio_devuelta[]=$parametros[0]->valor : $dia_inicio_devuelta[]=$parametros[1]->valor; $h++;
    }
    // (trim($dias_inicio[0]) == 'D-1') ? $dia_inicio_presentada=$parametros[0]->valor : $dia_inicio_presentada=$parametros[1]->valor;
    // (trim($dias_inicio[1]) == 'D-1') ? $dia_inicio_devuelta=$parametros[0]->valor : $dia_inicio_devuelta=$parametros[1]->valor;
    //20181105. adaptacion a multiples liquidaciones - devueltas
    for ($h=0; $h < count($dias_fin); $h++) {
      (trim($dias_fin[$h]) == 'D-1') ? $dia_fin_presentada[]=$parametros[0]->valor : $dia_fin_presentada[]=$parametros[1]->valor; $h++;
    }
    for ($h=1; $h < count($dias_fin); $h++) {
      (trim($dias_fin[$h]) == 'D-1') ? $dia_fin_devuelta[]=$parametros[0]->valor : $dia_fin_devuelta[]=$parametros[1]->valor; $h++;
    }
    // (trim($dias_fin[0]) == 'D-1') ? $dia_fin_presentada=$parametros[0]->valor : $dia_fin_presentada=$parametros[1]->valor;
    // (trim($dias_fin[1]) == 'D-1') ? $dia_fin_devuelta=$parametros[0]->valor : $dia_fin_devuelta=$parametros[1]->valor;
    //20181105. adaptacion a multiples liquidaciones - presentadas
    for ($h=0; $h < count($dias_inicio); $h++) {
      $hora_inicio_presentada[]=str_replace ( ':' , '' , $horas_inicio[$h]); $h++;
    }
    // $hora_inicio_presentada=str_replace ( ':' , '' , $horas_inicio[0]);
    //20181105. adaptacion a multiples liquidaciones - devueltas
    for ($h=1; $h < count($dias_inicio); $h++) {
      $hora_inicio_devuelta[]=str_replace ( ':' , '' , $horas_inicio[$h]); $h++;
    }
    // $hora_inicio_devuelta=str_replace ( ':' , '' , $horas_inicio[1]);
    //20181105. adaptacion a multiples liquidaciones - presentadas
    for ($h=0; $h < count($dias_fin); $h++) {
      $hora_fin_presentada[]=str_replace ( ':' , '' , $horas_fin[$h]); $h++;
    }
    // $hora_fin_presentada=str_replace ( ':' , '' , $horas_fin[0]);
    //20181105. adaptacion a multiples liquidaciones - devueltas
    for ($h=1; $h < count($dias_fin); $h++) {
      $hora_fin_devuelta[]=str_replace ( ':' , '' , $horas_fin[$h]); $h++;
    }
    // $hora_fin_devuelta=str_replace ( ':' , '' , $horas_fin[1]);
    //20181105. adaptacion a multiples liquidaciones - presentadas
    for ($h=0; $h < count($dia_inicio_presentada); $h++) {
      $periodo_inicio_presentada[]=Carbon::parse($dia_inicio_presentada[$h].$hora_inicio_presentada[$h]);
    }
    // $periodo_inicio_presentada=Carbon::parse($dia_inicio_presentada.$hora_inicio_presentada);
    //20181105. adaptacion a multiples liquidaciones - devueltas
    for ($h=0; $h < count($dia_inicio_devuelta); $h++) {
      $periodo_inicio_devuelta[]=Carbon::parse($dia_inicio_devuelta[$h].$hora_inicio_devuelta[$h]);
    }
    // $periodo_inicio_devuelta=Carbon::parse($dia_inicio_devuelta.$hora_inicio_devuelta);
    //20181105. adaptacion a multiples liquidaciones - presentadas
    for ($h=0; $h < count($dia_fin_presentada); $h++) {
      $periodo_fin_presentada[]=Carbon::parse($dia_fin_presentada[$h].$hora_fin_presentada[$h]);
    }
    // $periodo_fin_presentada = Carbon::parse($dia_fin_presentada.$hora_fin_presentada);
    //20181105. adaptacion a multiples liquidaciones - devueltas
    for ($h=0; $h < count($dia_fin_devuelta); $h++) {
      $periodo_fin_devuelta[]=Carbon::parse($dia_fin_devuelta[$h].$hora_fin_devuelta[$h]);
    }
    // $periodo_fin_devuelta=Carbon::parse($dia_fin_devuelta.$hora_fin_devuelta);
    $num_liquidacion = $request->input('num_liquidacion');
    $num_liquidacion_new = [];
    //al obtener los numeros de liq, se obtiene segun la cantidad de registros, cada dos franjas contiene el mismo
    for($j=0; $j < count($num_liquidacion); $j++){ //se debe eliminar la duplicidad y tomar valores unicos
      if(!in_array($num_liquidacion[$j],$num_liquidacion_new)){ //si ya se guardo un numero, no se vuelve a guardar
        $num_liquidacion_new[] = $num_liquidacion[$j];
      }
    }
    $num_liquidacion = $num_liquidacion_new;

    $num_cmpsa = $request->input('num_cmpsa');
    $num_cmpsa_new = [];
    //al obtener los numeros de liq, se obtiene segun la cantidad de registros, cada dos franjas contiene el mismo
    for($j=0; $j < count($num_cmpsa); $j++){ //se debe eliminar la duplicidad y tomar valores unicos
      if(!in_array($num_cmpsa[$j],$num_cmpsa_new)){ //si ya se guardo un numero, no se vuelve a guardar
        $num_cmpsa_new[] = $num_cmpsa[$j];
      }
    }
    $num_cmpsa = $num_cmpsa_new;

    // VALIDACIONES CON EL PERIODO GENERAL. 20181105. adaptacion a multiples liquidaciones
    for($h=0; $h < count($periodo_inicio_presentada); $h++){ //se toma en cuenta que por cada liquidacion dentro del periodo se hacen las validaciones

      if($periodo_inicio_presentada[$h]<$periodo_general_inicio) {
        $validator->getMessageBag()->add('periododeiniciomayoralconfiguradopresentadas', 'El inicio de la franja de presentadas no puede ser menor al inicio del período del producto'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if($periodo_inicio_devuelta[$h]<$periodo_general_inicio) {
        $validator->getMessageBag()->add('periododeiniciomayoralconfiguradodevueltas', 'El inicio de la franja de devueltas no puede ser menor al inicio del período del producto'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if($periodo_fin_presentada[$h]>$periodo_general_fin) {
        $validator->getMessageBag()->add('periodofinmayoralconfiguradopresentadas', 'El fin de la franja de presentadas no puede ser mayor al fin del período del producto'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if($periodo_fin_devuelta[$h]>$periodo_general_fin) {
        $validator->getMessageBag()->add('periodofinmayoralconfiguradodevueltas', 'El fin de la franja de devueltas no puede ser mayor al fin del período del producto'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if($periodo_fin_presentada[$h]>$periodo_general_fin->copy()->subMinutes($tiempoDiferenciaDeFranjas)) {
        $validator->getMessageBag()->add('periodofinmayoralconfiguradopresentadas', 'El fin de la franja de presentadas no puede ser mayor al fin del período del producto menos '.$tiempoDiferenciaDeFranjas.' minutos'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      // FIN VALIDACIONES CON EL PERIODO GENERAL
      if($periodo_inicio_presentada[$h]->diffInMinutes($periodo_fin_presentada[$h]) < $tiempoMinimoDeFranja->valor) {
        $validator->getMessageBag()->add('periododeiniciomayoralperiodofinpresentadas', 'La franja de presentadas debe tener un tiempo de duración de al menos '.$tiempoMinimoDeFranja->valor.' minutos'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }
      if($periodo_inicio_devuelta[$h]->diffInMinutes($periodo_fin_devuelta[$h]) < $tiempoMinimoDeFranja->valor) {
        $validator->getMessageBag()->add('periododeiniciomayoralperiodofindevueltas', 'La franja de devueltas debe tener un tiempo de duración de al menos '.$tiempoMinimoDeFranja->valor.' minutos'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      //se valida que el inicio de presentada no se encuentre luego del inicio de devuelta
      if($periodo_inicio_presentada[$h]>$periodo_inicio_devuelta[$h]) {
        $validator->getMessageBag()->add('periododeiniciomayoralperiodoiniciodevuelta', 'El inicio de la franja de presentadas no puede ser mayor al inicio de la franja de devueltas'./*, para la liquidación Nro.'.$num_liquidacion[$h]*/'.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if($periodo_fin_devuelta[$h] < $periodo_fin_presentada[$h]->copy()->addMinutes($tiempoDiferenciaDeFranjas)) {
        $validator->getMessageBag()->add('tiempoentrecuelminacionculminaciondefranjas', 'El tiempo mínimo entre culminación de franjas debe ser de '.$tiempoDiferenciaDeFranjas.' minutos.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      //20181105. validaciones de liquidacion. si existe liquidacion siguiente
      /*if(isset($periodo_fin_devuelta[$h+1])){
        //el final de la franja dos para una liquidacion actual debe ser menor al final de la franja dos para una liquidacion siguiente
        if($periodo_fin_devuelta[$h]>$periodo_fin_devuelta[$h+1]) {
          $validator->getMessageBag()->add('periodofinliqamayoralperiodofinliqs', 'El período fin configurado en la operación devuelta para la liquidación Nro.'.$num_liquidacion[$h].' no puede ser mayor al período fin configurado en la operación devuelta para la liquidación Nro.'.($num_liquidacion[$h]+1).' .');
          return back()
          ->withInput()
          ->withErrors($validator);
        }
        //la diferencia entre las dos liquidaciones debe ser lo indicado por el parametro
        if($periodo_fin_devuelta[$h+1]->diffInMinutes($periodo_fin_devuelta[$h])<$tiempoDiferenciaDeFranjas) {
          $validator->getMessageBag()->add('periodofinliqadifalperiodofinliqs', 'El período fin configurado en la operación devuelta para la liquidación Nro.'.$num_liquidacion[$h].
            ' no puede tener una diferencia menor a los '.$tiempoDiferenciaDeFranjas.' minutos con respecto al período fin configurado en la operación devuelta
             para la liquidación Nro.'.($h+2).'.');
          return back()
          ->withInput()
          ->withErrors($validator);
        }

        //el inicio de la franja uno para una liquidacion siguiente debe ser mayor o igual al fin de la franja uno para una liquidacion anterior
        if($periodo_inicio_presentada[$h+1]<$periodo_fin_presentada[$h] && $intervalos_liquidacion[$h]->producto_id == $periodos->producto_id) {
          $validator->getMessageBag()->add('periodoiniliqsmenoralperiodofinliqa', 'El período de inicio configurado en la operación presentada para la liquidación Nro.'.($num_liquidacion[$h]+1).' no puede ser menor al período fin configurado en la operación presentada para la liquidación Nro.'.$num_liquidacion[$h].' .');
          return back()
          ->withInput()
          ->withErrors($validator);
        }
      }*/

    }

    if ($validator->fails()) {
      return back()
      ->withInput()
      ->withErrors($validator);
    } else {

      $dia_inicio = $request->input('dia_inicio');
      $hora_inicio = $request->input('hora_inicio');
      $dia_fin = $request->input('dia_fin');
      $hora_fin = $request->input('hora_fin');
      $num_liquidacion = $request->input('num_liquidacion');
      $num_cmpsa = $request->input('num_cmpsa');
      $num_franja = $request->input('num_franja');

      foreach($request->input('intervalo_liquidacion') as $key => $intervalo_liquidacion_id) {
        $intervalo_liquidacion = IntervaloLiquidacion::FindOrFail($intervalo_liquidacion_id);
        $input['dia_inicio'] = $dia_inicio[$key];
        $input['hora_inicio'] = substr($hora_inicio[$key],0,5).':01';
        $input['dia_fin'] = $dia_fin[$key];
        $input['hora_fin'] = substr($hora_fin[$key],0,5).':00';
        $input['num_liquidacion'] = $num_liquidacion[$key];
        $input['num_cmpsa'] = $num_cmpsa[$key];
        $result = $intervalo_liquidacion->fill($input)->save();
      }

       notify()->flash('Intervalo de liquidación actualizado satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

      return redirect()->route('IntervalosLiquidacion');
    }
  }

  public function destroy($id)
  {
    $intervalo_liquidacion = IntervaloLiquidacion::FindOrFail($id);
    $result = $intervalo_liquidacion->delete();

    if ($result)
    {
      return response()->json(['success'=>'true']);
    }
    else
    {
      return response()->json(['success'=> 'false']);
    }
  }
}
