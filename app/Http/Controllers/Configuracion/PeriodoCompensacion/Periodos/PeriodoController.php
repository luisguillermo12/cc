<?php

namespace App\Http\Controllers\Configuracion\PeriodoCompensacion\Periodos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\SesionCompensacion\Periodo;
use App\Models\Configuracion\SesionCompensacion\IntervaloLiquidacion;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Http\Requests\Configuracion\SesionCompensacion\Periodo\PeriodoCreateRequest;
use App\Http\Requests\Configuracion\SesionCompensacion\Periodo\PeriodoUpdateRequest;
use Validator;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;

class PeriodoController extends Controller
{
  public function getIndex()
  {
    $periodos = Periodo::select('t_periodos.*', 't_productos.nombre as producto')
    ->join('t_productos','t_productos.id','=','t_periodos.producto_id')
    ->join('t_intervalos_liquidacion', 't_intervalos_liquidacion.periodo_id', '=', 't_periodos.id')
    ->orderBy('t_periodos.id', 'ASC')
    ->distinct('t_periodos.nombre')
    ->get();

    return View('configuracion.periodo_compensacion.periodos.co_pc_01_i_periodos')
    ->with('periodos', $periodos);
  }

  public function create()
  {
    $productos = Producto::select('id', 'nombre')
    ->where('estatus','ACTIVO')
    ->orderBy('codigo', 'asc')
    ->pluck('nombre','id')
    ->prepend('Seleccione Producto', 0);

    return View('configuracion.periodo_compensacion.periodos.co_pc_02_c_crearperiodo')
    ->with('productos', $productos);
  }

  public function store(PeriodoCreateRequest $request)
  {
    DB::beginTransaction();

    try {

      $rules = [
        'producto_id'=> 'not_in:0|unique:t_periodos,producto_id',
        'dia_inicio'=> 'not_in:0',
        'hora_inicio'=> 'required',
       // 'dia_fin'=> 'not_in:0',
        'hora_fin'=> 'required',
        'num_liquidacion'=> 'required',
      ];

      $messages = [
        'producto_id.not_in' => 'El Producto es requerido.',
        'producto_id.unique' => 'El período de compensación para el producto elegido ya se encuentra registrado.',
        'dia_inicio.not_in' => 'El Día de Inicio es requerido.',
        'hora_inicio.required' => 'La Hora de Inicio es requerida.',
        //'dia_fin.not_in' => 'El Día de Fin es requerido.',
        'hora_fin.required' => 'La Hora de Fin es requerida.',
        'num_liquidacion.required' => 'El Número de Intervalos de Liquidación es requerido.',
      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      $hora_inicio = explode(":" , $request->input('hora_inicio'));
      $hora_fin = explode(":" , $request->input('hora_fin'));

      $hora_inicio[0] = (int) $hora_inicio[0];
      $hora_inicio[1] = (int) $hora_inicio[1];

      $hora_inicio_min = ($hora_inicio[0] * 60) + $hora_inicio[1];
      $hora_fin_min = ($hora_fin[0] * 60) + $hora_fin[1];

      if($request->input('dia_inicio') == 'D') {
        $dia_fin_aux = 'D';
      } else {
        $dia_fin_aux = $request->input('dia_fin');
      }

      if(($hora_fin_min < $hora_inicio_min) && ($request->input('dia_inicio') == $dia_fin_aux)) {
        $validator->getMessageBag()->add('intervaloliquidacionHoraInicioMenor', 'La Hora de Fin no puede ser menor a la Hora de Inicio.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      $tipos_operaciones = TipoOperacion::select('t_tipos_operaciones.*')
      ->where(
      [
        ['t_tipos_operaciones.producto_id', '=', $request->input('producto_id') ],
        ['t_tipos_operaciones.estatus', '=', 'ACTIVO'],
      ])
      ->orderBy('direccion', 'desc')
      ->get();

      if( count($tipos_operaciones) == 0 ) {
        $validator->getMessageBag()->add('periodoFaltaTipoOperacion', 'No se ha encontrado ningún tipo de operación configurada para el producto seleccionado. Por favor dirijase al Menú de Navegación -> Configuración -> Medios de Pagos -> Tipos de Operaciones. En esa opción podrá crear los tipos de operaciones necesarias para poder crear el período recuerda generar el tipo de operación <strong>Presentada</strong> como el tipo de operación <strong>Devuelta</strong> para el mismo producto.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      $presentada = false;
      $devuelta = false;

      foreach ($tipos_operaciones as $tipo_operacion) {
        if( $tipo_operacion->direccion== 'P' ) {
          $presentada = true;
        }

        if( $tipo_operacion->direccion== 'D' ) {
          $devuelta = true;
        }
      }

      if( !$presentada ) {
        $validator->getMessageBag()->add('periodoFaltaTipoOperacionP', 'No se ha encontrado ningún tipo de operación configurada como <strong>Presentada</strong>. Por favor dirijase al Menú de Navegación -> Configuración -> Medios de Pagos -> Tipos de Operaciones. En esa opción podrá crear los tipos de operaciones necesarias para poder crear el período recuerda generar el tipo de operación <strong>Presentada</strong> para el mismo producto.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if( !$devuelta ) {
        $validator->getMessageBag()->add('periodoFaltaDireccionTipoOperacionD', 'No se ha encontrado ningún tipo de operación configurada como <strong>Devuelta</strong>. Por favor dirijase al Menú de Navegación -> Configuración -> Medios de Pagos -> Tipos de Operaciones. En esa opción podrá crear los tipos de operaciones necesarias para poder crear el período recuerda generar el tipo de operación <strong>Devuelta</strong> para el mismo producto.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if ($validator->fails()) {
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      // Periodo
      $periodo = new Periodo();
      $periodo->producto_id = $request->input('producto_id');
      $periodo->dia_inicio = $request->input('dia_inicio');
      $periodo->hora_inicio = $request->input('hora_inicio');

      $periodo->dia_fin = $request->input('dia_fin');
      $periodo->hora_fin = $request->input('hora_fin');
      $periodo->num_liquidacion = (int) $request->input('num_liquidacion');
      $periodo->num_franja = ($periodo->num_liquidacion) * 2;
      $periodo->estatus = $request->input('estatus');
      $resultPeriodo = $periodo->save();

      if($periodo->dia_inicio == $periodo->dia_fin) {
        $intervaloLiquidacionDiaInicio = $periodo->dia_inicio;
        $intervaloLiquidacionDiaFin = $periodo->dia_fin;
      } else {
        $intervaloLiquidacionDiaInicio = '';
        $intervaloLiquidacionDiaFin = '';
      }

      // Intervalos De Liquidación
      for ($i = 1; $i <= $request->input('num_liquidacion'); $i++) {
        $contador = 1;
        foreach ($tipos_operaciones as $tipo_operacion) {
          $intervaloLiquidacion = new IntervaloLiquidacion();
          $intervaloLiquidacion->tipo_operacion_id = $tipo_operacion->id;
          $intervaloLiquidacion->dia_inicio = $intervaloLiquidacionDiaInicio;
          $intervaloLiquidacion->dia_fin = $intervaloLiquidacionDiaFin;
          $intervaloLiquidacion->num_liquidacion = $i;
          $intervaloLiquidacion->producto_id = $request->input('producto_id');
          $intervaloLiquidacion->num_franja = $contador;
          $resultIntervaloLiquidacion = $periodo->intervalo_liquidacion()->save($intervaloLiquidacion);
          $contador++;
        }
      }

      DB::commit();


      notify()->flash('Período creado correctamente', 'success', [
        'timer' => 3000,
        'text' => ' '
      ]);

      return redirect()->route('Periodos');

    } catch (QueryException $ex){
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }
  public function show($id)
  {
    $periodo = Periodo::
    select('t_periodos.*','t_productos.nombre as producto')
    ->join('t_productos','t_productos.id','=','t_periodos.producto_id')
    ->FindOrFail($id);
    return response()->json($periodo); /*Devuelve un array con los datos del registro*/
  }

  public function edit($id)
  {
    $periodo = Periodo::find($id);

    $productos = Producto::select('id', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
    ->where('estatus','ACTIVO')
    ->where('id',$periodo->producto_id)
    ->orderBy('codigo', 'asc')
    ->first();

    return view('configuracion.periodo_compensacion.periodos.co_pc_03_e_editarperiodo', array('periodo'=>$periodo, 'productos'=>$productos));
  }

  public function update(PeriodoUpdateRequest $request, $id)
  {

    DB::beginTransaction();

    try {

      $rules = [
        'producto_id'=> 'not_in:0',
        'dia_inicio'=> 'not_in:0',
        'hora_inicio'=> 'required',
        'dia_fin'=> 'not_in:0',
        'hora_fin'=> 'required',
        'num_liquidacion'=> 'required',
      ];

      $messages = [
        'producto_id.not_in' => 'El Producto es requerido.',
        'dia_inicio.not_in' => 'El Día de Inicio es requerido.',
        'hora_inicio.required' => 'La Hora de Inicio es requerida.',
        'dia_fin.not_in' => 'El Día de Fin es requerido.',
        'hora_fin.required' => 'La Hora de Fin es requerida.',
        'num_liquidacion.required' => 'El Número de Intervalos de Liquidación es requerido.',
      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      $hora_inicio = explode(":" , $request->input('hora_inicio'));
      $hora_fin = explode(":" , $request->input('hora_fin'));

      $hora_inicio[0] = (int) $hora_inicio[0];
      $hora_inicio[1] = (int) $hora_inicio[1];

      $hora_inicio_min = ($hora_inicio[0] * 60) + $hora_inicio[1];
      $hora_fin_min = ($hora_fin[0] * 60) + $hora_fin[1];

      if($request->input('dia_inicio') == 'D') {
        $dia_fin_aux = 'D';
      } else {
        $dia_fin_aux = $request->input('dia_fin');
      }

      if(($hora_fin_min < $hora_inicio_min) && ($request->input('dia_inicio') == $dia_fin_aux)) {
        $validator->getMessageBag()->add('sesionfranjaHoraInicioMenor', 'La Hora de Fin no puede ser menor a la Hora de Inicio.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      $tipos_operaciones = TipoOperacion::select('t_tipos_operaciones.*')
      ->where(
      [
        ['t_tipos_operaciones.producto_id', '=', $request->input('producto_id') ],
        ['t_tipos_operaciones.estatus', '=', 'ACTIVO'],
      ])
      ->orderBy('direccion', 'desc')
      ->get();

      if( count($tipos_operaciones) == 0 ) {
        $validator->getMessageBag()->add('periodoFaltaTipoOperacion', 'No se ha encontrado ningún tipo de operación configurada para el producto seleccionado. Por favor dirijase al Menú de Navegación -> Configuración -> Medios de Pagos -> Tipos de Operaciones. En esa opción podrá crear los tipos de operaciones necesarias para poder crear el período recuerda generar el tipo de operación <strong>Presentada</strong> como el tipo de operación <strong>Devuelta</strong> para el mismo producto.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      $presentada = false;
      $devuelta = false;

      foreach ($tipos_operaciones as $tipo_operacion) {
        if( $tipo_operacion->direccion== 'P' ) {
          $presentada = true;
        }

        if( $tipo_operacion->direccion== 'D' ) {
          $devuelta = true;
        }
      }

      if( !$presentada ) {
        $validator->getMessageBag()->add('periodoFaltaTipoOperacionP', 'No se ha encontrado ningún tipo de operación configurada como <strong>Presentada</strong>. Por favor dirijase al Menú de Navegación -> Configuración -> Medios de Pagos -> Tipos de Operaciones. En esa opción podrá crear los tipos de operaciones necesarias para poder crear el período recuerda generar el tipo de operación <strong>Presentada</strong> para el mismo producto.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if( !$devuelta ) {
        $validator->getMessageBag()->add('periodoFaltaDireccionTipoOperacionD', 'No se ha encontrado ningún tipo de operación configurada como <strong>Devuelta</strong>. Por favor dirijase al Menú de Navegación -> Configuración -> Medios de Pagos -> Tipos de Operaciones. En esa opción podrá crear los tipos de operaciones necesarias para poder crear el período recuerda generar el tipo de operación <strong>Devuelta</strong> para el mismo producto.');
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      if ($validator->fails()) {
        return back()
        ->withInput()
        ->withErrors($validator);
      }

      // Periodo
      $periodo = Periodo::FindOrFail($id);
      $input['producto_id'] = $request->input('producto_id');
      $input['dia_inicio'] = $request->input('dia_inicio');
      $input['hora_inicio'] = $request->input('hora_inicio');
      $input['dia_fin'] = $request->input('dia_fin');
      $input['hora_fin'] = $request->input('hora_fin');
      $input['num_liquidacion'] = (int) $request->input('num_liquidacion');
      $input['num_franja'] = $request->input('num_liquidacion') * 2;
      $input['estatus'] = $request->input('estatus');
      $result = $periodo->fill($input)->save(); /*Se guarda la información*/

      $intervaloLiquidacion = IntervaloLiquidacion::where('periodo_id', $id);
      $resultIntervaloLiquidacion = $intervaloLiquidacion->delete();

      if($periodo->dia_inicio == $periodo->dia_fin) {
        $intervaloLiquidacionDiaInicio = $periodo->dia_inicio;
        $intervaloLiquidacionDiaFin = $periodo->dia_fin;
      } else {
        $intervaloLiquidacionDiaInicio = '';
        $intervaloLiquidacionDiaFin = '';
      }

      // Intervalos De Liquidación
      for ($i = 1; $i <= $request->input('num_liquidacion'); $i++) {
        $contador = 1;
        foreach ($tipos_operaciones as $tipo_operacion) {
          $intervaloLiquidacion = new IntervaloLiquidacion();
          $intervaloLiquidacion->tipo_operacion_id = $tipo_operacion->id;
          $intervaloLiquidacion->dia_inicio = $intervaloLiquidacionDiaInicio;
          $intervaloLiquidacion->dia_fin = $intervaloLiquidacionDiaFin;
          $intervaloLiquidacion->num_liquidacion = $i;
          $intervaloLiquidacion->num_franja = $contador;
          $intervaloLiquidacion->producto_id = $request->input('producto_id');
          $resultIntervaloLiquidacion = $periodo->intervalo_liquidacion()->save($intervaloLiquidacion);
          $contador++;
        }
      }

      DB::commit();

      notify()->flash('Período actualizado correctamente', 'success', [
        'timer' => 3000,
        'text' => ' '
      ]);

      return redirect()->route('Periodos');

    } catch (QueryException $ex){
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }

  public function destroy($id)
  {

    DB::beginTransaction();

    try {

      $periodo = Periodo::FindOrFail($id);
      $intervaloLiquidacion = IntervaloLiquidacion::where('periodo_id', $id);
      $resultIntervaloLiquidacion = $intervaloLiquidacion->delete();
      $resultPeriodo = $periodo->delete();

      if ($resultIntervaloLiquidacion && $resultPeriodo)
      {

        notify()->flash('Período eliminado correctamente', 'success', [
          'timer' => 3000,
          'text' => ' '
        ]);

        DB::commit();

        //return redirect()->route('Periodos');
        return response()->json(['success'=>'true']);

      }
      else
      {
        DB::rollback();

        //return redirect()->route('Periodos');
        return response()->json(['success'=> 'false']);
      }

    } catch (QueryException $ex){
      DB::rollback();
      throw new Exception($ex);
    } catch (PDOException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (FatalErrorException $ex) {
      DB::rollback();
      throw new Exception($ex);
    } catch (Exception $ex) {
      DB::rollback();
      throw new Exception($ex);
    }
  }
}
