<?php

namespace App\Http\Controllers\Configuracion\Participantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Banco\EstadoBanco;
use App\Models\Configuracion\Calendario\Calendario;
use App\Models\Configuracion\Calendario\DiasExcepcionales;
use App\Models\Configuracion\SemanaCompensacion\SemanaCompensacion;
use App\Models\Configuracion\Contacto\Contacto;
use App\Models\Configuracion\Contacto\ContactoBancario;
use App\Http\Requests\Configuracion\Participantes\ParticipanteCreateRequest;
use App\Http\Requests\Configuracion\Participantes\ParticipanteUpdateRequest;
use DB;
use Carbon\Carbon;

class ParticipantesController extends Controller
{
    public function inactivos()
    {
        $bancosInactivos = Banco::whereIn('estatus', ['INACTIVO', 'SUSPENDIDO','EXCLUIDO'])
        ->get();

        return view('configuracion.participantes.co_pa_01_i_inicio')
        ->with('seccion','Inactivos')
        ->with('bancos',$bancosInactivos);
    }

    public function activos()
    {

        $bancosActivos = Banco::where('estatus', 'ACTIVO')
        ->paginate();

        return view('configuracion.participantes.co_pa_01_i_inicio')
        ->with('seccion','Activos')
        ->with('bancos',$bancosActivos);
    }

    public function fusionados()
    {

        $bancosfusionados = Banco::where('estatus', 'FUSIONADO')
        ->get();

        return view('configuracion.participantes.co_pa_01_i_inicio')
        ->with('seccion','Fusionados')
        ->with('bancos',$bancosfusionados);
    }

    public function show($id)
    {
        $bancoMostrar = Banco::select('t_bancos.*','banco_representante.nombre as banco_representante','banco_principal.nombre as banco_principal')
        ->leftJoin('t_bancos as banco_representante','t_bancos.banco_representante_id','=','banco_representante.id')
        ->leftJoin('t_bancos as banco_principal','t_bancos.banco_principal_id','=','banco_principal.id')
        ->where('t_bancos.id','=', $id)
        ->first();

        $historico = EstadoBanco::select('t_historico_estado_banco.*')
        ->where('id_bancos',$id)
        ->first();

        if ($bancoMostrar->estatus == 'ACTIVO') {
            $seccion = 'Activos';
        } elseif ($bancoMostrar->estatus == 'FUSIONADO') {
            $seccion = 'Fusionados';
        } else {
            $seccion = 'Inactivos';
        }

        return view('configuracion.participantes.co_pa_04_d_detalle')
        ->with('historico', $historico)
        ->with('bancoMostrar', $bancoMostrar)
        ->with('seccion', $seccion);
    }

    public function create()
    {
        $bancosRepresentantes = Banco::buscarTodos(['Seleccione el Participante Representante','']);

        $bancosPrincipales = Banco::buscarTodos(['Seleccionar el Participante Principal','']);

        $anos = date('Y');
        $fecha = date($anos.'-01-01');

        $bancarios_feriados=Calendario::select ('t_calendario.fecha_validacion')
        ->get()
        ->toArray();

        $bancarios_feriados = array_column($bancarios_feriados,'fecha_validacion');

        $dias_excepcionales = DiasExcepcionales::whereYear('fecha', '>=', $anos)
        ->orderBy('fecha', 'asc')
        ->get();

        $val_dias = DiasExcepcionales::select ('fecha_validacion')
        ->get()
        ->toArray();

        $val_dias = array_column($val_dias,'fecha_validacion');

        $semana_compensacion = SemanaCompensacion::select('validacion')
        ->where ('estatus','ABIERTO')
        ->get()
        ->toArray();

        $semana_compensacion = array_column($semana_compensacion,'validacion');

        for ( $i=1; $i < 3650; $i++ ) {
            $flat = false;
            $fecha = strtotime ( '+1 day' , strtotime ( $fecha ));
            $var= date('w', $fecha);
            $fecha = date('Y-m-d', $fecha);

            foreach($semana_compensacion as $dia) {
                if($dia == $var) {

                    $flat = true;
                }
            }

            foreach($bancarios_feriados as $bancario_feriado) {

                if($fecha == $bancario_feriado) {
                    $flat = false;
                }
            }

            foreach($val_dias as $val_dia) {

                if($fecha == $val_dia) {
                    $flat = true;
                }
            }

            if($flat==false) {
                // Se debe retornar este arreglo para que estos dias sean bloqueados en el datepicker
                $calendario[$i] = $fecha;
                $calendarios[] = $calendario[$i];
            }

        }

        return view('configuracion.participantes.co_pa_02_c_crear')
        ->with('bancosRepresentantes',$bancosRepresentantes)
        ->with('bancosPrincipales',$bancosPrincipales)
        ->with('calendarios',json_encode($calendarios,JSON_NUMERIC_CHECK))
        ->with('semana_compensacion',json_encode($semana_compensacion,JSON_NUMERIC_CHECK))
        ->with('val_dias',json_encode($val_dias,JSON_NUMERIC_CHECK))
        ->with('bancarios_feriados',json_encode($bancarios_feriados,JSON_NUMERIC_CHECK))
        ->with('dias_excepcionales', $dias_excepcionales);
    }

    public function store(ParticipanteCreateRequest $request)
    {
        DB::beginTransaction();

        try {
            $newBanco = new Banco();
            $newBanco->codigo = $request->codigo;
            $newBanco->nombre = trim($request->nombre);
            $newBanco->liquidacion_idt = $request->liquidacion_idt;
            $newBanco->modalidad_participacion = $request->modalidad_participacion;

            if ($request->modalidad_participacion ==0){  $newBanco->banco_representante_id = $request->banco_representante_id;}    

            $newBanco->fecha_ingreso = $request->input('fecha');
            $newBanco->save();
            $historico = new EstadoBanco();
            $historico->estatus_actual = 'INACTIVO';
            $historico->codigo_banco = $request->codigo;
            $historico->id_bancos = $newBanco->id;
            $result = $historico->save();

            if ($request->modalidad_participacion ==1){
                $newBanco->banco_representante_id = $newBanco->id;
            }
            $newBanco->banco_principal_id = $newBanco->id;
            $newBanco->save();
            DB::commit();



        } catch (QueryException $ex) {
            DB::rollback();
            throw new Exception($ex);
        } catch (PDOException $ex) {
            DB::rollback();
            throw new Exception($ex);
        } catch (FatalErrorException $ex) {
            DB::rollback();
            throw new Exception($ex);
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex);
        }

        notify()->flash('Participante creado', 'success', [
            'timer' => 3000,
            'text' => ''
        ]);

        return redirect()->route('Participantes.Inactivos');
    }

    public function edit($id)
    {
        $banco = Banco::find($id);

        $bancos = Banco::select('id', 'nombre', 'codigo')
        ->where('estatus', 'ACTIVO')
        ->get()
        ->pluck('nombre_completo', 'id');

        $bancosRepresentantes = $bancos->prepend('Seleccione el Participante Representante', '');
        $bancosPrincipales = $bancos->prepend('Seleccionar el Participante Principal', '');

        $cambiarEstatus = [];
        if ($banco->estatus == 'INACTIVO') {
            $cambiarEstatus = [
                'ACTIVO',
                'INACTIVO',
            ];
        } elseif ($banco->estatus == 'FUSIONADO') {
            $cambiarEstatus = [
                'FUSIONADO',
                'ABSORBIDO',
            ];
        } elseif ($banco->estatus == 'SUSPENDIDO') {
            $cambiarEstatus = [
                'ACTIVO',
                'SUSPENDIDO',
                'EXCLUIDO',
            ];
        } elseif ($banco->estatus == 'ACTIVO') {
            if($banco->modalidad_participacion=='0'){
                $cambiarEstatus = [
                    'ACTIVO',
                    'SUSPENDIDO',
                ];} else{
                    $cambiarEstatus = [
                        'ACTIVO',
                        'SUSPENDIDO',
                        'FUSIONADO',
                    ];
                }
            }

            if ($banco->estatus == 'ACTIVO') {
                $seccion = 'Activos';
            } elseif ($banco->estatus == 'FUSIONADO') {
                $seccion = 'Fusionados';
            } else {
                $seccion = 'Inactivos';
            }

            return view('configuracion.participantes.co_pa_03_e_editar')
            ->with('bancosRepresentantes',$bancosRepresentantes)
            ->with('bancosPrincipales',$bancosPrincipales)
            ->with('banco',$banco)
            ->with('cambiarEstatus', $cambiarEstatus)
            ->with('seccion', $seccion);
        }

        public function update(ParticipanteUpdateRequest $request, $id)
        {
            DB::beginTransaction();

            try {
                $banco = Banco::select('t_bancos.*')
                ->where('id',$id)
                ->first();

            // Banco
                $bancoInactivo = Banco::FindOrFail($id);

                /*Guardamos toda la información que se esta enviando*/

                $input = $request->all(); 




                if ($bancoInactivo->estatus == 'INACTIVO' && $request->get('estatus') == 'ACTIVO') {
                    $input['fecha_ingreso'] = Carbon::now()->format('Y-m-d');
                }

                if ($request->get('estatus') == 'ABSORBIDO' || $request->get('estatus') == 'EXCLUIDO') {
                    $input['deleted_at'] = Carbon::now();

                    if ($request->get('estatus') == 'ABSORBIDO'){ 
                        $input['fecha_absorsion'] = Carbon::now()->format('Ymd');
                    }
                }

                $result = $bancoInactivo->fill($input)->save(); /*Se guarda la información*/

                if ($request->modalidad_participacion == 1){
                    $bancoInactivo->banco_representante_id=  $bancoInactivo->id;
                    $bancoInactivo->save();

                }

            // historico estado banco
                $historico = new EstadoBanco();
                $historico->estatus_anterior = $banco->estatus;
                $historico->estatus_actual = $request->input('estatus');
                $historico->codigo_banco = $request->input('codigo');
                $historico->id_bancos = $id;
                $historico->fecha_cambio = $request->input('fecha_cambio');
                $result = $historico->save();

                DB::commit();

            } catch (QueryException $ex) {
                DB::rollback();
                throw new Exception($ex);
            } catch (PDOException $ex) {
                DB::rollback();
                throw new Exception($ex);
            } catch (FatalErrorException $ex) {
                DB::rollback();
                throw new Exception($ex);
            } catch (Exception $ex) {
                DB::rollback();
                throw new Exception($ex);
            }


            if ($banco->estatus == 'ACTIVO') {
                $seccion = 'Activos';
            } elseif ($banco->estatus == 'FUSIONADO') {
                $seccion = 'Fusionados';
            } else {
                $seccion = 'Inactivos';
            }

            notify()->flash('Participante actualizado', 'success', [
                'timer' => 3000,
                'text' => ''
            ]);

            return redirect()->route('Participantes.'.$seccion);
        }

        public function destroy($id)
        {
            $bancoInactivo = Banco::FindOrFail($id);
            $result = $bancoInactivo->delete();

            if ($result) {
                return response()->json(['success'=>'true']);
            } else {
                return response()->json(['success'=> 'false']);
            }

            DB::beginTransaction();

            try {

                $bancoInactivo = Banco::FindOrFail($id);
                $result = $bancoInactivo->delete();

                $contacto = Contacto::select('t_contactos.*')
                ->join('t_contactos_bancarios','t_contactos_bancarios.contacto_id','=','t_contactos.id')
                ->first();

                $resultContacto = $contacto->delete();

                $ContactosBancarios = ContactoBancario::where('banco_id', $id);

                $resultadoContactosBancarios = $ContactosBancarios->delete();


                if ($result && $resultContacto && $resultadoContactosBancarios ) {
                    DB::commit();
                    return response()->json(['success'=>'true']);
                } else {
                    DB::rollback();
                    return response()->json(['success'=> 'false']);
                }

            } catch (QueryException $ex) {
                DB::rollback();
                throw new Exception($ex);
            } catch (PDOException $ex) {
                DB::rollback();
                throw new Exception($ex);
            } catch (FatalErrorException $ex) {
                DB::rollback();
                throw new Exception($ex);
            } catch (Exception $ex) {
                DB::rollback();
                throw new Exception($ex);
            }
        }
    }
