<?php

namespace App\Http\Controllers\Configuracion\Eventos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eventos\Eventos;
use App\User;
use App\Models\Seguridad\Role;
use Alert;


class EventosController extends Controller
{
  	public function getIndex()
  	{
        $eventos = Eventos::all();

        return view('configuracion.eventos.co_ev_01_i_eventos')
        ->with('eventos', $eventos);
  	}

    public function edit($id)
  	{
        $evento = Eventos::find($id);

        //dd($evento->destinatarios);

        $roles = Role::select('name', 'id')
        ->pluck('name', 'id');

        $users = User::select('name', 'id')
        ->pluck('name', 'id');

        return view('configuracion.eventos.co_ev_02_e_editar')
        ->with('users', $users)
        ->with('roles', $roles)
        ->with('evento', $evento);
  	}

  	public function update(Request $request, $id)
  	{
        $evento = Eventos::find($id);

        $evento->asunto = $request->get('asunto');
        $evento->mensaje = $request->get('mensaje');
        $evento->estatus = $request->get('estatus');
        $evento->correo = $request->get('correo');

        $destinatarios = [
            'roles' => !is_null($request->get('roles')) ? $request->get('roles') : [],
            'usuarios' => !is_null($request->get('usuarios')) ? $request->get('usuarios') : [],
            'correos' => !is_null($request->get('correos')) ? $request->get('correos') : [],
        ];

        $evento->destinatarios = json_encode($destinatarios, JSON_NUMERIC_CHECK);
        
        $evento->save();

        Alert::success('!','Evento actualizado');

      	return redirect()->route('ConfiguracionEventos');
  	}
}
