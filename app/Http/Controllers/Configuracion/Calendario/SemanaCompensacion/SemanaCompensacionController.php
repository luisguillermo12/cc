<?php

namespace App\Http\Controllers\Configuracion\Calendario\SemanaCompensacion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Configuracion\SemanaCompensacion\SemanaCompensacion;
use App\Http\Requests\Configuracion\SemanaCompensacion\SemanaCompensacionUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;

class SemanaCompensacionController extends Controller
{

  public function getIndex()
  {
    $semana_compensacion = SemanaCompensacion::
    orderBy('id', 'asc')
    ->get();

    return view('configuracion.calendario.semana_compensacion.co_ca_01_i_semanacompensacion')
	->with('semana_compensacion', $semana_compensacion);
  }

  public function create()
  {

  }

  public function store(Request $request)
  {

  }

  public function show($id)
  {

  }

  public function edit($id)
  {
    $semana_compensacion = SemanaCompensacion::find($id);
    return view('configuracion.calendario.semana_compensacion.co_ca_02_e_editardia')
    ->with('semana_compensacion', $semana_compensacion);
  }

  public function update(SemanaCompensacionUpdateRequest $request, $id)
  {
    $semana_compensacion = SemanaCompensacion::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/
    $result = $semana_compensacion->fill($input)->save(); /*Se guarda la información*/

    notify()->flash('Semana de compensación actualizada', 'success', [
      'timer' => 3000,
      'text' => ' '
    ]);

    return redirect()->route('SemanaCompensacion');
  }

  public function destroy($id)
  {

  }
}
