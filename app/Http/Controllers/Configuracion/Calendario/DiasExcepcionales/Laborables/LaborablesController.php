<?php

namespace App\Http\Controllers\Configuracion\Calendario\DiasExcepcionales\Laborables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Calendario\Calendario;
use App\Models\Configuracion\Calendario\DiasExcepcionales;
use App\Models\Configuracion\Calendario\FechasCalendario;
use App\Models\Configuracion\SemanaCompensacion\SemanaCompensacion;
use App\Http\Requests\Configuracion\Calendario\DiasExcepcionales\DiasExcepcionalesCreateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
// use Yajra\Datatables\Facades\Datatables;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class LaborablesController extends Controller
{
  public function __construct() {
    Date::setLocale('es');
  }

  public function getIndex()
  {

    $query ="SELECT SYSDATE AS fecha FROM DUAL";
    $fecha = DB::select($query)[0]->fecha;
    $fechac = Carbon::parse($fecha);

    $anos = Date::now()->year;
    $calendarios1 = Calendario::
    whereYear('fecha', '>=', $anos)
    ->orderBy('fecha', 'asc')
    ->get();
    $fecha = date($anos.'-01-01');

    $bancarios_feriados=Calendario::select ('t_calendario.fecha_validacion')
    ->get()->toArray();
    $bancarios_feriados = array_column($bancarios_feriados,'fecha_validacion');

    $dias_excepcionales = DiasExcepcionales::
    whereYear('fecha', '>=', $anos)
    ->orderBy('fecha', 'asc')
    ->get();


    $val_dias=DiasExcepcionales::select ('fecha_validacion')
    ->get()->toArray();
    $val_dias = array_column($val_dias,'fecha_validacion');


    $semana_compensacion = SemanaCompensacion::select('validacion')
    ->where ('estatus','CERRADO')
    ->get()->toArray();
    $semana_compensacion = array_column($semana_compensacion,'validacion');

    for ( $i=1; $i < 3650; $i++ )
    {
      $flat = false;
      $fecha = strtotime ( '+1 day' , strtotime ( $fecha ));
      $var= date('w', $fecha);
      $fecha = date('Y-m-d', $fecha);

      foreach($semana_compensacion as $dia)
      {
        if($dia == $var)
        {
          $flat = true;
        }
      }
      foreach($bancarios_feriados as $bancario_feriado)
      {
        if($fecha == $bancario_feriado)
        {
          $flat = true;
          //dd($fecha);
        }
      }
      foreach($val_dias as $val_dia)
      {
        if($fecha == $val_dia)
        {
          $flat = false;
          //dd($fecha);
        }
      }
      if($flat==false)
      {
        // Se debe retornar este arreglo para que estos dias sean bloqueados en el datepicker
        $calendario[$i] = $fecha;
        $calendarios[] = $calendario[$i];
      }

    }

    return View('configuracion.calendario.laborables.co_ca_01_i_laborables')
    ->with('semana_compensacion',json_encode($semana_compensacion,JSON_NUMERIC_CHECK))
    ->with('bancarios_feriados',json_encode($bancarios_feriados))
    ->with('fechac',$fechac)
    ->with('dias_excepcionales', $dias_excepcionales)
    ->with('val_dias',json_encode($val_dias,JSON_NUMERIC_CHECK))
    ->with('calendarios',json_encode($calendarios)) ;
  }


public function store(DiasExcepcionalesCreateRequest $request)
  {
    $fecha = str_replace('-', '', $request->get('fecha'));

    $newCalendario = FechasCalendario::where('fecha_validacion', $fecha)
    ->first();
    $newCalendario->estatus = 'EXCEPCIONAL';
    $newCalendario->observaciones = $request->input('observaciones');
    $newCalendario->save();

    notify()->flash('Día laboral creado correctamente', 'success', [
      'timer' => 3000,
      'text' => ' '
    ]);

    return redirect()->route('Laborables');
  }
  public function destroy($id)
  {
    $fecha = FechasCalendario::find($id);
    $fecha->estatus = 'CERRADO';
    $fecha->save();

    if ($fecha)
    {
      return response()->json(['success'=>'true']);
    }
    else
    {
      return response()->json(['success'=> 'false']);
    }
  }

}
