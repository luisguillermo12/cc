<?php

namespace App\Http\Controllers\Configuracion\Calendario\DiasExcepcionales\NoLaborables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Configuracion\Calendario\FechasCalendario;
use App\Models\Configuracion\Calendario\Calendario;
use App\Models\Configuracion\SemanaCompensacion\SemanaCompensacion;
use App\Http\Requests\Configuracion\Calendario\CalendarioCreateRequest;
use App\Http\Requests\Configuracion\Calendario\CalendarioUpdateRequest;
use Session;
use DB;
use Illuminate\Database\Connection;
// use Yajra\Datatables\Facades\Datatables;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class NoLaborablesController extends Controller
{
  public function __construct() {
    Date::setLocale('es');
  }

  public function getIndex()
  {
    $query ="SELECT SYSDATE AS fecha FROM DUAL";
    $fecha = DB::select($query)[0]->fecha;
    $fechac = Carbon::parse($fecha);

    $ano = Date::now()->year;
    $calendarios = Calendario::
    whereYear('fecha', '>=', $ano)
    ->orderBy('fecha', 'asc')
    ->get();

    $no_laboral=Calendario::select ('fecha_validacion')
    ->get()->toArray();
    $no_laboral = array_column($no_laboral,'fecha_validacion');
    //dd($no_laboral);

    $semana_compensacion = SemanaCompensacion::select('validacion')
    ->where ('estatus','CERRADO')
    ->get()->toArray();
    $semana_compensacion = array_column($semana_compensacion,'validacion');


    return View('configuracion.calendario.no_laborables.co_ca_01_i_nolaborables')
    ->with('fechac', $fechac)
    ->with('semana_compensacion',json_encode($semana_compensacion,JSON_NUMERIC_CHECK))
    ->with('no_laboral',json_encode($no_laboral))
    ->with('calendarios', $calendarios);
  }

  public function create()
  {
    return View('configuracion.calendario.no_laborables.co_ca_01_i_nolaborables');
  }

  public function store(CalendarioCreateRequest $request)
  {
    $fecha = str_replace('-', '', $request->get('fecha'));

    $newCalendario = FechasCalendario::where('fecha_validacion', $fecha)
    ->first();

    $newCalendario->estatus = $request->input('estatus');
    $newCalendario->save();

    notify()->flash('Día no laborable creado correctamente', 'success', [
      'timer' => 3000,
      'text' => ' '
    ]);

    return redirect()->route('NoLaborables');
  }

  public function show($id)
  {
    $calendario = Calendario::FindOrFail($id);
    return response()->json($calendario); /*Devuelve un array con los datos del registro*/
  }

  public function edit($id)
  {
    $calendario = Calendario::find($id);
    return response()->json($calendario); /*Devuelve un array con los datos del registro*/
  }

  public function update(CalendarioUpdateRequest $request, $id)
  {
    $calendario = Calendario::FindOrFail($id);
    $input = $request->all();  /*Guardamos toda la información que se esta enviando*/

    $result = $calendario->fill($input)->save(); /*Se guarda la información*/
  }

  public function destroy($id)
  {
    $fecha = FechasCalendario::find($id);

    // Se verifica si el día es sábado o domingo, o no.
    if ($fecha->fecha_validacion->format('w') == 0 || $fecha->fecha_validacion->format('w') == 6) {
      $estatus = 'CERRADO';
    } else {
      $estatus = 'ABIERTO';
    }

    $fecha->estatus =  $estatus;
    $fecha->save();

    if ($fecha) {
      return response()->json(['success'=> 'true']);
    } else {
      return response()->json(['warning'=> 'false']);
    }
  }

}
