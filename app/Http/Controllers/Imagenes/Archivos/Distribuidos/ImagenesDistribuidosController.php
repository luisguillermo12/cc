<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Imagenes\Distribuidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Monitoreo\TransferenciaArchivo;
use DB;

class ImagenesDistribuidosController extends Controller
{
    public function getIndex(Request $request)
    {

        /*
         * Recibe el cod_banco desde la vista
         * Si no se recibe ningún valor se establece '*' por defecto
         */
        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';

        /*
         * Recibe el periodo desde la vista
         * Si no se recibe ningún valor se establece '*' por defecto
         */
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : '*';

        /*
         * Listado de todos los bancos desde el modelo Banco
         */
        $bancos = Banco::buscarTodos();

        /*
         * Listado de parámetros para T y T+1
         */
        $parametros = DB::table('t_parametros_sistema')
        ->select('valor')
        ->orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

        /*
         * Formatear cadena a formato de fecha
         */
        foreach ($parametros as $parametro) {
            $fechas[] = date( "Y-m-d", strtotime( $parametro->valor ) );
        };

        /*
         * Listado de bancarios y feriados
        */
        $calendario = DB::table('t_calendario')
        ->select('fecha_validacion')
        ->get();

        /*
         * Arreglo con las fechas de bancarios y feriados
         */
       foreach ($calendario as $calendario) {
            $feriados[] = $calendario->fecha_validacion;
        };

        /*
         * Consulta al SP por el listado de los archivos de imágenes enviadas
         * Recibe como parámetro el cod_banco
         */
        $query = "SELECT EPSACH.sp_monitoreo_imagenes_enviados('$cod_banco','$fecha') FROM dual";
        $ImagenesEnviadas = DB::select($query);

        /*
         * Consulta al SP por la cantidad total de archivos de imágenes enviadas
         * Recibe como parámetro el cod_banco
         */
        //$query = "SELECT EPSACH.sp_monitoreo_total_imagenes_enviados('$cod_banco') FROM dual";
        //$TotalImagenesEnviadas = DB::select($query)[0];

        /*
         * Cálculo de Totales
         */
        $totales['recibidas'] = TransferenciaArchivo::totalRecibidos('MAILI', $cod_banco);
        $totales['distribuidas'] = TransferenciaArchivo::totalAceptados('MAILO', $cod_banco);
        $totales['rechazadas'] = TransferenciaArchivo::totalRechazados('MAILI', $cod_banco);
        $totales['porcentaje'] = $totales['recibidas'] != 0 ? ($totales['distribuidas'] / $totales['recibidas']) * 100 : 0;

        return view('monitoreo.archivos.imagenes.distribuidos.mo17_i_distribuidos')
        ->with('bancos', $bancos)
        ->with('cod_banco', $cod_banco)
        ->with('ImagenesEnviadas', $ImagenesEnviadas)
        ->with('totales', $totales)
        ->with('fecha', $fecha)
        ->with('fechas', $fechas)
        ->with('feriados', $feriados);
    }
}
