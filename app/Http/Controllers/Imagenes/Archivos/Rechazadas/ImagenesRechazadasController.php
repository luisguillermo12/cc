<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Imagenes\Rechazadas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\TipoArchivo\TipoArchivo;
use App\Models\Monitoreo\ArchivosRechazados;
use App\Models\Monitoreo\ResultadosCodigos;
use App\Models\Monitoreo\TransferenciaArchivo;
use DB;

class ImagenesRechazadasController extends Controller
{
    public function getIndex(Request $request)
    {
      //FECHA DE INTERCAMBIO
      $cod_resultado = ResultadosCodigos::select('codigo', "CONCAT(codigo, CONCAT(' - ', descripcion)) nombre")
      ->orderBy('id', 'asc')
      ->whereIn('id', [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21])
      ->lists('nombre','codigo')
      ->prepend('*', '*');

        // BANCOS
        $bancos = Banco::buscarTodos();
        $cod_banco_emisor = $request->get('cod_banco_emisor') ? $request->get('cod_banco_emisor') : '*';
        $cod_banco_receptor = $request->get('cod_banco_receptor') ? $request->get('cod_banco_receptor') : '*';
        $cod_rechazo = $request->get('cod_rechazo') ? $request->get('cod_rechazo') : '*';

        $query = "SELECT EPSACH.sp_monitoreo_archivos_rechazados('$cod_banco_emisor', 'MAILI',$cod_rechazo) FROM dual";
        $ArchivosRechazados = DB::select($query);
dd('lol');
        /*
         * Cálculo de Totales
         */
        $totales['recibidas'] = TransferenciaArchivo::totalRecibidos('MAILI', $cod_banco_emisor);
        $totales['validadas'] = TransferenciaArchivo::totalAceptados('MAILI', $cod_banco_emisor);
        $totales['rechazadas'] = TransferenciaArchivo::totalRechazados('MAILI', $cod_banco_emisor);
        $totales['porcentaje'] = $totales['recibidas'] != 0 ? ($totales['validadas'] / $totales['recibidas']) * 100 : 0;

        return view('monitoreo.archivos.imagenes.rechazados.mo18_i_rechazadas')
        ->with('bancos', $bancos)
        ->with('cod_banco_emisor', $cod_banco_emisor)
        ->with('cod_banco_receptor', $cod_banco_receptor)
        ->with('ArchivosRechazados', $ArchivosRechazados)
        ->with('totales', $totales)
        ->with('cod_resultado', $cod_resultado)
        ->with('cod_rechazo', $cod_rechazo);
    }
}
