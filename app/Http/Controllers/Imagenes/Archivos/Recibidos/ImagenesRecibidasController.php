<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Imagenes\Recibidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Monitoreo\TransferenciaArchivo;
use DB;

class ImagenesRecibidasController extends Controller
{
    public function getIndex(Request $request)
    {
        /*
         * Obtener el listado de todos los bancos desde el modelo Banco
         */
        $bancos = Banco::BuscarTodos();

        /*
         * Toma el cod_banco desde la vista
         * Si no se recibe ningún valor se establece '*' por defecto
         */
        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';

        /*
         * Recibe el periodo desde la vista
         * Si no se recibe ningún valor se establece '*' por defecto
         */
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : '*';

        /*
         * Listado de parámetros para T y T+1
         */
        $parametros = DB::table('t_parametros_sistema')
        ->select('valor')
        ->orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

        /*
         * Formatear cadena a formato de fecha
         */
        foreach ($parametros as $parametro) {
            $fechas[] = date( "Y-m-d", strtotime( $parametro->valor ) );
        };

        /*
         * Listado de bancarios y feriados
        */
        $calendario = DB::table('t_calendario')
        ->select('fecha_validacion')
        ->get();

        /*
         * Arreglo con las fechas de bancarios y feriados
         */
       foreach ($calendario as $calendario) {
            $feriados[] = $calendario->fecha_validacion;
        };

        /*
         * Consulta al SP por el listado de las imágenes enviadas
         * Recibe como parámetro el cod_banco
         */
        $query = "SELECT EPSACH.sp_monitoreo_imagenes_recibidos('$cod_banco', '$fecha') FROM dual";
        $ImagenesRecibidas = DB::select($query);

        /*
         * Cálculo de Totales
         */
        $totales['recibidas'] = TransferenciaArchivo::totalRecibidos('MAILI', $cod_banco);
        $totales['validadas'] = TransferenciaArchivo::totalAceptados('MAILI', $cod_banco);
        $totales['rechazadas'] = TransferenciaArchivo::totalParcialmenteAceptados('MAILI', $cod_banco);
        $totales['porcentaje'] = TransferenciaArchivo::totalRechazados('MAILI', $cod_banco);

        /*
         * Retorna la vista y le envía las variables
         */
        return view('monitoreo.archivos.imagenes.Recibidos.mo15_i_recibidas')
        ->with('bancos', $bancos)
        ->with('cod_banco', $cod_banco)
        ->with('ImagenesRecibidas', $ImagenesRecibidas)
        ->with('totales', $totales)
        ->with('fecha', $fecha)
        ->with('fechas', $fechas)
        ->with('feriados', $feriados);
    }
}
