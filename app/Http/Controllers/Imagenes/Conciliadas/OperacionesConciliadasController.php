<?php

namespace App\Http\Controllers\Conciliador\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Conciliador\Operaciones;
use App\Models\Conciliador\Imagenes;
use App\Models\Conciliador\ArchivosSCS;
use App\Models\Conciliador\Outgo;
use App\Models\Conciliador\ParametrosSistemaConciliador;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\Mediopago\Producto;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Log;
use Illuminate\Support\Collection;
use App\Models\Conciliador\Mailo;

class OperacionesConciliadasController extends Controller
{
    public function getIndex(Request $request)
    {
        
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
        ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
        ->where('t_parametros_sistema.id', '35')
        ->where('t_monedas.estatus','ACTIVO')
        ->first();

        $bancos = Banco::buscarTodos();
        $cod_banco_emisor = $request->get('cod_banco_emisor') != null ?  $request->get('cod_banco_emisor') : '*';

        $totales= (object)['total_operaciones'=>Outgo::
          where(function($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*'){
             $query->where('cod_banco_emisor',$cod_banco_emisor);
            }
        })
          ->get()->count(), 

          'total_operaciones_por_conciliar' =>Outgo::
          where(function($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*'){
             $query->where('cod_banco_emisor',$cod_banco_emisor);
            }
        })
          ->where('estatus','!=','CONCILIADO')
          //->where('estatus','INITIALISED')
          ->get()->count(), 

          'total_operaciones_conciliadas' =>  Mailo::
          where(function($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*'){
             $query->where('cod_banco_emisor',$cod_banco_emisor);
            }
        })
          ->where('estatus','CONCILIADO')
          ->get()->count(), 

          'total_monto_conciliado' => Outgo::
          where(function($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*'){
             $query->where('cod_banco_emisor',$cod_banco_emisor);
            }
        })
          ->where('estatus','CONCILIADO')
          ->get()->sum('monto') ];       
        

         //dd($totales); 
        
    /*    $registros = Outgo::select('t_outgo.cod_banco_emisor', 't_outgo.cod_banco_destinatario','SUM(t_outgo.monto) as monto', 'COUNT(t_outgo.id) as operaciones_presentadas', 'COUNT(t_mailo.id) as imagenes_distribuidas')
        ->leftJoin('t_mailo', 't_outgo.num_ibrn', '=', 't_mailo.num_ibrn')
        ->where(function ($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*') {
                $query->where('t_outgo.cod_banco_emisor', $cod_banco_emisor);
            }
        })
        ->groupBy('t_outgo.cod_banco_emisor', 't_outgo.cod_banco_destinatario')
        ->orderBy('t_outgo.cod_banco_emisor', 'ASC')
        ->orderBy('t_outgo.cod_banco_destinatario', 'ASC')
        ->get();*/

         $registros = Outgo::select('cod_banco_emisor','cod_banco_destinatario', 'SUM(t_outgo.monto) as monto','COUNT(id) as operaciones_presentadas')
          ->where(function ($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*') {
                $query->where('cod_banco_emisor', $cod_banco_emisor);
            }
        })
       /* ->where(function ($query) use ($cod_banco_receptor) {
            if ($cod_banco_receptor != '*') {
                $query->where('cod_banco_destinatario', $cod_banco_receptor);
            }
        })*/
        ->groupBy('cod_banco_emisor', 'cod_banco_destinatario')
        ->orderBy('cod_banco_emisor', 'ASC')
        ->orderBy('cod_banco_destinatario', 'ASC')
        ->get();

          foreach ($registros as $registro) {

          $prueba=Mailo::select('COUNT(id) as imagenes_distribuidas')
                                                   ->where('cod_banco_emisor', $registro->cod_banco_emisor)
                                                   ->where('cod_banco_pagador', $registro->cod_banco_destinatario)
                                                   ->where('estatus', 'CONCILIADO')
                                                   ->first();
          //dd($prueba);
          $registro->imagenes_distribuidas = $prueba->imagenes_distribuidas;
         }

        // dd($registros);

        return view('conciliador.operaciones.conciliadas.im08_i_conciliadas')
        ->with('registros', $registros)
        ->with('totales', $totales)
        ->with('bancos', $bancos)
        ->with('monedas', $monedas)
        ->with('cod_banco', $cod_banco_emisor);
   
    }


    public function detalle(Request $request, $emisor, $receptor)

    {

        $sub_productos= Producto::where('codigo', '030')
        ->first()
        ->sub_producto
        ->lists('nombre_completo', 'codigo')
        ->prepend('*', '');

        $sub_productos_fil = $request->get('sub_producto') != null ? $request->get('sub_producto') : '*';

        $registros_totales = Outgo::select('t_outgo.*', 't_bancos.codigo', 't_bancos.nombre')
        ->leftJoin('t_mailo', 't_outgo.num_ibrn', '=', 't_mailo.num_ibrn')
        ->leftJoin('epsach.t_bancos', 't_outgo.cod_banco_destinatario', '=', 'epsach.t_bancos.codigo')
        ->where('t_outgo.cod_banco_emisor', $emisor)
        ->where('t_outgo.cod_banco_destinatario', $receptor)
        ->orderBy('t_outgo.cod_banco_emisor', 'ASC')
        ->orderBy('t_outgo.cod_banco_destinatario', 'ASC')
        ->get();

        //dd($registros_totales);

        $signo =  $request->get('nivel') != null ? $request->get('nivel') : '*';
        $monto =  $request->get('monto');
        $n_cheque = $request->get('n_cheque');

        $registros = Outgo::select('t_outgo.*', 't_bancos.codigo', 't_bancos.nombre')
        ->leftJoin('t_mailo', 't_outgo.num_ibrn', '=', 't_mailo.num_ibrn')
        ->leftJoin('epsach.t_bancos', 't_outgo.cod_banco_destinatario', '=', 'epsach.t_bancos.codigo')
        ->where(function($query) use ($sub_productos_fil, $signo, $monto, $n_cheque) {
            if ($n_cheque != null) {
              $query->where('t_outgo.num_cheque', $n_cheque);
            };
            
            if ($sub_productos_fil != '*') {
              $query->where('t_outgo.cod_producto', $sub_productos_fil);
            };

             if ($signo != '*' && $monto != null ) {
              $query->where('t_outgo.monto', $signo, $monto );
            };
        })
        ->where('t_outgo.cod_banco_emisor', $emisor)
        ->where('t_outgo.cod_banco_destinatario', $receptor)
        ->orderBy('t_outgo.cod_banco_emisor', 'ASC')
        ->orderBy('t_outgo.cod_banco_destinatario', 'ASC')
        ->get();
 
        $banco_emisor = Banco::where('codigo', $emisor)
        ->first();

        $totales = (object)[
            'total_operaciones' => $registros_totales->count(),
            'total_operaciones_concilidas' => $registros_totales->where('estatus', 'CONCILIADO')->count(),
            'porcentaje_distribuidas' => $registros_totales->where('estatus', 'CONCILIADO')->count()*100/$registros_totales->count(),
            'total_operaciones_por_concilidas' => $registros_totales->where('estatus', 'POR CONCILIAR')->count(),
            'porcentaje_faltantes' => $registros_totales->where('estatus', 'POR CONCILIAR')->count()*100/$registros_totales->count(),
        ];

        return view('conciliador.operaciones.conciliadas.im09_s_detalles')
        ->with('sub_productos', $sub_productos)
        ->with('sub_productos_fil', $sub_productos_fil)
        ->with('totales', $totales)
        ->with('banco_emisor', $banco_emisor)
        ->with('registros', $registros);
    }
}
