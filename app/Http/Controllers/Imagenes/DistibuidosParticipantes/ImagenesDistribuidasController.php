<?php

namespace App\Http\Controllers\Conciliador\Monitoreo;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Conciliador\Outgo;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Log;
use Illuminate\Support\Collection;
use App\Models\Conciliador\Imagenes;
use App\Models\Configuracion\Mediopago\Producto;
use App\Models\Conciliador\Mailo;
class ImagenesDistribuidasController extends Controller
{
    public function getIndex(Request $request) {

        // BANCOS
        $bancos = Banco::buscarTodos();
        $cod_banco_emisor = $request->get('cod_banco_emisor') != null ? $request->get('cod_banco_emisor') : '*';
        $cod_banco_receptor = $request->get('cod_banco_receptor') != null ? $request->get('cod_banco_receptor') : '*';



 /*       $registros = Outgo::select('t_outgo.cod_banco_emisor', 't_outgo.cod_banco_destinatario','SUM(t_outgo.monto) as monto', 'COUNT(t_outgo.id) as operaciones_presentadas', 'COUNT(t_imagenes.id) as imagenes_distribuidas')
        ->leftJoin('t_imagenes', 't_outgo.num_ibrn', '=', 't_imagenes.num_ibrn')
        ->where(function ($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*') {
                $query->where('t_outgo.cod_banco_emisor', $cod_banco_emisor);
            }
        })
        ->where(function ($query) use ($cod_banco_receptor) {
            if ($cod_banco_receptor != '*') {
                $query->where('t_outgo.cod_banco_destinatario', $cod_banco_receptor);
            }
        })
        ->groupBy('t_outgo.cod_banco_emisor', 't_outgo.cod_banco_destinatario')
        ->orderBy('t_outgo.cod_banco_emisor', 'ASC')
        ->orderBy('t_outgo.cod_banco_destinatario', 'ASC')
        ->get();*/

        $registros = Outgo::select('cod_banco_emisor','cod_banco_destinatario', 'SUM(t_outgo.monto) as monto','COUNT(id) as operaciones_presentadas')
          ->where(function ($query) use ($cod_banco_emisor) {
            if ($cod_banco_emisor != '*') {
                $query->where('cod_banco_emisor', $cod_banco_emisor);
            }
        })
        ->where(function ($query) use ($cod_banco_receptor) {
            if ($cod_banco_receptor != '*') {
                $query->where('cod_banco_destinatario', $cod_banco_receptor);
            }
        })
        ->groupBy('cod_banco_emisor', 'cod_banco_destinatario')
        ->orderBy('cod_banco_emisor', 'ASC')
        ->orderBy('cod_banco_destinatario', 'ASC')
        ->get();
         

         foreach ($registros as $registro) {

          $prueba=Outgo::select('COUNT(id) as imagenes_distribuidas')
                                                   ->where('cod_banco_emisor', $registro->cod_banco_emisor)
                                                   ->where('cod_banco_destinatario', $registro->cod_banco_destinatario)
                                                   ->where('estatus', 'CONCILIADO')
                                                   ->first();
          $registro->imagenes_distribuidas = $prueba->imagenes_distribuidas;
         }
        
        $total_operaciones = $registros->sum('operaciones_presentadas');
        $total_imagenes = $registros->sum('imagenes_distribuidas');
        $total_monto_conciliado = Outgo::select('monto as monto')
                                        // ->leftJoin('t_imagenes', 't_outgo.num_ibrn', '=', 't_imagenes.num_ibrn')
                                         ->where('estatus', 'CONCILIADO')
                                            ->where(function ($query) use ($cod_banco_emisor) {
                                               if ($cod_banco_emisor != '*') {
                                                    $query->where('cod_banco_emisor', $cod_banco_emisor);
                                                }
                                                                                             })
                                             ->where(function ($query) use ($cod_banco_receptor) {
                                               if ($cod_banco_receptor != '*') {
                                                     $query->where('cod_banco_destinatario', $cod_banco_receptor);
                                               }
                                                                                                 })
                                             ->get()->sum('monto');
        //dd($total_monto_conciliado); 
        $totales = (object) [
            'operaciones' => $total_operaciones ,
            'imagenes' => $total_imagenes,
            'total_monto_conciliado' =>$total_monto_conciliado,
            'porcentaje' => $total_operaciones != 0 ? $total_imagenes * 100 / $total_operaciones : 0,
            'faltantes' => $total_operaciones - $total_imagenes,
            'porcentaje_faltante' => $total_operaciones != 0 ? ($total_operaciones - $total_imagenes) * 100 / $total_operaciones : 0,
        ];

        return view('conciliador.monitoreo.imagenes_distribuidas.im01_i_distribuidas')
        ->with('bancos', $bancos)
        ->with('cod_banco_emisor', $cod_banco_emisor)
        ->with('cod_banco_receptor', $cod_banco_receptor)
        ->with('totales', $totales)
        ->with('registros', $registros);
    }

    public function detalle(Request $request,$emisor, $receptor)
    {

$sub_productos= Producto::where('codigo', '030')
->first() ->sub_producto
->lists('nombre_completo', 'codigo')
->prepend('*', '');

$sub_productos_fil=$request->get('sub_producto') != null ? $request->get('sub_producto') : '*';


 $registros_totales = Outgo::select('t_outgo.*', 't_bancos.codigo', 't_bancos.nombre')
        ->leftJoin('t_mailo', 't_outgo.num_ibrn', '=', 't_mailo.num_ibrn')
        ->leftJoin('epsach.t_bancos', 't_outgo.cod_banco_destinatario', '=', 'epsach.t_bancos.codigo')
         ->where('t_outgo.cod_banco_emisor', $emisor)
        ->where('t_outgo.cod_banco_destinatario', $receptor)
        ->orderBy('t_outgo.cod_banco_emisor', 'ASC')
        ->orderBy('t_outgo.cod_banco_destinatario', 'ASC')
        ->get();

        //dd($registros_totales);

$signo =  $request->get('nivel') != null ? $request->get('nivel') : '*';
$monto =  $request->get('monto');
$n_cheque = $request->get('n_cheque');


  $registros = Outgo::select('t_outgo.*', 't_bancos.codigo', 't_bancos.nombre')
        ->leftJoin('t_mailo', 't_outgo.num_ibrn', '=', 't_mailo.num_ibrn')
        ->leftJoin('epsach.t_bancos', 't_outgo.cod_banco_destinatario', '=', 'epsach.t_bancos.codigo')

->where(function($query) use ($sub_productos_fil, $signo, $monto, $n_cheque) {
 

      if ($n_cheque != null) {
        $query->where('t_outgo.num_cheque',$n_cheque);
      };
      
      if ($sub_productos_fil != '*') {
        $query->where('t_outgo.cod_producto',$sub_productos_fil);
      };

       if ($signo != '*' && $monto != null ) {
        $query->where('t_outgo.monto',$signo, $monto );
      };
      

})
        ->where('t_outgo.cod_banco_emisor', $emisor)
        ->where('t_outgo.cod_banco_destinatario', $receptor)
        ->orderBy('t_outgo.cod_banco_emisor', 'ASC')
        ->orderBy('t_outgo.cod_banco_destinatario', 'ASC')
        ->get();
 
//46000778



    $banco_emisor = Banco::where('codigo', $emisor)
    ->first();




     $totales = (object)['total_operaciones'=>$registros_totales->count(),
                        'total_operaciones_concilidas'=>$registros_totales->where('estatus', 'CONCILIADO')->count(),
                        'porcentaje_distribuidas' =>$registros_totales->where('estatus', 'CONCILIADO')->count()*100/$registros_totales->count(),
                        'total_operaciones_por_concilidas'=>$registros_totales->where('estatus', 'POR CONCILIAR')->count(),
                        'porcentaje_faltantes'=>$registros_totales->where('estatus', 'POR CONCILIAR')->count()*100/$registros_totales->count() ];

      

        

   
     
   
    return view('conciliador.monitoreo.imagenes_distribuidas.im02_s_detalles')
    ->with('sub_productos', $sub_productos)
    ->with('sub_productos_fil', $sub_productos_fil)
    ->with('totales', $totales)
    ->with('banco_emisor', $banco_emisor)
    ->with('registros', $registros);
  }




  public function cheques ($ibrn){
    
   $detalle_cheque = Outgo::select('t_outgo.num_cheque','t_outgo.num_cuenta_pagador','t_outgo.monto','t_outgo.cod_tipo_operacion','t_outgo.cod_banco_emisor','t_outgo.cod_banco_destinatario','t_outgo.cod_moneda', 't_outgo.cod_producto' , 't_imagenes.*')
             ->leftJoin('t_imagenes', 't_imagenes.num_ibrn', '=', 't_outgo.num_ibrn')
              ->where('t_imagenes.estatus','CONCILIADO')
             ->where('t_outgo.num_ibrn', $ibrn)
             ->first();

    return view('conciliador.monitoreo.imagenes_distribuidas.cheque')
    ->with ('detalle_cheque', $detalle_cheque);
}


}
