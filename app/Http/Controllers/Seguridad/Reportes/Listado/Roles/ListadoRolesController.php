<?php

namespace App\Http\Controllers\Seguridad\Reportes;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Access\Role\Role;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;

class ListadoRolesController extends Controller
{
    public function getIndex()
    {
        $roles = Role::orderBy('id', 'ASC')
        ->paginate(5);

        $roles2 = Role::orderBy('id', 'ASC')
        ->get();

      //  dd($roles2[0]->user-count());

        return view('seguridad.reportes.roles.se11_i_roles')
        ->with('roles', $roles);
    }



  public function excel()
    {
        $roles = Role::orderBy('id', 'ASC')
        ->get();

        Excel::create("ReporteRoles", function ($excel) use ($roles) {
            $excel->setTitle("ReporteRoles");
            $excel->sheet("Roles", function ($sheet) use ($roles) {
                $sheet->loadView('seguridad.reportes.roles.se19_ex_excel')->with('roles', $roles);
            });
        })->download('xls');

        return back();
    }


   public function pdf()
    {
        $roles = Role::orderBy('id', 'ASC')
        ->get();

        $view =  \View::make('seguridad.reportes.roles.se20_p_pdf', compact('roles', 'pdf'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait');
        
        return $pdf->download('Reporte_Roles.pdf');
    }
}
