<?php

namespace App\Http\Controllers\Seguridad\Reportes;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ControlAccesoController extends Controller
{
    public function getIndex()
    {
        $logs = DB::table('audits')
        ->where('url', 'LIKE', '%login')
        ->orWhere('url', 'LIKE', '%logout')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->orderBy('audits.id', 'DESC')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->paginate(15);

        for($x = 0; $x < count($logs); $x++) {
            $logs[$x]->action = strpos($logs[$x]->url, 'login') ? 'Inicio de sesión' : 'Cierre de sesión';
        }


        return view('seguridad.reportes.control_acceso.se12_i_controlacceso')
        ->with('logs', $logs);
    }

    public function excel()
    {
        $logs = DB::table('audits')
        ->where('url', 'LIKE', '%login')
        ->orWhere('url', 'LIKE', '%logout')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->orderBy('audits.id', 'DESC')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->get();

        for($x = 0; $x < count($logs); $x++) {
            $logs[$x]->action = strpos($logs[$x]->url, 'login') ? 'Inicio de sesión' : 'Cierre de sesión';
        }

        Excel::create("ReporteControlAcceso", function ($excel) use ($logs) {
            $excel->setTitle("ReporteControlAcceso");
            $excel->sheet("ControlAcceso", function ($sheet) use ($logs) {
                $sheet->loadView('seguridad.reportes.control_acceso.se17_ex_excel')->with('logs', $logs);
            });
        })->download('xls');

        return back();
    }

    public function pdf()
    {
        $logs = DB::table('audits')
        ->where('url', 'LIKE', '%login')
        ->orWhere('url', 'LIKE', '%logout')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->orderBy('audits.id', 'DESC')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->paginate(15);

        for($x = 0; $x < count($logs); $x++) {
            $logs[$x]->action = strpos($logs[$x]->url, 'login') ? 'Inicio de sesión' : 'Cierre de sesión';
        }

        $view =  \View::make('seguridad.reportes.control_acceso.se18_p_pdf', compact('logs', 'pdf'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait');
        
        return $pdf->download('Reporte_Control_Acceso.pdf');
    }
}
