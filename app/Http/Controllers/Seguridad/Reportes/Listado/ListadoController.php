<?php

namespace App\Http\Controllers\Seguridad\Reportes\Listado;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ListadoController extends Controller
{
	public function Listado(){
       return view('seguridad.reportes.listado.se22_i_listado');
  	}
  public function  Excel(){
     Excel::create("Listado de reportes(seguridad)", function ($excel){
       $excel->setTitle("Listado_de_reportes");
       $excel->sheet("Hoja1", function ($sheet) {
       $sheet->loadView('seguridad.reportes.listado.se24_ex_excel');
       });
     })->download('xls');
  	return back();
    }

    public function Pdf() {

      $view =  \View::make('seguridad.reportes.listado.se23_p_pdf', compact('pdf'))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      $pdf->setPaper('a4', 'portrait');
      //return $pdf->stream('ejemplo.pdf');
      return $pdf->download('Listado de reportes(seguridad).pdf');
  }

}
