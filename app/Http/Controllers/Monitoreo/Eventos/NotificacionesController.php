<?php

namespace App\Http\Controllers\Monitoreo\Eventos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Eventos\EventosNotificaciones;

class NotificacionesController extends Controller
{
    public function getIndex(Request $request)
    {
        $notificaciones = EventosNotificaciones::all();

        return view('monitoreo.eventos.mo40_i_notificaciones')
        ->with('notificaciones', $notificaciones);
    }

    public function show($id)
    {
        $notificacion = EventosNotificaciones::find($id);

        return view('monitoreo.eventos.mo41_s_detalles')
        ->with('notificacion', $notificacion);
    }
}
