<?php

namespace App\Http\Controllers\Monitoreo\Alarmas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\ValidacionesEPS;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Monitoreo\Alarmas;
use App\Models\MensajesInformativos\MensajesInformativos;
use App\Models\Monitoreo\DetallesAlarmas;
use App\Models\Access\User\User;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Jenssegers\Date\Date;

class AlarmasController extends Controller
{
  public function getIndex(Request $request)
  {
    //FECHA DE INTERCAMBIO
    $filtro_initr = ValidacionesEPS::orderBy('id', 'asc')
    ->whereIn('id', [1,2,8])
    ->lists('codigo','id')
    ->prepend('*', '0');

    $filtro_icom1 = ValidacionesEPS::orderBy('id', 'asc')
    ->whereIn('id', [21,23,31])
    ->lists('codigo','id')
    ->prepend('*', '0');

    $filtro_icom2 = ValidacionesEPS::orderBy('id', 'asc')
    ->whereIn('id', [131,133,141])
    ->lists('codigo','id')
    ->prepend('*', '0');

    $filtro_fposr = ValidacionesEPS::orderBy('id', 'asc')
    ->whereIn('id', [173,175,181,183])
    ->lists('codigo','id')
    ->prepend('*', '0');

    $filtro_stmta = ValidacionesEPS::orderBy('id', 'asc')
    ->whereBetween('id', [220, 242])
    ->lists('codigo','id')
    ->prepend('*', '0');

    $filtro_maili = ValidacionesEPS::orderBy('id', 'asc')
    ->whereIn('id', [193,195,196,205,208])
    ->lists('codigo','id')
    ->prepend('*', '0');

    $filtro_msjs = ValidacionesEPS::orderBy('id', 'asc')
    ->whereIn('id', [193,195,196,205,208])
    ->lists('codigo','id')
    ->prepend('*', '0');

    // BANCOS
    $bancos = Banco::buscarTodos();

    $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';
    $cod_initr = $request->get('cod_initr') != null ? $request->get('cod_initr') : '0';
    $cod_icom1 = $request->get('cod_icom1') != null ? $request->get('cod_icom1') : '0';
    $cod_icom2 = $request->get('cod_icom2') != null ? $request->get('cod_icom2') : '0';
    $cod_fposr = $request->get('cod_fposr') != null ? $request->get('cod_fposr') : '0';
    $cod_stmta = $request->get('cod_stmta') != null ? $request->get('cod_stmta') : '0';
    $cod_maili = $request->get('cod_maili') != null ? $request->get('cod_maili') : '0';
    $cod_msj = $request->get('cod_msjs') != null ? $request->get('cod_msjs') : '0';


    $query = "SELECT EPSACH.sp_monitoreo_alarmas('$cod_banco', 'INITR', '$cod_initr') FROM dual";
    $Initr = DB::select($query);

    $query = "SELECT EPSACH.sp_monitoreo_alarmas('$cod_banco', 'ICOM1', '$cod_icom1') FROM dual";
    $Icom1 = DB::select($query);

    $query = "SELECT  EPSACH.sp_monitoreo_alarmas('$cod_banco', 'ICOM2', '$cod_icom2') FROM dual";
    $Icom2 = DB::select($query);

    $query = "SELECT EPSACH.sp_monitoreo_alarmas('$cod_banco', 'FPOSR', '$cod_fposr') FROM dual";
    $Fposr = DB::select($query);

    $query = "SELECT EPSACH.fn_monitoreo_alarmas_liquidacion('$cod_banco', 'STMTA', '$cod_stmta') FROM dual";
    $Stmta = DB::select($query);

    $query = "SELECT EPSACH.sp_monitoreo_alarmas('$cod_banco', 'MAILI', '$cod_maili') FROM dual";
    $Maili = DB::select($query);

    $Mensajes = MensajesInformativos::alarmas();
//dd($Mensajes);

    return view('monitoreo.alarmas.mo01_i_alarmas')
      ->with('bancos', $bancos)
      ->with('cod_banco', $cod_banco)
      ->with('filtro_initr', $filtro_initr)
      ->with('cod_initr', $cod_initr)
      ->with('initr', collect($Initr))
      ->with('filtro_icom1', $filtro_icom1)
      ->with('cod_icom1', $cod_icom1)
      ->with('icom1', collect($Icom1))
      ->with('filtro_icom2', $filtro_icom2)
      ->with('cod_icom2', $cod_icom2)
      ->with('icom2', collect($Icom2))
      ->with('filtro_fposr', $filtro_fposr)
      ->with('cod_fposr', $cod_fposr)
      ->with('fposr', collect($Fposr))
      ->with('filtro_stmta', $filtro_stmta)
      ->with('cod_stmta', $cod_stmta)
      ->with('stmta', collect($Stmta))
      ->with('filtro_maili', $filtro_maili)
      ->with('cod_maili', $cod_maili)
      ->with('maili', collect($Maili))
      ->with('filtro_msjs', $filtro_msjs)
      ->with('cod_msj', $cod_msj)
      ->with('mensajes', $Mensajes);
  }

  public function show($id, $archivo = null)
  {
    if ($archivo != null ){
        $query = "SELECT EPSACH.fn_monitoreo_alarmas_liquidacion_detalle($id) FROM dual";
        $alarma = DB::select($query);
    } else {
        $query = "SELECT EPSACH.sp_monitoreo_alarmas_detalles($id) FROM dual";
        $alarma = DB::select($query);
    }

    $query = "SELECT  EPSACH.sp_monitoreo_alarmas_detalles_observacion($id) FROM dual";
    $observaciones = DB::select($query);

   return view('monitoreo.alarmas.mo02_s_detalles')
    ->with('alarma', $alarma)
    ->with('observaciones', $observaciones);
  }

  public function detalleMensajes($id)
  {
    $alarma = $Mensajes = MensajesInformativos::alarma($id);
//dd($alarma);

    $query = "SELECT  EPSACH.sp_monitoreo_alarmas_detalles_observacion($id) FROM dual";
    $observaciones = DB::select($query);

   return view('monitoreo.alarmas.mo02_s_detalles')
    ->with('alarma', $alarma)
    ->with('observaciones', $observaciones);
  }

  public function edit($id)
  {
    $query = "SELECT  EPSACH.sp_monitoreo_alarmas_detalles($id) FROM dual";
    $alarma = DB::select($query);

    return view('configuracion.bancos.bancos_activos.show')
    ->with('alarma',$alarma);
  }
}
