<?php

namespace App\Http\Controllers\Monitoreo\ArchivosResumen\Liquidaciones;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Monitoreo\LiquidacionArchivos;

class LiquidacionesArchivosController extends Controller
{
    public function getIndex(Request $request)
    {
        //$query = "SELECT  sp_buscar_liquidaciones_archivos() FROM dual";

        $LiquidacionesArchivos = LiquidacionArchivos::get();
        foreach ($LiquidacionesArchivos as $liquidacion) 
        {
        	    
        $query = "SELECT  DISTINCT t_tipos_operaciones.codigo AS tipo_operacion_codigo ,LIQ.codigo|| '-' || LIQ.nombre as producto, t_tipos_operaciones.nombre AS tipo_operacion_nombre , t_tipos_operaciones.codigo || '-' ||  t_tipos_operaciones.nombre AS nombre_completo
                  FROM (SELECT t_intervalos_liquidacion.num_liquidacion, t_intervalos_liquidacion.producto_id , t_productos.codigo , t_productos.nombre
                              FROM t_intervalos_liquidacion 
                                    LEFT JOIN
                                              t_productos
                                              ON 
                                              t_productos.id = t_intervalos_liquidacion.producto_id
                                              WHERE (t_intervalos_liquidacion.num_liquidacion =  ('$liquidacion->num_liquidaciones') )) LIQ
                                                      LEFT JOIN t_tipos_operaciones 
                                                      ON 
                                                      LIQ.producto_id = t_tipos_operaciones.producto_id 
                                                            ORDER BY tipo_operacion_codigo";




         $operaciones = DB::select($query); 
         $liquidacion->operaciones = collect($operaciones);
         $queryy = "SELECT  DISTINCT codigo, nombre,  codigo|| '-' || nombre as producto
                  FROM (SELECT t_intervalos_liquidacion.num_liquidacion, t_intervalos_liquidacion.producto_id , t_productos.codigo , t_productos.nombre
                              FROM t_intervalos_liquidacion 
                                    LEFT JOIN
                                              t_productos
                                              ON 
                                              t_productos.id = t_intervalos_liquidacion.producto_id
                                              WHERE (t_intervalos_liquidacion.num_liquidacion =  ('$liquidacion->num_liquidaciones')))  ORDER BY codigo ";

        $productos = DB::select($queryy); 
        $liquidacion->productos = collect($productos);
         }
        return view('monitoreo.archivos_resumen.liquidaciones.mo39_i_liquidaciones')
        ->with('LiquidacionesArchivos',$LiquidacionesArchivos );
    }
}
