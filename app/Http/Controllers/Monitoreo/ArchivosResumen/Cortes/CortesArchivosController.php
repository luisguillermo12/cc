<?php

namespace App\Http\Controllers\Monitoreo\ArchivosResumen\Cortes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\EjecucionAdministrativa\RecepcionArchivos\RecepcionArchivos;
use App\Models\Configuracion\MedioPago\TipoOperacion;
 use Carbon\Carbon;

class CortesArchivosController extends Controller
{
    public function getIndex(Request $request)
    {
        $CortesArchivos= null;

        $CortesArchivos = RecepcionArchivos::select(DB::raw("SUBSTR(t_horarios_recepcion_archivos.fecha_hora_fin,0,8) as fecha"), DB::raw("SUBSTR (t_horarios_recepcion_archivos.fecha_hora_fin,9,5) as hora"),'estatus_corte as estatus','t_tipos_operaciones.nombre','t_tipos_operaciones.codigo as cod_tipo_operacion', 'fecha_hora_fin as fecha_hora_ejecucion', 'corte_0')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_horarios_recepcion_archivos.id_tipo_operacion')
        ->orderBy('fecha_hora_ejecucion')
        ->get();

        $CorteFinal = $CortesArchivos->last();
        $CorteFinal->estatus = is_null($CortesArchivos[count($CortesArchivos)-2]->corte_0) ? 'POR ENVIAR' : 'ENVIADO';
        $CortesArchivos->push($CorteFinal);
        $aux =TipoOperacion::where('codigo',$CortesArchivos[2]->cod_tipo_operacion)->get();  
        

        foreach ($CortesArchivos as $corte) {

            $corte->producto = TipoOperacion::where('codigo',$corte->cod_tipo_operacion)->first()->producto;
            
        }

        $CortesArchivos_new = RecepcionArchivos::select('corte_id', 'fecha_hora_fin', 'estatus_corte', 'id_tipo_operacion', DB::raw("SUBSTR(fecha_hora_fin,0,8) as fecha"), DB::raw("SUBSTR (fecha_hora_fin,9,6) as hora") )->get();
            foreach($CortesArchivos_new as $cortes){

                $cortes->cod_tipo_operacion = TipoOperacion::where('id' , $cortes->id_tipo_operacion)->first()->codigo;
                $cortes->nom_tipo_operacion = TipoOperacion::where('id' , $cortes->id_tipo_operacion)->first()->nombre;
            }
            
            foreach($CortesArchivos_new as $cortes){

                $cortes->cod_producto = TipoOperacion::where('id' , $cortes->id_tipo_operacion)->first()->producto->codigo;
                $cortes->nom_producto = TipoOperacion::where('id' , $cortes->id_tipo_operacion)->first()->producto->nombre;
            }


        $corte_0=clone $CortesArchivos_new[count($CortesArchivos_new)-1];
        $corte_0->corte_id = 0;
        $corte_0->id_tipo_operacion=$CortesArchivos_new->pluck('id_tipo_operacion')->implode(', ');
        $corte_0->cod_tipo_operacion =$CortesArchivos_new->pluck('cod_tipo_operacion')->implode(', ');
        $corte_0->nom_tipo_operacion =$CortesArchivos_new->pluck('nom_tipo_operacion')->implode(', ');
        $corte_0->cod_producto=$CortesArchivos_new->pluck('cod_producto')->implode(', ');
        $corte_0->nom_producto=$CortesArchivos_new->pluck('nom_producto')->implode(', ');
        

        $val_cor_0 = 0;
        $cortes_recep =  RecepcionArchivos::select('corte_0')->get();
          foreach ($cortes_recep as $regis) {

                    if ($regis->corte_0 != null){
                                $val_cor_0 = $val_cor_0+1;          
                   }
         }

        if ( $val_cor_0 >0 ){ 
                             $corte_0->estatus_corte = 'ENVIADO';
                             }else{
                                    $corte_0->estatus_corte = 'POR ENVIAR';
                            }
        
        $CortesArchivos_new->push($corte_0);

      
     




        return view('monitoreo.archivos_resumen.cortes.mo38_i_cortes')
        ->with('CortesArchivos_new', $CortesArchivos_new)
        ->with('CortesArchivos', $CortesArchivos)
        ->with('CorteFinal', $CorteFinal);
	}
}