<?php

namespace App\Http\Controllers\Monitoreo\Liquidacion\Multilateral\Liquidado;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Monitoreo\LiquidacionArchivos;
use App\Models\Monitoreo\LiquidacionArchivosDetalle;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use DB;
use Jenssegers\Date\Date;

class MultilateraliqpController extends Controller
{
  public function __construct()
  {
    Date::setLocale('es');
  }
  public function getIndex(Request $request)
  {
  //SIGNO
    $signos = [
        '*' => '*',
        'C' => 'DEBITO',
        'D' => 'CREDITO',
    ];
    $num_liquidacion=1;
    // MONEDAS
      $moneda_activa = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
      ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
      ->where('t_parametros_sistema.id', '35')
      ->where('t_monedas.estatus','ACTIVO')
      ->first();

      $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
      ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
      ->where('t_productos.estatus', 'ACTIVO')
      ->orderBy('t_productos.codigo', 'asc')
      ->distinct()
      ->lists('t_productos.nombre','t_productos.codigo')
       ->prepend('*', '0');



    if($request->get('producto') != null) {


      $signo = $request->get('signo');
    

      if ($signo != '*') {
        $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_tipos_operaciones.signo', $signo)
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.codigo')
        ->prepend('*', '0');
      } else {
        $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.codigo')
        ->prepend('*', '0');
      }

      $producto = $request->get('producto');
      

      if(count($producto) > 1) {
        $producto = implode(",", $producto);
      } else {
        $producto = implode($producto);
      }

  
    } else {
      $signo = '*';
      $producto = '0';
      
    }

  

switch ([$signo, $producto]) {
    case ['*', '0']:
        $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                      LEFT JOIN
                      (select participante as codigo, sum (posicion)as posicion from 
                      (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                          from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'SETTLED'
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'SETTLED'
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)       
                      group by participante
                      union all 
                        select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'SETTLED'
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'SETTLED'
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante
                      union all 
                        select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                 from   t_bancos t1, t_transacciones_cheques t2
                                 where  t1.estatus = 'ACTIVO'
                                 and    t2.estatus = 'SETTLED'
                                 and    t1.CODIGO = t2.cod_banco_emisor
                                 group by t1.codigo
                      union all 
                                select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_cheques t2
                                where  t1.estatus = 'ACTIVO'  
                                and    t2.estatus = 'SETTLED'
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante)
                      group by participante
                      order by participante) tr
            on tr.codigo = t_bancos.codigo
           WHERE t_bancos.estatus= 'ACTIVO'
           order by codigo";
        break;
    case  ['*', '010']:
        $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                                LEFT JOIN
                                (select participante as codigo, sum (posicion)as posicion from 
                                (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                                    from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = 'SETTLED'
                                          and    t1.CODIGO = t2.cod_banco_emisor
                                          group by t1.codigo
                                      union all
                                          select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = 'SETTLED'
                                          and    t1.CODIGO = t2.cod_banco_destinatario
                                          group by t1.codigo)       
                                group by participante)
                                group by participante
                                order by participante) tr
                                on tr.codigo = t_bancos.codigo
                                WHERE t_bancos.estatus= 'ACTIVO'
                              order by codigo";
        break;
    case ['*', '020']:
        $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                          LEFT JOIN
                          (select participante as codigo, sum (posicion)as posicion from 
                          ( select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                              from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                    from   t_bancos t1, t_transacciones_domiciliaciones t2
                                    where  t1.estatus = 'ACTIVO'
                                    and    t2.estatus = 'SETTLED'
                                    and    t1.CODIGO = t2.cod_banco_emisor
                                    group by t1.codigo
                                union all
                                    select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                    from   t_bancos t1, t_transacciones_domiciliaciones t2
                                    where  t1.estatus = 'ACTIVO'
                                    and    t2.estatus = 'SETTLED'
                                    and    t1.CODIGO = t2.cod_banco_destinatario
                                    group by t1.codigo)
                          group by participante)
                          group by participante
                          order by participante) tr
                          on tr.codigo = t_bancos.codigo
                          WHERE t_bancos.estatus= 'ACTIVO'
                        order by codigo";
          break;

    case ['*', '030']:
        $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                            LEFT JOIN
                            (select participante as codigo, sum (posicion)as posicion from 
                            (select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                                from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                       from   t_bancos t1, t_transacciones_cheques t2
                                       where  t1.estatus = 'ACTIVO'
                                       and    t2.estatus = 'SETTLED'
                                       and    t1.CODIGO = t2.cod_banco_emisor
                                       group by t1.codigo
                            union all 
                                      select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                      from   t_bancos t1, t_transacciones_cheques t2
                                      where  t1.estatus = 'ACTIVO'  
                                      and    t2.estatus = 'SETTLED'
                                      and    t1.CODIGO = t2.cod_banco_destinatario
                                      group by t1.codigo)
                            group by participante)
                            group by participante
                            order by participante) tr
                            on tr.codigo = t_bancos.codigo
                            WHERE t_bancos.estatus= 'ACTIVO'
                          order by codigo";
        break;

     case ['D', '0']:

     $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                                LEFT JOIN
                                (select participante as codigo, sum (posicion)as posicion from 
                                (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                                    from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = 'SETTLED'
                                          and    t1.CODIGO = t2.cod_banco_emisor
                                          group by t1.codigo
                                      union all
                                          select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = 'SETTLED'
                                          and    t1.CODIGO = t2.cod_banco_destinatario
                                          group by t1.codigo)       
                                group by participante)
                                group by participante
                                order by participante) tr
                                on tr.codigo = t_bancos.codigo
                                WHERE t_bancos.estatus= 'ACTIVO'
                              order by codigo";
        break;

     case ['D', '010']:

     $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                                LEFT JOIN
                                (select participante as codigo, sum (posicion)as posicion from 
                                (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                                    from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = 'SETTLED'
                                          and    t1.CODIGO = t2.cod_banco_emisor
                                          group by t1.codigo
                                      union all
                                          select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = 'SETTLED'
                                          and    t1.CODIGO = t2.cod_banco_destinatario
                                          group by t1.codigo)       
                                group by participante)
                                group by participante
                                order by participante) tr
                                on tr.codigo = t_bancos.codigo
                                WHERE t_bancos.estatus= 'ACTIVO'
                              order by codigo";
        break;
      
    case ['C', '0']:

     $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                        LEFT JOIN
                        (select participante as codigo, sum (posicion)as posicion from 
                        (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                            from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                  from   t_bancos t1, t_transacciones_domiciliaciones t2
                                  where  t1.estatus = 'ACTIVO'
                                  and    t2.estatus = 'SETTLED'
                                  and    t1.CODIGO = t2.cod_banco_emisor
                                  group by t1.codigo
                              union all
                                  select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                  from   t_bancos t1, t_transacciones_domiciliaciones t2
                                  where  t1.estatus = 'ACTIVO'
                                  and    t2.estatus = 'SETTLED'
                                  and    t1.CODIGO = t2.cod_banco_destinatario
                                  group by t1.codigo)
                        group by participante
                        union all 
                          select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                            from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                   from   t_bancos t1, t_transacciones_cheques t2
                                   where  t1.estatus = 'ACTIVO'
                                   and    t2.estatus = 'SETTLED'
                                   and    t1.CODIGO = t2.cod_banco_emisor
                                   group by t1.codigo
                        union all 
                                  select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                  from   t_bancos t1, t_transacciones_cheques t2
                                  where  t1.estatus = 'ACTIVO'  
                                  and    t2.estatus = 'SETTLED'
                                  and    t1.CODIGO = t2.cod_banco_destinatario
                                  group by t1.codigo)
                        group by participante)
                        group by participante
                        order by participante) tr
                        on tr.codigo = t_bancos.codigo
                        WHERE t_bancos.estatus= 'ACTIVO'
                      order by codigo";
        break;

    case ['C', '020']:
         $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                          LEFT JOIN
                          (select participante as codigo, sum (posicion)as posicion from 
                          ( select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                              from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                    from   t_bancos t1, t_transacciones_domiciliaciones t2
                                    where  t1.estatus = 'ACTIVO'
                                    and    t2.estatus = 'SETTLED'
                                    and    t1.CODIGO = t2.cod_banco_emisor
                                    group by t1.codigo
                                union all
                                    select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                    from   t_bancos t1, t_transacciones_domiciliaciones t2
                                    where  t1.estatus = 'ACTIVO'
                                    and    t2.estatus = 'SETTLED'
                                    and    t1.CODIGO = t2.cod_banco_destinatario
                                    group by t1.codigo)
                          group by participante)
                          group by participante
                          order by participante) tr
                          on tr.codigo = t_bancos.codigo
                          WHERE t_bancos.estatus= 'ACTIVO'
                        order by codigo";
          break;


    case ['C', '030']:
         $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                            LEFT JOIN
                            (select participante as codigo, sum (posicion)as posicion from 
                            (select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                                from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                       from   t_bancos t1, t_transacciones_cheques t2
                                       where  t1.estatus = 'ACTIVO'
                                       and    t2.estatus = 'SETTLED'
                                       and    t1.CODIGO = t2.cod_banco_emisor
                                       group by t1.codigo
                            union all 
                                      select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                      from   t_bancos t1, t_transacciones_cheques t2
                                      where  t1.estatus = 'ACTIVO'  
                                      and    t2.estatus = 'SETTLED'
                                      and    t1.CODIGO = t2.cod_banco_destinatario
                                      group by t1.codigo)
                            group by participante)
                            group by participante
                            order by participante) tr
                            on tr.codigo = t_bancos.codigo
                            WHERE t_bancos.estatus= 'ACTIVO'
                          order by codigo";
        break;

    default:
        $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                      LEFT JOIN
                      (select participante as codigo, sum (posicion)as posicion from 
                      (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                          from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'PICKED_U'
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'PICKED_U'
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)       
                      group by participante
                      union all 
                        select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'PICKED_U'
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = 'PICKED_U'
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante
                      union all 
                        select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                 from   t_bancos t1, t_transacciones_cheques t2
                                 where  t1.estatus = 'ACTIVO'
                                 and    t2.estatus = 'PICKED_U'
                                 and    t1.CODIGO = t2.cod_banco_emisor
                                 group by t1.codigo
                      union all 
                                select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_cheques t2
                                where  t1.estatus = 'ACTIVO'  
                                and    t2.estatus = 'PICKED_U'
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante)
                      group by participante
                      order by participante) tr
            on tr.codigo = t_bancos.codigo
           WHERE t_bancos.estatus= 'ACTIVO'
          order by codigo";
        break;
}

    $posiciones = DB::select($query);
    $total_positivos=0;
    $total_negativos=0;

      foreach ($posiciones as $posicion) {

                     if($posicion->posicion>0){
                                $total_positivos=$total_positivos+$posicion->posicion;
                      }
                         else{
                                $total_negativos=$total_negativos+$posicion->posicion;
                         }
       }

    return view('monitoreo.liquidacion.multilateral.liquidado.mo35_i_liquidado')
    ->with('posiciones', $posiciones)
    ->with('total_positivos', $total_positivos)
    ->with('total_negativos', $total_negativos)
    ->with('moneda_activa', $moneda_activa)
    ->with('signos', $signos)
    ->with('signo', $signo)
    ->with('producto', $producto)
    ->with('productos', $productos);

  }

  public function producto(Request $request, $signo)
  {
        $signo = Producto::producto($signo);

        return response()->json($signo);
  }


}
