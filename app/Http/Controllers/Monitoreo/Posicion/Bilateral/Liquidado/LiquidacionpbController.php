<?php

namespace App\Http\Controllers\Monitoreo\Liquidacion\Bilateral\Liquidado;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Monitoreo\LiquidacionArchivos;
use App\Models\Monitoreo\LiquidacionArchivosDetalle;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use DB;
use Jenssegers\Date\Date;
 use Illuminate\Support\Collection;

class LiquidacionpbController extends Controller
{
  public function __construct()
  {
    Date::setLocale('es');
  }

  public function getIndex(Request $request)
  {
       //SIGNO
    $signos = [
        '*' => '*',
        'D' => 'CREDITO',
        'C' => 'DEBITO',
    ];

    // BANCOS
    $bancos = Banco::buscarTodos('Seleccione', '*');

    $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
    ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
    ->where('t_productos.estatus', 'ACTIVO')
    ->orderBy('t_productos.codigo', 'asc')
    ->distinct()
    ->lists('t_productos.nombre','t_productos.codigo')
     ->prepend('*', '0');

    // MONEDAS
      $moneda_activa = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
      ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
      ->where('t_parametros_sistema.id', '35')
      ->where('t_monedas.estatus','ACTIVO')
      ->first();

    // LIQUIDACIONES
    $query = "SELECT EPSACH.sp_lista_liquidaciones_productos() FROM dual";
    $liquidaciones = DB::select($query);

    $liquidaciones = array_add($liquidaciones, 0, 'Seleccione');
    $liquidaciones = array_sort_recursive($liquidaciones);

    //$moneda = $moneda_activa->codigo_iso;
    $posicion_bilateral = null;
    $posicion_bilateral_sumapa = null;
    $total = 0;
    $total2 = 0;

    if ($request->get('cod_banco') != null || $request->get('cod_banco_receptor') != null || $request->get('producto') != null) {

      // Asignación De Variables
      $signo = $request->get('signo');
      $banco = $request->get('cod_banco');
      $banco_receptor = $request->get('cod_banco_receptor');

      if ($signo != '*') {
        $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_tipos_operaciones.signo', $signo)
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.codigo')
         ->prepend('*', '0');
      } else {
        $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.codigo')
        ->prepend('*', '0');
      }

      $producto = $request->get('producto');
      if(count($producto) > 1) {
        $producto = implode(",", $producto);
      } else {
        $producto = implode($producto);
      }
     
      $banco_destinatario = Banco::select('t_bancos.codigo','t_bancos.nombre')
      ->where('t_bancos.codigo', $banco_receptor)
      ->first();

      $banco_emisor = Banco::select('t_bancos.codigo','t_bancos.nombre')
      ->where('t_bancos.codigo', $banco)
      ->first();

    } else {
      $liquidacion = 0;
      $banco_destinatario = '';
      $banco = '';
      $banco_receptor = '';
      $banco_emisor = '';
      $producto = '0';
      $signo = '*';
    }
$sub_total_positivo_presentadas_emi = 0;
$sub_total_negativo_presentadas_emi = 0;
$sub_total_positivo_presentadas_des = 0;
$sub_total_negativo_presentadas_des = 0;

$sub_total_positivo_devueltas_emi = 0;
$sub_total_negativo_devueltas_emi = 0;
$sub_total_positivo_devueltas_des = 0;
$sub_total_negativo_devueltas_des = 0;

    switch ([$signo, $producto]) {
    case ['*', '0']:

         $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
          $query_1_1 ="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";       


        $query_2_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor =  ('$banco_receptor')   AND COD_BANCO_DESTINATARIO =('$banco') and COD_TIPO_OPERACION IN('010')
                              union all 
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco')   and COD_TIPO_OPERACION IN('020')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO  = ('$banco')  and COD_TIPO_OPERACION IN('030')";



        $query_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('010')
                              union all 
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('020')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('030')";


 $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('110')
                              union all 
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('120')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('130')";



  $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('110')
                              union all 
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('120')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('130')";


        break;

    case  ['*', '010']:
       $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                          )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
       

        $query_1_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                          )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";



        $query_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('010')";



        $query_2_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') and COD_TIPO_OPERACION IN('010')";
        

        $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('110')";



       $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('110')";



       break;

    case ['*', '020']:
        $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado)
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";


          $query_1_1 ="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                      
                                            )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";       


        $query_2_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco')   and COD_TIPO_OPERACION IN('020')";



        $query_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('020')";


 $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('120')";



  $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('120')";



          break;

    case ['*', '030']:
          $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
          $query_1_1 ="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";         


        $query_2_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO  = ('$banco')  and COD_TIPO_OPERACION IN('030')";



        $query_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('030')";


 $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('130')";



  $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('130')";



        break;

     case ['D', '0']:

      $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                          )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
       

        $query_1_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                          )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";



        $query_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('010')";



        $query_2_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') and COD_TIPO_OPERACION IN('010')";
        

        $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('110')";



       $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('110')";


      break;



     case ['D', '010']:

      $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                          )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
       

        $query_1_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                          )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";



        $query_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('010')";



        $query_2_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') and COD_TIPO_OPERACION IN('010')";
        

        $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('110')";



       $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_creditos_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('110')";
      break;
      
    case ['C', '0']:

         $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
          $query_1_1 ="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";       


        $query_2_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco')   and COD_TIPO_OPERACION IN('020')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO  = ('$banco')  and COD_TIPO_OPERACION IN('030')";



        $query_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('020')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('030')";


 $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('120')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('130')";



  $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('120')
                              union all
                              SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('130')";


        break;

    case ['C', '020']:
            $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado)
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";


          $query_1_1 ="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                      
                                            )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";       


        $query_2_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco')   and COD_TIPO_OPERACION IN('020')";



        $query_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('020')";


 $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('120')";



  $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('120')";

     break;

    case ['C', '030']:
            $query_1="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
          $query_1_1 ="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (POSICION_BILATERAL) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_cheques_cortes_liquidado
                                                                           )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor =('$banco_receptor')  AND COD_BANCO_DESTINATARIO = ('$banco') ";         


        $query_2_2= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO  = ('$banco')  and COD_TIPO_OPERACION IN('030')";



        $query_2="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('030')";


 $query_3= "SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor') and COD_TIPO_OPERACION IN('130')";



  $query_3_3="SELECT cod_tipo_operacion ,cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                    FROM v_posicion_bilateral_cheques_cortes_liquidado
                                            WHERE cod_banco_emisor = ('$banco_receptor') AND COD_BANCO_DESTINATARIO = ('$banco')  and COD_TIPO_OPERACION IN('130')";


        break;

    default:
        $query="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_bilateral) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_creditos_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL
                                                                          FROM v_posicion_bilateral_domiciliaciones_cortes_liquidado
                                                                           
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_BILATERAL_cortes_liquidado
                                                                          FROM v_posicion_bilateral_cheques
                                                                            )
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = ('$banco') AND COD_BANCO_DESTINATARIO = ('$banco_receptor')";
}
  $posicion_bilateral_new_1 = DB::select($query_1);


          if (count($posicion_bilateral_new_1)>0){ 
                  foreach ($posicion_bilateral_new_1 as $registro) {
                      $registro->nombre_banco_emisor = Banco::where('codigo', $registro->cod_banco_emisor)->first()->nombre;
                      $registro->nombre_banco_destinatario = Banco::where('codigo', $registro->cod_banco_destinatario)->first()->nombre;
                  }
          }


   $posicion_bilateral_new_1_1 = DB::select($query_1_1);

           if (count($posicion_bilateral_new_1_1)>0){ 
                  foreach ($posicion_bilateral_new_1_1 as $registro) {
                      $registro->nombre_banco_emisor = Banco::where('codigo', $registro->cod_banco_emisor)->first()->nombre;
                      $registro->nombre_banco_destinatario = Banco::where('codigo', $registro->cod_banco_destinatario)->first()->nombre;
                  }
          }


    $posicion_bilateral_presenta_new_2 = DB::select($query_2);
    $posicion_bilateral_presenta_new_2 = Collection::make($posicion_bilateral_presenta_new_2);
    $posicion_bilateral_presenta_new_2_2 = DB::select($query_2_2);
    $posicion_bilateral_presenta_new_2_2 = Collection::make($posicion_bilateral_presenta_new_2_2);

    
 // sub totales de emisor de operaciones presentadas
     if (count($posicion_bilateral_presenta_new_2)>0){ 
         foreach($posicion_bilateral_presenta_new_2 as $registro){
          
            if ($registro->posicion_bilateral >0 ){ 
              $sub_total_positivo_presentadas_emi = $registro->posicion_bilateral+$sub_total_positivo_presentadas_emi;
            }else{
               $sub_total_negativo_presentadas_emi = $registro->posicion_bilateral+$sub_total_negativo_presentadas_emi; 
            }

         }
     }
// sub totales de destinatario de operaciones presentadas
    if (count($posicion_bilateral_presenta_new_2_2)>0){ 
         foreach($posicion_bilateral_presenta_new_2_2 as $registro){
          
            if ($registro->posicion_bilateral >0 ){ 
              $sub_total_positivo_presentadas_des = $registro->posicion_bilateral+$sub_total_positivo_presentadas_des;
            }else{
               $sub_total_negativo_presentadas_des = $registro->posicion_bilateral+$sub_total_negativo_presentadas_des; 
            }

         }
     }

    $posicion_bilateral_devuelta_new_3 = DB::select($query_3);
    $posicion_bilateral_devuelta_new_3 = Collection::make($posicion_bilateral_devuelta_new_3);
    $posicion_bilateral_devuelta_new_3_3 = DB::select($query_3_3);
     $posicion_bilateral_devuelta_new_3_3 = Collection::make($posicion_bilateral_devuelta_new_3_3);

  // sub totales de emisor de operaciones devueltas
     if (count($posicion_bilateral_devuelta_new_3)>0){ 
         foreach($posicion_bilateral_devuelta_new_3 as $registro){
          
            if ($registro->posicion_bilateral >0 ){ 
              $sub_total_positivo_devueltas_emi = $registro->posicion_bilateral+$sub_total_positivo_devueltas_emi;
            }else{
               $sub_total_negativo_devueltas_emi = $registro->posicion_bilateral+$sub_total_negativo_devueltas_emi; 
            }

         }
     }
  // sub totales de destinatario de operaciones devueltas

        if (count($posicion_bilateral_devuelta_new_3_3)>0){ 
         foreach($posicion_bilateral_devuelta_new_3_3 as $registro){
          
            if ($registro->posicion_bilateral >0 ){ 
              $sub_total_positivo_devueltas_des = $registro->posicion_bilateral+$sub_total_positivo_devueltas_des;
            }else{
               $sub_total_negativo_devueltas_des = $registro->posicion_bilateral+$sub_total_negativo_devueltas_des; 
            }

         }
     }



    return view('monitoreo.liquidacion.bilateral.liquidado.mo33_i_liquidado')
    ->with('posicion_bilateral_new_1', $posicion_bilateral_new_1)
    ->with('posicion_bilateral_new_1_1', $posicion_bilateral_new_1_1)
    ->with('posicion_bilateral_presenta_new_2', $posicion_bilateral_presenta_new_2)
    ->with('posicion_bilateral_presenta_new_2_2', $posicion_bilateral_presenta_new_2_2)
    ->with('posicion_bilateral_devuelta_new_3', $posicion_bilateral_devuelta_new_3)
    ->with('posicion_bilateral_devuelta_new_3_3', $posicion_bilateral_devuelta_new_3_3)
    ->with('sub_total_positivo_presentadas_emi', $sub_total_positivo_presentadas_emi)
    ->with('sub_total_negativo_presentadas_emi', $sub_total_negativo_presentadas_emi)
    ->with('sub_total_positivo_devueltas_emi', $sub_total_positivo_devueltas_emi)
    ->with('sub_total_negativo_devueltas_emi', $sub_total_negativo_devueltas_emi)
    ->with('sub_total_positivo_presentadas_des', $sub_total_positivo_presentadas_des)
    ->with('sub_total_negativo_presentadas_des', $sub_total_negativo_presentadas_des)
    ->with('sub_total_positivo_devueltas_des', $sub_total_positivo_devueltas_des)
    ->with('sub_total_negativo_devueltas_des', $sub_total_negativo_devueltas_des)


    ->with('signos', $signos)
    ->with('signo', $signo)
    ->with('banco', $banco)
    ->with('banco', $banco)
    ->with('banco_receptor', $banco_receptor)
    ->with('bancos', $bancos)
    ->with('producto', $producto)
    ->with('productos', $productos)
   // ->with('moneda', $moneda)
    ->with('moneda_activa', $moneda_activa)
    ->with('liquidaciones', $liquidaciones)
    //->with('posicion_bilateral', $posicion_bilateral)
    //->with('total', $total)
    //->with('total2', $total2)
    ->with('banco_destinatario', $banco_destinatario)
    ->with('banco_emisor', $banco_emisor);
  }





  public function producto(Request $request, $signo)
  {
        $signo = Producto::producto($signo);

        return response()->json($signo);
  }

  public function liquidacion(Request $request, $signo, $producto)
  {
    if ($producto == '0')
    {
      $producto = LiquidacionArchivos::select('t_liquidaciones_archivos.id','t_liquidaciones_archivos.num_liquidaciones')
      ->join('t_liquidaciones_archivos_detalle','t_liquidaciones_archivos.id','=','t_liquidaciones_archivos_detalle.liquidacion_archivo_id')
		  ->join('t_productos','t_productos.id','=','t_liquidaciones_archivos_detalle.producto_id')
      ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
      ->where('t_productos.estatus', 'ACTIVO')
      ->where('t_tipos_operaciones.signo', $signo)
      ->orderBy('t_liquidaciones_archivos.id', 'asc')
      ->distinct()
      ->get();
    }
    else
    {
      $producto = LiquidacionArchivos::select('t_liquidaciones_archivos.id','t_liquidaciones_archivos.num_liquidaciones','t_productos.nombre')
      ->join('t_liquidaciones_archivos_detalle','t_liquidaciones_archivos.id','=','t_liquidaciones_archivos_detalle.liquidacion_archivo_id')
		  ->join('t_productos','t_productos.id','=','t_liquidaciones_archivos_detalle.producto_id')
      ->where('t_productos.estatus', 'ACTIVO')
		  ->where('t_productos.id', $producto)
      ->orderBy('t_liquidaciones_archivos.id', 'asc')
      ->distinct()
      ->get();
    }
        return response()->json($producto);
  }

 
  function calcularPosicionBilateral($posiciones_bilaterales) {

    $total_creditos = 0;
    $total_debitos = 0;
    $total = 0;

    foreach ($posiciones_bilaterales as $posicion_bilateral) {
      if ($posicion_bilateral->signo == 'C') {
        $total_creditos = $total_creditos + $posicion_bilateral->monto;
        
      } else {
        $total_debitos = $total_debitos + $posicion_bilateral->monto;

      }
    }

    $total = $total_creditos - $total_debitos;
    return $total;

  }
}