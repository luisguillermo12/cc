<?php

namespace App\Http\Controllers\Monitoreo\Liquidacion\Bilateral\Liquidado;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Monitoreo\LiquidacionArchivos;
use App\Models\Monitoreo\LiquidacionArchivosDetalle;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use DB;
use Jenssegers\Date\Date;


class LiquidacionpbController extends Controller
{
  public function __construct()
  {
    Date::setLocale('es');
  }

  public function getIndex(Request $request)
  {
       //SIGNO
    $signos = [
        '*' => '*',
        'D' => 'CREDITO',
        'C' => 'DEBITO',
    ];

    // BANCOS
    $bancos = Banco::buscarTodos('Seleccione', '*');

    $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
    ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
    ->where('t_productos.estatus', 'ACTIVO')
    ->orderBy('t_productos.codigo', 'asc')
    ->distinct()
    ->lists('t_productos.nombre','t_productos.id')
     ->prepend('*', '0');

    // MONEDAS
      $moneda_activa = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
      ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
      ->where('t_parametros_sistema.id', '35')
      ->where('t_monedas.estatus','ACTIVO')
      ->first();

    // LIQUIDACIONES
    $query = "SELECT EPSACH.sp_lista_liquidaciones_productos() FROM dual";
    $liquidaciones = DB::select($query);

    $liquidaciones = array_add($liquidaciones, 0, 'Seleccione');
    $liquidaciones = array_sort_recursive($liquidaciones);

    //$moneda = $moneda_activa->codigo_iso;
    $posicion_bilateral = null;
    $posicion_bilateral_sumapa = null;
    $total = 0;
    $total2 = 0;

    if ($request->get('cod_banco') != null || $request->get('cod_banco_receptor') != null || $request->get('producto') != null) {

      // Asignación De Variables
      $signo = $request->get('signo');
      $banco = $request->get('cod_banco');
      $banco_receptor = $request->get('cod_banco_receptor');

      if ($signo != '*') {
        $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_tipos_operaciones.signo', $signo)
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.id')
         ->prepend('*', '0');
      } else {
        $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.id')
        ->prepend('*', '0');
      }


      $producto = $request->get('producto');
      //dd(count($producto));
      if(count($producto) > 1) {
        $producto = implode(",", $producto);
      } else {
        $producto = implode($producto);
      }
      //dd($producto);
      $banco_destinatario = Banco::select('t_bancos.codigo','t_bancos.nombre')
      ->where('t_bancos.codigo', $banco_receptor)
      ->first();

      $banco_emisor = Banco::select('t_bancos.codigo','t_bancos.nombre')
      ->where('t_bancos.codigo', $banco)
      ->first();

     $query = "SELECT EPSACH.sp_monitoreo_posiciones_bilaterales_liquidado('".$banco."','".$banco_receptor."','".$signo."','".$producto."') FROM dual";
      //$query = "SELECT * FROM      sp_monitoreo_posiciones_bilaterales('".$banco."','".$banco_receptor."','".$moneda."','".$producto."')";
      $posicion_bilateral = DB::select($query);
     // dd($posicion_bilateral);
      $total = number_format($this->calcularPosicionBilateral($posicion_bilateral),0,'','');

      $query = "SELECT EPSACH.sp_monitoreo_posiciones_bilaterales_liquidado('".$banco_receptor."','".$banco."','".$signo."','".$producto."') FROM dual";
      //$query = "SELECT * FROM sp_monitoreo_posiciones_bilaterales('".$banco_receptor."','".$banco."','".$moneda."','".$producto."')";
      $posicion_bilateral = DB::select($query);
     // dd($posicion_bilateral);
      $total2 = number_format($this->calcularPosicionBilateral($posicion_bilateral),0,'','');

      // dd($total);

    } else {
      $liquidacion = 0;
      $banco_destinatario = '';
      $banco = '';
      $banco_receptor = '';
      $banco_emisor = '';
      $producto = '0';
      $signo = '*';
    }
    
        $query="SELECT cod_banco_emisor, cod_banco_destinatario,posicion FROM
                          (SELECT cod_banco_emisor, cod_banco_destinatario, sum (posicion_emisor) AS posicion  FROM 
                                          (SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_EMISOR
                                                                          FROM v_posicion_bilateral_creditos
                                                                            WHERE estatus = 'SETTLED'
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_EMISOR
                                                                          FROM v_posicion_bilateral_domiciliaciones
                                                                            WHERE estatus = 'SETTLED'
                                            UNION ALL
                                            SELECT cod_banco_emisor, cod_banco_destinatario, POSICION_EMISOR
                                                                          FROM v_posicion_bilateral_cheques
                                                                            WHERE estatus = 'SETTLED')
                          GROUP BY  cod_banco_emisor, cod_banco_destinatario)
              WHERE cod_banco_emisor = '0001' AND COD_BANCO_DESTINATARIO = '0108'";

        $posicion_bilateral_new = DB::select($query);


    return view('monitoreo.liquidacion.bilateral.liquidado.mo33_i_liquidado')
    ->with('signos', $signos)
    ->with('signo', $signo)
    ->with('banco', $banco)
    ->with('banco', $banco)
    ->with('banco_receptor', $banco_receptor)
    ->with('bancos', $bancos)
    ->with('producto', $producto)
    ->with('productos', $productos)
   // ->with('moneda', $moneda)
    ->with('moneda_activa', $moneda_activa)
    ->with('liquidaciones', $liquidaciones)
    ->with('posicion_bilateral', $posicion_bilateral)
    ->with('total', $total)
    ->with('total2', $total2)
    ->with('banco_destinatario', $banco_destinatario)
    ->with('banco_emisor', $banco_emisor);
  }





  public function producto(Request $request, $signo)
  {
        $signo = Producto::producto($signo);

        return response()->json($signo);
  }

  public function liquidacion(Request $request, $signo, $producto)
  {
    if ($producto == '0')
    {
      $producto = LiquidacionArchivos::select('t_liquidaciones_archivos.id','t_liquidaciones_archivos.num_liquidaciones')
      ->join('t_liquidaciones_archivos_detalle','t_liquidaciones_archivos.id','=','t_liquidaciones_archivos_detalle.liquidacion_archivo_id')
		  ->join('t_productos','t_productos.id','=','t_liquidaciones_archivos_detalle.producto_id')
      ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
      ->where('t_productos.estatus', 'ACTIVO')
      ->where('t_tipos_operaciones.signo', $signo)
      ->orderBy('t_liquidaciones_archivos.id', 'asc')
      ->distinct()
      ->get();
    }
    else
    {
      $producto = LiquidacionArchivos::select('t_liquidaciones_archivos.id','t_liquidaciones_archivos.num_liquidaciones','t_productos.nombre')
      ->join('t_liquidaciones_archivos_detalle','t_liquidaciones_archivos.id','=','t_liquidaciones_archivos_detalle.liquidacion_archivo_id')
		  ->join('t_productos','t_productos.id','=','t_liquidaciones_archivos_detalle.producto_id')
      ->where('t_productos.estatus', 'ACTIVO')
		  ->where('t_productos.id', $producto)
      ->orderBy('t_liquidaciones_archivos.id', 'asc')
      ->distinct()
      ->get();
    }
        return response()->json($producto);
  }

 
  function calcularPosicionBilateral($posiciones_bilaterales) {

    $total_creditos = 0;
    $total_debitos = 0;
    $total = 0;

    foreach ($posiciones_bilaterales as $posicion_bilateral) {
      if ($posicion_bilateral->signo == 'C') {
        $total_creditos = $total_creditos + $posicion_bilateral->monto;
        
      } else {
        $total_debitos = $total_debitos + $posicion_bilateral->monto;

      }
    }

    $total = $total_creditos - $total_debitos;
    //dd($total);
    return $total;

  }
}