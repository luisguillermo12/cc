<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Rechazados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\TipoArchivo\TipoArchivo;
use App\Models\Monitoreo\ArchivosRechazados;
use App\Models\Monitoreo\ValidacionesEPS;
use App\Models\Monitoreo\ValidacionDetalle;
use App\Models\Monitoreo\TransferenciaArchivo;
use DB;

class ArchivosRechazadosController extends Controller
{

    public function getIndex(Request $request)
    {
      //FECHA DE INTERCAMBIO
      $cod_resultado = ValidacionDetalle::select('respuesta_codigo', 'id', 'codigo_descripcion')
      ->orderBy('respuesta_codigo', 'asc')
      ->where('nivel', 0)
      ->lists('respuesta_codigo','id')
      ->prepend('*', '*');

      // BANCOS
      $bancos = Banco::buscarTodos();

      // TIPOS DE ARCHIVOS
      $tipo_archivos = TipoArchivo::select('codigo','descripcion')
      ->Where('codigo', 'ICOM1')
      ->orWhere('codigo', 'ICOM2')
      ->orderBy('codigo', 'asc')
      ->lists('descripcion','codigo')
      ->prepend('*', '*');

      $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco'): '*';
      $tipo_archivo = $request->get('tipo_archivo') != null ? $request->get('tipo_archivo'): '*';
      $cod_rechazo = $request->get('cod_rechazo') != null ? $request->get('cod_rechazo') : '*';

      $ArchivosRechazados = ArchivosRechazados::select('t_archivos_rechazados.*', 't_transferencias_archivos.nombre', 't_transferencias_archivos.tipo', 't_validacion_detalle.respuesta_codigo as cod_rechazo', 't_validacion_detalle.codigo_descripcion as descripcion_rechazo')
      ->selectRaw('SUBSTR(t_transferencias_archivos.nombre, 1, 4) as cod_banco_emisor')
      ->join('t_transferencias_archivos', 't_transferencias_archivos.id', '=', 't_archivos_rechazados.transferencia_archivo_id')
      ->leftJoin('t_validacion_detalle', 't_validacion_detalle.id', '=', 't_archivos_rechazados.cod_validacion_eps_id')
      ->where(function ($query) use ($cod_banco) {
            if ($cod_banco != '*') {
                  $query->where('SUBSTR(t_transferencias_archivos.nombre, 1, 4)', '=', $cod_banco);
            }
      })->where(function ($query) use ($tipo_archivo) {
            if ($tipo_archivo != '*') {
                  $query->where('t_transferencias_archivos.tipo', '=', $tipo_archivo);
            }
      })->where(function ($query) use ($cod_rechazo) {
            if ($cod_rechazo != '*') {
                  $query->where('t_archivos_rechazados.cod_validacion_eps_id', '=', $cod_rechazo);
            }
      })
      ->get();

      //dd($ArchivosRechazados);

      $archivos_recibidos = TransferenciaArchivo::totalRecibidos('ICOM1', $cod_banco) + TransferenciaArchivo::totalRecibidos('ICOM2', $cod_banco);
      $totalmente_validados = TransferenciaArchivo::totalAceptados('ICOM1', $cod_banco)+ TransferenciaArchivo::totalAceptados('ICOM2', $cod_banco);
      $parcialmente_rechazados = TransferenciaArchivo::totalParcialmenteAceptados('ICOM1', $cod_banco) + TransferenciaArchivo::totalParcialmenteAceptados('ICOM2', $cod_banco);
      $totalmente_rechazados = TransferenciaArchivo::totalRechazados('ICOM1', $cod_banco) + TransferenciaArchivo::totalRechazados('ICOM2', $cod_banco);

      return view('monitoreo.archivos.operaciones.rechazados.mo31_i_rechazados')
      ->with('bancos', $bancos)
      ->with('cod_banco', $cod_banco)
      ->with('tipo_archivos', $tipo_archivos)
      ->with('tipo_archivo', $tipo_archivo)
      ->with('ArchivosRechazados', $ArchivosRechazados)
      ->with('cod_resultado', $cod_resultado)
      ->with('cod_rechazo', $cod_rechazo)
      ->with('archivos_recibidos', $archivos_recibidos)
      ->with('totalmente_validados', $totalmente_validados)
      ->with('parcialmente_rechazados', $parcialmente_rechazados)
      ->with('totalmente_rechazados', $totalmente_rechazados);
    }

}
