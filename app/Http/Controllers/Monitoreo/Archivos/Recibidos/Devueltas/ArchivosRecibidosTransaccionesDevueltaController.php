<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Recibidos\Devueltas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Monitoreo\FlujoLote;
use App\Models\Monitoreo\TransaccionCreditoLote;
use App\Models\Monitoreo\TransaccionDevCreditoLote;
use App\Models\Monitoreo\TransaccionChequeLote;
use App\Models\Monitoreo\TransaccionDevChequeLote;
use App\Models\Monitoreo\TransaccionDomiciliacionLote;
use App\Models\Monitoreo\TransaccionDevDomiciliacionLote;
use App\Models\Monitoreo\ValidacionesEPS;
use App\Models\Monitoreo\ValidacionDetalle;
use DB;

class ArchivosRecibidosTransaccionesDevueltaController extends Controller
{
    public function show($id)
    {
        // MONEDAS
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
       // ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
      //  ->where('t_parametros_sistema.id', '35')
        ->where('t_monedas.estatus','ACTIVO')
        ->first();

        $query = "SELECT EPSACH.sp_monitoreo_transaciones_registros('$id') FROM dual";
        $encarc_lote = DB::select($query);

       // dd($encarc_lote);

        foreach ($encarc_lote as $encarclote) {

            //BUSCAR LAS TRANSACCIONES DE CREDITOS DEVUELTOS DEL LOTE CONSULTADO
            if($encarclote->cod_tipo_operacion_lote == '110') {
                $query = "SELECT EPSACH.sp_monitoreo_transaciones_creditos_devuelto('$id') FROM dual";
                $transacciones = DB::select($query);
            }

            //BUSCAR LAS TRANSACCIONES DE DOMICILIACION DEVUELTOS DEL LOTE CONSULTADO
            if($encarclote->cod_tipo_operacion_lote == '120') {
                $query = "SELECT EPSACH.sp_monitoreo_transaciones_domiciliacion_devuelto('$id') FROM dual";
                $transacciones = DB::select($query);
            }

            //BUSCAR LAS TRANSACCIONES DE CHEQUE DEVUELTOS DEL LOTE CONSULTADO
            if($encarclote->cod_tipo_operacion_lote == '130') {
                $query = "SELECT EPSACH.sp_monitoreo_transaciones_cheques_devuelto('$id') FROM dual";
                $transacciones = DB::select($query);
            }
        }
         

                

        for ($x=0; $x<count($transacciones); $x++) {
            $transacciones[$x]->descripcion_resultado = ValidacionDetalle::descripcion(3, $transacciones[$x]->cod_rechazo);
        }

                //dd($transacciones);

        return view('monitoreo.archivos.operaciones.recibidos.devueltas.mo24_s_transacciones')
        ->with('monedas', $monedas)
        ->with('encarc_lote', $encarc_lote)
        ->with('transacciones', $transacciones);
    }
}
