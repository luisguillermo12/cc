<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Recibidos\Devueltas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Monitoreo\FlujoLote;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Monitoreo\ValidacionesEPS;
use DB;

class ArchivosRecibidosLotesDevueltaController extends Controller
{
    public function show(Request $request, $id)
    {
        $cod_tipo_operacion = $request->get('cod_tipo_operacion') != null ? $request->get('cod_tipo_operacion') : '*';
        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';

        // MONEDAS
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
        ->where('t_monedas.estatus','ACTIVO')
        ->first();

        // BANCOS
        $bancos = Banco::buscarTodos();

        // TIPO DE OPERACIONES
        $tipo_operaciones = TipoOperacion::select('codigo', DB::raw("CONCAT(codigo,nombre) AS nombre_completo"))
        ->where('estatus','ACTIVO')
        ->orderBy('codigo', 'asc')
        ->lists('nombre_completo','codigo')
        ->prepend('*', '*');

        $flujoInfo = FlujoInfo::select('t_transferencias_archivos.inicio_dt','t_flujos_info.*','t_codigos_validaciones_eps.descripcion')
        ->join('t_transferencias_archivos','t_transferencias_archivos.id','=','t_flujos_info.transferencia_archivo_in_id')
        ->join('t_codigos_validaciones_eps','t_flujos_info.cod_resultado','=','t_codigos_validaciones_eps.codigo')
        ->where('t_flujos_info.cod_flujo_info', '=', $id)
        ->first();

        $fecha_envio_icoma = FlujoInfo::select('t_transferencias_archivos.fin_dt')
        ->join('t_transferencias_archivos','t_transferencias_archivos.id','=','t_flujos_info.transferencia_archivo_out_id')
        ->where('t_flujos_info.transferencia_archivo_in_id', '=', $flujoInfo->transferencia_archivo_in_id)
        ->where('t_flujos_info.cod_flujo_tipo', '=', 'ICOMA')
        ->get()
        ->first();

        $flujoLotes = FlujoLote::select('t_flujos_lote.*','t_validacion_detalle.codigo_descripcion as descripcion')
        ->leftJoin('t_validacion_detalle', function ($join) {
            $join->on('t_validacion_detalle.respuesta_codigo','=','t_flujos_lote.cod_resultado')
            ->where('nivel', '=', 2);
        })
        ->where('t_flujos_lote.cod_flujo_info','=', $id)
        ->where(function ($query) use ($cod_banco) {
            if ($cod_banco != '*') {
                $query->where('t_flujos_lote.cod_banco_destinatario', $cod_banco);
            }
        })
        ->where(function ($query) use ($cod_tipo_operacion) {
            if ($cod_tipo_operacion != '*') {
                $query->where('t_flujos_lote.cod_tipo_operacion', $cod_tipo_operacion);
            }
        })
        ->orderBy('cod_banco_destinatario', 'ASC')
        ->orderBy('num_lote', 'ASC')
        ->get();

        return view('monitoreo.archivos.operaciones.recibidos.devueltas.mo23_s_lotes')
        ->with('monedas', $monedas)
        ->with('banco_destinatario', $cod_banco)
        ->with('bancos', $bancos)
        ->with('tipo_operacion', $cod_tipo_operacion)
        ->with('tipo_operaciones', $tipo_operaciones)
        ->with('flujo_lotes', $flujoLotes)
        ->with('flujo_info', $flujoInfo)
        ->with('fecha_envio_icoma', $fecha_envio_icoma);
    }
}
