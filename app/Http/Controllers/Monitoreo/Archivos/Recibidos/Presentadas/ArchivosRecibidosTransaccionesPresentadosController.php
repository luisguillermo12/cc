<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Recibidos\Presentadas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Monitoreo\FlujoLote;
use App\Models\Monitoreo\TransaccionCreditoLote;
use App\Models\Monitoreo\TransaccionDevCreditoLote;
use App\Models\Monitoreo\TransaccionChequeLote;
use App\Models\Monitoreo\TransaccionDevChequeLote;
use App\Models\Monitoreo\TransaccionDomiciliacionLote;
use App\Models\Monitoreo\TransaccionDevDomiciliacionLote;
use App\Models\Monitoreo\ValidacionesEPS;
use App\Models\Monitoreo\ValidacionDetalle;
use DB;

class ArchivosRecibidosTransaccionesPresentadosController extends Controller
{
    public function show($id)
    {

        // MONEDAS
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
        ->where('t_monedas.estatus','ACTIVO')
        ->first();

        $query = "SELECT EPSACH.sp_monitoreo_transaciones_registros('$id') FROM dual";
        $encarc_lote = DB::select($query);

        $lote = FlujoLote::where('cod_flujo_info', $id)->get();

        foreach ($encarc_lote as $encarclote) {

            // BUSCAR LAS TRANSACCIONES DE CREDITO DEL LOTE CONSULTADO
            if($encarclote->cod_tipo_operacion_lote == '010') {
                $query = "SELECT EPSACH.sp_monitoreo_transaciones_creditos_presentados('$id') FROM dual";
                $transacciones = DB::select($query);
            }

            // BUSCAR LAS TRANSACCIONES DE DOMICILIACION DEL LOTE CONSULTADO
            if($encarclote->cod_tipo_operacion_lote == '020') {
                $query = "SELECT EPSACH.sp_monitoreo_transaciones_domiciliacion_presentados('$id') FROM dual";
                $transacciones = DB::select($query);
            }

            // BUSCAR LAS TRANSACCIONES DE CHEQUE DEL LOTE CONSULTADO
            if($encarclote->cod_tipo_operacion_lote == '030') {
                $query = "SELECT EPSACH.sp_monitoreo_transaciones_cheques_presentados('$id') FROM dual";
                $transacciones = DB::select($query);
            }
        }

        for ($x=0; $x<count($transacciones); $x++) {
            $transacciones[$x]->descripcion_resultado = ValidacionDetalle::descripcion(3, $transacciones[$x]->cod_rechazo);
        }

        return view('monitoreo.archivos.operaciones.recibidos.presentadas.mo21_s_transacciones')
        ->with('monedas', $monedas)
        ->with('transacciones', $transacciones)
        ->with('encarc_lote', $encarc_lote);
    }
}
