<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Recibidos\Presentadas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Monitoreo\TransferenciaArchivo;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\TipoArchivo\TipoArchivo;
use App\Models\Monitoreo\ResultadosCodigos;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Monitoreo\ValidacionesEPS;
use App\Models\Monitoreo\ValidacionDetalle;
use App\Models\Monitoreo\FlujoLote;
use DB;
use Jenssegers\Date\Date;

class ArchivosRecibidosPresentadosController extends Controller
{
    public function getIndex(Request $request)
    {
        //FECHA DE INTERCAMBIO
        $parametros = DB::table('t_parametros_sistema')
        ->orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

        // TIPOS DE ARCHIVOSs
        $tipo_archivos = TipoArchivo::select('codigo','nombre')
        ->where('codigo','ICOM1')
        ->orWhere('codigo', 'ICOM2')
        ->orderBy('codigo', 'asc')
        ->lists('nombre','codigo')
        ->prepend('*', '*');

        $cod_resultados = [
            '*' => '*',
            '00' => '00 - ACEPTADOS',
            '98' => '98 - RECHAZADO',
            '99' => '99 - PARCIALMENTE ACEPTADOS',
        ];

        // BANCOS
        $bancos = Banco::buscarTodos();

        // MONEDAS
        $monedas = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo_iso', 'asc')
        ->lists('codigo_iso','codigo_iso')
        ->prepend('*', '*');

        $tipo_archivo = $request->get('cod_tipo_archivo') != null ? $request->get('cod_tipo_archivo') : '*';
        $banco = $request->get('cod_banco') != null ?$request->get('cod_banco') : '*';
        $moneda = $request->get('cod_moneda') != null ? $request->get('cod_moneda') : '*';
        $cod_resultado = $request->get('cod_resultado') != null ? $request->get('cod_resultado') : '*';
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : '*';

        /*
         * Listado de parámetros para T y T+1
         */
        $parametros = DB::table('t_parametros_sistema')
        ->select('valor')
        ->orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

        /*
         * Formatear cadena a formato de fecha
         */
        foreach ($parametros as $parametro) {
            $fechas[] = date( "Y-m-d", strtotime( $parametro->valor ) );
        };

        /*
         * Listado de bancarios y feriados
        */
        $calendario = DB::table('t_calendario')
        ->select('fecha_validacion')
        ->get();

        /*
         * Arreglo con las fechas de bancarios y feriados
         */
        $feriados = [];
        foreach ($calendario as $calendario) {
            $feriados[] = $calendario->fecha_validacion;
        };

        $archivos_recibidos = TransferenciaArchivo::totalRecibidos('ICOM1', $banco, null, $moneda);
        $totalmente_validados = TransferenciaArchivo::totalAceptados('ICOM1', $banco, null, $moneda);
        $parcialmente_rechazados = TransferenciaArchivo::totalParcialmenteAceptados('ICOM1', $banco, null, $moneda);
        $totalmente_rechazados = TransferenciaArchivo::totalRechazados('ICOM1', $banco, null, $moneda);

        $archivos = TransferenciaArchivo::archivosRecibidos('ICOM1', $banco, $moneda, $cod_resultado, $fecha);

        return view('monitoreo.archivos.operaciones.recibidos.presentadas.mo19_i_presentadas')
        ->with('tipo_archivo', $tipo_archivo)
        ->with('tipo_archivos', $tipo_archivos)
        ->with('banco', $banco)
        ->with('bancos', $bancos)
        ->with('moneda', $moneda)
        ->with('monedas', $monedas)
        ->with('cod_resultado', $cod_resultado)
        ->with('cod_resultados', $cod_resultados)
        ->with('archivos_recibidos', $archivos_recibidos)
        ->with('totalmente_validados', $totalmente_validados)
        ->with('parcialmente_rechazados', $parcialmente_rechazados)
        ->with('totalmente_rechazados', $totalmente_rechazados)
        ->with('archivos', $archivos)
        ->with('fecha', $fecha)
        ->with('fechas',$fechas)
        ->with('feriados',$feriados);
    }
}
