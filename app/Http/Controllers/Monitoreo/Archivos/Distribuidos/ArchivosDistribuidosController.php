<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Distribuidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Monitoreo\TransferenciaArchivo;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\TipoArchivo\TipoArchivo;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use DB;

class ArchivosDistribuidosController extends Controller
{
    public function getIndex(Request $request)
    {
        // BANCOS
        $bancos = Banco::buscarTodos();

        // TIPOS DE ARCHIVOS
        $tipo_archivos = TipoArchivo::select('codigo','nombre')
        ->where('codigo', 'OUTGO')
        ->orWhere('codigo', 'ICOMA')
        ->orderBy('codigo', 'asc')
        ->lists('nombre','codigo')
        ->prepend('*', '*');

        // MONEDAS
        $monedas = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo_iso', 'asc')
        ->lists('codigo_iso', 'codigo_iso')
        ->prepend('*', '*');

        $tipo_archivo = $request->get('cod_tipo_archivo') != null ? $request->get('cod_tipo_archivo') : '*';
        $banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';
        $moneda = $request->get('cod_moneda') != null ? $request->get('cod_moneda') : '*';
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : '*';

        /*
         * Listado de parámetros para T y T+1
         */
        $parametros = DB::table('t_parametros_sistema')
        ->select('valor')
        ->orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

        /*
         * Formatear cadena a formato de fecha
         */
        foreach ($parametros as $parametro) {
            $fechas[] = date( "Y-m-d", strtotime( $parametro->valor ) );
        };

        /*
         * Listado de bancarios y feriados
        */
        $calendario = DB::table('t_calendario')
        ->select('fecha_validacion')
        ->get();

        /*
         * Arreglo con las fechas de bancarios y feriados
         */
        $feriados = [];
        foreach ($calendario as $calendario) {
            $feriados[] = $calendario->fecha_validacion;
        };

        $archivos_recibidos = TransferenciaArchivo::totalRecibidos('ICOM1', $banco, $moneda) + TransferenciaArchivo::totalRecibidos('ICOM2', $banco, $moneda);
        $archivos_distribuidos = TransferenciaArchivo::totalAceptados('OUTGO', $banco, $moneda);
        $parcialmente_rechazados = TransferenciaArchivo::totalParcialmenteAceptados('ICOM1', $banco, $moneda) + TransferenciaArchivo::totalParcialmenteAceptados('ICOM2', $banco, $moneda);
        $totalmente_rechazados = TransferenciaArchivo::totalRechazados('ICOM1', $banco, $moneda) + TransferenciaArchivo::totalRechazados('ICOM2', $banco, $moneda);

        $query = "SELECT EPSACH.sp_monitoreo_archivos_distribuidos('".$moneda."','".$banco."','".$fecha."') FROM dual";
        $archivos_enviados = DB::select($query);

        return view('monitoreo.archivos.operaciones.distribuidos.mo28_i_distribuidos')
        ->with('banco', $banco)
        ->with('bancos', $bancos)
        ->with('moneda', $moneda)
        ->with('monedas', $monedas)
        ->with('tipo_archivo', $tipo_archivo)
        ->with('tipo_archivos', $tipo_archivos)
        ->with('archivos_recibidos', $archivos_recibidos)
        ->with('archivos_distribuidos', $archivos_distribuidos)
        ->with('parcialmente_rechazados', $parcialmente_rechazados)
        ->with('totalmente_rechazados', $totalmente_rechazados)
        ->with('archivos_enviados', $archivos_enviados)
        ->with('fecha', $fecha)
        ->with('fechas',$fechas)
        ->with('feriados',$feriados);

    }
}
