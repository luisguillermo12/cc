<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Distribuidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\TransferenciaArchivo;
use App\Models\Monitoreo\FlujoInfo;
use App\Models\Monitoreo\FlujoLote;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Monitoreo\TransaccionCreditoLote;
use App\Models\Monitoreo\TransaccionChequeLote;
use App\Models\Monitoreo\TransaccionDomiciliacionLote;
use App\Models\Monitoreo\ValidacionesEPS;
use DB;

class ArchivosDistribuidosTransaccionesController extends Controller
{
  public function show($id)
  {
      // MONEDAS
      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
    //  ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
    //  ->where('t_parametros_sistema.id', '35')
      ->where('t_monedas.estatus','ACTIVO')
      ->first();

      $query = "SELECT EPSACH.sp_monitoreo_transaciones_registros_distribuidos('$id') FROM dual";
      $encarc_lote = DB::select($query);

      foreach ($encarc_lote as $encarclote)
      {

          //BUSCAR LAS TRANSACCIONES DE CREDITO DEL LOTE CONSULTADO
          if($encarclote->cod_tipo_operacion_lote == '010')
          {
            $query = "SELECT EPSACH.sp_monitoreo_transaciones_creditos_presentados('$id') FROM dual";
            $transacciones = DB::select($query);
          }

          //BUSCAR LAS TRANSACCIONES DE DOMICILIACION DEL LOTE CONSULTADO
          if($encarclote->cod_tipo_operacion_lote == '020')
          {
            $query = "SELECT EPSACH.sp_monitoreo_transaciones_domiciliacion_presentados('$id') FROM dual";
            $transacciones = DB::select($query);
          }

          //BUSCAR LAS TRANSACCIONES DE CHEQUE DEL LOTE CONSULTADO
          if($encarclote->cod_tipo_operacion_lote == '030') {
            $query = "SELECT EPSACH.sp_monitoreo_transaciones_cheques_presentados('$id') FROM dual";
            $transacciones = DB::select($query);
          }

          //BUSCAR LAS TRANSACCIONES DE CREDITOS DEVUELTOS DEL LOTE CONSULTADO
          if($encarclote->cod_tipo_operacion_lote == '110')
          {
            $query = "SELECT EPSACH.sp_monitoreo_transaciones_creditos_devuelto('$id') FROM dual";
            $transacciones = DB::select($query);
          }

          //BUSCAR LAS TRANSACCIONES DE DOMICILIACION DEVUELTOS DEL LOTE CONSULTADO
          if($encarclote->cod_tipo_operacion_lote == '120')
          {
            $query = "SELECT EPSACH.sp_monitoreo_transaciones_domiciliacion_devuelto('$id') FROM dual";
            $transacciones = DB::select($query);
          }

          //BUSCAR LAS TRANSACCIONES DE CHEQUE DEVUELTOS DEL LOTE CONSULTADO
          if($encarclote->cod_tipo_operacion_lote == '130')
          {
            $query = "SELECT EPSACH.sp_monitoreo_transaciones_cheques_devuelto('$id') FROM dual";
            $transacciones = DB::select($query);
          }
      }


      for ($x=0; $x<count($transacciones); $x++) {
        $transacciones[$x]->descripcion_resultado = ValidacionesEPS::descripcion($encarc_lote[0]->cod_tipo_operacion_lote, $transacciones[$x]->cod_rechazo);
      }

    return view('monitoreo.archivos.operaciones.distribuidos.mo30_s_transacciones')
    ->with('monedas', $monedas)
    ->with('transacciones', $transacciones)
    ->with('encarc_lote', $encarc_lote);
  }
}
