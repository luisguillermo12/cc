<?php

namespace App\Http\Controllers\Monitoreo\Archivos\Operaciones\Distribuidos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\TransferenciaArchivo;
use App\Models\Monitoreo\FlujoLote;
use App\Models\Configuracion\Moneda\Moneda;

class ArchivosDistribuidosLotesController extends Controller
{
  public function show($id)
  {

    // MONEDAS
      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
     // ->join('t_parametros_sistema','t_parametros_sistema.valor','=','t_monedas.codigo_iso')
     // ->where('t_parametros_sistema.id', '35')
      ->where('t_monedas.estatus','ACTIVO')
      ->first();

    $info = TransferenciaArchivo::
    select('t_transferencias_archivos.inicio_dt','t_flujos_info.cod_flujo_tipo','t_flujos_info.num_archivo','t_flujos_info.num_lote','t_flujos_info.cod_banco_emisor','t_flujos_info.cod_moneda','t_flujos_info.cod_flujo_info')
    ->join('t_flujos_info','t_flujos_info.transferencia_archivo_out_id','=','t_transferencias_archivos.id')
    ->where('t_flujos_info.cod_flujo_info', '=', $id)
    ->get()
    ->first();

    $lotes = FlujoLote::
    select('t_flujos_lote.*')
    ->where('t_flujos_lote.cod_flujo_info', '=', $id)
    ->get();

    return view('monitoreo.archivos.operaciones.distribuidos.mo29_s_lotes')
    ->with('monedas', $monedas)
    ->with('info', $info)
    ->with('lotes', $lotes);
  }
}
