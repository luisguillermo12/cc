<?php

namespace App\Http\Controllers\Monitoreo\EstadosParticipante\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\InicioOperacion;
use App\Models\Monitoreo\EstadoGeneral;
use App\Models\Monitoreo\EstadoGeneralPar;
use App\Models\Configuracion\Banco\Banco;
use DB;

class GeneralController extends Controller
{
    public function getIndex()
    {
        /* Cantidad total de bancos */
        $bancos = Banco::where('estatus', 'ACTIVO')->count();

        //ordenar en la vista los 010,020,030
        $estado_participantes = EstadoGeneral::where('cod_tipo_operacion','<','031')->where('cod_tipo_operacion','<>','INI')->get()->groupby('cod_banco');

        //ordenar en la vista los 110,120,130
        $estado_participantes2 = EstadoGeneral::where('cod_tipo_operacion','>','109')->where('cod_tipo_operacion','<','131')->where('cod_tipo_operacion','<>','INI')->get()->groupby('cod_banco');

        //ordenar en la vista los IMG
        $estado_participantesimg = EstadoGeneral::where('cod_tipo_operacion','IMG')->get()->groupby('cod_banco');

        $query = "SELECT EPSACH.sp_monitoreo_inicio_operaciones() FROM dual";
        $inicio_operaciones = collect(DB::select($query));


        //dd($estado_participantes['0001'][0]->cod_tipo_operacion);

        $operaciones = $inicio_operaciones->filter(function ($banco, $indi) {
            return $banco->fecha_inicio != null;
        });

        $transmision = $inicio_operaciones->filter(function ($banco, $indi) {
            return $banco->inicio_dt != null;
        });

        $total_operaciones['operaciones'] = count($operaciones);
        $total_operaciones['transmision'] = count($transmision);

        return view('monitoreo.estado_participante.general.mo13_i_general')
        ->with('inicio_operaciones', $inicio_operaciones)
        ->with('estado_participantes', $estado_participantes)
        ->with('bancos', $bancos)
        ->with('total_operaciones', $total_operaciones)
        ->with('estado_participantesimg', $estado_participantesimg)
        ->with('estado_participantes2', $estado_participantes2);
    }
}
