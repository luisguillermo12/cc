<?php

namespace App\Http\Controllers\Monitoreo\EstadosParticipante\Detallado;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Monitoreo\Detallado as Model;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\Banco\Banco;
use DB;

/**
 *
 * @pendiente  crear un método dentro del controlador para construir los valores
 * que debe exportar a la vista para construir las gráficas:
 *     1) tomando desde la BD los productos activos
 *     2) Totalizando las operaciones presentadas y devueltas por hora y totales
 *
 * Con esto se evita colocar manualmente los productos dentro del código PHP
 */
class DetalladoController extends Controller
{
    public function getIndex(Request $request)
    {
        /*
         * Listado de bancos activos
         */
        $bancos = Banco::buscarTodos();

        /*
         * Listado de parámetros para T y T+1
         */
        $parametros = DB::table('t_parametros_sistema')
        ->select('valor')
        ->orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

        /*
         * Formatear cadena a formato de fecha
         */
        foreach ($parametros as $parametro) {
            $fechas[] = date( "Y-m-d", strtotime( $parametro->valor ) );
        };

        /*
         * Listado de bancarios y feriados
        */
        $calendario = DB::table('t_calendario')
        ->select('fecha_validacion')
        ->get();

        /*
         * Arreglo con las fechas de bancarios y feriados
         */
       foreach ($calendario as $calendario) {
            $feriados[] = $calendario->fecha_validacion;
        };

        /*
         * Recibe el código de banco desde la vista.
         * Si no se recibe ningún valor se establece '*' por defecto.
         */
        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';

        /*
         * Recibe el periodo desde la vista
         * Si no se recibe ningún valor se establece '*' por defecto
         */
        $fecha = $request->get('sistema') != null ? $request->get('sistema') : '*';

        /*
         * Recibe el código de operación desde la vista.
         * Si no se recibe ningún valor se establece '*' por defecto.
         */
        $codigo_operacion = $request->get('codigo_operacion') != null ? $request->get('codigo_operacion') : '*';

        /*
         * Recibe la fecha de consulta desde la vista
         * Si no se recibe ningún valor se establece '*' por defecto
         */
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : '*';



        /*
         * Operaciones Presentadas
         */

        /*
         * Buscamos todas las operaciones de crédito presentadas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','010', '" . $fecha . "') from dual ";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars()
         * y eliminamos las claves con "array_values()"
         */
        $presentadas['credito'] = DB::select($query);
        $presentadas['credito'] = $presentadas['credito'] == null ? null : array_values(get_object_vars($presentadas['credito'][0]));

        /*
         * Buscamos todas las operaciones de domiciliaciones presentadas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','020','" . $fecha . "') from dual";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $presentadas['domiciliaciones'] = DB::select($query);
        $presentadas['domiciliaciones'] = $presentadas['domiciliaciones'] == null ? null : array_values(get_object_vars($presentadas['domiciliaciones'][0]));
        /*
         * Buscamos todas las operaciones de cheque presentadas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','030','" . $fecha . "') from dual";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $presentadas['cheque'] = DB::select($query);
        $presentadas['cheque'] = $presentadas['cheque'] == null ? null : array_values(get_object_vars($presentadas['cheque'][0]));

        /*
         * Buscamos todas las operaciones de crédito inmediato presentadas
         * para la IB actual y para la fecha establecida.
         *
         * NOTA: se debe cambiar "000" por el código que se le asigne al producto Crédito Inmediato
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','000','" . $fecha . "') from dual";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $presentadas['cred_inmediato'] = DB::select($query);
        $presentadas['cred_inmediato'] = $presentadas['cred_inmediato'] == null ? null : array_values(get_object_vars($presentadas['cred_inmediato'][0]));



        /*
         * Datos de Operaciones Devueltas
         */

        /*
         * Buscamos todas las operaciones de crédito devueltas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','110','*') from dual";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $devueltas['credito'] = DB::select($query);
        $devueltas['credito'] = $devueltas['credito'] == null ? null : array_values(get_object_vars($devueltas['credito'][0]));

        /*
         * Buscamos todas las operaciones de domiciliaciones devueltas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','120','" . $fecha . "') from dual";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $devueltas['domiciliaciones'] = DB::select($query);
        $devueltas['domiciliaciones'] = $devueltas['domiciliaciones'] == null ? null : array_values(get_object_vars($devueltas['domiciliaciones'][0]));

        /*
         * Buscamos todas las operaciones de cheque devueltas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado('" . $cod_banco . "','130','" . $fecha . "') from dual";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $devueltas['cheque'] = DB::select($query);
        $devueltas['cheque'] = $devueltas['cheque'] == null ? null : array_values(get_object_vars($devueltas['cheque'][0]));
        /*
         * Buscamos todas las operaciones de crédito inmediato devueltas
         * para la IB actual y para la fecha establecida.
         */
        $query = "SELECT EPSACH.sp_monitoreo_detallado( '" . $cod_banco . "','000','" . $fecha . "') from dual ";

        /*
         * Si la consulta está vacía asignar null
         * Si no, seleccionamos el primer valor del arreglo devuelto,
         * luego convertimos el objeto devuelto a un arreglo con object_vars
         * y eliminamos las claves con "array_values()"
         */
        $devueltas['cred_inmediato'] = DB::select($query);
        $devueltas['cred_inmediato'] = $devueltas['cred_inmediato'] == null ? null : array_values(get_object_vars($devueltas['cred_inmediato'][0]));



        $total_presentadas['general'] =  Model::TotalCantOperaciones($cod_banco, ['010', '020', '030'], $fecha);
        $total_presentadas['credito'] =  Model::TotalCantOperaciones($cod_banco, '010', $fecha);
        $total_presentadas['domiciliaciones'] =  Model::TotalCantOperaciones($cod_banco, '020', $fecha);
        $total_presentadas['cheque'] =  Model::TotalCantOperaciones($cod_banco, '030', $fecha);
        $total_presentadas['cred_inmediato'] =  Model::TotalCantOperaciones($cod_banco, '000', $fecha);

        $total_devueltas['general'] =  Model::TotalCantOperaciones($cod_banco, ['110', '120', '130'], $fecha);
        $total_devueltas['credito'] = Model::TotalCantOperaciones($cod_banco, '110', $fecha);
        $total_devueltas['domiciliaciones'] = Model::TotalCantOperaciones($cod_banco, '120', $fecha);
        $total_devueltas['cheque'] = Model::TotalCantOperaciones($cod_banco, '130', $fecha);
        $total_devueltas['cred_inmediato'] = Model::TotalCantOperaciones($cod_banco, '000', $fecha);

        $suma['general'] = $total_presentadas['general'] + $total_devueltas['general'];
        $suma['credito'] = $total_presentadas['credito'] + $total_devueltas['credito'];
        $suma['domiciliaciones'] = $total_presentadas['domiciliaciones'] + $total_devueltas['domiciliaciones'];
        $suma['cheque'] = $total_presentadas['cheque'] + $total_devueltas['cheque'];
        $suma['cred_inmediato'] = $total_presentadas['cred_inmediato'] + $total_devueltas['cred_inmediato'];

        /*
         * Mostrar la vista enviando las variables.
         */
        return view('monitoreo.estado_participante.detallado.mo14_i_detallado')
        ->with('cod_banco',$cod_banco)
        ->with('bancos',$bancos)
        ->with('fecha', $fecha)
        ->with('fechas',$fechas)
        ->with('feriados',$feriados)
        ->with('presentadas', $presentadas)
        ->with('devueltas', $devueltas)
        ->with('total_presentadas', $total_presentadas)
        ->with('total_devueltas', $total_devueltas)
        ->with('suma', $suma);
    }
}
