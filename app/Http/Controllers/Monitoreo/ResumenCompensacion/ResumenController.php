<?php

namespace App\Http\Controllers\Monitoreo\Resumen;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Jenssegers\Date\Date;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Monitoreo\OperacionesValidadas2;
use App\Models\Configuracion\SesionCompensacion\IntervaloLiquidacion;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use Carbon\Carbon;
use SPDF;
use Input;
use Illuminate\Pagination\LengthAwarePaginator;
use DateTimeZone;
use DateTime;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Reportes\RangoHora;
use App\Models\Configuracion\Moneda\Moneda;
class ResumenController extends Controller
{
 
  public  function cuadroM(Request $request){
    
    $operaciones = OperacionesValidadas2::get();
    $intervalos_liquidacion = IntervaloLiquidacion::select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
                                                    ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
                                                    ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
                                                    ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
                                                    ->orderBy('t_intervalos_liquidacion.id', 'asc')
                                                    ->get();

        $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
                                             ->whereIn('id', [2,39])
                                             ->get();
        $dia_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
        $dia_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      foreach ($intervalos_liquidacion as $intervalo) {
            $intervalo->cod_tipo_operacion = TipoOperacion::where('id', $intervalo->tipo_operacion_id)->first()->codigo; 
            
      }
      foreach ($intervalos_liquidacion as $intervalo) {
           $hora_inicio = Carbon::parse($intervalos_liquidacion->where('cod_tipo_operacion', $intervalo->cod_tipo_operacion)->first()->hora_inicio)->format('H:i:s');
           $intervalo->fecha_inicio_completa = $dia_D.$hora_inicio;
           $hora_fin = Carbon::parse($intervalos_liquidacion->where('cod_tipo_operacion', $intervalo->cod_tipo_operacion)->first()->hora_fin)->format('H:i:s');
           $intervalo->fecha_fin_completa = $dia_D_1.$hora_fin;
      } 
        foreach ($intervalos_liquidacion as $intervalo) {
          $aux_fecha_inicio = Carbon::parse($intervalo->fecha_inicio_completa);
          $aux_fecha_fin = Carbon::parse( $intervalo->fecha_fin_completa );
          $intervalo->diferencias_minutos = $aux_fecha_fin->diffInMinutes(($aux_fecha_inicio),false);
        }
        
         foreach ($intervalos_liquidacion as $intervalo) {
          $intervalo->transcurrido = $aux_fecha_fin->diffInMinutes((Carbon::now()),false);
        } 

            foreach ($intervalos_liquidacion as $intervalo) {
          $intervalo->porcentaje = $intervalo->transcurrido*100/ $intervalo->diferencias_minutos ;
        }


 $estatus = [
        'PICKED_UP' => 'POR LIQUIDAR',
        'SETTLED' => 'LIQUIDADO',
    ];

  if ($request->get('estatus') != null){  $estatu=$request->get('estatus'); } else { $estatu = 'PICKED_UP'; } 


  $productos = Producto::select('t_productos.id','t_productos.codigo','t_productos.nombre')
        ->join('t_tipos_operaciones','t_tipos_operaciones.producto_id','=','t_productos.id')
        ->where('t_productos.estatus', 'ACTIVO')
        ->orderBy('t_productos.codigo', 'asc')
        ->distinct()
        ->lists('t_productos.nombre','t_productos.codigo')
        ->prepend('*', '0');
   $producto = $request->get('producto');
 
   //dd ($producto);

 $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                      LEFT JOIN
                      (select participante as codigo, sum (posicion)as posicion from 
                      (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                          from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)       
                      group by participante
                      union all 
                        select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante
                      union all 
                        select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                 from   t_bancos t1, t_transacciones_cheques t2
                                 where  t1.estatus = 'ACTIVO'
                                 and    t2.estatus = ('$estatu')
                                 and    t1.CODIGO = t2.cod_banco_emisor
                                 group by t1.codigo
                      union all 
                                select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_cheques t2
                                where  t1.estatus = 'ACTIVO'  
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante)
                      group by participante
                      order by participante) tr
            on tr.codigo = t_bancos.codigo
           WHERE t_bancos.estatus= 'ACTIVO'
           order by codigo";

  if ( $producto == '*'){
         $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                      LEFT JOIN
                      (select participante as codigo, sum (posicion)as posicion from 
                      (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                          from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_creditos t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)       
                      group by participante
                      union all 
                        select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_emisor
                                group by t1.codigo
                            union all
                                select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_domiciliaciones t2
                                where  t1.estatus = 'ACTIVO'
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante
                      union all 
                        select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                          from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                 from   t_bancos t1, t_transacciones_cheques t2
                                 where  t1.estatus = 'ACTIVO'
                                 and    t2.estatus = ('$estatu')
                                 and    t1.CODIGO = t2.cod_banco_emisor
                                 group by t1.codigo
                      union all 
                                select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                from   t_bancos t1, t_transacciones_cheques t2
                                where  t1.estatus = 'ACTIVO'  
                                and    t2.estatus = ('$estatu')
                                and    t1.CODIGO = t2.cod_banco_destinatario
                                group by t1.codigo)
                      group by participante)
                      group by participante
                      order by participante) tr
            on tr.codigo = t_bancos.codigo
           WHERE t_bancos.estatus= 'ACTIVO'
           order by codigo";
    }
    if($producto == '010'){
       $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                                LEFT JOIN
                                (select participante as codigo, sum (posicion)as posicion from 
                                (select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido,sum(monto_recibido - monto_presentado) posicion
                                    from (select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = ('$estatu')
                                          and    t1.CODIGO = t2.cod_banco_emisor
                                          group by t1.codigo
                                      union all
                                          select t1.codigo participante, 0 monto_presentado, sum(monto) monto_recibido
                                          from   t_bancos t1, t_transacciones_creditos t2
                                          where  t1.estatus = 'ACTIVO'
                                          and    t2.estatus = ('$estatu')
                                          and    t1.CODIGO = t2.cod_banco_destinatario
                                          group by t1.codigo)       
                                group by participante)
                                group by participante
                                order by participante) tr
                                on tr.codigo = t_bancos.codigo
                                WHERE t_bancos.estatus= 'ACTIVO'
                              order by codigo";
    }

    if($producto == '030'){
      $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                            LEFT JOIN
                            (select participante as codigo, sum (posicion)as posicion from 
                            (select participante,  sum(monto_presentado) monto_presentado,  sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                                from ( select t1.codigo participante,  sum(monto) monto_presentado, 0 monto_recibido 
                                       from   t_bancos t1, t_transacciones_cheques t2
                                       where  t1.estatus = 'ACTIVO'
                                       and    t2.estatus = ('$estatu')
                                       and    t1.CODIGO = t2.cod_banco_emisor
                                       group by t1.codigo
                            union all 
                                      select t1.codigo participante,  0 monto_presentado,  sum(monto) monto_recibido
                                      from   t_bancos t1, t_transacciones_cheques t2
                                      where  t1.estatus = 'ACTIVO'  
                                      and    t2.estatus = ('$estatu')
                                      and    t1.CODIGO = t2.cod_banco_destinatario
                                      group by t1.codigo)
                            group by participante)
                            group by participante
                            order by participante) tr
                            on tr.codigo = t_bancos.codigo
                            WHERE t_bancos.estatus= 'ACTIVO'
                          order by codigo";
    }

    if($producto == '020'){
       $query = "SELECT t_bancos.codigo , t_bancos.nombre , t_bancos.estatus, tr.posicion from t_bancos
                          LEFT JOIN
                          (select participante as codigo, sum (posicion)as posicion from 
                          ( select participante, sum(monto_presentado) monto_presentado, sum(monto_recibido) monto_recibido, sum(monto_presentado - monto_recibido) posicion
                              from (select t1.codigo participante, sum(monto) monto_presentado, 0 monto_recibido
                                    from   t_bancos t1, t_transacciones_domiciliaciones t2
                                    where  t1.estatus = 'ACTIVO'
                                    and    t2.estatus = ('$estatu')
                                    and    t1.CODIGO = t2.cod_banco_emisor
                                    group by t1.codigo
                                union all
                                    select t1.codigo participante, 0 monto_presentado,  sum(monto) monto_recibido
                                    from   t_bancos t1, t_transacciones_domiciliaciones t2
                                    where  t1.estatus = 'ACTIVO'
                                    and    t2.estatus = ('$estatu')
                                    and    t1.CODIGO = t2.cod_banco_destinatario
                                    group by t1.codigo)
                          group by participante)
                          group by participante
                          order by participante) tr
                          on tr.codigo = t_bancos.codigo
                          WHERE t_bancos.estatus= 'ACTIVO'
                        order by codigo";  
    }



    $posiciones = DB::select($query);      
  

    $total_positivos=0;
    $total_negativos=0;
       foreach ($posiciones as $posicion) {

                     if($posicion->posicion>0){
                                $total_positivos=$total_positivos+$posicion->posicion;
                      }
                         else{
                                $total_negativos=$total_negativos+$posicion->posicion;
                         }
       }

// grafica 
    $horas = [
      '19:00','20:00','21:00','22:00',
      '23:00','00:00','01:00','02:00',
      '03:00','04:00','05:00','06:00',
      '07:00','08:00','09:00','10:00',
      '11:00','12:00','13:00','14:00',
      '15:00','16:00','17:00','18:00'
    ];

    $data_gris = ['10','40','60','30','20','32','65','98','95','56','64','95','65','98','62','65','98','98','98','65','32','15','48','79'];
    $data_azul = ['26','36','25','56','98','48','15','26','36','59','48','79','45','16','23','59','48','79','46','15','26','25','15','49'];
    $data_negro = ['50','90','23','44','51','45','78','23','97','67','8','12','9','50','74','47','21','50','54','10','20','18','42','32'];

// 
     
      $extenciones=TipoOperacion::select('t_tipos_operaciones.codigo', 't_tipos_operaciones.nombre', 't_extension_franja.*')
     ->leftJoin('t_extension_franja','t_extension_franja.cod_tipo_operacion','=','t_tipos_operaciones.codigo')
      ->where('t_tipos_operaciones.estatus', 'ACTIVO')
      ->get();

      //dd( $extenciones);



  return view('monitoreo.resumen.re02_i_resumen')
    ->with('intervalos_liquidacion', $intervalos_liquidacion)
    ->with('operaciones', $operaciones)
    ->with('productos', $productos)
    ->with('producto', $producto)
    ->with('estatus', $estatus)
    ->with('estatu', $estatu)
    ->with('posiciones', $posiciones)
    ->with('total_positivos', $total_positivos)
    ->with('total_negativos', $total_negativos)
    ->with('horas',$horas)
    ->with('data_gris',$data_gris)
    ->with('data_azul',$data_azul)
    ->with('data_negro',$data_negro);
  }

}
