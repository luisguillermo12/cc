<?php

namespace App\Http\Controllers\EjecucionAdministrativa\StandBy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Services\Log\Log;
use DB;
use App\Services\Email\Correo;
use App\Models\EjecucionAdministrativa\MonitoreoSistema\PentahoJobs;

class StandByController extends Controller
{

     public function __construct()
    {
        $this->log = new Log();
        set_time_limit(300);
        $this->ruta_respaldo = ParametroSistema::find(14)->valor;
        $this->ruta_scripts = ParametroSistema::find(15)->valor;
        $this->ruta_sql = 'E:\BASE\Oracle\instantclient_12_1\\';
        $this->ruta_carte = 'E:\BASE\Pentaho\CarteService\Default\bat\\';
        $this->ruta_etl = "F:\EPSACH\ETL\\";
    }

    public function index(Request $request)
    {
        $procesos = ParametroSistema::whereIn('id', [23,24,34])
        ->get()
        ->keyBy('id');

        $host_carte = ParametroSistema::find(12)->valor;
        $puerto_carte = ParametroSistema::find(13)->valor;

        $url_carte = $host_carte . ':' . $puerto_carte;

        // Establece los minutos que debe esperar el script para ejecutar el cierre de los ETLs
        // Este parámetro se debe tomar desde la tabla t_parametros_sistema
        $tiempo_espera = 1;

        return view('ejecucion_administrativa.stand_by.ea_sb_01_i_standby')
        ->with('procesos', $procesos)
        ->with('tiempo_espera', $tiempo_espera)
        ->with('url_carte', $url_carte);
    }

    /*public function iniciar_jobs()
    {
        set_time_limit(300);

        $ruta = $this->ruta_scripts . 'IniciarJOBS.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta;

        // Se ejecuta el comando
        if (exec($comando)) {

            // Se actualiza el estatus del proceso.
            $cierre = ParametroSistema::find(24);
            $cierre->valor = '1';
            $cierre->save();

            notify()->flash('ETLs iniciados satisfactoriamente', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);
            Correo::notificacion('E001');
        } else {

            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'warning', [
              'timer' => 3000,
              'text' => ' '
            ]);
        }

        return redirect()->back();
    }*/

    public function iniciar_jobs()
    {
        $jobs = PentahoJobs::where('estatus', 1)
        ->where('ejecucion', 0)
        ->orderBy('orden_inicio')
        ->get();

        //$jobs = PentahoJobs::all();

        foreach ($jobs as $job) {
            $base = str_replace('%RUTA_ETL%\\', '', $job->ruta_job);
            $nombre = $job->nombre_job;
            $ruta = $this->ruta_etl . $base . $nombre;
            $url = "http://cluster:cluster@srvcarte:8081/kettle/executeJob/?job={$ruta}&level=Minimal";

            file_get_contents($url);
        }

        // Se actualiza el estatus del proceso.
        $cierre = ParametroSistema::find(24);
        $cierre->valor = '1';
        $cierre->save();

        $jobs = DB::table('t_pentaho_jobs')
        ->update(['ejecucion' => 1]);

        notify()->flash('ETLs iniciados satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        return redirect()->back();
    }

    /*public function detener_jobs()
    {
        $ruta = $this->ruta_scripts . 'DetenerJOBS.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'warning', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta;

        // Se ejecuta el comando
        if (exec($comando)) {

            DB::transaction(function () {

                // Se actualiza el estatus del proceso.
                $cierre = ParametroSistema::find(24);
                $cierre->valor = 2;
                $cierre->save();

                notify()->flash('ETLs detenidos satisfactoriamente', 'success', [
                  'timer' => 3000,
                  'text' => ' '
                ]);
            });

        } else {

            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'warning', [
              'timer' => 3000,
              'text' => ' '
            ]);
        }

        return redirect()->back();
    }*/


    public function detener_cmd()
    {
        $jobs = PentahoJobs::where('estatus', 1)
        ->where('ejecucion', 1)
        ->whereIn('nombre_job', ['00_01_CMD_INCMD_GENERAR.KJB', '00_02_CMD_RICMD_MAILI_IMG_GENERAR.KJB'])
        ->orderBy('orden_cierre')
        ->get();

        foreach ($jobs as $job) {
            $nombre = explode('.', $job->nombre_job)[0];

            $url_detener = "http://cluster:cluster@srvcarte:8081/kettle/stopJob/?xml=y&name={$nombre}";
            $url_retirar = "http://cluster:cluster@srvcarte:8081/kettle/removeJob/?xml=y&name={$nombre}";

            file_get_contents($url_detener);
            file_get_contents($url_retirar);

            $job->ejecucion = 0;
            $job->save();
        }

        /*notify()->flash('ETLs de entrada cancelados satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        return redirect()->back();*/

        return response()->json(['status' => 'success']);
    }

    public function detener_jobs()
    {
        $jobs = PentahoJobs::where('estatus', 1)
        ->where('ejecucion', 1)
        ->orderBy('orden_cierre')
        ->get();

        //$jobs = PentahoJobs::all();

        foreach ($jobs as $job) {
            $nombre = explode('.', $job->nombre_job)[0];

            $url_detener = "http://cluster:cluster@srvcarte:8081/kettle/stopJob/?xml=y&name={$nombre}";
            $url_retirar = "http://cluster:cluster@srvcarte:8081/kettle/removeJob/?xml=y&name={$nombre}";

            file_get_contents($url_detener);
            file_get_contents($url_retirar);
        }

        // Se actualiza el estatus del proceso.
        $cierre = ParametroSistema::find(24);
        $cierre->valor = '2';
        $cierre->save();

        $jobs = DB::table('t_pentaho_jobs')
        ->update(['ejecucion' => 0]);

        notify()->flash('ETLs cancelados satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        //return redirect()->back();

        return response()->json(['status' => 'success']);
    }
}
