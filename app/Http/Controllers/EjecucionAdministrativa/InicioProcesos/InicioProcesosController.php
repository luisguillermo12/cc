<?php

namespace App\Http\Controllers\EjecucionAdministrativa\InicioProcesos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\EjecucionAdministrativa\Proceso;
use App\Models\Monitoreo\LiquidacionArchivos;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\Calendario\FechasCalendario;
use App\Services\Log\Log;
use App\Services\Email\Correo;
use App\Services\Carpetas\Carpetas;
use App\Models\EjecucionAdministrativa\MonitoreoSistema\PentahoJobs;

class InicioProcesosController extends Controller
{
    public function __construct()
    {
        $this->log = new Log();
        //set_time_limit(300);
        $this->ruta_scripts = ParametroSistema::find(15)->valor;
        $this->ruta_pentaho = 'E:\BASE\Pentaho\data-integration\\';
        $this->ruta_carte = 'E:\BASE\Pentaho\CarteService\Default\bat\\';
        $this->ruta_etl = "F:\EPSACH\ETL\\";
    }

    public function getIndex(Request $request)
    {
        /*// Validar estructura de directorios
        $directorios = Carpetas::validarCarpetas();

        if (!$directorios) {
            $cierre = ParametroSistema::find(33);
            $cierre->valor = '0';
            $cierre->save();
        }*/

        $procesos = ParametroSistema::whereIn('id', [8, 24, 23, 33])
        ->get()
        ->keyBy('id');

        $host_carte = ParametroSistema::find(12)->valor;
        $puerto_carte = ParametroSistema::find(13)->valor;

        $url_carte = $host_carte . ':' . $puerto_carte;

        // Calcular último día laborado
        $query = "select fn_busca_ultimo_dia_compensado as fecha from dual";
        $fecha_ultimo = DB::select($query)[0]->fecha;

        $estatus_ultimo = FechasCalendario::whereDate('fecha', '=', $fecha_ultimo)
        ->first()
        ->estatus;

        $ultimo = (object) ['fecha' => Carbon::parse($fecha_ultimo), 'estatus' => $estatus_ultimo];

        // Calcular T+1
        $query = "select fn_busca_proximo_dia_por_compensar('{$ultimo->fecha}') as fecha from dual";
        $proximo = DB::select($query)[0]->fecha;
        $proximo = Carbon::parse($proximo);

        // Calcular día de cierre con relación a la fecha de sistema
        $query = "select fn_busca_proximo_dia_por_compensar as fecha from dual";
        $actual = DB::select($query)[0]->fecha;
        $actual = Carbon::parse($actual);

        /* Mostrar la vista enviando las variables. */
        return view('ejecucion_administrativa.inicio_procesos.ea_is_01_i_inicioprocesos')
        ->with('url_carte', $url_carte)
        ->with('ultimo', $ultimo)
        ->with('proximo', $proximo)
        ->with('actual', $actual)
        //->with('directorios', $directorios)
        ->with('procesos', $procesos);
    }

    public function iniciar_bd()
    {
        $query = 'BEGIN SP_INICIA_SISTEMA(); END;';
        DB::statement($query);

        notify()->flash('Sistema configurado satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        $this->crearEstructura();

        return redirect()->back();
    }

    public function actualizar_servidor($id)
    {
        $inicio = ParametroSistema::find(34);
        $inicio->valor = $id;
        $inicio->save();

        return response()->json(['status' => 'success']);
    }

    /*public function iniciar_jobs()
    {
        set_time_limit(300);

        $ruta = $this->ruta_scripts . 'IniciarJOBS.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta;

        // Se ejecuta el comando
        if (exec($comando)) {

            // Se actualiza el estatus del proceso.
            $cierre = ParametroSistema::find(24);
            $cierre->valor = '1';
            $cierre->save();

            notify()->flash('ETLs iniciados satisfactoriamente', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);
            Correo::notificacion('E001');
        } else {

            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'warning', [
              'timer' => 3000,
              'text' => ' '
            ]);
        }

        return redirect()->back();
    }*/

    public function iniciar_jobs()
    {
        $jobs = PentahoJobs::where('estatus', 1)
        ->where('ejecucion', 0)
        ->orderBy('orden_inicio')
        ->get();

        //$jobs = PentahoJobs::all();

        foreach ($jobs as $job) {
            $base = str_replace('%RUTA_ETL%\\', '', $job->ruta_job);
            $nombre = $job->nombre_job;
            $ruta = $this->ruta_etl . $base . $nombre;
            $url = "http://cluster:cluster@srvcarte:8081/kettle/executeJob/?job={$ruta}&level=Minimal";

            file_get_contents($url);
        }

        // Se actualiza el estatus del proceso.
        $cierre = ParametroSistema::find(24);
        $cierre->valor = '1';
        $cierre->save();

        $jobs = DB::table('t_pentaho_jobs')
        ->update(['ejecucion' => 1]);

        notify()->flash('ETLs iniciados satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        return redirect()->back();
    }

    public function crearEstructura()
    {
        $ruta = $this->ruta_scripts . 'Crea_Estructura.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta;

        // Se ejecuta el comando
        if (exec($comando)) {

            // Se actualiza el estatus del proceso.
            $cierre = ParametroSistema::find(33);
            $cierre->valor = '0';
            $cierre->save();

            return true;
        } else {

            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'warning', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return false;
        }

    }
}
