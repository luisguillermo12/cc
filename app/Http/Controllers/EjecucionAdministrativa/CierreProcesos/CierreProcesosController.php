<?php

namespace App\Http\Controllers\EjecucionAdministrativa\CierreProcesos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\EjecucionAdministrativa\Proceso;
use App\Models\Monitoreo\LiquidacionArchivos;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\EjecucionAdministrativa\RecepcionArchivos\RecepcionArchivos;
use App\Services\Log\Log;
use App\Services\Email\Correo;
use App\Services\Carpetas\Carpetas;
use App\Services\ParametrosSistema\Parametro;
use App\Models\EjecucionAdministrativa\MonitoreoSistema\PentahoJobs;

class CierreProcesosController extends Controller
{
    public $log;
    public $ruta_scripts;
    public $ruta_respaldo;

    public function __construct()
    {
        $this->log = new Log();
        set_time_limit(300);
        $this->ruta_respaldo = ParametroSistema::find(14)->valor;
        $this->ruta_scripts = ParametroSistema::find(15)->valor;
        $this->ruta_sql = 'E:\BASE\Oracle\instantclient_12_1\\';
        $this->ruta_carte = 'E:\BASE\Pentaho\CarteService\Default\bat\\';
    }

    public function getIndex(Request $request)
    {
        //$this->log->info('cierre', 'Inicio del log de ciere de Procesos');

        $procesos = ParametroSistema::whereIn('id', [24, 25, 26, 27, 28, 29, 30, 31, 32, 33])
        ->get()
        ->keyBy('id');


        // Obtener la fecha desde la base de datos
        $query = "select sysdate as fecha from dual";
        $actual = DB::select($query)[0]->fecha;
        $actual = Carbon::parse($actual);

        // Obtener la fecha fin del periodo que cierra de último
        $fecha_limite = RecepcionArchivos::select('fecha_hora_fin')
        ->orderBy('fecha_hora_fin', 'DESC')
        ->first()
        ->fecha_hora_fin;

        // Valida si la fecha del último base de datos actual es me
        //$cancelar_jobs = $actual->diffInSeconds($fecha_limite, false) <= 0;

        // Se está comentando la validación hasta que se confirme cómo se realizará
        // Deivi y Carlos
        $cancelar_jobs = true;

        // Valida que los archivos finales hayan sido enviados
        $archivos_enviados = $procesos[25]->valor && $procesos[26]->valor && $procesos[27]->valor && $procesos[28]->valor;

        // Verificar si todos los procesos fueron ejecutados correctamente.
        $sistema_cerrado = !Parametro::sistemaIniciado();

        $host_carte = ParametroSistema::find(12)->valor;
        $puerto_carte = ParametroSistema::find(13)->valor;

        $url_carte = $host_carte . ':' . $puerto_carte;

        // Establece los minutos que debe esperar el script para ejecutar el cierre de los ETLs
        // Este parámetro se debe tomar desde la tabla t_parametros_sistema
        $tiempo_espera = 1;

        /* Mostrar la vista enviando las variables. */
        return view('ejecucion_administrativa.cierre_procesos.ea_cp_01_i_cierreprocesos')
        ->with('url_carte', $url_carte)
        ->with('cancelar_jobs', $cancelar_jobs)
        ->with('archivos_enviados', $archivos_enviados)
        ->with('sistema_cerrado', $sistema_cerrado)
        ->with('tiempo_espera', $tiempo_espera)
        ->with('procesos', $procesos);
    }

    /*public function detener_jobs()
    {
        $ruta = $this->ruta_scripts . 'DetenerJOBS.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'warning', [
              'timer' => 3000,

            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta;

        // Se ejecuta el comando
        if (exec($comando)) {

            DB::transaction(function () {

                // Se actualiza el estatus del proceso.
                $cierre = ParametroSistema::find(24);
                $cierre->valor = 0;
                $cierre->save();

                notify()->flash('ETLs detenidos satisfactoriamente', 'success', [
                  'timer' => 3000,

                ]);
            });

        } else {

            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'warning', [
              'timer' => 3000,

            ]);
        }

        return redirect()->back();
    }*/

    public function detener_cmd()
    {
        $jobs = PentahoJobs::where('estatus', 1)
        ->where('ejecucion', 1)
        ->whereIn('nombre_job', ['00_01_CMD_INCMD_GENERAR.KJB', '00_02_CMD_RICMD_MAILI_IMG_GENERAR.KJB'])
        ->orderBy('orden_cierre')
        ->get();

        foreach ($jobs as $job) {
            $nombre = explode('.', $job->nombre_job)[0];

            $url_detener = "http://cluster:cluster@srvcarte:8081/kettle/stopJob/?xml=y&name={$nombre}";
            $url_retirar = "http://cluster:cluster@srvcarte:8081/kettle/removeJob/?xml=y&name={$nombre}";

            file_get_contents($url_detener);
            file_get_contents($url_retirar);

            $job->ejecucion = 0;
            $job->save();
        }

        /*notify()->flash('ETLs de entrada cancelados satisfactoriamente', 'success', [
            'timer' => 3000,

        ]);

        return redirect()->back();*/

        return response()->json(['status' => 'success']);
    }

    public function detener_jobs()
    {
        $jobs = PentahoJobs::where('estatus', 1)
        ->where('ejecucion', 1)
        ->orderBy('orden_cierre')
        ->get();

        //$jobs = PentahoJobs::all();

        foreach ($jobs as $job) {
            $nombre = explode('.', $job->nombre_job)[0];

            $url_detener = "http://cluster:cluster@srvcarte:8081/kettle/stopJob/?xml=y&name={$nombre}";
            $url_retirar = "http://cluster:cluster@srvcarte:8081/kettle/removeJob/?xml=y&name={$nombre}";

            file_get_contents($url_detener);
            file_get_contents($url_retirar);
        }

        // Se actualiza el estatus del proceso.
        $cierre = ParametroSistema::find(24);
        $cierre->valor = '0';
        $cierre->save();

        $jobs = DB::table('t_pentaho_jobs')
        ->update(['ejecucion' => 0]);

        notify()->flash('ETLs cancelados satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        //return redirect()->back();

        return response()->json(['status' => 'success']);
    }

    public function comprimir()
    {
        $fecha = Carbon::now()->format('Ymdhis');

        $origen = 'X:\ROOT_EPS';
        $destino = $this->ruta_respaldo . "RESPALDO_{$fecha}.zip";

        $ruta = $this->ruta_scripts . 'comprimir.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'error', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta . ' ' . escapeshellarg($destino) . ' ' . escapeshellarg($origen);

        // Se ejecuta el comando
        if (exec($comando, $resultado)) {

            // Se actualiza el estatus del proceso.
            $proceso = ParametroSistema::find(32);
            $proceso->valor = 1;
            $proceso->save();

            //$this->log->respaldo($resultado);

            notify()->flash('Respaldo ejecutado satisfactoriamente', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);

        } else {

            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'warning', [
              'timer' => 3000,
              'text' => ' '
            ]);
        }

        return redirect()->back();
    }

    public function limpiar()
    {
        $ruta = $this->ruta_scripts . 'limpiar.bat';

        // Verificar que el archivo de comando exista
        if (!file_exists($ruta)) {

            // Si no existe se notifica el error.
            notify()->flash('El archivo de comando no se ha encontrado', 'error', [
              'timer' => 3000,
              'text' => ' '
            ]);

            return redirect()->back();
        }

        // Se construye el comando con la ruta del archivo y los parámetros.
        $comando = $ruta;

        //Se ejecuta el script
        exec($comando, $resultado, $estatus);

        // Se verifica el estatus de la ejecución del script
        if ($estatus === 0) {
            // Se actualiza el estatus del proceso.
            $cierre = ParametroSistema::find(33);
            $cierre->valor = '1';
            $cierre->save();

            notify()->flash('Estructura de directorios limpiada satisfactoriamente', 'success', [
              'timer' => 3000,
              'text' => ' '
            ]);
        } else {
            // Si no se ha podido ejecutar notificar error.
            notify()->flash('Ha ocurrido un error, por favor intente de nuevo', 'error', [
              'timer' => 3000,
              'text' => ' '
            ]);
        }

        return redirect()->back();
    }

    public function generar_historico()
    {
        $query = "BEGIN sp_cierre_sistema_generar_historicos(); END;";
        DB::statement($query);

        $proceso = ParametroSistema::find(29);
        $proceso->valor = 1;
        $proceso->save();

        return redirect()->back();
    }

    public function inicio_db()
    {
        $query = "BEGIN sp_cierre_sistema_borrar_inicializar(); END;";
        DB::statement($query);

        $proceso = ParametroSistema::find(31);
        $proceso->valor = 1;
        $proceso->save();

        notify()->flash('La BD se ha inicializado satisfactoriamente', 'success', [
    			'timer' => 3000,
          'text' => ' '
    		]);

        return redirect()->back();
    }

    public function verificar_servidor()
    {
        // Se constrye la url del servidor carte
        $host_carte = ParametroSistema::find(12)->valor;
        $puerto_carte = ParametroSistema::find(13)->valor;

        // Se verifica si se tiene acceso a la url.
        $url_carte = 'http://cluster:cluster@'. strtolower($host_carte) . ':' . $puerto_carte;
        $check = curl_init($url_carte);
        //dd($check);
        curl_setopt($check, CURLOPT_NOBODY, true);

        if (curl_exec($check)) {

            curl_close($check);

            //Correo::notificacion('E002');
            return response()
            ->json(['resultado' => true]);
        }

        return response()
        ->json(['resultado' => false]);
    }
}
