<?php

namespace App\Http\Controllers\EjecucionAdministrativa\ModificacionFranjas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EjecucionAdministrativa\RecepcionArchivos\RecepcionArchivos;
use App\Models\EjecucionAdministrativa\CortesArchivos\CortesArchivos;
use App\Models\EjecucionAdministrativa\LiquidacionesArchivos\LiquidacionesArchivos;
use App\Models\EjecucionAdministrativa\ExtensionFranja\ExtensionFranja;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\SesionCompensacion\Periodo;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Http\Requests\Ejecucion\Sesion\OperacionesUpdateRequest;
use Validator;
use DB;
use Auth;
use Response;
use Carbon\Carbon;
use App\Services\Email\Correo;
use App\Services\Helpers\HelperService;

class ModificacionFranjasController extends Controller
{

    protected $helperService;
    protected $rangosHora;

    public function __construct(HelperService $helperService)
    {
        $this->helperService = $helperService;
        $this->rangosHora = $this->helperService->generateHoras(1);
    }

    public function index(Request $request)
    {
        $RecepcionArchivos = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja',
        't_productos.nombre','t_horarios_recepcion_archivos.num_liquidacion','t_periodos.num_franja as p_num_franja','t_productos.id as producto_id')
        ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
        ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
        ->orderBy('t_productos.id', 'asc')
        ->orderBy('t_horarios_recepcion_archivos.num_liquidacion', 'asc')
        ->orderBy('t_horarios_recepcion_archivos.num_franja', 'asc')
        ->get();

        $Total_Columna = RecepcionArchivos::select('num_franja')
        ->max('num_franja');

        //20181113. información general para las graficas
        $operaciones = $this->generateInfoOperacionesJson($RecepcionArchivos,$this->rangosHora);

        // Obtener el tiempo mínimo para modificar la franja en minutos.
        $min_modificar = ParametroSistema::find(5)->valor;

        //fecha actual desde BD
        $fecha_actual = $this->helperService->actual_date();

        return view('ejecucion_administrativa.extension_franjas.ea_ef_01_i_extfranjas')
        ->with('RecepcionArchivos', $RecepcionArchivos)
        ->with('Total_Columna', $Total_Columna)
        ->with('rangos_hora', $this->rangosHora)
        ->with('operaciones', $operaciones)
        ->with('min_modificar', $min_modificar)
        ->with('fecha_actual', $fecha_actual);
    }

    //para buscar la posicion de la hora en el arreglo de horas
    private function findHora($hora,$rangos_hora){
      $hora_pos = null;
      for ($i=0; $i < count($rangos_hora); $i++) {
        if($rangos_hora[$i] == $hora){
          $hora_pos = $i;
          break;
        }
      }
      return $hora_pos == null ? 0 : $hora_pos;
    }

    //permite generar el objeto json para las graficas referentes a las franjas de operaciones
    private function generateInfoOperacionesJson($intervalos_liquidacion,$rangos_hora){
      $operaciones[] = null;
      $franjas_operaciones[] = null;
      $producto_id = '';
      $NombreOperacion = '';
      $i = 0; //para recorrer el objeto de franjas
      $j = 0; //para recorrer el objeto de operaciones
      foreach ($intervalos_liquidacion as $registro) {
        if(isset($producto_id) && $producto_id != $registro['producto_id']){
          //si se comienza con una nueva operacion
          if(isset($franjas_operaciones[0])){ //se guarda lo ya recorrido con otras operaciones
            $operaciones[$j] = new \stdClass();
            $operaciones[$j]->descripcion = $NombreOperacion->nombre_completo;
            $operaciones[$j]->datos = json_encode($franjas_operaciones); //se completa la informacion con datos en json
            $franjas_operaciones = null;
            $i = 0;
            $j++;
          }

          $producto_id = $registro['producto_id'];
          //se busca la informacion de la operacion para complementar el grafico
          $NombreOperacion = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
          ->where('estatus','ACTIVO')
          ->where('producto_id',$registro['producto_id'])
          ->orderBy('codigo', 'asc')
          ->first();
          if($i == 0){ //si se esta comenzando
            $franjas_operaciones[$i] = new \stdClass();
            $franjas_operaciones[$i]->x = count($rangos_hora)-1; //cantidad de posiciones que contiene el arreglo de horas
            $franjas_operaciones[$i]->x2 = count($rangos_hora)-1; //cantidad de posiciones que contiene el arreglo de horas
            $franjas_operaciones[$i]->y = 0; //base para indicar el intervalo de liquidacion completo
            $franjas_operaciones[$i]->partialFill = 1; //se rellena este campo para indicar que el grafico debe abarcar completamente el rango de horas
            $i++;
            //se busca la informacion del periodo
            $periodo = Periodo::
              select('t_periodos.*','t_productos.nombre as producto')
              ->join('t_productos','t_productos.id','=','t_periodos.producto_id')
              ->where('t_productos.id',$registro['producto_id'])
              ->first();
            //descripcion de la informacion del intervalo
            $franjas_operaciones[$i] = new \stdClass();
            $franjas_operaciones[$i]->x = $this->findHora($periodo['hora_inicio'],$rangos_hora);
            $franjas_operaciones[$i]->x2 = $this->findHora($periodo['hora_fin'],$rangos_hora);
            $franjas_operaciones[$i]->y = 0; //posicion en el grafico para el intervalo de liquidacion completo
            $i++;
          }
        }
        if($producto_id == $registro['producto_id']){ //si se sigue en el mismo producto, se acumulan las franjas
          $franjas_operaciones[$i] = new \stdClass();
          $franjas_operaciones[$i]->x = $this->findHora(substr($registro['fecha_hora_inicio']->format('H:i:s'), 0, 5),$rangos_hora);
          $franjas_operaciones[$i]->x2 = $this->findHora(substr($registro['fecha_hora_fin']->format('H:i:s'), 0, 5),$rangos_hora);
          $franjas_operaciones[$i]->y = intVal($registro['num_franja']);
          $franjas_operaciones[$i]->nl = intVal($registro['num_liquidacion']);
          $i++;
        }
      }
      //para la ultima operacion
      $operaciones[$j] = new \stdClass();
      $operaciones[$j]->descripcion = $NombreOperacion->nombre_completo;
      $operaciones[$j]->datos = json_encode($franjas_operaciones); //se completa la informacion con datos en json

      return $operaciones;
    }

    public function edit(Request $request, $id)
    {
        $HoraRecepcionArchivos = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja',
        't_productos.nombre','t_horarios_recepcion_archivos.num_liquidacion','t_productos.id as producto_id')
        ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
        ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
        ->where('t_horarios_recepcion_archivos.id','=',$id)
        ->first();

        // Validar que el id indicado no sea invalido
        if (!isset($HoraRecepcionArchivos)) {
            notify()->flash("La franja seleccionada es inválida o no se ha seleccionado ninguna.", 'error', [
                'timer' => 5000,
                'text' => ' '
            ]);

            return redirect()->back();
        }

        //20181114. se busca la informacion general de ese mismo producto para rellenar la grafica
        $HoraRecepcionArchivos_grafico = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja',
        't_productos.nombre','t_horarios_recepcion_archivos.num_liquidacion','t_productos.id as producto_id')
        ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
        ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
        //->where('t_horarios_recepcion_archivos.num_liquidacion','=',$HoraRecepcionArchivos->num_liquidacion)
        ->where('t_productos.id','=',$HoraRecepcionArchivos->producto_id)
        ->get();

        // Obtener las fechas para T y T+1
        // Siendo T el índice [0] y T+1 el índice [1]]
        $parametros = ParametroSistema::whereIn('id', [39, 2])
        ->orderBy('id', 'DESC')
        ->get(['valor']);

        $bancos = Banco::buscarTodos(['Seleccione', '']);

        //20181114. información general para las graficas
        $operaciones = $this->generateInfoOperacionesJson($HoraRecepcionArchivos_grafico,$this->rangosHora);

        // Obtener el tiempo mínimo para modificar la franja en minutos.
        $min_modificar = ParametroSistema::find(5)->valor;

        //fecha actual desde BD
        $fecha_actual = $this->helperService->actual_date();

        return view('ejecucion_administrativa.extension_franjas.ea_ef_02_e_extfranjas')
        ->with('HoraRecepcionArchivos', $HoraRecepcionArchivos)
        ->with('bancos', $bancos)
        ->with('fechas', [$parametros[0]->valor => 'D-1', $parametros[1]->valor => 'D'])
        ->with('rangos_hora', $this->rangosHora)
        ->with('operaciones', $operaciones)
        ->with('min_modificar', $min_modificar)
        ->with('fecha_actual', $fecha_actual);
    }

    public function update(Request $request, $id)
    {
        // Obtener las fechas para T y T+1
        // Siendo T el índice [0] y T+1 el índice [1]
        $parametros = ParametroSistema::whereIn('id', [39, 2])
        ->orderBy('id', 'DESC')
        ->get(['valor']);

        // Obtener el tiempo mínimo para modificar la franja en minutos.
        $min_modificar = ParametroSistema::where('id', 5)
        ->first(['valor'])
        ->valor;

        // Obtener el tiempo mínimo entre franjas.
        $min_franjas = ParametroSistema::where('id', 4)
        ->first(['valor'])
        ->valor;

        // Obtener el tiempo mínimo entre cierre de franjas.
        $min_cierre_franjas = ParametroSistema::where('id', 3)
        ->first(['valor'])
        ->valor;

        // Obtener el horario de recepción de archivos que se está editando.
        $horario = RecepcionArchivos::FindOrFail($id);

        // Obtener período correspondiente al horario.
        $periodo = Periodo::find($horario->periodo_id);

        // Producto correspondiente al horario que se está modificando.
        $producto = Producto::find($periodo->producto_id);

        // Tipo de Operacion (presentada) correspondientes al horario que se está modificando.
        $operacion = TipoOperacion::where('producto_id', $producto->id)
        ->where(function ($query) use ($horario) {
            if ($horario->num_franja == 1) {
                $query->where('direccion', 'P');
            } else {
                $query->where('direccion', 'D');
            }
        })
        ->first();

        // Obtener la nueva hora desde el formulario.
        $nueva_hora = $request->input('hora_fin');

        // Obtener el nueva día desde el formulario.
        $nuevo_dia = $request->input('fecha');

        // Convertir la hora nueva a formato de fecha.
        $nueva_fecha = Carbon::parse("{$nuevo_dia} {$nueva_hora}");

        // Calcular la diferencia entre la fecha y hora actual y la nueva
        $diferencia = $horario->fecha_hora_fin->diffInMinutes($nueva_fecha, false);

        //fecha actual desde BD
        $fecha_actual = $this->helperService->actual_date();

        /*
         * Validaciones
         */

        // Validar que la hora nueva no sea igual que la hora vieja.
        if ($diferencia === 0) {
            notify()->flash('La nueva hora no puede ser igual a la hora ya establecida.', 'warning', [
                'timer' => 5000,
                'text' => ' '
            ]);

            return redirect()->back();
        }
;
        // Validar que la hora nueva no esté en el pasado.
        if ($fecha_actual->diffInMinutes($nueva_fecha, false) <= $min_modificar) {
            notify()->flash("La nueva hora debe ser al menos {$min_modificar}min. posterior a la hora actual.", 'warning', [
                'timer' => 5000,
                'text' => ' '
            ]);

            return redirect()->back();
        }

        // Validar que el tiempo minimo de la franja no sea menor al mínimo establecido.
        if ($horario->fecha_hora_inicio->copy()->subSecond()->diffInMinutes($nueva_fecha,false) < $min_franjas) {
            notify()->flash("La franja debe tener un tiempo de duración de al menos {$min_franjas}min.", 'warning', [
                'timer' => 5000,
                'text' => ' '
            ]);

            return redirect()->back();
        }

        //20181102. se busca la informacion de la franja 2 para una hipotetica liquidacion posterior
        /*$info_liquidacion_posterior_f2 = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja',
        't_productos.nombre','t_productos.id as producto_id')
        ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
        ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
        //->where('t_productos.id',$periodo->producto_id)
        ->where('t_horarios_recepcion_archivos.num_liquidacion',$horario->num_liquidacion+1)
        ->where('t_horarios_recepcion_archivos.num_franja',2)
        ->orderBy('fecha_hora_fin', 'desc')
        ->orderBy('t_horarios_recepcion_archivos.num_liquidacion', 'asc')
        ->orderBy('t_horarios_recepcion_archivos.num_franja', 'asc')
        ->first();
        //20181105. se busca la informacion de la franja 1 para una hipotetica liquidacion posterior
        $info_liquidacion_posterior_f1 = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja',
        't_productos.nombre','t_productos.id as producto_id')
        ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
        ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
        //->where('t_productos.id',$periodo->producto_id)
        ->where('t_horarios_recepcion_archivos.num_liquidacion',$horario->num_liquidacion+1)
        ->where('t_horarios_recepcion_archivos.num_franja',1)
        ->orderBy('fecha_hora_fin', 'desc')
        ->orderBy('t_horarios_recepcion_archivos.num_liquidacion', 'asc')
        ->orderBy('t_horarios_recepcion_archivos.num_franja', 'asc')
        ->first();*/

        // Validar que la hora nueva para la franja 1 no sea mayor la hora de la liquidación.
        if ($horario->num_franja === '1') {

            //20181102. se quita la validacion ya que en extension de franja no se requiere validar con respecto al final el periodo
            // switch($periodo->dia_fin) {
            //     case 'D-1' :
            //         $periodo_fecha = $parametros[0]->valor;
            //         break;
            //     case 'D' :
            //         $periodo_fecha = $parametros[1]->valor;
            //         break;
            //     default :
            //         $periodo_fecha = $parametros[0]->valor;
            // }
            //
            // $fin_periodo = Carbon::parse("{$periodo_fecha} {$periodo->hora_fin}");
            //
            // if ($fin_periodo->diffInMinutes($nueva_fecha, false) <= 0) {
            //
            //     notify()->flash("El fin de la primera franja no puede establecerse a menos de {$min_franjas}min. del cierre del período de liquidación.", 'warning', [
            //         'timer' => 5000
            //     ]);
            //     return redirect()->back();
            // }

           //20181102. se busca la informacion de la franja 2 con los datos de la franja 1
           $info_franja_2 = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja','t_productos.nombre')
           ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
           ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
           ->where('t_productos.id',$periodo->producto_id)
           ->where('t_horarios_recepcion_archivos.num_liquidacion',$horario->num_liquidacion)
           ->where('t_horarios_recepcion_archivos.num_franja',2)
           ->first();
           //para validar que el tiempo final de la franja 1 sea menor al tiempo establecido en relacion al cierre de la franja 2, y no se encuentre despues del cierre de esta
           if($info_franja_2->fecha_hora_fin < $nueva_fecha->copy()->addMinutes($min_cierre_franjas)){
             notify()->flash("El tiempo mínimo entre culminación de franjas debe ser de {$min_cierre_franjas} minutos.", 'warning', [
                 'timer' => 6000,
                 'text' => ' '
             ]);
             return redirect()->back();
           }

           //si existe una liquidacion posterior
           /*if (isset($info_liquidacion_posterior_f1) && $periodo->producto_id == $info_liquidacion_posterior_f1->producto_id) {
             //para validar que la nueva fecha no sea mayor a la inicial de la franja 1 para una proxima liquidacion
             if($nueva_fecha > $info_liquidacion_posterior_f1->fecha_hora_inicio){
               notify()->flash("El fin de la franja para esta liquidación no puede establecerse luego del inicio de la franja de presentadas para la liquidación posterior.", 'warning', [
                   'timer' => 8000
                  ]);
                  return redirect()->back();
             }
           }*/
        }

        //20181101. si se refiere a la franja 2
        if ($horario->num_franja === '2') {
           //se busca la informacion de la franja 1 con los datos de la franja 2
          $info_franja_1 = RecepcionArchivos::select('t_horarios_recepcion_archivos.id','fecha_hora_inicio','fecha_hora_fin','t_horarios_recepcion_archivos.num_franja','t_productos.nombre')
          ->join('t_periodos','t_horarios_recepcion_archivos.periodo_id','=','t_periodos.id')
          ->join('t_productos','t_periodos.producto_id','=','t_productos.id')
          ->where('t_productos.id',$periodo->producto_id)
          ->where('t_horarios_recepcion_archivos.num_liquidacion',$horario->num_liquidacion)
          ->where('t_horarios_recepcion_archivos.num_franja',1)
          ->first();
          //para validar que el tiempo final de la franja 2 sea mayor al tiempo establecido en relacion al cierre de la franja 1, y no se encuentre antes del cierre de esta
          if($nueva_fecha < $info_franja_1->fecha_hora_fin->copy()->addMinutes($min_cierre_franjas)){
            notify()->flash("El tiempo mínimo entre culminación de franjas debe ser de {$min_cierre_franjas} minutos.", 'warning', [
                'timer' => 6000,
                'text' => ' '
            ]);
            return redirect()->back();
          }
          //si existe una liquidacion posterior
          /*if (isset($info_liquidacion_posterior_f2) && $periodo->producto_id == $info_liquidacion_posterior_f2->producto_id) {
            //para validar que la nueva fecha no sea mayor a la inicial de la franja 2 para una proxima liquidacion
            if($nueva_fecha > $info_liquidacion_posterior_f2->fecha_hora_inicio){
              notify()->flash("El fin de la franja para esta liquidación no puede establecerse luego del inicio de la franja de devueltas para la liquidación posterior.", 'warning', [
                  'timer' => 8000
                 ]);
                 return redirect()->back();
            }
          }else if(isset($info_liquidacion_posterior_f2)){
            //para validar que la nueva fecha no sea posterior a la fecha de la liquidacion posterior
            if($nueva_fecha->diffInMinutes($info_liquidacion_posterior_f2->fecha_hora_fin,false) < $min_cierre_franjas){
              notify()->flash("El fin de la franja para esta liquidación no puede establecerse a menos de {$min_cierre_franjas}min. antes de la liquidación posterior, o luego de que finalice esta.", 'warning', [
                  'timer' => 8000
                 ]);
                 return redirect()->back();
            }
          }*/
        }

        /*
         * Crear una nueva extensión de franja.
         *
         * La asignación de los valores en el modelo se realiza fuera de la transacción,
         * pero la inseción en la BD si se encuentra dentro. [$extensión->save()]
         * Se realizó así mantener el tiempo de la transacción en el mínimo posible.
         */
        $extension = new ExtensionFranja();
        $extension->id_usuario = Auth::user()->id;
        $extension->cod_banco = $request->get('cod_banco');
        $extension->cod_tipo_operacion = $operacion->codigo;
        $extension->tiempo_extension = abs($diferencia);
        $extension->fecha = $fecha_actual->format('Ymd');
        $extension->descripcion = $request->get('descripcion');
        $extension->estatus = $diferencia > 0 ? 'E' : 'D';


        /*
         * Guardar los cambios en la base de datos.
         */
        DB::transaction(function () use ($horario, $producto, $operacion, $nueva_fecha, $extension) {

            $query = DB::raw("UPDATE t_cortes_archivos ca
                SET
                    ca.fecha_hora_ejecucion = '{$nueva_fecha->format('YmdHis')}',
                    ca.critico_ejecucion = '{$nueva_fecha->format('YmdHis')}'
                WHERE
                    ca.fecha_hora_ejecucion = '{$horario->fecha_hora_fin->format('YmdHis')}'
                AND
                    ca.id IN (
                    SELECT
                        cad.corte_id
                    FROM
                        t_cortes_archivos_detalle cad
                    WHERE
                        cad.cod_tipo_operacion = '{$operacion->codigo}'
                    AND
                        cad.num_liquidacion = '{$horario->num_liquidacion}'
                    AND
                        cad.num_franja = '{$horario->num_franja}'
                    )
            ");
            DB::statement($query);

            /*
             * Si se trata de la franja 1, actualizar el inicio de la franja 2 de la misma liquidación.
             */
            // if ($horario->num_franja == 1) {
            //     $franja_dos =  RecepcionArchivos::where('num_liquidacion', $horario->num_liquidacion)
            //     ->where('periodo_id', $horario->periodo_id)
            //     ->where('num_franja', 2)
            //     ->update([
            //         'fecha_hora_inicio' => $nueva_fecha->copy()->addSecond()->format('YmdHis'),
            //         'critico_inicio_dt' => $nueva_fecha->copy()->addSecond()->format('YmdHis'),
            //     ]);
            // }

            /*
             *Chequea si se trata de la franja 2.
             */
            if ($horario->num_franja == 2) {

                $query = DB::raw("UPDATE t_liquidaciones_archivos la
                SET
                    la.fecha_hora_ejecucion = '{$nueva_fecha->format('YmdHis')}',
                    la.critico_ejecucion = '{$nueva_fecha->format('YmdHis')}'
                WHERE
                    la.fecha_hora_ejecucion = '{$horario->fecha_hora_fin->format('YmdHis')}'
                AND
                    la.id = (
                    SELECT
                        lad.liquidacion_archivo_id
                    FROM
                        t_liquidaciones_archivos_detalle lad
                    WHERE
                        lad.producto_id = '{$producto->id}'
                    AND
                        lad.num_franja = 2
                    )
                ");
                DB::statement($query);

                /*
                 * Actualizar el inicio de la primera franja de la siguiente liquidación.
                 * Si no existe una siguiente liquidación no se ejecuta nada.
                 */
                $liq_siguiente = $horario->num_liquidacion + 1;
                $cod = substr($operacion->codigo, -2);

                $query = DB::raw("UPDATE t_horarios_recepcion_archivos hra
                SET
                    hra.fecha_hora_inicio = '{$nueva_fecha->copy()->addSecond()->format('YmdHis')}',
                    hra.critico_inicio_dt = '{$nueva_fecha->copy()->addSecond()->format('YmdHis')}'
                WHERE
                    hra.num_franja = 1
                AND
                    hra.num_liquidacion = {$liq_siguiente}
                AND
                    hra.id IN (
                    SELECT
                        hrad.horario_recepcion_id
                    FROM
                        t_horarios_recepcion_archivos_detalle hrad
                    WHERE
                        hrad.cod_tipo_operacion LIKE '%{$cod}'
                    )
                ");
                DB::statement($query);
            }

            // Guarda en la BD la nueva extensión de franja.
            $extension->save();

            // Asignar la nueva fecha de la franja que se está editando.
            $horario->update([
                'fecha_hora_fin' => $nueva_fecha->format('YmdHis'),
                'critico_fin_dt' => $nueva_fecha->format('YmdHis'),
            ]);

        }, 5);

        notify()->flash('Franja actualizada satisfactoriamente.', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);
        Correo::notificacion('E003');
        return redirect()->route('ModificacionFranja.index');
    }
}
