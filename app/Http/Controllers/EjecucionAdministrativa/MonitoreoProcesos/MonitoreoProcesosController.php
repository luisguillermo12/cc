<?php

namespace App\Http\Controllers\EjecucionAdministrativa\MonitoreoProcesos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\EjecucionAdministrativa\Proceso;
use App\Http\Controllers\Traits\conditions_star;
use App\Models\EjecucionAdministrativa\MonitoreoSistema\PentahoJobs;
use App\Models\EjecucionAdministrativa\MonitoreoSistema\ObjetosInvalidos;

class MonitoreoProcesosController extends Controller
{
    public function __construct()
    {
        $this->ruta_etl = "F:\EPSACH\ETL\\";
    }

public function getIndex(Request $request)
    {
        $objetos_invalidos = ObjetosInvalidos::all();
        return view('ejecucion_administrativa.monitoreo_procesos.ea_mp_01_i_monitoreo')
        ->with('objetos_invalidos', $objetos_invalidos);
    }

    public function consultar(Request $request)
    {

      $busqueda = $request->get('busqueda') == null ? '' : strtoupper($request->get('busqueda'));

        //listado de jobs en db
        $jobs = DB::table('T_PENTAHO_JOBS')->where('estatus', 1)->get()->toArray();
        $jobs = $this->ordenar($jobs, $request->get('columna'), $request->get('orden'));
       //host + port del servidor carte
        $host_carte = ParametroSistema::find(12)->valor;
        $puerto_carte = ParametroSistema::find(13)->valor;
        $url_carte = $host_carte . ':' . $puerto_carte;

        //verificar que hay acceso al servidor carte
        $file = 'http://cluster:cluster@'.$url_carte.'/';
        $file_headers = @get_headers($file);

        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {

            //$exists = false;
            $estatus = [];

        }else {
            //$exists = true;

            $jobs_estatus = file_get_contents('http://cluster:cluster@'.$url_carte.'/kettle/status/?xml=y');
            $estatus=(array) simplexml_load_string($jobs_estatus)->jobstatuslist;

            if (isset($estatus['jobstatus']) && !is_array($estatus['jobstatus'])) {

                $estatus_tmp = $estatus['jobstatus'];
                $estatus['jobstatus'] = [];

               $estatus['jobstatus'][] = $estatus_tmp;
            }

        }


    $respuesta = [];
    $contador = 0;
    $estado_final = "";
    $activar_jobs="";
    $url= 'EjecucionAdministrativa/MonitoreoProcesos/ActivarJob';

        foreach ($jobs as $job) {

			if (count($estatus) == 0) {
            $estado_final = '<small class="badge badge-danger">Jobs no iniciados ( server carte caido ! ) &nbsp;<i class="fa fa-tag"></i></small>';
        	} elseif (count($estatus) == 1 && !isset($estatus['jobstatus'])) {
            $estado_final = '<small class="badge badge-danger">Jobs no iniciados &nbsp;<i class="fa fa-tag"></i></small>';
                if($job->ejecucion==1){
                    $activar_jobs='<a data-toggle="tooltip" data-placement="top" title="Activar" class="btn-sm btn-success" href="' . url($url,$job->codigo_job) . '"><i class="fa fa-power-off"></i></a>';
                } else {
                    $activar_jobs="";
                }
        	} else {
	        	foreach ($estatus['jobstatus'] as $job_carte) {
              $name_temp = str_replace('.KJB', '', $job->nombre_job);
              $name_carte = (String) $job_carte->jobname;
              //var_dump($name_temp.'-'.$name_carte);
	        		if (isset($name_carte) && $name_temp == $name_carte) {
	        			if ((string) $job_carte->status_desc == 'Running') {
                  $estado_final = '<small class="badge badge-success">&nbsp;&nbsp;&nbsp;&nbsp;Ejecutando &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>';
                  $activar_jobs='';
	        				break;
	        			} elseif ((string) $job_carte->status_desc == 'Stopped')  {
                  $estado_final = '<small class="badge badge-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detenido &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>';
                  if($job->ejecucion==1){
                    $activar_jobs='<a data-toggle="tooltip" data-placement="top" title="Activar" class="btn-sm btn-success" href="' . url($url,$job->codigo_job) . '"><i class="fa fa-power-off"></i></a>';
                    break;
                    } else {
                    $activar_jobs="";
                }

	        			} elseif ((string) $job_carte->status_desc == 'Finished') {
                  $estado_final = '<small class="badge badge-primary">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Finalizado &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>';
                  $activar_jobs='';
	        				break;
	        			}
                        else{
                            $estado_final = '<small class="badge badge-danger">&nbsp;Finalizado con errores &nbsp;<i class="fa fa-tag"></i></small>';
                            $activar_jobs='';
                            break;
                        }
	        		}
	        	}
            /*$name_temp = str_replace('.KJB', '', $job->nombre_job);
            $name_carte = (String) $job_carte->jobname;
	        	if (isset($name_carte) && $name_temp !== $name_carte) {
	        		// $respuesta[] = [$job , '<small class="label bg-red">Jobs no iniciado &nbsp;<i class="fa fa-tag"></i></small>'];
              $estado_final = '<small class="badge badge-red">Job no iniciado... &nbsp;<i class="fa fa-tag"></i></small>';
              if($job->ejecucion==1){
                $activar_jobs='<a data-toggle="tooltip" data-placement="top" title="Activar" class="btn-sm btn-success" href="' . url($url,$job->codigo_job) . '"><i class="fa fa-power-off"></i></a>';
              }else {
                    $activar_jobs="";
                }

	        	}*/
        	}

          $job = [ //se guarda la informacion del proceso en el mismo vector
            //$contador+1,
            $job->codigo_job,
            str_replace('.KJB', '', $job->nombre_job),
            $job->clasificacion_job,
            $estado_final,
            $activar_jobs
          ];

          if($busqueda != ""){
            if($this->findValor($job,$busqueda)){
              $respuesta[$contador] = $job; //se guarda el vector en una casilla del vector de respuesta
            }
          }else {
            $respuesta[$contador] = $job; //se guarda el vector en una casilla del vector de respuesta
          }

          $contador++; //se incrementan las posiciones

        }

        return response()->json($respuesta);
    }

    //buscar el string en los valores del objeto
    private function findValor($job,$findme){
      $found = false;
      for($i=0; $i < count($job); $i++){
        $found = strpos($job[$i], $findme) === false ? false : true;
        //echo strpos($job[$i], $findme).' - ';
        if ($found) {
          return true;
        }
      }
      return false;
    }

    public function activar_job($codigo)
    {
        $job = PentahoJobs::where('codigo_job', $codigo)
        ->first();
        //dd($job);

        $nombre = explode('.', $job->nombre_job)[0];

        $url_detener = "http://cluster:cluster@srvcarte:8081/kettle/stopJob/?xml=y&name={$nombre}";
        $url_retirar = "http://cluster:cluster@srvcarte:8081/kettle/removeJob/?xml=y&name={$nombre}";

        file_get_contents($url_detener);
        file_get_contents($url_retirar);

        $base = str_replace('%RUTA_ETL%\\', '', $job->ruta_job);
        $nombre = $job->nombre_job;
        $ruta = $this->ruta_etl . $base . $nombre;
        $url_iniciar = "http://cluster:cluster@srvcarte:8081/kettle/executeJob/?job={$ruta}&level=Minimal";

        $respuesta = file_get_contents($url_iniciar);
        $xml = simplexml_load_string($respuesta);
        //dd($xml);

        notify()->flash('Proceso iniciado satisfactoriamente', 'success', [
            'timer' => 3000,
            'text' => ' '
        ]);

        return redirect()->back();
    }

    public function diskc()
    {

        $diskt = 0;
        $diskf = 0;
        $usado0 = 0;
        $raizf0 = "C:";


        if ( file_exists("C:")) {

            $diskcf = disk_free_space("C:");
            $diskct= disk_total_space("C:");

            $diskctotal = $this->formatBytes($diskct);
            $diskcfree = $this->formatBytes($diskcf);
            $usadoc = $this->formatBytes($diskct - $diskcf);


            return response()->json(array($diskctotal,$diskcfree,$usadoc,$raizf0 ) );

        }else{

            return response()->json(array($diskt,$diskf,$usado0,$raizf0 ) );

        };

    }

    public function diskx()
    {

        $diskx = ParametroSistema::find(1)->valor;
        $raizx= substr($diskx, 0, 2);

        $diskt = 0;
        $diskf = 0;
        $usado0 = 0;
        $raizf0 = $raizx;

        if ( file_exists($raizx)) {

            $diskxf = disk_free_space($raizx);
            $diskxt= disk_total_space($raizx);

            $diskctotal = $this->formatBytes($diskxt);
            $diskcfree = $this->formatBytes($diskxf);
            $usadoc = $this->formatBytes($diskxt - $diskxf);

            return response()->json(array($diskctotal,$diskcfree,$usadoc,$raizx ) );

        }else{

            return response()->json(array($diskt,$diskf,$usado0,$raizf0 ) );

        };

    }

    public function disky()
    {

        $disky = ParametroSistema::find(14)->valor;
        $raizy= substr($disky, 0, 2);

        $diskt = 0;
        $diskf = 0;
        $usado0 = 0;
        $raizf0 = $raizy;

        if ( file_exists($raizy) ) {

            $diskyf = disk_free_space($raizy);
            $diskyt= disk_total_space($raizy);

            $diskctotal = $this->formatBytes($diskyt);
            $diskcfree = $this->formatBytes($diskyf);
            $usadoc = $this->formatBytes($diskyt - $diskyf);

            return response()->json(array($diskctotal,$diskcfree,$usadoc,$raizy ) );

        }else{

            return response()->json(array($diskt,$diskf,$usado0,$raizf0 ) );

        };

    }

        public function diskf()
    {

        $diskf = ParametroSistema::find(15)->valor;
        $raizf= substr($diskf, 0, 2);

        $diskt = 0;
        $diskf = 0;
        $usado0 = 0;
        $raizf0 = $raizf;

        if ( file_exists($raizf)) {

            $diskff = disk_free_space($raizf);
            $diskft= disk_total_space($raizf);

            $diskctotal = $this->formatBytes($diskft);
            $diskcfree = $this->formatBytes($diskff);
            $usadoc = $this->formatBytes($diskft - $diskff);


            return response()->json(array($diskctotal,$diskcfree,$usadoc,$raizf ) );

        }else{

            return response()->json(array($diskt,$diskf,$usado0,$raizf0 ) );

        };

    }
    private function formatBytes($bytes, $precision = 2) {
            $units = array('B', 'KB', 'MB', 'GB', 'TB');

            $bytes = max($bytes, 0);
            $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
            $pow = min($pow, count($units) - 1);
            // Uncomment one of the following alternatives
             $bytes /= pow(1024, $pow);
            // $bytes /= (1 << (10 * $pow));
            return round($bytes, $precision) . ' ' . $units[$pow];
        }

    public function objetosInvalidosBD()
    {
        return response()->json(ObjetosInvalidos::all());
    }

    public function compilarBD()
    {
        $query = "BEGIN epsach.sp_compilar_objetos_invalidos(); END;";
        DB::statement($query);

        return response()->json(['result' => 'success']);
    }

     //ordenamiento de array de objetos por quicksort
    private function ordenar($array, $columna, $orden = 'ASC')
    {
        $aux = $array;
        usort($array, function ($a, $b) use ($columna, $orden) {
            if (strtoupper($orden) == 'ASC') {
                if ($a->$columna == $b->$columna) return 0;
                if ($a->$columna > $b->$columna) return 1;
                if ($a->$columna < $b->$columna) return -1;
            } elseif (strtoupper($orden) == 'DESC') {
                if ($b->$columna == $a->$columna) return 0;
                if ($b->$columna > $a->$columna) return 1;
                if ($b->$columna < $a->$columna) return -1;
            }
        });
        return $array;
    }
}
