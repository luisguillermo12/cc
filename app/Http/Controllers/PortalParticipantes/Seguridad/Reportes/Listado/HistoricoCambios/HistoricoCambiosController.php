<?php

namespace App\Http\Controllers\Seguridad\Reportes;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class HistoricoCambiosController extends Controller
{
    public function getIndex()
    {
        $logs = DB::table('audits')
        ->where('url', 'NOT LIKE', '%login')
        ->where('url', 'NOT LIKE', '%logout')
        ->where('old_values', 'NOT LIKE', '%remember_token%')
        ->where('new_values', 'NOT LIKE', '%remember_token%')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->orderBy('audits.id', 'DESC')
        ->paginate(15);

        for($x = 0; $x < count($logs); $x++) {

            /** Cambiamos el evento por su contraparte en español */
            if($logs[$x]->event == 'created') {
                $logs[$x]->event = 'Creación';
            } elseif($logs[$x]->event == 'updated') {
                $logs[$x]->event = 'Actualización';
            } elseif($logs[$x]->event == 'deleted') {
                $logs[$x]->event = 'Eliminación';
            }

        }

        return view('seguridad.reportes.historico_cambios.se13_i_historicocambios')
        ->with('logs', $logs);
    }

    public function show($id)
    {

        $log = DB::table('audits')
        ->where('audits.id', $id)
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->first();

        /** Cambiamos el evento por su contraparte en español */
        if($log->event == 'created') {
            $log->event = 'Creación';
        } elseif($log->event == 'updated') {
            $log->event = 'Actualización';
        } elseif($log->event == 'deleted') {
            $log->event = 'Eliminación';
        }

        //dd($log);

        return view('seguridad.reportes.historico_cambios.se14_s_detalles')
        ->with('log', $log);
    }

    public function excel()
    {
        $logs = DB::table('audits')
        ->where('url', 'NOT LIKE', '%login')
        ->where('url', 'NOT LIKE', '%logout')
        ->where('old_values', 'NOT LIKE', '%remember_token%')
        ->where('new_values', 'NOT LIKE', '%remember_token%')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->orderBy('audits.id', 'DESC')
        ->get();

        for($x = 0; $x < count($logs); $x++) {
            /* Cambiamos el evento por su contraparte en español */
            if($logs[$x]->event == 'created') {
                $logs[$x]->event = 'Creación';
            } elseif($logs[$x]->event == 'updated') {
                $logs[$x]->event = 'Actualización';
            } elseif($logs[$x]->event == 'deleted') {
                $logs[$x]->event = 'Eliminación';
            }
        }

        Excel::create("ReporteHistoricoCambios", function ($excel) use ($logs) {
            $excel->setTitle("ReporteHistoricoCambios");
            $excel->sheet("Cambios", function ($sheet) use ($logs) {
                $sheet->loadView('seguridad.reportes.historico_cambios.se20_ex_excel')->with('logs', $logs);
            });
        })->download('xls');

        return back();
    }

    public function pdf()
    {
        $logs = DB::table('audits')
        ->where('url', 'NOT LIKE', '%login')
        ->where('url', 'NOT LIKE', '%logout')
        ->where('old_values', 'NOT LIKE', '%remember_token%')
        ->where('new_values', 'NOT LIKE', '%remember_token%')
        ->leftJoin('users', 'users.id', '=', 'audits.user_id')
        ->select('audits.*', 'users.name as username', 'users.email as user_email')
        ->orderBy('audits.id', 'DESC')
        ->paginate(15);

        for($x = 0; $x < count($logs); $x++) {
            /* Cambiamos el evento por su contraparte en español */
            if($logs[$x]->event == 'created') {
                $logs[$x]->event = 'Creación';
            } elseif($logs[$x]->event == 'updated') {
                $logs[$x]->event = 'Actualización';
            } elseif($logs[$x]->event == 'deleted') {
                $logs[$x]->event = 'Eliminación';
            }
        }

        $view =  \View::make('seguridad.reportes.historico_cambios.se21_p_pdf', compact('logs', 'pdf'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a3', 'portrait');
        
        return $pdf->download('Reporte_Historico_Cambios.pdf');
    }

}
