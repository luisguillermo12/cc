<?php

namespace App\Http\Controllers\Seguridad\Reportes;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use Maatwebsite\Excel\Facades\Excel;

class ListadoUsuariosController extends Controller
{
    public function getIndex()
    {
        $usuarios = User::withTrashed()
        ->orderBy('id', 'ASC')
        ->paginate(10);

        $activos = User::where('status', 1)
        ->count();

        $inactivos = User::where('status', 0)
        ->count();

        $eliminados = User::onlyTrashed()
        ->count();

        return view('seguridad.reportes.usuarios.se10_i_usuarios') 
        ->with('usuarios', $usuarios)
        ->with('activos', $activos)
        ->with('inactivos', $inactivos)
        ->with('eliminados', $eliminados);
    }

    public function excel()
    {
        $usuarios = User::withTrashed()
        ->orderBy('id', 'ASC')
        ->get();

        Excel::create("ReporteUsuarios", function ($excel) use ($usuarios) {
            $excel->setTitle("ReporteUsuarios");
            $excel->sheet("Usuarios", function ($sheet) use ($usuarios) {
                $sheet->loadView('seguridad.reportes.usuarios.se15_ex_excel')->with('usuarios', $usuarios);
            });
        })->download('xls');

        return back();
    }

    public function pdf()
    {
        $usuarios = User::withTrashed()
        ->orderBy('id', 'ASC')
        ->get();

        $view =  \View::make('seguridad.reportes.usuarios.se16_p_pdf', compact('usuarios', 'pdf'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait');
        
        return $pdf->download('Reporte_Usuarios.pdf');
    }
}
