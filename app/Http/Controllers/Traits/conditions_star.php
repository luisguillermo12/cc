<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Carbon\Carbon;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\EjecucionAdministrativa\Proceso;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\Configuracion\SesionCompensacion\Periodo;
use Illuminate\Support\Collection;
use App\Services\Log\Log;

trait conditions_star
{

    function conditions_pre () {
        $validacion=false;
        $conexion_estable=true;
        $directorios_existen=true;
        $sincambios_tables=true;
        $log = new Log();
        $procesos = DB::table('t_procesos')
        ->select('id','descripcion','estatus','fecha_inicio')
        ->whereIN('id', [11])
        ->orderBy('id', 'ASC')
        ->first();
        $today = date('Y-m-d');
         if (date("Y-m-d",strtotime($procesos->fecha_inicio))==$today && $procesos->estatus== 1) {
            $validacion = false;
         } else {
             $validacion = true;
         }
        if ($validacion) {
            $test=true; $log->info('inicio', 'test de conexion a la base de datos OK');
            $ping=true; $log->info('inicio', 'ping de conexion al servidor de base de datos OK');              
            $status=true; $log->info('inicio', 'status de la conexion :  Disponible');
            $busy=false;  $log->info('inicio', 'status de la disponibilidad para consultas :  Disponible');
            $query ="select table_name from user_tables order by table_name";
            $array_tables_eps = DB::select($query);
            $tables_eps=['ASSIGNED_ROLES','AUDITS','HISTORY_TYPES','MIGRATIONS','PASSWORD_RESETS','PERMISSION_ROLE','PERMISSIONS','ROLES','T_ALARMAS','T_ALARMAS_DETALLE','T_ARCHIVOS_RECHAZADOS','T_BANCOS','T_CALENDARIO','T_CODIGO_ISO','T_CODIGOS_VALIDACIONES_EPS','T_CODIGOS_VALIDACIONES_UIC','T_CONTACTOS','T_CONTACTOS_BANCARIOS','T_CONTACTOS_ENTES','T_CORTES_ARCHIVOS','T_CORTES_ARCHIVOS_DETALLE','T_DIAS_EXCEPCIONALES','T_EMPRESAS_ORDENANTES','T_ENTES','T_ESCALA_MONTOS','T_FACTURACION_MONTOS','T_FLUJOS_INFO','T_FLUJOS_LOTE','T_FLUJOS_TIPO','T_HISTORICO_ESTADO_BANCO','T_HORARIOS_RECEPCION_ARCHIVOS','T_HORARIOS_RECEPCION_ARCHIVOS_DETALLE','T_INICIO_OPERACIONES','T_INTERVALOS_LIQUIDACION','T_LIQUIDACION_FLUJOS_INFO','T_LIQUIDACIONES_ARCHIVOS','T_LIQUIDACIONES_ARCHIVOS_DETALLE','T_MONEDAS','T_MONEDAS_OPERACIONES','T_NUMEROS_ARCHIVOS','T_PARAMETROS_SISTEMA','T_PERIODOS','T_POSICION_ESCALAMIENTO_CONTACTO','T_PROCESOS','T_PRODUCTOS','T_RANGOS_HORAS','T_RESULTADOS_CODIGOS','T_SEMANAS','T_SUB_PRODUCTOS','T_TIPOS_CONTACTOS','T_TIPOS_CONTRIBUYENTES','T_TIPOS_DEVOLUCIONES','T_TIPOS_OPERACIONES','T_TRANS_CHEQUES_LOTES','T_TRANS_CREDITOS_LOTES','T_TRANS_DEV_CHEQUES_LOTES','T_TRANS_DEV_CREDITOS_LOTES','T_TRANS_DEV_DOMICILIACIONES_LOTES','T_TRANS_DOMICILIACIONES_LOTES','T_TRANSACCIONES_CHEQUES','T_TRANSACCIONES_CREDITOS','T_TRANSACCIONES_DEV_CHEQUES','T_TRANSACCIONES_DEV_CREDITOS','T_TRANSACCIONES_DEV_DOMICILIACIONES','T_TRANSACCIONES_DOMICILIACIONES','T_TRANSFERENCIAS_ARCHIVOS','T_TURNOS_CONTACTOS','USERS'];
            $tables_conciliador=['T_ARCHIVOS_DUPLICADOS','T_CONCILIACION','T_IMAGENES','T_MAILO','T_OUTGO','T_PARAMETROS_SISTEMA','T_REGISTROS_DUPLICADOS','T_FLUJOS_TIPO','T_TRANSFERENCIAS_ARCHIVOS'];
            $cr=count($tables_eps);
            $crq=count($array_tables_eps);
            //if ($cr!=$crq){$ct=false; } else {$ct=true;}  
            foreach ($array_tables_eps as $array_tables_eps) {$rq[] = $array_tables_eps->table_name; }
            $errortableseps = array_diff($tables_eps, $rq); // devuelve un array con las tablas que esten diferentes o no existan en la base de datos.
            $cr_errortableseps = count($errortableseps);  // cantidad de errores en las tablas
            if ($cr_errortableseps>0){
                foreach ($errortableseps as $tabla_error) {
                    $log->alert('inicio', 'Error en la Estructura de Alamacenado del EPS la tabla'.$tabla_error.'no existe');
                }
            }
            else{$log->info('inicio', 'Estructura de almacenamiento de la base de datos del EPSACH : OK');}
            // verificar tablas conciliador
            $query ="select table_name from user_tables order by table_name";
            $array_tables_con = DB::connection('oraclecon')->select($query);
            $crc = count($tables_conciliador);
            $crcq = count($array_tables_con);
        //    if ($crc != $crcq) {$ctc=false;} else {$ctc=true;}
            foreach ($array_tables_con as $array_tables_con) {
                $rqc[] = $array_tables_con->table_name;
            }
            $errortablescon = array_diff($tables_conciliador, $rqc);  //devuelve un array con las tablas diferentes
            $cr_errortablescon = count($errortablescon);
            if ($cr_errortablescon>0) {
                foreach ($errortablescon as $tabla_error) {
                    $log->alert('inicio', 'Error en la Estructura de Almacenado del Conciliador la tabla'.$tabla_error.' no existe');
                }
            }
            else{$log->info('inicio', 'Estructura de almacenamiento de la base de datos del Conciliador : OK');}
            // verificacion del directorio principal eps existe
            $directorio_principal_eps=DB::table('t_parametros_sistema')
            ->whereIN('id', [1])
            ->lists('valor', 'id');

            $ruta_directorio_eps = $directorio_principal_eps[1];
            $direps = file_exists($ruta_directorio_eps);  // true si el directorio existe 
            if ($direps) {
                $log->info('inicio', 'Estructura principal EPS  : OK ');    
            }
            else{$log->alert('inicio', 'Error en la Estructura de directorios principal EPS, el directorio '.$ruta_directorio_eps.' NO EXISTE');}
            $directorio_principal_con=DB::table('t_parametros_sistema')
            ->whereIN('id', [103])
            ->lists('valor', 'id');
            $ruta_directorio_con = $directorio_principal_con[103];
            $dircon = file_exists($ruta_directorio_con);  // true si el directorio existe 
            if ($dircon) {
                $log->info('inicio', 'Estructura principal Conciliador   : OK ');
            }
            else{ $log->alert('inicio', 'Error en la Estructura de directorios principal Conciliador, el directorio '.$ruta_directorio_con.' NO EXISTE');}
            $info_bancos=DB::table('t_bancos')
            ->select('codigo','nombre')
            ->where('estatus','=','ACTIVO' )
            ->orderBy('codigo', 'asc')
            ->get();
           //$dir_ex = 0; // contendra el numero de directorios de participantes que si existen 
            $ne = 0;      // contendra el numero de directorios de participantes que no existen  

            foreach ($info_bancos as $info_bancos) {
                $array_info_bancos['nombre'][]=$info_bancos->nombre;
                $array_info_bancos['codigo'][]=$info_bancos->codigo;
                $car_banco[] ="V".$info_bancos->codigo."V01"; 
                $array_ruta_completa_bancos[]=$ruta_directorio_eps.'\\V'.$info_bancos->codigo.'V01';
            };
            foreach ($array_ruta_completa_bancos as $indice =>$ruta_dir_banco) {
                if(file_exists($ruta_dir_banco)==false){
                  $ne=$ne+1;    
                     $log->alert('inicio', 'Error en la Estructura de directorios del EPS de participantes Activos  codigo de participante '.$array_info_bancos['codigo'][$indice]." nombre del participante ".$array_info_bancos['nombre'][$indice]." en la ruta ".$ruta_dir_banco.' NO EXISTE ');
                } 
            }

            if ($ne==0) {
             $log->info('inicio', 'Estructura de almacenamiento del EPS para los Participantes  : OK ');
            }
            //verificacion nivel 1  directorios de cada participante estan creados en el conciliador
            $dir_ex_con = 0; // contendra nuemro de directorios de los participantes q si existen en el conciliador
            $ne_con = 0;     // contendra nuemro de directorios de los participantes q no  existen en el conciliador
            foreach ($car_banco as  $car_banco_esp) {
                $array_ruta_completa_bancos_con[]=$ruta_directorio_con.'\\'.$car_banco_esp;
            }
            foreach ($array_ruta_completa_bancos_con as $indice =>$ruta_dir_banco_con) {
                if (file_exists($ruta_dir_banco_con)==false) {
                   $info_dir_ne_con['codigo'][] = $array_info_bancos['codigo'][$indice];
                    $info_dir_ne_con['nombre'][]=$array_info_bancos['nombre'][$indice];
                    $info_dir_ne_con['ruta'][]=$ruta_dir_banco_con;
                    $ne_con=$ne_con+1;
                    $log->alert('inicio', 'Error en la Estructura de directorios del CONCILIADOR para participantes Activos  codigo de banco '.$array_info_bancos['codigo'][$indice]." nombre del participante ".$array_info_bancos['nombre'][$indice]." en la ruta ".$ruta_dir_banco_con.' NO EXISTE ');
                } 
            }
            if ($ne_con==0) {$log->info('inicio', 'Estructura de almacenamiento del Conciliador para los Participantes  : OK ');}
            // verificacion   nivel 1 otras carpetas necesarias eps
            $ne_other = 0;   // contendra el numero de directorios directorios que no existen 
            $other_dir_ex = 0; // contrendra el numero de directorios directorios que si existen
            $other_dir=['ALCMD','INCMD','OUTCMD','RICMD','ROCMD','SOCMD','TMP','VARECV00','VNCCEV00','VSCOPV00','VSICCV00'];
            foreach ($other_dir as $indice =>$name_other_dir){//$ruta_other_dir[] =$ruta_directorio_eps.'\\'.$name_other_dir;
                    if(file_exists($ruta_directorio_eps.'\\'.$name_other_dir)==false){ 
                        $ne_other = $ne_other + 1;
                        $log->alert('inicio', 'Error en la Estructura de directorios de archivos principales en el EPS, el directorio '.$name_other_dir." en la ruta ".$ruta_directorio_eps.'\\'.$name_other_dir.' NO EXISTE ');
                        
                    }
        }
        if ($ne_other==0){$log->info('inicio', ' Estructura de directorios de archivos principales en el EPS  : OK '); }
            // verificacion   nivel 1 otras carpetas necesarias conciliador
            $other_dir_ex_con = 0; //contendra la cantidad de otras carpetas que si existen
            $ne_other_con = 0;  // contendra la cantidad de otras carpetas que no existen
            $other_dir_con=['RICMD'];
            foreach ($other_dir_con as $name_other_dir_con) {
               // $ruta_other_dir_con[] = $ruta_directorio_con.'\\'.$name_other_dir_con;
                if(file_exists($ruta_directorio_con.'\\'.$name_other_dir_con)==false){
                    $ne_other_con=$ne_other_con+1;
                    $log->alert('inicio', 'Error en la Estructura de directorios de archivos principales en el CONCILIADOR, el directorio '.$name_other_dir_con." en la ruta ".$ruta_directorio_con.'\\'.$name_other_dir_con.' NO EXISTE ');  
                }
            }
            if($ne_other_con==0){$log->info('inicio', ' Estructura de directorios de archivos principales en el Conciliador  : OK ');}

            //validaciones nivel 2 sub carpetsa de archivos de cada banco eps  
            $cr_archivos_ex = 0;  // contendra en numero de directorios de archivos que si existen  eps
            $cr_archivos_no = 0;   // contendra en numero de directorios de archivos que no existen eps
            $dir_archivos = DB::table('t_flujos_tipo')
            ->select('codigo')
            ->where('tipo','<>','DEFAUL' ) 
            ->where('tipo','<>','ASOIN' )
            ->where('tipo','<>','ASOOUT' )
            ->orderBy('codigo', 'asc')
            ->get();

            $salida = '';

            foreach ($car_banco as $indice => $banco) {
                foreach ($dir_archivos as $indice1 => $archivo) {
                    $ruta_archivos_eps[] = $ruta_directorio_eps.'\\'.$banco.'\\'.$archivo->codigo;
                    $ruta_archivos_tem = $ruta_directorio_eps.'\\'.$banco.'\\'.$archivo->codigo;
                    if (file_exists($ruta_archivos_tem)) {
                        $cr_archivos_ex = $cr_archivos_ex + 1;
                    } else {
                        $cr_archivos_no = $cr_archivos_no + 1;

                        $salida .= "\r\n".'['.Carbon::now().'] local.ALERT: Error en la Estructura de directorios de archivos de participantes Activos en el EPS, codigo del banco : '.$array_info_bancos['codigo'][$indice]."  nombre del participante ".$array_info_bancos['nombre'][$indice]." carpeta de archivos ".$dir_archivos[$indice1]->codigo." en la ruta ".$ruta_archivos_tem.' NO EXISTE ';
                    }
                }
            }

            if ($cr_archivos_no==0){$log->info('inicio', ' Estructura de directorios de archivos de los Participantes en el EPS  : OK ');
            }
            // validaciones nivel 2 de sub carpetas de archivos de cada banco conciliador 
            $cr_archivos_ex_con = 0;
            $cr_archivos_no_con = 0;  // contendra en numero de directorios de archivos que no existen conciliador
              $dir_archivos_con=DB::connection('oraclecon')->table('EPSCON'. "." .'t_flujos_tipo')
            ->select('codigo')
            ->orderBy('codigo', 'asc')
            ->get();

            foreach ($car_banco as $indice => $banco) {
                foreach ($dir_archivos_con as $indice1 => $archivo_con) {
                    $ruta_archivos_con[] = $ruta_directorio_con.'\\'.$banco.'\\'.$archivo_con->codigo;
                    $ruta_archivos_tem_con = $ruta_directorio_con.'\\'.$banco.'\\'.$archivo_con->codigo;
                    $re_archivos_ex_con = file_exists($ruta_archivos_tem_con);

                    if ($re_archivos_ex_con) {
                        $cr_archivos_ex_con = $cr_archivos_ex_con + 1;
                    } else {
                        $cr_archivos_no_con = $cr_archivos_no_con + 1;

                        $salida .= "\r\n".'['.Carbon::now().'] local.ALERT: Error en la Estructura de directorios de archivos de participantes Activos en el CONCILIADOR, codigo del banco : '.$array_info_bancos['codigo'][$indice]."  nombre del banco ".$array_info_bancos['nombre'][$indice]." carpeta de archivos ".$dir_archivos_con[$indice1]->codigo." en la ruta ".$ruta_archivos_tem_con.' NO EXISTE - ';   
                    }
                }
            }

            $log->alert('inicio', $salida);   
            
            if ($cr_archivos_no_con==0) {$log->info('inicio', ' Estructura de directorios de archivos de los Participantes en el Conciliador  : OK ');}
            //validacion paar el CÓDIGO DE SISTEMA DE LIQUIDACIÓN
            $parametros = DB::table('t_parametros_sistema')
            ->whereIN('id', [2, 12, 39, 36, 46,102])
            ->lists('valor', 'id');
            $codigoliquidacion=$parametros[46];
            $codigoeps=$parametros[36];
            // validacion para ver si el dia esta cerrado 
            $procesos = Proceso::orderBy('id', 'ASC')
            ->get()
            ->keyBy('id');
            // validacion para los intervalos de liquidacion 
            /** Cantidad de tipos de operaciones activas */
            $tipos_operaciones = TipoOperacion::where('estatus', 'ACTIVO')->count();
             /** Seleccionar la primera liquidacion de cada tipo de operación activa */
            $liquidaciones = TipoOperacion::where('estatus', 'ACTIVO')
            ->leftJoin('t_intervalos_liquidacion as ia', 't_tipos_operaciones.id', '=', 'ia.tipo_operacion_id')
            ->distinct()
            ->count('ia.tipo_operacion_id');
            // validacion al menos 1 moneda activa
            $canmo = Moneda::where('estatus', '=', 'ACTIVO')
            ->count();
           
            // validacion de un producto activo
            $canpro = Producto::where('estatus', '=', 'ACTIVO')
            ->count();
           // validacion del periodo para el inicio del eps 
            $periodos = Periodo::
            select('producto_id','dia_inicio','hora_inicio','dia_fin','hora_fin')
            ->orderBy('producto_id', 'asc')
            ->get();
        
            foreach ($periodos as $indice => $cadap) {
                        $aux=$parametros[39]." ".$cadap->hora_inicio;
                        $aux2=$parametros[2]." ".$cadap->hora_fin;
                        $per_min[]=Carbon::parse($aux)->format('d-m-Y H:i');
                        $per_max[]=Carbon::parse($aux2)->format('d-m-Y H:i');
            }
            $periodo_inicio= Carbon::parse(Collection::make($per_min)->min());
            $periodo_final= Carbon::parse(Collection::make($per_max)->max());


            // condicio que valida que la conexion a la base de datos este estable 
            if ($status == true AND $test == true  AND $ping == true AND $busy == false) {
                $conexion_estable = true;
            } else {
                $conexion_estable = false;
            }

            // condicion que valida que valida que las tablas del eps y la gui esten bien , sin cambios 
            if ($cr_errortableseps == 0 AND $cr_errortablescon == 0 ) {
                $sincambios_tables = true;
            } else {
                $sincambios_tables=false;
            }
            //condicion paar el CÓDIGO DE SISTEMA DE LIQUIDACIÓN
            if($codigoliquidacion != null){
                $valcodliq=true; 
                 $log->info('inicio', ' Codigo de liquidacion   : OK ');
            } else {
                $valcodliq=false;
                $log->alert('inicio', 'Codigo de Liquidacion no Definido');
            }
            //condicion paar el CÓDIGO DEl eps
            if($codigoeps != null){
                $valcodeps=true;
                $log->info('inicio', ' Codigo de sistema EPS   : OK ');
            } else {
                $valcodeps=false;
                $log->alert('inicio', 'Codigo de sistema EPS  NO DEFINIDO ');
            }
            // condicion del dia cerrado
            if($procesos[10]->estatus == 1){
                $diacerrado=true;
            } else {
                $diacerrado=false;
            }
            // condicion intervalos de liquidacion
            if($tipos_operaciones == $liquidaciones){
                $intervalosl=true;
                $log->info('inicio', ' Cierre de dia de Compensancion    : OK ');
            } else {
                $intervalosl=false;
                $log->alert('inicio', ' Dia de Compensancion  NO CERRADO  ');
            }
            // condicion al menos 1 moneda activa 
            if($canmo > 0 ){
                $moneda_activa=true;
            } else {
                $moneda_activa=false;
            }
            // codicion al menos 1 producto activo
            if($canpro > 0 ){
                $producto_activo=true;
                $log->info('inicio', ' Existte al Menos una Modeda Activa    : OK ');
            } else {
                $producto_activo=false;
                $log->alert('inicio', ' NO EXISTEN MONEDAS ACTIVAS ');
            }
            //condicion de los periodos para el inicio del eps
            if(Carbon::now()->diffInMinutes(($periodo_inicio),false)<=$parametros[102] && Carbon::now()->diffInMinutes(($periodo_final),false)>=0 ){
                $periodo_act=true;
                $log->info('inicio', ' Periodo de Compensancion definido correctamente y dentro del intervalo para iniciar    : OK ');
            }
            else{
                $periodo_act=false;
                $log->alert('inicio', ' Periodo de compensancion no definido correctamente o no e encuentra en el tiempo definido para el Inicio  ');
            }
        
            // condicion que valida que ningun archivo falte ni eps ni conciliador    
            if ($direps == true && $dircon == true && $ne == 0 && $ne_con == 0 && $ne_other == 0 && $ne_other_con == 0 && $cr_archivos_no == 0 && $cr_archivos_no_con == 0 ) {
                $directorios_existen=true;
            } else {
                $directorios_existen=false;
            }

            //condicion que valida todas las demas para el inicio de procesos 
            if ($directorios_existen == true &&  $conexion_estable == true && $sincambios_tables == true  && $valcodeps==true && $valcodliq==true && $diacerrado==true && $intervalosl==true && $moneda_activa==true && $producto_activo==true && $periodo_act==true ) {
                $inicio=true;
                $proceso = Proceso::where('id', 11)
                ->first();
                $proceso->fecha_inicio = Carbon::now();
                $proceso->fecha_fin = Carbon::now();
                $proceso->estatus = 1;
                $proceso->update();

                $log->info('inicio', ' PUEDE INICIAR PERIODO DE COMPENSACION CORRECTAMENTE  ');
            } else {
                $inicio=false;
                $log->alert('inicio', ' NO PUEDE INICIAR PERIODO DE COMPENSACION ');
            }

            $condiciones = array(
                "conexion_estable"   => $conexion_estable,
                "sincambios_tables"  => $sincambios_tables,
                "directorios_existen"=> $directorios_existen,
                "codigol"            => $codigoliquidacion,
                "codigoe"            => $codigoeps,
                "diacerrado"         => $diacerrado,
                "intervalosl"        => $intervalosl,
                "moneda_activa"      => $moneda_activa,
                "producto_activo"    => $producto_activo,
                "periodo_activo"     => $periodo_act,
                "inicio"             => $inicio

            );

            return $condiciones;

        } else {
            $condiciones=0;
            return $condiciones;
        }

    } // fin de la funcion


}
