<?php

namespace App\Http\Controllers\Reportes\Transaccional\TiempoLiquidacion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\SesionCompensacion\IntervaloLiquidacion;
use App\Models\Reportes\Transaccional\TiempoLiquidacion\TiempoLiquidacion;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use SPDF;
use Date;
use Config;

class TiempoLiquidacionController extends Controller
{
    protected $extension_excel;
    protected $extension_pdf;
    protected $temporary_folder;

    public function __construct()
    {
        $this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
        $this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
        $this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
    }

    public function getIndex(Request $request)
    {
       $liquidaciones = IntervaloLiquidacion::select('num_liquidacion as num_intervalo_liquidacion')
       ->distinct()
       ->pluck('num_intervalo_liquidacion','num_intervalo_liquidacion')
       ->prepend('Seleccione', '*');

       $liquidacion = $request->get('liquidacion');

        $estatus = [
        '*' => 'Seleccione',
        'FAILED' => 'FAILED',
        'SETTLED' => 'SETTLED',
    ];

        $estatu = $request->get('estatu');

        $busqueda = $request->get('busqueda');


    $fe_desde = ($request->get('FechaDesde') == null || $request->get('FechaDesde') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaDesde');
    $fe_desde = Date('Ymd', strtotime( $fe_desde ));
    $fe_hasta = ($request->get('FechaHasta') == null || $request->get('FechaHasta') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaHasta');
    $fe_hasta = Date('Ymd', strtotime( $fe_hasta ));

    $fechas[1] = Date('Y-m-d', strtotime( ParametroSistema::find(39)->valor ));
    $fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));

    $calendario = DB::table('t_fechas_calendario')
    ->select('fecha_validacion')
    ->whereIn('estatus',['CERRADO','FERIADO','BANCARIO'])
    ->get();
    foreach ($calendario as $calendario) {
      $feriados[] = Date('Y-m-d', strtotime( $calendario->fecha_validacion ));
    };



if(!empty($busqueda)){

    $this->validate($request, [
        'validacion_fechas' => 'fecha_mayor_que:' .
          Date::createFromFormat('Ymd', $fe_desde)->format('Y-m-d') . ',' .
          Date::createFromFormat('Ymd', $fe_hasta)->format('Y-m-d')
      ]);


   $tiempo_liquidacion=TiempoLiquidacion::
     where(function($query) use ($liquidacion, $estatu ) {
          if ($liquidacion != '*') {
              $query->where('num_liquidaciones',$liquidacion);
            };

          if ($estatu != '*') {
              $query->where('estatus',$estatu);
            };
    })
    ->get();
} else {

	$tiempo_liquidacion=null;
}

        return view('reportes.transaccional.tiempo_liquidacion.re_tl_01_i_tiempoliquidacion')
          ->with('liquidacion', $liquidacion)
        ->with('tiempo_liquidacion',$tiempo_liquidacion)
        ->with('busqueda',$busqueda)
        ->with('feriados',$feriados)
        ->with('fechas',$fechas)
        ->with('fe_desde',$fe_desde)
        ->with('fe_hasta',$fe_hasta)
        ->with('estatus', $estatus)
        ->with('estatu', $estatu)
        ->with('liquidaciones', $liquidaciones);
    }

    public function reporteTiempoLiquidacion($num_liquidacion, $estatus, $fe_desde , $fe_hasta, $tipoReporte ){
      //constantes
      $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
      $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

      $fe_desde = $fe_desde == null ? ParametroSistema::find(02)->valor : $fe_desde;
      $fe_hasta = $fe_hasta == null ? ParametroSistema::find(02)->valor : $fe_hasta;

      $tiempo_liquidacion=TiempoLiquidacion::
        where(function($query) use ($num_liquidacion, $estatus ) {
             if ($num_liquidacion != '*') {
                 $query->where('num_liquidaciones',$num_liquidacion);
               };

             if ($estatus != '*') {
                 $query->where('estatus',$estatus);
               };
       })
       ->get();

      //asignacion de variables
      $fe_desde = Date::createFromFormat('Ymd', $fe_desde)->format('d/m/Y');
      $fe_hasta = Date::createFromFormat('Ymd', $fe_hasta)->format('d/m/Y');
      $param = array(
          'tiempo_liquidacion' => $tiempo_liquidacion,
          'fe_desde' => $fe_desde,
          'fe_hasta' => $fe_hasta,
          'fecha_actual' => $fecha_actual_slh
      );

      //tiempo limite para generacion de reporte
      set_time_limit(120);

      //toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construcción PDF
  	      $headerHtml = \View::make('reportes.transaccional.tiempo_liquidacion.re_tl_03_pd_pdfheader', $param)
            ->render();
          //$footerHtml = view()->make('pdf.footer')->render();
          $pdf = SPDF::loadView('reportes.transaccional.tiempo_liquidacion.re_tl_03_pd_pdfbody', $param)
            ->setOption('header-html', $headerHtml)
            ->setOption('margin-top', '50mm')
            ->setTemporaryFolder($this->temporary_folder);
          $pdf->setPaper('a4', 'landscape');
          return $pdf->download('TiempoLiquidacion'.$fecha_actual_flg.'.'.$this->extension_pdf);

          break;
        case 'XLS': //construccion excel
          Excel::create("TiempoLiquidacion".$fecha_actual_flg, function ($excel) use ($param) {
            $excel->setTitle("TiempoLiquidacion");
            $excel->sheet("Hoja1", function ($sheet) use ($param) {
            $sheet->loadView('reportes.transaccional.tiempo_liquidacion.re_tl_02_ex_excel')
            ->with('param', $param);
            });
          })->download($this->extension_excel);
          return back();

	        break;
        default:
          break;
      }

    }
}
