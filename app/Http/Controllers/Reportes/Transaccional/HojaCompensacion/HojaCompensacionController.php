<?php

namespace App\Http\Controllers\Reportes\Transaccional\HojaCompensacion;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\Moneda\Moneda;
use Session;
use DB;
use Illuminate\Database\Connection;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use SPDF;
use Config;

class HojaCompensacionController extends Controller
{
    protected $extension_excel;
		protected $extension_pdf;
		protected $caract_busqueda;
		protected $temporary_folder;

		public function __construct()
		{
				$this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
				$this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
				$this->caract_busqueda = Config::get('constants.reportes.caract_busqueda_trans');
				$this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
		}

    public function getIndex(Request $request)
    {
        //Listado de parámetros para T y T+1
  	    $fechas[1] = Date('Y-m-d', strtotime( ParametroSistema::find(39)->valor ));
  	    $fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));

        //20180924. moneda activa
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
         ->where('t_monedas.estatus','ACTIVO')
         ->first();

        //variables
        $busqueda = $request->get('busqueda');
        $fecha = $request->get('sistema') != null ? $request->get('sistema') : '*';
        $fecha_desde = ($request->get('FechaDesde') == null || $request->get('FechaDesde') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaDesde');
        $fecha_desde = Date('Ymd', strtotime( $fecha_desde ));
        $fecha_hasta = ($request->get('FechaHasta') == null || $request->get('FechaHasta') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaHasta');
        $fecha_hasta = Date('Ymd', strtotime( $fecha_hasta ));

        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';
        $codigo_operacion = $request->get('codigo_operacion') != null ? $request->get('codigo_operacion') : '*';

        //20180924. Arreglo con las fechas de bancarios, feriados y fines
  	    $calendario = DB::table('t_fechas_calendario')
  	    ->select('fecha_validacion')
        ->whereIn('estatus',['CERRADO','FERIADO','BANCARIO'])
  	    ->get();
  	    foreach ($calendario as $calendario) {
  	      $feriados[] = Date('Y-m-d', strtotime( $calendario->fecha_validacion ));
  	    };

        //listado de tipos de operaciones
        $listaOperacion = TipoOperacion::select('codigo', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
        ->where('estatus','ACTIVO')
        //->whereIn('codigo', array('010','020','030'))
        ->orderBy('codigo', 'asc')
        ->pluck('nombre_completo','codigo')
        ->prepend('Seleccione', '');
        //lista de operaciones para validaciones
        $listaOperacionVal = TipoOperacion::select('codigo','nombre')
        ->where('estatus','ACTIVO')
        //->whereIn('codigo', array('010','020','030'))
        ->orderBy('codigo', 'asc')
        ->pluck('nombre','codigo');

        //Listado de participantes
        $bancos = Banco::buscarTodos(['Seleccione', '*']);

        //si se realiza una busqueda desde el boton de buscar
        if(!empty($busqueda)){
          //validacion de fechas
          if ($fecha_desde && $fecha_hasta){
            if (($fecha_desde != '*') && ($fecha_hasta != '*')) {
              //desde no puede ser mayor que hasta
              $this->validate($request, [
                'validacion_fechas' => 'fecha_mayor_que:' .
                  Date::createFromFormat('Ymd', $fecha_desde)->format('Y-m-d') . ',' .
                  Date::createFromFormat('Ymd', $fecha_hasta)->format('Y-m-d')
              ]);
            }
          }
          //busqueda
          $query = "SELECT EPSACH.SP_R0265_PARAMETROS(:cod_banco,:codigo_operacion,:fecha_desde,:fecha_hasta,:caract_busqueda) from dual";
          $arrayVar = array('cod_banco' => $cod_banco,
                        'codigo_operacion' => $codigo_operacion,
                        'fecha_desde' => $fecha_desde,
                        'fecha_hasta' => $fecha_hasta,
                        'caract_busqueda' => $this->caract_busqueda );
          $r0265_parametros = DB::select(DB::raw($query),$arrayVar);
        }else{
          $r0265_parametros = null;
        }
        //retorno de la vista
        return view('reportes.transaccional.hoja_compensacion.re_hc_01_i_hojacompensacion')
        ->with('bancos',$bancos)
        ->with('monedas',$monedas)
        ->with('cod_banco',$cod_banco)
        ->with('r0265_parametros', $r0265_parametros)
        ->with('fe_desde',$fecha_desde)
        ->with('fe_hasta',$fecha_hasta)
        ->with('codigo_operacion',$codigo_operacion)
        ->with('listaOperacion',$listaOperacion)
        ->with('listaOperacionVal',$listaOperacionVal)
        ->with('fecha', $fecha)
        ->with('fechas',$fechas)
        ->with('busqueda', $busqueda)
        ->with('feriados',$feriados);
    }

    public function reporteHojaCompensacion($cod_banco,$codigo_operacion,$fecha_desde,$fecha_hasta,$tipoReporte){
        //constantes
        $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
        $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

        //20180924. moneda activa
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
         ->where('t_monedas.estatus','ACTIVO')
         ->first();

        //parametros para fechas de compensacion
        $fechas[1] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));
  	    $fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));
        //validacion de banco
        if($cod_banco != '*'){
            //Listado de participantes
            $descrip_b = Banco::buscaruno($cod_banco);
            $descrip_banco = $descrip_b->codigo.' - '.$descrip_b->nombre;
        }else{
            $descrip_banco = 'TODOS';
        }
        //var_dump($descrip_banco);
        //seleccion de fecha
        $fecha_desde = $fecha_desde == '*' ? $fechas[1] : $fecha_desde;
        $fecha_desde = Date('Ymd', strtotime( $fecha_desde ));
        $fecha_hasta = $fecha_hasta == '*' ? $fechas[0] : $fecha_hasta;
        $fecha_hasta = Date('Ymd', strtotime( $fecha_hasta ));

        //busqueda
        $query = "SELECT EPSACH.SP_R0265_PARAMETROS(:cod_banco,:codigo_operacion,:fecha_desde,:fecha_hasta,:caract_busqueda) from dual";
        $arrayVar = array('cod_banco' => $cod_banco,
                        'codigo_operacion' => $codigo_operacion,
                        'fecha_desde' => $fecha_desde,
                        'fecha_hasta' => $fecha_hasta,
                        'caract_busqueda' => $this->caract_busqueda );
        $r0265_parametros = DB::select(DB::raw($query),$arrayVar);
        //2018/07/22. asignacion de variables
        $fecha_desde = Date::createFromFormat('Ymd', $fecha_desde)->format('d/m/Y');
        $fecha_hasta = Date::createFromFormat('Ymd', $fecha_hasta)->format('d/m/Y');
        $param = array(
            'r0265_parametros' => $r0265_parametros,
            'fecha_desde' => $fecha_desde,
            'fecha_hasta' => $fecha_hasta,
            'cod_banco' => $cod_banco,
            'descrip_banco' => $descrip_banco,
            'codigo_operacion' => $codigo_operacion,
            'fecha_actual' => $fecha_actual_slh,
            'monedas' => $monedas
        );

        //tiempo limite para generacion de reporte
        set_time_limit(120);

        //2018/07/22. toma en cuenta el tipo de reporte seleccionado
        switch ($tipoReporte) {
          case 'PDF': //2018/07/22. construccion pdf
            $headerHtml = \View::make('reportes.transaccional.hoja_compensacion.re_hc_03_pd_pdfheader', $param)
              ->render();
            //$footerHtml = view()->make('pdf.footer')->render();
            $pdf = SPDF::loadView('reportes.transaccional.hoja_compensacion.re_hc_03_pd_pdfbody', $param)
              ->setOption('header-html', $headerHtml)
              ->setOption('margin-top', '50mm')
              ->setTemporaryFolder($this->temporary_folder);
              //->setOption('footer-html', $footerHtml);
            $pdf->setPaper('a4', 'landscape');
            return $pdf->download('HojaCompensacion'.$fecha_actual_flg.'.'.$this->extension_pdf);

            break;
          case 'XLS': //2018/07/22. construccion excel
            Excel::create("HojaCompensacion".$fecha_actual_flg, function ($excel) use ($param) {
              $excel->setTitle("HojaCompensacion");
              $excel->sheet("HojaCompensacion", function ($sheet) use ($param) {
                $sheet->loadView('reportes.transaccional.hoja_compensacion.re_hc_02_ex_excel')
                ->with('param', $param);
                $sheet->getStyle('A15')->getAlignment()->setWrapText(true);
                $sheet->getStyle('B15')->getAlignment()->setWrapText(true);
                $sheet->getStyle('C15')->getAlignment()->setWrapText(true);
                $sheet->getStyle('D15')->getAlignment()->setWrapText(true);
                $sheet->getStyle('E15')->getAlignment()->setWrapText(true);
                $sheet->getStyle('F15')->getAlignment()->setWrapText(true);
                $sheet->setColumnFormat(array(
                    'B' => '#,##0',
                    'C' => '#,##0.00',
                    'D' => '#,##0',
                    'E' => '#,##0.00',
                    'F' => '#,##0.00'
                ));
              });
            })->download($this->extension_excel);
            return back();

            break;
          default:
            break;
        }

    }

}
