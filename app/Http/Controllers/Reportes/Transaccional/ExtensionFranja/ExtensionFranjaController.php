<?php
namespace App\Http\Controllers\Reportes\Transaccional\ExtensionFranja;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use Session;
use DB;
use Illuminate\Database\Connection;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Input;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use Carbon\Carbon;
use App\Models\Configuracion\Moneda\Moneda;
use Config;
use SPDF;

class ExtensionFranjaController extends Controller
{
    protected $extension_excel;
    protected $extension_pdf;
    protected $caract_busqueda;
    protected $temporary_folder;

    public function __construct()
    {
        $this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
        $this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
        $this->caract_busqueda = Config::get('constants.reportes.caract_busqueda_trans');
        $this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
    }

    public function getIndex(Request $request)
    {
        $cod_rango = '*';
        $cod_archivo = '*';
        $cod_hora_desde = "*";
        $cod_hora_hasta="*";
        $var_cod_desde_r = 0;
        $var_cod_hasta_r = 0;
        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';
        $codigo_operacion = $request->get('codigo_operacion') != null ? $request->get('codigo_operacion') : '*';
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : ParametroSistema::find(02)->valor;
        $page =  $request->get('page');
        $busqueda = $request->get('busqueda');

        //20180919. fechas para el calendario
  	    $fechas[1] = Date('Y-m-d', strtotime( ParametroSistema::find(39)->valor ));
  	    $fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));

        //20180920. moneda activa
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
         ->where('t_monedas.estatus','ACTIVO')
         ->first();

        //20180919. Arreglo con las fechas de bancarios, feriados y fines
  	    $calendario = DB::table('t_fechas_calendario')
  	    ->select('fecha_validacion')
        ->whereIn('estatus',['CERRADO','FERIADO','BANCARIO'])
  	    ->get();
  	    foreach ($calendario as $calendario) {
  	      $feriados[] = Date('Y-m-d', strtotime( $calendario->fecha_validacion ));
  	    };

        //20180919. listado de tipos de operaciones
        $listaOperacion = TipoOperacion::select('codigo', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
        ->where('estatus','ACTIVO')
        //->whereIn('codigo', array('010','020','030','110','120','130'))
        ->orderBy('codigo', 'asc')
        ->pluck('nombre_completo','codigo')
        ->prepend('Seleccione', '*');

        //20180919. Listado de participantes
        $bancos = Banco::buscarTodos(['Seleccione', '*']);

        //20180919. si se realiza una busqueda desde el boton de buscar
  	    if(!empty($busqueda)){
          $query = "SELECT EPSACH.sp_reportes_extension_franja(:cod_banco,:codigo_operacion,:fecha,:fecha,:caract_busqueda) FROM DUAL";
          $arrayVar = array('cod_banco' => $cod_banco,
                      'codigo_operacion' => $codigo_operacion,
                      'fecha' => $fecha,
                      'caract_busqueda' => $this->caract_busqueda );
          $EXTENSION_FRANJA = DB::select(DB::raw($query),$arrayVar);
          $EXTENSION_FRANJA = $this->arrayPaginator($EXTENSION_FRANJA, $request);
          $EXTENSION_FRANJAS = DB::select(DB::raw($query),$arrayVar);
        }else{
  	      $EXTENSION_FRANJA = null;
  	      $EXTENSION_FRANJAS = null;
  	    }

        return view('reportes.transaccional.extension_franja.re_ef_01_i_extensionfranja')
        ->with('EXTENSION_FRANJA',$EXTENSION_FRANJA)
        ->with('EXTENSION_FRANJAS',$EXTENSION_FRANJAS)
        ->with('busqueda', $busqueda)
        ->with('page',$page)
        ->with('listaOperacion', $listaOperacion)
        ->with('codigo_operacion',$codigo_operacion)
        ->with('bancos',$bancos)
        ->with('cod_banco',$cod_banco)
        ->with('fecha',$fecha)
        ->with('fechas',$fechas)
        ->with('monedas',$monedas)
        ->with('feriados',$feriados);
    }

    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = 8;
        $offset = ($page * $perPage) - $perPage;

        return new LengthAwarePaginator(
            array_slice($array, $offset, $perPage, true),
            count($array),
            $perPage,
            $page,
            ['path' => $request->url(), 'query' => $request->query()]
        );
    }

    /*Funcion para crear el archivo a exportar*/
    public function ReporteExtensionFranja($cod_banco,$codigo_operacion,$fecha,$tipoReporte)
    {
      //constantes
      $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
      $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

      //20180920. moneda activa
      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
       ->where('t_monedas.estatus','ACTIVO')
       ->first();

      //seleccion de fecha
      $fecha = $fecha == '*' ? ParametroSistema::find(02)->valor : $fecha;
      $fecha = Date('Ymd', strtotime( $fecha ));

      $query = "SELECT EPSACH.SP_REPORTES_EXTENSION_FRANJA(:cod_banco,:codigo_operacion,:fecha,:fecha,:caract_busqueda) FROM DUAL";
      $arrayVar = array('cod_banco' => $cod_banco,
                  'codigo_operacion' => $codigo_operacion,
                  'fecha' => $fecha,
                  'caract_busqueda' => $this->caract_busqueda );
      $EXTENSION_FRANJA = DB::select(DB::raw($query),$arrayVar);
      //asignacion de variables
      $fecha = Date::createFromFormat('Ymd', $fecha)->format('d/m/Y');
      $param = array(
          'EXTENSION_FRANJA' => $EXTENSION_FRANJA,
          'fecha' => $fecha,
          'fecha_actual' => $fecha_actual_slh,
          'monedas' => $monedas
      );

      //tiempo limite para generacion de reporte
      set_time_limit(120);

      //toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construccion pdf
          $headerHtml = \View::make('reportes.transaccional.extension_franja.re_ef_03_pd_pdfheader', $param)
            ->render();
          //$footerHtml = view()->make('pdf.footer')->render();
          $pdf = SPDF::loadView('reportes.transaccional.extension_franja.re_ef_03_pd_pdfbody', $param)
            ->setOption('header-html', $headerHtml)
            ->setOption('margin-top', '45mm')
            ->setTemporaryFolder($this->temporary_folder);
          $pdf->setPaper('a4', 'landscape');
          return $pdf->download('ExtensionFranja'.$fecha_actual_flg.'.'.$this->extension_pdf);

          break;
        case 'XLS': //construccion excel
          Excel::create("ExtensionFranja".$fecha_actual_flg, function ($excel) use ($param) {
              $excel->setTitle("ExtensionFranja");
              $excel->sheet("ExtensionFranja", function ($sheet) use ($param) {
                  $sheet->loadView('reportes.transaccional.extension_franja.re_ef_02_ex_excel')->with('param', $param );
              });
          })->download($this->extension_excel);
          return back();
          break;
        default:
          break;
      }
    }
}
