<?php

namespace App\Http\Controllers\Reportes\Transaccional\IntercambioImagenesHora;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Reportes\RangoHora;
use DB;
use App\Models\Configuracion\TipoArchivo\TipoArchivo;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use App\Models\Reportes\Transaccional\IntercambioImagenesHora\ImagenesIntercambiadasHora;
use Maatwebsite\Excel\Facades\Excel;
use SPDF;
use Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Config;

class IntercambioImagenesHoraController extends Controller
{
    protected $extension_excel;
    protected $extension_pdf;
    protected $temporary_folder;

    public function __construct()
    {
        $this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
        $this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
        $this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
    }

    public function getIndex(Request $request)
    {

    $cod_banco_emisor = $request->get('cod_banco_emisor') == null ? '*' : $request->get('cod_banco_emisor');
    $cod_banco_receptor = $request->get('cod_banco_receptor') == null ? '*' : $request->get('cod_banco_receptor');

    $cod_hora_desde = $request->get('cod_hora_desde') == null ? 0 : $request->get('cod_hora_desde');
    $cod_hora_hasta= $request->get('cod_hora_hasta') == null ? 0 : $request->get('cod_hora_hasta');
    $busqueda = $request->get('busqueda');
    $page =  $request->get('page');

    $fe_desde = ($request->get('FechaDesde') == null || $request->get('FechaDesde') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaDesde');
    $fe_desde = Date('Ymd', strtotime( $fe_desde ));
    $fe_hasta = ($request->get('FechaHasta') == null || $request->get('FechaHasta') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaHasta');
    $fe_hasta = Date('Ymd', strtotime( $fe_hasta ));

    $listaDesde = RangoHora::select('id', DB::raw("SUBSTR(nombre, 1,6) AS rango_horas"))
    ->pluck('rango_horas','id')
    ->prepend('Seleccione', '0');

    $listaHasta = RangoHora::select('id', DB::raw("SUBSTR(nombre, 9,12) AS rango_horas"))
    ->pluck('rango_horas','id')
    ->prepend('Seleccione', '0');

    $listaParticipante = Banco::buscarTodos(['Seleccione', '*']);

    $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
     ->where('t_monedas.estatus','ACTIVO')
     ->first();

    if(!empty($busqueda)){

      $this->validate($request, [
        'validacion_fechas' => 'fecha_mayor_que:' .
          Date::createFromFormat('Ymd', $fe_desde)->format('Y-m-d') . ',' .
          Date::createFromFormat('Ymd', $fe_hasta)->format('Y-m-d')
      ]);

      if(($cod_hora_desde != 0) && ($cod_hora_hasta != 0)){
        $this->validate($request,[
          'cod_hora_desde' => 'mayor_que:'.$cod_hora_desde.','.$cod_hora_hasta
        ]);
      }else if(($cod_hora_desde == 0) && ($cod_hora_hasta == 0)){
     		 }else{
        			$this->validate($request,['cod_hora_desde' => 'no_cero:'.$cod_hora_desde.',Hora desde']);
        			$this->validate($request,['cod_hora_hasta' => 'no_cero:'.$cod_hora_hasta.',Hora hasta']);
      }
  }

 if(!empty($busqueda)){

     $Registros = ImagenesIntercambiadasHora::
       where(function($query) use ($cod_banco_emisor, $cod_banco_receptor , $cod_hora_desde , $cod_hora_hasta) {
          if ($cod_banco_emisor != '*') {
              $query->where('cod_banco_emisor',$cod_banco_emisor);
            };

          if ($cod_banco_receptor != '*') {
              $query->where('cod_banco_pagador',$cod_banco_receptor);
            };

          if($cod_hora_desde != 0 && $cod_hora_hasta != 0){
          	 $query->whereBetween('hora_id', [$cod_hora_desde, $cod_hora_hasta])->get();
          }
    })
    ->get()
    ->sortBy('cod_banco_pagador')
    ->sortBy('cod_banco_emisor')
    ->sortBy('hora_id');

    //se calculan los totales previamente a la paginacion del objeto
    $total_operaciones = [];
    $total_operaciones[0] = $Registros->sum('archivos_img_env');
    $total_operaciones[1] = $Registros->sum('cant_operaciones_env');
    $total_operaciones[2] = $Registros->sum('cant_img_env');
    $total_operaciones[3] = $Registros->sum('cant_img_concilia_env');
    $total_operaciones[4] = $Registros->sum('archivos_img_rec');
    $total_operaciones[5] = $Registros->sum('bilat_cant_operaciones_rec');
    $total_operaciones[6] = $Registros->sum('cant_img_rec');
    $total_operaciones[7] = $Registros->sum('cant_img_concilia_rec');

    $Registros = $this->arrayPaginator($Registros->toArray(), $request);

}else{
      $Registros = null;
      $total_operaciones = null;
    }

    return view('reportes.transaccional.intercambio_imagenes_hora.re_ih_01_i_imageneshora')
    ->with('page',$page)
    ->with('monedas',$monedas)
    ->with('img_intercambiadas',$Registros)
    ->with('listaDesde',$listaDesde)
    ->with('listaHasta',$listaHasta)
    ->with('listaParticipante',$listaParticipante)
    ->with('cod_banco_emisor',$cod_banco_emisor)
    ->with('cod_banco_receptor',$cod_banco_receptor)
    ->with('cod_hora_desde',$cod_hora_desde)
    ->with('cod_hora_hasta',$cod_hora_hasta)
    ->with('fe_desde',$fe_desde)
    ->with('fe_hasta',$fe_hasta)
    ->with('total_operaciones',$total_operaciones);
    }

    public function reporteIntercambioImagenesHora($cod_banco_emisor, $cod_banco_receptor, $desde , $hasta ,  $cod_hora_desde , $cod_hora_hasta, $tipoReporte){
      //constantes
      $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
      $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

      $desde = $desde == null ? ParametroSistema::find(02)->valor : $desde;
      $hasta = $hasta == null ? ParametroSistema::find(02)->valor : $hasta;
      $cod_hora_desde = $cod_hora_desde == null ? 0 : $cod_hora_desde;
      $cod_hora_hasta = $cod_hora_hasta == null ? 0 : $cod_hora_hasta;

      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
       ->where('t_monedas.estatus','ACTIVO')
       ->first();

      $img_intercambiadas= ImagenesIntercambiadasHora::
       where(function($query) use ($cod_banco_emisor, $cod_banco_receptor , $cod_hora_desde , $cod_hora_hasta) {
          if ($cod_banco_emisor != '*') {
              $query->where('cod_banco_emisor',$cod_banco_emisor);
            };

          if ($cod_banco_receptor != '*') {
              $query->where('cod_banco_pagador',$cod_banco_receptor);
            };

          if($cod_hora_desde != 0 && $cod_hora_hasta != 0){
             $query->whereBetween('hora_id', [$cod_hora_desde, $cod_hora_hasta])->get();
          }
    })
    ->get()
    ->sortBy('cod_banco_pagador')
    ->sortBy('cod_banco_emisor')
    ->sortBy('hora_id');

    //asignacion de variables
    $desde = Date::createFromFormat('Ymd', $desde)->format('d/m/Y');
    $hasta = Date::createFromFormat('Ymd', $hasta)->format('d/m/Y');
    $param = array(
        'img_intercambiadas' => $img_intercambiadas,
        'desde' => $desde,
        'hasta' => $hasta,
        'fecha_actual' => $fecha_actual_slh,
        'monedas' => $monedas
    );

    //tiempo limite para generacion de reporte
    set_time_limit(120);

    //toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construcción PDF
            $headerHtml = \View::make('reportes.transaccional.intercambio_imagenes_hora.re_ih_03_pd_pdfheader', $param)
              ->render();
            //$footerHtml = view()->make('pdf.footer')->render();
            $pdf = SPDF::loadView('reportes.transaccional.intercambio_imagenes_hora.re_ih_03_pd_pdfbody', $param)
              ->setOption('header-html', $headerHtml)
              ->setOption('margin-top', '50mm')
              ->setTemporaryFolder($this->temporary_folder);
            $pdf->setPaper('a4', 'landscape');
            return $pdf->download('IntercambiaoImagenesPorHora'.$fecha_actual_flg.'.'.$this->extension_pdf);

            break;
        case 'XLS': //construccion excel

            Excel::create("IntercambiaoImagenesPorHora".$fecha_actual_flg, function ($excel) use ($img_intercambiadas,$desde,$hasta,$fecha_actual_slh) {
            $excel->setTitle("IntercambiaoImagenesPorHora");
            $excel->sheet("Hoja1", function ($sheet) use ($img_intercambiadas,$desde,$hasta,$fecha_actual_slh) {
            $sheet->loadView('reportes.transaccional.intercambio_imagenes_hora.re_ih_02_ex_excel')
            ->with('img_intercambiadas', $img_intercambiadas)
            ->with('desde', $desde)
            ->with('hasta', $hasta)
            ->with('fecha_actual', $fecha_actual_slh);
            $sheet->setColumnFormat(array(
                'D' => '#,##0',
                'E' => '#,##0',
                'F' => '#,##0',
                'G' => '#,##0',
                'H' => '#,##0',
                'I' => '#,##0',
                'J' => '#,##0',
                'K' => '#,##0'
            ));
            });
          })->download($this->extension_excel);
          return back();

            break;
        default:
          break;
      }

     }

     //Paginador
     public function arrayPaginator($array, $request)
     {
         //return count($array);
         $page = Input::get('page', 1);
         $perPage = 8;
         $offset = ($page * $perPage) - $perPage;
           return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
             ['path' => $request->url(), 'query' => $request->query()]);
     }

}
