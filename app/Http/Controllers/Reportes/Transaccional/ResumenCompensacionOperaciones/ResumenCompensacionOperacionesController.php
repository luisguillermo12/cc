<?php

namespace App\Http\Controllers\Reportes\Transaccional\ResumenCompensacionOperaciones;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use SPDF;
use DB;
use Input;
use Maatwebsite\Excel\Facades\Excel;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use Config;

class ResumenCompensacionOperacionesController extends Controller
{
	protected $extension_excel;
	protected $extension_pdf;
	protected $caract_busqueda;
	protected $temporary_folder;

	public function __construct()
	{
			$this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
			$this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
			$this->caract_busqueda = Config::get('constants.reportes.caract_busqueda_trans');
			$this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
	}

	public function getIndex(Request $request){

		//20180924. Listado de participantes
		$bancos = Banco::buscarTodos(['Seleccione', '*']);

		$operaciones = TipoOperacion::select('codigo', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
        ->where('estatus','ACTIVO')
        ->orderBy('codigo', 'asc')
        ->pluck('nombre_completo','codigo')
        ->prepend('Seleccione', '*');

		$nombre_operacion="";

		//20180924. moneda activa
		$monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
		 ->where('t_monedas.estatus','ACTIVO')
		 ->first();

		//fechas para el calendario
		$fechas[1] = Date('Y-m-d', strtotime( ParametroSistema::find(39)->valor ));
		$fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));
		//Arreglo con las fechas de bancarios, feriados y fines
		$calendario = DB::table('t_fechas_calendario')
		->select('fecha_validacion')
		->whereIn('estatus',['CERRADO','FERIADO','BANCARIO'])
		->get();
		foreach ($calendario as $calendario) {
			$feriados[] = Date('Y-m-d', strtotime( $calendario->fecha_validacion ));
		};

		//fin variables querys
		//variables http

		$Pparticipante=$request->get('bancos');
		$Poperaciones=$request->get('operaciones');
		$Pfechai = $request->get('fechai') != null ? $request->get('fechai') : ParametroSistema::find(02)->valor;
		$Pfechaf = $request->get('fechaf') != null ? $request->get('fechaf') : ParametroSistema::find(02)->valor;
		//fin variables http

		//variables de calculos y otros
		$parametros="";
		$valor=0;
		$insercion="";
		$Pnombre="";
		$Pparticipantecomp="";
		$parametros010="";
		$parametros020="";
		$parametros030="";
		$parametros110="";
		$parametros120="";
		$parametros130="";
		$Pcontraparte='*';
		//fin variables de calculos y otros


		if (
			$Pparticipante!=null ||
			$Poperaciones !=null
			) {
			$this->validate($request, [
            'fecha_mayor_que:' .
              Date::createFromFormat('Ymd', $Pfechai)->format('Y-m-d') . ',' .
              Date::createFromFormat('Ymd', $Pfechaf)->format('Y-m-d')
          ]);

			$Pnombre=DB::select("SELECT BAN.NOMBRE FROM EPSACH.T_BANCOS BAN WHERE BAN.CODIGO='".$Pparticipante."'");
			//$Pparticipantecomp=$Pparticipante.'-'.$Pnombre[0]->nombre;
			if ($Poperaciones=='*') {
				//echo "valor";

				$nombre_operacion = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
		    ->where('estatus','ACTIVO')
		    ->orderBy('codigo', 'asc')
				->get();

				$valor=1;
				$insercion="var";
				if($Pparticipante!='*'){
					$mod010=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$mod020=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','020','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$mod030=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','030','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$mod110=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','110','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$mod120=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','120','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$mod130=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','130','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");

					foreach ($mod010 as $key => $value) {
						if ($value->nombre_banco) {
							$parametros010[$key]=$value;
						}
					}
					foreach ($mod020 as $key => $value) {
						if ($value->nombre_banco) {
							$parametros020[$key]=$value;
						}
					}
					foreach ($mod030 as $key => $value) {
						if ($value->nombre_banco) {
							$parametros030[$key]=$value;
						}
					}
					foreach ($mod110 as $key => $value) {
						if ($value->nombre_banco) {
							$parametros110[$key]=$value;
						}
					}
					foreach ($mod120 as $key => $value) {
						if ($value->nombre_banco) {
							$parametros120[$key]=$value;
						}
					}
					foreach ($mod130 as $key => $value) {
						if ($value->nombre_banco) {
							$parametros130[$key]=$value;
						}
					}
				}else{
					$parametros010=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$parametros020=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','020','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$parametros030=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','030','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$parametros110=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','110','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$parametros120=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','120','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					$parametros130=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','130','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
				}

			}else{
				//$nombre_operacion=DB::select("SELECT TOPE.NOMBRE FROM EPSACH.T_TIPOS_OPERACIONES TOPE WHERE TOPE.CODIGO='".$Poperaciones."'")[0]->nombre;

				$nombre_operacion = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
		    ->where('estatus','ACTIVO')
		    ->where('codigo', $Poperaciones)
		    ->orderBy('codigo', 'asc')
				->first();

				$valor=2;
				$insercion="var";
				if ($Pparticipante!='*') {
					$mod=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','".$Poperaciones."','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
					foreach ($mod as $key => $value) {
						if($value->nombre_banco){
							$parametros[$key]=$value;
						}

					}
				}else{
					$parametros=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','".$Poperaciones."','".$Pfechai."','".$Pfechaf."','".$this->caract_busqueda."') FROM DUAL");
				}

			}



		}
		return view('reportes.transaccional.resumen_compensacion_operaciones.re_rc_01_i_resumencompensacion')
		->with('bancos',$bancos)
		->with('operaciones',$operaciones)
		->with('insercion',$insercion)
		->with('parametros',$parametros)
		->with('codigo_participante',$Pparticipantecomp)
		->with('valor',$valor)
		->with('Poperaciones',$Poperaciones)
		->with('Pparticipante',$Pparticipante)
		->with('Pfechai',$Pfechai)
		->with('Pfechaf',$Pfechaf)
		->with('fechas',$fechas)
		->with('feriados',$feriados)
		->with('monedas',$monedas)
		->with('paginadord',$parametros010)
		->with('paginadorv',$parametros020)
		->with('paginadort',$parametros030)
		->with('paginadorcd',$parametros110)
		->with('paginadorcv',$parametros120)
		->with('paginadorct',$parametros130)
		->with('noperacion',$nombre_operacion);

	}

   public function ResumenCompensacionPDF($Pparticipante,$Poperaciones,$Fechai,$Fechaf){
		//constantes
		$fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
		$fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

		//20180924. moneda activa
		$monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
		 ->where('t_monedas.estatus','ACTIVO')
		 ->first();

   	   	$bancos = Banco::select('codigo', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS banco"))
        ->where('estatus','ACTIVO')
        ->where('codigo',$Pparticipante)->get();

        $bancoDef="";

   		$info="";
   		$nombre="RESUMEN DE COMPENSACI&OacuteN POR TIPO DE OPERACI&OacuteN";
   		$fecha= Carbon::now()->format('YmdHis');
   		$info=array(
   			'fechai'=>Date::createFromFormat('Ymd', $Fechai)->format('d/m/Y'),
   			'fechaf'=>Date::createFromFormat('Ymd', $Fechai)->format('d/m/Y'),
				'fecha_actual'=> $fecha_actual_slh,
   			'nombre'=>$nombre);
   		$header=\View::make('reportes.transaccional.resumen_compensacion_operaciones.re_rc_03_pd_pdfheader',compact('info',$info))->render();
   		$parametros="";
   		$pdf="";
   		$Pcontraparte='*';
   		if($Poperaciones=='*'){
   			if($Pparticipante!='*'){

 				$mod010=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod020=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','020','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod030=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','030','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod110=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','110','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod120=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','120','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod130=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','130','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");

				foreach ($mod010 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros010[$key]=$value;
					}
				}
				foreach ($mod020 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros020[$key]=$value;
					}
				}
				foreach ($mod030 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros030[$key]=$value;
					}
				}
				foreach ($mod110 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros110[$key]=$value;
					}
				}
				foreach ($mod120 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros120[$key]=$value;
					}
				}
				foreach ($mod130 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros130[$key]=$value;
					}
				}
			}else{
 				$parametros010=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros020=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','020','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros030=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','030','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros110=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','110','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros120=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','120','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros130=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','130','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
			}

			$operacionDef = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
			->where('estatus','ACTIVO')
			->orderBy('codigo', 'asc')
			->get();

		   	$informacion=array(
		   		'parametrosd'=>$parametros010,
		   		'parametrosv'=>$parametros020,
		   		'parametrost'=>$parametros030,
		   		'parametroscd'=>$parametros110,
		   		'parametroscv'=>$parametros120,
		   		'parametrosct'=>$parametros130,
				'monedas' => $monedas,
		   		'fechaf'=>$Fechaf,
		   		'fecha'=>$Fechai,
		   		'Pparticipante'=>$Pparticipante,
		   		'operaciones'=>$operacionDef);
		   	$pdf=SPDF::loadView('reportes.transaccional.resumen_compensacion_operaciones.re_rc_03_pd_pdfbody_all', compact('informacion',$informacion))
		   	->setOption('header-html',$header)
		   	->setOption('margin-top','50mm')
				->setTemporaryFolder($this->temporary_folder);
		    $pdf->setPaper('a4','landscape');
   		}else{
   			// echo "SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Fechai."','".$Fechaf."') FROM DUAL";
				if ($Pparticipante!='*') {
					$mod=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','".$Poperaciones."','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
					foreach ($mod as $key => $value) {
						if($value->nombre_banco){
							$parametros[$key]=$value;
						}

					}
				}else{
					$parametros=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','".$Poperaciones."','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
				}

				$operacionDef = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
		    ->where('estatus','ACTIVO')
		    ->where('codigo', $Poperaciones)
		    ->orderBy('codigo', 'asc')
				->first();

		   	$informacion=array(
		   		'parametros'=>$parametros,
		   		'fechaf'=>$Fechaf,
					'monedas' => $monedas,
					'fecha'=>$Fechai,
		   		'Pparticipante'=>$Pparticipante,
		   		'operaciones'=>$operacionDef);
		   	$pdf=SPDF::loadView('reportes.transaccional.resumen_compensacion_operaciones.re_rc_03_pd_pdfbody', compact('informacion',$informacion))
		   	->setOption('header-html',$header)
		   	->setOption('margin-top','50mm')
				->setTemporaryFolder($this->temporary_folder);
		    $pdf->setPaper('a4','landscape');

   		}
	   	return $pdf->download('ResumenCompensacion'.$fecha_actual_flg.'.'.$this->extension_pdf);
   }



   public function ResumenCompensacionEXCEL($Pparticipante,$Poperaciones,$Fechai,$Fechaf){
		 //constantes
		 $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
		 $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

		//20180924. moneda activa
		$monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
		 ->where('t_monedas.estatus','ACTIVO')
		 ->first();

   		$bancos = Banco::select('codigo', DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS banco"))
        ->where('estatus','ACTIVO')
        ->where('codigo',$Pparticipante)->get();
        $bancoDef="";
    	$parametros="";

    	if (sizeof($bancos)>0) {
    		$bancoDef=$bancos[0]->banco;
    	}

   		$pdf="";
   		$Pcontraparte='*';

   		if($Poperaciones=='*'){
   			if($Pparticipante!='*'){
 				$mod010=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod020=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','020','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod030=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','030','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod110=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','110','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod120=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','120','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$mod130=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','130','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");

				foreach ($mod010 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros010[$key]=$value;
					}
				}
				foreach ($mod020 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros020[$key]=$value;
					}
				}
				foreach ($mod030 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros030[$key]=$value;
					}
				}
				foreach ($mod110 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros110[$key]=$value;
					}
				}
				foreach ($mod120 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros120[$key]=$value;
					}
				}
				foreach ($mod130 as $key => $value) {
					if ($value->nombre_banco) {
						$parametros130[$key]=$value;
					}
				}
			}else{
				$bancoDef="TODOS";
 				$parametros010=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','010','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros020=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','020','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros030=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','030','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros110=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','110','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros120=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','120','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
   				$parametros130=DB::select("SELECT EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','130','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
			}

			$operacionDef = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
			->where('estatus','ACTIVO')
			->orderBy('codigo', 'asc')
			->get();

		   	$informacion=array(
		   		'parametrosd'=>$parametros010,
		   		'parametrosv'=>$parametros020,
		   		'parametrost'=>$parametros030,
		   		'parametroscd'=>$parametros110,
		   		'parametroscv'=>$parametros120,
		   		'parametrosct'=>$parametros130,
					'monedas' => $monedas,
					'operaciones' => $operacionDef,
	   			'fecha_param'=>Date::createFromFormat('Ymd', $Fechai)->format('d/m/Y'),
					'fecha_param2'=>Date::createFromFormat('Ymd', $Fechaf)->format('d/m/Y'),
		   		'fecha'=> $fecha_actual_slh,
		   		'Pparticipante'=>$Pparticipante,
		   		'bancos'=>$bancoDef);

					//tiempo limite para generacion de reporte
					set_time_limit(120);

		   	Excel::create("ResumenCompensacion".$fecha_actual_flg, function ($excel) use ($informacion) {
    		$excel->setTitle("ResumenCompensacion");
    		$excel->sheet("ResumenCompensacion", function ($sheet) use ($informacion) {
	      		$sheet->loadView('reportes.transaccional.resumen_compensacion_operaciones.re_rc_02_ex_excel_all')->with('informacion', $informacion);
	      		$sheet->getStyle('A9')->getAlignment()->setWrapText(true);
           	 	$sheet->getStyle('B9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('C9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('D9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('E9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('F9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('A10')->getAlignment()->setWrapText(true);
           	 	$sheet->getStyle('B10')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('C10')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('D10')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('E10')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('F10')->getAlignment()->setWrapText(true);
            	$sheet->setColumnFormat(array(
                'B' => '#,##0',
            		'C' => '#,##0.00',
                'D' => '#,##0',
                'E' => '#,##0.00',
                'F' => '#,##0.00',
								'G' => '#,##0.00'
            ));

	      		});
  		})->download($this->extension_excel);
   		}
   		if ($Poperaciones!='*') {
			if ($Pparticipante!='*') {
				$mod=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','".$Poperaciones."','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
				foreach ($mod as $key => $value) {
					if($value->nombre_banco){
						$parametros[$key]=$value;
					}

				}
			}else{
				$bancoDef="TODOS";
				$parametros=DB::select("select EPSACH.RESUMEN_COMPENSACION_POR_TIPO('".$Pparticipante."','".$Pcontraparte."','".$Poperaciones."','".$Fechai."','".$Fechaf."','".$this->caract_busqueda."') FROM DUAL");
			}

			$operacionDef = TipoOperacion::select(DB::raw("CONCAT(CONCAT(codigo,' - '),nombre) AS nombre_completo"))
			->where('estatus','ACTIVO')
			->where('codigo', $Poperaciones)
			->orderBy('codigo', 'asc')
			->first();

		   	$informacion=array(
		   		'parametros'=>$parametros,
					'fecha_param'=>Date::createFromFormat('Ymd', $Fechai)->format('d/m/Y'),
					'fecha_param2'=>Date::createFromFormat('Ymd', $Fechaf)->format('d/m/Y'),
		   		'fecha'=> $fecha_actual_slh,
		   		'Pparticipante'=>$Pparticipante,

		   		'operaciones'=>$operacionDef,
		   		'bancos'=>$bancoDef);

					//tiempo limite para generacion de reporte
					set_time_limit(120);

		   	 Excel::create("ResumenCompensacion".$fecha_actual_flg, function ($excel) use ($informacion) {
    			$excel->setTitle("ResumenCompensacion");
    			$excel->sheet("ResumenCompensacion", function ($sheet) use ($informacion) {
	      		$sheet->loadView('reportes.transaccional.resumen_compensacion_operaciones.re_rc_02_ex_excel')->with('informacion', $informacion);
	      		$sheet->getStyle('A9')->getAlignment()->setWrapText(true);
           	 	$sheet->getStyle('B9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('C9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('D9')->getAlignment()->setWrapText(true);
            	$sheet->getStyle('E9')->getAlignment()->setWrapText(true);
            	$sheet->setColumnFormat(array(
								'B' => '#,##0',
            		'C' => '#,##0.00',
                'D' => '#,##0',
                'E' => '#,##0.00',
                'F' => '#,##0.00',
								'G' => '#,##0.00'
            ));

	      		});
  		})->download($this->extension_excel);
   		}

  		return back();
		}

}
