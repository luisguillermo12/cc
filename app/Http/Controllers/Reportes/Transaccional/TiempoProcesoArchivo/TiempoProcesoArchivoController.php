<?php

namespace App\Http\Controllers\Reportes\Transaccional\TiempoProcesoArchivo;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use SPDF;
use Config;

class TiempoProcesoArchivoController extends Controller
{
    protected $extension_excel;
    protected $extension_pdf;
    protected $temporary_folder;

    public function __construct()
    {
        $this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
        $this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
        $this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
    }

    public function getIndex(Request $request)
    {
        $cod_banco = $request->get('cod_banco') != null ? $request->get('cod_banco') : '*';
        $fecha = $request->get('fecha') != null ? $request->get('fecha') : ParametroSistema::find(02)->valor;
        $busqueda = $request->get('busqueda');

        //Listado de participantes
        $bancos = Banco::buscarTodos(['Seleccione', '*']);

        //moneda activa
        $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
         ->where('t_monedas.estatus','ACTIVO')
         ->first();

        //parametros para fechas de compensacion
        $fechas[1] = Date('Y-m-d', strtotime( ParametroSistema::find(39)->valor ));
        $fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));

        //Arreglo con las fechas de bancarios, feriados y fines
        $calendario = DB::table('t_fechas_calendario')
        ->select('fecha_validacion')
        ->whereIn('estatus',['CERRADO','FERIADO','BANCARIO'])
        ->get();
        foreach ($calendario as $calendario) {
          $feriados[] = Date('Y-m-d', strtotime( $calendario->fecha_validacion ));
        };

        //si se realiza una busqueda desde el boton de buscar
  	    if(!empty($busqueda)){

          $query = "SELECT EPSACH.sp_reportes_tiempo_proceso_archivos_icom(:cod_banco,:fecha) FROM dual";
	      	$arrayVar = array('cod_banco' => $cod_banco,
	      				            'fecha' => $fecha );
	      	$Tiempo_archivo_icom = collect(DB::select(DB::raw($query),$arrayVar));

          $Total_tiempo_bancos_icom = [new \stdClass()];
          $Total_tiempo_bancos_icom[0]->total_archivo = $Tiempo_archivo_icom->count();
          $Total_tiempo_bancos_icom[0]->total_operaciones = $Tiempo_archivo_icom->sum('operaciones');
          //calculo de Tiempos
          $totalDuration = 0;
          foreach ($Tiempo_archivo_icom as $valor) {
            $startTime = Carbon::parse($valor->hor_ini);
            $finishTime = Carbon::parse($valor->hor_fin);
            $totalDuration = $totalDuration + $finishTime->diffInSeconds($startTime);
          }
          //tiempo total
          $Total_tiempo_bancos_icom[0]->tiempo_procesamiento = gmdate("H",floor($totalDuration / 3600)).gmdate(":i:s", $totalDuration % 3600);
          //tiempo promedio total
          $totalDuration = $totalDuration / ($Tiempo_archivo_icom->count() == 0 ? 1 : $Tiempo_archivo_icom->count());
          $Total_tiempo_bancos_icom[0]->tiempo_promedio = gmdate("H",floor($totalDuration / 3600)).gmdate(":i:s", $totalDuration % 3600);

          $query = "SELECT EPSACH.sp_reportes_tiempo_proceso_archivos_icoma(:cod_banco,:fecha) FROM dual";
          $arrayVar = array('cod_banco' => $cod_banco,
                            'fecha' => $fecha );
          $Tiempo_archivo_icoma = collect(DB::select(DB::raw($query),$arrayVar));

          $Total_tiempo_bancos_icoma = [new \stdClass()];
          $Total_tiempo_bancos_icoma[0]->total_archivo = $Tiempo_archivo_icoma->count();
          $Total_tiempo_bancos_icoma[0]->total_operaciones = $Tiempo_archivo_icoma->sum('operaciones');
          //calculo de Tiempos
          $totalDuration = 0;
          foreach ($Tiempo_archivo_icoma as $valor) {
            $startTime = Carbon::parse($valor->hor_ini);
            $finishTime = Carbon::parse($valor->hor_fin);
            $totalDuration = $totalDuration + $finishTime->diffInSeconds($startTime);
          }
          //tiempo total
          $Total_tiempo_bancos_icoma[0]->tiempo_procesamiento = gmdate("H",floor($totalDuration / 3600)).gmdate(":i:s", $totalDuration % 3600);
          //tiempo promedio total
          $totalDuration = $totalDuration / ($Tiempo_archivo_icoma->count() == 0 ? 1 : $Tiempo_archivo_icoma->count());
          $Total_tiempo_bancos_icoma[0]->tiempo_promedio = gmdate("H",floor($totalDuration / 3600)).gmdate(":i:s", $totalDuration % 3600);
        } else {
          $Tiempo_archivo_icom = [new \stdClass()];
          $Total_tiempo_bancos_icom = [new \stdClass()];
          $Tiempo_archivo_icoma = [new \stdClass()];
          $Total_tiempo_bancos_icoma = [new \stdClass()];
        }

        return view('reportes.transaccional.tiempo_proceso_archivo.re_tp_01_i_tiempoproceso')
        ->with('monedas', $monedas)
        ->with('bancos',$bancos)
        ->with('cod_banco',$cod_banco)
        ->with('Tiempo_archivo_icom', $Tiempo_archivo_icom)
        //->with('Total_tiempo_archivo_icom', $Total_tiempo_archivo_icom)
        ->with('Total_tiempo_bancos_icom', $Total_tiempo_bancos_icom)
        ->with('Tiempo_archivo_icoma', $Tiempo_archivo_icoma)
        //->with('Total_Tiempo_archivo_icoma', $Total_Tiempo_archivo_icoma)
        ->with('Total_tiempo_bancos_icoma', $Total_tiempo_bancos_icoma)
        ->with('fecha_intercambio', $fecha)
        ->with('fecha', $fecha)
        ->with('fechas', $fechas)
        ->with('busqueda', $busqueda)
        ->with('feriados', $feriados);
    }

    public function reporteTiempoProcesoArchivoICOM($cod_banco,$fecha,$tipoReporte) {
      //constantes
      $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
      $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

      //moneda activa
      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
       ->where('t_monedas.estatus','ACTIVO')
       ->first();

       $cod_banco = $cod_banco == null ? '*' : $cod_banco;
       $fecha = $fecha == null ? ParametroSistema::find(02)->valor : $fecha;

       $query = "SELECT EPSACH.sp_reportes_tiempo_proceso_archivos_icom(:cod_banco,:fecha) FROM dual";
       $arrayVar = array('cod_banco' => $cod_banco,
                         'fecha' => $fecha );
       $Tiempo_archivo_icom = collect(DB::select(DB::raw($query),$arrayVar));

       $fecha = Date::createFromFormat('Ymd', $fecha)->format('d/m/Y');
       $param = array(
          'TiempoProceso' => $Tiempo_archivo_icom,
          'fecha' => $fecha,
          'fecha_actual' => $fecha_actual_slh,
          'monedas' => $monedas
      );

      //tiempo limite para generacion de reporte
      set_time_limit(120);

      //toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construcción PDF
  	      $headerHtml = \View::make('reportes.transaccional.tiempo_proceso_archivo.re_tp_03_pd_pdfheader', $param)
            ->render();
          //$footerHtml = view()->make('pdf.footer')->render();
          $pdf = SPDF::loadView('reportes.transaccional.tiempo_proceso_archivo.re_tp_03_pd_pdfbody', $param)
            ->setOption('header-html', $headerHtml)
            ->setOption('margin-top', '45mm')
            ->setTemporaryFolder($this->temporary_folder);
          $pdf->setPaper('a4', 'landscape');
          return $pdf->download('TiempoProcesoArchivosICOM'.$fecha_actual_flg.'.'.$this->extension_pdf);

          break;
        case 'XLS': //construccion excel
          Excel::create("TiempoProcesoArchivosICOM".$fecha_actual_flg, function ($excel) use ($param) {
           $excel->setTitle("TiempoProcesoArchivosICOM");
           $excel->sheet("TiempoProcesoArchivosICOM", function ($sheet) use ($param) {
           $sheet->loadView('reportes.transaccional.tiempo_proceso_archivo.re_tp_02_ex_excel')->with('param',
           $param);
             });
           })->download($this->extension_excel);
           return back();

	        break;
        default:
          break;
      }
    }

    public function reporteTiempoProcesoArchivoICOMA($cod_banco,$fecha,$tipoReporte) {
      //constantes
      $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
      $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

      //moneda activa
      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
       ->where('t_monedas.estatus','ACTIVO')
       ->first();

       $cod_banco = $cod_banco == null ? '*' : $cod_banco;
       $fecha = $fecha == null ? ParametroSistema::find(02)->valor : $fecha;

       $query = "SELECT EPSACH.sp_reportes_tiempo_proceso_archivos_icoma(:cod_banco,:fecha) FROM dual";
       $arrayVar = array('cod_banco' => $cod_banco,
                         'fecha' => $fecha );
       $Tiempo_archivo_icoma = collect(DB::select(DB::raw($query),$arrayVar));

       $fecha = Date::createFromFormat('Ymd', $fecha)->format('d/m/Y');
       $param = array(
          'TiempoProceso' => $Tiempo_archivo_icoma,
          'fecha' => $fecha,
          'fecha_actual' => $fecha_actual_slh,
          'monedas' => $monedas
      );

      //tiempo limite para generacion de reporte
      set_time_limit(120);

      //toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construcción PDF
  	      $headerHtml = \View::make('reportes.transaccional.tiempo_proceso_archivo.re_tp_03_pd_pdfheader', $param)
            ->render();
          //$footerHtml = view()->make('pdf.footer')->render();
          $pdf = SPDF::loadView('reportes.transaccional.tiempo_proceso_archivo.re_tp_03_pd_pdfbody', $param)
            ->setOption('header-html', $headerHtml)
            ->setOption('margin-top', '45mm')
            ->setTemporaryFolder($this->temporary_folder);
          $pdf->setPaper('a4', 'landscape');
          return $pdf->download('TiempoProcesoArchivosICOMA'.$fecha_actual_flg.'.'.$this->extension_pdf);

          break;
        case 'XLS': //construccion excel
          Excel::create("TiempoProcesoArchivosICOMA".$fecha_actual_flg, function ($excel) use ($param) {
           $excel->setTitle("TiempoProcesoArchivosICOMA");
           $excel->sheet("TiempoProcesoArchivosICOMA", function ($sheet) use ($param) {
           $sheet->loadView('reportes.transaccional.tiempo_proceso_archivo.re_tp_02_ex_excel')->with('param',
           $param);
             });
           })->download($this->extension_excel);
           return back();

	        break;
        default:
          break;
      }
    }
}
