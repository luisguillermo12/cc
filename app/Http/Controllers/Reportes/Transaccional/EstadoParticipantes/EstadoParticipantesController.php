<?php

namespace App\Http\Controllers\Reportes\Transaccional\EstadoParticipantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Database\Connection;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Configuracion\Banco\Banco;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use SPDF;
use Config;

class EstadoParticipantesController extends Controller
{

    protected $extension_excel;
		protected $extension_pdf;
		protected $temporary_folder;

		public function __construct()
		{
				$this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
				$this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
				$this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
		}

    public function getIndex(Request $request)
    {
        $estatus = [
            '*' => 'Seleccione',
            'ACTIVO' => 'ACTIVO',
            'INACTIVO' => 'INACTIVO',
            'EXCLUIDO' => 'EXCLUIDO',
            'SUSPENDIDO' => 'SUSPENDIDO',
            'FUSIONADO' => 'FUSIONADO',
            'ABSORBIDO' => 'ABSORBIDO'
        ];

        $estado = $request->get('estatus') != null ? $request->get('estatus') : '*';
        $banco = $request->get('banco') != null ? $request->get('banco') : '*';

        //Listado de participantes
        $lista_bancos = Banco::buscarTodos(['Seleccione', '*']);

        $query = "SELECT EPSACH.sp_reporte_participantes(:estado,:banco) FROM dual";
      	$arrayVar = array('estado' => $estado,
      				'banco' => $banco
             );
      	$EstatusParticipante = DB::select(DB::raw($query),$arrayVar);

        $totalparticipantes = (object) [
            'total' => Banco::count(),
            'activo' => Banco::where('estatus', '=', 'ACTIVO')->count(),
            'inactivo' => Banco::where('estatus', '=', 'INACTIVO')->count(),
            'excluido' => Banco::where('estatus', '=', 'EXCLUIDO')->count(),
            'suspendido' => Banco::where('estatus', '=', 'SUSPENDIDO')->count(),
            'fusionado' => Banco::where('estatus', '=', 'FUSIONADO')->count(),
            'absorbido' => Banco::where('estatus', '=', 'ABSORBIDO')->count(),
        ];

        return view('reportes.transaccional.estado_participantes.re_ep_01_i_estadoparticipantes')
        ->with('totalparticipantes', $totalparticipantes)
        ->with('EstatusParticipante', $EstatusParticipante)
        ->with('estado', $estado)
        ->with('banco', $banco)
        ->with('lista_bancos', $lista_bancos)
        ->with('estatus', $estatus);
    }

  public function ReporteEstadoParticipantes($estado,$banco,$tipoReporte) {
    //constantes
    $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
    $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

    $estado = $estado != null ? $estado : '*';
    $banco = $banco != null ? $banco : '*';

    $query = "SELECT EPSACH.sp_reporte_participantes(:estado,:banco) FROM dual";
    $arrayVar = array('estado' => $estado,
          'banco' => $banco
         );
    $EstatusParticipante = DB::select(DB::raw($query),$arrayVar);

    $param = array(
        'EstatusParticipante' => $EstatusParticipante,
        'fecha' => Carbon::now()->format('d/m/Y'),
        'fecha_actual' => $fecha_actual_slh
    );

    //tiempo limite para generacion de reporte
    set_time_limit(120);

    //toma en cuenta el tipo de reporte seleccionado
    switch ($tipoReporte) {
      case 'PDF': //construcción PDF
        $headerHtml = \View::make('reportes.transaccional.estado_participantes.re_ep_03_pd_pdfheader', $param)
          ->render();
        //$footerHtml = view()->make('pdf.footer')->render();
        $pdf = SPDF::loadView('reportes.transaccional.estado_participantes.re_ep_03_pd_pdfbody', $param)
          ->setOption('header-html', $headerHtml)
          ->setOption('margin-top', '50mm')
          ->setTemporaryFolder($this->temporary_folder);
        $pdf->setPaper('a4', 'landscape');
        return $pdf->download('EstatusParticipante'.$fecha_actual_flg.'.'.$this->extension_pdf);

        break;
      case 'XLS': //construccion excel
        Excel::create("EstatusParticipante".$fecha_actual_flg, function ($excel) use ($param) {
        $excel->setTitle("EstatusParticipante");
        $excel->sheet("Hoja1", function ($sheet) use ($param) {
        $sheet->loadView('reportes.transaccional.estado_participantes.re_ep_02_ex_excel')->with('param',
        $param);
          });
        })->download($this->extension_excel);
        return back();

        break;
      default:
        break;
    }

    }

}
