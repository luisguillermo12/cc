<?php

namespace App\Http\Controllers\Reportes\Transaccional\IntercambioImagenesBilateral;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reportes\Transaccional\IntercambioImagenesBilateral\ImagenesIntercambiadas;

// use App\Models\Monitoreo\TransferenciaArchivo;
// use App\Models\Monitoreo\FlujoInfo;
// use App\Models\Monitoreo\FlujoLote;
// use App\Models\Reportes\RangoHora;
// use App\Models\Cobranza\ParticipacionBanco;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\Banco\Banco;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\Moneda\Moneda;
use DB;
use Carbon\Carbon;
use Date;
use Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use SPDF;
use Config;

class IntercambioImagenesBilateralController extends Controller
{
    protected $extension_excel;
		protected $extension_pdf;
		protected $temporary_folder;

		public function __construct()
		{
				$this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
				$this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
				$this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
		}

    public function getIndex(Request $request)
    {

    $busqueda = $request->get('busqueda');
    $page = $request->get('page');
    $desde = $request->get('desde') != null ? $request->get('desde') : ParametroSistema::find(02)->valor;
    $hasta = $request->get('hasta') != null ? $request->get('hasta') : ParametroSistema::find(02)->valor;
    $cod_banco_emisor = $request->get('cod_banco_emisor') != null ? $request->get('cod_banco_emisor') : '*';
    $cod_banco_receptor = $request->get('cod_banco_receptor') != null ? $request->get('cod_banco_receptor') : '*';

    $bancos = Banco::buscarTodos(['Seleccione', '*']);

    //moneda activa
    $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
     ->where('t_monedas.estatus','ACTIVO')
     ->first();

    if(!empty($busqueda)){

      $this->validate($request, [
        'validacion_fechas' => 'fecha_mayor_que:' .
          Date::createFromFormat('Ymd', $desde)->format('Y-m-d') . ',' .
          Date::createFromFormat('Ymd', $hasta)->format('Y-m-d')
      ]);

    $img_intercambiadas= ImagenesIntercambiadas::
      where(function($query) use ($cod_banco_emisor, $cod_banco_receptor) {
          if ($cod_banco_emisor != '*') {
              $query->where('cod_banco_emisor',$cod_banco_emisor);
            };

          if ($cod_banco_receptor != '*') {
              $query->where('cod_banco_pagador',$cod_banco_receptor);
            };
    })
    ->get()
    ->sortBy('cod_banco_pagador')
    ->sortBy('cod_banco_emisor');

    //se calculan los totales previamente a la paginacion del objeto
    $total_operaciones = [];
    $total_operaciones[0] = $img_intercambiadas->sum('archivos_img_env');
    $total_operaciones[1] = $img_intercambiadas->sum('cant_operaciones_env');
    $total_operaciones[2] = $img_intercambiadas->sum('cant_img_env');
    $total_operaciones[3] = $img_intercambiadas->sum('cant_img_concilia_env');
    $total_operaciones[4] = $img_intercambiadas->sum('archivos_img_rec');
    $total_operaciones[5] = $img_intercambiadas->sum('bilat_cant_operaciones_rec');
    $total_operaciones[6] = $img_intercambiadas->sum('cant_img_rec');
    $total_operaciones[7] = $img_intercambiadas->sum('cant_img_concilia_rec');

    $img_intercambiadas = $this->arrayPaginator($img_intercambiadas->toArray(), $request);

    }else{
      $img_intercambiadas = null;
      $total_operaciones = null;
    }

    return view('reportes.transaccional.intercambio_imagenes_bilateral.re_ib_01_i_imagenesbilateral')
    ->with('img_intercambiadas', $img_intercambiadas)
    ->with('cod_banco_emisor',$cod_banco_emisor)
    ->with('cod_banco_receptor',$cod_banco_receptor)
    ->with('bancos',$bancos)
    ->with('desde', $desde)
    ->with('hasta', $hasta)
    ->with('page', $page)
    ->with('monedas', $monedas)
    ->with('total_operaciones', $total_operaciones)
    ->with('busqueda', $busqueda);
    }

    public function reporteIntercambioImagenesBilateral($cod_banco_emisor, $cod_banco_receptor, $desde , $hasta, $tipoReporte){
      //constantes
      $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
      $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

      //moneda activa
      $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
       ->where('t_monedas.estatus','ACTIVO')
       ->first();

      $desde = $desde != null ? $desde : ParametroSistema::find(02)->valor;
      $hasta = $hasta != null ? $hasta : ParametroSistema::find(02)->valor;
      $cod_banco_emisor = $cod_banco_emisor != null ? $cod_banco_emisor : '*';
      $cod_banco_receptor = $cod_banco_receptor != null ? $cod_banco_receptor : '*';

      $img_intercambiadas= ImagenesIntercambiadas::
        where(function($query) use ($cod_banco_emisor, $cod_banco_receptor) {
            if ($cod_banco_emisor != '*') {
                $query->where('cod_banco_emisor',$cod_banco_emisor);
              };

            if ($cod_banco_receptor != '*') {
                $query->where('cod_banco_pagador',$cod_banco_receptor);
              };
          })
         ->get()
         ->sortBy('cod_banco_pagador')
         ->sortBy('cod_banco_emisor');

      //asignacion de variables
      $desde = Date::createFromFormat('Ymd', $desde)->format('d/m/Y');
      $hasta = Date::createFromFormat('Ymd', $hasta)->format('d/m/Y');
      $param = array(
          'img_intercambiadas' => $img_intercambiadas,
          'desde' => $desde,
          'hasta' => $hasta,
          'fecha_actual' => $fecha_actual_slh,
          'monedas' => $monedas
      );

      //tiempo limite para generacion de reporte
      set_time_limit(120);

      //toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construcción PDF
      	  $headerHtml = \View::make('reportes.transaccional.intercambio_imagenes_bilateral.re_ib_03_pd_pdfheader', $param)
            ->render();
          //$footerHtml = view()->make('pdf.footer')->render();
          $pdf = SPDF::loadView('reportes.transaccional.intercambio_imagenes_bilateral.re_ib_03_pd_pdfbody', $param)
            ->setOption('header-html', $headerHtml)
            ->setOption('margin-top', '50mm')
            ->setTemporaryFolder($this->temporary_folder);
          $pdf->setPaper('a4', 'landscape');
          return $pdf->download('IntercambioImagenesBilateral'.$fecha_actual_flg.'.'.$this->extension_pdf);

          break;
        case 'XLS': //construccion excel
          Excel::create("IntercambioImagenesBilateral".$fecha_actual_flg, function ($excel) use ($param) {
          $excel->setTitle("IntercambioImagenesBilateral");
          $excel->sheet("IntercambioImagenesBilateral", function ($sheet) use ($param) {
          $sheet->loadView('reportes.transaccional.intercambio_imagenes_bilateral.re_ib_02_ex_excel')
          ->with('param', $param);
          $sheet->setColumnFormat(array(
              'D' => '#,##0',
              'E' => '#,##0',
              'F' => '#,##0',
              'G' => '#,##0',
              'H' => '#,##0',
              'I' => '#,##0',
              'J' => '#,##0',
              'K' => '#,##0'
          ));
          });
         })->download($this->extension_excel);
         return back();

      	  break;
        default:
          break;
      }
    }

     //Paginador
     public function arrayPaginator($array, $request)
     {
         //return count($array);
         $page = Input::get('page', 1);
         $perPage = 8;
         $offset = ($page * $perPage) - $perPage;
           return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
             ['path' => $request->url(), 'query' => $request->query()]);
     }
}
