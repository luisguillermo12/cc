<?php

namespace App\Http\Controllers\Reportes\Historico\ListadoGeneral;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use SPDF;
use Carbon\Carbon;
use Config;

class ListadoHistoricoController extends Controller
{

		protected $extension_excel;
		protected $extension_pdf;
		protected $caract_busqueda;
		protected $temporary_folder;

		public function __construct()
		{
			$this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
			$this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
			$this->caract_busqueda = Config::get('constants.reportes.lista_disponible_histo');
			$this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
		}

		public function listadoHistorico(){

       return view('reportes.historico.listado_general.re_lh_01_i_listadohistorico')->with('caract_busqueda',$this->caract_busqueda);
  	}

		public function reporteListadoHistorico($tipoReporte){
			//constantes
			$fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
			$fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

			$param = array(
	        'fecha_actual' => $fecha_actual_slh,
					'caract_busqueda' => $this->caract_busqueda
	    );

			//tiempo limite para generacion de reporte
			set_time_limit(120);

			//toma en cuenta el tipo de reporte seleccionado
      switch ($tipoReporte) {
        case 'PDF': //construcción PDF
		  		$headerHtml = \View::make('reportes.historico.listado_general.re_lh_03_pd_pdfheader', $param)
	          ->render();
	        //$footerHtml = view()->make('pdf.footer')->render();
	        $pdf = SPDF::loadView('reportes.historico.listado_general.re_lh_03_pd_pdfbody', $param)
	          ->setOption('header-html', $headerHtml)
	          ->setOption('margin-top', '40mm')
	          ->setTemporaryFolder($this->temporary_folder);
	        $pdf->setPaper('a4', 'portrait');
	        return $pdf->download('ListadoGeneralHistorico'.$fecha_actual_flg.'.'.$this->extension_pdf);

          break;
        case 'XLS': //construccion excel
					Excel::create("ListadoGeneralHistorico".$fecha_actual_flg, function ($excel) use ($param) {
	 	       $excel->setTitle("ListadoGeneralHistorico");
	 	       $excel->sheet("ListadoGeneralHistorico", function ($sheet) use ($param) {
	 	       $sheet->loadView('reportes.historico.listado_general.re_lh_02_ex_excel')->with('param',
           	$param);
	 	       });
	 	     	})->download($this->extension_excel);
 	  			return back();

	  			break;
        default:
          break;
      }

	  }
}
