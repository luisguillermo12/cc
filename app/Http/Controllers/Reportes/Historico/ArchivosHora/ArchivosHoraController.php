<?php
namespace App\Http\Controllers\Reportes\Historico\ArchivosHora;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Reportes\RangoHora;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\TipoArchivo\TipoArchivo;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\Moneda\Moneda;
use Session;
use DB;
use Illuminate\Database\Connection;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Input;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use SPDF;
use Config;
use App\Services\Helpers\HelperService;

class ArchivosHoraController extends Controller
{
  protected $extension_excel;
	protected $extension_pdf;
	protected $temporary_folder;
  protected $helperService;
  protected $rangosHora;

	public function __construct(HelperService $helperService)
	{
      $this->helperService = $helperService;
			$this->extension_excel = Config::get('constants.reportes.caract_extension_excel');
			$this->extension_pdf = Config::get('constants.reportes.caract_extension_pdf');
			$this->temporary_folder = Config::get('constants.reportes.carac_temporary_folder');
      $this->rangosHora = $this->helperService->generateHoras(60);
	}

  public function getIndex(Request $request)
  {
    //variables
    $cod_banco = $request->get('cod_banco');
    $cod_archivo = $request->get('cod_archivo');
    $cod_hora_desde = $request->get('cod_hora_desde');
    $cod_hora_hasta= $request->get('cod_hora_hasta');
    $busqueda = $request->get('busqueda');
    $page =  $request->get('page');

    //seleccion de fecha
    $fe_desde = ($request->get('FechaDesde') == null || $request->get('FechaDesde') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaDesde');
    $fe_desde = Date('Ymd', strtotime( $fe_desde ));
    $fe_hasta = ($request->get('FechaHasta') == null || $request->get('FechaHasta') == '') ? ParametroSistema::find(02)->valor : $request->get('FechaHasta');
    $fe_hasta = Date('Ymd', strtotime( $fe_hasta ));

    //lista horas desde
    $listaDesde = RangoHora::select('id', DB::raw("SUBSTR(nombre, 1,6) AS rango_horas"))
    ->pluck('rango_horas','id')
    ->prepend('Seleccione', '0');
    //lista horas hasta
    $listaHasta = RangoHora::select('id', DB::raw("SUBSTR(nombre, 9,12) AS rango_horas"))
    ->pluck('rango_horas','id')
    ->prepend('Seleccione', '0');
    //lista tipo de archivo
    $listaArchivo = TipoArchivo::select('codigo', DB::raw("nombre AS nombre_completo"))
    ->where('codigo','ICOM1')
    ->orWhere('codigo','ICOM2')
    ->pluck('nombre_completo','codigo')
    ->prepend('Seleccione', '*');

    //lista participantes
    $listaParticipante = Banco::buscarTodos(['Seleccione', '*']);

    //parametros para fechas de compensacion
    $fechas[1] = Date('Y-m-d', strtotime( '1970-01-01' ));
    $fechas[0] = Date('Y-m-d', strtotime( ParametroSistema::find(02)->valor ));

    //Arreglo con las fechas de bancarios y feriados
    $calendario = DB::table('t_fechas_calendario')
    ->select('fecha_validacion')
    ->whereIn('estatus',['CERRADO','FERIADO','BANCARIO'])
    ->get();
    foreach ($calendario as $calendario) {
      $feriados[] = Date('Y-m-d', strtotime( $calendario->fecha_validacion ));
    };

    //20180925. moneda activa
    $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
     ->where('t_monedas.estatus','ACTIVO')
     ->first();

    //horas para los graficos
    $desde = RangoHora::select('hora')
            ->where('id','=',$cod_hora_desde)
            ->first();
    $desde = empty($desde) ? '19' : $desde->hora;
    $desde = $desde.':00';
    $hasta = RangoHora::select('hora')
            ->where('id','=',$cod_hora_hasta)
            ->first();
    $hasta = empty($hasta) ? '18' : $hasta->hora;
    $hasta = $hasta.':59';
    $rango_seleccionado = $desde.' a '.$hasta;

    $rangoF_selec = Date::createFromFormat('Ymd', $fe_desde)->format('d/m/Y').' al '.Date::createFromFormat('Ymd', $fe_hasta)->format('d/m/Y');


    //si se realiza una busqueda desde el boton de buscar
    if(!empty($busqueda)){

      //desde no puede ser mayor que hasta
      $this->validate($request, [
        'validacion_fechas' => 'fecha_mayor_que:' .
          Date::createFromFormat('Ymd', $fe_desde)->format('Y-m-d') . ',' .
          Date::createFromFormat('Ymd', $fe_hasta)->format('Y-m-d')
      ]);

      if(($cod_hora_desde != 0) && ($cod_hora_hasta != 0)){
        $this->validate($request,[
          'cod_hora_desde' => 'mayor_que:'.$cod_hora_desde.','.$cod_hora_hasta
        ]);
      }else if(($cod_hora_desde == 0) && ($cod_hora_hasta == 0)){
        //se seleccionaron los filtros para todas las horas
      }else{
        //si ambas horas no son distintas de cero, o iguales ambas a cero, una de las dos no puede serlo
        $this->validate($request,[
          'cod_hora_desde' => 'no_cero:'.$cod_hora_desde.',Hora desde'
        ]);
        $this->validate($request,[
          'cod_hora_hasta' => 'no_cero:'.$cod_hora_hasta.',Hora hasta'
        ]);
      }
      //busqueda
      $tipoBusqueda = $cod_banco != '*' ? 1 : 0;
      $query = "SELECT EPSACH.sp_reportes_operaciones_archivos(:cod_banco,:cod_archivo,:fe_desde,:fe_hasta,:cod_hora_desde,:cod_hora_hasta,:tipoBusqueda) from dual";
      $arrayVar = array('cod_banco' => $cod_banco,
                'cod_archivo' => $cod_archivo,
                'fe_desde' => $fe_desde,
                'fe_hasta' => $fe_hasta,
                'cod_hora_desde' => $cod_hora_desde,
                'cod_hora_hasta' => $cod_hora_hasta,
                'tipoBusqueda' => $tipoBusqueda );
      $ARCHIVOS_INTERVALO = DB::select(DB::raw($query),$arrayVar);

      //2018/07/10. se calculan los totales
      $cant_archg = 0;
      $monto_totalg = 0;
      $ARCHIVOS_INTERVALO_NEW = null;
      for($i = 0; $i < count($ARCHIVOS_INTERVALO); $i++){
        if(($ARCHIVOS_INTERVALO[$i]->banco_nombre == null) && ($ARCHIVOS_INTERVALO[$i]->archivo_tipo == null)){
          $cant_archg = $cant_archg + $ARCHIVOS_INTERVALO[$i]->archivo_cantidad;
          $monto_totalg = $monto_totalg + $ARCHIVOS_INTERVALO[$i]->archivo_monto;
          if($cod_archivo == '*'){ //2018/08/07. si no se selecciono un tipo de archivo, se muestran los subtotales
            $ARCHIVOS_INTERVALO_NEW[$i] = clone $ARCHIVOS_INTERVALO[$i];
          } //si no, no se agregan al nuevo objeto
        }else{
          $ARCHIVOS_INTERVALO_NEW[$i] = clone $ARCHIVOS_INTERVALO[$i];
        }
      }
      //paginador
      if(!empty($ARCHIVOS_INTERVALO_NEW)){
        $ARCHIVOS_INTERVALO = $this->arrayPaginator($ARCHIVOS_INTERVALO_NEW, $request);
      }else {
        $ARCHIVOS_INTERVALO = $this->arrayPaginator($ARCHIVOS_INTERVALO, $request);
      }

      // TOTAL ARCHIVOS ICOM1 //
      $query = "SELECT EPSACH.SP_REPORTES_ARCHIVOS_INTERVALO_GENERAL(:cod_banco,:cod_archivo,:fe_desde,:fe_hasta,:cod_hora_desde,:cod_hora_hasta) from dual";
      $arrayVar = array('cod_banco' => $cod_banco,
                'cod_archivo' => 'ICOM1',
                'fe_desde' => $fe_desde,
                'fe_hasta' => $fe_hasta,
                'cod_hora_desde' => $cod_hora_desde,
                'cod_hora_hasta' => $cod_hora_hasta );
      $cuenta = DB::select(DB::raw($query),$arrayVar);
      $resultArray = json_decode(json_encode($cuenta), true);
      $ICOM1 = array_map('current', $resultArray);
      //objeto de grafico ICOM1
      $TotalIcom1=Collection::make($cuenta);
      $TotalIcom1=$TotalIcom1->sum('cantidad');
      $TotalIcom1=collect(['total_icom1'=> $TotalIcom1] );
      $TotalIcom1= [(object) $TotalIcom1->toArray()];
      // TOTAL ARCHIVOS ICOM2 //
      $query = "SELECT EPSACH.SP_REPORTES_ARCHIVOS_INTERVALO_GENERAL(:cod_banco,:cod_archivo,:fe_desde,:fe_hasta,:cod_hora_desde,:cod_hora_hasta) from dual";
      $arrayVar = array('cod_banco' => $cod_banco,
                'cod_archivo' => 'ICOM2',
                'fe_desde' => $fe_desde,
                'fe_hasta' => $fe_hasta,
                'cod_hora_desde' => $cod_hora_desde,
                'cod_hora_hasta' => $cod_hora_hasta );
      $cuenta = DB::select(DB::raw($query),$arrayVar);
      $resultArray = json_decode(json_encode($cuenta), true);
      $ICOM2 = array_map('current', $resultArray);
      //objeto de grafico ICOM2
      $TotalIcom2=Collection::make($cuenta);
      $TotalIcom2=$TotalIcom2->sum('cantidad');
      $TotalIcom2=collect(['total_icom2'=> $TotalIcom2] );
      $TotalIcom2= [(object) $TotalIcom2->toArray()];

    }else{
      $ARCHIVOS_INTERVALO = [new \stdClass()];
      $ICOM1 = null;
      $ICOM2 = null;
      $TotalIcom1 = null;
      $TotalIcom2 = null;
      $cant_archg = 0;
      $monto_totalg = 0;
    }

    //retorno de vista
    return view('reportes.historico.archivos_hora.re_ah_01_i_archivoshora')
    ->with('ARCHIVOS_INTERVALO',$ARCHIVOS_INTERVALO)
    ->with('cantidad_archivos',$cant_archg)
    ->with('monto_total_archivos',$monto_totalg)
    ->with('page',$page)
    ->with('monedas',$monedas)
    ->with('listaDesde',$listaDesde)
    ->with('listaHasta',$listaHasta)
    ->with('listaParticipante',$listaParticipante)
    ->with('cod_banco',$cod_banco)
    ->with('cod_hora_desde',$cod_hora_desde)
    ->with('cod_hora_hasta',$cod_hora_hasta)
    ->with('listaArchivo',$listaArchivo)
    ->with('cod_archivo',$cod_archivo)
    ->with('rangoF_selec',$rangoF_selec)
    ->with('fe_desde',$fe_desde)
    ->with('fe_hasta',$fe_hasta)
    ->with('fechas',$fechas)
    ->with('rango_seleccionado',$rango_seleccionado)
    ->with('feriados',$feriados)
    ->with('ICOM1',json_encode($ICOM1,JSON_NUMERIC_CHECK))
    ->with('TotalIcom1',$TotalIcom1)
    ->with('TotalIcom2',$TotalIcom2)
    ->with('busqueda',$busqueda)
    ->with('ICOM2',json_encode($ICOM2,JSON_NUMERIC_CHECK))
    ->with('rangos_hora',$this->rangosHora);
  }
  //Paginador
  public function arrayPaginator($array, $request)
  {
      //return count($array);
      $page = Input::get('page', 1);
      $perPage = 8;
      $offset = ($page * $perPage) - $perPage;
        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
          ['path' => $request->url(), 'query' => $request->query()]);
  }

  public function reporteArchivosHora($cod_banco,$cod_archivo,$fe_desde,$fe_hasta,$desde,$hasta,$tipoReporte,$tipoBusqueda){
    //constantes
    $fecha_actual_slh = Carbon::now()->format('d/m/Y H:i:s a'); //fecha DD/MM/YYYY
    $fecha_actual_flg = Carbon::now()->format('YmdHis'); //fecha YYYYMMDDHHMMSS

    //20180925. moneda activa
    $monedas = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
     ->where('t_monedas.estatus','ACTIVO')
     ->first();

    //seleccion de fecha
    $fe_desde = ($fe_desde == null || $fe_desde == '') ? ParametroSistema::find(02)->valor : $fe_desde;
    $fe_desde = Date('Ymd', strtotime( $fe_desde ));
    $fe_hasta = ($fe_hasta == null || $fe_hasta == '') ? ParametroSistema::find(02)->valor : $fe_hasta;
    $fe_hasta = Date('Ymd', strtotime( $fe_hasta ));

    $query = "SELECT EPSACH.sp_reportes_operaciones_archivos(:cod_banco,:cod_archivo,:fe_desde,:fe_hasta,:cod_hora_desde,:cod_hora_hasta,:tipoBusqueda) from dual";
    $arrayVar = array('cod_banco' => $cod_banco,
                'cod_archivo' => $cod_archivo,
                'fe_desde' => $fe_desde,
                'fe_hasta' => $fe_hasta,
                'cod_hora_desde' => $desde,
                'cod_hora_hasta' => $hasta,
                'tipoBusqueda' => $tipoBusqueda );
    $ARCHIVOS_INTERVALO = DB::select(DB::raw($query),$arrayVar);
    //2018/07/10. se calculan los totales
    $cant_archg = 0; //por total
    $monto_totalg = 0; //por total
    $cant_archinst = 0; //por inst
    $monto_totalinst = 0; //por inst
    $institucion = '';
    for($i = 0; $i < count($ARCHIVOS_INTERVALO); $i++){
      if(($ARCHIVOS_INTERVALO[$i]->banco_nombre == null) && ($ARCHIVOS_INTERVALO[$i]->archivo_tipo == null)){
        $cant_archg = $cant_archg + $ARCHIVOS_INTERVALO[$i]->archivo_cantidad;
        $monto_totalg = $monto_totalg + $ARCHIVOS_INTERVALO[$i]->archivo_monto;
        $cant_archinst = $cant_archinst + $ARCHIVOS_INTERVALO[$i]->archivo_cantidad;
        $monto_totalinst = $monto_totalinst + $ARCHIVOS_INTERVALO[$i]->archivo_monto;
        if($cod_archivo == '*'){ //2018/08/07. si no se selecciono un tipo de archivo, se muestran los subtotales
          $ARCHIVOS_INTERVALO_NEW[$i] = clone $ARCHIVOS_INTERVALO[$i];
        }else{ //si no, no se agregan al nuevo objeto
          $posElementoSig = ($i+1)>=count($ARCHIVOS_INTERVALO) ? $i : ($i+1);
          $posElementoAnt = ($i-1);
          if($institucion != $ARCHIVOS_INTERVALO[ $posElementoSig ]->banco_nombre){ //si la institucion que viene despues del subtotal es la misma, se continua, si no, se muestra el subtotal para la institucion
            if($ARCHIVOS_INTERVALO[ $posElementoSig ]->rango_id != $ARCHIVOS_INTERVALO[ $posElementoAnt ]->rango_id){ //si el rango de horas que sigue al subtotal es distinto, significa que se cambia a otro y se muestra el total, si no, se sigue en el mismo rango con otra inst
              $ARCHIVOS_INTERVALO_NEW[$i] = clone $ARCHIVOS_INTERVALO[$i];
              $ARCHIVOS_INTERVALO_NEW[$i]->archivo_cantidad = $cant_archinst;
              $ARCHIVOS_INTERVALO_NEW[$i]->archivo_monto = $monto_totalinst;
              $cant_archinst = 0;
              $monto_totalinst = 0;
            }
          }
        }
      }else{
        $ARCHIVOS_INTERVALO_NEW[$i] = clone $ARCHIVOS_INTERVALO[$i];
        $institucion = $ARCHIVOS_INTERVALO[$i]->banco_nombre;
      }
    }
    //2018/07/12. asignacion de variables
    $fe_desde = Date::createFromFormat('Ymd', $fe_desde)->format('d/m/Y');
    $fe_hasta = Date::createFromFormat('Ymd', $fe_hasta)->format('d/m/Y');
    $param = array(
        'ARCHIVOS_INTERVALO' => $ARCHIVOS_INTERVALO_NEW,
        'fe_desde' => $fe_desde,
        'fe_hasta' => $fe_hasta,
        'cantidad_archivos' => $cant_archg,
        'monto_total_archivos' => $monto_totalg,
        'fecha_actual' => $fecha_actual_slh,
        'monedas' => $monedas
    );

    //tiempo limite para generacion de reporte
    set_time_limit(120);

    //2018/07/12. toma en cuenta el tipo de reporte seleccionado
    switch ($tipoReporte) {
      case 'PDF': //2018/07/12. construccion pdf
        $headerHtml = \View::make('reportes.historico.archivos_hora.re_ah_03_pd_pdfheader', $param)
          ->render();
        $pdf = SPDF::loadView('reportes.historico.archivos_hora.re_ah_03_pd_pdfbody', $param)
          ->setOption('header-html', $headerHtml)
          ->setOption('margin-top', '50mm')
          ->setTemporaryFolder($this->temporary_folder);
        $pdf->setPaper('a4', 'portrait');
        return $pdf->download('ArchivosPorHora'.$fecha_actual_flg.'.'.$this->extension_pdf);

        break;
      case 'XLS': //2018/07/12. construccion excel
        Excel::create("ArchivosPorHora".$fecha_actual_flg, function ($excel) use ($param) {
          $excel->setTitle("ArchivosPorHora");
          $excel->sheet("ArchivosPorHora", function ($sheet) use ($param) {
            $sheet->loadView('reportes.historico.archivos_hora.re_ah_02_ex_excel')
            ->with('param', $param);
            $sheet->getStyle('A9')->getAlignment()->setWrapText(true);
            $sheet->getStyle('B9')->getAlignment()->setWrapText(true);
            $sheet->getStyle('C9')->getAlignment()->setWrapText(true);
            $sheet->getStyle('D9')->getAlignment()->setWrapText(true);
            $sheet->getStyle('E9')->getAlignment()->setWrapText(true);
            $sheet->setColumnFormat(array(
                'D' => '#,##0',
                'E' => '#,##0.00'
            ));
          });
        })->download($this->extension_excel);
        return back();

        break;
      default:
        break;
    }

  }


}
