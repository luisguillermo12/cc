<?php

namespace App\Http\Controllers\Log\GUI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Log\Log;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;

class LogsGUIController extends Controller
{
    public function inicio(Request $request)
    {
        $logger = new Log();

        /* Cantidad de items por página */
        $cant = 15;

        /* Pagina actual */
        $pagina = $request->get('page', 1);

        /* Listado del total de mensajes del log */
        $resultado = $logger->mostrar('inicio');

        /* Listado filtrado para la página actual */
        $paginado = $resultado->forPage($pagina, $cant);

        /* Se establece en path, la ruta de la vista */
        $opciones = [
            'path' => '/GestionLogs/GUI/Inicio',
        ];

        /* Se establecen las etiquetas para cada nivel */
        $etiquetas = [
            'INFO' => 'info',
            'ALERT' => 'warning',
            'WARNING' => 'danger',
        ];

        /* Creamos el paginado para utilizar los botones de navegación */
        $logs = new LengthAwarePaginator($paginado, count($resultado), $cant, $pagina , $opciones);

        return view('log.gui.lg01_i_inicio')
        ->with('logs', $logs)
        ->with('etiquetas', $etiquetas);
    }

    public function descargarInicio() {

        $fecha = Carbon::now()->format('Y-m-d');

        $ruta =  storage_path("logs".DIRECTORY_SEPARATOR."log-inicio-{$fecha}.log");

        if (file_exists($ruta)) {
            return response()->download($ruta);
        }
    }

     public function descargarCierre() {

        $fecha = Carbon::now()->format('Y-m-d');

        $ruta =  storage_path("logs".DIRECTORY_SEPARATOR."log-cierre-{$fecha}.log");

        if (file_exists($ruta)) {
            return response()->download($ruta);
        }
    }

    public function cierre(Request $request)
    {
        $logger = new Log();

        /* Cantidad de items por página */
        $cant = 15;

        /* Pagina actual */
        $pagina = $request->get('page', 1);

        /* Listado del total de mensajes del log */
        $resultado = $logger->mostrar('cierre');

        /* Listado filtrado para la página actual */
        $paginado = $resultado->forPage($pagina, $cant);

        /* Se establece en path, la ruta de la vista */
        $opciones = [
            'path' => '/GestionLogs/GUI/Cierre',
        ];

        /* Se establecen las etiquetas para cada nivel */
        $etiquetas = [
            'INFO' => 'info',
            'ALERT' => 'warning',
            'WARNING' => 'danger',
        ];

        /* Creamos el paginado para utilizar los botones de navegación */
        $logs = new LengthAwarePaginator($paginado, count($resultado), $cant, $pagina , $opciones);

        return view('log.gui.lg02_i_cierre')
        ->with('logs', $logs)
        ->with('etiquetas', $etiquetas);
    }
}
