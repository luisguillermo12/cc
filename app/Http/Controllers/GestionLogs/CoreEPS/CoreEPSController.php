<?php

namespace App\Http\Controllers\Log\CoreEPS;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Log\Log;
use Carbon\Carbon;
use Storage;

class CoreEPSController extends Controller
{
    public function getIndex()
    {
        $log = new Log();

        $contenido = $log->lineas('eps');

        return view('log.core_eps.lg03_i_core_eps')
        ->with('contenido', $contenido);
    }

    public function descargar() {

        $fecha = Carbon::now()->format('Y-m-d');

        $ruta =  storage_path("logs".DIRECTORY_SEPARATOR."log-eps-{$fecha}.log");

        if (file_exists($ruta)) {
            return response()->download($ruta);
        }
    }
}
