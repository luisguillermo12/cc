<?php

namespace App\Http\Controllers\Inicio;

use App\Models\Configuracion\SesionCompensacion\IntervaloLiquidacion;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\Configuracion\SesionCompensacion\Periodo;
use App\Models\Access\User\Traits\UserPasswordChanges;
use App\Models\Configuracion\MedioPago\TipoOperacion;
use App\Models\Configuracion\Banco\Banco;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Input;
use DB;

class InicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        /* pendiente por validar
        $clave_v = Auth::user()->claveVencida();
        $dias_v = Auth::user()->ProrrogaclaveVencida();
        $dias_restantes = 10 - $dias_v;
        */
        $bancosAnsorbidos = Banco::where('estatus','=','FUSIONADO')
        ->where('fecha_absorsion','<>',null)
        ->orderBy('codigo', 'asc')
        ->get();
    
        $operaciones = TipoOperacion::get();

        $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();
    
        //dd($intervalos_liquidacion );

        for($x = 0; $x < count($bancosAnsorbidos); $x++) {
            $bancosAnsorbidos[$x]->resta_fecha = Carbon::now()->diffInDays(Carbon::parse($bancosAnsorbidos[$x]->fecha_absorsion), false);
        }

      return view('inicio.in_01_i_inicio')
      ->with('operaciones', $operaciones)
      ->with('intervalos_liquidacion', $intervalos_liquidacion);
    }

    public function creditod(){
        
      $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();

      //busca los tipos de operacion 
      $operacion = TipoOperacion::where('codigo', '010')->first();

      $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

      $d_1 =$parametros_sistema[1]->valor;

      //dia de inicio
      $fecha_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
      //dia de cierre
      $fecha_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      //fecha de compensacion dia + hora de inicio (credito directo)) 
      $hora_D_ini = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_inicio)->format('H:i:s');
      $fecha_cheques_i = $fecha_D.$hora_D_ini;

      //fecha de compensacion dia + hora de fin (credito directo)) 
      $hora_D_fin = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_fin)->format('H:i:s');
      $fecha_cheques_f = $fecha_D_1.$hora_D_fin;

      return response()->json(array($fecha_cheques_i,$fecha_cheques_f));
    }

    public function creditodd(){
        
      $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();

      //graficas fecha
      $operacion = TipoOperacion::where('codigo', '110')->first();

      $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
          ->whereIn('id', [2,39])
          ->get();

      $d_1 =$parametros_sistema[1]->valor;

      //dia de inicio
      $fecha_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
      //dia de cierre
      $fecha_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      //fecha de compensacion dia + hora de inicio (credito directo)) 
      $hora_D_ini = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_inicio)->format('H:i:s');
      $fecha_cheques_i = $fecha_D.$hora_D_ini;

      //fecha de compensacion dia + hora de fin (credito directo)) 
      $hora_D_fin = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_fin)->format('H:i:s');
      $fecha_cheques_f = $fecha_D_1.$hora_D_fin;

      return response()->json(array($fecha_cheques_i,$fecha_cheques_f));
    }

    public function cheques(){

      //graficas fecha
      $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();
    
      $operacion = TipoOperacion::where('codigo', '030')->first();

      //grficas fecha

      $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
          ->whereIn('id', [2,39])
          ->get();

      $d_1 =$parametros_sistema[1]->valor;

      //dia de inicio
      $fecha_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
      //dia de cierre
      $fecha_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      //fecha de compensacion dia + hora de inicio (cheques)) 
      $hora_D_ini = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_inicio)->format('H:i:s');
      $fecha_cheques_i = $fecha_D.$hora_D_ini;

      //fecha de compensacion dia + hora de fin (cheques)) 
      $hora_D_fin = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_fin)->format('H:i:s');
      $fecha_cheques_f = $fecha_D_1.$hora_D_fin;

      return response()->json(array($fecha_cheques_i,$fecha_cheques_f));
    }

    public function chequesd(){

      //grficas fecha
      $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();
  
      $operacion = TipoOperacion::where('codigo', '130')->first();

      $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

      $d_1 =$parametros_sistema[1]->valor;

      //dia de inicio
      $fecha_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
      //dia de cierre
      $fecha_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      //fecha de compensacion dia + hora de inicio (cheques)) 
      $hora_D_ini = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_inicio)->format('H:i:s');
      $fecha_cheques_i = $fecha_D.$hora_D_ini;

      //fecha de compensacion dia + hora de fin (cheques)) 
      $hora_D_fin = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_fin)->format('H:i:s');
      $fecha_cheques_f = $fecha_D_1.$hora_D_fin;

      return response()->json(array($fecha_cheques_i,$fecha_cheques_f));
    }

    public function domiciliaciones(){

      //graficas fecha
      $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();
      
      $operacion = TipoOperacion::where('codigo', '020')->first();

      $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

      $d_1 =$parametros_sistema[1]->valor;

      //dia de inicio
      $fecha_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
      //dia de cierre
      $fecha_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      //fecha de compensacion dia + hora de inicio (domiciliaciones)) 
      $hora_D_ini = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_inicio)->format('H:i:s');
      $fecha_cheques_i = $fecha_D.$hora_D_ini;

      //fecha de compensacion dia + hora de fin (domiciliaciones)) 
      $hora_D_fin = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_fin)->format('H:i:s');
      $fecha_cheques_f = $fecha_D_1.$hora_D_fin;

      return response()->json(array($fecha_cheques_i,$fecha_cheques_f));
    }

    public function domiciliacionesd(){

      //graficas fecha
      $intervalos_liquidacion = IntervaloLiquidacion::
        select('t_intervalos_liquidacion.*','t_tipos_operaciones.nombre as tipo_operacion','t_productos.nombre as producto','t_periodos.num_franja as p_num_franja')
        ->join('t_periodos','t_periodos.id','=','t_intervalos_liquidacion.periodo_id')
        ->join('t_tipos_operaciones','t_tipos_operaciones.id','=','t_intervalos_liquidacion.tipo_operacion_id')
        ->join('t_productos','t_productos.id','=','t_tipos_operaciones.producto_id')
        ->orderBy('t_intervalos_liquidacion.id', 'asc')
        ->get();
  
      $operacion = TipoOperacion::where('codigo', '120')->first();

      $parametros_sistema = ParametroSistema::orderBy('id', 'asc')
        ->whereIn('id', [2,39])
        ->get();

      $d_1 =$parametros_sistema[1]->valor;

      //dia de inicio
      $fecha_D = Carbon::parse($parametros_sistema[0]->valor)->format('d-m-Y ');
      //dia de cierre
      $fecha_D_1 = Carbon::parse($parametros_sistema[1]->valor)->format('d-m-Y ');

      //fecha de compensacion dia + hora de inicio (domiciliaciones)) 
      $hora_D_ini = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_inicio)->format('H:i:s');
      $fecha_cheques_i = $fecha_D.$hora_D_ini;

      //fecha de compensacion dia + hora de fin (domiciliaciones)) 
      $hora_D_fin = Carbon::parse($intervalos_liquidacion->where('tipo_operacion_id', (string)$operacion->id)->first()->hora_fin)->format('H:i:s');
      $fecha_cheques_f = $fecha_D_1.$hora_D_fin;

      return response()->json(array($fecha_cheques_i,$fecha_cheques_f));
    }
}
