<?php

namespace App\Http\Controllers\ProcesosEspeciales\ExclusionParticipante; 
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use App\Models\ProcesosEspeciales\ProcesosEspeciales;
use App\Models\ProcesosEspeciales\HRegistroProcesosEspeciales;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;
use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\ProcesosEspeciales\v_procesos_especiales_exclusion;
use Carbon\Carbon;
use Auth;
use DB;
use App\Services\Email\Correo;
use Alert;

class ExclusionParticipanteController extends Controller
{
  public function getIndex(Request $request)
  {
      $cerrado =  ParametroSistema::find(34);
      $cerrado =  (bool) $cerrado->valor;

      $bancos = Banco::where('estatus', 'ACTIVO')
      ->select('codigo', 'nombre')
      ->get()
      ->pluck('nombre_completo', 'codigo');

      $fecha = Carbon::now()->format('d/m/Y h:i:s');
   
      $reprocesos = v_procesos_especiales_exclusion::select('v_procesos_especiales_exclusion.*','t_bancos.nombre')
      ->join('t_bancos','t_bancos.codigo','=','v_procesos_especiales_exclusion.cod_banco')
      ->where('v_procesos_especiales_exclusion.id_procesos_especiales', 3)
      ->orderBy('v_procesos_especiales_exclusion.id', 'DESC')
      ->get();

       $reprocesosh = HRegistroProcesosEspeciales::select('t_histo_registro_procesos_especiales.*')
     // ->join('epsach.t_bancos','epsach.t_bancos.codigo','=',' t_histo_registro_procesos_especiales.cod_banco')
      ->where('t_histo_registro_procesos_especiales.id_procesos_especiales', 3)
      ->where('t_histo_registro_procesos_especiales.estatus', 'APROBADO')
       ->orwhere('t_histo_registro_procesos_especiales.estatus', 'DENEGADO')
      ->orderBy('t_histo_registro_procesos_especiales.id', 'DESC')
      ->get();

      foreach($reprocesosh as $registro){
       $registro->nombre = Banco::where('codigo', $registro->cod_banco)->first()->nombre;
      }
      

      $registrados=0;

      foreach ($reprocesos as $registro) {
        if ($registro->estatus=='REGISTRADO') {
          $registrados=$registrados+1;
        }
      }

      return view('procesos_especiales.exclusionparticipante.pr_ex_01_i_exclusion')
      ->with('bancos', $bancos)
      ->with('cerrado', $cerrado)
      ->with('reprocesos', $reprocesos)
      ->with('reprocesosh', $reprocesosh)
      ->with('registrados', $registrados) 
      ->with('fecha', $fecha);
  }

  public function store(Request $request)
  {
    if ($request->get('banco') == null ){

      Alert::warning('!','No ha seleccionado bancos para su exclusion');

      return redirect()->back();

    } else { 
      $bancos_f=$request->get('banco');
      $observacion = $request->get('causa');
      $fecha = Carbon::now()->format('Ymdhis');
      $proceso_especial = ProcesosEspeciales::find(3);


      foreach ($bancos_f as $banco_r) {
      
        $banco = Banco::where('codigo', $banco_r)->first();      
        $Proceso = new RegistroProcesosEspeciales();
        $Proceso->cod_operacion = rand(1, 99999999);
        $Proceso->id_procesos_especiales = $proceso_especial->id;
        $Proceso->nombre_proceso = $proceso_especial->nombre;
        $Proceso->user_id = Auth::user()->id;
        $Proceso->nombre_usuario = Auth::user()->name;
        $Proceso->estatus = 'REGISTRADO';
        $Proceso->ip_conexion = $request->ip();
        $Proceso->fecha = $fecha;
        $Proceso->cod_banco = $banco->codigo;
        $Proceso->descripcion = $fecha;
        $Proceso->observaciones = $observacion;
        $Proceso->cargar = '1';
        $Proceso->save();

      }

      Alert::success('!','Registro de exclusión satisfactorio');

      Correo::notificacion('E012');
      return redirect()->back();
    }
  }

  public function aprobar(Request $request)
  {
    $reprocesos = v_procesos_especiales_exclusion::
    where('id_procesos_especiales', 3)
    ->where('estatus', 'REGISTRADO')
    ->orderBy('id', 'DESC')
    ->get();

    $proceso_especial = ProcesosEspeciales::find(3);
    $fecha = Carbon::now()->format('Ymdhis');

       
    foreach ($reprocesos as $reproceso) {

      $banco = Banco::where('codigo', $reproceso->cod_banco)->first();      

        $Proceso = new RegistroProcesosEspeciales();
        $Proceso->cod_operacion = $reproceso->cod_operacion;
        $Proceso->id_procesos_especiales = $proceso_especial->id;
        $Proceso->nombre_proceso = $proceso_especial->nombre;
        $Proceso->user_id = Auth::user()->id;
        $Proceso->nombre_usuario = Auth::user()->name;
        $Proceso->estatus = 'APROBADO';
        $Proceso->ip_conexion = $request->ip();
        $Proceso->fecha = $fecha;
        $Proceso->cod_banco = $banco->codigo;
        $Proceso->descripcion = $reproceso->descripcion;
        $Proceso->observaciones = $reproceso->observaciones;
        $Proceso->cargar = '1';
        $Proceso->validar ='1';
        $Proceso->save();
       
      $banco->estatus = 'EXCLUIDO';
      $banco->deleted_at = Carbon::now();
      $banco->save();
    }
              
    DB::beginTransaction(); 
          
    try{
                
      $query = "select EPSACH.sp_procesar_oexcl() from dual";
      DB::statement($query);

      notify()->flash("Participante excluido satisfactoriamente", 'success', [
        'timer' => 3000,
        'text' => ''
      ]);
      Correo::notificacion('E013');
      return redirect()->back();

    } catch (\Exception $e) {

        Alert::warning('!','EXCLUSION NO EJECUTADA CORRECTAMENTE');

        return redirect()->back();
    }
  }

  public function denegar(Request $request)
  {
     
    $reprocesos = v_procesos_especiales_exclusion::where('id_procesos_especiales', 3)
      ->where('estatus', 'REGISTRADO')
      ->orderBy('id', 'DESC')
      ->get();

    $proceso_especial = ProcesosEspeciales::find(3);
    $fecha = Carbon::now()->format('Ymdhis');

    foreach ($reprocesos as $reproceso) {
      $banco = Banco::where('codigo', $reproceso->cod_banco)->first();      

      $Proceso = new RegistroProcesosEspeciales();
      $Proceso->cod_operacion = $reproceso->cod_operacion;
      $Proceso->id_procesos_especiales = $proceso_especial->id;
      $Proceso->nombre_proceso = $proceso_especial->nombre;
      $Proceso->user_id = Auth::user()->id;
      $Proceso->nombre_usuario = Auth::user()->name;
      $Proceso->estatus = 'DENEGADO';
      $Proceso->ip_conexion = $request->ip();
      $Proceso->fecha = $fecha;
      $Proceso->cod_banco = $banco->codigo;
      $Proceso->descripcion = $reproceso->descripcion;
      $Proceso->observaciones = $reproceso->observaciones;
      $Proceso->cargar = '1';
      $Proceso->validar ='1';
      $Proceso->save();
    }

    Alert::success('!','Exclusión denegada para el participante Exitosamente');


    return redirect()->back();
  } 
}
