<?php

namespace App\Http\Controllers\ProcesosEspeciales\CloseDayOff; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EjecucionAdministrativa\LiquidacionesArchivos\LiquidacionesArchivos;
use App\Models\Configuracion\Calendario\FechasCalendario;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;
use App\Models\ProcesosEspeciales\ProcesosEspeciales;
use App\Models\ProcesosEspeciales\v_totales_closedayoff;
use App\Models\ProcesosEspeciales\v_detalle_closedayoff;
use Carbon\Carbon;
use Auth;
use DB;
use App\Services\Email\Correo;
use Alert;



class CloseDayOffController extends Controller
{
    public function Index(Request $request)
    {
        // Verifica la cantidad de liquidaciones pendientes
        $cant_liquidaciones = LiquidacionesArchivos::where('estatus', 'TO_SETTLED')
        ->count();

        // Valida que exista por lo menos una liquidación pendiente
        if ($cant_liquidaciones >= 1) {
            $validar = true;
        } else {
            $validar = false;
        }

        // Consulta el último registro de Close Day Off en la tabla de procesos especiales
        $closedayoff = RegistroProcesosEspeciales::where('id_procesos_especiales', 2)
        ->orderBy('id', 'desc')
        ->first();
        
        $totales = v_totales_closedayoff::first();
        $totales_por_banco = v_detalle_closedayoff::paginate(3);

    
        $calendarios_historico = FechasCalendario::select('t_fechas_calendario.id','t_fechas_calendario.fecha','t_fechas_calendario.fecha_validacion','t_fechas_calendario.estatus', 't_histo_registro_procesos_especiales.cod_operacion','t_histo_registro_procesos_especiales.observaciones')
          ->join('epsach_his.t_histo_registro_procesos_especiales',DB::raw('SUBSTR(t_histo_registro_procesos_especiales.fecha, 0, 8)'), '=', 't_fechas_calendario.fecha_validacion')
          ->where('t_fechas_calendario.estatus', 'CLOSE_DAYOFF')
          ->orderBy('t_fechas_calendario.fecha', 'DESC')
          ->paginate(15);
        
        //dd($calendarios_historico);

        return view('procesos_especiales.closedayoff.pr_cl_01_i_closedayoff')
        ->with('calendarios_historico', $calendarios_historico)
        ->with('validar', $validar)
        ->with('totales', $totales)
        ->with('totales_por_banco', $totales_por_banco)
        ->with('closedayoff', $closedayoff);
    }

    public function store(Request $request)
    {  //dd($request->get('causa'));

        $emisor = 'NCCE';
        $fecha = Carbon::now()->format('Ymdhis');

        $proceso_especial = ProcesosEspeciales::find(2);

        $Proceso = new RegistroProcesosEspeciales();
        $Proceso->cod_operacion = rand(1, 99999999);
        $Proceso->id_procesos_especiales = $proceso_especial->id;
        $Proceso->nombre_proceso = $proceso_especial->nombre;
        $Proceso->user_id = Auth::user()->id;
        $Proceso->nombre_usuario = Auth::user()->name;
        $Proceso->estatus = 'REGISTRADO';
        $Proceso->ip_conexion = $request->ip();
        $Proceso->fecha = $fecha;
        $Proceso->cod_banco = $emisor;
        $Proceso->descripcion = $fecha;
        $Proceso->observaciones = $request->get('causa');
        $Proceso->cargar = '1';
        $Proceso->save();

        Alert::success('!','Registro satisfactorio de Close Day Off');

        Correo::notificacion('E008');
        return redirect()->back();
    }

    public function aprobar(Request $request, $id)
    {
    
    DB::beginTransaction();   
        try{ 
        $this->cambiarEstatus($id, true);
        
        $query = "begin sp_close_dayoff(); commit; end;";
        DB::statement($query);

        Alert::success('!','Close Day Off aprobado');

        Correo::notificacion('E009');
        return redirect()->back();
    }
    catch (\Exception $e) {
        
        Alert::error('!','ERROR aprobando Close day off');

   return redirect()->back();
    }


    }

    public function denegar(Request $request, $id)
    {
        $this->cambiarEstatus($id, false);

        Alert::success('!','Close day off denegado');

        return redirect()->back();
    }

    protected function cambiarEstatus($id, $aprobado)
    {
        $emisor = 'NCCE';
        $fecha = Carbon::now()->format('Ymdhis');

        $anterior = RegistroProcesosEspeciales::find($id);

        $Proceso = new RegistroProcesosEspeciales();
        $Proceso->cod_operacion = $anterior->cod_operacion;
        $Proceso->id_procesos_especiales = $anterior->id_procesos_especiales;
        $Proceso->nombre_proceso = $anterior->nombre_proceso;
        $Proceso->user_id = Auth::user()->id;
        $Proceso->nombre_usuario = Auth::user()->name;
        $Proceso->estatus = $aprobado ? 'APROBADO' : 'DENEGADO';
        $Proceso->ip_conexion = request()->ip();
        $Proceso->fecha = $fecha;
        $Proceso->cod_banco = $emisor;
        $Proceso->descripcion = $anterior->descripcion;
        $Proceso->observaciones = $anterior->observaciones;
        $Proceso->cargar = '1';
        $Proceso->validar = '1';
        $Proceso->save();
    }

    public function detalle(Request $request)
    {
        return view('procesosespeciales.closedayoff.pr02_i_closedayoff');
    }

    public function excel()
    {

    }

    public function pdf()
    {
    }
}
