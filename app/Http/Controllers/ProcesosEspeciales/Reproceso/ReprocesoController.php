<?php

namespace App\Http\Controllers\ProcesosEspeciales\Reproceso; 
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\MedioPago\Producto;
use App\Models\ProcesosEspeciales\V_posiciones_multilaterales_def;
use App\Models\ProcesosEspeciales\HRegistroProcesosEspeciales;
use Carbon\Carbon;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;
use App\Models\ProcesosEspeciales\v_procesos_especiales_reproceso;
use App\Models\ProcesosEspeciales\ProcesosEspeciales;
use App\Models\Monitoreo\liquidacionArchivos;
use Auth;
use App\Services\Email\Correo;

class ReprocesoController extends Controller
{
    public function getIndex(Request $request)
    {
        $bancos = Banco::where('estatus', 'ACTIVO')
        ->select('codigo', 'nombre')
        ->get()
        ->pluck('nombre_completo', 'codigo');
        
        $fecha = Carbon::now()->format('d/m/Y h:i:s');
        $hacer = false;

        $bancos_f_1=$request->get('banco');
        $bancos_f=$request->get('banco'); 
        if($bancos_f==null){
        $bancos_f=['0000','0009'];
        $bancos_f_1=['0000','0009'];    
        }

        $posiciones=[];
        
        if ($bancos_f != null ) {
        $bancos_f=implode("', '", $bancos_f);  
        $hacer = true;    
        $query = "SELECT s.cod_banco_emisor,b.nombre,s.posicion FROM
                    (SELECT cod_banco_emisor , sum (posicion) AS posicion 
                        FROM 
                          ((SELECT cod_banco_emisor , sum (posicion_emisor) AS posicion  FROM 
                          (SELECT cod_banco_emisor, sum(posicion_emisor) AS posicion_emisor 
                                FROM v_posicion_bilateral_creditos
                                  WHERE estatus = 'PICKED_UP'
                                  AND   (cod_banco_emisor NOT IN ('$bancos_f') and cod_banco_destinatario NOT IN ('$bancos_f'))
                          GROUP BY  cod_banco_emisor
                            UNION ALL
                          SELECT cod_banco_emisor, sum(posicion_emisor) AS posicion_emisor
                                FROM v_posicion_bilateral_cheques
                                  WHERE estatus = 'PICKED_UP'
                                  AND   (cod_banco_emisor NOT IN ('$bancos_f') and cod_banco_destinatario NOT IN ('$bancos_f'))
                          GROUP BY  cod_banco_emisor
                            UNION ALL
                          SELECT cod_banco_emisor, sum(posicion_emisor) AS posicion_emisor
                                FROM v_posicion_bilateral_domiciliaciones
                                  WHERE estatus = 'PICKED_UP'
                                  AND   (cod_banco_emisor NOT IN ('$bancos_f') and cod_banco_destinatario NOT IN ('$bancos_f'))
                          GROUP BY  cod_banco_emisor)
                    GROUP BY  cod_banco_emisor))
                GROUP BY  cod_banco_emisor )s
                INNER JOIN
                t_bancos b
                ON 
                s.cod_banco_emisor = b.codigo";
    $posiciones = DB::select($query);      
  }
  $total_positivos=0;
  $total_negativos=0;
 foreach ($posiciones as $posicion) {
   if($posicion->posicion>0){
    $total_positivos=$total_positivos+$posicion->posicion;
  }
    else{
      $total_negativos=$total_negativos+$posicion->posicion;
    }
 }


    return view('procesos_especiales.reproceso.simulacion.pr_re_01_i_simulacion')
    ->with('bancos', $bancos)
    ->with('total_positivos', $total_positivos)
    ->with('total_negativos', $total_negativos)
    ->with('bancos_f_1', $bancos_f_1)
    ->with('bancos_f', $bancos_f)
    ->with('posiciones',$posiciones)
    ->with('fecha', $fecha)
    ->with('hacer', $hacer);
}

    public function procesos(Request $request) 
    {
        
        $moneda_activa = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
        ->where('t_monedas.estatus','ACTIVO')
        ->first();

        $bancos = Banco::buscarTodos();
        $banco = $request->get('cod_banco') ? $request->get('cod_banco') : '*';
        $registrados=0;
        $reprocesos = v_procesos_especiales_reproceso::select('v_procesos_especiales_reproceso.*','t_bancos.nombre')
        ->join('t_bancos','t_bancos.codigo','=','v_procesos_especiales_reproceso.cod_banco')
        ->where('v_procesos_especiales_reproceso.id_procesos_especiales', 4)
        ->where(function ($query) use ($banco) {
            if ($banco != '*') {
                $query->where('v_procesos_especiales_reproceso.cod_banco', $banco);
            }
        })
        ->orderBy('v_procesos_especiales_reproceso.id', 'DESC')
        ->get();
        foreach($reprocesos as $registroo){
         $registroo->nombre = Banco::where('codigo', $registroo->cod_banco)->first()->nombre;
        }

        $reprocesos_historico =  HRegistroProcesosEspeciales::where('id_procesos_especiales', 4)->get();

        foreach($reprocesos_historico as $registro){
         $registro->nombre = Banco::where('codigo', $registro->cod_banco)->first()->nombre;
        }
        //dd($reprocesos_historico);

       foreach ($reprocesos as $registro) {if ($registro->estatus=='REGISTRADO'){$registrados=$registrados+1; }}
      

       return view('procesos_especiales.reproceso.consulta.pr_re_01_i_consulta')
        ->with('reprocesos_historico', $reprocesos_historico)
        ->with('banco', $banco)
        ->with('registrados', $registrados)  
        ->with('bancos', $bancos)
        ->with('reprocesos', $reprocesos);

    }

     public function aprobar(Request $request) 
 {

  $fecha = Carbon::now()->format('Ymdhis');
  $reprocesos = v_procesos_especiales_reproceso::
      where('id_procesos_especiales', 4)
     ->where('estatus', 'REGISTRADO')
     ->orderBy('id', 'DESC')
     ->get();

  foreach ($reprocesos as $reproceso) {
  $Proceso = new RegistroProcesosEspeciales();
    $Proceso->cod_operacion = $reproceso->cod_operacion;
    $Proceso->id_procesos_especiales = $reproceso->id_procesos_especiales;
    $Proceso->nombre_proceso = $reproceso->nombre_proceso;
    $Proceso->user_id = Auth::user()->id;
    $Proceso->nombre_usuario = Auth::user()->name;
    $Proceso->estatus = 'APROBADO';
    $Proceso->ip_conexion = request()->ip();
    $Proceso->fecha = $fecha;
    $Proceso->cod_banco = $reproceso->cod_banco;
    $Proceso->descripcion = $reproceso->descripcion;
    $Proceso->observaciones = $reproceso->observaciones;
    $Proceso->cargar = '1';
    $Proceso->tratar = '0';
    $Proceso->validar = '1';
    $Proceso->save();
}

   DB::beginTransaction(); 
      
  try {
 
  $query = "select SP_PROCESAR_OREJT() from dual";
  DB::statement($query);
  
  DB::commit();

     }

  catch (\Exception $e) {
  
  $error = $e->getMessage();
  DB::rollback();

   notify()->flash('REPROCESO NO EJECUTADO', 'warning', [
      'timer' => 3000,
      'text' => ''
        ]);
   return redirect()->back();

    }
 	 

$max_num_liquidacion=liquidacionArchivos::where('estatus', 'FAILED')
    ->distinct('num_liquidaciones')
    ->orderBy('id', 'DESC')
    ->get()->max('num_liquidaciones');

  $liquidacion_registros=liquidacionArchivos::where('estatus', 'FAILED')
  ->where('num_liquidaciones', $max_num_liquidacion) 
  ->orderBy('id', 'DESC')
  ->limit(2)
  ->get();


 foreach ( $liquidacion_registros as $liquidacion_c ) 
  {
         $liquidacion = new liquidacionArchivos();
         $liquidacion->fecha_hora_ejecucion=Carbon::now()->addMinute(1)->format('YmdGis');
         $liquidacion->critico_ejecucion=Carbon::now()->addMinute(1)->format('YmdGis');
         $liquidacion->estatus = 'TO_SETTLED';
         $liquidacion->num_liquidaciones=$liquidacion_c->num_liquidaciones;
         $liquidacion->codigos_tipos_operacion=$liquidacion_c->codigos_tipos_operacion;
         $liquidacion->tipo_operacion_id=$liquidacion_c->tipo_operacion_id;
         $liquidacion->save();    
  } 

 
Correo::notificacion('E011');
return redirect()->back();

 } 

  public function denegar(Request $request) 
 {  
  
  $reprocesos = RegistroProcesosEspeciales::
  where('id_procesos_especiales', 4)
  ->where('estatus', 'REGISTRADO')
  ->orderBy('id', 'DESC')
  ->get();

  $max_num_liquidacion=liquidacionArchivos::where('estatus', 'FAILED')
  ->distinct('num_liquidaciones')
  ->orderBy('id', 'DESC')
  ->get()->max('num_liquidaciones');

  $liquidacion_registros=liquidacionArchivos::where('estatus', 'FAILED')
  ->where('num_liquidaciones', $max_num_liquidacion) 
  ->orderBy('id', 'DESC')
  ->limit(2)
  ->get();


  $fecha = Carbon::now()->format('Ymdgis');
     
      
      DB::beginTransaction(); 
      
    try {


   foreach ( $liquidacion_registros as $liquidacion_c ) 
  {
         $liquidacion = new liquidacionArchivos();
         $liquidacion->fecha_hora_ejecucion=Carbon::now()->addMinute(1)->format('YmdGis');
         $liquidacion->critico_ejecucion=Carbon::now()->addMinute(1)->format('YmdGis');
         $liquidacion->estatus = 'TO_SETTLED';
         $liquidacion->num_liquidaciones=$liquidacion_c->num_liquidaciones;
         $liquidacion->codigos_tipos_operacion=$liquidacion_c->codigos_tipos_operacion;
         $liquidacion->tipo_operacion_id=$liquidacion_c->tipo_operacion_id;
         $liquidacion->save();    
  } 
foreach ($reprocesos as $reproceso) 
{
      $Proceso = new RegistroProcesosEspeciales();
      $Proceso->cod_operacion = $reproceso->cod_operacion;
      $Proceso->id_procesos_especiales = $reproceso->id_procesos_especiales;
      $Proceso->nombre_proceso = $reproceso->nombre_proceso;
      $Proceso->user_id = Auth::user()->id;
      $Proceso->nombre_usuario = Auth::user()->name;
      $Proceso->estatus = 'DENEGADO';
      $Proceso->ip_conexion = request()->ip();
      $Proceso->fecha = Carbon::now()->format('YmdGis');
      $Proceso->cod_banco = $reproceso->cod_banco;
      $Proceso->descripcion = $reproceso->descripcion;
      $Proceso->observaciones = $reproceso->observaciones;
      $Proceso->cargar = '1';
      $Proceso->validar = '2';
      $Proceso->save();
}
    DB::commit();

    notify()->flash('Reproceso DENEGADO exitosamente ' , 'success', [
      'timer' => 3000,
      'text' => ''
        ]);

return redirect()->back(); 
}
catch (\Exception $e) {
  
  $error = $e->getMessage();
  DB::rollback();

   notify()->flash('Reproceso NO PUDO se denegado correctamente', 'warning', [
      'timer' => 3000,
      'text' => ''
        ]);
   return redirect()->back();

    }


   
}

}


