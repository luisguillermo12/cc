<?php

namespace App\Http\Controllers\ProcesosEspeciales\DayOff; 

use App\Models\Configuracion\ParametroSistema\ParametroSistema;
use App\Models\ProcesosEspeciales\RegistroProcesosEspeciales;
use App\Models\Configuracion\Calendario\FechasCalendario;
use App\Models\ProcesosEspeciales\ProcesosEspeciales;
use App\Http\Controllers\Controller;
use App\Services\Email\Correo;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Carbon\Carbon;
use Alert;
use Auth;
use DB;

class DayOffController extends Controller
{
    public function __construct() {
        Date::setLocale('es');
    }

    public function index()
    {
        $query = 'select fn_busca_ultimo_dia_compensado from dual';
        $desde = DB::select($query)[0]->fn_busca_ultimo_dia_compensado;

        $hasta = Carbon::now();

        $no_laborables = FechasCalendario::whereIn('estatus', ['CERRADO', 'BANCARIO', 'FERIADO', 'COMPENSADO', 'DAYOFF', 'CLOSE_DAYOFF'])
        ->select('fecha')
        ->whereBetween('fecha', [Date::parse($desde), $hasta])
        ->get();


        $feriados = [];
        foreach ($no_laborables as $no_laboral) {
            $feriados[] = $no_laboral->fecha;
        }

        /* eps anterior
        $calendarios = FechasCalendario::select('t_fechas_calendario.id,t_fechas_calendario.fecha,t_fechas_calendario.fecha_validacion,t_fechas_calendario.estatus, t_registro_procesos_especiales.cod_operacion, t_registro_procesos_especiales.observaciones')
        ->join('t_registro_procesos_especiales','t_registro_procesos_especiales.id_t_calendario', '=', 't_fechas_calendario.id'  )
        ->where('t_fechas_calendario.estatus', 'DAYOFF')
        ->orderBy('t_fechas_calendario.fecha', 'DESC')
        ->paginate(15);
        */

        $calendarios = DB::table('t_fechas_calendario as t1')
          ->join('t_registro_procesos_especiales as t2' ,'t1.id','=','t2.id_t_calendario')
          ->select('t1.id', 't1.fecha', 't1.fecha_validacion', 't1.estatus', 't2.cod_operacion', 't2.observaciones')
          ->where('t1.estatus','DAYOFF')
          ->orderBy('t1.fecha', 'DESC')
          ->paginate(15);

        //dd($calendarios);

        /* eps anterior
          $calendarios_historico = FechasCalendario::select('t_fechas_calendario.id, t_fechas_calendario.fecha, t_fechas_calendario.fecha_validacion, t_fechas_calendario.estatus, t_histo_registro_procesos_especiales.cod_operacion, t_histo_registro_procesos_especiales.observaciones')
          ->join('epsach_his.t_histo_registro_procesos_especiales','t_histo_registro_procesos_especiales.id_t_calendario', '=', 't_fechas_calendario.id'  )
          ->where('t_fechas_calendario.estatus', 'DAYOFF')
          ->orderBy('t_fechas_calendario.fecha', 'DESC');
          ->paginate(15);
        */

        $calendarios_historico = DB::table('t_fechas_calendario as t1')
          ->join('epsach_his.t_histo_registro_procesos_especiales as t2' ,'t1.id','=','t2.id_t_calendario')
          ->select('t1.id', 't1.fecha', 't1.fecha_validacion', 't1.estatus', 't2.cod_operacion', 't2.observaciones')
          ->where('t1.estatus','DAYOFF')
          ->orderBy('t1.fecha', 'DESC')
          ->paginate(15);

        //dd($calendarios_historico);

        $dayoff = RegistroProcesosEspeciales::where('id_procesos_especiales', 1)
        ->orderBy('id', 'DESC')
        ->first();

        // Calcular último día laborado
        $query = "select fn_busca_ultimo_dia_compensado as fecha from dual";
        $fecha_ultimo = DB::select($query)[0]->fecha;

        $estatus_ultimo = FechasCalendario::whereDate('fecha', '=', $fecha_ultimo)
        ->first()
        ->estatus;

        $ultimo = (object) ['fecha' => Carbon::parse($fecha_ultimo), 'estatus' => $estatus_ultimo];
  

        return view('procesos_especiales.dayoff.pr_da_01_i_dayoff')
        ->with('desde', $desde)
        ->with('hasta', $hasta->format('Ymd'))
        ->with('feriados', $feriados)
        ->with('calendarios', $calendarios)
        ->with('calendarios_historico', $calendarios_historico)
        ->with('ultimo', $ultimo)
        ->with('dayoff', $dayoff);
    }

    public function detalle($id)
    {
        /* eps anterior
         $calendario = FechasCalendario::select('t_fechas_calendario.id,t_fechas_calendario.fecha,t_fechas_calendario.fecha_validacion,t_fechas_calendario.estatus, t_registro_procesos_especiales.cod_operacion, t_registro_procesos_especiales.observaciones')
        ->join('t_registro_procesos_especiales','t_registro_procesos_especiales.id_t_calendario', '=', 't_fechas_calendario.id'  )
        ->where('t_fechas_calendario.estatus', 'DAYOFF')
        ->where('t_fechas_calendario.id', $id)
        ->orderBy('t_fechas_calendario.fecha', 'DESC')
        ->first();
  */
        $calendario = DB::table('t_fechas_calendario as t1')
          ->join('t_registro_procesos_especiales as t2' ,'T1.id','=','T2.id_t_calendario')
          ->select('t1.id', 't1.fecha', 't1.fecha_validacion', 't1.estatus', 't2.cod_operacion', 't2.observaciones')
          ->where('t1.id',$id)
          ->orderBy('t1.fecha', 'DESC')
          ->paginate(15);

          //dd($calendario[0]->cod_operacion);

        $registrado = RegistroProcesosEspeciales::where('cod_operacion', $calendario[0]->cod_operacion)
        ->where('estatus', 'REGISTRADO')
        ->orderBy('id', 'DESC')
        ->first();

        $aprobado = RegistroProcesosEspeciales::where('cod_operacion', $calendario[0]->cod_operacion)
        ->where('id_t_calendario', $id)
        ->where('estatus', 'APROBADO')
        ->orderBy('id', 'DESC')
        ->first();

        //dd($registrado,);
       // $calendario = FechasCalendario::find($id);

        return view('procesos_especiales.dayoff.pr_da_02_v_detalles')
        ->with('calendario', $calendario)
        ->with('aprobado', $aprobado)
        ->with('registrado', $registrado);
    }

    public function store(Request $request)
    {
         $query ="SELECT SYSDATE AS fecha FROM DUAL"; 
         $fecha = DB::select($query)[0]->fecha; 
         $fecha =Carbon::parse($fecha)->format('Ymdhis');
      
        $emisor = 'NCCE';
        $proceso_especial = ProcesosEspeciales::find(1);

        $Proceso = new RegistroProcesosEspeciales();
        $Proceso->cod_operacion = rand(1, 99999999);
        $Proceso->id_procesos_especiales = $proceso_especial->id;
        $Proceso->nombre_proceso = $proceso_especial->nombre;
        $Proceso->user_id = Auth::user()->id;
        $Proceso->nombre_usuario = Auth::user()->name;
        $Proceso->estatus = 'REGISTRADO';
        $Proceso->ip_conexion = $request->ip();
        $Proceso->fecha = $fecha;
        $Proceso->cod_banco = $emisor;
        $Proceso->descripcion = Carbon::parse($request->get('fecha'))->format('Ymd');
        $Proceso->observaciones = $request->get('causa');
        $Proceso->cargar = '1';
        $Proceso->save();

        Alert::success('!','Registro satisfactorio de Day Off');

        Correo::notificacion('E006');
        return redirect()->back();
    }

    public function aprobar(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {

            $emisor = 'NCCE';
            $query ="SELECT SYSDATE AS fecha FROM DUAL"; 
            $fecha = DB::select($query)[0]->fecha; 
            $fecha =Carbon::parse($fecha)->format('Ymdhis');

            $anterior = RegistroProcesosEspeciales::find($id);

            $Proceso = new RegistroProcesosEspeciales();
            $Proceso->cod_operacion = $anterior->cod_operacion;
            $Proceso->id_procesos_especiales = $anterior->id_procesos_especiales;
            $Proceso->nombre_proceso = $anterior->nombre_proceso;
            $Proceso->user_id = Auth::user()->id;
            $Proceso->nombre_usuario = Auth::user()->name;
            $Proceso->estatus = 'APROBADO';
            $Proceso->ip_conexion = request()->ip();
            $Proceso->fecha = $fecha;
            $Proceso->cod_banco = $emisor;
            $Proceso->descripcion = $anterior->descripcion;
            $Proceso->observaciones = $anterior->observaciones;
            $Proceso->cargar = '1';
            $Proceso->validar = '1';
            $Proceso->save();

            $hasta = str_replace('-', '', $anterior->descripcion);

            $query = "begin sp_inicia_dayoff($hasta); commit; end;";
            DB::statement($query);

        });

        Alert::success('!','Day Off aprobado');

        Correo::notificacion('E007');
        return redirect()->back();
    }

    public function denegar(Request $request, $id)
    {
        $emisor = 'NCCE';
        $fecha = Carbon::now()->format('Ymdhis');

        $anterior = RegistroProcesosEspeciales::find($id);

        $Proceso = new RegistroProcesosEspeciales();
        $Proceso->cod_operacion = $anterior->cod_operacion;
        $Proceso->id_procesos_especiales = $anterior->id_procesos_especiales;
        $Proceso->nombre_proceso = $anterior->nombre_proceso;
        $Proceso->user_id = Auth::user()->id;
        $Proceso->nombre_usuario = Auth::user()->name;
        $Proceso->estatus = 'DENEGADO';
        $Proceso->ip_conexion = request()->ip();
        $Proceso->fecha = $fecha;
        $Proceso->cod_banco = $emisor;
        $Proceso->descripcion = $anterior->descripcion;
        $Proceso->observaciones = $anterior->observaciones;
        $Proceso->cargar = '1';
        $Proceso->validar = '1';
        $Proceso->save();

        Alert::error('!', 'Day Off Cancelado!');

        return redirect()->back();
    }
}
