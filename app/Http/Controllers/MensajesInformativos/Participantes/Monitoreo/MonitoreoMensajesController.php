<?php

namespace App\Http\Controllers\MensajesInformativos\Participantes\Monitoreo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MensajesInformativos\MensajesInformativos;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\Configuracion\Banco\Banco;

class MonitoreoMensajesController extends Controller
{
    public function getIndex(Request $request)
    {
        $emisor = $request->get('emisor') != null ? $request->get('emisor') : '*';
        $receptor = $request->get('receptor') != null ? $request->get('receptor') : '*';
        $moneda = $request->get('moneda') != null ? $request->get('moneda') : '*';

        $bancos = Banco::buscarTodos();
        $monedas = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo_iso', 'asc')
        ->pluck('codigo_iso', 'codigo_iso');

        $mensajes = MensajesInformativos::where('codigo_participante_emisor', '<>', 'NCCE')
        ->where('tipo_mensaje', 'IFP')
        ->where('estatus', 'SENT')
        ->where('codigo_participante_receptor', '<>', 'NCCE')
        ->where(function ($query) use ($emisor) {
            if ($emisor != '*') {
                $query->where('codigo_participante_emisor', $emisor);
            }
        })
        ->where(function ($query) use ($receptor) {
            if ($receptor != '*') {
                $query->where('codigo_participante_receptor', $receptor);
            }
        })
        ->where(function ($query) use ($moneda) {
            if ($moneda != '*') {
                $query->where('codigo_iso_moneda', $moneda);
            }
        })
        ->orderBy('fecha_intercambio', 'DESC')
        ->orderBy('codigo_participante_receptor', 'ASC')
        ->paginate(15);

        return view('mensajes_informativos.Participantes.monitoreo.me_mo_01_i_momensajes')
        ->with('bancos', $bancos)
        ->with('emisor', $emisor)
        ->with('receptor', $receptor)
        ->with('receptor', $receptor)
        ->with('monedas', $monedas)
        ->with('moneda', $moneda)
        ->with('mensajes', $mensajes);
    }
}
