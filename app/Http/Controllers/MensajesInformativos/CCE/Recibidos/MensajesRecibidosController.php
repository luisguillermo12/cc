<?php

namespace App\Http\Controllers\MensajesInformativos\CCE\Recibidos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuracion\Banco\Banco;
use Session;
use DB;
use Illuminate\Database\Connection;
use Yajra\Datatables\Facades\Datatables;
use Validator;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\MensajesInformativos\MensajesInformativos;

class MensajesRecibidosController extends Controller
{
    public function getIndex(Request $request)
    {
         // Buscar la moneda activa
        $moneda_activa = Moneda::select('t_monedas.codigo_iso','t_monedas.nombre','t_monedas.num_decimales','t_monedas.separador_miles','t_monedas.separador_decimales')
        ->where('t_monedas.estatus','ACTIVO')
        ->first();

        // Buscar los bancos activos
        $bancos = Banco::buscarTodos();

        // Buscar todas las moendas
        $monedas = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo_iso', 'asc')
        ->pluck('codigo_iso', 'codigo_iso');

        $banco = $request->get('cod_banco');
        $moneda_act = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->first();
        $moneda = $request->get('cod_moneda') ?? $moneda_act->codigo_iso;
        //dd($moneda_act);

        // Consultar las operaciones validadas
        $mensajes = MensajesInformativos::where('tipo_mensaje', 'IFC')
        ->where('codigo_iso_moneda', '=',  $moneda)
        ->where(function ($query) use ($banco) {
            if ($banco != null) {
                $query->where('codigo_participante_emisor', $banco);
            }
        })
        ->orderBy('fecha_intercambio', 'DESC')
        ->paginate(15);
	
        return view('mensajes_informativos.cce.recibidos.me_re_01_i_recibidos')
        ->with('banco', $banco)
        ->with('bancos', $bancos)
        ->with('moneda', $moneda)
        ->with('monedas', $monedas)
        ->with('mensajes', $mensajes);
    }
                     
    public function show($id)
    {
        $mensaje = MensajesInformativos::where('id', '=', $id)
        ->first();
            
        return view('mensajes_informativos.cce.recibidos.me_re_02_i_recibidos')
        ->with('mensaje', $mensaje);
    }
}
