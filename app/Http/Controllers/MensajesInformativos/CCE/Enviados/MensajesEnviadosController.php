<?php

namespace App\Http\Controllers\MensajesInformativos\CCE\Enviados;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Configuracion\Moneda\Moneda;
use App\Models\MensajesInformativos\MensajesInformativos;
use App\Models\Configuracion\Banco\Banco;
use App\User;

class MensajesEnviadosController extends Controller
{
    public function getIndex(Request $request)
    {
        // Buscar los bancos activos
        $bancos = Banco::buscarTodos();

        // Buscar todas las moendas
        $monedas = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->orderBy('codigo_iso', 'asc')
        ->pluck('codigo_iso', 'codigo_iso');

        $banco = $request->get('cod_banco');
        $moneda_act = Moneda::select('codigo_iso')
        ->where('estatus','ACTIVO')
        ->first();
        $moneda = $request->get('cod_moneda') ?? $moneda_act->codigo_iso;
	 
	     $mensajes = MensajesInformativos::where('codigo_participante_emisor', '=', 'NCCE')
        ->where('tipo_mensaje', 'IFP')
        ->where('estatus', 'SENT')
        ->where('codigo_participante_receptor', '<>', 'NCCE')
        ->where('codigo_iso_moneda',  $moneda)
        ->where(function ($query) use ($banco) {
            if ($banco != null) {
                $query->where('codigo_participante_receptor', $banco);
            }
        })
        ->orderBy('fecha_intercambio', 'DESC')
        ->orderBy('codigo_participante_receptor', 'ASC')
        ->paginate(15);

	
        return view('mensajes_informativos.cce.enviados.me_en_01_i_enviados')
        ->with('banco', $banco)
        ->with('bancos', $bancos)
        ->with('moneda', $moneda)
        ->with('monedas', $monedas)
        ->with('mensajes', $mensajes);
    }

    public function show($id)
    {
        $mensaje = MensajesInformativos::where('id', '=', $id)
        ->first();
        $user = User::where('id', '=', $mensaje->usuario_id)
        ->first();

        return view('mensajes_informativos.cce.enviados.me_en_02_v_detalles')
        ->with('mensaje', $mensaje)
        ->with('user', $user);
    }
}
