<?php

namespace App\Http\Controllers\MensajesInformativos\CCE\NuevoMensaje;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MensajesInformativos\MensajesInformativos;
use App\Models\Configuracion\Banco\Banco;
use App\Models\Configuracion\Moneda\Moneda;
use Carbon\Carbon;
use Auth;
use DB;

class NuevoMensajeController extends Controller
{
    public function index(Request $request)
    {
        $bancos = Banco::buscarTodos();

        return view('mensajes_informativos.cce.nuevo_mensaje.me_nu_01_i_nmensaje')
        ->with('bancos', $bancos);
    }

    public function store(Request $request)
    {

        // Variables generales
        $emisor = 'NCCE';
        $fecha = Carbon::now()->format('Ymdhis');
        $estatus = 'TO_SENT';
        $contenido = $request->get('mensaje');

        // Se busca la primera moneda activa
        // Si se le agrega el listado de monedas debe cambiarse acá
        $moneda = Moneda::where('estatus', 'ACTIVO')
        ->first();

        if ($request->get('para') != '*') {

            $mensaje = new MensajesInformativos();

            // Se busca al banco receptor por medio de su código
            $receptor = Banco::where('codigo', $request->get('para'))
            ->first();

            // El emisor es la Cámara de compensación.
            $mensaje->codigo_participante_emisor = $emisor;

            $mensaje->codigo_participante_receptor = $receptor->codigo;
            $mensaje->id_participante_receptor = $receptor->id;
            $mensaje->tipo_mensaje = 'IFP';

            // Se incluyen los datos de la moneda
            $mensaje->codigo_iso_moneda = $moneda->codigo_iso;
            $mensaje->t_moneda_id = $moneda->id;

            // Ambas fechas son la misma fecha de creación
            $mensaje->fecha_archivo = $fecha;
            $mensaje->fecha_intercambio = $fecha;

            // Se obtiene el mensaje
            $mensaje->mensaje = $contenido;

            // Se obtiene el id del usuario que envía el mensaje
            $mensaje->usuario_id = Auth::user()->id;

            // Se le agrega el estatus TO_SENT para que pueda ser tomado por el sistema.
            $mensaje->estatus = $estatus;
            $mensaje->cargar = '1';
            $mensaje->tratar = '1';
            $mensaje->validar = '2';
            $mensaje->respuesta = '2';

            $mensaje->save();

        } else {

            $bancos = Banco::where('estatus', 'ACTIVO')
            ->select('codigo', 'id')
            ->get();

            $mensaje = new MensajesInformativos();

            // El emisor es la Cámara de compensación.
            $mensaje->codigo_participante_emisor = $emisor;

            $mensaje->codigo_participante_receptor = 'NCCE';
            $mensaje->tipo_mensaje = 'IFG';

            // Se incluyen los datos de la moneda
            $mensaje->codigo_iso_moneda = $moneda->codigo_iso;
            $mensaje->t_moneda_id = $moneda->id;

            // Ambas fechas son la misma fecha de creación
            $mensaje->fecha_archivo = $fecha;
            $mensaje->fecha_intercambio = $fecha;

            // Se obtiene el mensaje
            $mensaje->mensaje = $contenido;

            // Se obtiene el id del usuario que envía el mensaje
            $mensaje->usuario_id = Auth::user()->id;

            // Se le agrega el estatus TO_SENT para que pueda ser tomado por el sistema.
            $mensaje->estatus = $estatus;
            $mensaje->cargar = 1;

            $mensaje->save();

            // Se llama al SP que genera los mensajes para todos los bancos activos.
            $query = "BEGIN SP_GENERA_MENSAJE_INFORMATIVO_IFG(); END;";
            DB::statement($query);
        }

        alert()->success('Mensaje registrado para su envio', 'success');

        return redirect()->back();
    }
}
