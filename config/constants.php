<?php

/*
|--------------------------------------------------------------------------
| Constantes de la aplicacion
|--------------------------------------------------------------------------
|
*/
return [
    'reportes' => [ //constantes para indicar al controlador caracteristicas de busqueda en bd
        'caract_extension_excel' => 'xls', //extension para generar el archivo excel
        'caract_extension_pdf' => 'pdf', //extension para generar el archivo pdf
        'caract_temporary_folder' => '/temp', //carpeta temporal que se va a usar al momento de generar el reporte
        'caract_busqueda_trans' => '0', //transaccional
        'caract_busqueda_histo' => '1', //historico
        'caract_busqueda_agrup' => '0', //por agrupacion o consolidado
        'caract_busqueda_discr' => '1', //por discriminacion o desglose
        'lista_disponible_trans' => array( //listado de reportes disponibles transaccionales
                                      'R0256' => array(
                                        'codigo' => 'R0256',
                                        'nombre' => 'Extensiones de franja horaria',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0256'
                                        ),
                                      'R0257' => array(
                                        'codigo' => 'R0257',
                                        'nombre' => 'Intercambio de imágenes bilateral',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0257'
                                        ),
                                      'R0258' => array(
                                        'codigo' => 'R0258',
                                        'nombre' => 'Intercambio de imágenes por hora',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0258'
                                        ),
                                      'R0264' => array(
                                        'codigo' => 'R0264',
                                        'nombre' => 'Resumen de compensación por tipo de operación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0264'
                                        ),
                                      'R0265' => array(
                                        'codigo' => 'R0265',
                                        'nombre' => 'Hoja de compensación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0265'
                                        ),
                                      'R0266' => array(
                                        'codigo' => 'R0266',
                                        'nombre' => 'Resumen de entrada',
                                        'XLS' => false,
                                        'PDF' => false,
                                        'disponible' => false,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0266'
                                        ),
                                      'R0268' => array(
                                        'codigo' => 'R0268',
                                        'nombre' => 'Tiempo de liquidación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0268'
                                        ),
                                      'R0272' => array(
                                        'codigo' => 'R0272',
                                        'nombre' => 'Estado de los participantes (Adicional al informe de requerimiento)',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0272'
                                        ),
                                      'R0273' => array(
                                        'codigo' => 'R0273',
                                        'nombre' => 'Tiempo de procesos por archivo (Adicional al informe de requerimiento)',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Transaccional/R0273'
                                        ),
                                      ),
        'lista_disponible_histo' => array( //listado de reportes disponibles historicos
                                      'R0251' => array(
                                        'codigo' => 'R0251',
                                        'nombre' => 'Archivos por hora',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0251'
                                        ),
                                      'R0252' => array(
                                        'codigo' => 'R0252',
                                        'nombre' => 'Comparativo de compensación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0252'
                                        ),
                                      'R0253' => array(
                                        'codigo' => 'R0253',
                                        'nombre' => 'Operaciones por escala de monto para facturación de alto valor',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => true,
                                        'url' => 'Reportes/Historico/R0253'
                                        ),
                                      'R0254' => array(
                                        'codigo' => 'R0254',
                                        'nombre' => 'Operaciones por escala de montos',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0254'
                                        ),
                                      'R0255' => array(
                                        'codigo' => 'R0255',
                                        'nombre' => 'Operaciones exoneradas de comisión por alto valor',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => true,
                                        'url' => 'Reportes/Historico/R0255'
                                        ),
                                      'R0256' => array(
                                        'codigo' => 'R0256',
                                        'nombre' => 'Extensiones de franja horaria',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0256'
                                        ),
                                      'R0257' => array(
                                        'codigo' => 'R0257',
                                        'nombre' => 'Intercambio de imágenes bilateral',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0257'
                                        ),
                                      'R0258' => array(
                                        'codigo' => 'R0258',
                                        'nombre' => 'Intercambio de imágenes por hora',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0258'
                                        ),
                                      'R0259' => array(
                                        'codigo' => 'R0259',
                                        'nombre' => 'Intercambio bilateral',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0259'
                                        ),
                                      'R0260' => array(
                                        'codigo' => 'R0260',
                                        'nombre' => 'Operaciones por hora',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0260'
                                        ),
                                      'R0261' => array(
                                        'codigo' => 'R0261',
                                        'nombre' => 'Porcentaje de participación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => true,
                                        'url' => 'Reportes/Historico/R0261'
                                        ),
                                      'R0262' => array(
                                        'codigo' => 'R0262',
                                        'nombre' => 'Posiciones liquidadas',
                                        'XLS' => true,
                                        'PDF' => false,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0262'
                                        ),
                                      'R0263' => array(
                                        'codigo' => 'R0263',
                                        'nombre' => 'Resultados netos de la compensación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0263'
                                        ),
                                      'R0264' => array(
                                        'codigo' => 'R0264',
                                        'nombre' => 'Resumen de compensación por tipo de operación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0264'
                                        ),
                                      'R0265' => array(
                                        'codigo' => 'R0265',
                                        'nombre' => 'Hoja de compensación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0265'
                                        ),
                                      'R0267' => array(
                                        'codigo' => 'R0267',
                                        'nombre' => 'Consolidado de subtipos de crédito directo',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0267'
                                        ),
                                      'R0268' => array(
                                        'codigo' => 'R0268',
                                        'nombre' => 'Tiempo de liquidación',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0268'
                                        ),
                                      'R0269' => array(
                                        'codigo' => 'R0269',
                                        'nombre' => 'Tendencias por participante',
                                        'XLS' => true,
                                        'PDF' => false,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0269'
                                        ),
                                      'R0270' => array(
                                        'codigo' => 'R0270',
                                        'nombre' => 'Tendencias por medio de pago',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0270'
                                        ),
                                      'R0271' => array(
                                        'codigo' => 'R0271',
                                        'nombre' => 'Horario de inicio de transmisiones',
                                        'XLS' => true,
                                        'PDF' => false,
                                        'disponible' => true,
                                        'cobranza' => false,
                                        'url' => 'Reportes/Historico/R0271'
                                        ),
                                      'R0275' => array(
                                        'codigo' => 'R0275',
                                        'nombre' => 'Operaciones de alto valor',
                                        'XLS' => true,
                                        'PDF' => true,
                                        'disponible' => true,
                                        'cobranza' => true,
                                        'url' => 'Reportes/Historico/R0275'
                                        ),
                                      ),
    ],
    'operaciones' => [ //constantes para indicar al controlador caracteristicas de validaciones para operaciones
        'caract_operacion_credito' => array('010'), //operacion para subtipos de credito
    ],
];
