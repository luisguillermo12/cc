<?php

// Controllers Within The "App\Http\Controllers\ProcesosEspeciales" Namespace
Route::namespace('ProcesosEspeciales')->group(function () {

  /* INICIO  DAY OFF  */
  Route::resource('ProcesosEspeciales/DayOff', 'DayOff\DayOffController');
  Route::get('ProcesosEspeciales/DayOff/Detalle/{id}', 'DayOff\DayOffController@detalle')->name('DayOffDetalle');
  Route::get('ProcesosEspeciales/DayOff/Aprobar/{id}', 'DayOff\DayOffController@aprobar')->name('dayoff.aprobar');
  Route::get('ProcesosEspeciales/DayOff/Denegar/{id}', 'DayOff\DayOffController@denegar')->name('dayoff.denegar');
  /* FIN  DAY OFF  */

  /* INICIO  CLOSE DAY OFF  */
  Route::resource('ProcesosEspeciales/CloseDayOff', 'CloseDayOff\CloseDayOffController');
  Route::post('ProcesosEspeciales/CloseDayOff/Registro', 'CloseDayOff\CloseDayOffController@store')->name('closedayoff.registrar');
  Route::get('ProcesosEspeciales/CloseDayOff/Aprobar/{id}', 'CloseDayOff\CloseDayOffController@aprobar')->name('closedayoff.aprobar');
  Route::get('ProcesosEspeciales/CloseDayOff/Denegar/{id}', 'CloseDayOff\CloseDayOffController@denegar')->name('closedayoff.denegar');
  /* FIN  CLOSE  DAY OFF  */


  /* INICIO  REPROCESO  */
  Route::get('ProcesosEspeciales/Reproceso/Simulacion', 'Reproceso\ReprocesoController@getIndex')->name('reproceso');
  Route::get('ProcesosEspeciales/Reproceso/Consulta', 'Reproceso\ReprocesoController@procesos')->name('reproceso.simular');
  Route::get('ProcesosEspeciales/Reproceso/Consulta/Aprobar', 'Reproceso\ReprocesoController@aprobar')->name('reproceso.aprobar');
  Route::get('ProcesosEspeciales/Reproceso/Consulta/Denegar', 'Reproceso\ReprocesoController@denegar')->name('reproceso.denegar');
  /* FIN  REPROCESO  */



  /* INICIO  EXCLUCION PARTICIPANTE  */
  Route::get('ProcesosEspeciales/ExclusionParticipante', 'ExclusionParticipante\ExclusionParticipanteController@getIndex')->name('exclusion');
  Route::post('ProcesosEspeciales/ExclusionParticipante/Excluir', 'ExclusionParticipante\ExclusionParticipanteController@store')->name('exclusion.excluir');
  Route::get('ProcesosEspeciales/ExclusionParticipante/Excluir', 'ExclusionParticipante\ExclusionParticipanteController@store')->name('exclusion.excluir');
  Route::get('ProcesosEspeciales/ExclusionParticipante/Aprobar', 'ExclusionParticipante\ExclusionParticipanteController@aprobar')->name('exclusion.aprobar');
  Route::get('ProcesosEspeciales/ExclusionParticipante/Denegar', 'ExclusionParticipante\ExclusionParticipanteController@denegar')->name('exclusion.denegar');
  /* FIN  EXCLUSION PARTICIPANTE  */

});