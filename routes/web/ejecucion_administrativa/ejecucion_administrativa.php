<?php

	/* Inicio de Procesos (estado del sistema preinicio) */
	// Route::resource('EjecucionAdministrativa/EstadoSistemaPreinicio', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController', [
	//   'anyData'  => 'InicioProcesos.inicio_procesos',
	//   'getIndex' => 'InicioProcesos',
	// ]);
	/* fin Inicio de Procesos */

	/* Inicio de Sistema (estado del sistema preinicio) */
	Route::get('EjecucionAdministrativa/InicioProcesos', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController@getIndex')->name('EjecucionAdministrativa.InicioProcesos');
	Route::get('EjecucionAdministrativa/InicioProcesos/IniciarBD', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController@iniciar_bd')->name('EjecucionAdministrativa.InicioProcesos.IniciarDB');
	Route::get('EjecucionAdministrativa/InicioProcesos/IniciarJobs', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController@iniciar_jobs')->name('EjecucionAdministrativa.InicioProcesos.IniciarJobs');
	Route::get('EjecucionAdministrativa/InicioProcesos/CrearEstructura', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController@crearEstructura')->name('EjecucionAdministrativa.InicioProcesos.CrearEstructura');
	//Route::get('EjecucionAdministrativa/InicioProcesos/IniciarServidor', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController@iniciar_servidor')->name('EjecucionAdministrativa.InicioProcesos.IniciarServidor');
	Route::get('EjecucionAdministrativa/InicioProcesos/ActualizarServidor/{id}', 'EjecucionAdministrativa\InicioProcesos\InicioProcesosController@actualizar_servidor')->name('EjecucionAdministrativa.InicioProcesos.ActualizarServidor');
	/* fin Inicio de Sistema */

	/** Monitoreo de Procesos */
	Route::get('EjecucionAdministrativa/MonitoreoProcesos', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@getIndex')->name('MonitoreoProcesos');
	Route::post('monitoreo_procesos/estado', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@cambiar_estado');
	Route::get('EjecucionAdministrativa/MonitoreoProcesos/Consultar', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@consultar');
	/*rutas ajax monitoreo de discos */
	Route::get('EjecucionAdministrativa/MonitoreoProcesos/diskc', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@diskc');

	Route::get('EjecucionAdministrativa/MonitoreoProcesos/diskx', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@diskx');

	Route::get('EjecucionAdministrativa/MonitoreoProcesos/disky', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@disky');

	Route::get('EjecucionAdministrativa/MonitoreoProcesos/diskf', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@diskf');

	Route::get('EjecucionAdministrativa/MonitoreoProcesos/objetosInvalidosBD', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@objetosInvalidosBD');
	Route::get('EjecucionAdministrativa/MonitoreoProcesos/compilarBD', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@compilarBD');
	Route::get('EjecucionAdministrativa/MonitoreoProcesos/ActivarJob/{codigo}', 'EjecucionAdministrativa\MonitoreoProcesos\MonitoreoProcesosController@activar_job');
	/* fin disk */


	/** Recepcion Archivos (extencion de franjas) */
	//Route::resource('modificacion_franjas', 'EjecucionAdministrativa\ModificacionFranjas\ModificacionFranjasController');
	Route::resource('EjecucionAdministrativa/ModificacionFranja', 'EjecucionAdministrativa\ModificacionFranjas\ModificacionFranjasController');

	/** Cierre de Procesos */
		Route::get('EjecucionAdministrativa/CierreProcesos', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@getIndex');
  	Route::get('EjecucionAdministrativa/CierreProcesos/DetenerCMD', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@detener_cmd');
  	Route::get('EjecucionAdministrativa/CierreProcesos/Detener', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@detener_jobs');
  	Route::get('EjecucionAdministrativa/CierreProcesos/Comprimir', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@comprimir');
		Route::get('EjecucionAdministrativa/CierreProcesos/Limpiar', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@limpiar');
  	Route::get('EjecucionAdministrativa/CierreProcesos/GenerarHistorico', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@generar_historico');

  	Route::get('EjecucionAdministrativa/CierreProcesos/RespaldoBD', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@respaldo_bd');

  	Route::get('EjecucionAdministrativa/CierreProcesos/InicioBD', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@inicio_db');
  	Route::get('EjecucionAdministrativa/CierreProcesos/CerrarServidor', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@cerrar_servidor');
  	Route::get('EjecucionAdministrativa/CierreProcesos/VerificarServidor', 'EjecucionAdministrativa\CierreProcesos\CierreProcesosController@verificar_servidor');

	/** Stand By */
  	Route::get('EjecucionAdministrativa/StandBy', 'EjecucionAdministrativa\StandBy\StandByController@index');
    Route::get('EjecucionAdministrativa/Standby/DetenerCMD', 'EjecucionAdministrativa\Standby\StandByController@detener_cmd');
    Route::get('EjecucionAdministrativa/Standby/Detener', 'EjecucionAdministrativa\Standby\StandByController@detener_jobs');
    Route::get('EjecucionAdministrativa/Standby/IniciarJobs', 'EjecucionAdministrativa\Standby\StandByController@iniciar_jobs')->name('EjecucionAdministrativa.InicioProcesos.IniciarJobss');
