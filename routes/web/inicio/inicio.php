<?php

// Controllers Within The "App\Http\Controllers\Inicio" Namespace
Route::namespace('Inicio')->group(function () {
  Route::get('/Inicio', 'InicioController@index')->name('inicio');
  Route::get('Inicio/creditod', 'InicioController@creditod');
  Route::get('Inicio/creditodevuelta', 'InicioController@creditodd');
  Route::get('Inicio/cheques', 'InicioController@cheques');
  Route::get('Inicio/chequesdevuelta', 'InicioController@chequesd');
  Route::get('Inicio/domiciliaciones', 'InicioController@domiciliaciones');
  Route::get('Inicio/domiciliacionesdevueltas', 'InicioController@domiciliacionesd');
});
