<?php

// inicio de lista transaccional
//------------------------------------------------------------------------------ listado general
  Route::get('Reportes/Transaccional/ListadoGeneral','Reportes\Transaccional\ListadoGeneral\ListadoTransaccionalController@listadoTransaccional');
  Route::get('Reportes/Transaccional/ListadoGeneral/Reporte/{tipoReporte}',
    'Reportes\Transaccional\ListadoGeneral\ListadoTransaccionalController@reporteListadoTransaccional');
//------------------------------------------------------------------------------ R0256
  Route::get('Reportes/Transaccional/R0256','Reportes\Transaccional\ExtensionFranja\ExtensionFranjaController@getIndex');
  Route::get('Reportes/Transaccional/R0256/Reporte/{cod_banco}/{codigo_operacion}/{fecha}/{tipoReporte}',
    'Reportes\Transaccional\ExtensionFranja\ExtensionFranjaController@reporteExtensionFranja');
//------------------------------------------------------------------------------ R0257
  Route::get('Reportes/Transaccional/R0257', 'Reportes\Transaccional\IntercambioImagenesBilateral\IntercambioImagenesBilateralController@getIndex');
  Route::get('Reportes/Transaccional/R0257/Reporte/{cod_banco_emisor}/{cod_banco_receptor}/{desde}/{hasta}/{tipoReporte}',
    'Reportes\Transaccional\IntercambioImagenesBilateral\IntercambioImagenesBilateralController@reporteIntercambioImagenesBilateral');
//------------------------------------------------------------------------------ R0258
  Route::get('Reportes/Transaccional/R0258', 'Reportes\Transaccional\IntercambioImagenesHora\IntercambioImagenesHoraController@getIndex');
  Route::get('Reportes/Transaccional/R0258/Reporte/{cod_banco_emisor}/{cod_banco_receptor}/{desde}/{hasta}/{cod_hora_desde}/{cod_hora_hasta}/{tipoReporte}',
    'Reportes\Transaccional\IntercambioImagenesHora\IntercambioImagenesHoraController@reporteIntercambioImagenesHora');
//------------------------------------------------------------------------------ R0264
  Route::get('Reportes/Transaccional/R0264', 'Reportes\Transaccional\ResumenCompensacionOperaciones\ResumenCompensacionOperacionesController@getIndex');
  Route::get('Reportes/Transaccional/R0264/ReporteEXCEL/{Pparticipantes}/{Fechai}/{Fechaf}/{Poperaciones}',
    'Reportes\Transaccional\ResumenCompensacionOperaciones\ResumenCompensacionOperacionesController@ResumenCompensacionEXCEL');
  Route::get('Reportes/Transaccional/R0264/ReportePDF/{Pparticipantes}/{Fechai}/{Fechaf}/{Poperaciones}',
    'Reportes\Transaccional\ResumenCompensacionOperaciones\ResumenCompensacionOperacionesController@ResumenCompensacionPDF');
//------------------------------------------------------------------------------ R0265
  Route::get('Reportes/Transaccional/R0265', 'Reportes\Transaccional\HojaCompensacion\HojaCompensacionController@getIndex');
  Route::get('Reportes/Transaccional/R0265/Reporte/{cod_banco}/{cod_operacion}/{fe_desde}/{fe_hasta}/{tipoReporte}',
    'Reportes\Transaccional\HojaCompensacion\HojaCompensacionController@reporteHojaCompensacion');
//------------------------------------------------------------------------------ R0266
  // Route::resource('Reportes/Transaccional/R0266', 'Reportes\ResumenEntrada\ResumenEntradaController');
//------------------------------------------------------------------------------ R0268
  Route::get('Reportes/Transaccional/R0268', 'Reportes\Transaccional\TiempoLiquidacion\TiempoLiquidacionController@getIndex');
  Route::get('Reportes/Transaccional/R0268/Reporte/{num_liquidacion}/{estatus}/{fe_desde}/{fe_hasta}/{tipoReporte}',
    'Reportes\Transaccional\TiempoLiquidacion\TiempoLiquidacionController@reporteTiempoLiquidacion');
//------------------------------------------------------------------------------ R0272
  Route::get('Reportes/Transaccional/R0272', 'Reportes\Transaccional\EstadoParticipantes\EstadoParticipantesController@getIndex');
  Route::get('Reportes/Transaccional/R0272/Reporte/{estado}/{banco}/{tipoReporte}',
    'Reportes\Transaccional\EstadoParticipantes\EstadoParticipantesController@reporteEstadoParticipantes');
//------------------------------------------------------------------------------ R0273
  Route::get('Reportes/Transaccional/R0273', 'Reportes\Transaccional\TiempoProcesoArchivo\TiempoProcesoArchivoController@getIndex');
  Route::get('Reportes/Transaccional/R0273/ReporteICOM/{cod_banco}/{fecha}/{tipoReporte}',
    'Reportes\Transaccional\TiempoProcesoArchivo\TiempoProcesoArchivoController@reporteTiempoProcesoArchivoICOM');
  Route::get('Reportes/Transaccional/R0273/ReporteICOMA/{cod_banco}/{fecha}/{tipoReporte}',
    'Reportes\Transaccional\TiempoProcesoArchivo\TiempoProcesoArchivoController@reporteTiempoProcesoArchivoICOMA');
// fin de lista transaccional

// inicio de lista historico
//------------------------------------------------------------------------------ listado general
  Route::get('Reportes/Historico/ListadoGeneral','Reportes\Historico\ListadoGeneral\ListadoHistoricoController@listadoHistorico');
  Route::get('Reportes/Historico/ListadoGeneral/Reporte/{tipoReporte}',
    'Reportes\Historico\ListadoGeneral\ListadoHistoricoController@reporteListadoHistorico');
//------------------------------------------------------------------------------ R0251
  Route::get('Reportes/Historico/R0251','Reportes\Historico\ArchivosHora\ArchivosHoraController@getIndex');
  Route::get('Reportes/Historico/R0251/Reporte/{cod_banco}/{cod_archivo}/{fe_desde}/{fe_hasta}/{desde}/{hasta}/{tipoReporte}/{tipoBusqueda}',
    'Reportes\Historico\ArchivosHora\ArchivosHoraController@reporteArchivosHora');
//------------------------------------------------------------------------------ R0252
  // Route::resource('comparativo_compensacion','Reportes\ComparativoCompensacion\ComparativoCompensacionController');
  // Route::resource('Reportes/Historico/R0252', 'Reportes\ComparativoCompensacion\ComparativoCompensacionController');
  // Route::controller('Reporte/ComparativoCompensacion','Reportes\ComparativoCompensacion\ComparativoCompensacionController', [
  //   'anyData'  => 'ComparativoCompensacion.comparativo_compensacion',
  //   'index' => 'ComparativoCompensacion',]);
  // Route::get('ReporteComparativoCompensacion/{cod_banco}/{cod_operacion}/{fe_ini_pe1}/{fe_fin_pe1}/{fe_ini_pe2}/{fe_fin_pe2}/{tipoReporte}/{tipoBusqueda}',
  //   'Reportes\ComparativoCompensacion\ComparativoCompensacionController@ReporteComparativoCompensacion');
//------------------------------------------------------------------------------ R0253
  // Route::get('Reportes/Historico/R0253', 'Reportes\OperacionesEscalaMontoFacturar\OperacionesEscalaMontoFacturarController@index');
  // Route::get('Reportes/Historico/R0253/ReporteCobranzaOperacionesAltoValor/{banco}/{banco_r}/{codigo_operacion}/{id_monto_desde}/{id_monto_hasta}/{desde}/{hasta}/{tipoReporte}',
  //   'Reportes\OperacionesEscalaMontoFacturar\OperacionesEscalaMontoFacturarController@ReporteCobranzaOperacionesAltoValor');
//------------------------------------------------------------------------------ R0254
  // Route::resource('operaciones_escala_montos', 'Reportes\OperacionesEscalaMontos\OperacionesEscalaMontosController');
  // Route::controller('Reportes/Historico/R0254', 'Reportes\OperacionesEscalaMontos\OperacionesEscalaMontosController', [
  //   'anyData'  => 'OperacionesEscalaMontos.operaciones_escala_montos',
  //   'getIndex' => 'OperacionesEscalaMontos',]);
  // Route::get('ReporteOperacionesPorEscalaMontos/{cod_banco}/{fecha_desde}/{fecha_hasta}/{id_monto_desde}/{id_monto_hasta}/{codigo_operacion}/{tipoReporte}',
  //   'Reportes\OperacionesEscalaMontos\OperacionesEscalaMontosController@ReporteOperacionesPorEscalaMontos');
//------------------------------------------------------------------------------ R0255
  // Route::get('Reportes/Historico/R0255', 'Reportes\OperacionesExoneradasAltoValor\OperacionesExoneradasAltoValorController@index');
  // Route::get('Reportes/Historico/R0255/ReporteOperacionesExoneradas/{banco}/{banco_r}/{codigo_operacion}/{desde}/{hasta}/{tipoReporte}/{tipoBusqueda}',
  //   'Reportes\OperacionesExoneradasAltoValor\OperacionesExoneradasAltoValorController@ReporteOperacionesExoneradas');
//------------------------------------------------------------------------------ R0256
  // Route::resource('extension_franja_histo','Reportes\ExtensionFranjaHisto\ExtensionFranjaHistoController');
  // Route::controller('Reportes/Historico/R0256','Reportes\ExtensionFranjaHisto\ExtensionFranjaHistoController', [
  //   'anyData'  => 'ExtensionFranjaHisto.extension_franja_histo',
  //   'getIndex' => 'ExtensionFranjaHisto',]);
  // Route::get('ReporteExtensionFranjaHisto/{cod_banco}/{codigo_operacion}/{fecha_desde}/{fecha_hasta}/{tipoReporte}',
  //   'Reportes\ExtensionFranjaHisto\ExtensionFranjaHistoController@ReporteExtensionFranjaHisto');
//------------------------------------------------------------------------------ R0257
  // Route::get('Reportes/Historico/R0257', 'Reportes\IntercambioImagenesBilateralesH\IntercambioImagenesBilateralesHController@getindex');
  // Route::get('Reportes/Historico/R0257/ReporteIntercambioImagenesBilateralHisto/{cod_banco_emisor}/{cod_banco_receptor}/{desde}/{hasta}/{tipoReporte}',
  //   'Reportes\IntercambioImagenesBilateralesH\IntercambioImagenesBilateralesHController@ReporteIntercambioImagenesBilateralHisto');
//------------------------------------------------------------------------------ R0258
  // Route::resource('Reportes/Historico/R0258', 'Reportes\IntercambioImagenesHoraH\IntercambioImagenesHoraHController');
  // Route::get('Reportes/Historico/R0258/Excel/{cod_banco_emisor}/{cod_banco_receptor}/{desde}/{hasta}/{cod_hora_desde}/{cod_hora_hasta}',
  //   'Reportes\IntercambioImagenesHoraH\IntercambioImagenesHoraHController@excel');
  // Route::get('Reportes/Historico/R0258/Pdf/{cod_banco_emisor}/{cod_banco_receptor}/{desde}/{hasta}/{cod_hora_desde}/{cod_hora_hasta}',
  //   'Reportes\IntercambioImagenesHoraH\IntercambioImagenesHoraHController@pdf');
//------------------------------------------------------------------------------ R0259
  // Route::get('Reportes/Historico/R0259','Reportes\IntercambioBilateral\IntercambioBilateralRController@intercambio_bilateral_vista');
  // Route::get('Reportes/intercambioBilateralPDF/{Pparticipantes}/{Pcontraparte}/{Pfechai}/{Pfechaf}/{Poperaciones}',
  //   'Reportes\IntercambioBilateral\IntercambioBilateralRController@intercambioBilateralPDF');
  // Route::get('Reportes/intercambioBilateralEXCEL/{Pparticipantes}/{Pcontraparte}/{Pfechai}/{Pfechaf}/{Poperaciones}',
  //   'Reportes\IntercambioBilateral\IntercambioBilateralRController@intercambioBilateralEXCEL');
//------------------------------------------------------------------------------ R0260
  // Route::get('Reportes/Historico/R0260','Reportes\OperacionesHora\OperacionesHoraController@OperacionesHoraVista');
  // Route::get('Reportes/OperacionesHoraPDF/{Pparticipantes}/{Poperaciones}/{Pfecha_desde}/{Pfecha_hasta}/{Phora_desde}/{Phora_hasta}/{tipoBusqueda}',
  //   'Reportes\OperacionesHora\OperacionesHoraController@OperacionesHoraPDF');
  // Route::get('Reportes/OperacionesHoraEXCEL/{Pparticipantes}/{Poperaciones}/{Pfecha_desde}/{Pfecha_hasta}/{Phora_desde}/{Phora_hasta}/{tipoBusqueda}',
  //   'Reportes\OperacionesHora\OperacionesHoraController@OperacionesHoraEXCEL');
//------------------------------------------------------------------------------ R0261
  // Route::resource('porcentaje_participacion','Reportes\PorcentajeParticipacion\PorcentajeParticipacionController');
  // Route::controller('Reportes/Historico/R0261','Reportes\PorcentajeParticipacion\PorcentajeParticipacionController', [
  //   'anyData'=>'PorcentajeParticipacion.porcentaje_participacion',
  //   'getIndex'=>'PorcentajeParticipacion']);
  // Route::get('ReportePorcentajeParticipacion/{cod_banco}/{codigo_operacion}/{desde}/{hasta}/{tipoReporte}',
  //   'Reportes\PorcentajeParticipacion\PorcentajeParticipacionController@ReportePorcentajeParticipacion');
//------------------------------------------------------------------------------ R0262
  // Route::get('Reportes/Historico/R0262','Reportes\PosicionesLiquidadas\PosicionesLiquidadasRController@posiciones_liquidadas_vista');
  // Route::get('Reportes/PosicionesLiquidadasEXCEL/{Pparticipantes}/{Fechai}/{Fechaf}',
  //   'Reportes\PosicionesLiquidadas\PosicionesLiquidadasRController@PosicionesLiquidadasEXCEL');
//------------------------------------------------------------------------------ R0263
  // Route::get('Reportes/Historico/R0263','Reportes\ResultadosNetosCompensacion\ResultadosNetosCompensacionController@ResultadosNetosVista');
  // Route::get('Reportes/ResultadosNetosPDF/{Pparticipantes}/{Fechai}/{Fechaf}/{Poperaciones}',
  //   'Reportes\ResultadosNetosCompensacion\ResultadosNetosCompensacionController@ResultadosNetosPDF');
  // Route::get('Reportes/ResultadosNetosEXCEL/{Pparticipantes}/{Fechai}/{Fechaf}/{Poperaciones}',
  //   'Reportes\ResultadosNetosCompensacion\ResultadosNetosCompensacionController@ResultadosNetosEXCEL');
//------------------------------------------------------------------------------ R0264
  // Route::resource('Reportes/Historico/R0264', 'Reportes\ResumenCompensacionTipoOperacionHisto\ResumenCompensacionToperacionHistoController@resumen_compesacion_t_operaciones_Histo');
  // Route::get('Reportes/ResumenCompensacionHistoEXCEL/{Pparticipantes}/{Fechai}/{Fechaf}/{Poperaciones}',
  //   'Reportes\ResumenCompensacionTipoOperacionHisto\ResumenCompensacionToperacionHistoController@ResumenCompensacionHistoEXCEL');
  // Route::get('Reportes/ResumenCompensacionHistoPDF/{Pparticipantes}/{Fechai}/{Fechaf}/{Poperaciones}',
  //   'Reportes\ResumenCompensacionTipoOperacionHisto\ResumenCompensacionToperacionHistoController@ResumenCompensacionHistoPDF');
//------------------------------------------------------------------------------ R0265
  // Route::resource('Reportes/Historico/R0265', 'Reportes\HojaCompensacionHisto\HojaCompensacionHistoController');
  // Route::controller('Reportes/Historico/R0265','Reportes\HojaCompensacionHisto\HojaCompensacionHistoController', [
  //   'getIndex'=>'HojaCompensacionHisto']);
  // Route::get('ReporteHojaCompensacionHisto/{cod_banco}/{cod_operacion}/{fe_desde}/{fe_hasta}/{tipoReporte}',
  //   'Reportes\HojaCompensacionHisto\HojaCompensacionHistoController@ReporteHojaCompensacionHisto');
//------------------------------------------------------------------------------ R0267
  // Route::resource('Reportes/Historico/R0267', 'Reportes\SubTiposCreditos\SubTiposCreditosController');
  // Route::controller('Reporte/SubTiposCreditos','Reportes\SubTiposCreditos\SubTiposCreditosController', [
  //   'anyData'  => 'SubTiposCreditos.sub_tipos_creditos',
  //   'Index' => 'SubTiposCreditos',]);
  // Route::get('ReporteSubTiposCreditos/{cod_banco}/{subtipo_credito}/{fe_desde}/{fe_hasta}/{tipoReporte}/{tipoBusqueda}',
  //   'Reportes\SubTiposCreditos\SubTiposCreditosController@ReporteSubTiposCreditos');
//------------------------------------------------------------------------------ R0268
  // Route::get('Reportes/Historico/R0268', 'Reportes\TiempoLiquidacionHistorico\TiempoLiquidacionHistoricoController@getIndex');
  // Route::get('Reportes/Historico/R0268/ReporteTiempoLiquidacionHisto/{num_liquidacion}/{estatus}/{fe_desde}/{fe_hasta}/{tipoReporte}',
  //   'Reportes\TiempoLiquidacionHistorico\TiempoLiquidacionHistoricoController@ReporteTiempoLiquidacionHisto');
//------------------------------------------------------------------------------ R0269
  // Route::resource('Reportes/Historico/R0269', 'Reportes\TendenciasIBP\TendenciasIBPController');
  // Route::controller('Reportes/Historico/R0269/TendenciasIBP','Reportes\TendenciasIBP\TendenciasIBPController', [
  //   'Index'=>'Reportes/TendenciasIBP']);
  // Route::get('Reportes/Historico/R0269/ReporteTendenciasIBP/{cod_banco}/{fe_desde}/{fe_hasta}/{tipoReporte}',
  //   'Reportes\TendenciasIBP\TendenciasIBPController@ReporteTendenciasIBP');
//------------------------------------------------------------------------------ R0270
  // Route::resource('tendencia_medio_pago','Reportes\TendenciaMedioPago\TendenciaMedioPagoController');
  // Route::controller('Reportes/Historico/R0270','Reportes\TendenciaMedioPago\TendenciaMedioPagoController', [
  //   'getIndex'=>'TendenciaMedioPago',
  //   'anyData' => 'TendenciaMedioPago.tendencia_medio_pago']);
  // Route::get('ReporteTendenciaMedioPago/{fe_desde}/{fe_hasta}/{tipoReporte}',
  //   'Reportes\TendenciaMedioPago\TendenciaMedioPagoController@ReporteTendenciaMedioPago');
//------------------------------------------------------------------------------ R0271
  // Route::resource('Reportes/Historico/R0271', 'Reportes\InicioTransmisiones\InicioTransmisionesController');
  // Route::controller('Reportes/Historico/R0271','Reportes\InicioTransmisiones\InicioTransmisionesController', [
  //   'anyData'  => 'InicioTransmisiones.inicio_transmisiones',
  //   'getIndex' => 'InicioTransmisiones',]);
  // Route::get('ReporteInicioTransmision/{cod_banco}/{fe_desde}/{fe_hasta}/{tipoReporte}',
  //   'Reportes\InicioTransmisiones\InicioTransmisionesController@ReporteInicioTransmision');
//------------------------------------------------------------------------------ R0275
  // Route::get('Reportes/Historico/R0275', 'Reportes\OperacionesAltoValor\OperacionesAltoValorController@index');
  // Route::get('Reportes/Historico/R0275/ReporteOperacionesAltoValor/{banco}/{codigo_operacion}/{desde}/{hasta}/{tipoReporte}',
  //   'Reportes\OperacionesAltoValor\OperacionesAltoValorController@ReporteOperacionesAltoValor');
// fin de lista historico

/* Razones tecnicas de rechazos*/
  //Route::resource('Reportes/Listado/R0274','Reportes\RazonRechazo\RazonRechazoController@construccion1');
  // Route::resource('razon_rechazo', 'Reportes\RazonRechazo\RazonRechazoController');
  // Route::controller('Reportes/Listado/R0274', 'Reportes\RazonRechazo\RazonRechazoController', [
  //   'anyData'  => 'RazonRechazo.razon_rechazo',
  //   'getIndex' => 'RazonRechazo',]);
  // Route::get('ExcelRazonRechazo/{cod_banco}/{tipo_archivo}/{cod_rechazo}', 'Reportes\RazonRechazo\RazonRechazoController@ExcelRazonRechazo');
/* Fin Razones tecnicas de rechazos*/
