<?php

/* Participantes */
Route::get('Configuracion/Participantes/Inactivos', 'Configuracion\Participantes\ParticipantesController@inactivos')->name('Participantes.Inactivos');
Route::get('Configuracion/Participantes/Activos', 'Configuracion\Participantes\ParticipantesController@activos')->name('Participantes.Activos');

Route::get('Configuracion/Participantes/Fusionados', 'Configuracion\Participantes\ParticipantesController@fusionados')->name('Participantes.Fusionados');

Route::resource('Configuracion/Participantes', 'Configuracion\Participantes\ParticipantesController')->names([
  'index' => 'Configuracion.Participantes.index',
  'show' => 'Configuracion.Participantes.show',
  'create' => 'Configuracion.Participantes.create',
  'store' => 'Configuracion.Participantes.store',
  'edit' => 'Configuracion.Participantes.edit',
  'update' => 'Configuracion.Participantes.update',
  'destroy' => 'Configuracion.Participantes.destroy',
]);
/* Fin Participantes */


/* Calendario */

/* Semana de Compensacion  */
Route::resource('Configuracion/Calendario/SemanaCompensacion', 'Configuracion\Calendario\SemanaCompensacion\SemanaCompensacionController');
Route::get('Configuracion/Calendario/SemanaCompensacion', 'Configuracion\Calendario\SemanaCompensacion\SemanaCompensacionController@getIndex')->name('SemanaCompensacion');
/* Fin Semana de Compensacion  */

/* Feriado */
Route::resource('Configuracion/Calendario/DiasExcepcionales/NoLaborables', 'Configuracion\Calendario\DiasExcepcionales\NoLaborables\NoLaborablesController');
Route::get('Configuracion/Calendario/DiasExcepcionales/NoLaborables', 'Configuracion\Calendario\DiasExcepcionales\NoLaborables\NoLaborablesController@getIndex')->name('NoLaborables');
/* Fin Feriado */

/* laborables */
Route::resource('Configuracion/Calendario/DiasExcepcionales/Laborables', 'Configuracion\Calendario\DiasExcepcionales\Laborables\LaborablesController');
Route::get('Configuracion/Calendario/DiasExcepcionales/Laborables', 'Configuracion\Calendario\DiasExcepcionales\Laborables\LaborablesController@getIndex')->name('Laborables');
/* fin laborables */

/* Fin Calendario */


  /* Medios de Pagos */

  /* Productos */
  Route::resource('Configuracion/MediosPagos/Productos', 'Configuracion\MedioPago\ProductoController');
  /* Fin Productos */

  /* Sub-Productos */
  Route::resource('Configuracion/MediosPagos/SubProductos', 'Configuracion\MedioPago\SubProductoController');

  /* Fin Sub-Productos */

  /* Razones de Devolucion */
  Route::resource('Configuracion/MediosPagos/RazonesDevolucion', 'Configuracion\MedioPago\RazonesDevolucionController');

  Route::get('autocomplete', 'Configuracion\MedioPago\RazonesDevolucionController@autocomplete');
  Route::get('autocompleteCodigo', 'Configuracion\MedioPago\RazonesDevolucionController@autocompleteCodigo');
  /* Fin Razones de Devolucion */
  /* Fin Medios de Pagos */

  /* Monedas */

  /* Codigo Iso */
  Route::resource('Configuracion/Monedas/CodigoISO', 'Configuracion\Monedas\CodigoISO\CodigoISOController');
  /* Fin Codigo Iso */

  /* Tipos*/
  Route::resource('Configuracion/Monedas/Tipos', 'Configuracion\Monedas\Tipos\TiposController')->names([
    'index' => 'Monedas.Tipos.index',
    'show' => 'Monedas.Tipos.show',
    'create' => 'Monedas.Tipos.create',
    'store' => 'Monedas.Tipos.store',
    'edit' => 'Monedas.Tipos.edit',
    'update' => 'Monedas.Tipos.update',
    'destroy' => 'Monedas.Tipos.destroy',
  ]);
  /* Fin Tipos */


  /* Periodo de Compensacion */

  /* Periodos */
  Route::resource('Configuracion/PeriodoCompensacion/Periodos', 'Configuracion\PeriodoCompensacion\Periodos\PeriodoController');
  Route::get('Configuracion/PeriodoCompensacion/Periodos', 'Configuracion\PeriodoCompensacion\Periodos\PeriodoController@getindex')->name('Periodos');
  /* Fin Periodos */

  /* Intervalos de Liquidacion */
  Route::resource('Configuracion/PeriodoCompensacion/IntervalosLiquidacion', 'Configuracion\PeriodoCompensacion\IntervalosLiquidacion\IntervalosLiquidacionController');
  Route::get('Configuracion/PeriodoCompensacion/IntervalosLiquidacion', 'Configuracion\PeriodoCompensacion\IntervalosLiquidacion\IntervalosLiquidacionController@getIndex')->name('IntervalosLiquidacion');
  /* Fin Intervalos de Liquidacion */

  /* Fin Periodo de Compensacion */


  /* Contacto */

  /* Participante */
  Route::resource('Configuracion/Contactos/Participantes', 'Configuracion\Contacto\ParticipantesController');
  Route::get('Configuracion/Contactos/Participantes', 'Configuracion\Contacto\ParticipantesController@getIndex')->name('ContactosParticipantes');
  /* Fin Participante */

  /* Tipos */
  Route::resource('Configuracion/Contactos/Tipos', 'Configuracion\Contacto\TipoContactoController');
  Route::get('Configuracion/Contactos/Tipos', 'Configuracion\Contacto\TipoContactoController@getIndex')->name('TiposContactos');

  /* Fin Tipos */
  /* Fin Contacto */



  /* ROUTE CONTACTOS ENTES INICIO */
  Route::resource('contactos_entes', 'Configuracion\Contacto\ContactoEnteController');
  Route::get('ContactosEntes', 'Configuracion\Contacto\ContactoEnteController@getIndex');
  /* ROUTE CONTACTOS ENTES FIN */


  /* ROUTE DIAS EXCEPCIONALES INICIO */
  Route::resource('dias_excepcionales', 'Configuracion\Calendario\DiasExcepcionalesController');
  Route::get('dias_excepcionales', 'Configuracion\Calendario\DiasExcepcionalesController@store');
  Route::get('dias_excepcionales','Configuracion\Calendario\DiasExcepcionalesController@index');
  Route::get('DiasExcepcionales', 'Configuracion\Calendario\DiasExcepcionalesController@getIndex');
  /* ROUTE DIAS EXCEPCIONALES FIN */



  /* ROUTE ENTES INICIO */
  Route::resource('entes', 'Configuracion\Ente\EnteController');
  Route::get('autocompleteRif', 'Configuracion\Ente\EnteController@autocompleteRif');
  Route::get('autocompleteNombre', 'Configuracion\Ente\EnteController@autocompleteNombre');
  Route::get('Entes', 'Configuracion\Ente\EnteController@getIndex');
  /* ROUTE ENTES FIN */



  /* ROUTE MONEDA OPERACION INICIO */
  Route::resource('monedas_operaciones', 'Configuracion\Moneda\MonedaOperacionController');

  Route::get('MontosOperaciones', 'Configuracion\Moneda\MonedaOperacionController@getIndex');
  /* ROUTE MONEDA OPERACION FIN */


  /* ROUTE PARAMETROS DE SISTEMA INICIO */
  Route::resource('parametros_sistema', 'Configuracion\ParametroSistema\ParametroSistemaController');

  Route::get('ParametroSistema', 'Configuracion\ParametroSistema\ParametroSistemaController@getIndex');
  /* ROUTE PARAMETROS DE SISTEMA FIN */

  /* ROUTE PARAMETROS DE SISTEMA INICIO */
  Route::resource('parametros_sistema_conciliador', 'Configuracion\ParametroSistema\ParametroSistemaConciliadorController');

  Route::get('ParametroSistemaConciliador', 'Configuracion\ParametroSistema\ParametroSistemaConciliadorController@getIndex');
  /* ROUTE PARAMETROS DE SISTEMA FIN */

  /*Cobranza*/

  /* INICIO COBRANZA RECUPERACIÓN DE COSTOS */
  Route::resource('Configuracion/Cobranza/RecuperacionCostos', 'Configuracion\Cobranza\RecuperacionCostos\RecuperacionCostosController');
  Route::post('Configuracion/Cobranza/RecuperacionCostos/Multiple/{dir}','Configuracion\Cobranza\RecuperacionCostos\RecuperacionCostosController@ajaxActualizar');
  /* FIN COBRANZA RECUPERACIÓN DE COSTOS */

  /* INICIO COBRANZA LIMITE BAJO VALOR */
  Route::resource('Configuracion/Cobranza/AltoValor/LimiteBajoValor', 'Configuracion\Cobranza\AltoValor\LimiteBajoValor\LimiteBajoValorController');
  /* FIN COBRANZA LIMITE BAJO VALOR */


  /* INICIO COBRANZA EXONERACIÓN */
  Route::resource('Configuracion/Cobranza/AltoValor/Exoneracion', 'Configuracion\Cobranza\AltoValor\Exoneracion\ExoneracionController');
  /* FIN COBRANZA EXONERACIÓN */

  /*Fin Cobranza*/


  /* EMPRESAS ORDENANTES */
    Route::resource('Configuracion/EmpresasOrdenantes', 'Configuracion\EmpresasOrdenantes\EmpresasOrdenantesController');
  /* FIN EMPRESAS ORDENANTES  */


  /* ADMINISTRACION DE TABLAS */

  Route::resource('Configuracion/AdministracionTablas/ParametrosSolucion', 'Configuracion\AdministracionTablas\ParametrosSistema\ParametrosSistemaController');

  //Route::resource('Configuracion/AdministracionTablas/ValidacionesSolucion', 'Configuracion\AdministracionTablas\ValidacionesSistema\ValidacionesSistemaController');

Route::resource('Configuracion/AdministracionTablas/ValidacionesSolucion', 'Configuracion\AdministracionTablas\ValidacionesSistema\ValidacionesSistemaController');


  /* ADMINISTRACION DE TABLAS  */

  /* NOTIFICACIONES */
  Route::get('Configuracion/Eventos', 'Configuracion\Eventos\EventosController@getIndex')->name('ConfiguracionEventos');
  Route::get('Configuracion/Eventos/{id}', 'Configuracion\Eventos\EventosController@edit')->name('ConfiguracionEventos.edit');
  Route::patch('Configuracion/Eventos/{id}', 'Configuracion\Eventos\EventosController@update')->name('ConfiguracionEventos.update');
  /* NOTIFICACIONES */
