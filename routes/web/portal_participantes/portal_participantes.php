<?php
Route::get('PortalParticipantes/Seguridad/Usuarios/Activos', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@usuariosActivos')->name('PortalParticipantes.Seguridad.Usuarios.Activos');
Route::get('PortalParticipantes/Seguridad/Usuarios/Inactivos', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@usuariosInactivos')->name('PortalParticipantes.Seguridad.Usuarios.Inactivos');
Route::get('PortalParticipantes/Seguridad/Usuarios/Eliminados', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@usuariosEliminados')->name('PortalParticipantes.Seguridad.Usuarios.Eliminados');

Route::get('PortalParticipantes/Seguridad/Usuarios/Crear', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@create')->name('PortalParticipantes.Seguridad.Usuarios.create');
Route::post('PortalParticipantes/Seguridad/Usuarios/Crear/Usuario', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@store')->name('PortalParticipantes.Seguridad.Usuarios.store');
Route::get('PortalParticipantes/Seguridad/Usuarios/Edit/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@edit')->name('PortalParticipantes.Seguridad.Usuarios.edit');
Route::post('PortalParticipantes/Seguridad/Usuarios/Actualizar/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@update')->name('PortalParticipantes.Seguridad.Usuarios.update');
Route::get('PortalParticipantes/Seguridad/Usuarios/Borrar/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@destroy')->name('PortalPArticipantes.Seguridad.Usuarios.destroy');
Route::get('PortalParticipantes/Seguridad/Usuarios/Desactivar/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@desactivate')->name('PortalParticipantes.Seguridad.Usuarios.desactivate');
Route::get('PortalParticipantes/Seguridad/Usuarios/Activate/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@activate')->name('PortalParticipantes.Seguridad.Usuarios.activate');
Route::get('PortalParticipantes/Seguridad/Usuarios/Restaurar/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@restaurar')->name('PortalParticipantes.Seguridad.Usuarios.restaurar');
Route::get('PortalParticipantes/Seguridad/Usuarios/CambiarContrasena/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@change_Password')->name('PortalParticipantes.Seguridad.Usuarios.ChangePassword');
Route::post('PortalParticipantes/Seguridad/Usuarios/ActualizarContrasena/{id}', 'PortalParticipantes\Seguridad\Usuarios\UsuariosPortalParticipantesController@update_Password')->name('PortalParticipantes.Seguridad.Usuarios.UpdatePassword');


Route::resource('PortalParticipantes/Seguridad/Role', 'PortalParticipantes\Seguridad\Roles\RolePortalParticipantesController');
Route::get('PortalParticipantes/Seguridad/Roles/Borrar/{id}', 'PortalParticipantes\Seguridad\Roles\RolePortalParticipantesController@destroy')->name('PortalParticipantes.Seguridad.Roles.destroy');
Route::get('PortalParticipantes/Seguridad/Roles/Clonar/{id}', 'PortalParticipantes\Seguridad\Roles\RolePortalParticipantesController@create_Clon')->name('PortalParticipantes.Seguridad.Roles.createclon');
Route::patch('PortalParticipantes/Seguridad/Roles/Clonar/Store{id}', 'PortalParticipantes\Seguridad\Roles\RolePortalParticipantesController@store_Clon')->name('PortalParticipantes.Seguridad.Roles.updateclon');


Route::get('PortalParticipantes/Seguridad/Reportes/Listado','PortalParticipantes\Seguridad\Reportes\Listado\ListadoPortalParticipantesController@Listado');
