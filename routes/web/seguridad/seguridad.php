<?php
Route::get('Seguridad/Usuarios/Activos', 'Seguridad\Usuarios\UsuariosController@usuariosActivos')->name('Seguridad.Usuarios.Activos');
Route::get('Seguridad/Usuarios/Inactivos', 'Seguridad\Usuarios\UsuariosController@usuariosInactivos')->name('Seguridad.Usuarios.Inactivos');
Route::get('Seguridad/Usuarios/Eliminados', 'Seguridad\Usuarios\UsuariosController@usuariosEliminados')->name('Seguridad.Usuarios.Eliminados');
Route::get('Seguridad/Usuarios/Activos/Crear', 'Seguridad\Usuarios\UsuariosController@create')->name('Seguridad.Usuarios.create');
Route::post('Seguridad/Usuarios/Crear/Usuario', 'Seguridad\Usuarios\UsuariosController@store')->name('Seguridad.Usuarios.store');
Route::get('Seguridad/Usuarios/{status}/Edit/{id}', 'Seguridad\Usuarios\UsuariosController@edit')->name('Seguridad.Usuarios.edit');
Route::post('Seguridad/Usuarios/Actualizar/{id}', 'Seguridad\Usuarios\UsuariosController@update')->name('Seguridad.Usuarios.update');
Route::get('Seguridad/Usuarios/Borrar/{id}', 'Seguridad\Usuarios\UsuariosController@destroy')->name('Seguridad.Usuarios.destroy');
Route::get('Seguridad/Usuarios/Desactivar/{id}', 'Seguridad\Usuarios\UsuariosController@desactivate')->name('Seguridad.Usuarios.desactivate');
Route::get('Seguridad/Usuarios/Activate/{id}', 'Seguridad\Usuarios\UsuariosController@activate')->name('Seguridad.Usuarios.activate');
Route::get('Seguridad/Usuarios/Restaurar/{id}', 'Seguridad\Usuarios\UsuariosController@restaurar')->name('Seguridad.Usuarios.restaurar');
Route::get('Seguridad/Usuarios/{status}/CambiarContrasena/{id}', 'Seguridad\Usuarios\UsuariosController@change_Password')->name('Seguridad.Usuarios.ChangePassword');
Route::post('Seguridad/Usuarios/ActualizarContrasena/{id}', 'Seguridad\Usuarios\UsuariosController@update_Password')->name('Seguridad.Usuarios.UpdatePassword');


Route::resource('Seguridad/Roles', 'Seguridad\Roles\RoleController');
Route::get('Seguridad/Roles/Borrar/{id}', 'Seguridad\Roles\RoleController@destroy')->name('Seguridad.Roles.destroy');
Route::get('Seguridad/Roles/Clonar/{id}', 'Seguridad\Roles\RoleController@create_Clon')->name('Seguridad.Roles.createclon');
Route::patch('Seguridad/Roles/Clonar/Store{id}', 'Seguridad\Roles\RoleController@store_Clon')->name('Seguridad.Roles.updateclon');


Route::get('Seguridad/Reportes/Listado','Seguridad\Reportes\Listado\ListadoController@Listado');
Route::get('Seguridad/Reportes/Listados/Excel', 'Seguridad\Reportes\ListadoController@Excel');
Route::get('Seguridad/Reportes/Listados/Pdf', 'Seguridad\Reportes\ListadoController@Pdf');
