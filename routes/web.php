<?php

/*Route::group(['middleware' => 'web'], function() {
  Route::group(['namespace' => 'Frontend'], function () {
    require (__DIR__ . '/Routes/Frontend/Access.php');
  });
});*/
Auth::routes();


Route::get('/', function () {
    return view('auth.login');
});



Route::group(['middleware' => ['admin']], function() {

  /* INICIO */
  require (__DIR__ . '/web/inicio/inicio.php');
  /* FIN INICIO */


  /* MONITOREO */
  require (__DIR__ . '/web/monitoreo/monitoreo.php');
  /* FIN MONITOREO */

/* administracion IBP*/
require (__DIR__ . '/web/portal_participantes/portal_participantes.php');
/* FINEMPRESAS ORDENANTES */


 /* MENSAJES */
    require (__DIR__ . '/web/mensajes_informativos/mensajes_informativos.php');
  /* FIN MENSAJES */


  /* REPORTES */
    require (__DIR__ . '/web/reportes/reportes.php');
  /* FIN REPORTES */


  /* IMAGENES */
    require (__DIR__ . '/web/imagenes/imagenes.php');
  /* FIN IMAGENES */


  /* SEGURIDAD */
    require (__DIR__ . '/web/seguridad/seguridad.php');
  /* FIN SEGURIDAD */

  /* CONFIGURACION */
    require (__DIR__ . '/web/configuracion/configuracion.php');
  /* FIN CONFIGURACION */


  /* EJECUCION ADMINISTRATIVA */
    require (__DIR__ . '/web/ejecucion_administrativa/ejecucion_administrativa.php');
  /* FIN EJECUCION ADMINISTRATIVA */


 /* PROCESOS ESPECIALES */
    require (__DIR__ . '/web/procesos_especiales/procesos_especiales.php');
  /* FIN PROCESOS ESPECIALES */



  /* GESTION DE LOGS */
    require (__DIR__ . '/web/gestion_logs/gestion_logs.php');
  /* FIN GESTION LOGS */

  /* PERFIL DE USUARIO */
    require (__DIR__ . '/web/perfil_usuario/perfil_usuario.php');
  /* FIN PERFIL DE USUARIOS */

  /* ROUTE ERROR */
  Route::get('500', function() {abort(500);});
  /* FIN ROUTE ERROR */
});
