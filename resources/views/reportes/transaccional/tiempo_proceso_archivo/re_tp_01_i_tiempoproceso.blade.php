{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio Tiempo de Proceso por Archivos--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> R0273</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
        <li class="breadcrumb-item active">R0273</li>
      </ol>
    </div>
  </div>
</div>
{{-- @section('title') --}}
<div class="row">
    <div class="col-sm-12">
        <div class="card card-success card-outline">
            <div class="card-header with-border" style="background-color: #e8f9ea;">
                <h4 class="card-title">
                    <b>R0273 - Tiempo de procesos por archivo</b>
                </h4>
            </div>
        </div>
    </div>
</div>
{{-- @endsection --}}
@endsection @section('content')

{{--Inicio de la seccion de Filtro--}}

<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-warning card-outline">
            <div class="card-header with-border">
                <h3 class="card-title">Filtro</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <div class="card-body">
                <form role="form" class="form-row">
                    <div class="col-sm-6">
                        <div class="form-group form-row">
                            {!!Form::label('Participante', 'Participante', array('class' => 'col-sm-3
                            col-form-label'))!!}
                            <div class="col-sm-9">
                                {!!Form::select('cod_banco',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control'])
                                !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-row">
                            {!!Form::label('Fecha', 'Fecha', array('class' => 'col-sm-3 col-form-label'))!!}
                            <div class="col-sm-6">
                              <div class="input-group date">
                                  <input
                                      type="text"
                                      class="form-control group-date"
                                      id="fecha"
                                      name="fecha"
                                      readonly="readonly"
                                      value="{!! $fecha !!}">
                                      <div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                      </div>
                              </div>
                            </div>
                        </div>
                    </div>
              </div>
              <div class="card-footer with-border">
                <div class="col-sm-12">
                  <div class="text-right">
                    <input type="hidden" name="busqueda" id="busqueda" value="1"/>
                    <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                  </div>
                </div>
              </form>
            </div>
        </div>
    </div>
    {{--Fin de la seccion de Filtro--}}

    @if ((!empty($Tiempo_archivo_icom)) && (!empty($Tiempo_archivo_icoma)) && ($busqueda))
    @if ((count($Tiempo_archivo_icom)) && (count($Tiempo_archivo_icoma)))

    {{--Inicio de la seccion de Totales--}}

      {{-- Inclusion de codigo para totales - Reporte TiempoProcesoArchivo --}}
      @include('reportes.transaccional.tiempo_proceso_archivo.re_tp_01_i_tiempoprocesototales')

    {{--Fin de la seccion de Totales--}}

    {{--Inicio de la seccion de Listados--}}
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header with-border">
                <!--codigo gui MOD-REPO-1.6-->
                <input type="hidden" name="codigo_gui" value="MOD-REPO-1.6.1" id="codigo_gui">
                <h3 class="card-title">Listados</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.card-header -->

            <!-- form start -->
            <div class="card-body">

              <ul class="nav nav-pills pills-list mb-3" role="tablist">
                <li class="nav-item"><a class="nav-link active" href="#tab1" data-toggle="pill">Recibidos por el sistema</a></li>
                <li class="nav-item"><a class="nav-link" href="#tab2" data-toggle="pill">Respuestas por el sistema</a></li>
              </ul>
                            <!-- form start -->
                            <div class="tab-content">

                              <div id="tab1" class="tab-pane fade active show" role="tabpanel">

                                {{-- Inclusion de codigo para tabla uno - Reporte TiempoProcesoArchivo --}}
                                @include('reportes.transaccional.tiempo_proceso_archivo.re_tp_01_i_tiempoprocesorecibidas')

                                <hr class="col-sm-12">

                                <div class="col-sm-12 text-center">
                                    <a
                                        href="{{url('Reportes/Transaccional/R0273/ReporteICOM',
                                        array('cod_banco' => $cod_banco,
                                              'fecha' => $fecha = Input::get('fecha'),
                                              'tipoReporte' => 'XLS'
                                              )
                                              )}}"
                                        class="btn btn-primary btn-success"
                                        role="button">Exportar a excel</a>

                                     <a
                                        href="{{url('Reportes/Transaccional/R0273/ReporteICOM',
                                        array('cod_banco' => $cod_banco,
                                              'fecha' => $fecha = Input::get('fecha'),
                                              'tipoReporte' => 'PDF'
                                              )
                                              )}}"
                                        class="btn btn-primary btn-danger"
                                        role="button">Exportar a pdf</a>
                                </div>
                              </div>

                                <div id="tab2" class="tab-pane fade" role="tabpanel">

                                  {{-- Inclusion de codigo para tabla dos - Reporte TiempoProcesoArchivo --}}
                                  @include('reportes.transaccional.tiempo_proceso_archivo.re_tp_01_i_tiempoprocesorespuestas')

                                  <hr class="col-sm-12">

                                  <div class="col-sm-12 text-center">
                                          <a
                                              href="{{url('Reportes/Transaccional/R0273/ReporteICOMA',
                                              array('cod_banco' => $cod_banco,
                                                    'fecha' => $fecha = Input::get('fecha'),
                                                    'tipoReporte' => 'XLS'
                                                    )
                                                    )}}"
                                              class="btn btn-primary btn-success"
                                              role="button">Exportar a excel</a>

                                           <a
                                              href="{{url('Reportes/Transaccional/R0273/ReporteICOMA',
                                              array('cod_banco' => $cod_banco,
                                                    'fecha' => $fecha = Input::get('fecha'),
                                                    'tipoReporte' => 'PDF'
                                                    )
                                                    )}}"
                                              class="btn btn-primary btn-danger"
                                              role="button">Exportar a pdf</a>
                                  </div>
                              </div>
                            </div>

            </div>
        </div>
    </div>
    {{--Fin de la seccion de Listados--}}
    @else
    <div class="col-md-12">
        <div class="alert alert-danger" style="text-align: center">No existe información</div>
    </div>
    @endif
    @endif

    @stop
    {{--Fin Tiempo de Proceso por Archivos--}}

@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_filtro_fecha')
@stop
