{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php use App\Models\Configuracion\Banco\Banco; ?>

<table>
  <thead>
    <tr>
      <th style="text-align:left;" colspan="6">Banco Central de Venezuela</th>
      <th style="text-align: right;">Fecha:</th>
      <th>{{ $param['fecha_actual'] }}</th>
    </tr>
    <tr>
      <th style="text-align:left;">Gerencia de Tesorer&#237;a</th>
    </tr>
    <tr>
      <th style="text-align:left;">Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th colspan="8" style="text-align:center;">C&#193;MARA DE COMPENSACI&#211;N REPORTE TIEMPO DE PROCESOS POR ARCHIVO</th>
    </tr>
    <tr>
      <th colspan="8" style="text-align:center;">PER&#205;ODO: {{ $param['fecha'] }}</th>
    </tr>
  </thead>
</table>


         <table>
                              <tr>
                                <th colspan="2" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" ></th>
                                <th colspan="3" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Tratamiento</th>
                                <th colspan="3" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" ></th>
                              </tr>
                              <tr>
                                <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Participante emisor</th>
                                <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Tipo de archivo</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Hora inicio</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Hora fin</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Tiempo de proceso</th>
                                <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Cantidad de lotes</th>
                                <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Cantidad de operaciones</th>
                                <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Monto de operaciones</th>
                              </tr>
                          <tbody>
                            @foreach ($param['TiempoProceso'] as  $registro)
                              <tr>
                             <td style="border-top: 1px solid #f4f4f4;text-align:left;"> {{ Banco::buscaruno($registro->{'banco emisor'})->nombre_completo  }} </td>
                             @if ($registro->{'tipo de archivo'} == 'ICOM1')
                             <td style="border-top: 1px solid #f4f4f4;text-align:center;"> PRESENTADAS </td>
                             @else
                             @if ($registro->{'tipo de archivo'} == 'ICOM2')
                             <td style="border-top: 1px solid #f4f4f4;text-align:center;"> DEVUELTAS</td>
                             @else
                             <td style="border-top: 1px solid #f4f4f4;text-align:center;"> RESPUESTA</td>
                             @endif
                             @endif
                              <td style="border-top: 1px solid #f4f4f4;text-align:center;"> {{ $registro->hor_ini }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:center;" > {{ $registro->hor_fin }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:center;" >{{ substr($registro->tiempo_procesamiento, 10, 9) }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:right;" >{{ number_format($registro->num_lote, 0, $param['monedas']->separador_decimales, $param['monedas']->separador_miles) }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:right;" >{{ number_format($registro->operaciones, 0, $param['monedas']->separador_decimales, $param['monedas']->separador_miles) }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:right;" > {{ number_format($registro->monto_operaciones/100, $param['monedas']->num_decimales, $param['monedas']->separador_decimales, $param['monedas']->separador_miles) }} </td>
                              </tr>
                            @endforeach
                          </tbody>
            </table>
