{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php use App\Models\Configuracion\Banco\Banco; ?>

  {!! Html::style('css/pdf.css') !!}

  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                              <thead>
                              <tr>
                                <th rowspan="2" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Participante emisor</th>
                                <th rowspan="2" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Tipo de archivo</th>
                                <th colspan="3" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Tratamiento</th>
                                <th rowspan="2" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Cantidad de lotes</th>
                                <th rowspan="2" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Cantidad de operaciones</th>
                                <th rowspan="2" style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Monto de operaciones</th>
                              </tr>
                              <tr>
                                <th style="text-align: center;border: 1px solid #000000;" >Hora inicio</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Hora fin</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Tiempo de proceso</th>
                              </tr>
                              </thead>

        <tbody>
                            @foreach ($TiempoProceso as  $registro)
                              <tr>
                             <td style="border-top: 1px solid #f4f4f4;text-align:left;" > {{ Banco::buscaruno($registro->{'banco emisor'})->nombre_completo  }} </td>
                             @if ($registro->{'tipo de archivo'} == 'ICOM1')
                             <td style="border-top: 1px solid #f4f4f4;text-align:center;"> PRESENTADAS </td>
                             @else
                             @if ($registro->{'tipo de archivo'} == 'ICOM2')
                             <td style="border-top: 1px solid #f4f4f4;text-align:center;"> DEVUELTAS</td>
                             @else
                             <td style="border-top: 1px solid #f4f4f4;text-align:center;"> RESPUESTA</td>
                             @endif
                             @endif
                              <td style="border-top: 1px solid #f4f4f4;text-align:center;" > {{ $registro->hor_ini }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:center;" > {{ $registro->hor_fin }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:center;" >{{ substr($registro->tiempo_procesamiento, 10, 9) }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:right;" >{{ number_format($registro->num_lote, 0, $monedas->separador_decimales, $monedas->separador_miles) }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:right;" >{{ number_format($registro->operaciones, 0, $monedas->separador_decimales, $monedas->separador_miles) }} </td>
                              <td style="border-top: 1px solid #f4f4f4;text-align:right;" > {{ number_format($registro->monto_operaciones/100, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }} </td>
                              </tr>
                            @endforeach
                          </tbody>
      </table>

    </body>
    </html>
