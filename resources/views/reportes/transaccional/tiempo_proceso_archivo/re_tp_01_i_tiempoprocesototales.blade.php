{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="col-md-12">
    <div class="card card-dark card-outline">
        <div class="card-header with-border">
            <h3 class="card-title">Totales</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            @foreach ($Total_tiempo_bancos_icom as $total_tiempo_archivo_icom)
            <div class="card card-secondary card-solid">
              <div class="card-header with-border">
                <h3 class="card-title" style="font-size: 14px;font-weight: bold;">Recibidos</h3>
              </div>
              <div class="card-body">
                <div class="row">

                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fa fa-file-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Archivos</span>
                      <span class="info-box-number">{{ number_format($total_tiempo_archivo_icom->total_archivo, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</span>
                    </div>
                  </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fa fa-sign-in"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Operaciones</span>
                      <span class="info-box-number">{{ number_format($total_tiempo_archivo_icom->total_operaciones, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</span>
                    </div>
                  </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fa fa-clock-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Tiempo general de proceso</span>
                      <span class="info-box-number">{{ $total_tiempo_archivo_icom->tiempo_procesamiento }}</span>
                    </div>
                  </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fa fa-clock-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Tiempo promedio de proceso</span>
                      <span class="info-box-number">{{ $total_tiempo_archivo_icom->tiempo_promedio }}</span>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
            @endforeach @foreach ($Total_tiempo_bancos_icoma as $total_Tiempo_archivo_icoma)
            <div class="card card-secondary card-solid">
              <div class="card-header with-border">
                <h2 class="card-title" style="font-size: 14px;font-weight: bold;">Respuestas</h3>
              </div>
              <div class="card-body">
                <div class="row">

                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-info elevation-1"><i class="fa fa-file-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Archivos</span>
                        <span class="info-box-number">{{ number_format($total_Tiempo_archivo_icoma->total_archivo, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-info elevation-1"><i class="fa fa-sign-in"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Operaciones</span>
                        <span class="info-box-number">{{ number_format($total_Tiempo_archivo_icoma->total_operaciones, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-success elevation-1"><i class="fa fa-clock-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Tiempo general de proceso</span>
                        <span class="info-box-number">{{ $total_Tiempo_archivo_icoma->tiempo_procesamiento }}</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                      <span class="info-box-icon bg-success elevation-1"><i class="fa fa-clock-o"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Tiempo promedio de proceso</span>
                        <span class="info-box-number">{{ $total_tiempo_archivo_icom->tiempo_promedio }}</span>
                      </div>
                    </div>
                  </div>

            </div>
          </div>
        </div>
            @endforeach
        </div>
    </div>
</div>
