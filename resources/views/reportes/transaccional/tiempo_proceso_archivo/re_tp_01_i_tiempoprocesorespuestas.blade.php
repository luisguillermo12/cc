{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php
use App\Models\Configuracion\Banco\Banco;
 ?>
<div class="table-responsive">
    <table class="table table-bordered table-hover table-sm">
        <thead class="theadtable">
            <tr>
                <th style="text-align:center;" rowspan="2">
                    Participante emisor
                </th>
                <th style="text-align:center;" rowspan="2">
                    Tipo de archivo
                </th>
                <th style="text-align:center;" colspan="3">
                    Tratamiento
                </th>
                <th style="text-align:center;" rowspan="2">
                    Cantidad de lotes
                </th>
                <th style="text-align:center;" rowspan="2">
                    Cantidad de operaciones
                </th>
                <th style="text-align:center;" rowspan="2">
                    Monto de operaciones
                </th>
            </tr>
            <tr>
                <th style="text-align:center;">
                    Hora inicio
                </th>
                <th style="text-align:center;">
                    Hora fin
                </th>
                <th style="text-align:center;">
                    Tiempo de proceso
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Tiempo_archivo_icoma as $tiempo_archivo_icoma)
            {{--*/ @$monto_total = substr($tiempo_archivo_icoma->monto_operaciones, 0, strlen($tiempo_archivo_icoma->ret_monto_operaciones) - 2).".".substr($tiempo_archivo_icoma->ret_monto_operaciones, strlen($tiempo_archivo_icoma->ret_monto_operaciones) - 2) /*--}}
            <tr>
                <td style="text-align:left;">
                    {{ Banco::buscaruno($tiempo_archivo_icoma->{'banco emisor'})->nombre_completo }}
                </td>
                @if($tiempo_archivo_icoma->{'tipo de archivo'} == 'ICOMA')
                {{--*/ $tipoI = 'RESPUESTA' /*--}}
                @endif
                <td style="text-align:center;">
                    {{ @$tipoI }}
                </td>
                <td style="text-align:center;">
                    {{ $tiempo_archivo_icoma->hor_ini }}
                </td>
                <td style="text-align:center;">
                    {{ $tiempo_archivo_icoma->hor_fin }}
                </td>
                <td style="text-align:center;">
                    {{ substr($tiempo_archivo_icoma->tiempo_procesamiento, 10, 9) }}
                </td>
                <td style="text-align:right;">
                    {{ $tiempo_archivo_icoma->num_lote }}
                </td>
                <td style="text-align:right;">
                    {{ number_format($tiempo_archivo_icoma->operaciones, 0, $monedas->separador_decimales, $monedas->separador_miles) }}
                </td>
                <td style="text-align:right;">
                    {{ number_format($tiempo_archivo_icoma->monto_operaciones/100, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
