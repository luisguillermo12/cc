{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio Tiempo Liquidación--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-line-chart"></i> R0268</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
          <li class="breadcrumb-item active">R0268</li>
        </ol>
      </div>
    </div>
  </div>
  {{-- @section('title') --}}
  <div class="row">
    <div class="col-sm-12">
      <div class="card card-success card-outline">
        <div class="card-header with-border" style="background-color: #e8f9ea;">
          <h4 class="card-title" ><b>R0268 - Tiempo de liquidación</b></h4>
        </div>
      </div>
    </div>
  </div>
  {{-- @endsection --}}
@endsection


@section('content')
  <!-- row -->
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-MONI-1.9-------->
          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.9.1" id="codigo_gui">
          <h3 class="card-title">Filtro</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div><!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
          <form role="form" class="form-row">
            <div class="col-sm-6">
              <div class="form-group form-row">
                {!!Form::label('Liquidacion', 'Liquidación', array('class' => 'col-sm-3 col-form-label'))!!}
                <div class="col-sm-6">
                  {!! Form::select('liquidacion',$liquidaciones,$liquidacion,['id'=>'liquidacion','class'=>'form-control']) !!}
                </div>
              </div>
            </div>
             <div class="col-sm-6">
              <div class="form-group form-row">
                {!!Form::label('Estatus', 'Estatus', array('class' => 'col-sm-3 col-form-label'))!!}
                <div class="col-sm-6">
                  {!! Form::select('estatu',$estatus,$liquidacion,['id'=>'estatu','class'=>'form-control']) !!}
                </div>
              </div>
            </div>

              <div class="col-sm-6">
            <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                {!!Form::label('Fecha desde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
                <div class="col-sm-6">
                  <div class="input-group date">
                      <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! $fe_desde !!}">
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                  </div>
                </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
              {!!Form::label('Fecha hasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
                <div class="input-group date">
                  <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! $fe_hasta !!}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <div class="card-footer with-border">
        <div class="col-sm-12">
          <div class="text-right">
            <input type="hidden" name="busqueda" id="busqueda" value="1"/>
            <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@include('includes.messages')

 @if ((!empty($tiempo_liquidacion)) && ($busqueda))
  @if(count($tiempo_liquidacion))

  <div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary card-outline">
      <div class="card-header with-border">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools pull-right">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </div>
      </div>
      <div class="card-body">
              <div class="table-responsive">
                <table id="PosicionesMultilateralesL1-table" class="table table-bordered table-hover table-sm">
                  <thead class="theadtable">
                    <tr>
                      <th style="text-align:center;">Liquidación</th>
                      <th style="text-align:center;">Hora de archivo STMTR</th>
                      <th style="text-align:center;">Hora de archivo STMTA</th>
                      <th style="text-align:center;">Hora de archivo SYNTR</th>
                      <th style="text-align:center;">Estatus</th>
                      <th style="text-align:center;">Diferencia de horario</th>

                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tiempo_liquidacion as $tiempo_liquidacion )
                    <tr>
                      <td style="text-align:center;">{{ $tiempo_liquidacion->num_liquidaciones  }}</td>
                      <td style="text-align:center;">{{ Date::parse( $tiempo_liquidacion->hora_archivo_stmtr)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;">{{ Date::parse($tiempo_liquidacion->hora_archivo_stmta)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;">{{ Date::parse($tiempo_liquidacion->hora_archivo_syntr)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;">{{ $tiempo_liquidacion->estatus }}</td>
                      <td style="text-align:center;">{{ $tiempo_liquidacion->minutos }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

              <hr class="col-sm-12">

                <div class="col-sm-12 text-center">
                      @if($liquidacion != null || $estatu != null || $fe_desde  != null || $fe_hasta != null )
                      <a href="{{url('Reportes/Transaccional/R0268/Reporte',
                      array('liquidacion' => $liquidacion,
                        'estatu' =>$estatu,
                        'fe_desde'=>$fe_desde,
                        'fe_hasta'=>$fe_hasta,
                        'tipoReporte' => 'XLS'
                      ))}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
                      <a href="{{url('Reportes/Transaccional/R0268/Reporte',
                      array('liquidacion' => $liquidacion,
                        'estatu' =>$estatu,
                        'fe_desde'=>$fe_desde,
                        'fe_hasta'=>$fe_hasta,
                        'tipoReporte' => 'PDF'
                      ))}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
                      @endif
                  </div>
      </div>
    </div>
  </div>
</div>
@else
  <div class="col-md-12">
        <div class="alert alert-danger" style="text-align: center">No existe información</div>
    </div>
@endif
@endif
  @endsection
  {{-- Fin Tiempo Liquidación --}}


  @section('after-scripts-end')
    @include('includes.scripts.reportes.scripts_filtro_fecha')
  @stop
