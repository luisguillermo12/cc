{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<table>
  <thead>
    <tr>
      <th style="text-align:left;" colspan="4">Banco Central de Venezuela</th>
      <th style="text-align: right;">Fecha:</th>
      <th>{{ $param['fecha_actual'] }}</th>
    </tr>
    <tr>
      <th style="text-align:left;">Gerencia de Tesorer&#237;a</th>
    </tr>
    <tr>
      <th style="text-align:left;">Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th colspan="6" style="text-align:center;">C&#193;MARA DE COMPENSACI&#211;N REPORTE TIEMPO DE LIQUIDACI&#211;N</th>
    </tr>
    <tr>
      <th colspan="6" style="text-align:center;">PER&#205;ODO: Del {{ $param['fe_desde'] }} al {{ $param['fe_hasta'] }}</th>
    </tr>
  </thead>
</table>

<table>
                  <thead>
                    <tr>

                      <th style="text-align:center;border:1px solid #000000;background-color: #c2e7fc;">Liquidación</th>
                      <th style="text-align:center;border:1px solid #000000;background-color: #c2e7fc;">Hora de archivo STMTR</th>
                      <th style="text-align:center;border:1px solid #000000;background-color: #c2e7fc;">Hora de archivo STMTA</th>
                      <th style="text-align:center;border:1px solid #000000;background-color: #c2e7fc;">Hora de archivo SYNTR</th>
                      <th style="text-align:center;border:1px solid #000000;background-color: #c2e7fc;">Estatus</th>
                      <th style="text-align:center;border:1px solid #000000;background-color: #c2e7fc;">Diferencia de horario</th>

                    </tr>
                  </thead>
                  <tbody>
                    @foreach($param['tiempo_liquidacion'] as $tiempo_liquidacion )
                    <tr>

                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ $tiempo_liquidacion->num_liquidaciones  }}</td>
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ Date::parse( $tiempo_liquidacion->hora_archivo_stmtr)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ Date::parse($tiempo_liquidacion->hora_archivo_stmta)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ Date::parse($tiempo_liquidacion->hora_archivo_syntr)->format('d/m/Y  H:i:s') }}</td>
                      @if($tiempo_liquidacion->estatus == 'SETTLED' )
                      <td style="text-align:center;border-top:1px solid #f4f4f4;"> LIQUIDADO </td>
                      @else
                      <td style="text-align:center;border-top:1px solid #f4f4f4;"> FALLIDA </td>
                      @endif
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{  $tiempo_liquidacion->minutos }}</td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
