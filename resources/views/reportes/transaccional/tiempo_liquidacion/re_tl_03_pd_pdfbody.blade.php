{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

  {!! Html::style('css/pdf.css') !!}

      <table>
                  <thead style="background-color: #C2E7FC;">
                    <tr>

                      <th style="text-align:center;border:1px solid #000000;">Liquidaci&#243;n</th>
                      <th style="text-align:center;border:1px solid #000000;">Hora de archivo STMTR</th>
                      <th style="text-align:center;border:1px solid #000000;">Hora de archivo STMTA</th>
                      <th style="text-align:center;border:1px solid #000000;">Hora de archivo SYNTR</th>
                      <th style="text-align:center;border:1px solid #000000;">Estatus</th>
                      <th style="text-align:center;border:1px solid #000000;">Diferencia de horario</th>

                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tiempo_liquidacion as $tiempo_liquidacion )
                    <tr>

                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ $tiempo_liquidacion->num_liquidaciones  }}</td>
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ Date::parse( $tiempo_liquidacion->hora_archivo_stmtr)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ Date::parse($tiempo_liquidacion->hora_archivo_stmta)->format('d/m/Y  H:i:s') }}</td>
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{ Date::parse($tiempo_liquidacion->hora_archivo_syntr)->format('d/m/Y  H:i:s') }}</td>
                      @if($tiempo_liquidacion->estatus == 'SETTLED' )
                      <td style="text-align:center;border-top:1px solid #f4f4f4;"> LIQUIDADO </td>
                      @else
                      <td style="text-align:center;border-top:1px solid #f4f4f4;"> FALLIDA </td>
                      @endif
                      <td style="text-align:center;border-top:1px solid #f4f4f4;">{{  $tiempo_liquidacion->minutos }}</td>
                    </tr>
                    @endforeach

                  </tbody>
      </table>
  </body>
  </html>
