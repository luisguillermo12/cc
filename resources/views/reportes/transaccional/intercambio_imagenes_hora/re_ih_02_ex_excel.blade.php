{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<table>
  <thead>
    <tr>
      <th style="text-align:left;">Banco Central de Venezuela</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th style="text-align: right;">Fecha:</th>
      <th>{{ $fecha_actual }}</th>
    </tr>
    <tr>
      <th style="text-align:left;">Gerencia de Tesorer&#237;a</th>
    </tr>
    <tr>
      <th style="text-align:left;">Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th colspan="11" style="text-align:center;">C&#193;MARA DE COMPENSACI&#211;N REPORTE INTERCAMBIO DE IM&#193;GENES POR HORA</th>
    </tr>
    <tr>
      <th colspan="11" style="text-align:center;">PER&#205;ODO: Del {{ $desde }} al {{ $hasta }}</th>
    </tr>
  </thead>
</table>
<table >
    <tr style="background-color: #C2E7FC">
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Participante emisor</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Participante receptor</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Rango de horas</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de archivos de imágenes enviados</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de operaciones enviadas</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de imágenes enviadas</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de imágenes conciliadas-enviadas</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de archivos de imágenes recibidos</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de operaciones recibidas</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de imágenes recibidas</p></th>
     <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de imágenes conciliadas-recibidas</p></th>
  </tr>
  <tbody>
    @forelse ($img_intercambiadas as  $registro)
      <tr>
          <td style="text-align:left;border-top: 1px solid #f4f4f4;">{{ $registro->banco_emisor->nombre_completo }}</td>
          <td style="text-align:left;border-top: 1px solid #f4f4f4;">{{ $registro->banco_receptor->nombre_completo }}</td>
          <td style="text-align:center;border-top: 1px solid #f4f4f4;">{{ $registro->hora }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->archivos_img_env }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->cant_operaciones_env  }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->cant_img_env  }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->cant_img_concilia_env  }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->archivos_img_rec  }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->bilat_cant_operaciones_rec  }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->cant_img_rec  }}</td>
          <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ $registro->cant_img_concilia_rec  }}</td>
      </tr>
    @empty
      <tr>
        <td colspan="11">No se encuentran registros para esta consulta</td>
      </tr>
    @endforelse
      <tr>
      <th colspan="3" style="background-color:#c2e7fc;text-align:left;border: 1px solid #000000;">RESUMEN GENERAL</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $img_intercambiadas->sum('archivos_img_env') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $img_intercambiadas->sum('cant_operaciones_env') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $img_intercambiadas->sum('cant_img_env') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $img_intercambiadas->sum('cant_img_concilia_env') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $img_intercambiadas->sum('archivos_img_rec') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $img_intercambiadas->sum('bilat_cant_operaciones_rec') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;" >{{ $img_intercambiadas->sum('cant_img_rec') }}</th>
      <th style="text-align:right;background-color:#c2e7fc;border: 1px solid #000000;" >{{ $img_intercambiadas->sum('cant_img_concilia_rec') }}</th>
     </tr>
  </tbody>
</table>
