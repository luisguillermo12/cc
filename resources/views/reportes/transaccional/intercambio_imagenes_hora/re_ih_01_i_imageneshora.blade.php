{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php use App\Models\Configuracion\Banco\Banco; ?>

@extends ('layouts.master')

{{--Inicio Intercambio imágenes por hora--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> R0258</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
        <li class="breadcrumb-item active">R0258</li>
      </ol>
    </div>
  </div>
</div>
{{-- @section('title') --}}
<div class="row">
  <div class="col-sm-12">
    <div class="card card-success card-outline">
      <div class="card-header with-border" style="background-color: #e8f9ea;">
        <h4 class="card-title" ><b>R0258 - Intercambio de imágenes por hora</b></h4>
      </div>
    </div>
  </div>
</div>
{{-- @endsection --}}
@endsection


 @section('content')
<div class="row">
<div class="col-md-12">
  <!-- general form elements -->
  <div class="card card-info card-outline">
    <div class="card-header with-border">
      <!--codigo gui MOD-MONI-1.3-->
      <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
      <h3 class="card-title">Filtros</h3>
      <div class="card-tools pull-right">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </div>
    </div><!-- /.card-header -->
    <div class="card-body">
      <form role="form" class="form-row">
        <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Banco', 'Participante emisor', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-9">
              {!!Form::select('cod_banco_emisor',$listaParticipante,$cod_banco_emisor,['id'=>'cod_banco_emisor','class'=>'form-control']) !!}
            </div>
          </div>
        </div>

          <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Banco', 'Participante receptor', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-8">
              {!!Form::select('cod_banco_receptor',$listaParticipante,$cod_banco_receptor,['id'=>'cod_banco_receptor','class'=>'form-control']) !!}
            </div>
          </div>
        </div>


        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaDesde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! $fe_desde !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaHasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! $fe_hasta !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('cod_hora_desde') ? 'has-error' :'' }}">
            {!!Form::label('Hora desde', 'Hora desde', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!! Form::select('cod_hora_desde',$listaDesde,$cod_hora_desde,['id'=>'nombre','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('cod_hora_hasta') ? 'has-error' :'' }}">
            {!!Form::label('Hora hasta', 'Hora hasta', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!! Form::select('cod_hora_hasta',$listaHasta,$cod_hora_hasta,['id'=>'nombre','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer with-border">
        <div class="col-sm-12">
          <div class="text-right">
              <input type="hidden" name="busqueda" id="busqueda" value="1"/>
              <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- seccion de errores -->
@include('includes.messages')
<!-- fin seccion de errores -->

       {{-- Inicio de la seccion Listado--}}
          @if (!is_null($img_intercambiadas))
          @if(!$img_intercambiadas->isEmpty())
            <div class="col-md-12">
              <div class="card card-warning card-outline">
                <div class="card-header with-border">
                  <h3 class="card-title">Listado</h3>
                  <div class="card-tools pull-right">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-hover table-sm">
                      <thead class="theadtable">
                        <tr>
                          <tr>
                            <th  class="th-compuesto"><p class="text-center">Participante emisor</p></th>
                            <th  class="th-compuesto"><p class="text-center">Participante receptor</p></th>
                            <th  class="th-compuesto"><p class="text-center">Rango de horas</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de archivos de imágenes enviados</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de operaciones enviadas</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes enviadas</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes conciliadas-enviadas</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de archivos de imágenes recibidos</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de operaciones recibidas</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes recibidas</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes conciliadas-recibidas</p></th>
                          </tr>
                        </tr>
                      </thead>
                      <tbody>
                        @forelse ($img_intercambiadas as  $registro)
                        <tr>
                          <td style="text-align:left;">{{ Banco::buscaruno($registro['cod_banco_emisor'])->nombre_completo }}</td>
                          <td style="text-align:left;">{{ Banco::buscaruno($registro['cod_banco_pagador'])->nombre_completo }}</td>
                          <td style="text-align:center;" nowrap>{{ $registro['hora'] }}</td>
                          <td style="text-align:right;">{{ number_format($registro['archivos_img_env'], 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                          <td style="text-align:right;">{{ number_format($registro['cant_operaciones_env'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;">{{ number_format($registro['cant_img_env'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;">{{ number_format($registro['cant_img_concilia_env'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;" >{{ number_format($registro['archivos_img_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;">{{ number_format($registro['bilat_cant_operaciones_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                           <td style="text-align:right;">{{ number_format($registro['cant_img_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                           <td style="text-align:right;" >{{ number_format($registro['cant_img_concilia_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                        </tr>
                        @empty
                        <tr>
                          <td colspan="11">No se encuentran registros para esta consulta</td>
                        </tr>
                        @endforelse
                        <tr style="background-color:#c2e7fc">
                          <th colspan="3">RESUMEN GENERAL</th>
                          @for ($i = 0; $i < count($total_operaciones); $i++)
                            <th style="text-align:right;">{{ number_format($total_operaciones[$i], 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                          @endfor
                        </tr>
                      </tbody>
                    </table>

                    <div class="pull-right">
                      <?php echo $img_intercambiadas->render(); ?>
                    </div>

                  <hr class="col-sm-12">

                  <div class="col-sm-12 text-center">
                      @if($cod_banco_emisor != null || $cod_banco_receptor != null || $fe_desde  != null || $fe_hasta != null || $cod_hora_desde != null || $cod_hora_hasta != null)
                      <a href="{{url('Reportes/Transaccional/R0258/Reporte',
                      array('cod_banco_emisor' => $cod_banco_emisor,
                        'cod_banco_receptor' =>$cod_banco_receptor,
                        'desde'=>$fe_desde,
                        'hasta'=>$fe_hasta,
                        'cod_hora_desde'=>$cod_hora_desde,
                        'cod_hora_hasta'=>$cod_hora_hasta,
                        'tipoReporte' => 'XLS'
                      ))}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
                      <a href="{{url('Reportes/Transaccional/R0258/Reporte',
                      array('cod_banco_emisor' => $cod_banco_emisor,
                        'cod_banco_receptor' =>$cod_banco_receptor,
                        'desde'=>$fe_desde,
                        'hasta'=>$fe_hasta,
                        'cod_hora_desde'=>$cod_hora_desde,
                        'cod_hora_hasta'=>$cod_hora_hasta,
                        'tipoReporte' => 'PDF'
                      ))}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
                      @endif
                  </div>
                </div>
              </div>
            </div>
          @else
            <div class="col-md-12">
                <div class="alert alert-danger" style="text-align: center">No existe información</div>
            </div>
          @endif

          @endif


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
{{--Fin de la seccion Filtros--}}

@endsection
{{-- Fin Intercambio imágenes por hora --}}


@section('after-scripts-end')
  @include('includes.scripts.cobranza.scripts_filtro_fecha')
@stop
