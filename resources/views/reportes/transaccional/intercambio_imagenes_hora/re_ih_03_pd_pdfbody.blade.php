{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}


{!! Html::style('css/pdf.css') !!}

    <table border="0" cellspacing="0" cellpadding="0">
      <thead style="background-color: #C2E7FC;">
    <tr>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Participante emisor</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Participante receptor</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Rango de horas</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de archivos de im&#225;genes enviados</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de operaciones enviadas</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de im&#225;genes enviadas</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de im&#225;genes conciliadas-enviadas</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de archivos de im&#225;genes recibidos</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de operaciones recibidas</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de im&#225;genes recibidas</p></th>
   <th  class="th-compuesto" style="border: 1px solid #000000;"><p class="text-center">Cantidad de im&#225;genes conciliadas-recibidas</p></th>
 </tr>
      </thead>
   <tbody>
  @forelse ($img_intercambiadas as  $registro)
    <tr >
        <td style="text-align:left;border-top: 1px solid #f4f4f4;">{{ $registro->banco_emisor->nombre_completo }}</td>
        <td style="text-align:left;border-top: 1px solid #f4f4f4;">{{ $registro->banco_receptor->nombre_completo }}</td>
        <td style="text-align:center;border-top: 1px solid #f4f4f4;" nowrap>{{ $registro->hora }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;" >{{ number_format($registro->archivos_img_env, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;" >{{ number_format($registro->cant_operaciones_env, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;" >{{ number_format($registro->cant_img_env, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</center></td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ number_format($registro->cant_img_concilia_env, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ number_format($registro->archivos_img_rec, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ number_format($registro->bilat_cant_operaciones_rec, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ number_format($registro->cant_img_rec, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
        <td style="text-align:right;border-top: 1px solid #f4f4f4;">{{ number_format($registro->cant_img_concilia_rec, 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
    </tr>
  @empty
    <tr>
      <td colspan="11">No se encuentran registros para esta consulta</td>
    </tr>
  @endforelse
    <tr style="background-color:#c2e7fc">
    <th colspan="3" style="border: 1px solid #000000;text-align:left;">RESUMEN GENERAL</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('archivos_img_env'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('cant_operaciones_env'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('cant_img_env'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('cant_img_concilia_env'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('archivos_img_rec'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('bilat_cant_operaciones_rec'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('cant_img_rec'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
    <th style="text-align:right;border: 1px solid #000000;">{{ number_format($img_intercambiadas->sum('cant_img_concilia_rec'), 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
   </tr>
      </tbody>
    </table>
</body>
</html>
