{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{-- Inicio Extension de franja--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> R0256</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
        <li class="breadcrumb-item active">R0256</li>
      </ol>
    </div>
  </div>
</div>
{{-- @section('title') --}}
<div class="row">
  <div class="col-sm-12">
    <div class="card card-success card-outline">
      <div class="card-header with-border" style="background-color: #e8f9ea;">
        <h4 class="card-title" ><b>R0256 - Extensiones de franja horaria</b></h4>
      </div>
    </div>
  </div>
</div>
{{-- @endsection --}}
@endsection

{{-- Inicio de la seccion de Filtros--}}

@section('content')
<div class="row">
<div class="col-md-12">
  <!-- general form elements -->
  <div class="card card-warning card-outline">
    <div class="card-header with-border">
      <!--codigo gui MOD-MONI-1.3-------->
      <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
      <h3 class="card-title">Filtros</h3>
      <div class="card-tools pull-right">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </div>
    </div><!-- /.card-header -->
    <div class="card-body">
      <form role="form" class="form-row">
        <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Banco', 'Participante', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-9">
              {!!Form::select('cod_banco',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Tipo de operación', 'Tipo de operación', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-8">
              {!! Form::select('codigo_operacion',$listaOperacion,$codigo_operacion,['id'=>'codigo','class'=>'form-control']) !!}
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Fecha', 'Fecha', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="fecha" name="fecha" readonly="readonly" value="{!! $fecha !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer with-border">
        <div class="col-sm-12">
          <div class="text-right">
            <input type="hidden" name="busqueda" id="busqueda" value="1"/>
            <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
{{-- Fin de la seccion de Filtros--}}

@include('includes.messages')

@if (!empty($EXTENSION_FRANJA))
@if(count($EXTENSION_FRANJA))
{{-- Inicio de la seccion de Listado--}}
<div class="col-md-12">
  <div class="card card-primary card-outline">
    <div class="card-header with-border">
      <h3 class="card-title">Listado</h3>
      <div class="card-tools pull-right">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </div>
    </div>
    <div class="card-body">
          <div class="table-responsive" >
            <table class="table table-bordered table-hover table-hover table-sm" style="font-size: 14px !important;">
              <thead class="theadtable">
                <tr>
                  <th><center>Participante</center></th>
                  <th><center>Tipo de operación</center></th>
                  <th><center>Cantidad de solicitudes</center></th>
                  <th><center>Porcentaje (%)</center></th>
                  <th><center>Tiempo de extensión (minutos)</center></th>
                </tr>
              </thead>
              <tbody>
                <?php $cant_sol = 0; $porcentaje = 0; $cant_solg = 0; $porcentajeg = 0; $varbanco1=0; $varbanco2=0; $varprimera=1; $total_reg=0; $totalmin=0;?>
                @foreach($EXTENSION_FRANJA as $extension_franja)
                <?php $total_reg = $total_reg + 1; ?>
                <?php $varbanco1 = $extension_franja->participante; ?>
                @if (($varbanco1 == $varbanco2) || $varprimera ==1)
                <tr>
                  <td style="text-align: left;">{{ $extension_franja->participante}}</td>
                  <td style="text-align: left;">{{ $extension_franja->cod_tipo_operacion }} - {{ $extension_franja->nombre_operacion }}</td>
                  <td><center>{{ number_format($extension_franja->cantidad_solicitudes, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</center></td>
                  <td><center>{{ number_format($extension_franja->porcentaje_extension, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</center></td>
                  <td><center>{{ $extension_franja->tiempo_extension }}</center></td>
                </tr>
                @else
                <?php $varprimera = 1; ?>
                <!-- Sub-Totales -->
                <tr>
                  <th colspan="2" style="background-color:#CBCACA">SUBTOTAL</th>
                  <th style="background-color:#CBCACA"><center>{{ number_format($cant_sol,0, $monedas->separador_decimales, $monedas->separador_miles)}}</center></th>
                  <th style="background-color:#CBCACA"><center>{{ number_format($porcentaje, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</center></th>
                  <th style="background-color:#CBCACA;text-align: center;">{{ number_format($totalmin, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                  <?php $cant_sol = 0; $porcentaje = 0; $totalmin = 0; ?>
                </tr>
                @if (($varbanco1 != $varbanco2))
                    <tr>
                        <td style="text-align: left;">{{ $extension_franja->participante}}</td>
                        <td style="text-align: left;">{{ $extension_franja->cod_tipo_operacion }} - {{ $extension_franja->nombre_operacion }}</td>
                        <td><center>{{ number_format($extension_franja->cantidad_solicitudes, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</center></td>
                        <td><center>{{ number_format($extension_franja->porcentaje_extension, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</center></td>
                        <td><center>{{ $extension_franja->tiempo_extension }}</center></td>
                    </tr>
                 @else
                  <?php if($varbanco1 == $varbanco2)  { ?>
                    <tr>
                        <td style="text-align: left;">{{ $extension_franja->participante}}</td>
                        <td style="text-align: left;">{{ $extension_franja->cod_tipo_operacion }} - {{ $extension_franja->nombre_operacion }}</td>
                        <td><center>{{ number_format($extension_franja->cantidad_solicitudes, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</center></td>
                        <td><center>{{ number_format($extension_franja->porcentaje_extension, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</center></td>
                        <td><center>{{ $extension_franja->tiempo_extension }}</center></td>
                    </tr>
                  <?php  } ?>
                @endif
                @endif
                <?php
                      $cant_sol = $cant_sol + $extension_franja->cantidad_solicitudes;
                      $porcentaje = $porcentaje + $extension_franja->porcentaje_extension;
                      $varprimera=0; $varbanco2=$extension_franja->participante;
                      $totalmin = $totalmin + $extension_franja->tiempo_extension;
                ?>
                @if($total_reg == count($EXTENSION_FRANJA)) <?php //si es la ultima posicion, para imprimir el ultimo subtotal ?>
                  <?php $varprimera = 1; ?>
                  <!-- Sub-Totales -->
                  <tr>
                    <th colspan="2" style="background-color:#CBCACA">SUBTOTAL</th>
                    <th style="background-color:#CBCACA"><center>{{number_format($cant_sol,0, $monedas->separador_decimales, $monedas->separador_miles)}}</center></th>
                    <th style="background-color:#CBCACA"><center>{{ number_format($porcentaje, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</center></th>
                    <th style="background-color:#CBCACA;text-align: center;">{{ number_format($totalmin, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                    <?php  $cant_sol = 0; $porcentaje = 0; $totalmin = 0; ?>
                  </tr>
                @endif
                @endforeach
                <?php
                      $cant_solg =0;
                      $porcentajeg = 0;
                      $totalming = 0;
                ?>
                @foreach($EXTENSION_FRANJAS as $extension_franja)
                  <?php
                      $cant_solg = $cant_solg + $extension_franja->cantidad_solicitudes;
                      $porcentajeg = $porcentajeg + $extension_franja->porcentaje_extension;
                      $totalming = $totalming + $extension_franja->tiempo_extension;
                  ?>
                @endforeach
                <?php $paginas = $EXTENSION_FRANJA->total() / 8;
                      $paginas = ceil($paginas);?>
                @if (($paginas == $page || $paginas == 1))
                      <tr>
                         <th colspan="2" style="background-color:#c2e7fc">TOTAL GENERAL</th>
                         <th style="background-color:#c2e7fc"><center>{{ number_format($cant_solg, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</center></th>
                         <th style="background-color:#c2e7fc;"><center>{{ number_format($porcentajeg, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</center></th>
                         <th style="background-color:#c2e7fc;text-align: center;">{{ number_format($totalming, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                           <?php $cant_solg = 0; $porcentajeg = 0; $totalming = 0; ?>
                       </tr>
                 @endif
              </tbody>
            </table>
             <div class="pull-right">
               <?php echo $EXTENSION_FRANJA->render(); ?>
             </div>
          </div>

          <hr class="col-sm-12">

          <div class="col-sm-12 text-center">
              <a href="{{url('Reportes/Transaccional/R0256/Reporte',
                array('cod_banco' => $cod_banco,
                  'codigo_operacion' => $codigo_operacion,
                  'fecha' => $fecha,
                  'tipo_reporte' => $tipo_reporte = 'XLS'
                )
              )}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
              <a href="{{url('Reportes/Transaccional/R0256/Reporte',
                array('cod_banco' => $cod_banco,
                  'codigo_operacion' => $codigo_operacion,
                  'fecha' => $fecha,
                  'tipo_reporte' => $tipo_reporte = 'PDF'
                )
              )}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
          </div>
      @else
      <div class="col-md-12">
          <div class="alert alert-danger" style="text-align: center">No existe información</div>
      </div>
      @endif

      @endif
{{-- Fin de la seccion de Listado--}}

</div><!--card body-->
</div>
</div>
</div>

@stop

{{-- Fin Extension de franja--}}

@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_filtro_fecha')
@stop
