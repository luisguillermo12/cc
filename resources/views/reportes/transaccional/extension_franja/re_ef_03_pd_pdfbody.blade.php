{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

{!! Html::style('css/pdf.css') !!}

  <table border="0" cellspacing="0" cellpadding="0">
    <thead style="background-color: #C2E7FC;">
       <tr class="class="thead-dark"" role="alert">
           <th style="border: 1px solid #000000;background-color:#c2e7fc;text-align: center;">Participante</th>
           <th style="border: 1px solid #000000;background-color:#c2e7fc;text-align: center;">Tipo de operaci&#243;n</th>
           <th style="border: 1px solid #000000;background-color:#c2e7fc;text-align: center;">Cantidad de solicitudes</th>
           <th style="border: 1px solid #000000;background-color:#c2e7fc;text-align: center;">Porcentaje (%)</th>
           <th style="border: 1px solid #000000;background-color:#c2e7fc;text-align: center;">Tiempo de extensi&#243;n (minutos)</th>
        </tr>
        </thead>
       <tbody>
         <?php $cant_sol = 0; $porcentaje = 0; $cant_solg = 0; $porcentajeg = 0; $varbanco1=0; $varbanco2=0; $varprimera=1; $total_reg=0; $totalmin=0;?>
         @foreach($EXTENSION_FRANJA as $extension_franja)
         <?php $total_reg = $total_reg + 1; ?>
         <?php $varbanco1 = $extension_franja->participante; ?>
         @if (($varbanco1 == $varbanco2) || $varprimera ==1)
         <tr>
           <td style="text-align: left;border-top: 1px solid #f4f4f4;">{{ $extension_franja->participante}}</td>
           <td style="text-align: left;border-top: 1px solid #f4f4f4;">{{ $extension_franja->cod_tipo_operacion }} - {{ $extension_franja->nombre_operacion }}</td>
           <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ number_format($extension_franja->cantidad_solicitudes, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
           <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ number_format($extension_franja->porcentaje_extension, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</td>
           <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ $extension_franja->tiempo_extension }}</td>
         </tr>
         @else
         <?php $varprimera = 1; ?>
         <!-- Sub-Totales -->
         <tr>
           <th colspan="2" style="background-color:#CBCACA;text-align: left;border: 1px solid #000000;">SUBTOTAL</th>
           <th style="background-color:#CBCACA;border: 1px solid #000000;text-align: center;">{{number_format($cant_sol,0, $monedas->separador_decimales, $monedas->separador_miles)}}</th>
           <th style="background-color:#CBCACA;border: 1px solid #000000;text-align: center;">{{ number_format($porcentaje, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
           <th style="background-color:#CBCACA;border: 1px solid #000000;text-align: center;">{{ number_format($totalmin, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
           <?php  $cant_sol = 0; $porcentaje = 0; $totalmin = 0; ?>
         </tr>
         @if (($varbanco1 != $varbanco2))
             <tr>
                 <td style="text-align: left;border-top: 1px solid #f4f4f4;">{{ $extension_franja->participante}}</td>
                 <td style="text-align: left;border-top: 1px solid #f4f4f4;">{{ $extension_franja->cod_tipo_operacion }} - {{ $extension_franja->nombre_operacion }}</td>
                 <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ number_format($extension_franja->cantidad_solicitudes, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                 <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ number_format($extension_franja->porcentaje_extension, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</td>
                 <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ $extension_franja->tiempo_extension }}</td>
             </tr>
          @else
           <?php if($varbanco1 == $varbanco2)  { ?>
             <tr>
                 <td style="text-align: left;border-top: 1px solid #f4f4f4;">{{ $extension_franja->participante}}</td>
                 <td style="text-align: left;border-top: 1px solid #f4f4f4;">{{ $extension_franja->cod_tipo_operacion }} - {{ $extension_franja->nombre_operacion }}</td>
                 <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ number_format($extension_franja->cantidad_solicitudes, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                 <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ number_format($extension_franja->porcentaje_extension, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</td>
                 <td style="text-align: center;border-top: 1px solid #f4f4f4;">{{ $extension_franja->tiempo_extension }}</td>
             </tr>
           <?php  } ?>
         @endif
         @endif
         <?php
               $cant_sol = $cant_sol + $extension_franja->cantidad_solicitudes;
               $porcentaje = $porcentaje + $extension_franja->porcentaje_extension;
               $varprimera=0; $varbanco2=$extension_franja->participante;
               $totalmin = $totalmin + $extension_franja->tiempo_extension;
         ?>
         @if($total_reg == count($EXTENSION_FRANJA)) <?php //si es la ultima posicion, para imprimir el ultimo subtotal ?>
           <?php $varprimera = 1; ?>
           <!-- Sub-Totales -->
           <tr>
             <th colspan="2" style="background-color:#CBCACA;text-align: left;border: 1px solid #000000;">SUBTOTAL</th>
             <th style="background-color:#CBCACA;border: 1px solid #000000;text-align: center;">{{number_format($cant_sol,0, $monedas->separador_decimales, $monedas->separador_miles)}}</th>
             <th style="background-color:#CBCACA;border: 1px solid #000000;text-align: center;">{{ number_format($porcentaje, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
             <th style="background-color:#CBCACA;border: 1px solid #000000;text-align: center;">{{ number_format($totalmin, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
             <?php  $cant_sol = 0; $porcentaje = 0; $totalmin = 0; ?>
           </tr>
         @endif
         @endforeach
         <?php
               $cant_solg =0;
               $porcentajeg = 0;
               $totalming = 0;
         ?>
         @foreach($EXTENSION_FRANJA as $extension_franja)
           <?php
               $cant_solg = $cant_solg + $extension_franja->cantidad_solicitudes;
               $porcentajeg = $porcentajeg + $extension_franja->porcentaje_extension;
               $totalming = $totalming + $extension_franja->tiempo_extension;
           ?>
         @endforeach
         <tr>
            <th colspan="2" style="background-color:#c2e7fc;text-align: left;border: 1px solid #000000;">TOTAL GENERAL</th>
            <th style="background-color:#c2e7fc;border: 1px solid #000000;text-align: center;">{{ number_format($cant_solg, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
            <th style="background-color:#c2e7fc;border: 1px solid #000000;text-align: center;">{{ number_format($porcentajeg, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)}}</th>
            <th style="background-color:#c2e7fc;border: 1px solid #000000;text-align: center;">{{ number_format($totalming, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
            <?php  $cant_solg = 0; $porcentajeg = 0; $totalming = 0; ?>
          </tr>
      </tbody>

      </table>
 </body>
</html>
