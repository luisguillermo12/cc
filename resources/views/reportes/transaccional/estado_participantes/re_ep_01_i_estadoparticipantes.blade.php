{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio Estado de los Participantes--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-line-chart"></i> R0272</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
          <li class="breadcrumb-item active">R0272</li>
        </ol>
      </div>
    </div>
  </div>
  {{-- @section('title') --}}
  <div class="row">
    <div class="col-sm-12">
      <div class="card card-success card-outline">
        <div class="card-header with-border" style="background-color: #e8f9ea;">
          <h4 class="card-title" ><b>R0272 - Estado de los participantes</b></h4>
        </div>
      </div>
    </div>
  </div>
  {{-- @endsection --}}
@endsection

@section('content')

{{--Inicio de la seccion de Filtros--}}

	<!-- row -->
	<div class="row">
    <div class="col-md-12">

      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-MONI-1.9-->
          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.9.1" id="codigo_gui">
          <h3 class="card-title">Filtros</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div><!-- /.card-header -->
        <div class="card-body">
          {!! Form::open(['method'=>'GET', 'class' => 'form-row', 'role' =>'form']) !!}
            <div class="col-sm-6">
              <div class="form-group form-row">
                {!!Form::label('Participante', 'Participante', array('class' => 'col-sm-3 col-form-label'))!!}
                <div class="col-sm-9">
                  {!! Form::select('banco',$lista_bancos,$banco,['id'=>'Participante','class'=>'form-control','onchange' => 'this.form.submit()']) !!}
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group form-row">
                {!!Form::label('Estatus', 'Estado', array('class' => 'col-sm-3 col-form-label'))!!}
                <div class="col-sm-6">
                  {!! Form::select('estatus',$estatus,$estado,['id'=>'Estatus','class'=>'form-control','onchange' => 'this.form.submit()']) !!}
                </div>
              </div>
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      {{--Fin de la seccion de Filtros--}}

      {{--Inicio de la seccion de Totales--}}
      <div class="card card-dark card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-MONI-1.9-->
          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.9.1" id="codigo_gui">
          <h3 class="card-title">Totales</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div><!-- /.card-header -->
        <div class="card-body">
              <div class="row" style="margin-top: 1rem!important;">

                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fa fa-university"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Total</span>
                      <span class="info-box-number">{{$totalparticipantes->total}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-success elevation-1"><i class="fa fa-check"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Activo</span>
                      <span class="info-box-number">{{$totalparticipantes->activo}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-times"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Inactivo</span>
                      <span class="info-box-number">{{$totalparticipantes->inactivo}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-exclamation"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Suspendido</span>
                      <span class="info-box-number">{{$totalparticipantes->suspendido}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-secondary elevation-1"><i class="fa fa-sign-out"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Excluido</span>
                      <span class="info-box-number">{{$totalparticipantes->excluido}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-indigo elevation-1"><i class="fa fa-exchange"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Fusionado</span>
                      <span class="info-box-number">{{$totalparticipantes->fusionado}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-pink elevation-1"><i class="fa fa-compress"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Absorbido</span>
                      <span class="info-box-number">{{$totalparticipantes->absorbido}}</span>
                    </div>
                  </div>
                </div>

                  </div>
        </div>
      </div>
      {{--Fin de la seccion de Totales--}}

      {{--Inicio de la seccion de Listado--}}
      @if (count($EstatusParticipante))
      <div class="card card-primary card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-MONI-1.9-->
          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.9.1" id="codigo_gui">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="table-responsive">
            <?php  $count = 0; ?>
            @foreach ($EstatusParticipante as $estatusparticipante)
            <?php  $estatus = str_replace(' ','&nbsp;', $estatusparticipante->estatus); ?>
            <?php  //$id_banco = str_replace(' ','&nbsp;', $estatusparticipante->id); ?>
            <?php  $nombre = str_replace(' ','&nbsp;', $estatusparticipante->nombre); ?>
            @if ($estatusparticipante->modalidad_participacion == '0')
            <?php  $modalidad = 'INDIRECTO'; ?>
            @else
            <?php  $modalidad = 'DIRECTO'; ?>
            @endif


            <div class="card card-secondary" style="border: 1px solid #dfdfdf; box-shadow: 0 0 0px;">
              <div class="card-header with-border">
                <h3 class="card-title">{{ $estatusparticipante->codigo }} - {{ $estatusparticipante->nombre }}</h3>
              </div>
              <div class="card-body">
                <div class="col-sm-12">
                  <h4>Datos generales del participante</h4>
                </div>
                  <div class="col-sm-12 row">
                    <div class="col-sm-4">
                      <p><strong>Fecha de ingreso a la cámara</strong></p>
                      <p class="text-muted">{{Date::parse($estatusparticipante->fecha_ingreso)->format('Y-m-d')}}</p>
                    </div>
                    <div class="col-sm-4">
                      <p><strong>Modalidad</strong></p>
                      <p class="text-muted">{{ $modalidad }}</p>
                    </div>
                    @if ($estatusparticipante->estatus == 'SUSPENDIDO')
                        <?php  $cesante = 'CESANTE'; ?>
                    @elseif ($estatusparticipante->estatus == 'EXCLUIDO')
                      <?php  $retirado = 'RETIRADO'; ?>
                    @endif
                    <div class="col-sm-4">
                      <p><strong>Estado</strong></p>
                      @if($estatusparticipante->estatus === 'ACTIVO')
                      <p class="text-muted"><span class="badge bg-success">{{ $estatusparticipante->estatus }}</span></p>
                      @elseif ($estatusparticipante->estatus === 'INACTIVO')
                      <p class="text-muted"><span class="badge bg-warning">{{ $estatusparticipante->estatus }}</span></p>
                      @elseif ($estatusparticipante->estatus === 'SUSPENDIDO')
                      <p class="text-muted"><span class="badge bg-danger">{{ $estatusparticipante->estatus }}</span></p>
                      @elseif ($estatusparticipante->estatus === 'FUSIONADO')
                      <p class="text-muted"><span class="badge bg-indigo">{{ $estatusparticipante->estatus }}</span></p>
                      @elseif ($estatusparticipante->estatus === 'ABSORBIDO')
                      <p class="text-muted"><span class="badge bg-pink">{{ $estatusparticipante->estatus }}</span></p>
                      @elseif ($estatusparticipante->estatus === 'EXCLUIDO')
                      <p class="text-muted"><span class="badge bg-secondary">{{ $estatusparticipante->estatus }}</span></p>
                      @endif
                    </div>
                  </div>
                  @if ($estatusparticipante->banco_principal_id == null)
                  @else
                  <div class="col-sm-12">
                    <h4>Datos Generales del Principal</h4>
                  </div>
                  <!--if ($estatusparticipante->modalidad_principal == '0')
                  <?php  $modalidad_principal = 'INDIRECTO'; ?>
                  else
                  <?php  $modalidad_principal = 'DIRECTO'; ?>
                  endif-->
                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <p><strong>Banco Principal</strong></p>
                      <p class="text-muted">{{ $estatusparticipante->codigo_principal }} - {{ $estatusparticipante->nombre_principal }}</p>
                    </div>
                  </div>
                  @endif
                  <!-- <div class="clearfix"></div> -->
                  @if ($estatusparticipante->modalidad_participacion == 1)
                  @else
                  <div class="col-sm-12">
                    <h4>Datos Generales del Representante</h4>
                  </div>
                  <div class="col-sm-12">
                    <div class="col-sm-4">
                      <p><strong>Banco Representante</strong></p>
                      <p class="text-muted">{{ $estatusparticipante->codigo_representante }} - {{ $estatusparticipante->nombre_representante }}</p>
                    </div>
                  </div>
                  @endif
                  <!-- <div class="clearfix"></div> -->
                </div>
              <!-- </div> -->
            </div>
          <?php  $count++; ?>
          @endforeach
          </div>

          <hr class="col-sm-12">

          <div class="col-sm-12 text-center">

              <a href="{{url('Reportes/Transaccional/R0272/Reporte',
              array('estatus' => $estado,
                    'banco' => $banco,
                    'tipoReporte' => 'XLS'
                    )
                    )}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
               <a href="{{url('Reportes/Transaccional/R0272/Reporte',
               array('estatus' => $estado,
                    'banco' => $banco,
                    'tipoReporte' => 'PDF')
                    )}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>

          </div>
        </div><!--table-responsive-->
      </div>
    </div>
    @else
    <div class="col-md-12">
        <div class="alert alert-danger" style="text-align: center">No existe información</div>
    </div>
    @endif
  </div>
</div>
{{--Fin de la seccion de Listado--}}

@stop
{{--Fin Estado de los Participantes--}}

@section('after-scripts-end')
@stop
