{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php use App\Models\Configuracion\Banco\Banco; ?>

{!! Html::style('css/pdf.css') !!}

  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                              <thead>
                               <tr style="background-color: #C2E7FC">
                                <th style="text-align: center;border: 1px solid #000000;" >C&#243;digo</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Nombre</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Estatus</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Fecha de ingreso</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Participante principal</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Modalidad de participaci&#243;n</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Banco representante</th>
                              </tr>
                              </thead>

        <tbody>
                            @foreach ($EstatusParticipante as  $registro)
                              <tr>
                             <td style="text-align: left;border-top: 1px solid #f4f4f4;"> {{ $registro->codigo }} </td>
                              <td style="text-align: left;border-top: 1px solid #f4f4f4;"> {{ $registro->nombre }}  </td>
                               <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->estatus }}  </td>
                                <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->fecha_ingreso }}  </td>
                                 <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->codigo_principal }} - {{ $registro->nombre_principal }} </td>
                                 @if($registro->modalidad_participacion==1)
                                   <td style="text-align: center;border-top: 1px solid #f4f4f4;"> DIRECTO </td>
                                   @else
                                   <td style="text-align: center;border-top: 1px solid #f4f4f4;"> INDIRECTO </td>
                                   @endif

                                   @if($registro->modalidad_participacion==0)
                                    <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->codigo_representante }}-{{ $registro->nombre_representante }} </td>
                                   @else
                                   <td style="text-align: center;border-top: 1px solid #f4f4f4;"></td>
                                   @endif
                              </tr>
                            @endforeach
                          </tbody>
      </table>

    </body>
    </html>
