{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php use App\Models\Configuracion\Banco\Banco; ?>

<table>
  <thead>
    <tr>
      <th style="text-align:left;" colspan="5">Banco Central de Venezuela</th>
      <th style="text-align: right;">Fecha:</th>
      <th>{{ $param['fecha_actual'] }}</th>
    </tr>
    <tr>
      <th style="text-align:left;">Gerencia de Tesorer&#237;a</th>
    </tr>
    <tr>
      <th style="text-align:left;">Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th colspan="7" style="text-align:center;">C&#193;MARA DE COMPENSACI&#211;N REPORTE ESTADO DE LOS PARTICIPANTES</th>
    </tr>
    <tr>
      <th colspan="7" style="text-align:center;">PER&#205;ODO: {{ $param['fecha'] }}</th>
    </tr>
  </thead>
</table>


         <table>
                              <tr style="background-color: #C2E7FC">
                                <th style="text-align: center;border: 1px solid #000000;" >Código</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Nombre</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Estatus</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Fecha de ingreso</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Participante principal</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Modalidad de participación</th>
                                <th style="text-align: center;border: 1px solid #000000;" >Banco representante</th>
                              </tr>
                          <tbody>
                            @foreach ($param['EstatusParticipante'] as  $registro)
                              <tr>
                             <td style="text-align: left;border-top: 1px solid #f4f4f4;"> {{ $registro->codigo }} </td>
                              <td style="text-align: left;border-top: 1px solid #f4f4f4;"> {{ $registro->nombre }} </td>
                               <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->estatus }} </td>
                                <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->fecha_ingreso }} </td>
                                 <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->codigo_principal }} - {{ $registro->nombre_principal }} </td>
                                 @if($registro->modalidad_participacion==1)
                                   <td style="text-align: center;border-top: 1px solid #f4f4f4;"> DIRECTO </td>
                                   @else
                                   <td style="text-align: center;border-top: 1px solid #f4f4f4;"> INDIRECTO </td>
                                   @endif

                                   @if($registro->modalidad_participacion==0)
                                    <td style="text-align: center;border-top: 1px solid #f4f4f4;"> {{ $registro->codigo_representante }}-{{ $registro->nombre_representante }} </td>
                                   @else
                                   <td style="text-align: center;border-top: 1px solid #f4f4f4;"></td>
                                   @endif
                              </tr>
                            @endforeach
                          </tbody>
            </table>
