{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php use App\Models\Configuracion\Banco\Banco; ?>

@extends ('layouts.master')

{{--Inicio Tiempo Liquidación--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> R0257</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
        <li class="breadcrumb-item active">R0257</li>
      </ol>
    </div>
  </div>
</div>
{{-- @section('title') --}}
<div class="row">
  <div class="col-sm-12">
    <div class="card card-success card-outline">
      <div class="card-header with-border" style="background-color: #e8f9ea;">
        <h4 class="card-title" ><b>R0257 - Intercambio de imágenes bilateral</b></h4>
      </div>
    </div>
  </div>
</div>
{{-- @endsection --}}
@endsection


@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header with-border">
                {{-- Inicio de la seccion Filtros--}}

                <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
                <h3 class="card-title">Filtros</h3>
                <div class="card-tools pull-right">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </div>
              </div><!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                {!! Form::open(['method'=>'GET', 'class' => 'form-row', 'role' =>'form']) !!}

                    <div class="col-sm-6">
                    <div class="form-group form-row">
                      {!!Form::label('Participante emisor', 'Participante emisor', array('class' => 'col-sm-3 col-form-label'))!!}
                      <div class="col-sm-9">
                        {!! Form::select('cod_banco_emisor',$bancos,$cod_banco_emisor,['id'=>'cod_banco_emisor','class'=>'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group form-row">
                      {!!Form::label('Participante receptor', 'Participante receptor', array('class' => 'col-sm-3 col-form-label'))!!}
                      <div class="col-sm-8">
                        {!! Form::select('cod_banco_receptor', $bancos, $cod_banco_receptor,['id'=>'cod_banco_receptor','class'=>'form-control']) !!}
                      </div>
                    </div>
                </div>




                <div class="col-sm-6">
                  <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    {!!Form::label('Fechadesde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
                    <div class="col-sm-6">
                      <div class="input-group date">
                        <input type="text" class="form-control group-date" id="desde" readonly="readonly" name="desde" value="{!! $desde !!}">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
                    {!!Form::label('Fecha hasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
                    <div class="col-sm-6">
                      <div class="input-group date">
                        <input type="text" class="form-control group-date" id="hasta" readonly="readonly" name="hasta" value="{!! $hasta !!}">
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer with-border">
                <div class="col-sm-12">
                  <div class="text-right">
                    <input type="hidden" name="busqueda" id="busqueda" value="1"/>
                    <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                  </div>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
            {{-- Fin de la seccion Filtros--}}

            @include('includes.messages')

            {{-- Inicio de la seccion Listado--}}
          @if (!is_null($img_intercambiadas))
          @if(!$img_intercambiadas->isEmpty())
            <div class="col-md-12">
              <div class="card card-warning card-outline">
                <div class="card-header with-border">
                  <h3 class="card-title">Listado</h3>
                  <div class="card-tools pull-right">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </div>
                </div>
                <div class="card-body">
                  <div class="col-sm-12">
                    <table class="table table-bordered table-hover table-sm">
                      <thead class="theadtable">
                        <tr>
                          <tr>
                            <th  class="th-compuesto"><p class="text-center">Participante emisor</p></th>
                            <th  class="th-compuesto"><p class="text-center">Participante receptor</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de archivos de imágenes enviados</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de operaciones enviadas</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes enviadas</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes conciliadas-enviadas</p></th>
                            <th  class="th-compuesto"><p class="text-center">Cantidad de archivos de imágenes recibidos</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de operaciones recibidas</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes recibidas</p></th>
                             <th  class="th-compuesto"><p class="text-center">Cantidad de imágenes conciliadas-recibidas</p></th>
                          </tr>
                        </tr>
                      </thead>
                      <tbody>

                        @forelse ($img_intercambiadas as  $registro)
                        <tr>
                          <td nowrap >{{ Banco::buscaruno($registro['cod_banco_emisor'])->nombre_completo }}</td>
                          <td nowrap >{{ Banco::buscaruno($registro['cod_banco_pagador'])->nombre_completo }}</td>
                          <td style="text-align:right;">{{ number_format($registro['archivos_img_env'], 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                          <td style="text-align:right;">{{ number_format($registro['cant_operaciones_env'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;">{{ number_format($registro['cant_img_env'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;">{{ number_format($registro['cant_img_concilia_env'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;" >{{ number_format($registro['archivos_img_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                          <td style="text-align:right;">{{ number_format($registro['bilat_cant_operaciones_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                           <td style="text-align:right;">{{ number_format($registro['cant_img_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                           <td style="text-align:right;" >{{ number_format($registro['cant_img_concilia_rec'], 0, $monedas->separador_decimales, $monedas->separador_miles)  }}</td>
                        </tr>
                        @empty
                        <tr>
                          <td colspan="10">No se encuentran registros para esta consulta</td>
                        </tr>
                        @endforelse
                        <tr style="background-color:#c2e7fc">
                          <th colspan="2">RESUMEN GENERAL</th>
                          @for ($i = 0; $i < count($total_operaciones); $i++)
                            <th style="text-align:right;">{{ number_format($total_operaciones[$i], 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                          @endfor
                        </tr>
                      </tbody>
                    </table>

                    <div class="pull-right">
                      <?php echo $img_intercambiadas->render(); ?>
                    </div>

                  </div><!--table-responsive-->

                  <hr class="col-sm-12">

                  <div class="col-sm-12 text-center">
                      @if($cod_banco_emisor != null || $cod_banco_receptor != null || $desde  != null || $hasta != null)
                      <a href="{{url('Reportes/Transaccional/R0257/Reporte',
                      array('cod_banco_emisor' => $cod_banco_emisor,
                      'cod_banco_receptor' =>$cod_banco_receptor,
                      'desde'=>$desde,
                      'hasta'=>$hasta,
                      'tipoReporte' => 'XLS'
                      ))}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
                      <a href="{{url('Reportes/Transaccional/R0257/Reporte',
                      array('cod_banco_emisor' => $cod_banco_emisor,
                      'cod_banco_receptor' =>$cod_banco_receptor,
                      'desde'=>$desde,
                      'hasta'=>$hasta,
                      'tipoReporte' => 'PDF'
                      ))}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
                      @endif
                  </div>
                </div>
              </div>
            </div>
          @else
            <div class="col-md-12">
                <div class="alert alert-danger" style="text-align: center">No existe información</div>
            </div>
          @endif

          @endif


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@stop
@section('after-scripts-end')
  @include('includes.scripts.cobranza.scripts_filtro_fecha')
@stop
