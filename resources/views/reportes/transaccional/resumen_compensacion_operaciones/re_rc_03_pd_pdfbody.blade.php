{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

  {!! Html::style('css/pdf.css') !!}

<br>
  <table class="table table-striped" style="font-size: 14px !important;">
    <thead class="thead-default">
      <tr style="background-color:#c2e7fc;border-bottom: 1px solid #000000;border-top: 1px solid #000000;">
        <th style=" text-align: center;border: 1px solid #000000;" colspan="2">Operaci&oacuten: {{ $informacion['operaciones']->nombre_completo }}</th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="3"><center>Debe</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="3"><center>Haber</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="4"><center>Saldos</center></th>
      </tr>
      <tr style="background-color:#c2e7fc;">

        <th style=" text-align: center;border: 1px solid #000000;" colspan="2"><center>Participante</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="1"><center>Cantidad de operaciones</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="2"><center>Montos</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="1"><center>Cantidad de operaciones</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="2"><center>Montos</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="2"><center>Acreedor</center></th>
        <th style=" text-align: center;border: 1px solid #000000;" colspan="2"><center>Deudor</center></th>
      </tr>
    </thead>
    <tbody>
      @foreach($informacion['parametros'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4;">
          <td colspan="2">{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="text-align: right;" colspan="1">{{ number_format($ai_objeto->d_cantidad, 0, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</td>
          <td style="text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>" colspan="2">{{ number_format($ai_objeto->h_montos, $informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</td>
          <td style="text-align: right;" colspan="1">{{ number_format($ai_objeto->d_cantidad, 0, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</td>
          <td style="text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>" colspan="2">{{ number_format($ai_objeto->d_montos, $informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</td>
          <td style="text-align: right;" colspan="2">{{ number_format($ai_objeto->acreedor, $informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</td>
          <td style="text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>" colspan="2">{{ number_format($ai_objeto->deudor, $informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</td>
        </tr>
        @else
        <tr>
          <th colspan="2" style="text-align: left;background-color:#c2e7fc;border: 1px solid black;">TOTAL</th>
          <th colspan="1" style="text-align: right;background-color:#c2e7fc;border: 1px solid black;">{{ number_format($ai_objeto->d_cantidad,0, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</th>
          <th colspan="2" style="text-align: right;background-color:#c2e7fc;border: 1px solid black;">{{ number_format($ai_objeto->d_montos,$informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</th>
          <th colspan="1" style="text-align: right;background-color:#c2e7fc;border: 1px solid black;">{{ number_format($ai_objeto->h_cantidad,0, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</th>
          <th colspan="2" style="text-align: right;background-color:#c2e7fc;border: 1px solid black;">{{ number_format($ai_objeto->h_montos,$informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</th>
          <th colspan="2"  style="text-align: right;background-color:#c2e7fc;border: 1px solid black;">{{ number_format($ai_objeto->acreedor, $informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</th>
          <th colspan="2"  style="text-align: right;background-color:#c2e7fc;border: 1px solid black;">{{ number_format($ai_objeto->deudor, $informacion['monedas']->num_decimales, $informacion['monedas']->separador_decimales, $informacion['monedas']->separador_miles) }}</th>
        </tr>
        @endif
      @endforeach
    </tbody>
  </table>

 </body>
</html>
