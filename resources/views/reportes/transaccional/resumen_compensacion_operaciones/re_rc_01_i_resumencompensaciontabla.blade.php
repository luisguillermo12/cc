{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<ul class="nav nav-pills pills-list mb-3" role="tablist">
  <li class="nav-item"><a class="nav-link active" href="#tabla" data-toggle="pill">{{ $noperacion->nombre_completo }}</a></li>
</ul>

<div class="tab-content">
  <div id="tab1" class="tab-pane fade active show" role="tabpanel">
          <div class="table-responsive" >
            <!-- Apertura de la tabla -->
            <table id="operaciones_hora_tabla" class="table table-bordered table-hover table-sm">
            <thead class="theadtable">
            <tr>
              <th colspan="2" style="text-align:center;"></th>
              <th colspan="4" style="text-align:center;">Haber</th>
              <th colspan="4" style="text-align:center;">Debe</th>
              <th colspan="4" colspan="2" style="text-align:center;">Saldo</th>
            </tr>
            <tr>
              <th colspan="2" style="text-align:center;">Participante</th>
              <th colspan="2" style="text-align:center;">Cantidad de operaciones</th>
              <th colspan="2" style="text-align:center;">Monto</th>
              <th colspan="2" style="text-align:center;">Cantidad de operaciones</th>
              <th colspan="2" style="text-align:center;">Monto</th>
            <th colspan="2" style="text-align:center;">Acreedor</th>
            <th colspan="2" style="text-align:center;">Deudor</th>
            </tr>

          </thead>
          <tbody>
            @foreach($parametros as $ai_objeto)
              @if(($ai_objeto->nombre_banco != null))
              <tr style="border-bottom: 1px solid lightgray;border-top: 1px solid lightgray;">
                <td colspan="2">{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
                <td style="text-align: right;" colspan="2">{{ number_format($ai_objeto->h_cantidad, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>" colspan="2">{{ number_format($ai_objeto->h_montos, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;" colspan="2">{{ number_format($ai_objeto->d_cantidad, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>" colspan="2">{{ number_format($ai_objeto->d_montos, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;" colspan="2">{{ number_format($ai_objeto->acreedor, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>" colspan="2">{{ number_format($ai_objeto->deudor, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              </tr>
              @else
              <tr>
                <th colspan="2" style="text-align: left;background-color:#c2e7fc;">TOTAL</th>
                <th colspan="2" style="text-align: right;background-color:#c2e7fc;">{{ number_format($ai_objeto->d_cantidad,0,$monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th colspan="2" style="text-align: right;background-color:#c2e7fc;">{{ number_format($ai_objeto->d_montos,$monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th colspan="2" style="text-align: right;background-color:#c2e7fc;">{{ number_format($ai_objeto->h_cantidad,0,$monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th colspan="2" style="text-align: right;background-color:#c2e7fc;">{{ number_format($ai_objeto->h_montos,$monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th colspan="2"  style="text-align: right;background-color:#c2e7fc;">{{ number_format($ai_objeto->acreedor, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th colspan="2"  style="text-align: right;background-color:#c2e7fc;">{{ number_format($ai_objeto->deudor, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
              </tr>
              @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
	</div>

<hr class="col-sm-12">

        <div class="col-sm-12 text-center">

             <a href="{{url('Reportes/Transaccional/R0264/ReporteEXCEL',
                      array(
	                      'Pparticipantes' => Input::get('bancos'),
	                      'Poperaciones' => Input::get('operaciones'),
	                      'Fechai' => Input::get('fechai'),
	                      'Fechaf' => Input::get('fechaf'),
                      ))}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>

             <a href="{{url('Reportes/Transaccional/R0264/ReportePDF',
                      array(
	                      'Pparticipantes' => Input::get('bancos'),
	                      'Poperaciones' => Input::get('operaciones'),
	                      'Fechai' => Input::get('fechai'),
	                      'Fechaf' => Input::get('fechaf'),
                      ))}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>


        </div>
