{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="card card-primary card-outline">

      <!-- Header del panel -->
  <div class="card-header with-border">
    <h3 class="card-title">Listado</h3>
    <div class="card-tools pull-right">
      <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </div>
  </div>
  <div class="card-body">

    <?php if($valor==2) {?>
    @include('reportes.transaccional.resumen_compensacion_operaciones.re_rc_01_i_resumencompensaciontabla')
    <?php }else{ ?>
    @include('reportes.transaccional.resumen_compensacion_operaciones.re_rc_01_i_resumencompensaciontabla_all')
    <?php } ?>


    </div>
</div>
