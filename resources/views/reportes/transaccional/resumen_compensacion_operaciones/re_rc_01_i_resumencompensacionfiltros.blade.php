{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="card card-warning card-outline">
	<!-- inicio cabecera del panel -->
		<div class="card-header with-border">
			<h3 class="card-title">Filtros</h3>
			<div class="card-tools pull-right">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </div>
		</div>
	<!-- Fin cabecera del panel -->
	<!-- Inciio del cuerpo del panel -->
	<div class="card-body">
			<!-- inicio FORM -->
			<form action="../Transaccional/R0264" class="form-row">
				<!-- Inicio de la Primera columna de filtros -->
				<div class="row col-sm-12">
						<!-- inicio Primer filtro -->
						<div class="col-sm-6">
							<div class="form-group form-row">
							{!! Form::Label('bancos', 'Participante', array('class' => 'col-sm-3 col-form-label')) !!}

							<div class="col-sm-9">
    						{!! Form::select('bancos', $bancos, $Pparticipante, ['class' => 'form-control']) !!}
    						</div>
						</div>
					</div>
						<!-- Fin primer filtro -->

						<div class="col-sm-6">
							<div class="form-group form-row">
							{!! Form::Label('operaciones', 'Tipo de operación', array('class' => 'col-sm-3 col-form-label')) !!}
							<div class="col-sm-8">
    						{!! Form::select('operaciones', $operaciones, $Poperaciones, ['class' => 'form-control']) !!}
    						</div>

						</div>
					</div>
				</div>
				<!-- fin de la primera columna de filtros -->
				<!-- Inicio de la segunda columna de filtros -->
				<div class="row col-sm-12">
						<div class="col-sm-6">
		          <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
		            {!!Form::label('Fecha desde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
		            <div class="col-sm-6">
		              <div class="input-group date">
		                <input type="text" class="form-control group-date" id="fechai" name="fechai" readonly="readonly" value="{!! $Pfechai !!}">
										<div class="input-group-append">
		                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
						<div class="col-sm-6">
		          <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
		            {!!Form::label('Fecha hasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
		            <div class="col-sm-6">
		              <div class="input-group date">
		                <input type="text" class="form-control group-date" id="fechaf" name="fechaf" readonly="readonly" value="{!! $Pfechaf !!}">
										<div class="input-group-append">
		                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
				</div>
				<!-- Fin de la segunda columna de filtros -->
	</div>
	<div class="card-footer with-border">
		<div class="col-sm-12">
			<div class="text-right">
				<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
			</div>
		</div>
	</div>
</form>
	<!-- Fin del cuerpo del panel -->


</div>
