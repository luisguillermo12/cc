{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<table>
  <tr>
    <th>Banco Central de Venezuela</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th style="text-align: right;">Fecha:</th>
    <th><?=$informacion['fecha']?></th>
  </tr>
  <tr>
    <th>Gerencia de Tesorería</th>
  </tr>
  <tr>
    <th>Departamento Cámara de Compensación Electrónica</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th colspan="7" style="text-align: center;">CÁMARA DE COMPENSACIÓN REPORTE RESUMEN DE COMPENSACIÓN POR TIPO DE OPERACIÓN</th>
  </tr>
  <tr>
    <th colspan="7" style="text-align: center;">PERÍODO: Del <?=$informacion['fecha_param']?> al <?=$informacion['fecha_param2']?></th>
  </tr>
  <tr>
    <th colspan="7" style="text-align: center;">(Montos en Bolívares)</th>
  </tr>
</table >

<table>
  <tr>
    <th>Participante</th>
    <th style="border:1px solid #000000;">{{ $informacion['bancos'] }}</th>
  </tr>
  <tr></tr>
  <tr>
    <th>Operación</th>
    <th style="border:1px solid #000000;">{{ $informacion['operaciones'][0]->nombre_completo }}</th>
  </tr>
</table>


<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr>
        <th  style="background-color: #c2e7fc;border:1px solid #000000;"></th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Haber</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Debe</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;" >Saldo</th>
    </tr>
    <tr>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Participante</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Acreedor</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Deudor</th>
    </tr>
  </thead>
  <tbody>
      @foreach($informacion['parametrosd'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: left;" >{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->h_cantidad  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->h_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->d_cantidad  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->d_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->acreedor  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->deudor  }}</td>
        </tr>
        @else
        <tr>
          <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->acreedor  }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->deudor  }}</th>
        </tr>
        @endif
      @endforeach
  </tbody>
</table>

<table>
  <tr>
    <th>Participante</th>
    <th style="border:1px solid #000000;">{{ $informacion['bancos'] }}</th>
  </tr>
  <tr></tr>
  <tr>
    <th>Operación</th>
    <th style="border:1px solid #000000;">{{ $informacion['operaciones'][1]->nombre_completo }}</th>
  </tr>
</table>

<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr>
        <th  style="background-color: #c2e7fc;border:1px solid #000000;"></th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Haber</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Debe</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;" >Saldo</th>
    </tr>
    <tr>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Participante</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Acreedor</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Deudor</th>
    </tr>
  </thead>
  <tbody>
      @foreach($informacion['parametrosv'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr >
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: left;" >{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->h_cantidad  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->h_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->d_cantidad  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->d_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->acreedor  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->deudor  }}</td>
        </tr>
        @else
        <tr>
          <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->acreedor  }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->deudor  }}</th>
        </tr>
        @endif
      @endforeach
  </tbody>
</table>

<table>
  <tr>
    <th>Participante</th>
    <th style="border:1px solid #000000;">{{ $informacion['bancos'] }}</th>
  </tr>
  <tr></tr>
  <tr>
    <th>Operación</th>
    <th style="border:1px solid #000000;">{{ $informacion['operaciones'][2]->nombre_completo }}</th>
  </tr>
</table>

<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr>
        <th  style="background-color: #c2e7fc;border:1px solid #000000;"></th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Haber</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Debe</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;" >Saldo</th>
    </tr>
    <tr>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Participante</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Acreedor</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Deudor</th>
    </tr>
  </thead>
  <tbody>
      @foreach($informacion['parametrost'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: left;" >{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->h_cantidad  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->h_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->d_cantidad  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->d_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->acreedor  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->deudor  }}</td>
        </tr>
        @else
        <tr>
          <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->acreedor  }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->deudor  }}</th>
        </tr>
        @endif
      @endforeach
  </tbody>
</table>

<table>
  <tr>
    <th>Participante</th>
    <th style="border:1px solid #000000;">{{ $informacion['bancos'] }}</th>
  </tr>
  <tr>
    <th>Operación</th>
    <th style="border:1px solid #000000;">{{ $informacion['operaciones'][3]->nombre_completo }}</th>
  </tr>
</table>

<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr>
        <th  style="background-color: #c2e7fc;border:1px solid #000000;"></th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Haber</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Debe</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;" >Saldo</th>
    </tr>
    <tr>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Participante</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Acreedor</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Deudor</th>
    </tr>
  </thead>
  <tbody>
      @foreach($informacion['parametroscd'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: left;" >{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->h_cantidad}}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->h_montos  }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->d_cantidad }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->d_montos }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->acreedor }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->deudor }}</td>
        </tr>
        @else
        <tr>
          <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->acreedor }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->deudor }}</th>
        </tr>
        @endif
      @endforeach
  </tbody>
</table>

<table>
  <tr>
    <th>Participante</th>
    <th style="border:1px solid #000000;">{{ $informacion['bancos'] }}</th>
  </tr>
  <tr></tr>
  <tr>
    <th>Operación</th>
    <th style="border:1px solid #000000;">{{ $informacion['operaciones'][4]->nombre_completo }}</th>
  </tr>
</table>

<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr>
        <th  style="background-color: #c2e7fc;border:1px solid #000000;"></th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Haber</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Debe</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;" >Saldo</th>
    </tr>
    <tr>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Participante</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Acreedor</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Deudor</th>
    </tr>
  </thead>
  <tbody>
      @foreach($informacion['parametroscv'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: left;" >{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->h_cantidad }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->h_montos }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->d_cantidad }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->d_montos }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->acreedor }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->deudor }}</td>
        </tr>
        @else
        <tr>
          <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{$ai_objeto->d_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{$ai_objeto->d_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{$ai_objeto->h_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{$ai_objeto->h_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{$ai_objeto->acreedor }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{$ai_objeto->deudor }}</th>
        </tr>
        @endif
      @endforeach
  </tbody>
</table>
<table>
  <tr>
    <th>Participante</th>
    <th style="border:1px solid #000000;">{{ $informacion['bancos'] }}</th>
  </tr>
  <tr></tr>
  <tr>
    <th>Operación</th>
    <th style="border:1px solid #000000;">{{ $informacion['operaciones'][5]->nombre_completo }}</th>
  </tr>
</table>
<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr>
        <th  style="background-color: #c2e7fc;border:1px solid #000000;"></th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Haber</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;">Debe</th>
        <th  colspan="2" style="background-color: #c2e7fc;border: 1px solid #000000;text-align: center;" >Saldo</th>
    </tr>
    <tr>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Participante</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">Cantidad de operaciones</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Monto</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Acreedor</th>
        <th   style="background-color: #c2e7fc; text-align: center;border:1px solid #000000;">&nbsp;Deudor</th>
    </tr>
  </thead>
  <tbody>
      @foreach($informacion['parametrosct'] as $ai_objeto)
        @if(($ai_objeto->nombre_banco != null))
        <tr>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: left;" >{{ $ai_objeto->codigo_banco }} - {{ $ai_objeto->nombre_banco }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->h_cantidad }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->h_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->h_montos }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->d_cantidad }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->d_montos < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->d_montos }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;">{{ $ai_objeto->acreedor }}</td>
          <td style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4; text-align: right;<?php if($ai_objeto->deudor < 0){ ?> color:#ff0000; <?php } ?>">{{ $ai_objeto->deudor }}</td>
        </tr>
        @else
        <tr>
          <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_cantidad}}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->d_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_cantidad }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->h_montos }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->acreedor }}</th>
          <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $ai_objeto->deudor }}</th>
        </tr>
        @endif
      @endforeach
  </tbody>
</table>
