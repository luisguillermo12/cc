{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> R0264</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
        <li class="breadcrumb-item active">R0264</li>
      </ol>
    </div>
  </div>
</div>
{{-- @section('title') --}}
<div class="row">
  <div class="col-sm-12">
    <div class="card card-success card-outline">
      <div class="card-header with-border" style="background-color: #e8f9ea;">
        <h4 class="card-title" ><b>R0264 - Resumen de compensación por tipo de operación</b></h4>
      </div>
    </div>
  </div>
</div>
{{-- @endsection --}}

@endsection
@section('content')
	@include('reportes.transaccional.resumen_compensacion_operaciones.re_rc_01_i_resumencompensacionfiltros')

  @include('includes.messages')

	<?php if ($insercion!=null) {?>
		@include('reportes.transaccional.resumen_compensacion_operaciones.re_rc_01_i_resumencompensaciontabs')
	<?php } ?>
@endsection

@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_filtro_fecha')
  @include('includes.scripts.reportes.scripts_calendario_operaciones')
@endsection
