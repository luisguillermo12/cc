{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

{!! Html::style('css/pdf.css') !!}

<table border="0" width="100%" cellspacing="0" cellpadding="5">
  <thead style="background-color: #C2E7FC;">
    <tr>
       <th style="text-align: center;border: 1px solid #000000;" >C&#243;digo</th>
       <th style="text-align: center;border: 1px solid #000000;" >Nombre del reporte </th>
       <th style="text-align: center;border: 1px solid #000000;" >Formato</th>
       <th style="text-align: center;border: 1px solid #000000;" >Estado</th>
    </tr>
  </thead>
  <tbody>
    @foreach($caract_busqueda as $elemento)
     <tr style="border-top: 1px solid #f4f4f4;">
       <td style="text-align:center;">{{ $elemento['codigo'] }}</td>
       <td style="text-align:left;">{{ $elemento['nombre'] }}</td>
       <td style="text-align:center;">
         @if($elemento['PDF'])
          PDF /
         @endif
         @if($elemento['XLS'])
          EXCEL
         @endif
       </td>
       <td style="text-align:center;">
         @if($elemento['disponible'])
          Disponible
         @else
          Pendiente
         @endif
         @if($elemento['cobranza'])
          /Cobranza
         @endif
       </td>
     </tr>
    @endforeach
  </tbody>
</table>
