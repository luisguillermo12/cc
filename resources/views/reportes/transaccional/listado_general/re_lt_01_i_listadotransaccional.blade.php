{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> Transaccional</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
        <li class="breadcrumb-item active">Listado general</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
	<div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <h3 class="card-title">Listado transaccional</h3>
        </div>
        <div class="card-body">

          <table width='100%' id="ListadoR" class="table table-striped table-hover table-sm">
            <thead class="theadtable">
            <tr>
                <th><center>Código</center></th>
                <th>Nombre del reporte</th>
                <th style="text-align: left;">Formato</th>
                <th><center>Estado</center></th>
                <th><center>Acción</center></th>
            </tr>
            </thead>

            <tr>
              <td>{{ $caract_busqueda['R0256']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0256']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0256']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0256']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0256']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0256']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>
               <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0256']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0257']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0257']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0257']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0257']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0257']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0257']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0257']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0258']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0258']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0258']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0258']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0258']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0258']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0258']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0264']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0264']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0264']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0264']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0264']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0264']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0264']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0265']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0265']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0265']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0265']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0265']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0265']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0265']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0266']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0266']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0266']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0266']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0266']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0266']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0266']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0268']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0268']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0268']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0268']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>


               <td style="text-align: left;">
                 @if($caract_busqueda['R0268']['disponible'])
                 <span class="pull-right-container">
                   <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                 </span>
                 @else
                 <span class="pull-right-container">
                   <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                 </span>
                 @endif
                 @if($caract_busqueda['R0268']['cobranza'])
                 <br>
                 <span class="pull-right-container">
                   <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                 </span>
                 @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0268']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0272']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0272']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0272']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0272']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0272']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0272']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0272']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0273']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0273']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0273']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0273']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0273']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0273']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0273']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

     <!--         <tr>
              <td>R0274</td>

              <td>Razones técnicas de rechazo (Adicional al informe de requerimiento)</td>

              <td style="text-align: left;">
                <span >
                  <small class="badge bg-"><i class="fa fa-file-excel-o"></i></small>
                </span>
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
              </td>

              <td style="text-align: left;">
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span></td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url('Reportes/Listado/R0274')}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr> -->

          </table>
        </div>

        <div class="card-footer">
          <div class="card-tools text-center">
            <a href="{{url('Reportes/Transaccional/ListadoGeneral/Reporte',
              array(
                'tiporeporte' => 'XLS'
              )
            )}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
            <a href="{{url('Reportes/Transaccional/ListadoGeneral/Reporte',
              array(
                'tiporeporte' => 'PDF'
              )
            )}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
          </div>
        </div>

      </div>
    </div>
  </div>
@stop
{{--Fin--}}
@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_listado')
@stop
