{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<table>
  <thead>
    <tr>
      <th style="text-align:left;" colspan="2">Banco Central de Venezuela</th>
      <th style="text-align: right;">Fecha:</th>
      <th>{{ $param['fecha_actual'] }}</th>
    </tr>
    <tr>
      <th style="text-align:left;">Gerencia de Tesorer&#237;a</th>
    </tr>
    <tr>
      <th style="text-align:left;">Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th colspan="4" style="text-align:center;">C&#193;MARA DE COMPENSACI&#211;N REPORTE LISTADO DE REPORTES TRANSACCIONALES</th>
    </tr>
  </thead>
</table>

<table>
    <thead>
      <tr>
         <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >C&#243;digo</th>
         <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Nombre del reporte </th>
         <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Formato</th>
         <th style="text-align: center;border: 1px solid #000000;background-color: #C2E7FC;" >Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($param['caract_busqueda'] as $elemento)
       <tr>
         <td style="text-align:center;border-top: 1px solid #f4f4f4;">{{ $elemento['codigo'] }}</td>
         <td style="text-align:left;border-top: 1px solid #f4f4f4;">{{ $elemento['nombre'] }}</td>
         <td style="text-align:center;border-top: 1px solid #f4f4f4;">
           @if($elemento['PDF'])
            PDF /
           @endif
           @if($elemento['XLS'])
            EXCEL
           @endif
         </td>
         <td style="text-align:center;border-top: 1px solid #f4f4f4;">
           @if($elemento['disponible'])
            Disponible
           @else
            Pendiente
           @endif
           @if($elemento['cobranza'])
            /Cobranza
           @endif
         </td>
       </tr>
      @endforeach
    </tbody>
  </table>
