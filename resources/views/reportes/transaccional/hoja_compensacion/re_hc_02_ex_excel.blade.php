{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<table>
  <thead>
    <tr>
      <th style="text-align:left;">Banco Central de Venezuela</th>
      <th></th>
      <th></th>
      <th></th>
      <th style="text-align: right;">Fecha:</th>
      <th><?=$param['fecha_actual']?></th>
    </tr>
    <tr>
      <th style="text-align:left;">Gerencia de Tesorer&#237;a</th>
    </tr>
    <tr>
      <th style="text-align:left;">Departamento C&#225;mara de Compensaci&#243;n Electr&#243;nica</th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th colspan="6" style="text-align:center;">C&#193;MARA DE COMPENSACI&#211;N REPORTE HOJA DE COMPENSACI&#211;N</th>
    </tr>
    <tr>
      <th colspan="6" style="text-align:center;">PER&#205;ODO: Del <?=$param['fecha_desde']?> al <?=$param['fecha_hasta']?></th>
    </tr>
    <tr>
      <th colspan="6" style="text-align:center;">(Montos en Bol&#237;vares)</th>
    </tr>
  </thead>
</table>

<?php $encabezado = 0; $opActual = ''; $primero = 0; ?>
@if(!empty($param['r0265_parametros']))
  @foreach ($param['r0265_parametros'] as $hc_objeto)
    @if($opActual != $hc_objeto->operacion_tipo_codigo)
      <?php
        $opActual = $hc_objeto->operacion_tipo_codigo;
        $encabezado = 0;
      ?>
    @endif
    @if($encabezado == 0)
      <table class="table table-hover" style="font-size: 14px !important;">
        <thead class="thead-default">
          <tr>
            <th style="padding: 8px; text-align: left;">Participante:</th>
            <th style="padding: 8px; text-align: left;border: 1px solid #000000;"><?=$param['descrip_banco']?></th>
          </tr>
          <tr>&nbsp;</tr>
          <tr>
            <th style="padding: 8px; text-align: left;">Tipo de operaci&#243;n:</th>
            <th style="padding: 8px; text-align: left;border: 1px solid #000000;"><?=$hc_objeto->operacion_tipo_codigo.' - '.$hc_objeto->operacion_tipo_nombre?></th>
          </tr>
        </thead>
      </table>
      <br>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead class="thead-default">
            <tr style="background-color:#c2e7fc;">
              <th style="text-align: center;border: 1px solid #000000;"></th>
              <th colspan="2" style="text-align: center;border: 1px solid #000000;">Operaciones enviadas</th>
              <th colspan="2" style="text-align: center;border: 1px solid #000000;">Operaciones recibidas</th>
              <th style="text-align: center;border: 1px solid #000000;"></th>
            </tr>
            <tr>
              <th style="text-align: center;background-color:#c2e7fc;border: 1px solid #000000;">Participante contraparte</th>
              <th style="text-align: center;border: 1px solid #000000;">Cantidad</th>
              <th style="text-align: center;border: 1px solid #000000;">Monto</th>
              <th style="text-align: center;border: 1px solid #000000;">Cantidad</th>
              <th style="text-align: center;border: 1px solid #000000;">Monto</th>
              <th style="text-align: center;background-color:#c2e7fc;border: 1px solid #000000;">Saldo</th>
            </tr>
          </thead>
          <tbody>
          <?php $encabezado = 1; $primero = 1; //luego se muestra la primera linea de recorrido?>
            <tr>
              <td>{{ $hc_objeto->banco_codigo.' - '.$hc_objeto->banco_nombre }}</td>
              <td style="text-align: right;">{{ $hc_objeto->cantidad_recibida }}</td>
              <td style="text-align: right;">{{ $hc_objeto->monto_recibido }}</td>
              <td style="text-align: right;">{{ $hc_objeto->cantidad_enviada }}</td>
              <td style="text-align: right;">{{ $hc_objeto->monto_enviado }}</td>
              <td style="text-align: right;<?php if($hc_objeto->saldo < 0){ ?> color:#ff0000; <?php } ?>">{{ $hc_objeto->saldo }}</td>
            </tr>
    @else
            @if(($hc_objeto->banco_nombre != null) && ($hc_objeto->operacion_tipo_id != null))
            <tr>
              <td>{{ $hc_objeto->banco_codigo.' - '.$hc_objeto->banco_nombre }}</td>
              <td style="text-align: right;">{{ $hc_objeto->cantidad_recibida }}</td>
              <td style="text-align: right;">{{ $hc_objeto->monto_recibido }}</td>
              <td style="text-align: right;">{{ $hc_objeto->cantidad_enviada }}</td>
              <td style="text-align: right;">{{ $hc_objeto->monto_enviado }}</td>
              <td style="text-align: right;<?php if($hc_objeto->saldo < 0){ ?> color:#ff0000; <?php } ?>">{{ $hc_objeto->saldo }}</td>
            </tr>
            @else
            <tr>
              <th style="text-align: left;background-color:#c2e7fc;border: 1px solid #000000;">TOTAL</th>
              <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $hc_objeto->cantidad_recibida }}</th>
              <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $hc_objeto->monto_recibido }}</th>
              <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $hc_objeto->cantidad_enviada }}</th>
              <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $hc_objeto->monto_enviado }}</th>
              <th style="text-align: right;background-color:#c2e7fc;border: 1px solid #000000;">{{ $hc_objeto->saldo }}</th>
            </tr>
          </tbody>
        </table>
      </div>
            @endif
    @endif
  @endforeach
@endif
