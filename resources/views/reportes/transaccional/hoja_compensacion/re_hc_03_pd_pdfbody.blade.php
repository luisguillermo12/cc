{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

{!! Html::style('css/pdf.css') !!}

<style type="text/css">
    div.page
    {
        page-break-after: always;
        page-break-inside: avoid;
    }
</style>

<?php $encabezado = 0; $opActual = ''; $primero = 0; $cantOpActual = 0; $opCount = 0; $opActualCod = '';?>

@foreach ($r0265_parametros as $hc_objeto) <?php //si la operacion que se tiene guardada no es igual, es otra operacion nueva a la que se le va a dar un salto de pagina al final de los datos ?>
  @if(($opActualCod != $hc_objeto->operacion_tipo_codigo) && ($hc_objeto->banco_nombre != null) && ($hc_objeto->operacion_tipo_id != null))
    <?php $opCount = $opCount + 1; //se cuentan las operaciones para saber cuantas hay, y evitar el salto de pagina en la ultima hoja del pdf, para la ultima operacion?>
    <?php $opActualCod = $hc_objeto->operacion_tipo_codigo; ?>
  @endif
@endforeach

@if(!empty($r0265_parametros))
  @foreach ($r0265_parametros as $hc_objeto)
    @if($opActual != $hc_objeto->operacion_tipo_codigo)
      <?php
        $opActual = $hc_objeto->operacion_tipo_codigo;
        $encabezado = 0;
      ?>
    @endif
    @if($encabezado == 0)
      <?php $cantOpActual = $cantOpActual + 1; ?>
      @if($opCount != $cantOpActual) <?php //si es la ultima operacion, no se añade el salto de pagina al finalizar los datos?>
    <div class="page">
      @endif
      <table class="table table-hover" style="font-size: 14px !important;">
        <thead class="thead-default">
          <tr>
            <th style="text-align: left;">Participante:</th>
            <th style="text-align: left;border: 1px solid #000000"><?=$descrip_banco?></th>
          </tr>
          <tr>&nbsp;</tr>
          <tr>
            <th style="text-align: left;">Tipo de operaci&#243;n:</th>
            <th style="text-align: left;border: 1px solid #000000"><?=$hc_objeto->operacion_tipo_codigo.' - '.$hc_objeto->operacion_tipo_nombre?></th>
          </tr>
        </thead>
      </table>
      <br>
      <div class="table-responsive">
        <table class="table table-hover table-bordered">
          <thead class="thead-default">
            <tr style="background-color:#c2e7fc;">
              <th rowspan="2" style="border: 1px solid #000000;text-align: center;">Participante contraparte</th>
              <th colspan="2" style="border: 1px solid #000000;text-align: center;">Operaciones enviadas</th>
              <th colspan="2" style="border: 1px solid #000000;text-align: center;">Operaciones recibidas</th>
              <th rowspan="2" style="border: 1px solid #000000;text-align: center;">Saldo</th>
            </tr>
            <tr>
              <th style="text-align: center;border: 1px solid #000000;">Cantidad</th>
              <th style="text-align: center;border: 1px solid #000000;">Monto</th>
              <th style="text-align: center;border: 1px solid #000000;">Cantidad</th>
              <th style="text-align: center;border: 1px solid #000000;">Monto</th>
            </tr>
          </thead>
          <tbody>
          <?php $encabezado = 1; $primero = 1; //luego se muestra la primera linea de recorrido?>
            <tr style="border-top: 1px solid #f4f4f4;">
              <td>{{ $hc_objeto->banco_codigo.' - '.$hc_objeto->banco_nombre }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_recibida, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->monto_recibido, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_enviada, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->monto_enviado, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;<?php if($hc_objeto->saldo < 0){ ?> color:#ff0000; <?php } ?>">{{ number_format($hc_objeto->saldo, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
            </tr>
    @else
            @if(($hc_objeto->banco_nombre != null) && ($hc_objeto->operacion_tipo_id != null))
            <tr style="border-top: 1px solid #f4f4f4;">
              <td>{{ $hc_objeto->banco_codigo.' - '.$hc_objeto->banco_nombre }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_recibida, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->monto_recibido, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_enviada, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;">{{ number_format($hc_objeto->monto_enviado, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              <td style="text-align: right;<?php if($hc_objeto->saldo < 0){ ?> color:#ff0000; <?php } ?>">{{ number_format($hc_objeto->saldo, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
            </tr>
            @else
            <tr style="background-color:#c2e7fc;">
              <th style="text-align: left;border: 1px solid #000000;">TOTAL</th>
              <th style="text-align: right;border: 1px solid #000000;">{{ number_format($hc_objeto->cantidad_recibida, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
              <th style="text-align: right;border: 1px solid #000000;">{{ number_format($hc_objeto->monto_recibido, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
              <th style="text-align: right;border: 1px solid #000000;">{{ number_format($hc_objeto->cantidad_enviada, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
              <th style="text-align: right;border: 1px solid #000000;">{{ number_format($hc_objeto->monto_enviado, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
              <th style="text-align: right;border: 1px solid #000000;">{{ number_format($hc_objeto->saldo, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
            </tr>
          </tbody>
        </table>
      </div>
              @if($opCount != $cantOpActual)<?php //si es la ultima operacion, no se añade el salto de pagina al finalizar los datos?>
    </div>
              @endif
            @endif
    @endif
  @endforeach
@endif
</body>
</html>
