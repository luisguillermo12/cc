{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<?php $encabezado = 0; $opActual = ''; $primero = 0; ?>
@if(!empty($r0265_parametros))
  <div class="tab-content">
  @foreach ($r0265_parametros as $hc_objeto)
    @if($opActual != $hc_objeto->operacion_tipo_codigo)
      <?php
        $opActual = $hc_objeto->operacion_tipo_codigo;
        $encabezado = 0;
      ?>
    @endif
    @if($encabezado == 0)
      <div id="home{{ $hc_objeto->operacion_tipo_codigo }}" class="tab-pane fade {{ $primero == 0 ? 'active show' : '' }}" role="tabpanel">
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-sm">
            <thead class="theadtable">
              <tr>
                <tr>
                  <th rowspan="2" style="text-align: center;background-color:#c2e7fc;">Participante contraparte</th>
                  <th colspan="2" style="text-align: center;background-color:#c2e7fc;">Operaciones enviadas</th>
                  <th colspan="2" style="text-align: center;background-color:#c2e7fc;">Operaciones recibidas</th>
                  <th rowspan="2" style="text-align: center;background-color:#c2e7fc;">Saldo</th>
                </tr>
              </tr>
              <tr>
                <th style="text-align: center;">Cantidad</th>
                <th style="text-align: center;">Monto</th>
                <th style="text-align: center;">Cantidad</th>
                <th style="text-align: center;">Monto</th>
              </tr>
            </thead>
            <tbody>
            <?php $encabezado = 1; $primero = 1; //luego se muestra la primera linea de recorrido?>
              <tr>
                <td>{{ $hc_objeto->banco_codigo.' - '.$hc_objeto->banco_nombre }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_recibida, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->monto_recibido, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_enviada, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->monto_enviado, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;<?php if($hc_objeto->saldo < 0){ ?> color:#ff0000; <?php } ?>">{{ number_format($hc_objeto->saldo, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              </tr>
    @else
              @if(($hc_objeto->banco_nombre != null) && ($hc_objeto->operacion_tipo_id != null))
              <tr>
                <td>{{ $hc_objeto->banco_codigo.' - '.$hc_objeto->banco_nombre }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_recibida, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->monto_recibido, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->cantidad_enviada, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;">{{ number_format($hc_objeto->monto_enviado, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
                <td style="text-align: right;<?php if($hc_objeto->saldo < 0){ ?> color:#ff0000; <?php } ?>">{{ number_format($hc_objeto->saldo, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
              </tr>
              @else
              <tr style="background-color:#c2e7fc;">
                <th style="text-align: left;">TOTAL</th>
                <th style="text-align: right;">{{ number_format($hc_objeto->cantidad_recibida, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th style="text-align: right;">{{ number_format($hc_objeto->monto_recibido, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th style="text-align: right;">{{ number_format($hc_objeto->cantidad_enviada, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th style="text-align: right;">{{ number_format($hc_objeto->monto_enviado, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
                <th style="text-align: right;">{{ number_format($hc_objeto->saldo, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
              </tr>
            </tbody>
          </table>
        </div><!--table-responsive-->
      </div>
              @endif
    @endif
  @endforeach
  </div>
@endif
