{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio Hoja de Compensación--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-line-chart"></i> R0265</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Transaccional</a></li>
          <li class="breadcrumb-item active">R0265</li>
        </ol>
      </div>
    </div>
  </div>
  {{-- @section('title') --}}
  <div class="row">
    <div class="col-sm-12">
      <div class="card card-success card-outline">
        <div class="card-header with-border" style="background-color: #e8f9ea;">
          <h4 class="card-title" ><b>R0265 - Hoja de compensación</b></h4>
        </div>
      </div>
    </div>
  </div>
  {{-- @endsection --}}
@endsection

{{--Inicio de la seccion Listados y Gráficas--}}

@section('content')
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-MONI-1.3-->

          {{--Inicio de la seccion Filtros--}}

          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
          <h3 class="card-title">Filtros</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div><!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
          {!! Form::open(['method'=>'GET', 'class' => 'form-row', 'role' =>'form']) !!}
          <div class="col-sm-6">
            <div class="form-group form-row">
              {!!Form::label('Banco', 'Participante', array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-9">
                {!!Form::select('cod_banco',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control']) !!}
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-row">
              {!!Form::label('Tipo de operación', 'Tipo de operación', array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-8">
                {!! Form::select('codigo_operacion',$listaOperacion,$codigo_operacion,['id'=>'codigo','class'=>'form-control']) !!}
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
              {!!Form::label('FechaDesde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
                <div class="input-group date">
                  <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! $fe_desde !!}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
              {!!Form::label('FechaHasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
                <div class="input-group date">
                  <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! $fe_hasta !!}">
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer with-border">
          <div class="col-sm-12">
            <div class="text-right">
              <input type="hidden" name="busqueda" id="busqueda" value="1"/>
              <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    {{--Fin de la seccion Filtros--}}

    @include('includes.messages')

    {{--Inicio de la seccion Listado--}}
    @if (!is_null($r0265_parametros))
    @if(!empty($r0265_parametros))
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header with-border">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div>
        <div class="card-body">
          {{--Inicio de la seccion Reporte total de archivos--}}

          <ul class="nav nav-pills pills-list mb-3" role="tablist">
            <?php $opActualTab = ''; $primeroTab = 0; ?>
            @foreach ($r0265_parametros as $hc_objeto)
              @if(($opActualTab != $hc_objeto->operacion_tipo_codigo) && ($hc_objeto->banco_nombre != null) && ($hc_objeto->operacion_tipo_id != null))
                <li class="nav-item"><a class="nav-link {{ $primeroTab == 0 ? 'active' : '' }}" data-toggle="pill" href="#home{{ $hc_objeto->operacion_tipo_codigo }}">{{ $hc_objeto->operacion_tipo_codigo }} - {{ $hc_objeto->operacion_tipo_nombre }}</a></li>
                <?php $opActualTab = $hc_objeto->operacion_tipo_codigo; $primeroTab = 1; ?>
              @endif
            @endforeach
          </ul>

          {{-- Inclusion de codigo para tabla de datos - Reporte Hoja de compensación --}}
          @include('reportes.transaccional.hoja_compensacion.re_hc_01_i_hojacompensaciontabla')

          <hr class="col-sm-12">

          <div class="col-sm-12 text-center">
              <a href="{{url('Reportes/Transaccional/R0265/Reporte',
                array('cod_banco' => $cod_banco,
                  'codigo_operacion' => $codigo_operacion,
                  'fe_desde' => $fe_desde = Input::get('FechaDesde') == null ? '*' : Input::get('FechaDesde'),
                  'fe_hasta' => $fe_hasta = Input::get('FechaHasta') == null ? '*' : Input::get('FechaHasta'),
                  'tipo_reporte' => $tipo_reporte = 'XLS'
                )
              )}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
              <a href="{{url('Reportes/Transaccional/R0265/Reporte',
                array('cod_banco' => $cod_banco,
                  'codigo_operacion' => $codigo_operacion,
                  'fe_desde' => $fe_desde = Input::get('FechaDesde') == null ? '*' : Input::get('FechaDesde'),
                  'fe_hasta' => $fe_hasta = Input::get('FechaHasta') == null ? '*' : Input::get('FechaHasta'),
                  'tipo_reporte' => $tipo_reporte = 'PDF'
                )
              )}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
          </div>
        </div>
      </div>
    </div>
    @else
    <div class="col-md-12">
        <div class="alert alert-danger" style="text-align: center">No existe información</div>
    </div>
    @endif

    @endif
{{--Fin de la seccion Listado--}}
  </div>


@stop
{{--Fin Hoja de Compensación--}}

@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_filtro_fecha')
@stop
