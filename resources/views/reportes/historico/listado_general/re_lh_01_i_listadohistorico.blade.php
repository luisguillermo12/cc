{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> Histórico</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Historico/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Historico/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Listado general</a></li>
        <li class="breadcrumb-item active">Histórico</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
	<!-- row -->
	<div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <h3 class="card-title">Listado histórico</h3>
        </div>
        <div class="card-body">

          <table width='100%' id="ListadoR" class="table table-striped table-hover table-sm">
            <thead class="theadtable">
            <tr>
                <th><center>Código</center></th>
                <th>Nombre del reporte</th>
                <th style="text-align: left;">Formato</th>
                <th><center>Estado</center></th>
                <th><center>Acción</center></th>
            </tr>
            </thead>
            <tr>
              <td>{{ $caract_busqueda['R0251']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0251']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0251']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0251']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0251']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0251']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0251']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0252']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0252']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0252']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0252']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0252']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0252']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0252']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0253']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0253']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0253']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0253']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0253']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0253']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0253']['url'])}} " data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0254']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0254']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0254']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0254']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0254']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0254']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

               <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0254']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>


            <tr>
              <td>{{ $caract_busqueda['R0255']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0255']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0255']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0255']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0255']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0255']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0255']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0256']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0256']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0256']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0256']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0256']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0256']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

               <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0256']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0257']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0257']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0257']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0257']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0257']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0257']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0257']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0258']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0258']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0258']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0258']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0258']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0258']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0258']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0259']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0259']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0259']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0259']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0259']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0259']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0259']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0260']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0260']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0260']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0260']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0260']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0260']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0260']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0261']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0261']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0261']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0261']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0261']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0261']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0261']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0262']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0262']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0262']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0262']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0262']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0262']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0262']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0263']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0263']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0263']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0263']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0263']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0263']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0263']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0264']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0264']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0264']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0264']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0264']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0264']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0264']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

            <tr>
              <td>{{ $caract_busqueda['R0265']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0265']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0265']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0265']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0265']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0265']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0265']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0267']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0267']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0267']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0267']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>


              <td style="text-align: left;">
                @if($caract_busqueda['R0267']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0267']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0267']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0268']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0268']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0268']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0268']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>


              <td style="text-align: left;">
                 @if($caract_busqueda['R0268']['disponible'])
                 <span class="pull-right-container">
                   <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                 </span>
                 @else
                 <span class="pull-right-container">
                   <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                 </span>
                 @endif
                 @if($caract_busqueda['R0268']['cobranza'])
                 <br>
                 <span class="pull-right-container">
                   <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                 </span>
                 @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0268']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0269']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0269']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0269']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0269']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0269']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0269']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0269']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0270']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0270']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0270']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0270']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0270']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0270']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0270']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0271']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0271']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0271']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0271']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0271']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0271']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0271']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

              <tr>
              <td>{{ $caract_busqueda['R0275']['codigo'] }}</td>

              <td>{{ $caract_busqueda['R0275']['nombre'] }}</td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0275']['PDF'])
                <span >
                  <small class="badge bg-danger"><i class="fa fa-file-pdf-o"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0275']['XLS'])
                <span >
                  <small class="badge bg-success"><i class="fa fa-file-excel-o"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: left;">
                @if($caract_busqueda['R0275']['disponible'])
                <span class="pull-right-container">
                  <small class="badge bg-success">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @else
                <span class="pull-right-container">
                  <small class="badge bg-danger">Pendiente &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
                @if($caract_busqueda['R0275']['cobranza'])
                <br>
                <span class="pull-right-container">
                  <small class="badge bg-warning">Cobranza &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
                @endif
              </td>

              <td style="text-align: center;">
                <a class="btn-sm btn-info" href="{{url($caract_busqueda['R0275']['url'])}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a></td>
            </tr>

          </table>

        </div><!--table-responsive-->

        <div class="card-footer">
          <div class="card-tools text-center">
             <a href="{{url('Reportes/Historico/ListadoGeneral/Reporte',
               array(
                 'tiporeporte' => 'XLS'
               )
             )}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
             <a href="{{url('Reportes/Historico/ListadoGeneral/Reporte',
               array(
                 'tiporeporte' => 'PDF'
               )
             )}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
           </div>
         </div>

      </div>
    </div>
  </div>
@stop
{{--Fin--}}
@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_listado')
@stop
