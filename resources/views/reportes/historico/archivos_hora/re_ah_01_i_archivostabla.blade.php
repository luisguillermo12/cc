{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="table-responsive" >
  <table class="table table-bordered table-hover table-sm" style="font-size: 14px !important;">
    <thead class="thead-default">
      <tr>
        <th style="padding: 8px; text-align: center;">Participante</th>
        <th style="padding: 8px; text-align: center;">Tipo de archivo</th>
        <th style="padding: 8px; text-align: center;">Rango de horas</th>
        <th style="padding: 8px; text-align: center;">Cantidad de archivos</th>
        <th style="padding: 8px; text-align: center;">Monto total</th>
      </tr>
    </thead>
    <tbody>
      @if(!empty($ARCHIVOS_INTERVALO))
        @foreach($ARCHIVOS_INTERVALO as $ai_objeto)
          @if(($ai_objeto->banco_nombre != null) && ($ai_objeto->archivo_tipo != null))
          <tr>
            <td style="text-align: left;">{{ $ai_objeto->banco_codigo != '*' ? $ai_objeto->banco_codigo.' - '.$ai_objeto->banco_nombre : $ai_objeto->banco_codigo }}</td>
            <td style="text-align: center;">{{ $ai_objeto->archivo_tipo }}</td>
            <td style="text-align: center;">{{ $ai_objeto->rango_nombre }}</td>
            <td style="text-align: right;">{{ number_format($ai_objeto->archivo_cantidad, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
            <td style="text-align: right;">{{ number_format($ai_objeto->archivo_monto, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
          </tr>
          @else
          <tr style="background-color:#CBCACA">
            <th colspan="3" style="text-align: left;">{{ $ai_objeto->banco_codigo }}</th>
            <th style="text-align: right;">{{ number_format($ai_objeto->archivo_cantidad, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
            <th style="text-align: right;">{{ number_format($ai_objeto->archivo_monto, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
          </tr>
          @endif
        @endforeach
      @endif
    </tbody>
  </table>
  <div class="table-responsive" >
   <table class="table table-bordered table-hover table-sm" style="font-size: 14px !important;">
     <thead class="thead-default">
       <tr>
         <th colspan="2">RESUMEN GENERAL</th>
         <th colspan="2" style="text-align: center;">Cantidad de archivos:&nbsp;&nbsp;<?=number_format($cantidad_archivos, 0, $monedas->separador_decimales, $monedas->separador_miles)?></th>
         <th colspan="2" style="text-align: center;">Monto total:&nbsp;&nbsp;<?=number_format($monto_total_archivos, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)?></th>
       </tr>
     </thead>
   </table>
  </div>
  @if(!empty($ARCHIVOS_INTERVALO))
  <div class="pull-right">
    <?php echo $ARCHIVOS_INTERVALO->render(); ?>
  </div>
  @endif
</div>
