{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="card card-secondary" style="border: 1px solid #dfdfdf; box-shadow: 0 0 0px;">
  <div class="card-header">
    <h4 class="card-title">Archivos presentados</h4>
  </div>
  <div class="card-content table-responsive">
    <div id="container1" style="width:100%"></div>
  </div>
</div>
<div class="card card-secondary" style="border: 1px solid #dfdfdf; box-shadow: 0 0 0px;">
  <div class="card-header">
    <h4 class="card-title">Archivos devueltos</h4>
  </div>
  <div class="card-content table-responsive">
    <div id="container2" style="width:100%"></div>
  </div>
</div>
