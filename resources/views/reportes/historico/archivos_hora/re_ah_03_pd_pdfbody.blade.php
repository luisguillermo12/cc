{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

{!! Html::style('css/pdf.css') !!}

  <table class="table table-hover" style="font-size: 14px !important;">
    <thead class="thead-default">
      <tr style="background-color:#c2e7fc;">
        <th style="padding: 8px; text-align: center;border: 1px solid #000000;">Participante</th>
        <th style="padding: 8px; text-align: center;border: 1px solid #000000;">Tipo de archivo</th>
        <th style="padding: 8px; text-align: center;border: 1px solid #000000;">Rango de horas</th>
        <th style="padding: 8px; text-align: center;border: 1px solid #000000;">Cantidad de archivos</th>
        <th style="padding: 8px; text-align: center;border: 1px solid #000000;">Monto total</th>
      </tr>
    </thead>
    <tbody>
      @foreach($ARCHIVOS_INTERVALO as $ai_objeto)
        @if(($ai_objeto->banco_nombre != null) && ($ai_objeto->archivo_tipo != null))
        <tr style="border-bottom: 1px solid #f4f4f4;border-top: 1px solid #f4f4f4;">
          <td style="text-align: left;">{{ $ai_objeto->banco_codigo != '*' ? $ai_objeto->banco_codigo.' - '.$ai_objeto->banco_nombre : $ai_objeto->banco_codigo }}</td>
          <td style="text-align: center;">{{ $ai_objeto->archivo_tipo }}</td>
          <td style="text-align: center;">{{ $ai_objeto->rango_nombre }}</td>
          <td style="text-align: right;">{{ number_format($ai_objeto->archivo_cantidad, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
          <td style="text-align: right;">{{ number_format($ai_objeto->archivo_monto, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</td>
        </tr>
        @else
        <tr>
          <th colspan="3" style="text-align: left;background-color:#CBCACA;border: 1px solid #000000;">{{ $ai_objeto->banco_codigo }}</th>
          <th style="text-align: right;background-color:#CBCACA;border: 1px solid #000000;">{{ number_format($ai_objeto->archivo_cantidad, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
          <th style="text-align: right;background-color:#CBCACA;border: 1px solid #000000;">{{ number_format($ai_objeto->archivo_monto, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}</th>
        </tr>
        @endif
      @endforeach
  </table>
  <br>
  <div class="table-responsive" >
   <table class="table table-striped" style="font-size: 14px !important;">
     <thead class="thead-default">
       <tr style="background-color:#c2e7fc;">
         <th colspan="2" style="border: 1px solid #000000;">RESUMEN GENERAL</th>
         <th colspan="2" style="text-align: center;border: 1px solid #000000;">Cantidad de archivos:&nbsp;&nbsp;<?=number_format($cantidad_archivos, 0, $monedas->separador_decimales, $monedas->separador_miles)?></th>
         <th colspan="2" style="text-align: center;border: 1px solid #000000;">Monto total:&nbsp;&nbsp;<?=number_format($monto_total_archivos, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles)?></th>
       </tr>
     </thead>
   </table>
  </div>
 </body>
</html>
