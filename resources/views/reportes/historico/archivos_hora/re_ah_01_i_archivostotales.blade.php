{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="card card-secondary" style="border: 1px solid #dfdfdf; box-shadow: 0 0 0px;">
  <div class="card-header with-border">
    <h3 class="card-title">Totales</h3>
  </div>
  <div class="card-body row">
    @if(!empty($TotalIcom1))
      @foreach ($TotalIcom1 as $totalicom1)
      <div class="col-12 col-sm-6 col-md-6">
        <div class="info-box">
          <span class="info-box-icon bg-success elevation-1"><i class="fa fa-file-o"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Presentado</span>
            <span class="info-box-number">{{ number_format($totalicom1->total_icom1, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</span>
          </div>
        </div>
      </div>
      @endforeach
    @endif
    @if(!empty($TotalIcom2))
      @foreach ($TotalIcom2 as $totalicom2)
      <div class="col-12 col-sm-6 col-md-6">
        <div class="info-box">
          <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-file-o"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Devuelto</span>
            <span class="info-box-number">{{ number_format($totalicom2->total_icom2, 0, $monedas->separador_decimales, $monedas->separador_miles) }}</span>
          </div>
        </div>
      </div>
      @endforeach
    @endif
  </div>
</div>
