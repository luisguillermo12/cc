{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<table>
  <tr>
    <th>Banco Central de Venezuela</th>
    <th></th>
    <th></th>
    <th style="text-align: right;">Fecha:</th>
    <th><?=$param['fecha_actual']?></th>
  </tr>
  <tr>
    <th>Gerencia de Tesorería</th>
  </tr>
  <tr>
    <th>Departamento Cámara de Compensación Electrónica</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th colspan="5" style="text-align: center;">CÁMARA DE COMPENSACIÓN REPORTE ARCHIVOS POR HORA</th>
  </tr>
  <tr>
    <th colspan="5" style="text-align: center;">PERÍODO: Del <?=$param['fe_desde']?> al <?=$param['fe_hasta']?></th>
  </tr>
  <tr>
    <th colspan="5" style="text-align: center;">(Montos en Bolívares)</th>
  </tr>
</table >
<table class="table table-striped" style="font-size: 14px !important;">
  <thead class="thead-default">
    <tr style="background-color:#c2e7fc;border: 1px solid #000000;">
      <th style="text-align: center;border: 1px solid #000000;">Participante</th>
      <th style="text-align: center;border: 1px solid #000000;">Tipo de archivo</th>
      <th style="text-align: center;border: 1px solid #000000;">Rango de horas</th>
      <th style="text-align: center;border: 1px solid #000000;">Cantidad de archivos</th>
      <th style="text-align: center;border: 1px solid #000000;">Monto total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($param['ARCHIVOS_INTERVALO'] as $ai_objeto)
      @if(($ai_objeto->banco_nombre != null) && ($ai_objeto->archivo_tipo != null))
      <tr>
        <td style="text-align: left;">{{ $ai_objeto->banco_codigo != '*' ? $ai_objeto->banco_codigo.' - '.$ai_objeto->banco_nombre : $ai_objeto->banco_codigo }}</td>
        <td style="text-align: center;">{{ $ai_objeto->archivo_tipo }}</td>
        <td style="text-align: center;">{{ $ai_objeto->rango_nombre }}</td>
        <td style="text-align: right;">{{ $ai_objeto->archivo_cantidad }}</td>
        <td style="text-align: right;">{{ $ai_objeto->archivo_monto }}</td>
      </tr>
      @else
      <tr>
        <th colspan="3" style="text-align: left;background-color:#CBCACA;border: 1px solid #000000;">{{ $ai_objeto->banco_codigo }}</th>
        <th style="text-align: right;background-color:#CBCACA;border: 1px solid #000000;">{{ $ai_objeto->archivo_cantidad }}</th>
        <th style="text-align: right;background-color:#CBCACA;border: 1px solid #000000;">{{ $ai_objeto->archivo_monto }}</th>
      </tr>
      @endif
    @endforeach
</table>

<div class="table-responsive" >
 <table class="table table-striped" style="font-size: 14px !important;">
   <thead class="thead-default">
     <tr style="background-color:#c2e7fc;">
       <th style="border: 1px solid #000000;">RESUMEN GENERAL</th>
       <th colspan="2" style="text-align: center;border: 1px solid #000000;">Cantidad de archivos:&nbsp;&nbsp;<?=number_format($param['cantidad_archivos'], 0, $param['monedas']->separador_decimales, $param['monedas']->separador_miles)?></th>
       <th colspan="2" style="text-align: center;border: 1px solid #000000;">Monto total:&nbsp;&nbsp;<?=number_format($param['monto_total_archivos'], $param['monedas']->num_decimales, $param['monedas']->separador_decimales, $param['monedas']->separador_miles)?></th>
     </tr>
   </thead>
 </table>
</div>
