{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio Archivos por horas--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-line-chart"></i> R0251</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Historico/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Reportes</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Reportes/Historico/ListadoGeneral') }}"><i class="fa fa-line-chart"></i> Histórico</a></li>
        <li class="breadcrumb-item active">R0251</li>
      </ol>
    </div>
  </div>
</div>
{{-- @section('title') --}}
<div class="row">
  <div class="col-sm-12">
    <div class="card card-success card-outline">
      <div class="card-header with-border" style="background-color: #e8f9ea;">
        <h4 class="card-title" ><b>R0251 - Archivos por hora</b></h4>
      </div>
    </div>
  </div>
</div>
{{-- @endsection --}}

@endsection
{{--Inicio de la seccion Filtros--}}

@section('content')
<div class="row">
<div class="col-md-12">
  <!-- general form elements -->
  <div class="card card-warning card-outline">
    <div class="card-header with-border">
      <!--codigo gui MOD-MONI-1.3-->
      <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
      <h3 class="card-title">Filtros</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div><!-- /.card-header -->
    <div class="card-body">
      <form role="form" class="form-row">
        <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Banco', 'Participante', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-9">
              {!!Form::select('cod_banco',$listaParticipante,$cod_banco,['id'=>'cod_banco','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row">
            {!!Form::label('Tipo de Archivo', 'Tipo de archivo', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-8">
              {!! Form::select('cod_archivo',$listaArchivo,$cod_archivo,['id'=>'codigo','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaDesde', 'Fecha desde', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="FechaDesde" name="FechaDesde" readonly="readonly" value="{!! $fe_desde !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('validacion_fechas') ? 'has-error' :'' }}">
            {!!Form::label('FechaHasta', 'Fecha hasta', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              <div class="input-group date">
                <input type="text" class="form-control group-date" id="FechaHasta" name="FechaHasta" readonly="readonly" value="{!! $fe_hasta !!}">
                <div class="input-group-append">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('cod_hora_desde') ? 'has-error' :'' }}">
            {!!Form::label('Hora desde', 'Hora desde', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!! Form::select('cod_hora_desde',$listaDesde,$cod_hora_desde,['id'=>'nombre','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group form-row {{ $errors->has('cod_hora_hasta') ? 'has-error' :'' }}">
            {!!Form::label('Hora hasta', 'Hora hasta', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!! Form::select('cod_hora_hasta',$listaHasta,$cod_hora_hasta,['id'=>'nombre','class'=>'form-control']) !!}
            </div>
          </div>
        </div>
      </div>
      <div class="card-footer with-border">
        <div class="col-sm-12">
          <div class="text-right">
              <input type="hidden" name="busqueda" id="busqueda" value="1"/>
              <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
{{--Fin de la seccion Filtros--}}

@include('includes.messages')

{{--Inicio de la seccion Listado--}}
@if ((!is_null($ARCHIVOS_INTERVALO)) && ($busqueda))
@if (!$ARCHIVOS_INTERVALO->isEmpty())
<div class="col-md-12">
  <div class="card card-primary card-outline">
    <div class="card-header with-border">
      <h3 class="card-title">Listado y gráfica</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body">

      <ul class="nav nav-pills pills-list mb-3" role="tablist">
        <li class="nav-item"><a class="nav-link active" href="#tab1" data-toggle="pill">Reporte de archivos por hora</a></li>
        <li class="nav-item"><a class="nav-link" href="#tab2" data-toggle="pill">Gráfica total de archivos</a></li>
      </ul>

      <div class="tab-content">
        <div id="tab1" class="tab-pane fade active show" role="tabpanel">

        {{-- Inclusion de codigo para tabla de datos - Reporte total de archivos --}}
        @include('reportes.historico.archivos_hora.re_ah_01_i_archivostabla')

        <hr class="col-sm-12">

        <div class="col-sm-12 text-center">
          <div class="btn-group">
            <button type="button" class="btn btn-success">Exportar a excel</button>
            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-top" role="menu" x-placement="top-start">
                <a class="dropdown-item" href="{{url('Reportes/Historico/R0251/Reporte',
                  array('cod_banco' => $cod_banco,
                    'cod_archivo' => $cod_archivo,
                    'fe_desde' => $fe_desde = Input::get('FechaDesde'),
                    'fe_hasta' => $fe_hasta = Input::get('FechaHasta'),
                    'cod_hora_desde' => $cod_hora_desde,
                    'cod_hora_hasta' => $cod_hora_hasta,
                    'tipo_reporte' => $tipo_reporte = 'XLS',
                    'tipoBusqueda' => '0'
                  )
                )}}">Por agrupación</a>
                <a class="dropdown-item" href="{{url('Reportes/Historico/R0251/Reporte',
                  array('cod_banco' => $cod_banco,
                    'cod_archivo' => $cod_archivo,
                    'fe_desde' => $fe_desde = Input::get('FechaDesde'),
                    'fe_hasta' => $fe_hasta = Input::get('FechaHasta'),
                    'cod_hora_desde' => $cod_hora_desde,
                    'cod_hora_hasta' => $cod_hora_hasta,
                    'tipo_reporte' => $tipo_reporte = 'XLS',
                    'tipoBusqueda' => '1'
                  )
                )}}">Por discriminación</a>
            </div>
          </div>
          <div class="btn-group">
            <button type="button" class="btn btn-danger">Exportar a pdf</button>
            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="caret"></span>
              <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-top" role="menu" x-placement="top-start">
                <a class="dropdown-item" href="{{url('Reportes/Historico/R0251/Reporte',
                  array('cod_banco' => $cod_banco,
                    'cod_archivo' => $cod_archivo,
                    'fe_desde' => $fe_desde = Input::get('FechaDesde'),
                    'fe_hasta' => $fe_hasta = Input::get('FechaHasta'),
                    'cod_hora_desde' => $cod_hora_desde,
                    'cod_hora_hasta' => $cod_hora_hasta,
                    'tipo_reporte' => $tipo_reporte = 'PDF',
                    'tipoBusqueda' => '0'
                  )
                )}}">Por agrupación</a>
                <a class="dropdown-item" href="{{url('Reportes/Historico/R0251/Reporte',
                  array('cod_banco' => $cod_banco,
                    'cod_archivo' => $cod_archivo,
                    'fe_desde' => $fe_desde = Input::get('FechaDesde'),
                    'fe_hasta' => $fe_hasta = Input::get('FechaHasta'),
                    'cod_hora_desde' => $cod_hora_desde,
                    'cod_hora_hasta' => $cod_hora_hasta,
                    'tipo_reporte' => $tipo_reporte = 'PDF',
                    'tipoBusqueda' => '1'
                  )
                )}}">Por discriminación</a>
            </div>
          </div>
        </div>
      </div>
    {{--Fin de la seccion Reporte total de archivos--}}

    {{--Inicio de la seccion Grafica total de archivos--}}
    <div id="tab2" class="tab-pane fade" role="tabpanel">

    {{-- Inclusion de codigo para totales - Reporte total de archivos --}}
    @include('reportes.historico.archivos_hora.re_ah_01_i_archivostotales')

    {{-- Inclusion de codigo para graficas - Reporte total de archivos --}}
    @include('reportes.historico.archivos_hora.re_ah_01_i_archivosgraficas')

    </div><!--menu1-->
  </div><!--table conten-->
</div><!--card body-->
</div>
</div>
@else
<div class="col-md-12">
    <div class="alert alert-danger" style="text-align: center">No existe información</div>
</div>
@endif
@endif
{{--Fin de la seccion Grafica total de archivos--}}

</div>
{{--Fin de la seccion Listado--}}

@stop

{{--Fin Archivos por horas--}}

@section('after-scripts-end')
  @include('includes.scripts.reportes.scripts_filtro_fecha')
  @include('includes.scripts.graficos_general.scripts_graficos_general')
  @include('includes.scripts.reportes.archivos_hora.scripts_archivos_hora')
@stop
