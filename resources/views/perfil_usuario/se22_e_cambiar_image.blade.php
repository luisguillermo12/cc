{{-- @Nombre del programa: --}}
{{-- @Funcion:  cambiar avatar de los usuarios--}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i>Cambiar Contraseña</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Perfil de usuarios</li>
              <li class="breadcrumb-item">Mi perfil</a></li>
            </ol>
          </div>
        </div>
      </div>
@endsection

@section('content')
    {!! Form::open(['route' => ['PerfilUsuario.updateImage', $user], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-change-avatar']) !!}

    <div class="col-12">
            <div class="card">
              <div class="card-header card-warning card-outline">
                <h3 class="card-title">Cambiar Logotipo,   usuario :{{$user->name}}</h3>

                <div class="card-body">

                  <div class="form-group">
                      <div class="col-sm-6 offset-sm-3">
                      @include('includes.messages')
                      </div>
                  </div>


                    <div class="form-group row">
                        <div class="col-sm-6 col-sm-offset-3">
                        <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        {!! Form::label('logotipo', 'Logotipo(*)', ['class' => 'col-lg-3 control-label']) !!}

                        <div class="col-lg-6">
                            {!! Form::file('logotipo', ['class' => 'form-control', 'required' => 'required']) !!}
                        </div><!--col-lg-10-->
                    </div><!--form control-->

                    <div class="form-group row">
                        <div class="col-sm-6 col-sm-offset-3">
                        <div class="pull-right">
                            {!! Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title'=>'Enviar']) !!}
                            {!! link_to_route('PerfilUsuario.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
                        </div>
                        </div>
                    </div>

              <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
        {{-- fin de la vista --}}
@stop

@section('after-scripts-end')
    {!! Html::script('js/backend/access/users/script.js') !!}
@stop
