{{-- @Nombre del programa: Vista Principal perfil de usuarios/Mi Perfil --}}
{{-- @Funcion: --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por:  --}}

@extends('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> Mi perfil</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-lock"></i>Perfil de usuari</a></li>
        <li class="breadcrumb-item active">Mi perfil</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header card-warning card-outline">
              <h3 class="card-title">Perfil</h3>
      <div class="box-body">
        <div role="tabpanel">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="profile">
                    <table id="example1" class="table table-bordered table-striped table-hover table-sm">
                <tr>
                  <th>Nombre</th>
                  <td>{{ $user->name }}</td>
                </tr>
                <tr>
                  <th>Usuario</th>
                  <td>{{ $user->email }}</td>
                </tr>
                <tr>
                   <th>Logo del usuario</th>
                    @if (Auth::user()->logotipo == null)
                        <td>
                        <img src="{{ asset("img/logos/user.png") }}" class="user-profile-image" style="max-height: 150px; width: auto;"/>
                        </td>
                       @else
                  <td>
                    <img src="{{ asset("img/logos/".$user->logotipo) }}" class="user-profile-image" style="max-height: 150px; width: auto;"/>
                  </td>
                  @endif
                </tr>
                <tr>
                  <th>Fecha de creación</th>
                  <td>{{ $user->created_at->format('d-m-Y h:i:s') }}</td>
                </tr>
                <tr>
                  <th>Última actualización</th>
                  <td>{{ $user->updated_at->format('d-m-Y h:i:s') }}</td>
                </tr>
                <tr>
                  <th>Acciones</th>
                  <td>
                    {!! link_to_route('PerfilUsuario.changePasword', 'Cambiar contraseña', ['id' => $user->id], ['class' => 'btn btn-warning btn-xs']) !!}

                    {!! link_to_route('PerfilUsuario.changeImage', 'Cambiar imagen ', ['id' => $user->id], ['class' => 'btn btn-primary btn-xs']) !!}
                  </td>
                </tr>
              </table>
            </div><!--tab panel profile-->
          </div><!--tab content-->
        </div><!--tab panel-->
      </div>
    </div>
  </div>
</div>
@endsection
