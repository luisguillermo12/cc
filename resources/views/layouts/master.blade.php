<!doctype html>
  @section('htmlheader')
      @include('includes.htmlheader')
  @show

<body class="hold-transition sidebar-mini @guest sidebar-collapse @endguest">

  <div class="wrapper">
    @include('includes.header')
    @include('includes.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper fixed_wrapper">

      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('page-header')
      </section>

      <!-- Main content -->
      <section class="content">
        @yield('content')
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->

    @include('includes.footer')
  </div><!-- ./wrapper -->

  @section('scripts')
      @include('includes.scripts')
  @show
</body>
</html>
