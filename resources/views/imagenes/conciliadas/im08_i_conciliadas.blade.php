<?php use App\Models\Configuracion\Banco\Banco; ?>
{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
  <h1><i class="fa fa-archive"></i> Distribuidas por participante</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-archive"></i> Imágenes</a></li>
    <li class="active"> Distribuidas por participante</li>
  </ol>
@endsection

@section('content')
  <div class="row">
  {{-- Inicio de la seccion Filtro --}}
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Filtro</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <form role="form" class="form-horizontal">
              <div class="col-sm-12">
                <div class="col-sm-6">
                  <div class="form-group">
                    {!!Form::label('Participante', 'Participante emisor', array('class' => 'col-sm-4 control-label'))!!}
                    <div class="col-sm-8">
                      {!! Form::select('cod_banco_emisor',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control','onchange' => 'this.form.submit()']) !!}
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
	  {{-- Fin de la secion Filtro --}}
	  {{-- Inicio de la seccion Totales --}}
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Totales</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body">
         <?php //   @foreach ($TotalConciliadas as $totalconciliadas) ?>

              <div class="col-md-4 col-sm-4 col-xs-12"><center>
                <div class="info-box-eps">
                  <span class="info-box-icon-eps bg-aqua-eps"><i class="fa fa-sign-in"></i></span>

                  <div class="info-box-content-eps">
                    <span class="info-box-text-eps">Operaciones</span>
                  <span class="info-box-number-eps">
                    {{ $totales->total_operaciones }}
                  </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </center></div>
              <!-- /.col -->

              <div class="col-md-4 col-sm-4 col-xs-12"><center>
                <div class="info-box-eps">
                  <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-check-square-o"></i></span>

                  <div class="info-box-content-eps">
                    <span class="info-box-text-eps">Distribuidas</span>
                   <span class="info-box-number-eps">
                    {{ $totales->total_operaciones_conciliadas }}
                  </span>
                    </div>
            
                  </div>
               
                </center></div>
          

                <div class="col-md-4 col-sm-4 col-xs-12"><center>
                  <div class="info-box-eps">
                    <span class="info-box-icon-eps bg-red-eps"><i class="fa fa-ban"></i></span>

                    <div class="info-box-content-eps">
                      <span class="info-box-text-eps">Faltantes</span>
                      <span class="info-box-number-eps">
                        {{ $totales->total_operaciones_por_conciliar }}
                      </div> 
                    </div> 
                  </center></div>

                  <div class="col-md-4 col-sm-4 col-xs-12"><center>
               <!--      <div class="info-box-eps">
                      <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-money"></i></span>
                  
                        <div class="info-box-content-eps">
                          <span class="info-box-text-eps">Monto conciliado</span>
                          <span class="info-box-number-eps">
                        {{ number_format($totales->total_monto_conciliado/100, 2 , ',' , '.') }}
                          </span>
                          </div>
                      </div>
                    </center>  -->
                  </div>  






  <div class="col-md-4 col-sm-4 col-xs-12"><center>
    <div class="info-box-eps">
      <span class="info-box-icon-eps small-box bg-green-eps"><i class="fa fa-percent"></i></span>
      <div class="info-box-content-eps">
        <span class="info-box-text-eps">% distribuidas</span>
        <span class="info-box-number-eps">
          @if($totales->total_operaciones != 0)
    {{ number_format($totales->total_operaciones_conciliadas *100/$totales->total_operaciones, 2, ',','.') }}
          @else
          0
          @endif
        </span>
      </div>
    </div>
  </center></div>

  <?php 
?>



  <div class="col-md-4 col-sm-4 col-xs-12"><center>
    <div class="info-box-eps">
      <span class="info-box-icon-eps small-box bg-red-eps"><i class="fa fa-percent"></i></span>
      <div class="info-box-content-eps">
        <span class="info-box-text-eps"> % faltantes</span>
        <span class="info-box-number-eps">
   @if($totales->total_operaciones != 0)
    {{ number_format($totales->total_operaciones_por_conciliar *100/$totales->total_operaciones, 2 , ',' , '.') }}
          @else
          0
          @endif
        </span> 
      </div>

    </div>

  </center></div>


</div>
</div>
</div>

<div class="col-md-12">
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Listado</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table id="Conciliadas-table" class="table table-striped">
          <thead class="thead-default">
            <tr>
              <th><center>Participante emisor</center></th>
              <th><center>Participante receptor</center></th>
              <th><center>Operaciones</center></th>
              <th><center> Distribuidas</center></th>
              <th><center>(%) Distribuidas</center></th>
              <th><center>Faltantes</center></th>
              <th><center>(%) Faltantes</center></th>
              <th><center>Acción</center></th>
            </tr>
          </thead>
     @foreach ($registros as $registro)
                  <tr>
                    <td nowrap><Justify>{{  Banco::buscaruno($registro->cod_banco_emisor)->nombre_completo   }}</Justify></td>
                    <td nowrap><Justify>{{ Banco::buscaruno($registro->cod_banco_destinatario)->nombre_completo  }}</Justify></td>
                    <td><center>{{ number_format($registro->operaciones_presentadas, 0, ',', '.') }}</center></td>
                    <td><center>{{ number_format($registro->imagenes_distribuidas, 0, ',', '.') }}</center></td>
                    <td><center>{{ number_format($registro->imagenes_distribuidas * 100 / $registro->operaciones_presentadas, 2, ',', '.') }}</center></td>
                    <td><center>{{ number_format($registro->operaciones_presentadas - $registro->imagenes_distribuidas, 0, ',', '.') }}</center></td>
                    <td><center>{{ number_format(($registro->operaciones_presentadas - $registro->imagenes_distribuidas) * 100 / $registro->operaciones_presentadas, 2, ',', '.') }}</center></td>
                    
           

            
           @permissions(['R0150-03-02'])
                    <td><center><a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn btn-xs btn-info" href="{{ route('Imagenes.OperacionesConciliadas.Detalle',[$registro->cod_banco_emisor, $registro->cod_banco_destinatario])}}"><i class="fa fa-eye"></i></a></center></td>
            @endauth
                

                  </tr>
                  @endforeach
  </table>
</div>
</div>
</div>
</div>
</div>


@stop

@section('after-scripts-end')
  @include('includes.partials.conciliador.scripts_conciliadas')
@stop
