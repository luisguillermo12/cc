@extends ('layouts.master')
@section ('title', 'Electronic Payment Suite (EPS) - Operaciones Conciliadas')
@section('page-header')
  <h1><i class="fa fa-archive"></i> Conciliadas</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('Imagenes/DistribuidasParticipante')}}"><i class="fa fa-archive"></i> Imágenes</a></li>
    <li><a href="{{url('Imagenes/DistribuidasParticipante')}}"> Operaciones</a></li>
    <li><a href="{{url('Imagenes/DistribuidasParticipante')}}"> Conciliadas</a></li>
    <li> <a href="{{url('c_operaciones_conciliadas/'.$banco_destinatario->cod_banco_pagador)}}">Detalle</a></li>
    <li class="active"> Cheque</li>
  </ol>
@endsection

@section('content')
  <div class="row">
<div class="col-md-12">
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Totales</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <?php 
                 //dd($TotalConciliadas);
        ?>
        @foreach ($TotalConciliadas as $totalconciliadas)
          <div class="col-md-4 col-sm-4 col-xs-12"><center>
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-aqua-eps"><i class="fa fa-sign-in"></i></span>

              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Presentadas</span>
                <span class="info-box-number-eps">
                  @if ($totalconciliadas->cantidad_operaciones_presentadas == '')
                    0
                  @else
                    {{ number_format($totalconciliadas->cantidad_operaciones_presentadas, 0, ',', '.') }}
                  @endif
                </span>
              </div>
            </div>
          </center>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12"><center>
          <div class="info-box-eps">
            <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-check-square-o"></i></span>

            <div class="info-box-content-eps">
              <span class="info-box-text-eps">Conciliadas</span>
              <span class="info-box-number-eps">
                @if ($totalconciliadas->cantidad_operaciones == '')
                  0
                @else
                  {{ number_format($totalconciliadas->cantidad_operaciones, 0, ',', '.') }}
                @endif</span>
              </div>
            </div>
          </center>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12"><center>
          <div class="info-box-eps">
            <span class="info-box-icon-eps bg-red-eps"><i class="fa fa-ban"></i></span>

            <div class="info-box-content-eps">
              <span class="info-box-text-eps">No conciliadas</span>
              <span class="info-box-number-eps">
                @if ($totalconciliadas->cantidad_no_operaciones == '')
                  0
                @else
                  {{ number_format($totalconciliadas->cantidad_no_operaciones, 0, ',', '.') }}
                @endif</span>
              </div>
            </div>
          </center>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12"><center>
          <div class="info-box-eps">
            <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-money"></i></span>
            @foreach ($monto_total as $montototal)
              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Monto</span>
                <span class="info-box-number-eps">
                  @if ($montototal->monto_total == '')
                    0
                  @else
                    {{ number_format($montototal->monto_total, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}
                  @endif</span>
                </div>
              @endforeach
            </div>
          </center>
        </div>
        <!--<div class="col-md-4 col-sm-4 col-xs-12"><center>
        <div class="">
        <span class=""><i class=""></i></span>
        <div class="info-box-content-eps">
        <span class="info-box-text-eps"></span>
        <span class="info-box-number-eps">

      </span>
    </div>
  </div>
</center></div>

<!--<div class="col-md-4 col-sm-4 col-xs-12"><center>
<div class="info-box-eps">
<span class="info-box-icon-eps bg-green-eps"><i class="fa fa-picture-o"></i></span>
<div class="info-box-content-eps">
<span class="info-box-text-eps">Img. Conciliadas</span>
<span class="info-box-number-eps">
@if ($totalconciliadas->cantidad_anverso == '')
0
@else
{{ number_format($totalconciliadas->cantidad_anverso, 0, ',', '.') }}
@endif
</span>
</div>
</div>
</center></div>-->
{{--*/ @$porcentaje= $totalconciliadas->cantidad_conciliadas * 100 / $totalconciliadas->cantidad_operaciones_presentadas /*--}}
{{--*/ @$porcentaje = abs($porcentaje) /*--}}
<div class="col-md-4 col-sm-4 col-xs-12"><center>
  <div class="info-box-eps">
    <span class="info-box-icon-eps small-box bg-green-eps"><i class="fa fa-percent"></i></span>
    <div class="info-box-content-eps">
      <span class="info-box-text-eps">Conciliadas</span>
      <span class="info-box-number-eps">
        @if ($porcentaje == '')
          0
        @else
          {{ number_format($porcentaje, 2, ',', '.') }}
        @endif
      </span>
    </div>
  </div>
</center></div>

{{--*/ @$porcentaje= $totalconciliadas->cantidad_no_operaciones * 100 / $totalconciliadas->cantidad_operaciones_presentadas /*--}}
{{--*/ @$porcentaje = abs($porcentaje) /*--}}
<div class="col-md-4 col-sm-4 col-xs-12"><center>
  <div class="info-box-eps">
    <span class="info-box-icon-eps small-box bg-red-eps"><i class="fa fa-percent"></i></span>
    <div class="info-box-content-eps">
      <span class="info-box-text-eps">No conciliadas</span>
      <span class="info-box-number-eps">
        @if ($porcentaje == '')
          0
        @else
          {{ number_format($porcentaje, 2, ',', '.') }}
        @endif
      </span>
    </div>

  </div>

</center></div>
@endforeach
</div>
</div>
</div>
</div>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Total {{ $banco_destinatario->cod_banco_pagador }} - {{ $banco_destinatario->nombre }}</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="row">

       <?php //dd( $cantidadesDetalles);
        ?>

        @foreach ($cantidadesDetalles as $cantidadeDetalle)

          <div class="col-md-4 col-sm-4 col-xs-12"><center>
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-aqua-eps"><i class="fa fa-sign-in"></i></span>

              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Presentadas</span>
                <span class="info-box-number-eps">
                  @if ($cantidadeDetalle->total_operaciones_presentadas == '')
                    0
                  @else
                    {{ number_format($cantidadeDetalle->total_operaciones_presentadas, 0, ',', '.') }}
                  @endif</span>
                </div>
              </div>
            </center>
          </div>

          <div class="col-md-4 col-sm-4 col-xs-12"><center>
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-check-square-o"></i></span>

              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Conciliadas</span>
                <span class="info-box-number-eps">
                  @if ($cantidadeDetalle->total_operaciones == '')
                    0
                  @else
                    {{ number_format($cantidadeDetalle->total_operaciones, 0, ',', '.') }}
                  @endif</span>
                </div>
              </div>
            </center>
          </div>

          {{--*/ @$no_conciliadas = $cantidadeDetalle->total_operaciones_presentadas - $cantidadeDetalle->total_operaciones /*--}}
          <div class="col-md-4 col-sm-4 col-xs-12"><center>
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-red-eps"><i class="fa fa-ban"></i></span>

              <div class="info-box-content-eps">
                <span class="info-box-text-eps">No conciliadas</span>
                <span class="info-box-number-eps">
                  @if ($no_conciliadas == '')
                    0
                  @else
                    {{ number_format($no_conciliadas, 0, ',', '.') }}
                  @endif</span>
                </div>
              </div>
            </center>
          </div>

          <div class="col-md-4 col-sm-4 col-xs-12"><center>
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-money"></i></span>

              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Monto conciliado</span>
                <span class="info-box-number-eps">
                  @if ($cantidadeDetalle->monto_global == '')
                    0
                  @else
                    {{--*/ @$total = substr($cantidadeDetalle->monto_global, 0, strlen($cantidadeDetalle->monto_global) - 2).".".substr($cantidadeDetalle->monto_global, strlen($cantidadeDetalle->monto_global	) - 2) /*--}}
                    {{--*/ @$total = abs($total) /*--}}
                      {{ number_format($total, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}
                  @endif</span>
                </div>
              </div>
            </center>
          </div>

          {{--*/ @$porcentaje= $cantidadeDetalle->cantidad_conciliadas * 100 / $cantidadeDetalle->total_operaciones_presentadas /*--}}
          {{--*/ @$porcentaje = abs($porcentaje) /*--}}
          <div class="col-md-4 col-sm-4 col-xs-12"><center>
            <div class="info-box-eps">
              <span class="info-box-icon-eps small-box bg-green-eps"><i class="fa fa-percent"></i></span>
              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Conciliadas</span>
                <span class="info-box-number-eps">
                  @if ($porcentaje == '')
                    0
                  @else
                    {{ number_format($porcentaje, 2, ',', '.') }}
                  @endif
                </span>
              </div>
            </div>
          </center>
        </div>
        {{--*/ @$porcentaje= $no_conciliadas * 100 / $cantidadeDetalle->total_operaciones_presentadas /*--}}
        {{--*/ @$porcentaje = abs($porcentaje) /*--}}
        <div class="col-md-4 col-sm-4 col-xs-12"><center>
          <div class="info-box-eps">
            <span class="info-box-icon-eps small-box bg-red-eps"><i class="fa fa-percent"></i></span>
            <div class="info-box-content-eps">
              <span class="info-box-text-eps">No conciliadas</span>
              <span class="info-box-number-eps">
                @if ($porcentaje == '')
                  0
                @else
                  {{ number_format($porcentaje, 2, ',', '.') }}
                @endif
              </span>
            </div>
          </div>
        </center>
      </div>
    @endforeach

  </div>
</div>
</div>
</div>
<div class="col-md-12">
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Listado {{ $banco_emisor->cod_banco_emisor }} - {{ $banco_emisor->nombre }}</h3>
    </div>
    <div class="box-body">
      <div class="table-responsive">
        <table id="Conciliadas-table" class="table table-striped">
          <thead class="thead-default">
            <tr>
              <th nowrap><center>Código unico de identificación</center></th>
              <th nowrap><center>N° de cheque</center></th>
              <th nowrap><center>Cuenta</center></th>
              <th nowrap><center>Monto conciliado</center></th>
            </tr>
          </thead>
          <?php 
            //dd($detalle_cheques);
          ?>
          @foreach ($detalle_cheques as $detalle_cheque)
            <tr>
              <td><center>{{$detalle_cheque->num_ibrn}}</center></td>
              <td><center>{{$detalle_cheque->num_cheque}}</center></td>
              <td><center>{{$detalle_cheque->num_cuenta_pagador}}</center></td>
              <td><center>
                {{ number_format($detalle_cheque->monto, $monedas->num_decimales, $monedas->separador_decimales, $monedas->separador_miles) }}
              </center></td>
            </tr>
            <tr>
              {{--*/ @$ruta = 'http://' /*--}}
              <td colspan="2"><img src="{{ asset($ruta_imagen.$detalle_cheque->num_ibrn.'_A.jpg')}}" style ="height:255px; width:476px"></td>
              <td colspan="2"><img src="{{ asset($ruta_imagen.$detalle_cheque->num_ibrn.'_R.jpg')}}" style ="height:255px; width:475px"></td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@stop

@section('after-scripts-end')
  @include('includes.partials.conciliador.scripts_conciliadas')
@stop
