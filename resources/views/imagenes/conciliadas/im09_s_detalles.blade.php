
{{-- @Nombre del programa: --}}
{{-- @Funcion: Imagenes Distribuidas Detalles --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}




    @extends ('layouts.master')
  {{-- Inicio Imagenes Distribuidas Detalles --}}
    @section('page-header')
    <h1><i class="fa fa-archive"></i> Operaciones bilaterales</h1>
    <ol class="breadcrumb">
      <li><a href="{{url('Imagenes/DistribuidasParticipante')}}"><i class="fa fa-archive"></i> Imágenes</a></li>
      <li> <a href="{{url('Imagenes/DistribuidasParticipante')}}">Distribuidas por participante</a></li>
      <li class="active"> Listado de cheques por participante</li>
    </ol>
    @endsection

@section('content')
        <div class="row">
    {{-- Inicio de la seccion Totales --}}
          <div class="col-md-12">
              <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Totales</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                <table border="0" align="center" width="100%">
                      <tbody>
                     
                        <tr>
                          <td>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <div class="info-box-eps">
                                <span class="info-box-icon-eps bg-aqua-eps"><i class="fa fa-sign-in"></i></span>
                                  <div class="info-box-content-eps">
                                    <span class="info-box-text-eps">Operaciones</span>
                                    <span class="info-box-number-eps">
                                      {{ number_format($totales->total_operaciones, 0, ',', '.') }}
                                    </span>
                                  </div>
                                  
                                </div>
                                
                            </div>
                              
                           </td>
                          <td><center>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <div class="info-box-eps">
                                <span class="info-box-icon-eps bg-green-eps"><i class="fa fa-picture-o"></i></span>
                                <div class="info-box-content-eps">
                                  <span class="info-box-text-eps">Imágenes distribuidas</span>
                                  <span class="info-box-number-eps">
                                    {{ number_format($totales->total_operaciones_concilidas, 0, ',', '.') }}
                                  </span>
                                </div>
                             
                              </div>
                              
                            </div>
                           
                          </center></td>
                       
                          <td><center>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <div class="info-box-eps">
                                <span class="info-box-icon-eps small-box bg-green-eps"><i class="fa fa-percent"></i></span>
                                  <div class="info-box-content-eps">
                                    <span class="info-box-text-eps">Distribuidas</span>
                                    <span class="info-box-number-eps">
                                  
                                      {{ number_format($totales->porcentaje_distribuidas, 2, '.', ',') }}
                                      
                                    </span>
                                  </div>
                                  
                                </div>
                                
                            </div>
                              
                          </center></td>
                    
                          <td><center>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <div class="info-box-eps">
                                <span class="info-box-icon-eps bg-red-eps"><i class="fa fa-picture-o"></i></span>
                                <div class="info-box-content-eps">
                                  <span class="info-box-text-eps">Imágenes faltantes</span>
                                  <span class="info-box-number-eps">
                                    {{ number_format($totales->total_operaciones_por_concilidas, 0, ',', '.') }}
                                  </span>
                                </div>
                                
                              </div>
                              
                            </div>
                           
                          </center></td>
                      
                          <td><center>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <div class="info-box-eps">
                                <span class="info-box-icon-eps small-box bg-red-eps"><i class="fa fa-percent"></i></span>
                                  <div class="info-box-content-eps">
                                    <span class="info-box-text-eps">Faltantes</span>
                                    <span class="info-box-number-eps">
                               
                                      {{ number_format($totales->porcentaje_faltantes, 2, '.', ',') }}
                                  
                                    </span>
                                  </div>
                                
                                </div>
                                
                            </div>
                              
                          </center></td>
                        </tr>
                         
                      </tbody>
                    </table>
                    <!--fin table-->
                  </div>
                  </div>
                </div>
            </div>
      {{-- Fin de la seccion Totales --}}
      {{-- Inicio de la seccion Participante Emisor --}}
         



            <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"><b>Participante emisor:</b> {{ $banco_emisor->nombre_completo }}</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
              </div><!-- /.box-header -->
              <!-- form start -->
              <div class="box-body">
                <form role="form" class="form-horizontal">
                <div class="col-sm-12">

                  <div class="col-sm-6">
                    <div class="form-group">
                      {!!Form::label('Sub producto', 'Sub producto', array('class' => 'col-sm-2 control-label'))!!}
                      <div class="col-sm-8">
                        {!! Form::select('sub_producto',$sub_productos,$sub_productos,['id'=>'cod_banco_emisor','class'=>'form-control']) !!}
                      </div>
                    </div>
                  </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="n_cheque" class="col-sm-3 control-label">Número de cheque</label>
                        <div class="col-sm-8">
                          {!!Form::text('n_cheque',null,['id'=>'n_cheque','class'=>'form-control ','placeholder'=>'Número de cheque','maxlength'=>8,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="nivel"> Operador </label>
                         <div class="col-sm-6">
                          <select class="form-control" id="nivel" name="nivel">
                            <option value="">*</option> 
                            <option value=">">> (mayor al monto)</option> 
                            <option value="<">< (menor al monto)</option> 
                            <option value="=">= (igual al monto)</option> 
                          </select> 
                         </div>
                      </div>
                    </div>

                     <div class="col-sm-6">
                      <div class="form-group">
                        <label for="monto" class="col-sm-3 control-label">Monto</label>  
                        <div class="col-sm-8">
                          {!!Form::text('monto',null,['id'=>'monto','class'=>'form-control ','placeholder'=>'Ingrese monto','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
                        </div>
                      </div>
                    </div>


                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-default btn-sm  pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                  </div>
              </div>
              </form>
            </div>
      </div>
      </div>

          </div>    <!-- /.box-header -->

             
 
      {{-- Fin de Participante Emisor --}}
    {{-- Inicio de la seccion Listado --}} 
    
        <div class="col-md-13">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Listado de cheques por participante</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <table id="EmpresasOrdenantes-table" class="table table-striped">
                  <thead class="thead-default">
                    <tr>
                      <!--<th nowrap><center>Banco emisor</center></th>-->
                      <th ><center>Participante receptor</center></th>
                       <th ><center>Código de sub producto</center></th>
                       <th ><center>Código de moneda</center></th>
                       <th ><center>Fecha</center></th>
                      <th ><center>Número de cheque</center></th>
                      <th ><center>Monto</center></th>
                      <!--<th nowrap><center>Reverso</center></th>-->
                    </tr>
                  </thead>
                  @foreach ($registros as $registro)
                  <tr>
                    <td><center>{{ $registro->codigo }} - {{ $registro->nombre  }}</center></td>
                    <td><center>{{ $registro->cod_producto }}</center></td>
                    <td><center>{{ $registro->cod_moneda }}</center></td>
                    <td><center>{{ Date::parse($registro->fecha_intercambio)->format('d/m/Y') }}</center></td>
                    <td><center>{{ $registro->num_cheque }}</center></td>
                    <td><center>{{number_format($registro->monto/100, 0, ',', '.') }}</center></td>
                  </tr>
                  @endforeach
                </table>
              </div><!--table-responsive-->
            </div>
          </div>
        </div>
    {{-- Fin de la seccion Listado --}}
      </div>
    {{-- Fin de Imagenes Distribuidas Detalles --}}
@stop

@section('after-scripts-end')
@include('includes.partials.conciliador.scripts_imagenes_distribuidas')
@stop
