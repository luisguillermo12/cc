{{-- @Nombre del programa: Vista de Detalles de mo->Alarmas --}}
{{-- @Funcion:  --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 16/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
  <h1><i class="fa fa-desktop"></i>  Alarmas</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-desktop"></i> Monitoreo</a></li>
    <li><a href="{{url('Monitoreo/Alarmas')}}">Alarmas</a></li>
    <li>Detalles</li>
  </ol>
@endsection
@section('content')
  <div class="box box-warning">
      <div class="box-header with-border">
        <!--codigo gui MOD-CONF-1.2-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.2.2" id="codigo_gui">
          <h3 class="box-title">Detalles</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
      </div><!-- /.box-header -->


      @foreach($alarma as $alarmas)
      <div class="box-body">
        <div class="col-sm-12">
            <div class="col-sm-4">
                <p><strong>Fecha de recepción</strong></p>
                <p class="text-muted">{{ Date::parse($alarmas->fecha_hora)->toDateTimeString() }}</p>
            </div>
            <div class="col-sm-4">
                <p><strong>Participante</strong></p>
                <p class="text-muted">{{ $alarmas->participante_emisor }}</p>
            </div>
            <div class="col-sm-4">
                <p><strong>Estado</strong></p>
                <p class="text-muted"><span class="label label-primary">{{ $alarmas->estatus }}</span></p>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-4">
                <p><strong>Código de alarma</strong></p>
                <p class="text-muted">{{ $alarmas->codigo }}</p>
            </div>
            <div class="col-sm-8">
                <p><strong>Descripción de la alarma</strong></p>
                <p class="text-muted">{{ $alarmas->descripcion }}</p>
            </div>
        </div>
        <div class="clearfix"></div>
      </div><!-- /.box-body -->
        @endforeach
  </div>
  <div class="box box-primary">
      <div class="box-header with-border">
        <!--codigo gui MOD-CONF-1.2-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.2.2" id="codigo_gui">
          <h3 class="box-title">Listado</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
      </div><!-- /.box-header -->
      @foreach($alarma as $alarmas)
        <div class="box-body">
           {{--*/ @$count = 0 /*--}}
          <div class="table-responsive">
            <table id="Conciliadas-table" class="table table-striped">
                  <thead class="thead-default">
                    <tr>
                      <th nowrap><center>Usuario</center></th>
                      <th nowrap><center>Observaciones</center></th>
                      <th nowrap><center>Fecha</center></th>
                    </tr>
                  </thead>
                  @foreach($observaciones as $observacion)
                    <tr>
                      <td nowrap><center>{{ $observacion->nombre_usuario }}</center></td>
                      <td><left>{{ $observacion->observaciones }}</left></td>
                      <td nowrap><center>{{ $observacion->fecha_observaciones }}</center></td>
                    </tr>
                  @endforeach
            </table>
          </div>
        </div><!-- /.box-body -->
        @endforeach
  </div><!--box--><!--box-->
@stop
{{--Fin--}}