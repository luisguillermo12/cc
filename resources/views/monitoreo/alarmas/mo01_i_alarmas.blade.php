{{-- @Nombre del programa: Vista Principal mo->Alarmas--}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 16/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
  <h1><i class="fa fa-desktop"></i>  Alarmas</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-desktop"></i> Monitoreo</a></li>
    <li class="active">Alarmas</li>
  </ol>
@endsection

@section('content')
  <div class="row">
  <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Totales</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
            <table class="table table-striped text-center">
              <thead class="thead-default">
                <tr>
                  <th class="col-sm-1">Estado</th>
                  <th class="col-sm-1">Inicio de<br>operaciones</th>
                  <th class="col-sm-1">Operaciones<br>presentadas</th>
                  <th class="col-sm-1">Operaciones<br>devueltas</th>
                  <th class="col-sm-1">Posiciones<br>financieras</th>
                  <th class="col-sm-1">Liquidaciones</th>
                  <th class="col-sm-1">Imágenes</th>
                  <th class="col-sm-1">Mensajes informativos</th>
                </tr>
              </thead>
              <tbody>
              <?php ?>
              <?php $grupoInitr = $initr->groupBy('estatus'); ?>
              <?php $grupoIcom1 = $icom1->groupBy('estatus'); ?>
              <?php $grupoIcom2 = $icom2->groupBy('estatus'); ?>
              <?php $grupoFposr = $fposr->groupBy('estatus'); ?>
              <?php $grupoStmta = $stmta->groupBy('estatus'); ?>
              <?php $grupoMaili = $maili->groupBy('estatus'); ?>
              <?php $grupoMsjs = $mensajes->groupBy('estatus'); ?>
                <tr>
                  <td><h4 class="text-left">Nuevas</h4></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoInitr['NO PROCESADO']) }}</span></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoIcom1['NO PROCESADO']) }}</span></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoIcom2['NO PROCESADO']) }}</span></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoFposr['NO PROCESADO']) }}</span></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoStmta['NO PROCESADO']) }}</span></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoMaili['NO PROCESADO']) }}</span></td>
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoMsjs['NO PROCESADO']) }}</span></td>
                </tr>
                <tr>
                  <td><h4 class="text-left">En proceso</h4></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoInitr['EN PROCESO']) }}</span></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoIcom1['EN PROCESO']) }}</span></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoIcom2['EN PROCESO']) }}</span></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoFposr['EN PROCESO']) }}</span></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoStmta['EN PROCESO']) }}</span></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoMaili['EN PROCESO']) }}</span></td>
                  <td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoMsjs['EN PROCESO']) }}</span></td>
                </tr>
                <tr>
                  <td><h4 class="text-left">Procesadas</h4></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoInitr['PROCESADO']) }}</span></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoIcom1['PROCESADO']) }}</span></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoIcom2['PROCESADO']) }}</span></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoFposr['PROCESADO']) }}</span></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoStmta['PROCESADO']) }}</span></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoMaili['PROCESADO']) }}</span></td>
                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 20px;">{{ count(@$grupoMsjs['PROCESADO']) }}</span></td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Alarmas de archivos</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <ul class="nav nav-tabs">
            <li class="active"><a @if (count(@$grupoInitr['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#inicio">Inicio de operaciones</a></li>
            <li><a @if (count(@$grupoIcom1['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#presentadas">Operaciones presentadas</a></li>
            <li><a @if (count(@$grupoIcom2['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#devueltas">Operaciones devueltas</a></li>
            <li><a @if (count(@$grupoFposr['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#posiciones">Posiciones financieras</a></li>
            <li><a @if (count(@$grupoStmta['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#liquidacion">Liquidaciones</a></li>
            <li><a @if (count(@$grupoMaili['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#imagenes">Imágenes</a></li>
            <li><a @if (count(@$grupoMsjs['NO PROCESADO']) > 0) class="bg-red-eps" @endif data-toggle="tab" href="#mensajes">Mensajes informativos</a></li>
          </ul>
          <div class="tab-content">
            <br>
             <div id="inicio" class="tab-pane fade-in active">
               @include('monitoreo.alarmas.tablas.mo03_s_inicio')
             </div>

             <div id="presentadas" class="tab-pane fade">
               @include('monitoreo.alarmas.tablas.mo04_s_presentadas')
             </div>

             <div id="devueltas" class="tab-pane fade">
               @include('monitoreo.alarmas.tablas.mo05_s_devueltas')
             </div>

             <div id="posiciones" class="tab-pane fade">
               @include('monitoreo.alarmas.tablas.mo08_s_liquidacion')
             </div>

             <div id="liquidacion" class="tab-pane fade">
               @include('monitoreo.alarmas.tablas.mo07_s_posiciones')
             </div>

             <div id="imagenes" class="tab-pane fade">
               @include('monitoreo.alarmas.tablas.mo06_s_imagenes')
             </div>

             <div id="mensajes" class="tab-pane fade">
               @include('monitoreo.alarmas.tablas.mo09_s_mensajes')
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
{{--Fin--}}
@section('after-scripts-end')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>



@stop
