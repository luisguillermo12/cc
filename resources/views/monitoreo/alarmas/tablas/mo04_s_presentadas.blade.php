{{-- @Nombre del programa: Vista de la Tabla menu Presentadas en Alarmar de Archivos mo->Alarmas->tabla--}}
{{-- @Funcion:  --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 16/04/2018 --}}
{{-- @Modificado por:    --}}

{{--Inicio--}}
<div class="col-sm-12">
<form role="form" class="form-horizontal">
  <div class="col-sm-6">
    <div class="form-group">
      {!!Form::label('Participante', 'Participante', array('class' => 'col-sm-3 control-label'))!!}
      <div class="col-sm-9">
        {!! Form::select('cod_banco',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control']) !!}
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      {!!Form::label('Código de rechazo', 'Código de rechazo', array('class' => 'col-sm-7 control-label'))!!}
      <div class="col-sm-5">
        {!! Form::select('cod_icom1', $filtro_icom1, $cod_icom1, ['id'=>'cod_rechazo', 'class'=>'form-control']) !!}
      </div>
    </div>
  </div>
  <div class="col-sm-2">
    <div class="pull-right">
      <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
    </div>
  </div>
</form>
</div>
<br>
<div class="col-sm-12">
  <table id="operacionespresentadas-table" class="table table-striped">
    <thead class="thead-default">
      <tr>
        <th><center>Fecha de recepción</center></th>
        <th><center>Participante emisor</center></th>
        <th><center>Código de moneda</center></th>
        <th><center>Número de archivo</center></th>
        <th><center>Código de alarma</center></th>
        <th><center>Estado</center></th>
        <th><center>Acción</center></th>
      </tr>
    </thead>
    <tbody>
      @forelse ($icom1 as $archivo)
        <?php $nombre = preg_split("/[.,]+/", $archivo->nombre); ?>
        <tr>
          <td><center>{{ Date::parse($archivo->fecha_hora)->toDateTimeString() }}</center></td>
          <td class="text-center">{{ $nombre[0] }}</td>
          <td class="text-center">{{ $nombre[1] }}</td>
          <td class="text-center">{{ $nombre[2] }}</td>
          <td class="text-center"><a href="#" data-toggle="tooltip" data-placement="top" title="{{ $archivo->descripcion }}">{{ $archivo->codigo }}</a></td>
          <td class="text-center">{{ $archivo->estatus }}</td>
  @permissions(['R0100-02-02'])
          <td><center>
            <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn btn-xs btn-info" href="{{route('Monitoreo.Alarmas.show',$archivo->id)}}"><i class="fa fa-eye"></i></a>
          </center></center></td>
  @endauth
        </tr>
      @empty
        <tr>
          <td colspan="7">No existen alarmas para este tipo de archivo</td>
        </tr>
      @endforelse
    </tbody>
  </table>
</div><!--table-responsive-->
{{--Fin--}}