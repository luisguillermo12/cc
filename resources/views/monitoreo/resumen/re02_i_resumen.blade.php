@extends('layouts.master')

@section('page-header')
  <h1><i class="fa fa-desktop"></i> Resumen de compensación</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-desktop"></i> Monitoreo</a></li>
    <li class="active"> Resumen de compensación</li>
  </ol>
@endsection
@section('content')
{{-- @permissions(['R0100-01-02']) --}}
  @include('monitoreo.resumen.re01_i_grafico')
{{-- @endauth --}}
{{-- @permissions(['R0100-01-04']) --}}
  @include('monitoreo.resumen.re01_i_resumen_operaciones')
{{-- @endauth --}}
{{-- @permissions(['R0100-01-06']) --}}
  @include('monitoreo.resumen.re01_i_resumen_posiciones_multilaterales')
{{-- @endauth --}}




 

<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Modificación de franjas</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
        </div>
        <div class="table-responsive">
          <div class="box-body">
            <table class="table table-striped text-center">
              <thead class="thead-default">
                <tr>
                  <th class="col-sm-3">Tipo de operación</th>
                  <th class="col-sm-1">Participante</th>
                  <th class="col-sm-1">Tiempo</th>
                  <!--<th class="col-sm-2">Sin cambios</th>-->
                  <th class="col-sm-4">Descripción</th>
                </tr>
              </thead>
              <tbody>
                @foreach( $extenciones as $ext)
                <tr>
 
                  <td style="text-align:left" > {{ $ext->codigo }}-{{ $ext->nombre }} </td>
                  <td> {{ $ext->cod_banco }} </td>
                  @if($ext->tiempo_extension !=0 && $ext->estatus == 'E' )
                  <!--<td></td>-->

                  <td><span class="label bg-green col-sm-offset-1 col-sm-10" style="font-size: 12px;">{{ $ext->tiempo_extension }} min.</span></td>
                  @endif

                  @if($ext->tiempo_extension !=0 && $ext->estatus == 'D' )
                  <td><span class="label bg-red-eps col-sm-offset-1 col-sm-10" style="font-size: 12px;">{{ $ext->tiempo_extension }} min.</span></td>
                  <!--<td></td>-->
 
                  @endif
                  @if($ext->tiempo_extension == null )
                  <td></td>
                  <!--<td><span class="label bg-yellow-eps col-sm-offset-1 col-sm-10" style="font-size: 15px;">Sin extensiones</span></td>-->

                  @endif
                  <td> <p ALIGN="justify">{{ $ext->descripcion }}</p> </td>
                </tr>
              @endforeach
              </tbody>
           
            </table>
        </div>
        </div>
      </div>
      <div class="box-footer clearfix">
      </div>
    </div>
  </div>
</div>







</div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog" style="width: auto; height: auto; margin: 7vh auto 0px 2vh; ">
      <div class="modal-content bmd-modalContent">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Detalles</h4>
        </div>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" frameborder="0"></iframe>
          </div>
        </div> *
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- end iframe -->
@endsection
@section('after-scripts-end')
@include('includes.partials.monitoreo.resumen.Chart')
@include('includes.partials.monitoreo.resumen.dashboard2')
{{-- 
@include('includes.partials.monitoreo.resumen.demo')

@include('includes.partials.monitoreo.resumen.dashboard1')
@include('includes.partials.monitoreo.resumen.adminlte')--}}

  {{-- script for iframe modal --}}
  <script>
    (function($) {
    
      $.fn.bmdIframe = function( options ) {
      var self = this;
      var settings = $.extend({
        classBtn: '.bmd-modalButton',
      }, options );
      
      $(settings.classBtn).on('click', function(e) {
      
        var dataResumen = {
          'src': $(this).attr('data-bmdSrc'),
        };
        
        // print our data in the iframe
        $(self).find("iframe").attr(dataResumen);
        });
        
        // if we close the modal we reset the data of the iframe to prevent a video from continuing to reproduce even when the modal is closed
        this.on('hidden.bs.modal', function(){
          $(this).find('iframe').html("").attr("src", "");
        });
        
        return this;
      };
    
    })(jQuery);
    jQuery(document).ready(function(){
      jQuery("#myModal").bmdIframe();
    });
  </script>

@stop