@extends('layouts.master')
@section('page-header')
<h1>
<b><?php //{{ access()->user()->name }} ?></b> Resumen compensacion
<small></small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-home"></i> Resumen compensacion </a></li>
</ol>
@endsection
@section('content')
<div class="row">
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Resumen de productos</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <th style="width: 10px">Codigo</th>
                <th style="width: 30px" >Nombre</th>
                <th style="width: 20px">N.operaciones</th>
                <th style="width: 20px">Monto</th>
                <th>tiempo</th>
                <th style="width: 40px">%</th>
              </tr>
              <tr>
                <td>010 </td>
                <td>Creditos</td>
                <th>6.532.141</th>
                <th> 9.000.000</th>
                <td>
                  <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                  </div>
                </td>
                <td><span class="badge bg-red">55%</span></td>
              </tr>
              <tr>
                <td>020</td>
                <td>domiciliaciones</td>
                <th>3.000.000</th>
                <th>5.000.000</th>
                <td>
                  <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                  </div>
                </td>
                <td><span class="badge bg-yellow">70%</span></td>
              </tr>
              <tr>
                <td>030</td>
                <td>cheques</td>
                <th>2.000.000</th>
                <th>7.000.000</th>
                <td>
                  <div class="progress progress-xs progress-striped active">
                    <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                  </div>
                </td>
                <td><span class="badge bg-light-blue">30%</span></td>
              </tr>
              
            </table>
        
            <!-- buttom to iframe -->
              <button type="button" class="bmd-modalButton btn-lg btn-info" data-toggle="modal" data-bmdSrc="{{url('Monitoreo/Resumenopc2')}}" data-target="#myModal">Resumen #2</button>
            <!-- end buttom to iframe -->

          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>
          </div>
        </div>
        <!-- AREA CHART -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Area Chart</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="areaChart" style="height:250px"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Line Chart</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="lineChart" style="height:250px"></canvas>
            </div>
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-6">
        <div class="box box-success">
          <div class="box-header">
            <h3 class="box-title">Posicion Multilateral</h3>
            <div></div>
            <form role="form" class="form-horizontal">
              <div class="col-sm-6">
                <br>
                <div class="form-group">
                  {!!Form::label('Producto(*)', 'Producto(*)', array('class' => 'col-sm-4 control-label'))!!}
                  <div class="col-sm-6">
                    {!! Form::select('producto[]',$productos,$producto,['id'=>'producto', "class"=>"form-control",'required']) !!}
                  </div>
                </div>
              </div>
              <div class="col-sm-4"><br>
                <div class="pull-right">
                  <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                </div>
              </div>
            </form>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px">Codigo</th>
                  <th>Nombre</th>
                  <th>Deudor</th>
                  <th style="width: 40px">Acreedor</th>
                </tr>
                <tr>
                  <td>0102</td>
                  <td>BANCO DE VENEZUELA, S.A.C.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0104</td>
                  <td>VENEZOLANO DE CREDITO S.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0105</td>
                  <td> BANCO MERCANTIL C.A. S.A.C.A BANCO UNIVERSAL </td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0108</td>
                  <td>BANCO PROVINCIAL, S.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0114</td>
                  <td>  BANCO DEL CARIBE C.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0115</td>
                  <td>BANCO EXTERIOR C.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0116</td>
                  <td>BANCO OCCIDENTAL DE DESCUENTO BANCO UNIVERSAL, C.A.</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0128</td>
                  <td>BANCO CARONII C.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0134</td>
                  <td>BANESCO BANCO UNIVERSAL S.A.C.A.</td>
                  
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0137</td>
                  <td>  BANCO SOFITASA BANCO UNIVERSAL, C.A.</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0138</td>
                  <td>  BANCO PLAZA, C.A.</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0146</td>
                  <td>BANCO DE LA GENTE EMPRENDEDORA, C.A.</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0148</td>
                  <td>TOTAL BANK, C.A. BANCO UNIVERSAL</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
                <tr>
                  <td>0149</td>
                  <td>  Banco del Pueblo Soberano, Banco de Desarrollo</td>
                  <td> 0.00 </td>
                  <td> 0.00 </td>
                </tr>
              </table>
              <div class="box-tools">
                <ul class="pagination pagination-sm no-margin pull-right">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  </div>

  <!-- modal for iframe -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog" style="width: auto; height: auto; margin: 7vh auto 0px 2vh; ">
      <div class="modal-content bmd-modalContent">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Modelo de resumen #2</h4>
        </div>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" frameborder="0"></iframe>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
  <!-- end iframe -->
  @endsection

  @section('after-scripts-end')



    @include('includes.partials.monitoreo.resumen.Chart')

    {{-- script for iframe modal --}}
    <script>
      (function($) {
      
        $.fn.bmdIframe = function( options ) {
        var self = this;
        var settings = $.extend({
          classBtn: '.bmd-modalButton',
        }, options );
        
        $(settings.classBtn).on('click', function(e) {
        
          var dataResumen = {
            'src': $(this).attr('data-bmdSrc'),
          };
          
          // print our data in the iframe
          $(self).find("iframe").attr(dataResumen);
          });
          
          // if we close the modal we reset the data of the iframe to prevent a video from continuing to reproduce even when the modal is closed
          this.on('hidden.bs.modal', function(){
            $(this).find('iframe').html("").attr("src", "");
          });
          
          return this;
        };
      
      })(jQuery);
      jQuery(document).ready(function(){
        jQuery("#myModal").bmdIframe();
      });
    </script>
  @stop