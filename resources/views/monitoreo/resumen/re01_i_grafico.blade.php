<div class="row">
  <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title"> Operaciones por hora</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <p class="text-center">
              <strong>Fecha compensación 24-10-2018 </strong>
            </p>
            <div class="chart">
              <!-- Sales Chart Canvas -->
              <canvas id="salesChart" style="height: 180px;"></canvas>
            </div>
            <!-- /.chart-responsive -->
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="text-center">
                <span class="text-black"><i class="fa fa-square" style="color: #066413;"></i>&nbsp; Cheques </span>&nbsp;&nbsp;
                <span class="text-black"><i class="fa fa-square" style="color: #3b8bba;"></i>&nbsp; Domiciliaciones </span>&nbsp;&nbsp;
                <span class="text-black"><i class="fa fa-square" style="color: #c1c7d1;"></i>&nbsp; Créditos directos </span>
              </div>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      {{-- @permissions(['R0100-01-03']) --}}

        <div class="box-footer clearfix">
          <!-- buttom to iframe -->
<!--          <button type="button" class="bmd-modalButton btn btn-sm btn-info btn-flat pull-right" data-toggle="modal" data-bmdSrc="{{url('Inicio')}}" data-target="#myModal">Ver más</button> -->
          <!-- end buttom to iframe -->
        </div>
        
      {{-- @endauth --}}
    </div>
  </div>
</div>
