<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Período de compensación</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="row col-sm-12">
          <!-- presentadas creditos directos -->
          <div class="col-sm-6 col-xs-6" style="background-color: #ebe6ed">
            <div class="description-block"><br>
              <span class="description-text pull-left"><b>Créditos directos</b></span><br>
              <center>{{ $operaciones->where('cod_tipo_operacion', '010')->first()->cod_tipo_operacion }}- Presentadas</center>
              <div class="row">
                <div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->dia_inicio }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->hora_inicio }}</div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->dia_fin }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->hora_fin }} </div>
              </div>
              <div class="progress progress-sm">
                <div class="progress-bar progress-bar-success" style="width: {{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->porcentaje  }}%" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">70% Complete</span>
                </div>
              </div>
              <div class="row text-left" style="margin: 2px">
                <span class="description-percentage"><b>Operaciones:</b></span>
                <span class="description-percentage">{{ number_format($operaciones->where('cod_tipo_operacion', '010')->first()->num_transaccion_validadas, 0 , ',', '.')  }}</span>
                <br>
                <span class="description-percentage"><b>Montos:</b></span>
                <span class="description-percentage">Bs.S. {{ number_format($operaciones->where('cod_tipo_operacion', '010')->first()->monto_validadas/100, 2 , ',', '.')  }}</span>
              </div>
            </div>
            <br>
          </div>
          <!-- fin de presentadas domiciliacion -->
          <!-- Devueltas credito directo-->
          <div class="col-sm-6 col-xs-6" style="background-color: #ebe6ed"><br>
            <div class="description-block">
              <span class="description-text"><b></b></span><br>
              <center>{{ $operaciones->where('cod_tipo_operacion', '110')->first()->cod_tipo_operacion }}- Devueltas</center>
              <div class="row">
                <div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '110')->first()->dia_inicio }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->hora_inicio }}</div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '110')->first()->dia_fin }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '010')->first()->hora_fin }}</div>
              </div>
              <div class="progress progress-sm">
                <div class="progress-bar progress-bar-warning" style="width:{{ $intervalos_liquidacion->where('cod_tipo_operacion', '110')->first()->porcentaje  }}%" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">30% Complete</span>
                </div>
              </div>
              <div class="row text-left" style="margin: 2px">              
                <span class="description-percentage"><b>Operaciones:</b></span>
                <span class="description-percentage">{{ number_format($operaciones->where('cod_tipo_operacion', '110')->first()->num_transaccion_validadas, 0 , ',', '.')  }}</span>
                <br>
                <span class="description-percentage"><b>Montos:</b></span>
                <span class="description-percentage">Bs.S. {{ number_format($operaciones->where('cod_tipo_operacion', '110')->first()->monto_validadas/100, 2 , ',', '.')  }}</span>
              </div>
            </div>
            <br>
          </div>
          <!-- fin de Devueltas domiciliacion -->


          <!-- Presentadas cheques -->
          <div class="col-sm-6 col-xs-6" style="background-color: #f8f8f8">
            <div class="description-block"><br>
              <span class="description-text pull-left"><b>Cheques</b></span><br>
              <center>{{ $operaciones->where('cod_tipo_operacion', '030')->first()->cod_tipo_operacion }}- Presentadas</center>
              <div class="row">
                <div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '030')->first()->dia_inicio }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '030')->first()->hora_inicio }}</div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '030')->first()->dia_fin }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '030')->first()->hora_fin }}</div>
              </div>
              <div class="progress progress-sm">
                <div class="progress-bar progress-bar-success" style="width: {{ $intervalos_liquidacion->where('cod_tipo_operacion', '030')->first()->porcentaje  }}%" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">70% Complete</span>
                </div>
              </div>
              <div class="row text-left" style="margin: 2px">
                <span class="description-percentage"><b>Operaciones:</b></span>
                <span class="description-percentage">{{ number_format($operaciones->where('cod_tipo_operacion', '030')->first()->num_transaccion_validadas, 0 , ',', '.')  }}</span>
                <br>
                <span class="description-percentage"><b>Montos:</b></span>
                <span class="description-percentage">Bs.S. {{ number_format($operaciones->where('cod_tipo_operacion', '030')->first()->monto_validadas/100, 2 , ',', '.')  }}</span>
              </div>
            </div>
            <br>
          </div>
          <!-- fin de presentadas domiciliacion -->
          <!-- Devueltas cheques-->
          <div class="col-sm-6 col-xs-6" style="background-color: #f8f8f8"><br>
            <div class="description-block">
              <span class="description-text"><b></b></span><br>
              <center>{{ $operaciones->where('cod_tipo_operacion', '130')->first()->cod_tipo_operacion }}- Devueltas</center>
              <div class="row">
                <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '130')->first()->dia_inicio }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '130')->first()->hora_inicio }}</div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '130')->first()->dia_fin }}) 
                  {{ $intervalos_liquidacion->where('cod_tipo_operacion', '130')->first()->hora_fin }}</div>
              </div>
              <div class="progress progress-sm">
                <div class="progress-bar progress-bar-warning" style="width: {{ $intervalos_liquidacion->where('cod_tipo_operacion', '130')->first()->porcentaje  }}%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">50% Complete</span>
                </div>
              </div>
              <div class="row text-left" style="margin: 2px">
                <span class="description-percentage"><b>Operaciones:</b></span>
                <span class="description-percentage">{{ number_format($operaciones->where('cod_tipo_operacion', '130')->first()->num_transaccion_validadas, 0 , ',', '.')  }}</span>
                <br>
                <span class="description-percentage"><b>Montos:</b></span>
                <span class="description-percentage">Bs.S. {{ number_format($operaciones->where('cod_tipo_operacion', '130')->first()->monto_validadas/100, 2 , ',', '.')  }}</span>
              </div>
            </div>
            <br>
          </div>
          <!-- fin de Devueltas cheques -->


          <!--Presentadas domiciliacion -->
          <div class="col-sm-6 col-xs-6" style="background-color: #ebe6ed"><br>
            <div class="description-block">
              <span class="description-text pull-left"><b>Domiciliación</b></span><br>
              <center>{{ $operaciones->where('cod_tipo_operacion', '020')->first()->cod_tipo_operacion }}- Presentadas</center>
              <div class="row">
                <div class="col-md-6 col-xs-6 text-left">Inicio:({{ $intervalos_liquidacion->where('cod_tipo_operacion', '020')->first()->dia_inicio }}) {{ $intervalos_liquidacion->where('cod_tipo_operacion', '020')->first()->hora_inicio }}</div>
                <div class="col-md-6 col-xs-6 text-right">Fin:({{ $intervalos_liquidacion->where('cod_tipo_operacion', '020')->first()->dia_fin }}) {{ $intervalos_liquidacion->where('cod_tipo_operacion', '020')->first()->hora_fin }}</div>
              </div>
              <div class="progress progress-sm">
                <div class="progress-bar progress-bar-success" style="width: {{ $intervalos_liquidacion->where('cod_tipo_operacion', '020')->first()->porcentaje  }}%" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">40% Complete</span>
                </div>
              </div>
              <div class="row text-left" style="margin: 2px">
                <span class="description-percentage"><b>Operaciones:</b></span>
                <span class="description-percentage">{{ number_format($operaciones->where('cod_tipo_operacion', '020')->first()->num_transaccion_validadas, 0 , ',', '.')  }}</span>
                <br>
                <span class="description-percentage"><b>Montos:</b></span>
                <span class="description-percentage">Bs.S. {{ number_format($operaciones->where('cod_tipo_operacion', '020')->first()->monto_validadas/100, 2 , ',', '.')  }}</span>
              </div>
            </div>
            <br>
          </div>
          <!-- fin de pesentadas domiciliacion -->
          <!-- Devueltas domiciliacion -->
          <div class="col-sm-6 col-xs-6" style="background-color: #ebe6ed"><br>
            <div class="description-block">
              <span class="description-text"><b></b></span><br>
              <center>{{ $operaciones->where('cod_tipo_operacion', '120')->first()->cod_tipo_operacion }}- Devueltas</center>
              <div class="row">
                <div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '120')->first()->dia_inicio }}) {{ $intervalos_liquidacion->where('cod_tipo_operacion', '120')->first()->hora_inicio }}</div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('cod_tipo_operacion', '120')->first()->dia_fin }}) {{ $intervalos_liquidacion->where('cod_tipo_operacion', '120')->first()->hora_fin }}</div>
              </div>
              <div class="progress progress-sm">
                <div class="progress-bar progress-bar-warning" style="width: {{ $intervalos_liquidacion->where('cod_tipo_operacion', '120')->first()->porcentaje  }}%" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only">60% Complete</span>
                </div>
              </div>
              <div class="row text-left" style="margin: 2px">
                <span class="description-percentage"><b>Operaciones:</b></span>
                <span class="description-percentage">{{ number_format($operaciones->where('cod_tipo_operacion', '120')->first()->num_transaccion_validadas, 0 , ',', '.')  }}</span>
                <br>
                <span class="description-percentage"><b>Montos:</b></span>
                <span class="description-percentage">Bs.S. {{ number_format($operaciones->where('cod_tipo_operacion', '120')->first()->monto_validadas/100, 2 , ',', '.')  }}</span>
              </div>
            </div>
            <br>
          </div>
          <!-- fin de Devueltas domiciliacion -->
        </div>
        <!-- /.row -->
      </div>
      <div class="box-footer clearfix">
       {{-- @permissions(['R0100-01-05']) --}}

          <!-- buttom to iframe -->
          <?php //         <button type="button" class="bmd-modalButton btn btn-sm btn-info btn-flat pull-right" data-toggle="modal" data-bmdSrc="{{url('Inicio')}}" data-target="#myModal">Ver más</button>  --> //?>
          <!-- end buttom to iframe -->
        {{-- @endauth --}}
      </div>
    </div>
  </div>
</div>