<div class="row">
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Posiciones multilaterales</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button><!--
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <!-- filtro de busqueda -->
          <form role="form" class="form-horizontal">
            <div class="col-sm-4">
              <br>
              <div class="form-group">
                {!!Form::label('Estado', 'Estado', array('class' => 'col-sm-2 control-label'))!!}
                <div class="col-sm-6">
                  {!! Form::select('estatus',$estatus,$estatu,['id'=>'estatus','class'=>'form-control']) !!}
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <br>
              <div class="form-group">
                {!!Form::label('Producto(*)', 'Producto(*)', array('class' => 'col-sm-3 control-label'))!!}
                <div class="col-sm-6">
                  {!! Form::select('producto',$productos,$producto,['id'=>'producto', "class"=>"form-control",'required']) !!}
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <br>
              <div class="">
                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
              </div>
            </div>
          </form>
          <!-- Fin filtro de busqueda -->
        </div>
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>Código </th>
                <th>Nombre </th>
                <th>Estado</th>
                <th>Deudor</th>
                <th>Acreedor</th>
              </tr>
            </thead>
            <tbody>
              
              @foreach($posiciones as $posicion)
              <tr>
                <td><a href="">{{ $posicion->codigo }}</a></td>
                <td>{{ $posicion->nombre }}</td>
                
                @if ($posicion->posicion>0 and $posicion->posicion !=null )
                <td> <span class="label label-success">Acreedor</span></td>
                <td style="text-align: right"> <div class="sparkbar" data-color="#00a65a" data-height="20"> 0, 00 </div> </td>
                <td style="text-align: right"><div class="sparkbar" data-color="#00a65a" data-height="20">   {{ number_format($posicion->posicion/100, '2', ',' , '.') }} </div> </td>
                @endif
                @if ($posicion->posicion<0 and $posicion->posicion !=null )
                <td><span class="label label-danger">&nbsp;Deudor&nbsp;&nbsp;</span></td>
                <td style="text-align: right"><div class="sparkbar" data-color="#00a65a" data-height="20">     {{ number_format($posicion->posicion/100, '2', ',' , '.') }} </div> </td>
                <td style="text-align: right"><div class="sparkbar" data-color="#00a65a" data-height="20">  0, 00 </div></td>
                @endif
                @if ($posicion->posicion==null)
                <td> <span class="label bg-yellow-eps">&nbsp;&nbsp;En cero&nbsp;&nbsp;</span></td>
                <td style="text-align: right"><div class="sparkbar" data-color="#00a65a" data-height="20"> 0, 00 </div></td>
                <td style="text-align: right"><div class="sparkbar" data-color="#00a65a" data-height="20"> 0, 00 </div></td>
                @endif
                
              </tr>
              @endforeach
              <tr style="background-color: #CBCACA; font-weight: bold;" >
                <td></td>
                <td></td>
                <td>Totales</td>
                <td style="text-align: right">{{ number_format($total_negativos/100, '2', ',' , '.') }}</td>
                <td style="text-align: right">{{ number_format($total_positivos/100, '2', ',' , '.') }}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <!-- buttom to iframe -->
     {{-- @permissions(['R0100-01-07']) --}}

        <?php //         <button type="button" class="bmd-modalButton btn btn-sm btn-info btn-flat pull-right" data-toggle="modal" data-bmdSrc="{{url('Monitoreo/Resumenopc2')}}" data-target="#myModal">Ver más</button> ?>
        <!-- end buttom to iframe -->
     {{-- @endauth --}}
      </div>
      <!-- /.box-footer -->
    </div>
  </div>
</div>