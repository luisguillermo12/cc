{{-- @Nombre del programa: Vista Principal Index--}}
{{-- @Funcion: Se visualiza la pagina de bienvenida despues del login --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 03/12/2018 --}}
{{-- @Modificado por: ivan --}}
@extends('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-10">
      <h1 class="m-0 text-dark">Bienvenido <b>{{ auth::user()->name }}</b> a Electronic Payment Suite (EPS)<small>Panel de control</small></h1>
    </div>
    <div class="col-sm-2">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active"><i class="fa fa-home"></i><a> Inicio</a></li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('content')
  <style type="text/css">
    .productos-icons {
      color: #f4a329;
      font-size: 50px !important;
    }
    .daterangeBarHourCheques .progress ,
    .daterangeBarHourCreditod .progress,
      .daterangeBarHourDomiciliaciones .progress {
      background-color: #ccc !important;
    }
    .img-home-page {
      height: 500px;
      width: 100%;
    }
  </style>
  <div class="row">
    <div class="col-md-12">
      <img class="img-thumbnail img-home-page" src="{{asset('/img/financial.jpg')}}" alt="Electronic Payment Suite (EPS)">
    </div>
  </div>
  <br />
  <div class="container-fluid">
    <div class="row">
        <!-- CRÉDITOS DIRECTOS -->
        <div class="col-md-4">
          <div class="card card-primary card-outline collapsed-card">
            <div class="card-header">
              <h3 class="card-title">
              </h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <center><span class="fa fa-exchange productos-icons"></span>
              <h4>CRÉDITOS DIRECTOS</h4>
              <p>Transferencia electrónica de fondos</p></center><!-- /.box-tools -->
              <br>
              <strong><center>PERÍODO DE COMPENSACIÓN </center></strong>
              <br>
              <strong><center>{{ $operaciones->where('codigo', '010')->first()->codigo }}- PRESENTADAS</center></strong>
              <div class="row">
                <p><div class="col-md-6 col-xs-6 text-left" >Inicio: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '010')->first()->id)->first()->dia_inicio  }})
                {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '010')->first()->id)->first()->hora_inicio  }} </div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '010')->first()->id)->first()->dia_fin  }})
                {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '010')->first()->id)->first()->hora_fin  }}</div> </p>
              </div>
              <div class="daterangeBarHourCreditod"></div>
              <strong><center>{{ $operaciones->where('codigo', '110')->first()->codigo }}- DEVUELTAS </center></strong>
              <div class="row">
                <p><div class="col-md-6 col-xs-6 text-left" >Inicio: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '110')->first()->id)->first()->dia_inicio  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '110')->first()->id)->first()->hora_inicio  }} </div>
                <div class="col-md-6 col-xs-6 text-right" >Fin: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '110')->first()->id)->first()->dia_fin  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '110')->first()->id)->first()->hora_fin  }}</div> </p>
              </div>
              <div class="daterangeBarHourCreditodevuelta"></div>
            </div>
            <div class="card-body">
              <p ALIGN="justify">
                Una orden escrita librada por una de las partes (el librador) hacia otra (el librado, generalmente un banco) que requiere que el librado pague una suma específica a pedido del librador o a un tercero que éste especifique.<br><br><br><br><br><br><br><br><br></p>
            </div>
          </div>
        </div>
        <!-- DOMICILIACIONES -->
        <div class="col-md-4">
          <div class="card card-primary card-outline collapsed-card">
            <div class="card-header">
              <h3 class="card-title">
              </h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <center><span class="fa fa-clock-o productos-icons"></span>
              <h4>DOMICILIACIONES</h4>
              <p>Débito automático</p></center><!-- /.box-tools -->
              <br>
              <strong><center>PERÍODO DE COMPENSACIÓN </center></strong>
              <br>
              <strong><center>{{ $operaciones->where('codigo', '020')->first()->codigo }}- PRESENTADAS </center></strong>
              <div class="row">
                <p ><div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '020')->first()->id)->first()->dia_inicio  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '020')->first()->id)->first()->hora_inicio  }} </div>
                <div class="col-md-6 col-xs-6 text-right" >Fin: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '020')->first()->id)->first()->dia_fin  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '020')->first()->id)->first()->hora_fin  }} </div> </p>
              </div>
              <div class="daterangeBarHourDomiciliaciones"></div>
              <strong><center>{{ $operaciones->where('codigo', '120')->first()->codigo }}- DEVUELTAS</center></strong>
              <div class="row">
                <p><div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '120')->first()->id)->first()->dia_inicio  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '120')->first()->id)->first()->hora_inicio  }} </div>
                <div class="col-md-6 col-xs-6 text-right" >Fin: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '120')->first()->id)->first()->dia_fin  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '120')->first()->id)->first()->hora_fin  }} </div></p>
              </div>
              <div class="daterangeBarHourDomiciliacionesdevu"></div>
            </div>
            <div class="card-body">
              <p ALIGN="justify">
               Débitos directos ejecutados, con cierta regularidad, mediante una orden de cobro por la prestación de servicios o adquisición de bienes, emitida por la empresa prestadora de servicios o proveedora de bienes, en virtud de la autorización emanada de un cliente ordenante, para que sea debitado automáticamente el monto del servicio prestado o bien adquirido de la cuenta que éste expresamente señale, en los términos acordados previamente por dicho cliente con la empresa o con una Institución Bancaria Participante.</p>
            </div>
          </div>
        </div>
        <!-- CRÉDITOS DIRECTOS -->
        <div class="col-md-4">
          <div class="card card-primary card-outline collapsed-card">
            <div class="card-header">
              <h3 class="card-title">
              </h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <center><span class="fa fa-lock productos-icons"></span>
              <h4>CHEQUES</h4>
              <p>Procesamiento de cheques con y sin imágenes</p></center><!-- /.box-tools -->
              <br>
              <strong><center>PERÍODO DE COMPENSACIÓN </center></strong>
              <br>
              <strong><center>{{ $operaciones->where('codigo', '030')->first()->codigo }}- PRESENTADAS </center></strong>
              <div class="row">
                <p><div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '030')->first()->id)->first()->dia_inicio  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '030')->first()->id)->first()->hora_inicio  }} </div>
                <div class="col-md-6 col-xs-6 text-right" >Fin: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '030')->first()->id)->first()->dia_fin  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '030')->first()->id)->first()->hora_fin  }}</div> </p>
              </div>
              <div class="daterangeBarHourCheques"></div>
              <strong><center>{{ $operaciones->where('codigo', '130')->first()->codigo }}- DEVUELTAS</center></strong>
              <div class="row">
                <p><div class="col-md-6 col-xs-6 text-left">Inicio: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '130')->first()->id)->first()->dia_inicio  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '130')->first()->id)->first()->hora_inicio  }} </div>
                <div class="col-md-6 col-xs-6 text-right">Fin: ({{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '130')->first()->id)->first()->dia_fin  }}) {{ $intervalos_liquidacion->where('tipo_operacion_id',(string)$operaciones->where('codigo', '130')->first()->id)->first()->hora_fin  }}</div> </p>
              </div>
              <div class="daterangeBarHourChequesdevuelta"></div>
            </div>
            <div class="card-body">
              <p ALIGN="justify">
               Una orden escrita librada por una de las partes (el librador) hacia otra (el librado, generalmente un banco) que requiere que el librado pague una suma específica a pedido del librador o a un tercero que éste especifique.<br><br><br><br><br><br><br><br><br></p>
            </div>
          </div>
        </div><!-- /.card -->
    </div>
  </div>
@endsection
@section('before-scripts-end')
  @include('includes.scripts.inicio.scripts_inicio')
@stop