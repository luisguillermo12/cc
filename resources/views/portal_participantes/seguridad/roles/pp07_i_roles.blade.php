{{-- @Nombre del programa: --}}
{{-- @Funcion: Listar los roles diaponibles --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> Roles </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-lock"></i>Portal Participantes</a></li>
        <li class="breadcrumb-item"></i>Seguridad</a></li>
        <li class="breadcrumb-item active">Roles</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if (auth()->user()->hasPermission(['R0350-01-01-02']))
          <a title="Crear usuario" class="btn btn-sm btn-success" href="{{route('Role.create')}}"><i class="fa fa-plus"></i> Crear Rol</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <thead>
            <tr class ="theadtable">
              <th>Nombre </th>
              <th >Usuarios</th>
              <th><center>Acciones</center></th>
            </tr>
          </thead>
          <tbody>
            @foreach($roles as $rol)
            <tr>
              <td>{{$rol->name}}</td>
              <td>{{$rol->users()->count()}}</td>
              <td>
                <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn-sm btn-primary" href="{{route('Role.edit', $rol->id)}}"><i class="fa fa-pencil"></i></a>
                <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm btn-danger" href="#" OnClick="Eliminar('{{$rol->id}}')"><i class="fa fa-trash"></i></a>
                <a data-toggle="tooltip" data-placement="top" title="Crear clon" class="btn-sm btn-info" href="{{route('PortalParticipantes.Seguridad.Roles.createclon', $rol->id)}}"><i class="fa fa-clone"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>
@stop

@section('after-scripts-end')
@include('includes.scripts.portal_participantes.roles')
@endsection

{{-- fin vista --}}
