{{-- @Nombre del programa: --}}
{{-- @Funcion: Editar un rol --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> Clonar </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-lock"></i>Seguridad</a></li>
        <li class="breadcrumb-item active">Roles</li>
        <li class="breadcrumb-item active">Clonar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{!! Form::model($role, ['route' => ['PortalParticipantes.Seguridad.Roles.updateclon', $role], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role']) !!}

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Crear</h3>

        <div class="card-body">
          <div class="box-body">
            <div class="form-group row">
              <div class="col-sm-6 offset-sm-3">
                @include('includes.messages')
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-6 offset-sm-3">
                <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios .</span>
                <span id="helpBlock" class="help-block">se debe Cambiar el nombre del rol ya que debe ser unico .</span>
              </div>
            </div>

            <div class="form-group row">
              {!! Form::label('name', 'Nombre(*)', ['class' => 'col-lg-3 col-form-label']) !!}
              <div class="col-lg-6">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del Rol']) !!}
              </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group row">
              {!! Form::label('associated-permissions','Permisos asociados(*)', ['class' => 'col-lg-3 col-form-label']) !!}

              <div class="col-sm-6">
                {!! Form::select('associated-permissions', array('all' => trans('labels.general.all'), 'custom' => trans('labels.general.custom')), $role->todos == 1 ? 'all' : 'custom', ['class' => 'form-control']) !!}
              </div>
              <div class="col-sm-8 offset-sm-2">
                <div id="available-permissions" class="{{ $role->todos == 1 ? 'hide' : '' }} mt-20">
                  <div class="row">
                    <div class="col-sm-12">
                      @forelse ($permissions as $module => $permisos)
                      <fieldset class="col-sm-12" style="border: 1px solid #e5e5e5">
                        <legend style="padding: 0 12px;">
                          <b>Permisos para el módulo de {{ $module }}:</b>
                          <div class="pull-right">
                            <small><i>Seleccionar todos</i></small>
                            &nbsp;
                            <input type="checkbox" class="none" id="{{ $module }}" onclick="seleccionarTodos('{{ $module }}')">
                          </div>
                        </legend>
                        @foreach ($permisos as $perm)
                        <div class="col-sm-6">
                          <input type="checkbox" class="{{ $module }} form-check-input" name="permissions[]" value="{{ $perm->id }}" id="perm_{{ $perm->id }}" {{in_array($perm->id, $role_permissions) ? 'checked' : ""}}>
                          <label class="form-check-label" for="perm_{{ $perm->id }}">{{ $perm->display_name }}</label><br/>
                        </div>
                        @endforeach
                      </fieldset>
                      @empty
                      <p>No hay permisos disponibles.</p>
                      @endforelse
                    </div><!--col-sm-6-->
                  </div><!--row-->
                </div><!--available permissions-->
              </div><!--col-sm-3-->
            </div><!--form control-->

            <div class="form-group row">
              <div class="col-lg-10">
                {!! Form::hidden('sort', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.eps.seguridad.role.sort')]) !!}
              </div><!--col-lg-10-->
            </div><!--form control-->

            <div class="form-group row">
              <div class="col-sm-12">
                <div class="text-center">
                  {!! Form::submit('Clonar', ['class' => 'btn btn-success btn-sm','title'=>'Actualizar']) !!}
                  {!! link_to_route('Roles.index','Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop

@section('after-scripts-end')
{{ Html::script('js/backend/access/roles/script.js') }}
<script type="text/javascript" >
  modules = [
  "{!! $permissions->keys()->implode('", "') !!}"
  ];

  function chearEstado(module) {
    if ($('input.' + module).not(':checked').length > 0) {
      $('input#' + module).prop('checked', false);
    } else if ($('input.' + module).not(':checked').length == 0) {
      $('input#' + module).prop('checked', true);
    }
  }

  for (i = 0; i < modules.length; i++) {
    chearEstado(modules[i]);
  }

  $("input:checkbox").click(function() {
    module = $(this).attr("class");
    chearEstado(module);
  });

  function seleccionarTodos(module) {
    if ($("#" + module).not(':checked').length == 0) {
      $("." + module).prop('checked', true);
    } else {
      $("." + module).prop('checked', false);
    }
  };
</script>
  @include('includes.scripts.seguridad.ocultar_permisos')
@stop
{{-- fin vista --}}
