{{-- @Nombre del programa: --}}
{{-- @Funcion:  cambiar la contraseña de acceso de los usuarios--}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i>@if ( $user->status ==1 ) Cambiar Contraseña Activos @else Cambiar Contraseña Inactivos @endif  </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item">Portal Participantes</li>
        <li class="breadcrumb-item">Seguridad</a></li>
        <li class="breadcrumb-item active">Usuarios</li>
        <li class="breadcrumb-item active">@if ( $user->status ==1 ) Activos @else Inactivos @endif</li>
        <li class="breadcrumb-item active">Cambiar Contraseña</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{!! Form::open(['route' => ['PortalParticipantes.Seguridad.Usuarios.UpdatePassword', $user->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-change-password']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Cambiar Contraseña</h3>

        <div class="card-body">

          <div class="form-group row">
            <div class="col-sm-6 offset-sm-3">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6 offset-sm-3">
              <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
            </div>
          </div>
          <div class="form-group row">
            {!! Form::label('password', 'Contraseña(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-6">
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña']) !!}
            </div><!--col-sm-10-->
          </div><!--form control-->

          <div class="form-group row">
            {!! Form::label('password_confirmation', 'Confirmar contraseña(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-6">
              {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmar Contraseña']) !!}
            </div><!--col-sm-10-->
          </div><!--form control-->

          <div class="form-group row ">
            <div class="col-sm-12">
              <div class="text-center">
                {!! Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title'=>'Actualizar']) !!}
                {!! link_to_route('Seguridad.Usuarios.Activos', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
              </div>
            </div>
          </div>

          <div class="clearfix"></div>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
{{-- fin de la vista --}}
@stop

@section('after-scripts-end')
{{ Html::script('js/backend/access/users/script.js') }}
@stop
