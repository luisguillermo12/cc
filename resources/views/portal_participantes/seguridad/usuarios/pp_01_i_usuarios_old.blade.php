{{-- @Nombre del programa: usuarios--}}
{{-- @Funcion: listar usuarios --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019--}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por: btc--}}
@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> {{$status}}</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><i class="fa fa-lock"></i>Seguridad</a></li>
              <li class="breadcrumb-item active">Usuarios</li>
              <li class="breadcrumb-item active">{{$status}}</li>
            </ol>
          </div>
        </div>
      </div>
@endsection

@section('content')
      <section class="content">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header card-warning card-outline">
                    <h3 class="card-title">Listado</h3>
                    @if (auth()->user()->hasPermission(['R0350-01-01-02']))
                    <div class=" pull-right">
                      <a title="Crear usuario" class="btn btn-sm btn-success" href=""><i class="fa fa-plus"></i> Crear usuario</a>
                            <button type="button" class="btn-sm btn-box-tool" data-widget="collapse">
                              <i class="fa fa-minus"></i>
                            </button>
                     </div>
                     @endif
                  </div>
                  <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr class ="theadtable">
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th >Roles</th>
                            <th >Creado</th>
                            <th><center>Acciones</center></th>
                      </tr>
                      </thead>
                      <tbody>
                @foreach($users as $user)
                            <tr>
                            <td nowrap>{{ $user->name }}</td>
                            <td nowrap >{{ $user->email }}</td>
                            <td>@if (count($user->roles) != 0 ) {{ $user->roles->pluck('name')->implode(', ') }} @else ningun rol asociado @endif</td>
                            <td nowrap>{{  $user->created_at }}</td>
                            <td nowrap >
                      <!-- verifica si tiene permiso -->
                      @if($status == 'Activos' || $status == 'Inactivos')
                        @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                        <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn-sm btn-primary" href=""><i class="fa fa-pencil"></i></a>
                        @endif
                      @endif
                      @if($status == 'Activos' || $status == 'Inactivos')
                        @if (auth()->user()->hasPermission(['R0350-01-01-04']))
                               <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm btn-danger" href="#" OnClick="Eliminar('{{$user->id}}')"><i class="fa fa-trash"></i></a>
                        @endif
                      @endif
                      @if($status == 'Activos' || $status == 'Inactivos')
                        @if (auth()->user()->hasPermission(['R0350-01-01-01', 'R0350-01-03-01', 'R0350-01-02-01']))
                               <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn-sm btn-info" href=""><i class="fa fa-eye"></i></a>
                        @endif
                      @endif
                      @if($status == 'Activos' || $status == 'Inactivos')
                        @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                               <a data-toggle="tooltip" data-placement="top" title="Cambiar contraseña" class="btn-sm btn-info" href=""><i class="fa fa-key"></i></a>
                        @endif
                    @endif
                     @if( $status == 'Activos')
                        @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                               <a data-toggle="tooltip" data-placement="top" title="Desactivar usuario" class="btn-sm btn-warning" href="" ><i class="fa fa-pause"></i></a>
                        @endif
                    @endif
                    @if( $status == 'Inactivos')
                        @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                                <a data-toggle="tooltip" data-placement="top" title="Activar usuario" class="btn-sm btn-success" href=""><i class="fa fa-play"></i></a>
                        @endif
                    @endif
                    @if( $status == 'Eliminados')
                        @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                         <a data-toggle="tooltip" data-placement="top" title="Restaurar usuario" class="btn-sm btn-primary" href=""><i class="fa fa-refresh"></i></a>
                        @endif
                    @endif
                           </td>
                           </tr>
                @endforeach

                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="col-sm-12 text-right">
              @if($users->count()>10)
                {{ $users->links() }}
              @endif
              </div>




@endsection

