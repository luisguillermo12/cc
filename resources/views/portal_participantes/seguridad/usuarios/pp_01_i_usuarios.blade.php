{{-- @Nombre del programa: usuarios--}}
{{-- @Funcion: listar usuarios --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019--}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por: btc--}}
@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> {{$status}}</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-lock"></i>Portal Participantes</li>
        <li class="breadcrumb-item"></i>Seguridad</li>
        <li class="breadcrumb-item active">Usuarios</li>
        <li class="breadcrumb-item active">{{$status}}</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if (auth()->user()->hasPermission(['R0350-01-01-02']))
          @if( $status == 'Activos')
          <a title="Crear usuario" class="btn btn-sm btn-success" href="{{route('PortalParticipantes.Seguridad.Usuarios.create')}}"><i class="fa fa-plus"></i> Crear usuario</a>
          @endif
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <thead>
            <tr class ="theadtable">
              <th>Nombre</th>
              <th>Usuario</th>
              <th >Roles</th>
              <th >Participante</th>
              <th >Creado</th>
              <th><center>Acciones</center></th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td nowrap>{{ $user->name }}</td>
              <td nowrap >{{ $user->email }}</td>
              <td>@if (count($user->roles) != 0 ) {{ $user->roles->pluck('name')->implode(', ') }} @else ningun rol asociado @endif</td>
              <td nowrap >{{ $user->cod_banco }}</td>
              <td nowrap>{{  $user->created_at }}</td>
              <td nowrap >
                <!-- verifica si tiene permiso -->
                @if($status == 'Activos' || $status == 'Inactivos')
                @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn-sm btn-primary" href="{{route('PortalParticipantes.Seguridad.Usuarios.edit',$user->id)}}"><i class="fa fa-pencil"></i></a>
                @endif
                @endif

                @if($status == 'Activos' || $status == 'Inactivos')
                @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                <a data-toggle="tooltip" data-placement="top" title="Cambiar contraseña" class="btn-sm btn-info" href="{{route('PortalParticipantes.Seguridad.Usuarios.ChangePassword',$user->id)}}"><i class="fa fa-key"></i></a>
                @endif
                @endif

                @if(($status == 'Activos' || $status == 'Inactivos') &&  auth()->user()->id != $user->id)
                @if (auth()->user()->hasPermission(['R0350-01-01-04']))
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm btn-danger" href="#" OnClick="Eliminar('{{$user->id}}')"><i class="fa fa-trash"></i></a>
                @endif
                @endif

                @if( $status == 'Activos' && auth()->user()->id != $user->id)
                @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                <a data-toggle="tooltip" data-placement="top" title="Desactivar usuario" class="btn-sm btn-warning" href="#" OnClick="Desactivar('{{$user->id}}')" ><i class="fa fa-pause"></i></a>
                @endif
                @endif

                @if( $status == 'Inactivos')
                @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                <a data-toggle="tooltip" data-placement="top" title="Activar usuario" class="btn-sm btn-success" href="{{route('PortalParticipantes.Seguridad.Usuarios.activate',$user->id)}}"><i class="fa fa-play"></i></a>
                @endif
                @endif
                @if( $status == 'Eliminados')
                @if (auth()->user()->hasPermission(['R0350-01-01-03']))
                <a data-toggle="tooltip" data-placement="top" title="Restaurar usuario" class="btn-sm btn-primary" href="{{route('PortalParticipantes.Seguridad.Usuarios.restaurar',$user->id)}}"><i class="fa fa-refresh"></i></a>
                @endif
                @endif
              </td>
            </tr>
            @endforeach

          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-12 text-right">
  @if($users->count()>=10)
  {!! $users->links() !!}
  @endif
</div>

@endsection
@section('after-scripts-end')
@include('includes.scripts.portal_participantes.usuarios')
@endsection
