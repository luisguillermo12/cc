{{-- @Nombre del programa: --}}
{{-- @Funcion: crear acceso a un usuario --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> Crear </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-lock"></i>Portal Participantes</a></li>
        <li class="breadcrumb-item">Seguridad</li>
        <li class="breadcrumb-item ">Usuarios</li>
        <li class="breadcrumb-item ">Crear</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{!! Form::open(['route' => 'PortalParticipantes.Seguridad.Usuarios.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Crear</h3>

        <div class="card-body">

          <div class="form-group row">
            <div class="col-sm-6 offset-sm-3">
              @include('includes.messages')
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-6 offset-sm-3">
              <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
            </div>
          </div>

          <div class="form-group row">

            {!! Form::label('name', 'Nombre(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-6">
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
            </div>
          </div>

          <div class="form-group row">
            {!! Form::label('email', 'Usuario(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-6">
              {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Usuario']) !!}
            </div><!--col-sm-10-->
          </div><!--form control-->

          <div class="form-group row">
            {!! Form::label('password', 'Contraseña(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-6">
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Contraseña']) !!}
            </div><!--col-sm-10-->
          </div><!--form control-->

          <div class="form-group row">
            {!! Form::label('password_confirmation','Confirmación de contraseña(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-6">
              {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmación de Contraseña']) !!}
            </div><!--col-sm-10-->
          </div><!--form control-->


          <div class="form-group row">
            {!!Form::label('Participante Asociado', 'Participante asociado (*)', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!!Form::select('cod_banco',$bancos,null,['id'=>'cod_banco','class'=>'form-control']) !!}
            </div>
          </div>



          <div class="form-group row">
            {!! Form::label('status','Activo', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-1">
              {!! Form::checkbox('status', '1', true) !!}
            </div><!--col-sm-1-->
          </div><!--form control-->

          <input type="hidden" name="confirmed" value="1">

          <div class="form-group row">
            {!! Form::label('status','Roles asociados(*)', ['class' => 'col-sm-3 col-form-label']) !!}

            <div class="col-sm-5 offset-sm-1">
              @if (count($roles) > 0)
              @foreach($roles as $role)
              <input class="form-check-input" type="checkbox" value="{{ $role->id }}" name="assignees_roles[]" id="role-{{ $role->id }}" />
              <label class="form-check-label" for="role-{{ $role->id }}">{{ $role->name }}</label>
              <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                (
                <span class="show-text">Mostrar</span>
                <span class="hide-text hidden">Ocultar</span>
                Permisos
                )
              </a>
              <br/>
              <div class="permission-list hidden" data-role="role_{{ $role->id }}">
                @if ($role->todos)
                Todos los Permisos asignados.<br/><br/>
                @else
                @if (count($role->permissions) > 0)
                <blockquote class="small">{{--
                  --}}@foreach ($role->permissions as $perm){{--
                  --}}<span style="text-transform: capitalize;">{{$perm->display_name}}</span><br/>
                  @endforeach
                </blockquote>
                @else
                Sin Permisos asignados.<br/><br/>
                @endif
                @endif
              </div><!--permission list-->
              @endforeach
              @else
              No hay Roles disponibles.
              @endif
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-12">
              <div class="text-center">
                {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
                {!! link_to_route('PortalParticipantes.Seguridad.Usuarios.Activos', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}
{{-- fin de la vista --}}

@stop

@section('after-scripts-end')
{!! Html::script('js/backend/access/users/script.js') !!}
@stop
