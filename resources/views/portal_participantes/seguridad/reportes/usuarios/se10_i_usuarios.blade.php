{{-- @Nombre del programa: Vista Principal de se->Reportes->Usuarios --}}
{{-- @Funcion:  --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio Usuarios--}}

@section('page-header')
  <h1><i class="fa fa-lock fa-lg"></i> Usuarios</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"><i class="fa fa-lock fa-lg"></i> Seguridad</a></li>
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"> Reportes</a></li>
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"> Listado</a></li>
    <li class="active"> Usuarios</li>
  </ol>
@endsection

{{--Inicio Totales--}}

@section('content')
 <div class="row">

   <div class="col-md-12">
      <div class="box box-warning">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2-->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Totales</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>

        <div class="box-body">

          <div class="col-lg-4 col-xs-12">
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-green-eps">
                <i class="fa fa-check"></i>
              </span>
              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Activos</span>
                <span class="info-box-number-eps">{{ $activos }}</span>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-xs-12">
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-gray-eps">
                <i class="fa fa-pause"></i>
              </span>
              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Inactivos</span>
                <span class="info-box-number-eps">{{ $inactivos }}</span>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-xs-12">
            <div class="info-box-eps">
              <span class="info-box-icon-eps bg-red-eps">
                <i class="fa fa-trash"></i>
              </span>
              <div class="info-box-content-eps">
                <span class="info-box-text-eps">Eliminados</span>
                <span class="info-box-number-eps">{{ $eliminados }}</span>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
{{--Fin Totales--}}

{{--Inicio Listado--}}
    <div class="col-md-12">
      <div class="box box-primary">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2-->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Listado</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->

        <div class="box-body">
          <table class="table table-striped" id="icono-tabla">
            <thead class="thead-primary">
              <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Roles</th>
                <th>Estado</th>
              </tr>
            </thead>
            @foreach($usuarios as $usuario)
            <tr>
              <td>{{ $usuario->name }}</td>
              <td>{{ $usuario->email }}</td>
              <td>{{ $usuario->roles()->pluck('name')->implode(', ') }}</td>
              <?php $status = $usuario->status == 1 ? ['Activo', 'success'] : ['Inactivo', 'default']; ?>
              <?php $status = $usuario->deleted_at != null ? ['Eliminado', 'danger'] : $status; ?>
              <td><span class="label label-{{ $status[1] }}">{{ $status[0] }}</span></td>
            </tr>
            @endforeach
          </table>
          <div class="col-sm-12 text-right">
            {{ $usuarios->links() }}
          </div>
          <div class="col-sm-12 text-center">
            <a href="{{route('Seguridad.Reportes.Usuarios.Excel')}}" class="btn btn-success" role="button">Exportar a excel</a>
            <a href="{{route('Seguridad.Reportes.Usuarios.PDF')}}" class="btn btn-danger" role="button">Exportar a pdf</a>
          </div>
        </div>
      </div>
    </div>

  </div>
{{--Fin Listado--}}

@stop
{{--Fin Usuarios--}}

@section('after-scripts-end')
@stop
