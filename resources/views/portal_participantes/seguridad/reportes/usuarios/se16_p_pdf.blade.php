{{-- @Nombre del programa: Vista de Excel  Reporte de Usuarios --}}
{{-- @Funcion: Descargar el reporte de usuarios registrados en excel --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}
<!DOCTYPE html>
<html>
<head>
  <title>REPORTE USUARIOS</title>
  {!! Html::style('css/AdminLTE.css') !!}
  {!! Html::style('css/pdf.css') !!}
<body>

  <p>Banco Central de Venezuela</p>
<p>Gerencia de Tesorería</p>
<p>Departamento Cámara de Compensación Electrónica</p>
<h4 align=center>CÁMARA DE COMPENSACIÓN - REPORTE DE USUARIOS</h4>
<br>
<div class=".contenido">
  <table style="width: 100%" border="0" cellspacing="0">
    <thead style="background-color: #C2E7FC;">
      <tr class="class="thead-dark"" role="alert">
        <th>Nombre</th>
        <th>Email</th>
        <th>Roles</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($usuarios as $usuario)
      <tr>
        <td>{{ $usuario->name }}</td>
        <td>{{ $usuario->email }}</td>
        <td>{{ $usuario->roles()->pluck('name')->implode(', ') }}</td>
        <?php $status = $usuario->status == 1 ? ['Activo', ''] : ['Inactivo', 'default']; ?>
              <?php $status = $usuario->deleted_at != null ? ['Eliminado', ''] : $status; ?>
          <td style="text-align: center;"><span class="label label-{{ $status[1] }}">{{ $status[0] }}</span></td>
      </tr>
      @endforeach
    </tbody>
   </table>
</div>
</body>
</html>
