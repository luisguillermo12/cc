{{-- @Nombre del programa: Vista PDF re->Listado General --}}
{{-- @Funcion: --}}
{{-- @Autor: --}}
{{-- @Fecha Creacion: 05/09/18--}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 05/09/18--}}
{{-- @Modificado por: Ericcor--}}

<p>Banco Central de Venezuela</p>
<p>Gerencia de Tesorería</p>
<p>Departamento Cámara de Compensación Electrónica</p>
<h4 align=center>CÁMARA DE COMPENSACIÓN REPORTES-SEGURIDAD </h4>
<h4 align=center>Listado de reportes</h4>
<br>
<div class="row">
   <table border="0" width="100%" cellspacing="0" cellpadding="5">
      <thead style="background-color: #C2E7FC;">
                <tr>
                   <th style="text-align: center;" >Código</th>
                   <th style="text-align: center;" >Nombre del reporte </th>
                   <th style="text-align: center;" >Formato</th>
                   <th style="text-align: center;" >Estado  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                 <td><center>R0280/center></td>
                 <td>Usuarios</td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

                 <tr>
                 <td><center>R0281</center></td>
                 <td>Roles</td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

                <tr>
                 <td><center>R0282</center></td>
                 <td>Control de acceso</td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

                 <tr>
                 <td><center>R0283</center></td>
                 <td>Histórico de cambios</td>
                 <td><center>PDF/EXCEL</center></td>
                 <td><center>Disponible</center></td>
                 </tr>

              </tbody>
            </table>
</div>




