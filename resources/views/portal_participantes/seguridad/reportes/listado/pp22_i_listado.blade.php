{{-- @Nombre del programa: Vista Principal se->Reportes->Listado --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 09/09/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 09/09/2018 --}}
{{-- @Modificado por:  --}}
@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-lock" style="font-size: 24px !important;"></i> Listado </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-lock"></i>Seguridad</a></li>
        <li class="breadcrumb-item active">Reportes</li>
        <li class="breadcrumb-item active">Listado</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          <button type="button" class="btn-sm btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <thead>
            <tr class ="theadtable">
              <th>Codigo</th>
              <th>Nombre del Reporte</th>
              <th >Formato</th>
              <th >Estado</th>
              <th><center>Accion</center></th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <td>R0280</td>
              <td>Usuarios</td>
              <td style="text-align: center;">
                <small class="label bg-red"><i class="fa fa-file-pdf-o"></i></small>
                <small class="label bg-green"><i class="fa fa-file-excel-o"></i></small>
              </td>

              <td style="text-align: center;">
                <span class="pull-right-container">
                  <small class="label bg-green">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
              </td>

              <td style="text-align: center;">
                <a class="btn btn-sm btn-info" href="{{url('Seguridad/Reportes/Listado/Usuarios')}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a>
              </td>
            </tr>

            <tr>
              <td>R0281</td>
              <td>Roles</td>
              <td style="text-align: center;">
                <small class="label bg-red"><i class="fa fa-file-pdf-o"></i></small>
                <small class="label bg-green"><i class="fa fa-file-excel-o"></i></small>
              </td>

              <td style="text-align: center;">
                <span class="pull-right-container">
                  <small class="label bg-green">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
              </td>

              <td style="text-align: center;">
                <a class="btn btn-sm btn-info" href="{{url('Seguridad/Reportes/Listado/Roles')}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a>
              </td>
            </tr>

            <tr>
              <td>R0282</td>
              <td>Control de acceso</td>
              <td style="text-align: center;">
                <small class="label bg-red"><i class="fa fa-file-pdf-o"></i></small>
                <small class="label bg-green"><i class="fa fa-file-excel-o"></i></small>
              </td>

              <td style="text-align: center;">
                <span class="pull-right-container">
                  <small class="label bg-green">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
              </td>

              <td style="text-align: center;">
                <a class="btn btn-sm btn-info" href="{{url('Seguridad/Reportes/Listado/ControlAcceso')}} " data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a>
              </td>
            </tr>

            <tr>
              <td>R0283</td>
              <td>Histórico de cambios</td>
              <td style="text-align: center;">
                <small class="label bg-red"><i class="fa fa-file-pdf-o"></i></small>
                <small class="label bg-green"><i class="fa fa-file-excel-o"></i></small>
              </td>

              <td style="text-align: center;">
                <span class="pull-right-container">
                  <small class="label bg-green">Disponible &nbsp;&nbsp;<i class="fa fa-tag"></i></small>
                </span>
              </td>

              <td style="text-align: center;">
                <a class="btn btn-sm btn-info" href="{{url('Seguridad/Reportes/Listado/HistoricodeCambios')}}" data-toggle="tooltip" data-placement="top" title="Ver reporte"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
          </tbody>
        </table>
        <br>
        <div class="col-sm-12">
          <p class="text-center">
            <a href="{{url('Seguridad/Reportes/Listados/Excel')}}" class="btn btn-primary btn-success" role="button">Exportar a excel</a>
            <a href="{{url('Seguridad/Reportes/Listados/Pdf')}}" class="btn btn-primary btn-danger" role="button">Exportar a pdf</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
{{--Fin--}}
@section('after-scripts-end')

@stop
