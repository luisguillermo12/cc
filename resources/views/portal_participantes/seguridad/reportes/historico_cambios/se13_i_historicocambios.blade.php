{{-- @Nombre del programa: Vista Principal de se->Reportes->Histórico de Cambios--}}
{{-- @Funcion:  --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio Histórico de Cambios--}}

@section('page-header')
  <h1><i class="fa fa-lock fa-lg"></i> Histórico de cambios</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"><i class="fa fa-lock fa-lg"></i> Seguridad</a></li>
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"> Reportes</a></li>
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"> Listado</a></li>
    <li class="active"> Histórico de cambios</li>
  </ol>
@endsection

{{--Inicio Listado--}}

@section('content')
 <div class="row">

    <div class="col-md-12">
      <div class="box box-warning">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2-->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Listado</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->

        <div class="box-body">
        <div class="table-responsive">
          <table class="table table-striped" id="icono-tabla">
            <thead class="thead-primary">
              <tr>
                <th>Usuario</td>
                <th>Fecha</td>
                <th>IP</td>
                <th>Acción</td>
                <th>Ruta</td>
                <th>Detalle</td>
              </tr>
            </thead>
            @foreach($logs as $log)
            <tr>
              <td>{{ $log->username }}</td>
              <td>{{ $log->created_at }}</td>
              <td>{{ $log->ip_address }}</td>
              <td>{{ $log->event }}</td>
              <td>{{ $log->url }}</td>
              <td class="text-center"><a data-toggle="tooltip" data-placement="top" title="Ver detalles"  class="btn btn-xs btn-info" href="{{ route('Seguridad.Reportes.Listado.HistoricodeCambios.show', $log->id) }}"  role="button"><i class="fa fa-eye"></i></a></td>
            </tr>
            @endforeach
          </table>
          <div class="col-sm-12 text-right">
            {{ $logs->links() }}
          </div>
          <div class="col-sm-12 text-center">
            <a href="{{route('Seguridad.Reportes.HistoricodeCambios.Excel')}}" class="btn btn-success" role="button">Exportar a excel</a>
            <a href="{{ route('Seguridad.Reportes.HistoricodeCambios.PDF') }}?page={{ request()->get('page') }}" class="btn btn-danger" role="button">Exportar a pdf</a>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
{{--Fin Listado--}}

@stop
{{--Fin Histórico de Cambios--}}

@section('after-scripts-end')

@stop
