{{-- @Nombre del programa: Vista Principal de se->Reportes->Control de Acceso--}}
{{-- @Funcion:  --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 30/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 30/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio Control de Acceso--}}

@section('page-header')
  <h1><i class="fa fa-lock fa-lg"></i> Control de acceso</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"><i class="fa fa-lock fa-lg"></i> Seguridad</a></li>
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"> Reportes</a></li>
    <li><a href="{{url('Seguridad/Reportes/Listado')}}"> Listado</a></li>
    <li class="active"> Control de acceso</li>
  </ol>
@endsection

{{--Inicio Listado--}}

@section('content')
 <div class="row">

    <div class="col-md-12">
      <div class="box box-warning">

        <div class="box-header with-border">
          <!--codigo gui MOD-REPO-1.2-->
          <input type="hidden" name="codigo_gui" value="MOD-REPO-1.2.1" id="codigo_gui">
          <h3 class="box-title">Listado</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->

        <div class="box-body">
          <table class="table table-striped" id="icono-tabla">
            <thead class="thead-primary">
              <tr>
                <th>Usuario</th>
                <th>IP</th>
                <th>Fecha</th>
                <th>Acción</th>
              </tr>
            </thead>
            @foreach($logs as $log)
            <tr>
              <td>{{ $log->username }}</td>
              <td>{{ $log->ip_address }}</td>
              <td>{{ $log->created_at }}</td>
              <td>{{ $log->action }}</td>
            </tr>
            @endforeach
          </table>
          <div class="col-sm-12 text-right">
            {{ $logs->links() }}
          </div>
          <div class="col-sm-12 text-center">
            <a href="{{route('Seguridad.Reportes.ControlAcceso.Excel')}}" class="btn btn-success" role="button">Exportar a excel</a>
            <a href="{{route('Seguridad.Reportes.ControlAcceso.PDF')}}?page={{ request()->get('page') }}" class="btn btn-danger" role="button">Exportar a pdf</a>
          </div>
        </div>
      </div>
    </div>

  </div>
{{--Fin Listado--}}

@stop
{{--Fin Control de Acceso--}}

@section('after-scripts-end')
  
@stop
