{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-envelope"></i> Monitoreo</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-envelope"></i> Mensajes informativos</a></li>
        <li class="breadcrumb-item active">Participante</li>
        <li class="breadcrumb-item active">Monitoreo</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Filtros
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          {!! Form::open(['route'=>'monitoreo.mensajes.participante','method'=>'GET', 'class' => 'form-horizontal', 'role' =>'form']) !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                {!!Form::label('emisor', 'Participante emisor', array('class' => 'col-sm-4 control-label'))!!}
                  <div class="col-sm-8">
                    {!! Form::select('emisor',$bancos,$emisor,['id'=>'codigo','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                 {!!Form::label('receptor', 'Participante receptor', array('class' => 'col-sm-4 control-label'))!!}
                  <div class="col-sm-8">
                   {!! Form::select('receptor',$bancos,$receptor,['id'=>'codigo','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  {!!Form::label('Moneda', null, array('class' => 'col-sm-4 control-label'))!!}
                  <div class="col-sm-8">
                   {!! Form::select('cod_moneda',$monedas,$moneda,['id'=>'codigo_iso','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="card-footer">
          <div class="col-sm-12">
              <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Listado
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-striped">
            <thead style="background-color: #c1e7fc">
                <th><center>Participante emisor</center></th>
                <th><center>Participante receptor</center></th>
                <th><center>Fecha de intercambio</center></th>
                <th><center>Tipo de mensaje</center></th>
                <th><center>Moneda</center></th>
            </thead>
            <tbody>
               @forelse ($mensajes as $mensaje)
                  <tr>
                    <td><center>{{ $mensaje->codigo_participante_emisor }}</center></td>
                    <td><center>{{ $mensaje->codigo_participante_receptor == 'NCCE' ? '*' : $mensaje->codigo_participante_receptor }}</center></td>
                    <td><center>{{ $mensaje->fecha_intercambio->format('d/m/Y h:i:s') }}</center></td>
                    <td><center>{{ $mensaje->tipo_mensaje }}</center></td>
                    <td><center>{{ $mensaje->codigo_iso_moneda }}</center></td>
                  </tr>
                  @empty
                    <tr>
                      <td colspan="5">No se han recibido mensajes.</td>
                    </tr>
                  @endforelse
            </tbody>
            </table>  
              <div class="col-sm-12">
                <div class="pull-right">
                @if($mensajes->count() > 15)
                  {{ $mensajes->links() }}
                @endif
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- Fin de la seccion Listado --}}
{{-- Fin Operaciones Conciliadas --}}
@stop

@section('after-scripts-end')

@stop
