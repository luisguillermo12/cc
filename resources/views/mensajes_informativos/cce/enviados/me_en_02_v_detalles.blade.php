{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Ing . Luis valles  --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:  Ing . Luis valles    --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-envelope"></i> Detalles</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('MensajesInformativos/CCE/Enviados')}}"><i class="fa fa fa-envelope"></i> Mensajes informativos</a></li>
        <li class="breadcrumb-item active"><a href="{{url('MensajesInformativos/CCE/Enviados')}}">CCE</a></li>
        <li class="breadcrumb-item active"><a href="{{url('MensajesInformativos/CCE/Enviados')}}">Enviados</a></li>
        <li class="breadcrumb-item active"><a href="#">Detalles</a></li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')

 <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Detalles Mensaje
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
           <div class="mailbox-read-info">
                <h5><strong>Mensaje Para IBP</strong></h5><br>
                <p> <strong>De: </strong> {{ $mensaje->codigo_participante_emisor }}  </p>
                <p> <strong>Para:</strong> {{ $mensaje->codigo_participante_receptor != 'NCCE' ? $mensaje->receptor->nombre_completo : 'Todos los participantes.' }} </p>
                <p> <span> <strong>Moneda:</strong> {{ $mensaje->moneda->codigo_iso }} . {{ $mensaje->moneda->nombre }} &nbsp; </span>
                <p><span><strong>Número de archivo : </strong>  {{ $mensaje->numero_archivo }} &nbsp; </span></p> 
                <p><span>  <strong> Tipo de archivo : </strong>  {{ $mensaje->tipo_mensaje }}   </span>
                </p> <span>  <strong> Estatus : </strong> {{ $mensaje->estatus }} </span> </p>
                <p>  <span class="mailbox-read-time pull-right">{{ $mensaje->fecha_intercambio->format('d/m/Y h:i:s') }}</span></p>
               <p> <strong> Enviado por : </strong>   {{ $user->name }} </p> 
              </div>
                            <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <br><br>
                <p>{{ $mensaje->mensaje }}</p>
               <br>
              </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- Fin de la seccion Listado --}}
{{-- Fin Operaciones Conciliadas --}}
@stop

@section('after-scripts-end')
 
@stop
