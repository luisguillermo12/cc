{{-- @Nombre del programa: --}}
{{-- @Funcion: Enviar msj CCE --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 04/12/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/12/2018 --}}
{{-- @Modificado por:  Ericcor  --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-envelope"></i> Nuevo mensaje</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-envelope"></i> Mensajes informativos</a></li>
        <li class="breadcrumb-item active">CCE</li>
        <li class="breadcrumb-item active">Nuevo mensaje</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Nuevo mensaje
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
           {!! Form::open(['id'=>'form-create']) !!}
              <div class="form-group">
                {!!  Form::label('de', 'De') !!}
                {!!  Form::text('para', 'Cámara de Compensación Electrónica (NCCE)', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
              </div>
              <div class="form-group">
                {!!  Form::label('para', 'Para') !!}
                {!! Form::select('para', $bancos, null, ['class' => 'form-control']) !!}
              </div>
              <div class="form-group">
                {!!  Form::label('mensaje', 'Mensaje') !!}
                {!! Form::textarea('mensaje', null, [
                  'class' => 'form-control', 'maxlength' => '500', 'required' => 'required']) !!}
              </div>
              <div class="col-sm-12">Enviado por: {{ auth()->user()->name }}</div>
              <div class="form-group pull-right">
                {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!} &nbsp;&nbsp;
                {!! Form::reset('Reiniciar', ['class' => 'btn btn-primary']) !!}
              </div>
            {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop

@section('scripts-end')

@stop
