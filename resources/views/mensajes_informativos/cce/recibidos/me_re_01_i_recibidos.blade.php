{{-- @Nombre del programa: Vista Principal mo->liquidacion->operaciones->aceptadas--}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 26/06/2018 --}}
{{-- @Modificado por: Deivi Peña   --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-envelope"></i> Recibidos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-envelope"></i> Mensajes informativos</a></li>
        <li class="breadcrumb-item active">CCE</li>
        <li class="breadcrumb-item active">Recibidos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Filtros
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          {!! Form::open(['route'=>'mensajesrecibidos','method'=>'GET', 'class' => 'form-horizontal', 'role' =>'form']) !!}
            <div class="row">
              <div class="col-md-8">
                <div class="form-inline">
                  {!!Form::label('Participante emisor', 'Participante emisor', array('class' => 'col-sm-2 control-label'))!!}
                  <div class="col-sm-8">
                   {!! Form::select('cod_banco',$bancos,$banco,['id'=>'codigo','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-inline">
                  {!!Form::label('Moneda', null, array('class' => 'col-sm-4 control-label'))!!}
                  <div class="col-sm-8">
                    {!! Form::select('cod_moneda',$monedas,$moneda,['id'=>'codigo_iso','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="card-footer">
          <div class="col-sm-12">
              <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Listado
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-striped table-sm">
            <thead style="background-color: #c1e7fc">
              <th><center>Participante emisor</center></th>
              <th><center>Participante receptor</center></th>
              <th><center>Fecha de intercambio</center></th>
              <th><center>Tipo de mensaje</center></th>
              <th><center>Moneda</center></th>
               <th><center>Acciones </center></th>
            </thead>
            <tbody>
              @forelse ($mensajes as $mensaje)
                  <tr>
                    <td><center>{{ $mensaje->codigo_participante_emisor }}</center></td>
                    <td><center>{{ $mensaje->codigo_participante_receptor }}</center></td>
                    <td><center>{{ $mensaje->fecha_intercambio->format('d/m/Y h:i:s') }}</center></td>
                    <td><center>{{ $mensaje->tipo_mensaje }}</center></td>
                    <td><center>{{ $mensaje->codigo_iso_moneda }}</center></td>
                     <td><center>
                      <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn accion btn-info accion" href="{{route('mensajesrecibidosdetalle',$mensaje->id)}}"><i class="fa fa-eye"></i></a>
                    </center></td>
                  </tr>
                  @empty
                    <tr>
                      <td colspan="6">No se han enviado mensajes.</td>
                    </tr>
                  @endforelse
            </tbody>
            </table>  
            <br>
              <div class="col-sm-12">
                <div class="pull-right">
                  {{ $mensajes->links() }}
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
{{--Fin--}}

@section('after-scripts-end')

@stop
