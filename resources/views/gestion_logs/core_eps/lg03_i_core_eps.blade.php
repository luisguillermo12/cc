@extends ('layouts.master')
@section('page-header')
  <h1><i class="fa fa-dashboard"></i> Ejecución</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Gestión de logs</a></li>
    <li class="active"> Core EPS</li>
    <li class="active"> Ejecución</li>
  </ol>
@endsection

@section('content')
  <!-- row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-header with-border">
        </div>
          <div class="text-right">
            @if (!empty($contenido))
              <a class="btn btn-success" href="{{ url('GestionLogs/CoreEPS/Ejecucion/Descargar') }}" >Descargar log</a>
            @endif
          </div>
          <div class="table-responsive">
            <table id="CoreEps-table" class="table table-striped">
              <thead class="thead-default">
                <tr>
                  <th><center>Log de pentaho</center></th>
                </tr>
              </thead>
              @forelse ($contenido as $cont)
                <tr>
                  <td>{{$cont}}</td>
                </tr>
              @empty
                <tr>
                  <td>No se encuentran registros para esta consulta.</td>
                </tr>
              @endforelse
            </table>
          </div><!--table-responsive-->
        </div>
      </div>
    </div>
  </div>
@stop

@section('after-scripts-end')
  @include('includes.partials.log.CoreEPS.scripts_core_eps')
@stop
