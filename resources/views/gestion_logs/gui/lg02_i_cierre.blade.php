{{-- @Nombre del programa: Vista Principal del Log de Inicio --}}
{{-- @Funcion: --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 06/06/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
  <h1><i class="fa fa-dashboard"></i> Cierre</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Gestión de logs</a></li>
    <li class="active"> GUI</li>
    <li class="active"> Cierre</li>
  </ol>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <!--codigo gui MOD-MONI-1.3-------->
          <input type="hidden" name="codigo_gui" value="MOD-MONI-1.3.1" id="codigo_gui">
          <h3 class="box-title">Listado</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div><!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          <div class="text-right">
            @if (isset($logs[0]))
              <a class="btn btn-success" href="{{ url('GestionLogs/GUI/Cierre/Descargar') }}" >Descargar log</a>
            @endif
          </div>
          <div><br></div>
          <div class="table-responsive">
            <table id="users-table" class="table table-hover table-striped">
              <thead class="thead-default">
                <tr>
                  <th>Nivel</th>
                  <th>Hora</th>
                  <th>Descripción</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($logs as $log)
                  <tr>
                    <td nowrap>
                      <label class="label label-{{ $etiquetas[$log->nivel] }}">{{ $log->nivel }}</label>
                    </td>
                    <td nowrap>{{ $log->fecha }}</td>
                    <td>{{ $log->contenido }}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="3">No se encuentran registros para esta consulta.</td>
                  </tr>
                @endforelse
              </tbody>
            </table>
            <div class="col-sm-12 text-right">
              {!! $logs->links() !!}
            </div>
          </div><!--table-responsive-->
        </div>
      </div>
    </div>
  </div>
@stop
{{--Fin--}}

@section('after-scripts-end')

@stop
