{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{-- Inicio Monitoreo de Procesos --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Monitoreo de procesos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/MonitoreoProcesos') }}"><i class="fa fa-cog"></i> Ejecución administrativa</a></li>
        <li class="breadcrumb-item active">Monitoreo de procesos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-warning card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-1.4-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
        <h3 class="card-title">Base de Datos</h3>
        <div class="card-tools">
          @if (auth()->user()->hasPermission(['20-01-XX-XX-02']))
            <button id="compilar" class="btn btn-info btn-sm"  onclick="compilarBD()">Compilar objetos de BD.</button>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="table-bd" class="table table-striped table-bordered table-hover table-sm">
          <thead class="theadtable">
            <tr>
              <th>Listado de objetos de BD inválidos</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($objetos_invalidos as $objeto)
            <tr>
              <td>{{ $objeto->invalido }}</td>
            </tr>
            @empty
            <tr><td>No existen objetos de BD inválidos.</td></tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <div class="card card-primary card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-1.4-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
        <h3 class="card-title">Espacio del disco</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
      </div>
      <div class="card-body">
        <div class="card-content table-responsive">
          <table id="disck-space-table" class="table table-bordered table-hover table-sm">
            <thead class="theadtable">
              <tr>
                <th><center>Volumen</center></th>
                <th><center>Capacidad</center></th>
                <th><center>Disponible</center></th>
                <th><center>Ocupado</center></th>
              </tr>
            </thead>
            <tbody>
              <tr id="C">
                <td id="unidad"></td>
                <td id="total"></td>
                <td id="libre"></td>
                <td id="ocupado"></td>
              </tr>
              <tr id="X">
                <td id="unidadX"></td>
                <td id="totalX"></td>
                <td id="libreX"></td>
                <td id="ocupadoX"></td>
              </tr>
              <tr id="Y">
                <td id="unidadY"></td>
                <td id="totalY"></td>
                <td id="libreY"></td>
                <td id="ocupadoY"></td>
              </tr>
              <tr id="F">
                <td id="unidadF"></td>
                <td id="totalF"></td>
                <td id="libreF"></td>
                <td id="ocupadoF"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  {{-- Inicio Listado de Procesos --}}

  <div class="col-md-12">
    <div class="card card-success card-outline" id="app">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-1.4-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
        <h3 class="card-title">Listado de procesos ETL</h3>
          </button>
          <div class="card-tools" style="right: 11rem;">
            <div class="input-group input-group-sm">
              <input type="text" class="form-control" v-model="busqueda" v-on:keydown="ajax" placeholder="Filtro">
              <div class="input-group-append">
                <button class="input-group-text" v-on:click="ajax" style="cursor: pointer;"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
          <div class="card-tools">
            <button type="button" class="btn btn-default btn-sm" v-on:click="ajax">
              <i class='fa fa-repeat'></i>&nbsp;&nbsp;&nbsp;Actualizar
            </button>
            <button type="button" class="btn btn-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
      </div>
      <div class="card-body">
        <div class="card-content table-responsive">
              <table class="table table-striped table-hover table-sm">
                <input id="orden" type="hidden" name="orden" value="ASC">
                <input id="columna" type="hidden" name="columna" value="nombre_job">
                <thead class="theadtable">
                  <tr>
                    <th><center>Código</center></th>
                    <th style="cursor: pointer;" onclick="orden('nombre_job');" v-on:click="ajax"><center>Proceso  &nbsp; <i id="3nombre_job" class="fa fa-sort-amount-asc"></i><i id="4nombre_job" class="fa"></i></center></th>
                    <th style="cursor: pointer;" onclick="orden('clasificacion_job');" v-on:click="ajax"><center>Clasificación  &nbsp;<i id="5clasificacion_job" class="fa fa-long-arrow-down"></i><i id="6clasificacion_job" class="fa fa-long-arrow-up"></i></center></th>
                    <th><center>Estado</center></th>
                    <th><center>Acción</center></th>
                  </tr>
                </thead>
                <tbody>
                 <tr v-for="(job) in items">
                   <td>
                    <center>
                      @{{job[0]}}
                    </center>
                   </td>
                   <td>
                      <p>@{{job[1]}}</p>
                    </td>
                    <td>
                      <p>@{{job[2]}}</p>
                    </td>
                    <td>
                      <center v-html="job[3]">

                      </center>
                    </td>
                    <td>
                      <center v-html="job[4]">

                      </center>
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  {{-- Fin Listado de Procesos --}}
@stop
{{-- Fin Monitoreo de Procesos --}}

@section('after-scripts-end')
  @include('includes.scripts.ejecucion_administrativa.monitoreo_procesos.scripts_ea_mp_01_i_monitoreo')
@stop
