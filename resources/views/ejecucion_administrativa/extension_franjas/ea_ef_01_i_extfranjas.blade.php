{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{-- Inicio Extension e Franjas --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Modificación de franjas</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/ModificacionFranja') }}"><i class="fa fa-cog"></i> Ejecución administrativa</a></li>
        <li class="breadcrumb-item active">Modificación de franjas</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-CONF-1.4-->
          <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
          <h3 class="card-title">Listado</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="BancosActivos-table" class="table table-striped table-bordered table-hover table-sm">
              <thead class="theadtable">
                <tr>
                  <th><center>Producto</center></th>
                  <th nowrap><center>Fecha de inicio</center></th>
                  <th><center>Hora de inicio</center></th>
                  <th><center>Fecha de fin</center></th>
                  <th><center>Hora de fin</center></th>
                  <th><center>Liquidación</center></th>
                  <th><center>Número de franja</center></th>
                  <th><center>Acción</center></th>
                </tr>
              </thead>
              <?php $cont = 1; $cont_general = 0; ?>
              @foreach ($RecepcionArchivos as $recepcionarchivos)
                @if ($cont==1 && $cont_general==1)
                  <tr><td colspan="9" style="background-color: #c2e7fc;"></td></tr>
                @endif
                <tr>
                  @if ($cont==1)
                    <td rowspan="{{ $recepcionarchivos->p_num_franja }}" nowrap style="vertical-align: middle;">{{ $recepcionarchivos->nombre }}</td>
                    <?php $cont_general = 1 ?>
                  @endif
                  <td><center>{{ $recepcionarchivos->fecha_hora_inicio->format('Y-m-d') }}<center></td>
                  <td><center>{{ $recepcionarchivos->fecha_hora_inicio->format('H:i:s') }}<center></td>
                  <td><center>{{ $recepcionarchivos->fecha_hora_fin->format('Y-m-d') }}<center></td>
                  <td><center>{{ $recepcionarchivos->fecha_hora_fin->format('H:i:s') }}<center></td>

                    @if ($recepcionarchivos->num_franja == 1)
                    <td rowspan="2" style="vertical-align: middle;"><center>{{ $recepcionarchivos->num_liquidacion }}</center></td>
                    @else
                    @endif

                  <td><center>{{ $recepcionarchivos->num_franja}}<center></td>
                  <td><center>
                    @if (auth()->user()->hasPermission(['20-03-XX-XX-03']))
                      @if ($fecha_actual->diffInMinutes($recepcionarchivos->fecha_hora_fin,false) > $min_modificar)
                        <a data-toggle="tooltip" data-placement="top" title="Editar franja" class="btn-sm btn-primary" href="{{ route('ModificacionFranja.edit',$recepcionarchivos->id) }}"><i class="fa fa-pencil"></i></a>
                      @endif
                    @endif
                  </center></td>
                </tr>
                  @if ($cont==$recepcionarchivos->p_num_franja)
                    <?php $cont = 1 ?>
                  @else
                    <?php $cont++ ?>
                  @endif
              @endforeach
              <tr><td colspan="9" style="background-color: #c2e7fc;"></td></tr>
            </table>
          </div><!--table-responsive-->
        </div>
      </div>
    </div>
    {{-- inicio seccion de graficas --}}

      @include('ejecucion_administrativa.extension_franjas.ea_ef_01_i_extgraficas')

    {{-- fin seccion de graficas --}}
  </div>
  {{-- Fin Extension e Franjas --}}
@stop

@section('after-scripts-end')
  @include('includes.scripts.graficos_general.scripts_graficos_general')
  @include('includes.scripts.ejecucion_administrativa.extension_franjas.scripts_ea_ef_01_i_extfranjas')
@stop
