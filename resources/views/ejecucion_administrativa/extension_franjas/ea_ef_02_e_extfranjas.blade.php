{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{-- Inicio Recepción de Archivos --}}
@section ('title', 'Electronic Payment Suite (EPS)' . ' | ' . 'Editar')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Modificación de franjas</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/ModificacionFranja') }}"><i class="fa fa-cog"></i> Ejecución administrativa</a></li>
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/ModificacionFranja') }}"><i class="fa fa-cog"></i> Modificación de Franjas</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        {!!Form::open([
          'method'=>'PUT',
          'id'=>'form-edit',
          'class'=>'form-horizontal',
          'autocomplete'=>'off',
          'route'=>['ModificacionFranja.update',$HoraRecepcionArchivos->id]
          ]) !!}
          <div class="card-header with-border">
            <h3 class="card-title">
              <!--codigo gui MOD-EJEC-1.3---->
            <input type="hidden" name="codigo_gui" value="MOD-EJEC-1.3.2" id="codigo_gui">
              Editar - {!!$HoraRecepcionArchivos->nombre!!}
            </h3>
          </div>
          <div class="card-body" id="app">
            @include('includes.messages')

            <div class="callout callout-info col-sm-4">
              <h4><i class="fa fa-info-circle"></i> Nota:</h4>
              <p>Los campos marcados con un asterisco (*) son obligatorios.</p>
            </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
            <br>
            <div class="row col-sm-12 text-center">
              <div class="col-sm-2">
                <div class="form-group">
                  {!! Form::label('Día Inicio') !!}
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  {!!Form::label('Hora Inicio')!!}
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  {!!Form::label('Día Fin')!!}
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  {!!Form::label('Hora Fin(*)')!!}
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  {!!Form::label('Liquidación')!!}
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  {!!Form::label('Franja')!!}
                </div>
              </div>
            </div>
            <div class="row col-sm-12">
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  <span class="badge badge-primary">{!! $HoraRecepcionArchivos->fecha_hora_inicio->format('d/m/Y')!!}</span>
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  <span class="badge badge-primary">{!! $HoraRecepcionArchivos->fecha_hora_inicio->format('H:i:s')!!}</span>
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group col-sm-12" style="margin: 0 auto;">
                  {!!Form::select('fecha', $fechas, $HoraRecepcionArchivos->fecha_hora_fin->format('Ymd'),['id'=>'dia_fin', 'class'=>'form-control input-sm','required'=>'required']) !!}
                </div>
              </div>

              <div class="col-sm-2 text-center bootstrap-timepicker">
                <div class="form-group text-center">
                  <div class="input-group">
                    {!!Form::text('hora_fin',$HoraRecepcionArchivos->fecha_hora_fin->format('H:i:s'),['id'=>'hora_fin','class'=>'form-control timepicker input-sm hora_fin fechahora_fin','placeholder'=>'Hora Fin','maxlength'=>8,'required'=>'required', 'v-on:change'=>'handleGraph'])!!}
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-2 text-center">
                <div class="form-group">
                  <span class="badge badge-primary">{!!$HoraRecepcionArchivos->num_liquidacion!!}</span>
                  <input type="hidden" value="{!!$HoraRecepcionArchivos->num_liquidacion!!}" class="num_liquidacion">
                </div>
              </div>
              <div class="col-sm-2 text-center">
                <div class="form-group">
                  <span class="badge badge-primary">{!!$HoraRecepcionArchivos->num_franja!!}</span>
                  <input type="hidden" value="{!!$HoraRecepcionArchivos->num_franja!!}" class="num_franja">
                </div>
              </div>
            </div>

            <hr class="col-sm-12">

            <div class="col-sm-12">
              <div class="form-group col-sm-12">
                {!!Form::label('cod_banco', 'Participante', array('class' => 'col-sm-2 control-label'))!!}
                <div class="col-sm-6">
                  {!!Form::select('cod_banco', $bancos, null, ['class'=>'form-control', 'required'=>'required']) !!}
                </div>
              </div>
              <div class="form-group col-sm-12">
                {!!Form::label('Descripción', null, array('class' => 'col-sm-2 control-label'))!!}
                <div class="col-sm-10">
                  {!! Form::textarea('descripcion', null, ['class' => 'form-control col-sm-12', 'maxlength' => '500', 'rows' => '6']) !!}
                </div>
              </div>
            </div>

            </div>

            @if ($fecha_actual->diffInMinutes($HoraRecepcionArchivos->fecha_hora_fin,false) > $min_modificar)
            <div class="card-footer">
              <div class="card-tools pull-right">
                  {!!form::submit('Actualizar',['class'=>'btn btn-success btn-sm m-t-10','title'=>'Actualizar'])!!}
              </div>
            </div>
            @endif

          </div>
          {!! Form::close() !!}
        </div>
        {{-- inicio seccion de graficas --}}

          @include('ejecucion_administrativa.extension_franjas.ea_ef_02_e_extgraficas')

        {{-- fin seccion de graficas --}}
      </div>
    </div>
    {{-- Fin Recepción de Archivos --}}
  @stop

  @section('after-scripts-end')
    @include('includes.scripts.graficos_general.scripts_graficos_general')
    @include('includes.scripts.ejecucion_administrativa.extension_franjas.scripts_ea_ef_02_e_extfranjas')
  @stop
