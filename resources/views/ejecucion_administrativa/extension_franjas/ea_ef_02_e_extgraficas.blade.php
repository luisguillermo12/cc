{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

<div class="col-md-12">
  <div class="card card-primary card-outline">
    <div class="card-header with-border">
      <!--codigo gui MOD-CONF-10.2---->
      <input type="hidden" name="codigo_gui" value="MOD-CONF-10.2.1" id="codigo_gui">
      <h3 class="card-title">Visualización</h3>
    </div>
    <div class="card-body">
      <?php for ($i=0; $i < count($operaciones); $i++) {?>
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">
              <div class="card-content table-responsive">
                <div id="container<?= $i ?>" style="width:98.5%"></div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
