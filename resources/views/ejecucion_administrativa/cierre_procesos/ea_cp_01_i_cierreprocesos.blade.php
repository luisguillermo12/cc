{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{-- Inicio Cierre de Procesos --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Cierre de procesos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/CierreProcesos') }}"><i class="fa fa-cog"></i> Ejecución administrativa</a></li>
        <li class="breadcrumb-item active">Cierre de procesos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-warning card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-1.4-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
        <h3 class="card-title">Cierre de compesanción</h3>
        <div class="card-tools pull-right">
        </div>
      </div>
      <div class="card-body">
        @if ($sistema_cerrado)
        <div class="col-sm-12">
          <div class="col-sm-8 offset-sm-2">
            <div class="alert alert-success text-center"><h3>El sistema ha sido cerrado satisfactoriamente.</h3></div>
          </div>
        </div>
        @endif
        <table class="table table-striped table-bordered table-hover table-sm">
          <thead class="theadtable">
            <tr>
              <th><center>Proceso</center></th>
              <th><center>Ejecutado</center></th>
              <th><center>Acción</center></th>
            </tr>
          </thead>
          <tbody>

            @for ($x=25; $x <= 28; $x++)
            <tr>
              <td><h4>{{ $procesos[$x]->nombre }}</h4></td>
              <td class="text-center">{!! $procesos[$x]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
              <td></td>
            </tr>
            @endfor

            <tr>
              <td><h4>{{ $procesos[24]->nombre }}</h4></td>
              <td id="etl-estatus" class="text-center">{!! $procesos[24]->valor != 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
              <td class="text-center">
                @if (auth()->user()->hasPermission(['20-04-XX-XX-06']))
                  @if ($procesos[24]->valor == 1 && $archivos_enviados && $cancelar_jobs)
                    <a id="etl-boton" class="btn-sm btn-primary" href="#" onclick="detenerCMD();">Ejecutar</a>
                  @endif
                @endif
              </td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card card-info card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-1.4-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
        <h3 class="card-title">Mantenimiento</h3>
        <div class="card-tools pull-right">
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped table-bordered table-hover table-sm">
          <thead class="theadtable">
            <tr>
              <th><center>Proceso</center></th>
              <th><center>Ejecutado</center></th>
              <th><center>Acción</center></th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <td><h4>{{ $procesos[29]->nombre }}</h4></td>
              <td class="text-center">{!! $procesos[29]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
              <td class="text-center">
                @if (auth()->user()->hasPermission(['20-04-XX-XX-02']))
                  @if ($procesos[24]->valor == 0 && $procesos[25]->valor == 1 && $procesos[26]->valor == 1 && $procesos[27]->valor == 1 && $procesos[28]->valor == 1 && $procesos[29]->valor != 1)
                    <a class="btn-sm btn-primary" href="{{ url('EjecucionAdministrativa/CierreProcesos/GenerarHistorico') }}">Ejecutar</a>
                  @endif
                @endif
              </td>
            </tr>

            <tr>
              <td><h4>{{ $procesos[31]->nombre }}</h4></td>
              <td class="text-center">{!! $procesos[31]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
              <td class="text-center">
                @if (auth()->user()->hasPermission(['20-04-XX-XX-03']))
                  @if ($procesos[24]->valor == 0 && $procesos[25]->valor == 1 && $procesos[26]->valor == 1 && $procesos[27]->valor == 1 && $procesos[28]->valor == 1 && $procesos[29]->valor == 1 && $procesos[31]->valor != 1)
                    <a class="btn-sm btn-primary" href="{{ url('EjecucionAdministrativa/CierreProcesos/InicioBD') }}">Ejecutar</a>
                  @endif
                @endif
              </td>
            </tr>

            <tr>
              <td><h4>{{ $procesos[32]->nombre }}</h4></td>
              <td class="text-center">{!! $procesos[32]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
              <td class="text-center">
                @if (auth()->user()->hasPermission(['20-04-XX-XX-04']))
                  @if ($procesos[24]->valor == 0 && $procesos[25]->valor == 1 && $procesos[26]->valor == 1 && $procesos[27]->valor == 1 && $procesos[28]->valor == 1 && $procesos[29]->valor == 1 && $procesos[31]->valor == 1  && $procesos[32]->valor != 1)
                    <a class="btn-sm btn-primary" href="{{ url('EjecucionAdministrativa/CierreProcesos/Comprimir') }}">Ejecutar</a>
                  @endif
                @endif
              </td>
            </tr>

            <tr>
              <td><h4>{{ $procesos[33]->nombre }}</h4></td>
              <td class="text-center">{!! $procesos[33]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
              <td class="text-center">
                @if (auth()->user()->hasPermission(['20-04-XX-XX-05']))
                  @if ($procesos[24]->valor == 0 && $procesos[25]->valor == 1 && $procesos[26]->valor == 1 && $procesos[27]->valor == 1 && $procesos[28]->valor == 1 && $procesos[29]->valor == 1 && $procesos[31]->valor == 1 && $procesos[32]->valor == 1 && $procesos[33]->valor != 1)
                    <a class="btn-sm btn-primary" href="{{ url('EjecucionAdministrativa/CierreProcesos/Limpiar') }}">Ejecutar</a>
                  @endif
                @endif
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
  {{-- Fin Cierre de Procesos --}}
@stop

@section('after-scripts-end')
<script type="text/javascript">
  function detenerETL() {
      $('#etl-estatus').html('<span class="col-sm-12 badge badge-warning" style="font-size: 18px">Deteniendo procesos</span>');
      $.ajax({
        url: "{{ url('EjecucionAdministrativa/CierreProcesos/Detener') }}",
        success: function(result) {
          //console.log(result);
          location.reload();
        }
      })
  }

  function detenerCMD() {
      console.log('deteniendo CMD');

      var delay = "{{ $tiempo_espera }}";
      $('#etl-estatus').html('<span class="col-sm-12 badge badge-warning" style="font-size: 18px">Deteniendo recepción de archivos</span>');
      $('#etl-boton').addClass('disabled');

      $.ajax({
        url: "{{ url('EjecucionAdministrativa/CierreProcesos/DetenerCMD') }}",
        success: function(result) {
          //console.log(result);

          // La variable se encuentra en minutos y se debe convertir a milisegundos.
          setTimeout(detenerETL, delay * 60 * 1000);
          //console.log('Esperando ' + delay * 60 * 1000 + ' milisegundos.')
        }
      });
  }
</script>

@stop
