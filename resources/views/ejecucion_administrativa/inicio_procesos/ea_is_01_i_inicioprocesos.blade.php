{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{-- Inicio Cierre de Procesos --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Inicio de procesos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/InicioProcesos') }}"><i class="fa fa-cog"></i> Ejecución administrativa</a></li>
        <li class="breadcrumb-item active">Inicio de procesos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">

    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-CONF-1.4-->
          <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
          <h3 class="card-title">Configuración</h3>
        </div>
        <div class="card-body">
          <div class="col-sm-12">
            <div class="col-sm-8 col-md-8 offset-md-2">
              @if ($procesos[24]->valor != 0)
              <div class="col-sm-12">
                <div class="alert alert-success text-center">
                  El sistema ha sido iniciado satisfactoriamente.
                </div>
              </div>
              @elseif ($proximo->diffInDays($actual) > 0)
              <div class="col-sm-12">
                <div class="alert alert-danger text-center">
                  El próximo día de compensación es distinto al próximo día hábil para la fecha del sistema.
                </div>
              </div>
              @elseif ($procesos[8]->valor == 0)
              <div class="col-sm-12">
                <div class="alert alert-danger text-center">
                  El sistema no puede iniciar porque existen objetos de BD inválidos.
                </div>
              </div>
              @endif
              <table width='100%' class="table table-striped table-bordered table-hover table-sm">
                <tbody>
                  <tr>
                    <td class="text-center" colspan="3"><h3>Inicio de período de compensación</h3></td>
                  </tr>
                  <tr>
                    <td><h4>Último día compensado / DayOff ejecutado: </h4></td>
                    <td class="text-center"><h4>{{ $ultimo->fecha->format('d-m-Y') }}</h4></td>
                    <td class="text-center"><h4>{{ $ultimo->estatus }}</h4></td>
                  </tr>
                  <tr>
                    <td><h4>Próximo día de compensación (D): </h4></td>
                    <td class="text-center">
                      @if ($proximo->diffInDays($actual) > 0)
                      <a class="col-sm-12  badge badge-danger" title="El próximo día de compensación es distinto al próximo día hábil para la fecha del sistema.">
                        <h4>
                          {{ $proximo->format('d-m-Y') }}
                        </h4>
                      </a>
                      @else
                      <h4>
                        {{ $proximo->format('d-m-Y') }}
                      </h4>
                      @endif
                    </td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
              <div class="col-sm-12 text-center">
                @if (auth()->user()->hasPermission(['20-02-XX-XX-02']))
                  @if ($procesos[23]->valor == 0 && $procesos[8]->valor ==1 )
                  <a class="btn-sm btn-primary @if ($proximo->diffInDays($actual) > 0) disabled @endif" href="{{ url('EjecucionAdministrativa/InicioProcesos/IniciarBD') }}">Aceptar</a>
                  @endif
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-CONF-1.4-->
          <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools pull-right">
          </div>
        </div>
        <div class="card-body">
          <div class="card-content table-responsive">
            <table width='100%' class="table table-striped table-bordered table-hover table-sm">
              <thead class="theadtable">
                <tr>
                  <th><center>Proceso</center></th>
                  <th><center>Ejecutado</center></th>
                  <th><center>Acción</center></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><h4>{{ $procesos[24]->nombre }}</h4></td>
                  <td id="estatusJobs" class="text-center">{!! $procesos[24]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
                  <td class="text-center">
                    @if (auth()->user()->hasPermission(['20-02-XX-XX-03']))
                      @if ($procesos[23]->valor == 1 && $procesos[24]->valor == 0 && $procesos[8]->valor == 1)
                        <a id="iniciarJobs" class="btn-sm btn-primary disabled" href="{{ url('EjecucionAdministrativa/InicioProcesos/IniciarJobs') }}">Ejecutar</a>
                      @endif
                    @endif
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- Fin Cierre de Procesos --}}
@stop

@section('after-scripts-end')
  @include('includes.scripts.ejecucion_administrativa.servidor_carte.scripts_servidor_carte')
@stop
