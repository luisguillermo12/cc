{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')
{{-- Inicio Monitoreo de Procesos --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Stand by</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('EjecucionAdministrativa/StandBy') }}"><i class="fa fa-cog"></i> Ejecución administrativa</a></li>
        <li class="breadcrumb-item active">Stand by</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  {{-- Inicio Listado de Procesos --}}
  <div class="col-md-12">
    <div class="card card-warning card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-1.4-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.4.1" id="codigo_gui">
        <h3 class="card-title">Listado de procesos</h3>
      </div>
      <div class="card-body">

        <table class="table table-striped table-bordered table-hover table-sm">
          <thead class="theadtable">
            <tr>
              <th><center>Proceso</center></th>
              <th><center>Estado</center></th>
              <th><center>Acción</center></th>
            </tr>
          </thead>
          <tbody>


              <tr>
                <td><center><h4> Ejecutar Stand By / DetenerJOBS.bat</h4></center></td>
                <td id="etl-estatus" class="text-center">{!! $procesos[24]->valor != 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
                <td class="text-center">
                  @if (auth()->user()->hasPermission(['20-04-XX-XX-06']))
                    @if ($procesos[24]->valor == 1)
                      <a id="etl-boton" class="btn-sm btn-primary" href="#" onclick="detenerCMD();">Ejecutar</a>
                    @endif
                  @endif
                </td>
              </tr>



               <tr>
                  <td><center><h4>Levantar Stand By / IniciarJOBS.bat</h4></center></td>
                  <td id="estatusJobs" class="text-center">{!! $procesos[24]->valor == 1 ? '<span class="col-sm-12 badge badge-success" style="font-size: 18px">Si</span>' : '<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>' !!}</td>
                  <td class="text-center">
                    @if (auth()->user()->hasPermission(['20-02-XX-XX-03']))
                      @if ($procesos[23]->valor == 1 && $procesos[24]->valor == 2)
                        <a id="iniciarJobs" class="btn-sm btn-primary disabled" href="{{ url('EjecucionAdministrativa/Standby/IniciarJobs') }}">Ejecutar</a>
                      @endif
                    @endif
                  </td>
                </tr>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- Fin Listado de Procesos --}}
@stop
{{-- Fin Monitoreo de Procesos --}}

@section('after-scripts-end')
  @include('includes.scripts.ejecucion_administrativa.servidor_carte.scripts_servidor_carte')
<script type="text/javascript">
  function detenerETL() {
      $('#etl-estatus').html('<span class="col-sm-12 badge badge-warning" style="font-size: 18px">Deteniendo procesos</span>');
      $.ajax({
        url: "{{ url('EjecucionAdministrativa/Standby/Detener') }}",
        success: function(result) {
          //console.log(result);
          location.reload();
        }
      })
  }

  function detenerCMD() {
      console.log('deteniendo CMD');

      var delay = "{{ $tiempo_espera }}";
      $('#etl-estatus').html('<span class="col-sm-12 badge badge-warning" style="font-size: 18px">Deteniendo recepción de archivos</span>');
      $('#etl-boton').addClass('disabled');

      $.ajax({
        url: "{{ url('EjecucionAdministrativa/Standby/DetenerCMD') }}",
        success: function(result) {
          //console.log(result);

          // La variable se encuentra en minutos y se debe convertir a milisegundos.
          setTimeout(detenerETL, delay * 60 * 1000);
          console.log('Esperando ' + delay * 60 * 1000 + ' milisegundos.')
        }
      });
  }
</script>
@stop
