{{-- @Nombre del programa: Vista de Excel  Reporte de Usuarios --}}
{{-- @Funcion: Descargar el reporte de usuarios registrados en excel --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}
<!DOCTYPE html>
<html>
<head>
  <title>REPORTE ROLES</title>
  {!! Html::style('css/AdminLTE.css') !!}
  {!! Html::style('css/pdf.css') !!}
<body>
<p>Banco Central de Venezuela</p>
<p>Gerencia de Tesorería</p>
<p>Departamento Cámara de Compensación Electrónica</p>
<h4 align=center>CÁMARA DE COMPENSACIÓN - REPORTE DE ROLES</h4>
<br>
<div class=".contenido">
  <table style="width: 100%" border="0" cellspacing="0">
    <thead style="background-color: #C2E7FC;">
      <tr class="class="thead-dark"" role="alert">
          <th>Nombre</th>
          <th style="text-align: center;">Usuarios </th>
          <th style="text-align: center;">Permisos </th>
      </tr>
    </thead>
    <tbody>
      @foreach($roles as $role)
      <tr>
        <td>{{ $role->name }}</td>
        <td style="text-align: center;">{{$role->users->pluck('name')->implode(', ') }}</td>
        <td style="text-align: center;">{{ $role->todos != 0 ? 'Todos' : $role->permissions->pluck('display_name')->implode(', ') }}</td>
      </tr>
      @endforeach
    </tbody>
   </table>
</div>
</body>
</html>
