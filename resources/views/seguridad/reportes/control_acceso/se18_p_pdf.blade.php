{{-- @Nombre del programa: Vista de Excel  Reporte de Control de Acceso --}}
{{-- @Funcion:  --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 30/05/2018 --}}
{{-- @Requerimiento:  --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}
<!DOCTYPE html>
<html>
<head>
  <title>REPORTE USUARIOS</title>
  {!! Html::style('css/AdminLTE.css') !!}
  {!! Html::style('css/pdf.css') !!}
<body>
<div class="header"> </span></div>
<p>Banco Central de Venezuela</p>
<p>Departamento Cámara de Compensación Electrónica</p>
<h4 align=center>CÁMARA DE COMPENSACIÓN - REPORTE DE CONTROL DE ACCESO</h4>
<br>
<div class=".contenido">
  <table style="width: 100%" border="0" cellspacing="0">
    <thead style="background-color: #C2E7FC;">
      <tr class="class="thead-dark"" role="alert">
        <th>Usuario</th>
        <th>IP</th>
        <th>Fecha</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($logs as $log)
      <tr>
        <td>{{ $log->username }}</td>
        <td>{{ $log->ip_address }}</td>
        <td>{{ $log->created_at }}</td>
        <td>{{ $log->action }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</body>
</html>
