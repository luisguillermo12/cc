{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Límite de bajo valor</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i>Configuración</li>
        <li class="breadcrumb-item active">Cobranza</li>
        <li class="breadcrumb-item active">Alto valor</li>
        <li class="breadcrumb-item active">Límite de bajo valor</li>
      </ol>
    </div>
  </div>
</div>
@endsection


@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped table-hover table-sm">
            <thead class="thead-default">
              <tr class ="theadtable" >
                <th><center>Producto</center></th>
                <th><center>Código presentada</center></th>
                <th><center>Código devuelta</center></th>
                <th><center>Monto límite bajo valor</center></th>
                <th><center>Acción</center></th>
              </tr>
            </thead>
            @foreach ($productos as $producto => $tipo_operacion)
            <tr>
              <td nowrap>{{ $producto }}</td>
              <td><center>{{ $tipo_operacion[0]->codigo }}</center></td>
              <td><center>{{ $tipo_operacion[1]->codigo }}</center></td>
              <td><center>{{ number_format($tipo_operacion[0]->limite_bajo_valor, $moneda_activa->num_decimales, $moneda_activa->separador_decimales, $moneda_activa->separador_miles)}}</center></td>
              <td>
              @if (!Parametro::sistemaIniciado())
              <center>
                  @if (auth()->user()->hasPermission(['00-07-02-XX-02']))
                  <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-primary" href="{{route('LimiteBajoValor.edit', $tipo_operacion[0]->id)}}"><i class="fa fa-pencil"></i></a>
                  @endauth
              </center>
              @endif
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')

@stop
