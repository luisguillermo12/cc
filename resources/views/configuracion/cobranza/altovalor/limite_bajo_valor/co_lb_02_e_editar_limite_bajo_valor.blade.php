{{-- @Nombre del programa:  --}}
{{-- @Funcion: --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Límite bajo valor </h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('Configuracion/Cobranza/AltoValor/LimiteBajoValor')}}"><i class="fa fa-cog"></i>&nbsp; Configuración</a></li>
              <li class="breadcrumb-item"><a href="{{url('Configuracion/Cobranza/AltoValor/LimiteBajoValor')}}">&nbsp; Cobranza</a></li>
              <li class="breadcrumb-item"><a href="{{url('Configuracion/Cobranza/AltoValor/LimiteBajoValor')}}">&nbsp; Alto valor</a></li>
              <li class="breadcrumb-item"><a href="{{url('Configuracion/Cobranza/AltoValor/LimiteBajoValor')}}">&nbsp; Límite bajo valor</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div>
        </div>
      </div>
@endsection
@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Editar</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body">
          {{ Form::model($producto, ['route' => ['LimiteBajoValor.update', $producto], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row col-md-12">
           {!! Form::hidden('id Producto(*)', 'id producto(*)', array('class' => 'col-sm-3 control-label')) !!}
            <div class="col-sm-6">
              {!!Form::hidden('producto_id',null,['id'=>'producto_id','class'=>'form-control','placeholder'=>'Ingrese Código','maxlength'=>3])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
             {!! Form::label('Nombre del Producto(*)', 'Producto(*)', array('class' => 'col-sm-3 control-label')) !!}
            <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre_producto','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'readonly'=>'readonly'])!!}
            </div>
          </div>
          <div class="form-group row col-md-12">
             {!! Form::label('Límite Bajo Valor(*)', 'Límite bajo valor(*)', array('class' => 'col-sm-3 control-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('limite_bajo_valor',null,['id'=>'limite_bajo_valor','class'=>'form-control formato-numero','placeholder'=>'Ingrese Límite  Bajo Valor ','maxlength'=>16,'onKeyUp'=>'this.value = $.number(this.value.replace(/\./g, ""), 0, ",", ".");'])!!}
            </div>
          </div>
        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarTipoOperacion']) }}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
      </div>
    </div>
  </div>
</div> 
@stop
@section('after-scripts-end')
    {{ Html::script("js/number/jquery.number.min.js") }}
    <script type="text/javascript">
      $("#limite_bajo_valor").val($.number($("#limite_bajo_valor").val(), 0, ",", "."));
    </script>
@stop
