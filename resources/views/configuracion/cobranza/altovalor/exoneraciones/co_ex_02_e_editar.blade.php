{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Exoneración</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">  Configuración</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">Cobranza</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">Alto valor</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">Exoneración</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Editar</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body">
          {{ Form::model($subproducto, ['route' => ['Exoneracion.update', $subproducto], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          {!!Form::hidden('id',null,['id'=>'id','class'=>'form-control'])!!}
          <div class="form-group row col-md-12">
          {!! Form::label('Producto(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
             {!! Form::text('producto_id',$productos->nombre,['id'=>'producto_id','class'=>'form-control', 'readonly'=>'readonly']) !!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Código de sub producto (*)', 'Código de sub producto (*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('codigo',null,['id'=>'codigo','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código','maxlength'=>3, 'readonly'=>'readonly'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('sub producto (*)', 'Sub producto (*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'readonly'=>'readonly','onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
            </div>
          </div>

          <div class="form-group row">
            {!! Form::label('Exonerado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              <label class="radio-inline">
                  {{ Form::radio('factura', 1) }} Facturar
              </label>
              <label class="radio-inline">
                  {{ Form::radio('factura', 0) }} No facturar
              </label>
            </div>
          </div>
          
        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id' => 'actualizarSubProducto']) }}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
      </div>
    </div>
  </div>
</div> 
@stop
@section('after-scripts-end')

@stop
