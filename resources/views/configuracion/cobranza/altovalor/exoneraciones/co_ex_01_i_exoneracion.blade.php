{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor:btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')

{{--Inicio Tiempo Liquidación--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Exoneración</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i>Configuración</li>
        <li class="breadcrumb-item active">Cobranza</li>
        <li class="breadcrumb-item active">Alto valor</li>
        <li class="breadcrumb-item active">Exoneración</li>
      </ol>
    </div>
  </div>
</div>
@endsection


@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Filtros
          </h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
         {!! Form::open(['method'=>'GET', 'class' => 'form-horizontal', 'role' =>'form']) !!}
         <div class="row">
          <div class="col-md-8">
            <div class="form-inline">
              {!!Form::label('Producto', 'Producto', array('class' => 'col-sm-4 control-label'))!!}
              <div class="col-sm-2">
               {!! Form::select('producto', $productos, $producto, ['id'=>'codigo','class'=>'form-control']) !!}
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="card-footer">
      <div class="col-sm-12">
        <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header card-primary card-outline">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table table id="example1" class="table table-bordered table-striped table-hover table-sm">
              <thead class="thead-default">
                <tr class ="theadtable">
                  <th><center>Producto</center></th>
                  <th><center>Sub-producto</center></th>
                  <th><center>Código del sub-producto</center></th>
                  <th><center>Exonerado</center></th>
                  <th><center>Acción</center></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($subproductos as $subproducto)
                <tr>
                  <td nowrap>{{ $subproducto->producto }}</td>
                  <td nowrap>{{ $subproducto->nombre }}</td>
                  <td><center>{{ $subproducto->codigo }}</center></td>

                  @if($subproducto->exonerado ==1)
                  <td><center><span class="text-white badge badge-success"  style="font-size: 13px;">Si</span></center></td>
                  @else
                  <td><center><span class="text-white badge badge-danger"  style="font-size: 11px;">No</span></center></td>
                  @endif
                  <td>
                   @if (!Parametro::sistemaIniciado())
                    @if (auth()->user()->hasPermission(['00-07-02-XX-02']))
                    <center>
                      <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-primary accion" href="{{route('Exoneracion.edit',$subproducto->id)}}"><i class="fa fa-pencil"></i></a>
                    </center>
                    @endauth
                     
                    @endif
                  </td>
                </tr>
              </tbody>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('after-scripts-end')
<?php // @include('backend.includes.partials.configuracion.medios_pagos.scripts_sub_productos') -->?>
@stop
