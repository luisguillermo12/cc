{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Productos(Editar) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Recuperación de costos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">  Configuración</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">Cobranza</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}">Recuperación de costos</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Editar</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body">
          {{ Form::model($operacion, ['route' => ['RecuperacionCostos.update', $operacion], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row col-md-12">
           {!! Form::label('Nombre', 'Tipo de operacion', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('nombre', null, ['id'=>'nombre','class'=>'form-control', 'disabled' => 'disabled'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Código', 'Código tipo de operacion', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('codigo', null, ['id'=>'codigo','class'=>'form-control', 'disabled' => 'disabled'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Direccion', 'Operacion', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('direccion', $operacion->direccion == 'P' ? 'Presentada' : 'Devuelta', ['id'=>'codigo','class'=>'form-control', 'disabled' => 'disabled'])!!}
            </div>
          </div>

          <div class="form-group row">
            {!! Form::label('Facturar(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              <label class="radio-inline">
                  {{ Form::radio('factura', 1) }} Facturar
              </label>
              <label class="radio-inline">
                  {{ Form::radio('factura', 0) }} No facturar
              </label>
            </div>
          </div>
          
        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarValidacionSistema']) }}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
      </div>
    </div>
  </div>
</div> 
@stop
@section('after-scripts-end')
 @include('includes.scripts.configuracion.scripts_recuperaciondecosto')
@stop
