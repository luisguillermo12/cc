{{-- @Nombre del programa: Vista Principal de re->Tiempo Liquidación--}}
{{-- @Funcion: --}}
{{-- @Autor: Milagros Negrín --}}
{{-- @Fecha Creacion: 29/06/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 15/05/18--}}
{{-- @Modificado por:    --}}


@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Recuperación de costos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i>Configuración</li>
        <li class="breadcrumb-item active">Cobranza</li>
        <li class="breadcrumb-item active">Recuperación de costos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
<div class="col-md-12">
  <!-- Line chart -->
  <div class="card card-warning card-outline">
    <div class="card-header">
      <h3 class="card-title">Actualizar</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="box-body">
            <div class="col-sm-12 form-horizontal">
                <div class="form-group row ">
                {!! Form::label('Operaciones(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
                    <div class="col-sm-6">
                        <label class="radio offset-sm-1" onclick="habilitarBoton()">
                        @if($presentadas==1)
                            {{ Form::radio('factura', 'P', 'checked') }} Presentadas
                        @else
                        {{ Form::radio('factura', 'P') }} Presentadas
                        @endif
                        </label>
                        <label class="radio offset-sm-1" onclick="habilitarBoton()">
                        @if($devueltas==1)
                        {{ Form::radio('factura', 'D', 'checked') }} Devueltas
                        @else
                        {{ Form::radio('factura', 'D') }} Devueltas
                        @endif
                        </label>
                        <label class="radio offset-sm-1" onclick="habilitarBoton()">
                        @if($todas)
                        {{ Form::radio('factura', 'A',  'checked') }} Todas
                        @else
                        {{ Form::radio('factura', 'A') }} Todas
                        @endif
                        </label>
                    </div>
                </div>
                @if (!Parametro::sistemaIniciado())
                <div class="form-group">
                    <div class="col-sm-3 offset-sm-6">
                        <div class="pull-right">
                        <button id="actualizar" class="btn btn-success btn-sm" onclick="actualizarOperacion()" disabled>Actualizar</button>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
  </div>
</div>
</div>

<div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table id="EmpresasOrdenantes" class="table table-striped table-sm">
            <thead>
              <tr style="background-color: #c2e7fc;">
                <th><center>Tipo de operación</center></th>
                <th><center>Código</center></th>
                <th><center>Operaciones</center></th>
                <th><center>Facturable</center></th>
                <th><center>Acción</center></th>
              </tr>
            </thead>
             @foreach ($tipos_operaciones as $operacion)
            <tr>
                <td>{{ $operacion->nombre }}</td>
                <td><center>{{ $operacion->codigo }}</center></td>
                @if ($operacion->direccion == 'P')
                <td><center>Presentada</center></td>
                @else
                <td><center>Devuelta</center></td>
                @endif
                @if ($operacion->factura == 1)
                <td><center><span class="text-white badge badge-success"  style="font-size: 13px;">Si</span></center></td>
                @else
                <td><center><span class="text-white badge badge-danger"  style="font-size: 11px;">No</span></center></td>
                @endif
                <td>
                @if (!Parametro::sistemaIniciado())
                @if (auth()->user()->hasPermission(['00-07-01-XX-02']))
                <center><a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-primary accion" href="{{route('RecuperacionCostos.edit', $operacion->id)}}"><i class="fa fa-pencil"></i></a></center>
                @endauth
                @endif
                </td>
            </tr>
            @endforeach
          </table>
          <br>

        </div>
      </div>
    </div>
</div>


  @endsection
  @section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_recuperaciondecosto')
  @stop
