{{-- @Nombre del programa: Vista Principal co->Monedas->CodigoISO --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i>Código ISO</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item">Monedas</li>
        <li class="breadcrumb-item active">Código ISO</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if (!Parametro::sistemaIniciado())
          <a title="Crear participante" class="btn btn-sm btn-success" href="{{route('CodigoISO.create')}}"><i class="fa fa-plus"></i> Crear código ISO</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <div class="col-sm-12">
          <table id="example1" class="table table-bordered table-striped table-hover table-sm">
            <thead>
              <tr class ="theadtable">
                <th><center>Código ISO</center></th>
                <th><center>Descripción</center></th>
                <th><center>Estado</center></th>
                <th><center>Símbolo</center></th>
                <th><center>Acciones</center></th>
              </tr>
            </thead>
            @foreach ($codigo_iso as $codigoiso)
            <tr>
              <td><center>{{ $codigoiso->codigo_iso }}</center></td>
              <td>{{ $codigoiso->nombre }}</td>
              <td><center>{{ $codigoiso->estatus }}</center></td>
              <td><center>{{ $codigoiso->simbolo }}</center></td>
              <td>
                <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn btn-sm btn-info" href="{{route('CodigoISO.show',$codigoiso->id)}}"><i class="fa fa-eye"></i></a>

                @if (!Parametro::sistemaIniciado())
                <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-sm btn-primary" href="{{route('CodigoISO.edit',$codigoiso->id)}}"><i class="fa fa-pencil"></i></a>
                @if ($codigoiso->estatus == 'INACTIVO' )  
                <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn btn-sm btn-danger" href="#" OnClick="Eliminar('{{$codigoiso->id}}','{{$codigoiso->nombre}}')"><i class="fa fa-trash"></i></a>
                @endif
                @endif

              </td>
            </tr>
            @endforeach
          </table>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
</div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.monedas.scripts_monedas')
@stop
