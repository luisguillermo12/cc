{{-- @Nombre del programa: Vista Principal co->Monedas->CodigoISO(Detalles) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i>Código ISO</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Monedas/CodigoISO')}}">Monedas</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Monedas/CodigoISO')}}">Código ISO</a></li>
        <li class="breadcrumb-item active">Detalle</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Detalle</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">

        <div class="row">

          <div class="col-sm-6">
            <p><strong>Código ISO</strong></p>
            <p class="text-muted">{{ $monedaMostrar->codigo_iso }}</p>
          </div>

          <div class="col-sm-6">
            <p><strong>Moneda</strong></p>
            <p class="text-muted">{{ $monedaMostrar->nombre }}</p>
          </div>

        </div>
        <div class="row">

          <div class="col-sm-6">
            <p><strong>Cantidad de decimales</strong></p>
            <p class="text-muted">{{ $monedaMostrar->num_decimales }}</p>
          </div>

          <div class="col-sm-6">
            <p><strong>Estado</strong></p>
            <p class="text-muted"><span class="label label-primary">{{ $monedaMostrar->estatus }}</span></p>
          </div>

        </div>

        @if($monedaMostrar->simbolo == '')
        @else
        <div class="row">
          <div class="col-sm-6">
            <p><strong>Símbolo</strong></p>
            <p class="text-muted">{{ $monedaMostrar->simbolo }}</p>
          </div>
        </div>
        @endif

        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
@stop
{{--Fin--}}

@section('after-scripts-end')

@stop