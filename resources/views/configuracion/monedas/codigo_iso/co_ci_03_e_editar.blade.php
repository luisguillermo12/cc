{{-- @Nombre del programa: Vista Principal co->Monedas->CodigoISO(Editar) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i>Código ISO</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Monedas/CodigoISO')}}">Monedas</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Monedas/CodigoISO')}}">Código ISO</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Editar</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::model($codigoiso, ['route' => ['CodigoISO.update', $codigoiso], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        {!!Form::hidden('id',null,['id'=>'id','class'=>'form-control'])!!}

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Código ISO(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('codigo_iso',null,['id'=>'codigo_iso','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código ISO','maxlength'=>3])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombre(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();', 'readonly'=>'readonly'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Cantidad de decimales(*)', 'Cantidad de decimales(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('num_decimales',null,['id'=>'num_decimales','class'=>'form-control','placeholder'=>'Ingrese Cantidad de decimales','maxlength'=>1, 'readonly'=>'readonly'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Separador de miles(*)', 'Separador de miles(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('separador_miles',null,['id'=>'separador_miles','class'=>'form-control','placeholder'=>'Ingrese Símbolo monetario','maxlength'=>1, 'readonly'=>'readonly'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Separador de decimales(*)', 'Separador de decimales(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('separador_decimales',null,['id'=>'separador_decimales','class'=>'form-control','placeholder'=>'Ingrese Símbolo monetario','maxlength'=>1, 'readonly'=>'readonly'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Símbolo monetario(*)', 'Símbolo monetario(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('simbolo',null,['id'=>'simbolo','class'=>'form-control','placeholder'=>'Ingrese Símbolo monetario','maxlength'=>1, 'readonly'=>'readonly'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id' => 'actualizarTipoMoneda','disabled']) }}
            </div>
          </div>
        </div>

        {{ Form::close() }}
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.monedas.scripts_monedas')
@stop
