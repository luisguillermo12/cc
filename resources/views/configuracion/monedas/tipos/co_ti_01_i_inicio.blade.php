{{-- @Nombre del programa: Vista Principal co->Monedas->Monedas --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Tipos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item">Monedas</li>
        <li class="breadcrumb-item active">Tipos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if (!Parametro::sistemaIniciado())
          <a title="Crear participante" class="btn btn-sm btn-success" href="{{route('Monedas.Tipos.create')}}"><i class="fa fa-plus"></i> Crear tipo</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <thead>
            <tr class ="theadtable">
              <th><center>Código ISO</center></th>
              <th><center>Moneda</center></th>
              <th><center>Estado</center></th>
              <th><center>Símbolo monetario</center></th>
              <th><center>Acciones</center></th>
            </tr>
          </thead>
          @foreach ($monedas as $moneda)
          <tr>
            <td><center>{{ $moneda->codigo_iso }}</center></td>
            <td>{{ $moneda->nombre }}</td>
            <td><center>{{ $moneda->estatus }}</center></td>
            <td><center>{{ $moneda->simbolo }}</center></td>
            <td>
              <center>
                <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn btn-sm btn-info" href="{{route('Monedas.Tipos.show', $moneda->id)}}"><i class="fa fa-eye"></i></a>
                @if (!Parametro::sistemaIniciado())
                <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-sm btn-primary" href="{{route('Monedas.Tipos.edit', $moneda->id)}}"><i class="fa fa-pencil"></i></a>
                @endif
              </center>
            </td>
          </tr>
          @endforeach
        </table>
      </div><!--table-responsive-->
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.monedas.scripts_monedas')
@stop
