{{-- @Nombre del programa: Vista Principal co->Monedas->Monedas(Crear) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Tipos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Monedas/Monedas')}}">Monedas</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Monedas/Monedas')}}">Tipos</a></li>
        <li class="breadcrumb-item active">Crear</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Crear</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::open(['route' => 'Monedas.Tipos.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) }}

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Codigo ISO(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!! Form::select('codigo_iso',$codigo_iso,null,['id'=>'codigo_iso','class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::hidden('Nombre(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::hidden('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::hidden('Cantidad de decimales(*)', 'Cantidad de decimales(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::hidden('num_decimales',null,['id'=>'num_decimales','class'=>'form-control mayusculas','placeholder'=>'Ingrese Cantidad de decimales','maxlength'=>1])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::hidden('Monto máximo bajo valor(*)', 'Monto máximo bajo valor(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::hidden('monto_max_bv',null,['id'=>'monto_max_bv','class'=>'form-control mayusculas','placeholder'=>'Ingrese Monto Máximo Bajo Valor','maxlength'=>16])!!}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <div class="pull-right">
              {{ Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title' => 'Guardar']) }}
            </div>
          </div>
        </div>
        <div class="clearfix"></div>

        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.monedas.scripts_monedas')
@stop
