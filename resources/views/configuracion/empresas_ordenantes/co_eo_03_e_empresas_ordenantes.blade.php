{{-- @Nombre del programa: Vista Principal de re->Tiempo Liquidación--}}
{{-- @Funcion: --}}
{{-- @Autor: Milagros Negrín --}}
{{-- @Fecha Creacion: 29/06/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 15/05/18--}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')



@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Empresas Ordenantes </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/EmpresasOrdenantes')}}"><i class="fa fa-cog"></i>Configuracion</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/EmpresasOrdenantes')}}">Empresas ordenantes</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Crear</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body offset-sm-1">
        {!! Form::open(['route' => ['EmpresasOrdenantes.update', $empresa_ordenante->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch','id'=>'form-create']) !!}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row col-md-12">
            {!!Form::label('RIF(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
              {!!Form::text('rif',$empresa_ordenante->rif,['id'=>'rif','class'=>'form-control','placeholder'=>'Ingrese RIF','maxlength'=>9, 'readonly'=>'readonly' ])!!}
              </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Nombre(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('nombre',$empresa_ordenante->nombre,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>
        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-7 col-sm-offset-3">
            <div class="pull-right">
               {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
                {!! link_to_route('EmpresasOrdenantes.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
    </div>
    </div>
    </div>
  </div>   
@stop

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_empresas_ordenantes')
@stop
