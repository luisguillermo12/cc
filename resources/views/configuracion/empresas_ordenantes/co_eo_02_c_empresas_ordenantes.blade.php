{{-- @Nombre del programa: --}}
{{-- @Funcion:  --}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/02/2019 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

@section ('title', 'Electronic Payment Suite (EPS)' . ' | ' . 'Crear Empresa ordenante')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Empresas Ordenantes </h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/EmpresasOrdenantes')}}"><i class="fa fa-cog"></i>Configuracion</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/EmpresasOrdenantes')}}">Empresas ordenantes</a></li>
        <li class="breadcrumb-item active">Crear</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Crear</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body offset-sm-1">
        {!! Form::open(['route' => 'EmpresasOrdenantes.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','id'=>'form-create']) !!}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row col-md-12">
            {!!Form::label('RIF(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-2">
              {!!Form::select('rifAux', array('V' => 'V','E' => 'E','P' => 'P','J' => 'J', 'G' => 'G', 'C' => 'C'),null,['id'=>'rifAux','class'=>'form-control']) !!}
              </div>
              <div class="col-sm-4">
                {!!Form::text('rif',null,['id'=>'rif','class'=>'form-control','placeholder'=>'Ingrese RIF','maxlength'=>9 , 'onblur'=>'fucionar();'  ])!!}
              </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Nombre(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>

        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-7 col-sm-offset-3">
            <div class="pull-right">
               {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title'=>'Guardar']) !!}
                {!! link_to_route('Seguridad.Usuarios.Activos', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title'=>'Cancelar']) !!}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
    </div>
    </div>
    </div>
  </div>   
@stop

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_empresas_ordenantes')
@stop
