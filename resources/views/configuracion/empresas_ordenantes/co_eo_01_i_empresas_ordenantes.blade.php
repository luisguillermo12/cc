{{-- @Nombre del programa: --}}
{{-- @Funcion:--}}
{{-- @Autor: btc --}}
{{-- @Fecha Creacion: 01/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 01/01/2019 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Empresas ordenantes</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuracion</a></li>
              <li class="breadcrumb-item active">Empresas ordenantes</li>
            </ol>
          </div>
        </div>
      </div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-warning card-outline">
      <div class="card-header">
        <h3 class="card-title">
          Filtros
        </h3>
         <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
      </div>
      <div class="card-body">
          <div class="row">
            <div class="col-md-5">
              <div class="form-inline">
                RIF
                <div class="col-sm-8">
                  <form role="form" class="form-horizontal">
                  {!!Form::text('rif',null,['id'=>'rifselecemp','class'=>'form-control mayusculas','placeholder'=>'Ingrese rif','maxlength'=>10,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-inline">
                Empresa
                <div class="col-sm-8">
                  {!!Form::text('nombre',null,['id'=>'nombreselecemp','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="card-footer">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
        </div>
      </div>
     </form>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools">
            @if (auth()->user()->hasPermission(['00-08-01-XX-02']))
            <a title="Crear usuario" class="btn btn-success accion" href="{{route('EmpresasOrdenantes.create')}}"><i class="fa fa-plus"></i> Crear empresa ordenante</a>
            @endif
            <button type="button" class="btn btn-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>

        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover table-sm">
            <thead>
              <tr class ="theadtable">
                <th><center>RIF</center></th>
                <th><center>Empresas</center></th>
                <th><center>Estado</center></th>
                  <th><center>Acciones</center></th>
              </tr>
            </thead>
            @foreach ($empresasOrdenantes as $empresaOrdenante)
              {{--*/ @$nombre = str_replace(' ','&nbsp;', $empresaOrdenante->nombre) /*--}}
              <tr>
                <td><center>{{ $empresaOrdenante->rif }}</center></td>
                <td>{{ $empresaOrdenante->nombre }}</td>
                <td><center>{{ $empresaOrdenante->estatus }}</center></td>
                <td>
                  @if (!Parametro::sistemaIniciado())
                    <center>
                        @if (auth()->user()->hasPermission(['00-08-01-XX-03']))
                          <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn-sm btn-primary accion" href="{{route('EmpresasOrdenantes.edit',$empresaOrdenante->id)}}"><i class="fa fa-pencil"></i></a>
                        @endif
                        @if (auth()->user()->hasPermission(['00-08-01-XX-04']))
                          <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm btn-danger accion" href="#" OnClick="Eliminar('{{$empresaOrdenante->id}}')"><i class="fa fa-trash"></i></a>
                        @endif
                    </center>
                  @endif
                </td>
              </tr>
            @endforeach
          </table>
          <br>
        <div class="col-sm-12">
          <div class="pull-right">
            {{ $empresasOrdenantes->links() }}
          </div>
        </div>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
</div>
  @stop

  @section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_empresas_ordenantes')
  @stop
