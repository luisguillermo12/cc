@extends ('layouts.master')

@section ('title', 'Electronic Payment Suite (EPS) - Eventos'. ' | ' . 'Editar')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-md-6">
      <h1><i class="fa fa fa-cog"></i> Eventos</h1>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb float-md-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Eventos')}}"><i class="fa fa fa-cog"></i> Configuracion</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Eventos')}}"> Eventos</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Editar</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">

          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>

          {!! Form::open(['route' => ['ConfiguracionEventos.update', $evento], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id'=>'form-edit']) !!}

          <div class="row col-md-12 form-horizontal">

            <div class="form-group row col-md-12">
              {!! Form::label('Nombre', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                  {!!Form::text('nombre', $evento->nombre, ['class'=>'form-control', 'disabled'=>'disabled'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('cod_evento', 'Código de evento', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                  {!!Form::text('cod_evento', $evento->cod_evento, ['class'=>'form-control', 'disabled'=>'disabled'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('Descripcion', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text('descripcion', $evento->descripcion, ['class'=>'form-control', 'disabled'=>'disabled'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('estatus', 'Estado', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!! Form::select('estatus', ['Inactivo', 'Activo'], $evento->estatus, ['class'=>'form-control']) !!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('asunto', 'Asunto (*)', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text('asunto', $evento->asunto, ['class'=>'form-control'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('mensaje', 'Mensaje (*)', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!! Form::textarea('mensaje', $evento->mensaje, ['class'=>'form-control', 'rows' => "4"]) !!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('correo', 'Enviar por correo', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-1">
                {!! Form::checkbox('correo', 1, $evento->correo) !!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('roles', 'Roles', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::select('roles[]', $roles, $evento->destinatarios->roles, ['class'=>'form-control chosen-select custon', 'multiple'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('usuarios', 'Usuarios', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::select('usuarios[]', $users, $evento->destinatarios->usuarios, ['class'=>'form-control chosen-select', 'multiple'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('correos', 'Correos', array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text(null, null, ['class'=>'form-control', 'id' => 'agregar-correo', 'placeholder' => 'Agregar nuevo correo a la lista...'])!!}
                {!!Form::select('correos[]', array_combine($evento->destinatarios->correos, $evento->destinatarios->correos), $evento->destinatarios->correos, ['class'=>'form-control chosen-select', 'id' => 'chosen-correos', 'multiple'])!!}
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer text-center">
          {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-md','title' => 'Actualizar']) }}
        </div>
      </div>
    </div>
  </div>
   
  </div><!-- /.box-body -->
</div><!--box-->
@stop

@section('after-styles-end')
 <link rel="stylesheet" href="{{asset('css/chosen/chosen.css')}}">
@stop

@section('after-scripts-end')
  <script type="text/javascript" src="{{asset('js/chosen/chosen.jquery.js')}}"></script>

  <script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      no_results_text: "No se encontraron resultados para "
    }); 

    $('#agregar-correo').change(function () {
      var correo = $('#agregar-correo').val();
      $("#chosen-correos").append('<option value="' +correo+ '" selected="selected">' +correo+ '</option>');
      $('#chosen-correos').trigger("chosen:updated");
      $('#agregar-correo').val('')
    });

    $( "#form-edit" ).submit(function (event) {
      event.preventDefault(event);
      swal({
        title: '¿Está seguro que quiere actualizar este evento?',
        icon: 'warning',
        buttons: ['Cancelar', "Aceptar"]
      }).then((result) => {
        if (result) {
          $("#form-edit").off("submit").submit();
        }
      });
    });
  </script>
@stop
