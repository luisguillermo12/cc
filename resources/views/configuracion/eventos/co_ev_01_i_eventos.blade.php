@extends ('layouts.master')

@section('after-styles-end')
  <style type="text/css">
    .eps-warning {
      background-color: #f39c12 !important;
    }
  </style>
@endsection

@section ('title', 'Electronic Payment Suite (EPS) - Eventos'. ' | ' . 'Listado')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-cog"></i> Eventos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa fa-cog"></i> Configuracion</li>
        <li class="breadcrumb-item active">Eventos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-warning card-outline">
      <div class="card-header">
        <h3 class="card-title">Listado</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped table-sm">
          <tr style="background-color: #c2e7fc;">
            <th><center>Nombre</center></th>
            <th><center>Descripción</center></th>
            <th><center>Envío por correo</center></th>
            <th><center>Envío por sms</center></th>
            <th><center>Estado</center></th>
            <th><center>Acciones</center></th>
          </tr>
          @forelse ($eventos as $evento)
            <tr>
              <td class="text-center">{{ $evento->nombre }}</td>
              <td class="text-center">{{ $evento->descripcion }}</td>
              @if ($evento->correo)
                <td class="text-center"><span class="text-white badge badge-success">Si</span></td>
              @else
                <td class="text-center"><span class="text-white badge eps-warning">No</span></td>
              @endif
              @if ($evento->sms)
                <td class="text-center"><span class="text-white badge badge-success">Si</span></td>
              @else
                <td class="text-center"><span class="text-white badge eps-warning">No</span></td>
              @endif
              @if ($evento->estatus)
                <td class="text-center"><span class="text-white badge badge-success">Activo</span></td>
              @else
                <td class="text-center"><span class="text-white badge eps-warning">Inactivo</span></td>
              @endif
              <td><center>
                @if(auth()->user()->hasPermission(['00-10-XX-XX-03']))
                <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary" href="{{route('ConfiguracionEventos.edit', $evento->id)}}"><i class="fa fa-pencil"></i></a>
                @endif
              </center></td>
            </tr>
            @empty
              <td colspan="6">No existen registros de eventos.</td>
            @endforelse
        </table>
      </div>
      <!-- /.card-body-->
    </div>
    <!-- /.card -->
  </div>
</div>
@stop

@section('after-scripts-end')

@stop
