{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-cog"></i> Intervalos de liquidación</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/IntervalosLiquidacion') }}"><i class="fa fa-cog"></i> Configuración</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/IntervalosLiquidacion') }}"><i class="fa fa-cog"></i> Período de compensación</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/IntervalosLiquidacion') }}"><i class="fa fa-cog"></i> Intervalos de liquidación</a></li>
          <li class="breadcrumb-item active">Editar</li>
        </ol>
      </div>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
          <div class="card-header with-border">
            <h3 class="card-title">
              <!--codigo gui MOD-CONF-10.2---->
              <input type="hidden" name="codigo_gui" value="MOD-CONF-10.2.2" id="codigo_gui">
              Editar
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            {!!Form::open([
              'method'=>'PUT',
              'id'=>'form-edit',
              'class'=>'form-row',
              'autocomplete'=>'off',
              'route'=>['IntervalosLiquidacion.update',$periodo->id]]) !!}
          <div class="col-sm-12">
                <table class="col-sm-12 text-center table-sm">
                  <tr>
                    <td class="text-center"><strong>Producto</strong></td>
                    <td><strong>Día<br>de inicio</strong></td>
                    <td><strong>Hora<br>de inicio</strong></td>
                    <td><strong>Día<br>de fin</strong></td>
                    <td><strong>Hora<br>de fin</strong></td>
                    <td><strong>Intervalos<br>de<br>liquidación</strong></td>
                    <td><strong>Franjas</strong></td>
                  </tr>
                  <tr>
                    <td><h3>{{$periodo->producto}}</h3></td>
                    <td><h3><span class="badge badge-info">{{$periodo->dia_inicio}}</span></td>
                    <td><h3><span class="badge badge-info">{{$periodo->hora_inicio}}</span></td>
                    <td><h3><span class="badge badge-info">{{$periodo->dia_fin}}</span></td>
                    <td><h3><span class="badge badge-info">{{$periodo->hora_fin}}</span></td>
                    <td><h3><span class="badge badge-info">{{$periodo->num_liquidacion}}</span></td>
                    <td><h3><span class="badge badge-info">{{$periodo->num_franja}}</span></td>
                  </tr>

                </table>
          </div>
          <div class="col-sm-12" id="app">

            <hr class="col-sm-12">

            @include('includes.messages')

            <div class="callout callout-info col-sm-4 offset-sm-4">
              <h4><i class="fa fa-info-circle"></i> Nota:</h4>
              <p>Los campos marcados con un asterisco (*) son obligatorios.</p>
            </div>

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

            <div class="col-sm-12 form-row">
              <div class="col-sm-3">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Tipo de operación</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-1">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Día de inicio(*)</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Hora de inicio(*)</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-1">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Día de fin(*)</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Hora de fin(*)</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-1">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Liquidación</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-1">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Orden CMPSA</strong>
                  </div>
                </div>
              </div>
              <div class="col-sm-1">
                <div class="col-sm-12">
                  <div class="form-group text-center">
                    <strong>Franja</strong>
                  </div>
                </div>
              </div>
            </div>
            <?php $cont = 0; ?>
            @foreach ($intervalos_liquidacion as $intervalo_liquidacion)
              <div class="col-sm-12 form-row">

                {!! Form::hidden('intervalo_liquidacion['.$cont.']',$intervalo_liquidacion->id,['id'=>'intervalo_liquidacion','class'=>'form-control input-sm intervalo_liquidacion','readonly'=>'readonly', 'v-on:change'=>'handleGraph']) !!}

                <div class="col-sm-3">
                  <div class="col-sm-12">
                    <div class="form-group">
                      {!! Form::select('tipo_operacion_id['.$cont.']',$tipos_operaciones,$intervalo_liquidacion->tipo_operacion_id,['id'=>'tipo_operacion_id','class'=>'form-control input-sm','disabled'=>'disabled']) !!}
                    </div>
                  </div>
                </div>

                <div class="col-sm-1">
                  <div class="col-sm-12">
                    <div class="form-group">
                      {!!Form::select('dia_inicio['.$cont.']', array('' => 'Seleccione','D-1' => 'D-1','D' => 'D'),trim($intervalo_liquidacion->dia_inicio),['id'=>'dia_inicio','class'=>'form-control input-sm', 'id' => 'dia-inicio-' . $cont]) !!}
                    </div>
                  </div>
                </div>

                <div class="col-sm-2 bootstrap-timepicker">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        {!!Form::text('hora_inicio['.$cont.']',$intervalo_liquidacion->hora_inicio,['id'=>'hora_inicio','class'=>'form-control timepicker input-sm hora_inicio fechahora_inicio','placeholder'=>'Hora Inicio','maxlength'=>8,'required'=>'required', 'id' => 'hora-inicio-' . $cont, 'v-on:change'=>'handleGraph'])!!}
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-1">
                  <div class="col-sm-12">
                    <div class="form-group">
                      {!!Form::select('dia_fin['.$cont.']', array('' => 'Seleccione','D-1' => 'D-1', 'D' => 'D'),trim($intervalo_liquidacion->dia_fin),['id'=>'dia_fin','class'=>'form-control input-sm','required'=>'required', 'id' => 'dia-fin-' . $cont]) !!}
                    </div>
                  </div>
                </div>

                <div class="col-sm-2 bootstrap-timepicker">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <div class="input-group">
                        {!! Form::text('hora_fin['.$cont.']',$intervalo_liquidacion->hora_fin,['id'=>'hora_fin','class'=>'form-control timepicker input-sm hora_fin fechahora_fin','placeholder'=>'Hora Fin','maxlength'=>8,'required'=>'required', 'id' => 'hora-fin-' . $cont, 'v-on:change'=>'handleGraph']) !!}
                        <div class="input-group-append">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-1">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <?php $readonly = $intervalo_liquidacion->num_franja == '1' ? false : true; ?>
                      {!!Form::text('num_liquidacion['.$cont.']', $intervalo_liquidacion->num_liquidacion, ['id' => 'num_liquidacion' . $cont, 'class'=>'form-control input-sm text-center', 'readonly'=>$readonly, 'required'=>'required', 'onchange' => $readonly ? $readonly : "copiar_intervalo($cont)"])!!}
                    </div>
                  </div>
                </div>

                <div class="col-sm-1">
                  <div class="col-sm-12">
                    <div class="form-group">
                      {!!Form::text('num_cmpsa['.$cont.']', $intervalo_liquidacion->num_cmpsa, ['id' => 'num_cmpsa' . $cont, 'class'=>'form-control input-sm text-center', 'required'=>'required'])!!}
                    </div>
                  </div>
                </div>

                <div class="col-sm-1">
                  <div class="col-sm-12">
                    <div class="form-group">
                      {!!Form::text('num_franja['.$cont.']',$intervalo_liquidacion->num_franja,['id'=>'num_franja','class'=>'form-control input-sm text-center','readonly'=>'readonly'])!!}
                    </div>
                  </div>
                </div>

              </div>
              <?php $cont++; ?>
            @endforeach
          </div>
          </div>
          <div class="card-footer">
            <div class="card-tools pull-right">
                {{ Form::submit('Actualizar',['class'=>'btn btn-success btn-sm m-t-10','title'=>'Actualizar']) }}
            </div>
          </div>
          {!!Form::close()!!}
        </div>
      </div>
      {{-- inicio seccion de graficas --}}

        @include('configuracion.periodo_compensacion.intervalos_liquidacion.co_pc_02_e_editar_graficas')

      {{-- fin seccion de graficas --}}
    </div>
  @stop

  {{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.graficos_general.scripts_graficos_general')
  @include('includes.scripts.configuracion.periodo_compensacion.intervalos_liquidacion.scripts_intervalos_editar_graficas')
@stop
