{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-cog"></i> Intervalos de liquidación</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/IntervalosLiquidacion') }}"><i class="fa fa-cog"></i> Configuración</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/IntervalosLiquidacion') }}"><i class="fa fa-cog"></i> Período de compensación</a></li>
          <li class="breadcrumb-item active">Intervalos de liquidación</li>
        </ol>
      </div>
    </div>
  </div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card card-warning card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-10.2---->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-10.2.1" id="codigo_gui">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools pull-right">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table id="Periodos-table" class="table table-striped table-bordered table-hover table-sm" align="center">
            <thead class="thead-default">
              <tr>
                <th><center>Producto</center></th>
                <th><center>Tipo de operación</center></th>
                <th><center>Inicio</center></th>
                <th ><center>Hora de inicio</center></th>
                <th><center>Fin</center></th>
                <th ><center>Hora de fin</center></th>
                <th><center>Liquidación</center></th>
                <th><center>Orden CMPSA</center></th>
                <th><center>Franja</center></th>
                <th><center>Acciones</center></th>
              </tr>
            </thead>
            <?php $cont = 1; $cont_general = 0; ?>
            @foreach ($intervalos_liquidacion as $intervalo_liquidacion)
            <?php $nombre = str_replace(' ','&nbsp;', $intervalo_liquidacion->tipo_operacion); ?>
            @if ($cont==1 && $cont_general==1)
              <tr><td colspan="10" style="background-color: #c2e7fc;"></td></tr>
            @endif
            <tr>
              @if ($cont==1)
                <td rowspan="{{ $intervalo_liquidacion->p_num_franja }}" style="vertical-align: middle;">{{ $intervalo_liquidacion->producto }}</td>
                <?php $cont_general = 1; ?>
              @endif
              <td>{{ $intervalo_liquidacion->tipo_operacion }}</td>
              <td><center>{{ $intervalo_liquidacion->dia_inicio }}</center></td>
              <td><center>{{ $intervalo_liquidacion->hora_inicio }}</center></td>
              <td><center>{{ $intervalo_liquidacion->dia_fin }}</center></td>
              <td><center>{{ $intervalo_liquidacion->hora_fin }}</center></td>
              @if ($intervalo_liquidacion->num_franja == 1)
              <td rowspan="2" style="vertical-align: middle;"><center>{{ $intervalo_liquidacion->num_liquidacion }}</center></td>
              @endif
              <td><center>{{ $intervalo_liquidacion->num_cmpsa }}</center></td>
              <td><center>{{ $intervalo_liquidacion->num_franja }}</center></td>
              @if ($cont==1)
              <td rowspan="{{ $intervalo_liquidacion->p_num_franja }}" style="vertical-align: middle;">
                @if (auth()->user()->hasPermission(['00-05-02-XX-03']))
                  @if (!Parametro::sistemaIniciado())
                  <center>
                    <a class="btn-sm btn-primary" href="{{route('IntervalosLiquidacion.edit',$intervalo_liquidacion->periodo_id)}}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                  </center>
                  @endif
                @endif
              </td>
              @endif
            </tr>
            @if ($cont==$intervalo_liquidacion->p_num_franja)
            <?php $cont = 1; ?>
            @else
            <?php $cont++; ?>
            @endif
            @endforeach
            <tr><td colspan="10" style="background-color: #c2e7fc;"></td></tr>
          </table>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
  {{-- inicio seccion de graficas --}}

    @include('configuracion.periodo_compensacion.intervalos_liquidacion.co_pc_01_i_intervalos_graficas')

  {{-- fin seccion de graficas --}}
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.graficos_general.scripts_graficos_general')
  @include('includes.scripts.configuracion.periodo_compensacion.intervalos_liquidacion.scripts_intervalos_liquidacion_graficas')
@stop
