{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-cog"></i> Períodos</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Configuración</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Período de compensación</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Períodos</a></li>
          <li class="breadcrumb-item active">Crear</li>
        </ol>
      </div>
    </div>
  </div>
@endsection

@section('content')
        <div class="card card-warning card-outline">
            <div class="card-header with-border">
              <!--codigo gui MOD-CONF-10.1---->
              <input type="hidden" name="codigo_gui" value="MOD-CONF-10.1.2" id="codigo_gui">
                <h3 class="card-title">Crear</h3>
            </div><!-- /.card-header -->
            <div class="card-body">
              {{ Form::open(['route' => 'Periodos.store', 'class' => 'form-row', 'role' => 'form', 'method' => 'POST','id'=>'form-create']) }}

              <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

              @include('includes.messages')

              <div class="callout callout-info col-sm-10 offset-sm-1">
                <h4><i class="fa fa-info-circle"></i> Nota:</h4>
                <p>Los campos marcados con un asterisco (*) son obligatorios.</p>
                <p>Al momento de crear un período automáticamente se crean los <strong> Intervalos de liquidación </strong>, para que usted pueda terminar de configurar el período de compensación.</p>
              </div>

              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Producto(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6">
                  {!! Form::select('producto_id',$productos,null,['id'=>'producto_id','class'=>'form-control']) !!}
                </div>
              </div>
              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Día de inicio(*)', 'Día de inicio(*)', array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6">
                  {!!Form::select('dia_inicio', array('0' => 'Seleccione','D-1' => 'D-1','D' => 'D'),null,['id'=>'dia_inicio','class'=>'form-control']) !!}
                </div>
              </div>
              <div class="col-sm-12 form-group form-row bootstrap-timepicker">
                {!! Form::label('Hora de inicio(*)', 'Hora de inicio(*)', array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-3">
                  <div class="input-group">
                    {!!Form::text('hora_inicio',null,['id'=>'hora_inicio','class'=>'form-control timepicker hora_inicio','placeholder'=>'Ingrese Hora Inicio','maxlength'=>6])!!}
                    <div class="input-group-append">
                      <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Día de fin(*)', 'Día de fin(*)', array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6">
                  {!!Form::select('dia_fin', array('0' => 'Seleccione', 'D' => 'D'),null,['id'=>'dia_fin','class'=>'form-control']) !!}
                </div>
              </div>
              <div class="col-sm-12 form-group form-row bootstrap-timepicker">
                {!! Form::label('Hora de fin(*)', 'Hora de fin(*)', array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-3">
                  <div class="input-group bootstrap-timepicker timepicker">
                  {!!Form::text('hora_fin',null,['id'=>'hora_fin','class'=>'form-control timepicker hora_fin','placeholder'=>'Ingrese Hora Fin','maxlength'=>6])!!}
                  <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                  </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Intervalos de liquidación(*)', 'Intervalos de liquidación(*)', array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6">
                  {!!Form::text('num_liquidacion',null,['id'=>'num_liquidacion','class'=>'form-control','placeholder'=>'Ingrese N° de Intervalos de Liquidación','maxlength'=>2])!!}
                </div>
              </div>
              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6 form-check form-check-inline">
                  <label class="radio-inline form-check-label" for="estatus">
                  {{ Form::radio('estatus', 'ACTIVO', true) }} Activo
                  </label>
                  <label class="radio-inline form-check-label" for="estatus">
                  {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
                  </label>
                </div>
              </div>
            </div><!-- /.card-body -->

            <div class="card-footer">
              <div class="card-tools pull-right">
                {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-success','title'=>'Guardar']) }}
              </div>
            </div>

        </div><!--card-->
    {{ Form::close() }}
@stop

@section('after-scripts-end')
    @include('includes.scripts.configuracion.periodo_compensacion.periodos.scripts_periodos')
@stop
