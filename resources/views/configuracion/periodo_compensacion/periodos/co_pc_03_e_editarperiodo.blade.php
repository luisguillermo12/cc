{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}
@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Períodos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Período de compensación</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Períodos</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
    <div class="card card-warning card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-10.1---->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-10.1.3" id="codigo_gui">
        <h3 class="card-title">Editar</h3>
        <div class="card-tools pull-right">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </div>
      </div>
      <div class="card-body">
        {!!Form::model($periodo, ['route'=>['Periodos.update',$periodo], 'class'=>'form-row', 'role' => 'form', 'method'=>'PUT','id'=>'form-edit']) !!}
        <div class="form-group form-row">
          <div class="col-sm-6 col-sm-offset-3">
            @include('includes.messages')
          </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <input type="hidden" id="id">

        <div class="callout callout-info col-sm-4 offset-sm-4">
          <h4><i class="fa fa-info-circle"></i> Nota:</h4>
          <p>Los campos marcados con un asterisco (*) son obligatorios.</p>
        </div>

        <div class="col-sm-12 form-group form-row">
          {!!Form::label('Producto(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!! Form::text('producto',$productos->nombre_completo, ['id'=>'producto','class'=>'form-control', 'disabled'=>'disabled']) !!}
          </div>
          <input type="hidden" name="producto_id" id="producto_id" value="{{ $periodo->producto_id }}">
        </div>
        <div class="col-sm-12 form-group form-row">
          {!! Form::label('Día de inicio(*)', 'Día de inicio(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::select('dia_inicio', array('0' => 'Seleccione','D-1' => 'D-1','D' => 'D'),null,['id'=>'dia_inicio','class'=>'form-control']) !!}
          </div>
        </div>
        <div class="col-sm-12 form-group form-row bootstrap-timepicker">
          {!! Form::label('Hora de inicio(*)', 'Hora de inicio(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-3">
            <div class="input-group">
              {!!Form::text('hora_inicio',null,['id'=>'hora_inicio','class'=>'form-control timepicker hora_inicio','placeholder'=>'Ingrese Hora Inicio'])!!}
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 form-group form-row">
          {!! Form::label('Día de fin(*)', 'Día de fin(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::select('dia_fin', array('0' => 'Seleccione', 'D' => 'D'),null,['id'=>'dia_fin','class'=>'form-control']) !!}
          </div>
        </div>
        <div class="col-sm-12 form-group form-row bootstrap-timepicker">
          {!!Form::label('Hora de fin(*)', 'Hora de fin(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-3">
            <div class="input-group">
              {!!Form::text('hora_fin',null,['id'=>'hora_fin','class'=>'form-control timepicker hora_fin','placeholder'=>'Ingrese Hora Fin'])!!}
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 form-group form-row">
          {!!Form::label('Intervalos de liquidación(*)', 'Intervalos de liquidación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!Form::text('num_liquidacion',null,['id'=>'num_liquidacion','class'=>'form-control','placeholder'=>'Ingrese Intervalo de Liquidación'])!!}
          </div>
        </div>
        <div class="col-sm-12 form-group form-row">
          {!!Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6 form-check form-check-inline">
            <label class="radio-inline form-check-label" for="estatus">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline form-check-label" for="estatus">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>
      </div>

      <div class="card-footer">
        <div class="card-tools pull-right">
          {!!Form::submit('Actualizar',['class'=>'btn btn-success btn-sm m-t-10','title'=>'Actualizar','id'=>'actualizarPeriodo','disabled'])!!}
        </div>
      </div>

    </div>
    {!!Form::close()!!}
  @stop

  {{--Inicio--}}

  @section('after-scripts-end')
    @include('includes.scripts.configuracion.periodo_compensacion.periodos.scripts_periodos')
  @stop
