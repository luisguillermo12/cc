{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Períodos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/PeriodoCompensacion/Periodos') }}"><i class="fa fa-cog"></i> Período de compensación</a></li>
        <li class="breadcrumb-item active">Períodos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-CONF-10.1---->
          <input type="hidden" name="codigo_gui" value="MOD-CONF-10.1.1" id="codigo_gui">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools pull-right">
            @if (auth()->user()->hasPermission(['00-05-01-XX-02']))
              @if (!Parametro::sistemaIniciado())
                <a class="btn btn-sm btn-success" href="{{route('Periodos.create')}}" title="Crear Período"><i class="fa fa-plus"></i> Crear período</a>
              @endif
            @endif
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div>
        <div class="card-body">
        <div class="col-sm-12">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          <div class="table-responsive">
            <table id="Periodos-table" class="table table-striped table-hover table-sm" align="center">
              <thead class="thead-default">
                <tr>
                  <th><center>Producto</center></th>
                  <th nowrap><center>Inicio</center></th>
                  <th nowrap><center>Hora de inicio</center></th>
                  <th nowrap><center>Fin</center></th>
                  <th nowrap><center>Hora de fin</center></th>
                  <th nowrap><center>Intervalos de liquidación</center></th>
                  <!--<th class="col-sm-1" nowrap><center>Número de liquidación</center></th>-->
                  <th nowrap><center>Franjas</center></th>
                  <th><center>Estado</center></th>
                  <th><center>Acciones</center></th>
                </tr>
              </thead>
              @foreach ($periodos as $periodo)
			    <?php $nombre = str_replace(' ','&nbsp;', $periodo->producto); ?>
                <tr>
                  <td nowrap>{{ $periodo->producto }}</td>
                  <td><center>{{ $periodo->dia_inicio }}</center></td>
                  <td><center>{{ $periodo->hora_inicio }}</center></td>
                  <td><center>{{ $periodo->dia_fin }}</center></td>
                  <td><center>{{ $periodo->hora_fin }}</center></td>
                  <td><center>{{ $periodo->num_liquidacion }}</center></td>
                  <!--<td><center>{{ $periodo->num_intervalo_liquidacion }}</center></td>-->
                  <td><center>{{ $periodo->num_franja }}</center></td>
                  <td><center>{{ $periodo->estatus }}</center></td>
                  <td>
                    @if (!Parametro::sistemaIniciado())
                    <center>
                      @if (auth()->user()->hasPermission(['00-05-01-XX-03']))
                        <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn-sm btn-primary" href="{{route('Periodos.edit',$periodo->id)}}"><i class="fa fa-pencil"></i></a>
                      @endif
                      @if (auth()->user()->hasPermission(['00-05-01-XX-04']))
                        @if ($periodo->estatus == 'INACTIVO')
                          <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm btn-danger" href="#" OnClick="Eliminar('{{$periodo->id}}','{{$nombre}}')"><i class="fa fa-trash"></i></a>
                        @endif
                      @endif
                    </center>
                    @endif
                  </td>
                </tr>
              @endforeach
            </table>
          </div><!--table-responsive-->
        </div>
        </div>
      </div>
    </div>
  </div>
@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.configuracion.periodo_compensacion.periodos.scripts_periodos')
@stop
