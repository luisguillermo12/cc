{{-- @Nombre del programa: Vista Principal co->contactos->Tipo(Crear) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section ('title', 'Electronic Payment Suite (EPS) - Tipos'. ' | ' . 'Crear')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-md-6">
      <h1><i class="fa fa fa-cog"></i> Tipos</h1>
    </div>
    <div class="col-md-6">
      <ol class="breadcrumb float-md-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Contactos/Tipos')}}"><i class="fa fa fa-cog"></i> Configuracion</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Contactos/Tipos')}}"> Contactos</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/Contactos/Tipos')}}"> Tipos</a></li>
        <li class="breadcrumb-item active">Crear</li>
      </ol>
    </div>
  </div>
</div>
@endsection
@section('content')

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Editar</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">

          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>

            {!! Form::open(['route' => 'Tipos.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) !!}

          <div class="form-group row">
            <div class="col-md-6 offset-md-3">
              <blockquote><p>Los campos marcados con un asterisco (*) son obligatorios.</p></blockquote><p></p>
            </div>
          </div>

          <div class="row col-md-12 form-horizontal">

            <div class="form-group row col-md-12">
              {!! Form::label('Nombre (*)', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('Estado(*)', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-sm-6">
                <label class="radio-inline">
                  {{ Form::radio('estatus', 'ACTIVO') }} Activo
                </label>
                <label class="radio-inline">
                  {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
                </label>
              </div>
            </div>
          </div>
          <div class="card-footer text-center" style="border-top: none !important">
            {!! Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title' => 'Guardar']) !!}
            {{-- {!! link_to_route('TiposContactos', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title' => 'Cancelar']) !!} --}}
          </div>
        </div>
        {{ Form::close() }}
      </div>
      <!-- /.card -->
    </div>
  </div>

@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_tipos_contactos')
@stop
