{{-- @Nombre del programa: Vista Principal co->contactos->Tipo--}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}
@section ('title', 'Electronic Payment Suite (EPS) - Contactos'. ' | ' . 'Tipos')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-cog"></i> Tipos</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-cog"></i> Configuracion</a></li>
        <li class="breadcrumb-item"><a href="#">Contactos</a></li>
        <li class="breadcrumb-item active">Tipos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">{{-- token para borrar --}}

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools">
            @if (!Parametro::sistemaIniciado())
              @if (auth()->user()->hasPermission(['00-06-02-XX-02']))
                <a title="Crear Tipo de contacto" class="btn btn-sm btn-success" href="{{route('Tipos.create')}}"><i class="fa fa-plus"></i> Crear tipo de contacto</a>
              @endif
            @endif
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="TiposContactos-table" class="table table-striped table-sm">
              <thead class="thead-default">
                <tr>
                  <th><center>Tipo de contacto</center></th>
                  <th><center>Estado</center></th>
                  <th><center>Acciones</center></th>
                </tr>
              </thead>
              @foreach ($tiposContactos as $tipoContacto)
                <?php $nombre = str_replace(' ','&nbsp;', $tipoContacto->nombre) ?>
                <tr>
                  <td>{{ $tipoContacto->nombre }}</td>
                  <td><center>{{ $tipoContacto->estatus }}</center></td>
                  <td>
                    @if (!Parametro::sistemaIniciado())
                    <center>
                      @if (auth()->user()->hasPermission(['00-06-02-XX-03']))
                        <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary" href="{{route('Tipos.edit',$tipoContacto->id)}}"><i class="fa fa-pencil"></i></a>
                      @endif
                      @if (auth()->user()->hasPermission(['00-06-02-XX-04']))
                        <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn accion btn-danger" href="#" OnClick="Eliminar('{{$tipoContacto->id}}','{{$nombre}}')"><i class="fa fa-trash"></i></a>
                      @endif
                    </center>
                    @endif
                  </td>
                </tr>
              @endforeach
            </table>
          </div><!--table-responsive-->
        </div>
      </div>
      <!-- /.card -->
    </div>
  </div>

@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_tipos_contactos')
@stop
