{{-- @Nombre del programa: Vista Principal co->contactos->Participantes --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Participantes</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i> Configuración</li>
        <li class="breadcrumb-item active">Contactos</li>
        <li class="breadcrumb-item active">Participantes</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">{{-- token para borrar --}}
<!-- row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Filtros
          </h3>
           <div class="card-tools">
              @if (!Parametro::sistemaIniciado())
              @if (auth()->user()->hasPermission(['00-06-01-XX-02']))
              <a title="Crear Contacto Participantes" class="btn btn-sm btn-success" href="{{route('Participantes.create')}}"><i class="fa fa-plus"></i> Crear contacto participantes</a>
              @endif
              @endif
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          <form role="form">
            <div class="row">
              <div class="col-md-8">
                <div class="form-inline">
                   {!!Form::label('Participante', null, array('class' => 'col-sm-2 control-label'))!!}
                  <div class="col-sm-8">
                   {!!Form::select('cod_banco',$bancos,$cod_banco,['id'=>'cod_banco','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-inline">
                  {!!Form::label('Nivel', null, array('class' => 'col-sm-2 control-label'))!!}
                  <div class="col-sm-8">
                   {!!Form::select('cod_nivel',$nivel,$cod_nivel,['id'=>'cod_nivel','class'=>'form-control']) !!}
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="card-footer">
          <div class="col-sm-12">
              <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>


@if ($contactosBancarios!=null)
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Listado
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          <br>
          @if (count($contactosBancarios) != null)
              <?php
              $count = 0
              ?>
              @foreach ($contactosBancarios as $contactoBancario)
              <?php
              $nombre = str_replace(' ','&nbsp;', $contactoBancario->nombre)
              ?>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <center>
                <div class="card mb-3" style="max-width: 60rem; text-align: left;">
                  <div class="card-header" style="background-color: #F0F0F0;">{{ $contactoBancario->nombre . ' - PERSONAL ' . $contactoBancario->tipo_contacto . ' - ' . $contactoBancario->posicion }} 
                  <div class="pull-right">
                    <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn btn-info accion" href="{{route('Participantes.show',$contactoBancario->id)}}"><i class="fa fa-eye text-white"></i></a>
                    @if (!Parametro::sistemaIniciado())
                      @if (auth()->user()->hasPermission(['00-06-01-XX-03']))
                      <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-primary accion" href="{{route('Participantes.edit',$contactoBancario->id)}}"><i class="fa fa-pencil text-white"></i></a>
                      @endif
                      @if (auth()->user()->hasPermission(['00-06-01-XX-04']))
                      <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn btn-danger accion" href="#" OnClick="Eliminar('{{$contactoBancario->id}}','{{$nombre}}')"><i class="fa fa-trash text-white"></i></a>
                      @endif
                    @endif
                  </div>
                  </div>
                    <div id="collapse{{$count}}" class="collapse show" role="tabpanel" aria-labelledby="heading{{$count}}">
                      <div class="card-body">
                      <div class="form-group row">
                        <div class="col-sm-3">
                          <p><strong>Teléfonos</strong></p>
                          <p class="text-muted">{{ $contactoBancario->telefono_celular}} / {{ $contactoBancario->telefono_local}}</p>
                        </div>
                        <div class="col-sm-3">
                          <p><strong>Correo electrónico</strong></p>
                          <p class="text-muted">{{ $contactoBancario->email }}</p>
                        </div>
                        <div class="col-sm-2">
                          <p><strong>Participante</strong></p>
                          <p class="text-muted">{{ $contactoBancario->codigo }}</p>
                        </div>
                        <div class="col-sm-2">
                          <p><strong>Turno</strong></p>
                          <p class="text-muted">{{ $contactoBancario->turno }}</p>
                        </div>
                        @if ($contactoBancario->estatus == 'INACTIVO')
                        <div class="col-sm-2">
                          <p><strong>Estado</strong></p>
                          <label class="label label-danger">{{ $contactoBancario->estatus }}</label>
                        </div>
                        @elseif ($contactoBancario->estatus == 'ACTIVO')
                        <div class="col-sm-2">
                          <p><strong>Estado</strong></p>
                          <span class="text-white badge badge-success"  style="font-size: 13px;">{{ $contactoBancario->estatus }}</span>
                        </div>
                        @endif
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                </center> 
              </div>   
                  <div class="panel-heading" role="tab" id="heading{{$count}}">
                    <h4 class="panel-title">
                      <!--<a role="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$count}}" aria-expanded="true" aria-controls="collapse{{$count}}"></a>-->
                    </h4>
                  </div>
                  <?php
                    $count++
                  ?>
              @endforeach
              @else
              <div>
              <p class="text-center">No se encontraron resultados</p>
              @endif
              </div>
        </div>
       </div>
    </div>
  </div>
@endif

@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_contactos_participantes')
@stop
