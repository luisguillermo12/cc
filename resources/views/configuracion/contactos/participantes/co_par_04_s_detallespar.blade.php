{{-- @Nombre del programa: Vista Principal co->contactos->Participantes(Detalles) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Participantes</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i><a href="{{url('Configuracion/Contactos/Participantes')}}">  Configuración</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Contactos/Participantes')}}">Contactos</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Contactos/Participantes')}}">Participantes</a></li>
        <li class="breadcrumb-item active">Detalle</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
 <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Detalle&nbsp;&nbsp;{{ $contactoBancario->banco }}
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          <div class="form-group row">

            <div class="col-sm-6">

              <div class="col-sm-8">
                  <p><strong>Participante</strong></p>
                  <p class="text-muted">{{ $contactoBancario->banco }}</p>
              </div>
              <div class="col-sm-6">
                <p><strong>Estado</strong></p>
                <p class="text-muted"><span class="text-white badge badge-primary"  style="font-size: 13px;">{{ $contactoBancario->estatus }}</span></p>
              </div>
              <div class="col-sm-6">
                  <p><strong>Cédula</strong></p>
                  <p class="text-muted">{{ number_format($contactoBancario->cedula, 0, '.', '.') }}</p>
              </div>
              <div class="col-sm-6">
                  <p><strong>Nombre y apellido</strong></p>
                  <p class="text-muted">{{ $contactoBancario->nombre }}</p>
              </div>
              <div class="col-sm-6">
                    <p><strong>Turno</strong></p>
                    <p class="text-muted">{{ $contactoBancario->turno }}</p>
              </div>

            </div>



            <div class="col-sm-6">
              <div class="col-sm-6">
                  <p><strong>Cargo</strong></p>
                  <p class="text-muted">{{ $contactoBancario->cargo }}</p>
              </div>
              <div class="col-sm-6">
                  <p><strong>Teléfonos</strong></p>
                  <p class="text-muted">{{ $contactoBancario->telefono_celular}} / {{ $contactoBancario->telefono_local}}</p>
              </div>
              <div class="col-sm-6">
                    <p><strong>Correo electrónico</strong></p>
                    <p class="text-muted">{{ $contactoBancario->email }}</p>
              </div>
              <div class="col-sm-6">
                  <p><strong>Tipo de contacto</strong></p>
                  <p class="text-muted">{{ $contactoBancario->tipo_contacto }}</p>
              </div>
              <div class="col-sm-6">
                    <p><strong>Escalamiento</strong></p>
                    <p class="text-muted">{{ $contactoBancario->posicion }}</p>
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
  </div>
@stop

{{--Fin--}}

@section('after-scripts-end')

@stop
