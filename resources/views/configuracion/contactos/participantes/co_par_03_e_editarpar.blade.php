{{-- @Nombre del programa: Vista Principal co->contactos->Participantes(Editar) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Participantes</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i><a href="{{url('Configuracion/Contactos/Participantes')}}">  Configuración</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Contactos/Participantes')}}">Contactos</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/Contactos/Participantes')}}">Participantes</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Editar</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body offset-sm-1">
        {{ Form::model($contacto_bancario, ['route' => ['Participantes.update', $contacto_bancario], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row col-md-12">
            {!!Form::label('Participante(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
               {!! Form::select('banco_id',$bancos,null,['id'=>'banco_id','class'=>'form-control']) !!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!!Form::label('Cédula(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!!Form::text('cedula',null,['id'=>'cedula','class'=>'form-control mayusculas','placeholder'=>'Ingrese Cédula','readonly'=>'readonly','maxlength'=>9])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!!Form::label('Nombre y apellido(*)', 'Nombre y apellido(*)', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre y Apellido','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!!Form::label('Cargo(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
            {!!Form::text('cargo',null,['id'=>'cargo','class'=>'form-control mayusculas','placeholder'=>'Ingrese Cargo','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
              {!! Form::label('Teléfono celular(*)', 'Teléfono celular(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
             {!!Form::text('telefono_celular',null,['id'=>'telefono_celular','class'=>'form-control mayusculas','placeholder'=>'Ingrese Teléfono Celular','maxlength'=>16, 'data-inputmask'=>'"mask": "(9999) 999-9999"', 'data-mask'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
             {!! Form::label('Teléfono local(*)', 'Teléfono local(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('telefono_local',null,['id'=>'telefono_local','class'=>'form-control mayusculas','placeholder'=>'Ingrese Teléfono Local','maxlength'=>16, 'data-inputmask'=>'"mask": "(9999) 999-9999"', 'data-mask'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
             {!! Form::label('Correo electrónico(*)', 'Correo electrónico(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::email('email',null,['id'=>'email','class'=>'form-control mayusculas','placeholder'=>'Ingrese E-mail','maxlength'=>100])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Tipo de contacto(*)', 'Tipo de contacto(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
             {!! Form::select('tipo_contacto_id',$tipos_contactos,null,['id'=>'tipo_contacto_id','class'=>'form-control']) !!}
            </div>
          </div>

          <div class="form-group row col-md-12">
           {!! Form::label('Turno(*)', 'Turno(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
             {!! Form::select('turno_id',$list_turnos,null,['id'=>'turno_id','class'=>'form-control']) !!}
            </div>
          </div>

          <div class="form-group row col-md-12">
           {!! Form::label('Posición de escalamiento(*)', 'Posición de escalamiento(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
            {!! Form::select('posicion_escalamiento_id',$list_posicion,null,['id'=>'posicion_escalamiento_id','class'=>'form-control']) !!}
            </div>
          </div>

          <div class="form-group row col-md-12">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>

        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarContactoBancario','disabled']) }}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
    </div>
    </div>
    </div>
  </div>    
@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_contactos_participantes')
@stop
