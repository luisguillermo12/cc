{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Validaciones de la solución</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i> Configuración</li>
        <li class="breadcrumb-item active">Administración de tablas</li>
        <li class="breadcrumb-item active">Validaciones de la solución</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Filtros
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
              <div class="col-md-4">
              <form role="form" class="form-horizontal">
                <div class="form-inline">
                 Código interno
                  <div class="col-sm-8">
                    {!!Form::text('codit',null,['id'=>'codit','class'=>'form-control','placeholder'=>'Ingrese codigo','maxlength'=>3])!!}
                  </div>
                </div>
              </div>
                <div class="col-md-4 ">
                  <div class="form-inline">
                    Código de respuesta
                    <div class="col-sm-4">
                       {!!Form::text('codrt',null,['id'=>'codrt','class'=>'form-control','placeholder'=>'Ingrese codigo','maxlength'=>3])!!}
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-inline">
                    <div class="col-md-4">
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nivel
                     </div>
                      <div class="col-sm-3">
                         <select class="form-control" id="nivel" name="nivel">
                            <option value="">*</option> 
                            <option value="0">0</option> 
                            <option value="1">1</option> 
                            <option value="2">2</option> 
                            <option value="3">3</option> 
                          </select> 
                      </div>
                  </div>
                </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-default btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">
            Listado
          </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-striped table-sm">
            <thead style="background-color: #c1e7fc">
                <th><center>Id</center></th>
                <th><center>Código Interno</center></th>
                <th><center>Nivel</center></th>
                <th><center>Respuesta</center></th>
                <th><center>Código Respuesta </center></th>
                <th><center>Descripción</center></th>
                <th><center>Causa</center></th>
                <th><center>Archivos aplica</center></th>
                <th><center>Acción</center></th>
            </thead>
            <tbody>
             @foreach ($validaciones as $registro) 
                  <tr>
                    <td><center>{{ $registro->id }}</center></td>
                    <td nowrap><center>{{ $registro->codigo_interno_error }}</center></td>
                    <td><center>{{ $registro->nivel }}</center></td>
                    <td><justify>{{ $registro->respuesta_accion }}</justify></td>
                    <td><center>{{ $registro->respuesta_codigo }}</center></td>
                    <td><justify>{{ $registro->codigo_descripcion  }}</justify></td>
                    <td><justify>{{ $registro->causa  }}</justify></td>
                    <td><justify>{{ $registro->archivos_aplica  }}</justify></td>
                    <td> 
                    @if (!Parametro::sistemaIniciado())
                     <center>
                       @if(auth()->user()->hasPermission(['00-09-02-XX-03']))
                       <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-primary accion" href="{{route('ValidacionesSolucion.edit',$registro->id)}}"><i class="fa fa-pencil"></i></a>
                       @endif
                     </center>
                     @endif 
                  </td>
                 </tr>
                @endforeach 
            </tbody>
            </table>  
            <br>
             <div class="col-sm-12">
                <div class="pull-right">
                  {{ $validaciones->links() }}
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

  @stop

  @section('after-scripts-end')
   @include('includes.scripts.configuracion.scripts_validaciones_solucion')

  @stop
