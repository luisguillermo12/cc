@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> Validaciones de la solución</h1>
    </div>
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i> Configuración</li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/AdministracionTablas/ValidacionesSolucion')}}">Administración de tablas</a></li>
        <li class="breadcrumb-item active"><a href="{{url('Configuracion/AdministracionTablas/ValidacionesSolucion')}}">Validaciones de la solución</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-header card-warning card-outline">
          <h3 class="card-title">Editar</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="card-body">
          {{ Form::model($validacion, ['route' => ['ValidacionesSolucion.update', $validacion], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>
          <div class="form-group row col-md-12">
           {!! Form::label('ID(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('id',null,['id'=>'id','class'=>'form-control' ,'readonly'=>'readonly' ])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Código interno(*)', 'Código interno(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('codigo_interno_error',null,['id'=>'codigo_interno_error','class'=>'form-control','readonly'=>'readonly','placeholder'=>'Ingrese cod interno', 'maxlength' => '3'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Código interno(*)', 'Código interno(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('codigo_interno_error',null,['id'=>'codigo_interno_error','class'=>'form-control','readonly'=>'readonly','placeholder'=>'Ingrese cod interno', 'maxlength' => '3'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Nivel(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
            {!!Form::text('nivel',null,['id'=>'nivel','class'=>'form-control','placeholder'=>'Ingrese nivel', 'maxlength' => '1', 'readonly'=>'readonly'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
             {!! Form::label('Respuesta(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
             {!!Form::text('respuesta_accion',null,['id'=>'respuesta_accion','class'=>'form-control','placeholder'=>'Ingrese respuesta', 'maxlength' => '99' , 'readonly'=>'readonly'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
             {!! Form::label('Código respuesta (*)', 'Código respuesta (*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('respuesta_codigo',null,['id'=>'respuesta_codigo','class'=>'form-control','placeholder'=>'Ingrese codigo de respuesta', 'maxlength' => '3'])!!}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!!Form::label('Descripción código', 'Descripción código', array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              {{ Form::textarea('codigo_descripcion', null, ['class' => 'form-control col-sm-12', 'maxlength' => '99', 'rows' => "2"]) }}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!!Form::label(' Causa.', null, array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
            {{ Form::textarea('causa', null, ['class' => 'form-control col-sm-12', 'maxlength' => '199', 'rows' => "2"]) }}
            </div>
          </div>

          <div class="form-group row col-md-12">
            {!! Form::label('Archivos aplica(*)', 'Archivos aplica(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::text('archivos_aplica',null,['id'=>'archivos_aplica','class'=>'form-control','placeholder'=>'Ingrese nivel', 'maxlength' => '99'])!!}
            </div>
          </div>

        </div>
        <!-- /.card-body-->
        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarValidacionSistema']) }}
            </div>
          </div>
        </div>
        {{ Form::close() }}
      <!-- /.card -->
      </div>
    </div>
  </div>
</div>    
@stop

@section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_validaciones_solucion')
@stop
