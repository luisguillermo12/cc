{{-- @Nombre del programa: --}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-cog"></i>  Parámetros de la solución</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa fa-cog"></i> Configuración</li>
        <li class="breadcrumb-item"> Administración de tablas</li>
        <li class="breadcrumb-item active"> Parámetros de la solución</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Filtros</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <form role="form" class="form-horizontal">
            <div class="col-md-8">
              <div class="form-group form-inline pull-right">
                <label for="inputEmail3" class="control-label">Id</label>
                <div class="col-md-8">
                  {!!Form::text('idt',null,['id'=>'rifselecemp','class'=>'form-control','placeholder'=>'Ingrese id','maxlength'=>3])!!}
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                </div>
              </div> 
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table id="EmpresasOrdenantes" class="table table-striped table-sm">
            <thead>
              <tr style="background-color: #c2e7fc;">
                <th><center>Id</center></th>
                <th><center>Nombre</center></th>
                <th><center>Valor</center></th>
                <th><center>Creado</center></th>
                <th><center>Actualizado</center></th>
                <th><center>Observaciones</center></th>
                <th><center>Acción</center></th>
              </tr>
            </thead>
            @foreach ($parametros as $parametro) 
              <tr>
                <td><center>{{ $parametro->id }}</center></td>
                <td><justify>{{ $parametro->nombre }}</justify></td>
                <td><justify>{{ $parametro->valor }}</justify></td>
                <td nowrap><center>{{ Date::parse($parametro->created_at)->format('d-m-Y h:m:s') }}</center></td>
                <td nowrap><center>{{ Date::parse($parametro->updated_at)->format('d-m-Y h:m:s') }}</center></td>
                <td ><justify>{{ $parametro->comentario }}</justify></td>
                <td> 
                @if (auth()->user()->hasPermission(['00-09-01-XX-03']))
                  
                  @if (!Parametro::sistemaIniciado()) 
                    <center><a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary" href="{{route('ParametrosSolucion.edit',$parametro->id)}}"><i class="fa fa-pencil"></i></a></center>
                  @endif 
                  
                @endif
              </td>
             </tr>
            @endforeach 
          </table>
          <br>
          <div class="col-sm-12">
            <div class="pull-right">
              {!! $parametros->links() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @stop

  @section('after-styles-end')
  <link rel="stylesheet" type="text/css" href="{{asset('css/datatables/datatables.bootstrap4.min.css')}}">
  @endsection

  @section('after-scripts-end')
    <script type="text/javascript" src="{{asset('js/datatables/jquery.datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/datatables/datatables.bootstrap4.min.js')}}"></script>
    @include('includes.scripts.configuracion.scripts_parametros_sistema')
  @stop
