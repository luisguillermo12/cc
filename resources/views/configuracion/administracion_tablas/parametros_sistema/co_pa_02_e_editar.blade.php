@extends ('layouts.master')

@section ('title', 'Electronic Payment Suite (EPS) - ParametrosSolucion'. ' | ' . 'Editar')

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-5">
      <h1><i class="fa fa fa-cog"></i>  Parámetros de la solución</h1>
    </div>
    <div class="col-sm-7">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/AdministracionTablas/ParametrosSolucion')}}"><i class="fa fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/AdministracionTablas/ParametrosSolucion')}}"> Administración de tablas</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/AdministracionTablas/ParametrosSolucion')}}"> Parámetros de la solución</a></li>
        <li class="breadcrumb-item active"> Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
  {{ Form::model($parametro_sistema, ['route' => ['ParametrosSolucion.update', $parametro_sistema], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Editar</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">

          <div class="form-group row">
            <div class="col-md-7 offset-md-2">
              @include('includes.messages')
            </div>
          </div>

          <div class="row col-md-12 form-horizontal text-center">

            <div class="col-md-12 text-center">
              <span>Los campos marcados con un asterisco (*) son obligatorios.</span><p></p>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('ID(*)', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text('id',null,['id'=>'id','class'=>'form-control' ,'readonly'=>'readonly' ])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('Nombre(*)', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese Nombre', 'maxlength' => '99'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('Valor(*)', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {!!Form::text('valor',null,['id'=>'valor','class'=>'form-control','placeholder'=>'Ingrese Valor', 'maxlength' => '199'])!!}
              </div>
            </div>

            <div class="form-group row col-md-12">
              {!! Form::label('Valor(*)', null, array('class' => 'col-md-3 col-form-label')) !!}
              <div class="col-md-6">
                {{ Form::textarea('comentario', null, ['class' => 'form-control', 'maxlength' => '199', 'rows' => "4"]) }}
              </div>
            </div>

          </div>
        </div>
        <!-- /.card-body-->
        <div class="card-footer text-center">
          {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarParametroSistema']) }}
          {{ link_to_route('ParametrosSolucion.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title' => 'Cancelar']) }}
        </div>
        {{ Form::close() }}
      </div>
      <!-- /.card -->
    </div>
  </div>
       
@stop

@section('after-scripts-end')
  @include('includes.scripts.configuracion.scripts_parametros_sistema')
@stop
