{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Sub-Productos(Editar) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i>Sub-productos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/SubProductos')}}"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/SubProductos')}}">Medios de pagos</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/SubProductos')}}">Sub-roductos</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Editar</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::model($subproducto, ['route' => ['SubProductos.update', $subproducto], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
        {!!Form::hidden('id',null,['id'=>'id','class'=>'form-control'])!!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span><p></p>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Producto(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!! Form::select('producto_id',$productos,null,['id'=>'producto_id','class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Código(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('codigo',null,['id'=>'codigo','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código','maxlength'=>3, 'readonly'=>'readonly'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombre(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id' => 'actualizarSubProducto','disabled']) }}
            </div>
          </div>
        </div>

        <div class="clearfix"></div>

        {{ Form::close() }}
      </div><!-- /.box-body -->
    </div><!--box-->
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_sub_productos')
@stop
