{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Sub-Productos --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i>Sub-productos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item">Medios de pagos</li>
        <li class="breadcrumb-item active">Sub-roductos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if (!Parametro::sistemaIniciado() && auth()->user()->hasPermission(['00-03-02-XX-02']))
          <a title="Crear participante" class="btn btn-sm btn-success" href="{{route('SubProductos.create')}}"><i class="fa fa-plus"></i> Crear participante</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <div class="col-sm-12">
          <table id="TiposOperaciones-table" class="table table-bordered table-striped table-hover table-sm">
            <thead>
              <tr class ="theadtable">
                <th><center>Código</center></th>
                <th><center>Sub-producto</center></th>
                <th><center>Producto</center></th>
                <th><center>Estado</center></th>
                <th><center>Acciones</center></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($subproductos as $subproducto)
              <tr>
                <td><center>{{ $subproducto->codigo }}</center></td>
                <td nowrap>{{ $subproducto->nombre }}</td>
                <td nowrap>{{ $subproducto->producto }}</td>
                <td><center>{{ $subproducto->estatus }}</center></td>
                <td>
                  @if (!Parametro::sistemaIniciado())
                  <center>
                    @if(auth()->user()->hasPermission(['00-03-02-XX-03']))
                      <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary" href="{{route('SubProductos.edit',$subproducto->id)}}"><i class="fa fa-pencil"></i></a>
                    @endif
                    @if ($subproducto->estatus != 'ACTIVO' && auth()->user()->hasPermission(['00-03-02-XX-04'])) 
                      <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn accion btn-danger" href="#" OnClick="Eliminar('{{$subproducto->id}}','{{ $subproducto->nombre }}')"><i class="fa fa-trash"></i></a>
                    @endif
                  </center>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <br>
          <div class="col-sm-12">
            <div class="pull-right">
              {!! $subproductos->links() !!}
            </div>
          </div>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_sub_productos')
@stop
