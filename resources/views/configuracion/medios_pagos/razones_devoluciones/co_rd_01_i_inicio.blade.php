{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Sub-Razones de Devolucion --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i>Razones de devolución</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item">Medios de pagos</li>
        <li class="breadcrumb-item">Razones de devolución</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Filtros</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open([null,'method'=>'GET', 'class' => 'form-horizontal', 'role' =>'form']) !!}

        <div class="row">

          <div class="col-sm-6">
            <div class="form-group row">
              {!!Form::label('Producto', null, array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
                {!! Form::select('producto', $operaciones, $operacion, ['id'=>'tipo_operacion_id','class'=>'form-control']) !!}
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group row">
              {!!Form::label('código', null, array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
                {!! Form::select('codigo', $codigos, $codigo, ['id'=>'tipo_operacion_id','class'=>'form-control']) !!}
              </div>
            </div>
          </div>

        </div>
        <div class="row">

          <div class="col-sm-6">
            <div class="form-group row">
              {!!Form::label('estado', null, array('class' => 'col-sm-3 col-form-label'))!!}
              <div class="col-sm-6">
                {!! Form::select('estatus', ['' => '*', 'ACTIVO' => 'Activo', 'INACTIVO' => 'Inactivo'], $estatus, ['id'=>'tipo_operacion_id','class'=>'form-control']) !!}
              </div>
            </div>
          </div>

        </div>

        <div class="col-sm-12">
          <div class="pull-right">
            <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
          </div>
        </div>


        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if (!Parametro::sistemaIniciado() && auth()->user()->hasPermission(['00-03-03-XX-02']))
          <a title="Crear participante" class="btn btn-sm btn-success" href="{{route('RazonesDevolucion.create')}}"><i class="fa fa-plus"></i> Crear participante</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <table id="RazonesDevolucion-table" class="table table-bordered table-striped table-hover table-sm">
          <thead>
            <tr class ="theadtable">
              <th class="text-center">Código</th>
              <th class="text-center">Razón de devolución</th>
              <th class="text-center">Producto</th>
              <th class="text-center">Estado</th>
              <th class="text-center">Acciones</th>
            </tr>
          </thead>
          @foreach ($tipos_devoluciones as $tipoDevolucion)
          <tr>
            <td><center>{{ $tipoDevolucion->codigo }}</center></td>
            <td nowrap>{{ $tipoDevolucion->nombre }}</td>
            <td nowrap>{{ $tipoDevolucion->producto }}</td>
            <td><center>{{ $tipoDevolucion->estatus }}</center></td>
            <td>
              @if (!Parametro::sistemaIniciado())
              <center>
                @if (auth()->user()->hasPermission(['00-03-03-XX-03']))
                  <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary" href="{{route('RazonesDevolucion.edit',$tipoDevolucion->id)}}"><i class="fa fa-pencil"></i></a>
                @endif
                @if ($tipoDevolucion->estatus != 'ACTIVO' && auth()->user()->hasPermission(['00-03-03-XX-04']))
                  <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn accion btn-danger" href="#" OnClick="Eliminar('{{$tipoDevolucion->id}}','{{$tipoDevolucion->nombre}}')"><i class="fa fa-trash"></i></a>
                @endif
              </center>
              @endif
            </td>
          </tr>
          @endforeach
        </table>
        <br>
        <div class="col-sm-12">
          <div class="pull-right">
            @if($tipos_devoluciones->count()>=10)
            {!! $tipos_devoluciones->links() !!}
            @endif
          </div>
        </div>
      </div><!--table-responsive-->
    </div>
  </div>
</div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_razones_devolucion')
@stop
