{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Productos --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 14/06/2018 --}}
{{-- @Modificado por: Deivi Peña --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Productos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item">Medios de pagos</li>
        <li class="breadcrumb-item active">Productos</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Filtros</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {!! Form::open([null,'method'=>'GET', 'class' => 'form-horizontal', 'role' =>'form']) !!}

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group row">
              {!!Form::label('estatus', 'Estado', array('class' => 'col-sm-4 col-form-label'))!!}
              <div class="col-sm-8">
                {!!Form::select('estatus', ['' => '*', 'ACTIVO' => 'Activo', 'INACTIVO' => 'Inactivo'] , $estatus, ['class'=>'form-control']) !!}
              </div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group row">
              {!!Form::label('codigo', 'Código', array('class' => 'col-sm-4 col-form-label'))!!}
              <div class="col-sm-8">
                {!!Form::select('codigo', $codigos , $codigo, ['class'=>'form-control']) !!}
              </div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group row">
              {!!Form::label('producto', 'Producto', array('class' => 'col-sm-4 col-form-label'))!!}
              <div class="col-sm-8">
                {!!Form::select('producto', $listadoProductos , $producto, ['class'=>'form-control']) !!}
              </div>
            </div>
          </div>
        </div>

       

        <div class="col-sm-12">
          <div class="pull-right">
            <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
          </div>
        </div>

        {!! Form::close() !!}
      </div>
    </div>
  </div>

  <div class="col-12">
    <div class="card">
      <div class="card-header card-primary card-outline">
        <h3 class="card-title">Crear</h3>
        <div class="card-tools">
          @if (!Parametro::sistemaIniciado() && auth()->user()->hasPermission(['00-03-02-XX-02']))
            <a title="Crear Producto" class="btn btn-sm btn-success" href="{{route('Productos.create')}}"><i class="fa fa-plus"></i> Crear producto</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <div class="col-sm-12">
          <div class="table-responsive">
            <table id="TiposOperaciones-table" class="table table-bordered table-striped table-hover table-sm">
              <thead>
                <tr class ="theadtable">
                  <th class="text-center">Producto</th>
                  <th class="text-center">Código producto</th>
                  <th class="text-center">Código presentada</th>
                  <th class="text-center">Código devuelta</th>
                  <th class="text-center">Signo</th>
                  <th class="text-center">Estado</th>
                  <th class="text-center">Acciones</th>
                </tr>
              </thead>
              @foreach ($productos as $producto => $tipo_operacion)
              {{--*/ @$nombre = str_replace(' ','&nbsp;', $producto->producto) /*--}}
              <tr>
                <td nowrap>{{ $producto }}</td>
                <td class="text-center">{{ $tipo_operacion[0]->codigo }}</td>
                <td class="text-center">{{ $tipo_operacion[0]->codigo_intervalo }}</td>
                <td class="text-center">{{ $tipo_operacion[1]->codigo_intervalo }}</td>
                @if ($tipo_operacion[0]->signo =='C')
                <td class="text-center">DÉBITO</td>
                @else
                <td class="text-center">CRÉDITO</td>
                @endif
                <td class="text-center">{{ $tipo_operacion[0]->estatus }}</td>
                <td class="text-center">
                  @if (!Parametro::sistemaIniciado() && auth()->user()->hasPermission(['00-03-02-XX-03']))
                    <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary" href="{{route('Productos.edit', $tipo_operacion[0]->id)}}"><i class="fa fa-pencil"></i></a>
                  @endif
                </td>
              </tr>
              @endforeach
            </table>
          </div><!--table-responsive-->
        </div>
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_tipos_operaciones')
@stop
