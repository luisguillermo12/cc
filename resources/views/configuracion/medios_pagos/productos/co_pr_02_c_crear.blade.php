{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Productos(Crear) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Productos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/Productos')}}"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/Productos')}}">Medios de pagos</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/Productos')}}">Productos</a></li>
        <li class="breadcrumb-item active">Crear</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Crear</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::open(['route' => 'Productos.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) }}

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span><p></p>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombre del producto(*)', 'Nombre del producto(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Límite Financiero(*)', 'Límite financiero(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('limite_financiero',null,['id'=>'limite_financiero','class'=>'form-control mayusculas','placeholder'=>'Ingrese Límite Financiero','maxlength'=>16])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Código producto(*)', 'Código producto(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('codigo',null,['id'=>'codigo','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código','maxlength'=>3])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Código Presentada(*)', 'Código presentada(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('codigo_presentada',null,['id'=>'codigo_presentada','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código','maxlength'=>3])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Código Devuelta(*)', 'Código devuelta(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('codigo_devuelta',null,['id'=>'codigo_devuelta','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código','maxlength'=>3])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Signo(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::select('signo', array('0' => 'Seleccione Signo','C' => 'DB', 'D' => 'CR'),null,['id'=>'signo','class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              {{ Form::radio('estatus', 'ACTIVO') }} Activo
            </label>
            <label class="radio-inline">
              {{ Form::radio('estatus', 'INACTIVO') }} Inactivo
            </label>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title' => 'Guardar']) }}
            </div>
          </div>
        </div>

        <div class="clearfix"></div>

        {{ Form::close() }}
      </div><!-- /.box-body -->
    </div><!--box-->
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_productos')
@stop
