{{-- @Nombre del programa: Vista Principal co->Medios de pagos->Productos(Editar) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Productos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/Productos')}}""><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/Productos')}}">Medios de pagos</a></li>
        <li class="breadcrumb-item"><a href="{{url('Configuracion/MediosPagos/Productos')}}">Productos</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Editar</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::model($producto, ['route' => ['Productos.update', $producto], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id' => 'form-edit']) }}

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

        {!!Form::hidden('id',null,['id'=>'id','class'=>'form-control'])!!}

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>

        <div class="form-group row">
          {!! Form::hidden('id Producto(*)', 'id producto(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::hidden('producto_id',null,['id'=>'producto_id','class'=>'form-control','placeholder'=>'Ingrese Código','maxlength'=>3])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Nombre del Producto(*)', 'Nombre del producto(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre_producto','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre','maxlength'=>50,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Límite Financiero(*)', 'Límite financiero(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::text('limite_financiero',null,['id'=>'limite_financiero','class'=>'form-control mayusculas','placeholder'=>'Ingrese Límite Financiero','maxlength'=>16,'onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Signo(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::select('signo', array('0' => 'Seleccione Signo','C' => 'DB', 'D' => 'CR'),null,['id'=>'signo','class'=>'form-control']) !!}
          </div>
        </div>

        <div class="form-group row">
          {!! Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">{{ Form::radio('estatus', 'ACTIVO') }} Activo</label>
            <label class="radio-inline">{{ Form::radio('estatus', 'INACTIVO') }} Inactivo</label>
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarTipoOperacion','disabled']) }}
            </div><!--pull-left-->
          </div>
        </div>
        
        <div class="clearfix"></div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_productos')
@stop
