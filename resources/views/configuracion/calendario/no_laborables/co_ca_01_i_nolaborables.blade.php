{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa-cog"></i> No laborables</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/DiasExcepcionales/NoLaborables') }}"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/DiasExcepcionales/NoLaborables') }}"><i class="fa fa-cog"></i> Calendario</a></li>
        <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/DiasExcepcionales/NoLaborables') }}"><i class="fa fa-cog"></i> Días excepcionales</a></li>
        <li class="breadcrumb-item active">No laborables</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
  @if (!Parametro::sistemaIniciado())
    @if (auth()->user()->hasPermission(['00-02-02-XX-01']))
    <div class="card card-warning card-outline">
      <div class="card-header with-border">
        <!--codigo gui MOD-CONF-2.3-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-2.3.1" id="codigo_gui">
        <h3 class="card-title">Crear</h3>
        <div class="card-tools pull-right">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </div>
      </div><!-- /.card-header -->
      <!-- form start -->
      <div class="card-body">
        {{ Form::open(['route' => 'NoLaborables.store', 'class' => 'form-row', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) }}

        @include('includes.messages')

        <div class="callout callout-info col-sm-4 offset-sm-4">
          <h4><i class="fa fa-info-circle"></i> Nota:</h4>
          <p>Los campos marcados con un asterisco (*) son obligatorios.</p>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

        <div class="col-sm-12 form-group form-row">
          {!!Form::label('Fecha(*)', null, array('class' => 'col-sm-4 col-form-label'))!!}
          <div class="col-sm-3">
            <div class="input-group date">
              <input type="text" class="form-control group-date" id="fecha" name="fecha" readonly="readonly"/>
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 form-group form-row">
          {!!Form::label('Estado(*)', null, array('class' => 'col-sm-4 col-form-label'))!!}
          <div class="col-sm-4 form-check form-check-inline">
            <label class="radio-inline form-check-label" for="estatus">
              {{ Form::radio('estatus', 'BANCARIO') }} Bancario
            </label>
            <label class="radio-inline form-check-label" for="estatus">
              {{ Form::radio('estatus', 'FERIADO') }} Feriado
            </label>
          </div>
        </div>
      </div>
      @if (auth()->user()->hasPermission(['00-02-02-01-02']))
        <div class="card-footer">
          <div class="card-tools pull-right">
            {{ Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title' => 'Guardar','id'=>'guardarCalendario','disabled']) }}
          </div>
        </div>
      @endif
      {{ Form::close() }}
    </div>
    @endif
  @endif

    <div class="card card-primary card-outline">
      <div class="card-header with-border">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools pull-right">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table id="Calendario-table" class="table table-striped table-hover table-sm">
            <thead class="theadtable">
              <tr>
                <th><center>Fecha</center></th>
                <th><center>Fecha (Detalle)</center></th>
                <th><center>Estado</center></th>
                <th><center>Acciones</center></th>
              </tr>
            </thead>
            @foreach ($calendarios as $calendario)
            <?php $fecha = str_replace(' ','&nbsp;', ucfirst(Date::parse($calendario->fecha)->format('l, j \d\e F \d\e Y'))); ?>
            <?php $estatus = str_replace(' ','&nbsp;', $calendario->estatus); ?>
            <tr>
              <td><center>{{ Date::parse($calendario->fecha)->format('d/m/Y') }}</center></td>
              <td><center>{{ ucfirst(Date::parse($calendario->fecha)->format('l, j \d\e F \d\e Y')) }}</center></td>
              <td><center>{{ $calendario->estatus }}</center></td>
              @if (auth()->user()->hasPermission(['00-02-02-01-04']))
              <td>
                @if (!Parametro::sistemaIniciado())
                <center>
                  <?php
                  $antes=Date::parse($calendario->fecha)->format('ymd');
                  $actual =  Date::now()->format('ymd');
                  ?>
                  @if ($antes<$actual)
                  @else
                  <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm btn-danger" href="#" OnClick="Eliminar('{{$calendario->id}}','{{$fecha}}','{{$estatus}}')"><i class="fa fa-trash"></i></a>
                  @endif
                </center>
                @endif
              </td>
              @endif
            </tr>
            @endforeach
          </table>
        </div><!--table-responsive-->
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
  @include('includes.scripts.configuracion.calendario.scripts_calendario')
@stop
