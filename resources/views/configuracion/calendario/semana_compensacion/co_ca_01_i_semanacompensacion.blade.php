{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-cog"></i> Semana de compensación</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/SemanaCompensacion') }}"><i class="fa fa-cog"></i> Configuración</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/SemanaCompensacion') }}"><i class="fa fa-cog"></i> Calendario</a></li>
          <li class="breadcrumb-item active">Semana de compensación</li>
        </ol>
      </div>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      @if (auth()->user()->hasPermission(['00-02-01-XX-01']))
      <div class="card card-warning card-outline">
        <div class="card-header with-border">
          <!--codigo gui MOD-CONF-2.1-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-2.1.1" id="codigo_gui">
          <h3 class="card-title">Listado</h3>
          <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="SemanaCompensacion-table" class="table table-hover table-bordered table-sm" align="center">
              <thead class="theadtable">
                <tr>
                  <th><center>ID</center></th>
                  <th><center>Día</center></th>
                  <th><center>Estado</center></th>
                  <th><center>Acciones</center></th>
                </tr>
              </thead>
              <tbody>
              @foreach ($semana_compensacion as $semanaCompensacion)
                <?php $nombre = str_replace(' ','&nbsp;', $semanaCompensacion->nombre); ?>
                @if ($semanaCompensacion->estatus == 'ABIERTO')
                <?php $estatus = 'LABORABLE'; ?>
                  <tr class="bg-success-disabled">
                  @else
                    <?php $estatus = 'NO LABORABLE'; ?>
                    <tr class="bg-danger-disabled">
                    @endif
                    <td><center>{{ $semanaCompensacion->id }}</center></td>
                    <td><center>{{ $semanaCompensacion->dia }}</center></td>
                    <td><center>{{ $estatus }}</center></td>
                    <td>
                      <center>
                      @if (!Parametro::sistemaIniciado())
                        @if (auth()->user()->hasPermission(['00-02-01-XX-03']))
                        <a data-toggle="tooltip" data-placement="top" title="Editar"  class="btn-sm btn-primary" href="{{route('SemanaCompensacion.edit',$semanaCompensacion->id)}}"><i class="fa fa-pencil"></i></a>
                        @endif
                      @endif
                      </center>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              </table>
            </div><!--table-responsive-->
          </div>
        </div>
        @endif
      </div>
    </div>
  @stop

  {{--Fin--}}

  @section('after-scripts-end')
    @include('includes.scripts.configuracion.calendario.scripts_semana_compensacion')
  @stop
