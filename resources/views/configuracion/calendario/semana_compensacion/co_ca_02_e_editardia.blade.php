{{-- @Nombre del programa: --}}
{{-- @Funcion: --}}
{{-- @Autor: BTC --}}
{{-- @Fecha Creacion: 24/01/2019 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 24/01/2019 --}}
{{-- @Modificado por: BTC --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><i class="fa fa-cog"></i> Semana de compensación</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/SemanaCompensacion') }}"><i class="fa fa-cog"></i> Configuración</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/SemanaCompensacion') }}"><i class="fa fa-cog"></i> Calendario</a></li>
          <li class="breadcrumb-item"><a href="{{ url('Configuracion/Calendario/SemanaCompensacion') }}"><i class="fa fa-cog"></i> Semana de compensación</a></li>
          <li class="breadcrumb-item active">Editar</li>
        </ol>
      </div>
    </div>
  </div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
        <div class="card card-warning card-outline">
            <div class="card-header with-border">
              <!--codigo gui MOD-CONF-2.1-->
              <input type="hidden" name="codigo_gui" value="MOD-CONF-2.1.2" id="codigo_gui">
                <h3 class="card-title">Editar</h3>
                <div class="card-tools pull-right">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </div>
            </div><!-- /.card-header -->
            <div class="card-body">
              {{ Form::model($semana_compensacion, ['route' => ['SemanaCompensacion.update', $semana_compensacion], 'class' => 'form-row', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}

              @include('includes.messages')

              <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

              {!!Form::hidden('id',null,['id'=>'id','class'=>'form-control'])!!}

              <div class="callout callout-info col-sm-4 offset-sm-4">
                <h4><i class="fa fa-info-circle"></i> Nota:</h4>
                <p>Los campos marcados con un asterisco (*) son obligatorios.</p>
              </div>

              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Día(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6">
                  {!!Form::text('dia',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese Dìa','readonly'=>'readonly'])!!}
                </div>
              </div>
              <div class="col-sm-12 form-group form-row">
                {!! Form::label('Estatus(*)', null, array('class' => 'col-sm-3 col-form-label')) !!}
                <div class="col-sm-6 form-check form-check-inline">
                  <label class="radio-inline form-check-label" for="estatus">
                    {{ Form::radio('estatus', 'ABIERTO') }} Laborable
                  </label>
                  <label class="radio-inline form-check-label" for="estatus">
                    {{ Form::radio('estatus', 'CERRADO') }} No laborable
                  </label>
                </div>
              </div>
            </div>
            @if (auth()->user()->hasPermission(['00-02-01-XX-03']))
              <div class="card-footer">
                <div class="card-tools pull-right">
                    {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarSemanaCompensacion','disabled']) }}
                </div>
              </div>
            @endif
            {{ Form::close() }}
        </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
    @include('includes.scripts.configuracion.calendario.scripts_semana_compensacion')
@stop
