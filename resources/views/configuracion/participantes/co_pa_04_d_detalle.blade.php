{{-- @Nombre del programa: Vista Principal co->Participantes->Inactivo(Detalle) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> {{$seccion}}</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url("Configuracion/Participantes/{$seccion}")}}">Participantes</a></li>
        <li class="breadcrumb-item"><a href="{{url("Configuracion/Participantes/{$seccion}")}}">{{$seccion}}</a></li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Detalle</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <div class="col-sm-12">
          <h4>Datos generales</h4>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <p><strong>Código</strong></p>
            <p class="text-muted">{{ $bancoMostrar->codigo }}</p>
          </div>
          <div class="col-sm-4">
            <p><strong>Participante</strong></p>
            <p class="text-muted">{{ $bancoMostrar->nombre }}</p>
          </div>
          <div class="col-sm-4">
            <p><strong>Estado</strong></p>
            <p class="text-muted"><span class="badge badge-primary">{{ $bancoMostrar->estatus }}</span></p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <p><strong>Liquidación IDT</strong></p>
            <p class="text-muted">{{ $bancoMostrar->liquidacion_idt }}</p>
          </div>
          <div class="col-sm-4">
            <p><strong>Fecha ingreso a cámara</strong></p>
            <p class="text-muted">{{ isset($bancoMostrar->fecha_ingreso) ? $bancoMostrar->fecha_ingreso->format('d-m-Y') : 'No ha ingresado.' }}</p>
          </div>
          <div class="col-sm-4">
            <p><strong>Fecha último cambio</strong></p>
            <p class="text-muted">{{ isset($historico) ? $historico->updated_at->format('d-m-Y') : null }}</p>
          </div>
        </div>
        <hr>
        <div class="col-sm-12">
          <!-- <h4>Modalidad de participación</h4> -->
        </div>
        <div class="col-sm-12">
          <div class="col-sm-4">
            <p><strong>Modalidad de participación</strong></p>
            <p class="text-muted"><span class="badge badge-primary">{{ $bancoMostrar->modalidad_participacion == 1 ? 'DIRECTO' : 'INDIRECTO' }}</span></p>
          </div>
          <div class="col-sm-4">
            <p><strong>Participante representante</strong></p>
            @if (empty($bancoMostrar->banco_representante))
            <p class="text-muted">No Aplica</p>
            @else
            <p class="text-muted">{{ $bancoMostrar->banco_representante }}</p>
            @endif
          </div>
          <div class="col-sm-4">
            <p><strong>Participante principal</strong></p>
            @if (empty($bancoMostrar->banco_principal))
            <p class="text-muted">No Aplica</p>
            @else
            <p class="text-muted">{{ $bancoMostrar->banco_principal }}</p>
            @endif
          </div>
        </div>
        <div class="clearfix"></div>
      </div><!-- /.box-body -->
    </div><!--box-->
    @stop

    {{--Fin--}}

    @section('after-scripts-end')

    @stop