{{-- @Nombre del programa: Vista Principal co->Participantes->Inactivo(Crear) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> Inactivos</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url("Configuracion/Participantes/Inactivos")}}">Participantes</a></li>
        <li class="breadcrumb-item"><a href="{{url("Configuracion/Participantes/Inactivos")}}">Inactivos</a></li>
        <li class="breadcrumb-item active">Crear</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Crear</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::open(['route' => 'Configuracion.Participantes.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) }}
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <h4>Datos generales</h4>
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Código(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!form::text('codigo', null,['id'=>'codigo','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código del Banco','maxlength'=>'4','onchange'=>'javascript:copiar();'])!!}
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Código de liquidación(*)', 'Código de liquidación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!form::text('liquidacion_idt', null,['id'=>'liquidacion_idt','class'=>'form-control mayusculas','placeholder'=>'','maxlength'=>'4', 'readonly'=>'readonly'])!!}
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Nombre(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre del Banco','maxlength'=>'100','onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Estado', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!form::text('estatus','INACTIVO',['class'=>'form-control','disabled'=>'disabled'])!!}
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <h4>Modalidad de participación</h4>
          </div>
        </div>
        <div class="form-group row">
          {!! Form::label('Modalidad de Participación(*)', 'Modalidad de participación(*)', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            <label class="radio-inline">
              <input type="radio" name="modalidad_participacion" value="1" checked="checked" onclick="banco_representante_id.disabled = true" /> Directo
            </label>
            <label class="radio-inline">
              <input type="radio" name="modalidad_participacion" value="0" onclick="banco_representante_id.disabled = false" /> Indirecto
            </label>
          </div>
        </div>
        <div class="form-group row">
          {!! Form::label('Participante Representante', 'Participante representante', array('class' => 'col-sm-3 col-form-label')) !!}
          <div class="col-sm-6">
            {!!Form::select('banco_representante_id',$bancosRepresentantes,null,['id'=>'banco_representante_id','class' => 'form-control','disabled' => 'disabled']) !!}
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <div class="pull-right">
              {{ Form::submit('Guardar', ['class' => 'btn btn-success btn-sm','title' => 'Guardar']) }}
              <!--{{ link_to_route('Configuracion.Participantes.index', 'Cancelar', [], ['class' => 'btn btn-danger btn-sm','title' => 'Cancelar']) }}-->
            </div>
          </div>
        </div>
        <div class="pad margin no-print">
          <div class="bs-callout bs-callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info-circle"></i> Nota:</h4>
            Es importante tomar en cuenta que al crear un Participante este se guarda con estado <strong>INACTIVO</strong>. Para ubicarlo dirijase a la opción <strong>Participantes / Inactivos</strong>.
          </div>
        </div>
        <div class="clearfix"></div>
        {{ Form::close() }}
      </div><!-- /.box-body -->
    </div><!--box-->
    @stop

    {{--Fin--}}

    @section('after-scripts-end')
    @include('includes.scripts.configuracion.scripts_participantes')
    @include('includes.scripts.configuracion.scripts_validacion_fecha')
    @stop
