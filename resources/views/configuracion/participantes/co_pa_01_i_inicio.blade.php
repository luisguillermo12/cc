{{-- @Nombre del programa: Vista Principal co->Participantes->Inactivo --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> {{$seccion}}</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i> Configuración</a></li>
        <li class="breadcrumb-item active">Participantes</li>
        <li class="breadcrumb-item active">{{$seccion}}</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Listado</h3>
        <div class="card-tools">
          @if ($seccion == 'Inactivos' && !Parametro::sistemaIniciado() && auth()->user()->hasPermission(['00-01-01-XX-02']))
            <a title="Crear participante" class="btn btn-sm btn-success" href="{{route('Configuracion.Participantes.create')}}"><i class="fa fa-plus"></i> Crear participante</a>
          @endif
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
          <thead>
            <tr class ="theadtable">
              <th class="text-center">Código</th>
              <th class="text-center">Participante</th>
              <th class="text-center">Fecha de ingreso</th>
              <th class="text-center">Modalidad</th>
              <th class="text-center">Estado</th>
              <th class="text-center">Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($bancos as $banco)
            <tr>
              <td>{{ $banco->codigo }}</td>
              <td>{{ $banco->nombre }}</td>
              <td class="text-center">{{Date::parse($banco->fecha_ingreso)->format('d-m-Y')}}</td>
              <td class="text-center">{{ $banco->modalidad_participacion == '1' ? 'DIRECTO' : 'INDIRECTO' }}</td>
              <td class="text-center">{{ $banco->estatus }}</td>
              <td class="text-center">
                <a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn-sm accion btn-info" href="{{route('Configuracion.Participantes.show',$banco->id)}}"><i class="fa fa-eye"></i></a>

                @if (!Parametro::sistemaIniciado() && $banco->estatus != 'EXCLUIDO')
                  @if (auth()->user()->hasPermission(['00-01-01-XX-03']))
                    <a data-toggle="tooltip" data-placement="top" title="Editar" class="btn accion btn-primary "  href="{{route('Configuracion.Participantes.edit',$banco->id)}}"><i class="fa fa-pencil"></i></a>
                  @endif
                  @if ($banco->estatus == 'INACTIVO' && auth()->user()->hasPermission(['00-01-01-XX-04']))
                    <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn-sm accion btn-danger" href="#" OnClick="Eliminar('{{$banco->id}}','{{$banco->nombre_completo}}')"><i class="fa fa-trash"></i></a>
                  @endif
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <br>
        <div class="col-sm-12">
          <div class="pull-right">
            @if($bancos->count()>=10)
            {!! $bancos->links() !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_participantes')
@stop
