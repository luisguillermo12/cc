{{-- @Nombre del programa: Vista Principal co->Participantes->Inactivo(Editar) --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h4><i class="fa fa-cog" style="font-size: 24px !important;"></i> {{$seccion}}</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-cog"></i>Configuración</a></li>
        <li class="breadcrumb-item"><a href="{{url("Configuracion/Participantes/{$seccion}")}}">Participantes</a></li>
        <li class="breadcrumb-item"><a href="{{url("Configuracion/Participantes/{$seccion}")}}">{{$seccion}}</a></li>
        <li class="breadcrumb-item active">Editar</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-warning card-outline">
        <h3 class="card-title">Detalle</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body">
        {{ Form::model($banco, ['route' => ['Configuracion.Participantes.update', $banco], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'id'=>'form-edit']) }}
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            @include('includes.messages')
          </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        {!!Form::hidden('id',null,['id'=>'id','class'=>'form-control'])!!}
        {!!Form::hidden('fecha_cambio', Date::now()->toDateTimeString() ,['id'=>'fecha_cambio','readonly'=>'readonly'])!!}
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <span id="helpBlock" class="help-block">Los campos marcados con un asterisco (*) son obligatorios.</span>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-6 offset-sm-3">
            <h4>Datos generales</h4>
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Código(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!Form::text('codigo',null,['id'=>'codigo','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código','readonly'=>'readonly','maxlength'=>'6'])!!}
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Código liquidación(*)', 'Código liquidación(*)', array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!Form::text('liquidacion_idt',null,['id'=>'liquidacion_idt','class'=>'form-control mayusculas','placeholder'=>'Ingrese Código de Liquidación','readonly'=>'readonly','maxlength'=>'6'])!!}
          </div>
        </div>
        <div class="form-group row">
          {!!Form::label('Participante (*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
          <div class="col-sm-6">
            {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control mayusculas','placeholder'=>'Ingrese Nombre del Banco','maxlength'=>'100','onKeyUp'=>'this.value=this.value.toUpperCase();'])!!}
          </div>
        </div>

        <div id="estatus_contenedor" class="col-sm-12">
          <div class="form-group row">
            {!!Form::label('Estado(*)', null, array('class' => 'col-sm-3 col-form-label'))!!}
            <div class="col-sm-6">
              @foreach ($cambiarEstatus as $estatus)
              <?php  $disablePrincipales = $estatus == 'FUSIONADO' ? 'disablePrincipales(false)' : 'disablePrincipales(true)'; ?>
              <label class="radio-inline" onclick="{{ $disablePrincipales }}">
                {!! Form::radio('estatus', $estatus, $banco->estatus == $estatus, ['id' => $estatus]).' '.ucfirst(strtolower($estatus)) !!}
              </label>
              @endforeach
            </div>
          </div>

          @if($banco->modalidad_participacion !='0')
          <div class="form-group row">
            {!! Form::label('Participante Principal', 'Participante principal', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::select('banco_principal_id',$bancosPrincipales, $banco->banco_principal_id,['id'=>'banco_principal_id','class' => 'form-control']) !!}
            </div>
          </div>
          @endif
        </div>

        <div id="modalidad_contenedor" class="col-sm-12" >
          @if($banco->estatus !='FUSIONADO')
          <div class="form-group row">
            {!! Form::label('Modalidad de Participación(*)', 'Modalidad de participación(*)', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              <label class="radio-inline" onclick="disableRepresentantes(true)">
                {{ Form::radio('modalidad_participacion', '1', $banco->modalidad_participacion, ['id' => 'modalidad']) }}
                Directo
              </label>
              <label class="radio-inline" onclick="disableRepresentantes(false)">
                {{ Form::radio('modalidad_participacion', '0', $banco->modalidad_participacion) }}
                Indirecto
              </label>
            </div>
          </div>

          <div class="form-group row">
            {!! Form::label('Participante Representante', 'Participante representante', array('class' => 'col-sm-3 col-form-label')) !!}
            <div class="col-sm-6">
              {!!Form::select('banco_representante_id',$bancosRepresentantes,$banco->banco_representante_id,['id'=>'banco_representante_id','class' => 'form-control']) !!}
            </div>
          </div>
          @endif
        </div>

        <div class="form-group row">
          <div class="col-sm-6 col-sm-offset-3">
            <div class="pull-right">
              {{ Form::submit('Actualizar', ['class' => 'btn btn-success btn-sm','title' => 'Actualizar','id'=>'actualizarBanco','disabled']) }}
            </div>
          </div>
        </div>

        {{ Form::close() }}
        <div class="clearfix"></div>
      </div><!-- /.box-body -->
    </div><!--box-->
  </div>
</div>
@stop

{{--Fin--}}

@section('after-scripts-end')
@include('includes.scripts.configuracion.scripts_participantes')
@include('includes.scripts.configuracion.scripts_validacion_fecha')
@stop
