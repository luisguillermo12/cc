<html>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', env('APP_NAME'))</title>
  <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  {!! Html::script('js/sweetalert.min.js') !!}

  <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/epsach.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.googleapis.css') }}">


  @yield('before-styles-end')

  @yield('after-styles-end')

</head>
