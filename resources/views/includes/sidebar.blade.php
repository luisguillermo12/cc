<aside class="main-sidebar sidebar-dark-primary">

  <a href="/" class="brand-link bg-white col-sm-12" style="display: inline-block; height: 57px;">
    <img class="brand-image" src="{{asset('/img/logo_fbs.png')}}">
  </a>

  <div class="sidebar slimScroll">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img @if( Auth::user()->logotipo != null ) src="{{ asset('img/logos/'. Auth::user()->logotipo) }}" @else  src="{{ asset('img/logos/user.png')}}"@endif class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
      </div>
    </div>

    <div class="sidebar-form" style="border:none">
      <p class="text-center" style="margin-bottom: 0; font-size: 12px;"><font color='white'>Caracas, {{ fechaBd()->formatLocalized("%A %d %B %Y") }} </font></p>
      <span id="reloj" style="text-align:center; color:white; width: 100%;display: inline-block;"></span>
      <hr style="background-color: white; opacity: 0.2;">
      <p class="text-center" style="margin-bottom: 0;"><font color="white">Fecha de compensación:</font></p>
      <h4 class="text-center" style="margin-top: 0;"><font color="white">{{ compensacion()->format('d/m/Y') }}</font></h4>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2 mb-sidebar-body">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-header text-uppercase">Menú de navegación</li>

        @include('includes.sidebar.inicio.inicio')

        {{--@include('includes.sidebar.mensajes_informativos.mensajes_informativos')--}}
        @include('includes.sidebar.seguridad.seguridad')
        @include('includes.sidebar.configuracion.configuracion')
        @include('includes.sidebar.portal_participantes.portal_participantes')
        @include('includes.sidebar.perfil_usuario.perfil_usuario')
        {{--   @include('includes.sidebar.procesosespeciales.procesos')--}}
        {{-- @include('includes.sidebar.ejecucion_administrativa.ejecucion_administrativa') --}}
        {{-- @include('includes.sidebar.reportes.reportes') --}}
      </ul>
    </nav>
    <!-- end Sidebar Menu -->
  </div>
</aside>
