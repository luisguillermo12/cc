<footer class="main-footer">
  <strong>Copyright &copy; {{ fechaBD()->format('Y') }} <a href="#">{{ env('APP_NAME') }}</a> {{ version() }}</strong> Todos los derechos reservados.
  <div class="pull-right hidden-xs">
    <i class="fa fa-clock-o"></i> {{ round(microtime(true) - LARAVEL_START, 6) }}
  </div>
</footer>