@if (auth()->user()->hasPermissionModule('mensajes_informativos'))
<li class="nav-item has-treeview {{routeIs(['MensajesInformativos/CCE/*','MensajesInformativos/Participante/*'])}}">
  <a href="#" class="nav-link">
    <i class="nav-icon fa fa-envelope"></i>
    <p>Mensajes informativos<i class="right fa fa-angle-left"></i>
    </p>
  </a>
    <ul class="nav nav-treeview ">
    
@if(auth()->user()->hasPermission(['80-01-01-XX-02','80-01-02-XX-01','80-01-03-XX-01']))
<li class="nav-item has-treeview {{routeIs('MensajesInformativos/CCE/*')}}">
        <a href="#" class="nav-link {{routeIs('MensajesInformativos/CCE/*','active')}}">
          <i class="nav-icon fa fa-envelope"></i>
          <p>CCE<i class="right fa fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item has-treeview">
            @if(auth()->user()->hasPermission(['80-01-01-XX-02']))
              <a href="{{url('MensajesInformativos/CCE/NuevoMensaje')}}" class="nav-link {{routeIs('MensajesInformativos/CCE/NuevoMensaje','active')}}">
                <i class="nav-icon fa fa-envelope"></i>
                <p>Nuevo mensaje</p>
              </a>
            @endif
            @if(auth()->user()->hasPermission(['80-01-02-XX-01']))
              <a href="{{url('MensajesInformativos/CCE/Recibidos')}}" class="nav-link {{routeIs('MensajesInformativos/CCE/Recibidos','active')}}">
                <i class="nav-icon fa fa-envelope"></i>
                <p>Recibidos</p>
              </a>
            @endif
            @if(auth()->user()->hasPermission(['80-01-03-XX-01']))
              <a href="{{url('MensajesInformativos/CCE/Enviados')}}" class="nav-link {{routeIs('MensajesInformativos/CCE/Enviados','active')}}">
                <i class="nav-icon fa fa-envelope"></i>
                <p>Enviados</p>
              </a>
            @endif
        </li>
    </ul>
    </li>
@endif

  
@if(auth()->user()->hasPermission(['80-02-01-XX-01']))
 <li class="nav-item has-treeview {{routeIs('MensajesInformativos/Participante/*')}}">
        <a href="#" class="nav-link {{routeIs('MensajesInformativos/Participante/*','active')}}"><i class="nav-icon fa fa-envelope"></i><p>Participantes<i class="right fa fa-angle-left"></i></p></a>
        <ul class="nav nav-treeview ">
        <li class="nav-item has-treeview">
        @if(auth()->user()->hasPermission(['80-02-01-XX-01']))
        <a href="{{url('MensajesInformativos/Participante/Monitoreo')}}" class="nav-link {{routeIs('MensajesInformativos/Participante/Monitoreo','active')}}"><i class="nav-icon fa fa-envelope"></i><p>Monitoreo</p></a>
        @endif
        </li>
        </ul>
</li>
@endif
  </ul>
</li>
@endif




