@if (auth()->user()->hasPermissionModule('procesos_especiales'))
<li class="nav-item has-treeview {{routeIs('ProcesosEspeciales/*')}}">
  <a href="#" class="nav-link">
    <i class="nav-icon fa fa-book"></i>
    <p>Procesos especiales<i class="right fa fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview ">
    <li class="nav-item has-treeview">
      @if (auth()->user()->hasPermission(['50-01-XX-XX-01','50-01-XX-XX-02','50-01-XX-XX-03']))
        <a href="{{url('ProcesosEspeciales/DayOff')}}" class="nav-link  {{routeIs('ProcesosEspeciales/DayOff','active')}}{{routeIs('ProcesosEspeciales/DayOff/*','active')}}">
          <i class="nav-icon fa fa-book"></i>
          <p>Day off</p>
        </a>
      @endif
      @if (auth()->user()->hasPermission(['50-02-XX-XX-01','50-02-XX-XX-02','R0500-02-03']))
        <a href="{{url('ProcesosEspeciales/CloseDayOff')}}" class="nav-link {{routeIs('ProcesosEspeciales/CloseDayOff','active')}}">
          <i class="nav-icon fa fa-book"></i>
          <p>Close day off</p>
        </a>
      @endif
      @if(auth()->user()->hasPermission(['50-03-01-XX-01','50-03-01-XX-02','50-03-02-XX-01']))
       <li class="nav-item has-treeview {{routeIs(['ProcesosEspeciales/Reproceso/Consulta','ProcesosEspeciales/Reproceso/Simulacion'])}}">
        <a href="#" class="nav-link {{routeIs('ProcesosEspeciales/Reproceso/*','active')}}">
          <i class="nav-icon fa fa-book"></i>
          <p>Reproceso<i class="right fa fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview ">
          <li class="nav-item has-treeview">
            @if(auth()->user()->hasPermission(['50-03-01-XX-01','50-03-01-XX-02']))
              <a href="{{url('ProcesosEspeciales/Reproceso/Consulta')}}" class="nav-link {{routeIs('ProcesosEspeciales/Reproceso/Consulta','active')}}">
                <i class="nav-icon fa fa-book"></i>
                <p>Consulta</p>
              </a>
            @endif
            @if(auth()->user()->hasPermission(['50-03-02-XX-01']))
              <a href="{{url('ProcesosEspeciales/Reproceso/Simulacion')}}" class="nav-link {{routeIs('ProcesosEspeciales/Reproceso/Simulacion','active')}}">
                <i class="nav-icon fa fa-book"></i>
                <p>Simulación</p>
              </a>
            @endif
          </li>
      @endif
        </ul>
      @if(auth()->user()->hasPermission(['50-04-XX-XX-01','50-04-XX-XX-02','50-04-XX-XX-03']))
        <a href="{{url('ProcesosEspeciales/ExclusionParticipante')}}" class="nav-link {{routeIs('ProcesosEspeciales/ExclusionParticipante','active')}}">
          <i class="nav-icon fa fa-book"></i>
          <p>Exclusión participantes</p>
        </a>
      @endif
      </li>
    </li>
  </ul>
</li>
@endif