@if (auth()->user()->hasPermissionModule('configuracion'))
<!-- Configuracion -->
<li class="nav-item has-treeview {{ routeIs('Configuracion/*') }}">
  <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Configuración<i class="right fa fa-angle-left"></i></p></a>
  <ul class="nav nav-treeview">

    <!-- Participantes -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/Participantes/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog" class="nav-link"></i><p>Participantes<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- Participantes Inactivos -->
        <li class="nav-item">
          <a href="{{ url('Configuracion/Participantes/Activos') }}" class="nav-link {{ routeIs(['Configuracion/Participantes/Activos'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Activos</a>
        </li>

        <!-- Participantes Fusionados -->
        <li class="nav-item">
          <a href="{{ url('Configuracion/Participantes/Fusionados') }}" class="nav-link {{ routeIs('Configuracion/Participantes/Fusionados', 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Fusionados</a>
        </li>

        <!-- Participantes inactivos -->
        <li class="nav-item">
          <a href="{{url('Configuracion/Participantes/Inactivos')}}" class="nav-link {{ routeIs(['Configuracion/Participantes/Inactivos', 'Participantes_inactivos/*', 'ParticipantesSuspendido', 'Configuracion/Participantes/*/edit', 'Configuracion/Participantes/create'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Inactivos</a>
        </li>

      </ul>
    </li>
    <!-- /.Participantes -->

    <!-- Calendario -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/Calendario/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Calendario<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- Semana de Compensación -->
        <li class="nav-item">
          <a href="{{ url('Configuracion/Calendario/SemanaCompensacion') }}" class="nav-link {{ routeIs(['Configuracion/Calendario/SemanaCompensacion', 'Configuracion/SemanaCompensacion/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Semana de compensación</a>
        </li>
        <!-- /.Semana de Compensación -->

        <li class="nav-item has-treeview {{ routeIs('Configuracion/Calendario/DiasExcepcionales/*') }}">
          <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Días excepcionales<i class="fa fa-angle-left right"></i></p></a>
          <ul class="nav nav-treeview">

            <!-- No Laborables -->
            <li class="nav-item">
              <a href="{{url('Configuracion/Calendario/DiasExcepcionales/NoLaborables')}}" class="nav-link {{ routeIs('Configuracion/Calendario/DiasExcepcionales/NoLaborables', 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>No laborables</p></a>
            </li>
            <!-- /.No Laborables  -->

            <!-- Laborables -->
            <li class="nav-item">
              <a href="{{url('Configuracion/Calendario/DiasExcepcionales/Laborables')}}" class="nav-link {{ routeIs('Configuracion/Calendario/DiasExcepcionales/Laborables', 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Laborables</p></a>
            </li>
            <!-- /. Laborables  -->

          </ul>
        </li>

      </ul>
    </li>
    <!-- Calendario -->

    <!-- Medios de Pagos -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/MediosPagos/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Medios de pagos<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- Productos -->
        <li class="nav-item">
          <a href="{{url('Configuracion/MediosPagos/Productos')}}" class="nav-link {{ routeIs(['Configuracion/MediosPagos/Productos', 'Configuracion/MediosPagos/Productos/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Productos</a>
        </li>

        <!-- Sub-Productos -->
        <li class="nav-item">
          <a href="{{url('Configuracion/MediosPagos/SubProductos')}}" class="nav-link {{ routeIs(['Configuracion/MediosPagos/SubProductos', 'Configuracion/MediosPagos/SubProductos/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Sub-productos</a>
        </li>

        <!-- Razones de Devolución-->
        <li class="nav-item">
          <a href="{{url('Configuracion/MediosPagos/RazonesDevolucion')}}" class="nav-link {{ routeIs(['Configuracion/MediosPagos/RazonesDevolucion', 'Configuracion/MediosPagos/RazonesDevolucion/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Razones de devolución</a>
        </li>
        <!--Razones de Devolución -->

      </ul>
    </li>
    <!-- /.Medios de Pagos -->

    <!-- Monedas -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/Monedas/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Monedas<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- cODIGO iso -->
        <li class="nav-item">
          <a href="{{url('Configuracion/Monedas/CodigoISO')}}" class="nav-link {{ routeIs(['Configuracion/Monedas/CodigoISO', 'Configuracion/Monedas/CodigoISO/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Código ISO</a>
        </li>

        <!-- Monedas -->
        <li class="nav-item">
          <a href="{{url('Configuracion/Monedas/Tipos')}}" class="nav-link {{ routeIs(['Configuracion/Monedas/Tipos', 'Configuracion/Monedas/Tipos/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Tipos</a>
        </li>

      </ul>
    </li>
    <!-- /.Monedas -->

    <!-- Período de Compensación -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/PeriodoCompensacion/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Período de compensación<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- Períodos -->
        <li class="nav-item">
          <a href="{{url('Configuracion/PeriodoCompensacion/Periodos')}}" class="nav-link {{ routeIs(['Configuracion/PeriodoCompensacion/Periodos', 'Configuracion/PeriodoCompensacion/Periodos/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Períodos</a>
        </li>

        <!-- Sesiones y Franjas -->
        <li class="nav-item">
          <a href="{{url('Configuracion/PeriodoCompensacion/IntervalosLiquidacion')}}" class="nav-link {{ routeIs(['Configuracion/PeriodoCompensacion/IntervalosLiquidacion', 'Configuracion/PeriodoCompensacion/IntervalosLiquidacion/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Intervalos de liquidación</a>
        </li>
      </ul>
    </li>
    <!-- /.Período de Compensación -->

    <!-- Contactos -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/Contactos/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Contactos<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- Bancarios -->
        <li class="nav-item">
          <a href="{{url('Configuracion/Contactos/Participantes')}}" class="nav-link {{ routeIs(['Configuracion/Contactos/Participantes', 'Configuracion/Contactos/Participantes/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Participantes</a>
        </li>

        <!-- Tipos -->
        <li class="nav-item">
          <a href="{{url('Configuracion/Contactos/Tipos')}}" class="nav-link  {{ routeIs(['Configuracion/Contactos/Tipos', 'Configuracion/Contactos/Tipos/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i>Tipos</a>
        </li>

      </ul>
    </li>
    <!-- /.Contactos -->

    <!-- Cobranza -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/Cobranza/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Cobranza<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!-- Recuperación de Costos -->
        <li class="nav-item">
          <a href="{{url('Configuracion/Cobranza/RecuperacionCostos')}}" class="nav-link {{ routeIs(['Configuracion/Cobranza/RecuperacionCostos', 'Configuracion/Cobranza/RecuperacionCostos/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Recuperación de costos</p></a>
        </li>

        <!--/. Recuperación de Costos -->
        <li class="nav-item has-treeview {{ routeIs('Configuracion/Cobranza/AltoValor/*') }}">
          <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Alto valor<i class="nav-icon fa fa-angle-left right"></i></p></a>
          <ul class="nav nav-treeview">

            <!-- Logs Limite Bajo valor -->
            <li class="nav-item">
              <a href="{{ url('Configuracion/Cobranza/AltoValor/LimiteBajoValor') }}" class="nav-link {{ routeIs(['Configuracion/Cobranza/AltoValor/LimiteBajoValor', 'Configuracion/Cobranza/AltoValor/LimiteBajoValor/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Límite bajo valor</p></a>
            </li>
            <!--/. Limite Bajo valor -->

            <!-- Exoneración -->
            <li class="nav-item">
              <a href="{{url('Configuracion/Cobranza/AltoValor/Exoneracion')}}" class="nav-link {{ routeIs(['Configuracion/Cobranza/AltoValor/Exoneracion', 'Configuracion/Cobranza/AltoValor/Exoneracion/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Exoneración</p></a>
            </li>
            <!--/. fin Exoneración -->
          </ul>
        </li>

      </ul>
    </li>
    <!-- /. Cobranza -->

    <!-- Empresas ordenates -->
    <li class="nav-item">
      <a href="{{url('Configuracion/EmpresasOrdenantes')}}" class="nav-link {{ routeIs('Configuracion/EmpresasOrdenantes', 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Empresas ordenantes</p></a>
    </li>
    <!-- empresas ordenantes -->

    <!--administracionn de tablas -->
    <li class="nav-item has-treeview {{ routeIs('Configuracion/AdministracionTablas/*') }}">
      <a href="#" class="nav-link"><i class="nav-icon fa fa fa-cog"></i><p>Administración de tablas<i class="fa fa-angle-left right"></i></p></a>
      <ul class="nav nav-treeview">

        <!--Parametros del sistema -->
        <li class="nav-item">
          <a href="{{url('Configuracion/AdministracionTablas/ParametrosSolucion')}}" class="nav-link {{ routeIs(['Configuracion/AdministracionTablas/ParametrosSolucion', 'Configuracion/AdministracionTablas/ParametrosSolucion/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Parámetros de la solución</p></a>
        </li>

        <!-- validaciones del sistema -->
        <li class="nav-item">
          <a href="{{url('Configuracion/AdministracionTablas/ValidacionesSolucion')}}" class="nav-link {{ routeIs(['Configuracion/AdministracionTablas/ValidacionesSolucion', 'Configuracion/AdministracionTablas/ValidacionesSolucion/*'], 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Validaciones de la solución</p></a>
        </li>

      </ul>
    </li>

    <!-- Eventos -->
    <li class="nav-item">
      <a href="{{url('Configuracion/Eventos')}}" class="nav-link {{ routeIs('Configuracion/Eventos', 'active') }}"><i class="nav-icon fa fa fa-cog"></i><p>Eventos</p></a>
    </li>
    <!-- eventos -->

  </ul>
</li>
<!-- /. Configuracion -->
@endif
