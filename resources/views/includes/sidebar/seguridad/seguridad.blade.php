@if (auth()->user()->hasPermissionModule('seguridad'))
<!-- Seguridad -->
<li class="nav-item has-treeview {{ routeIs('Seguridad/*') }}">
  <a href="#" class="nav-link">
    <i class="nav-icon fa fa-lock"></i>
    <p>Seguridad<i class="right fa fa-angle-left"></i></p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item has-treeview {{ routeIs('Seguridad/Usuarios/*') }}">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-lock"></i>
        <p>Usuarios
          <i class="right fa fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{url('Seguridad/Usuarios/Activos')}}" class="nav-link {{ routeIs(['Seguridad/Usuarios/Activos', 'Seguridad/Usuarios/Crear', 'Seguridad/Usuarios/Edit/*', 'Seguridad/Usuarios/CambiarContrasena/*'], 'active') }}">
            <i class="fa fa-lock nav-icon"></i>
            <p>Activos</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{url('Seguridad/Usuarios/Inactivos')}}" class="nav-link {{ routeIs('Seguridad/Usuarios/Inactivos', 'active') }}">
            <i class="fa fa-lock nav-icon"></i>
            <p>Inactivos</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{url('Seguridad/Usuarios/Eliminados')}}" class="nav-link {{ routeIs('Seguridad/Usuarios/Eliminados', 'active') }}">
            <i class="fa fa-lock nav-icon"></i>
            <p>Eliminados</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="{{url('Seguridad/Roles')}}" class="nav-link {{ routeIs(['Seguridad/Roles', 'Seguridad/Roles/*'], 'active') }}">
        <i class="fa fa-lock nav-icon"></i>
        <p>Roles</p>
      </a>
    </li>
    <li class="nav-item has-treeview {{ routeIs('Seguridad/Reportes/*') }}">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-lock"></i>
        <p>Reportes
          <i class="right fa fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{url('Seguridad/Reportes/Listado')}}" class="nav-link {{ routeIs('Seguridad/Reportes/*', 'active') }}">
            <i class="fa fa-lock nav-icon"></i>
            <p>Listado</p>
          </a>
        </li>
      </ul>
    </li>
  </ul>
</li>
<!-- /.Seguridad -->
@endif
