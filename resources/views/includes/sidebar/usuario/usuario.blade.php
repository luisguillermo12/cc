<!-- /Perfil de Usuario -->
<li class="{{ $perfil }} treeview"><a href="#"><i class="fa fa-user"></i><span>Perfil de usuario</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
    @if(Request::is('PerfilUsuario/MiPerfil') || Request::is('PerfilUsuario/MiPerfil/*') )
    <li class="active">
    @else
    <li>
    @endif
      <a href="{{route('frontend.user.perfil')}}"><i class="fa fa-user"></i>Mi perfil</a>
    </li>
  </ul>
</li>
<!-- Perfil de Usuario -->
