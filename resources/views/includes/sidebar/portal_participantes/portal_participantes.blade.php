@if (auth()->user()->hasPermissionModule('portal_participantes'))
<li class="nav-item has-treeview {{routeIs('PortalParticipantes/*')}}">
  <a href="#" class="nav-link">
    <i class="nav-icon fa fa-lock"></i>
    <p>
      Portal Participantes
      <i class="right fa fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item has-treeview {{ routeIs('PortalParticipantes/Seguridad/*')}}">
      <a href="#" class="nav-link">
        <i class="nav-icon fa fa-lock"></i>
        <p>
          Seguridad
          <i class="right fa fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item has-treeview {{ routeIs('PortalParticipantes/Seguridad/Usuarios/*')}}">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-lock"></i>
            <p>Usuarios
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('PortalParticipantes/Seguridad/Usuarios/Activos')}}" class="nav-link  {{ routeIs(['PortalParticipantes/Seguridad/Usuarios/Activos', 'PortalParticipantes/Seguridad/Usuarios/Edit/*', 'PortalParticipantes/Seguridad/Usuarios/Crear', 'PortalParticipantes/Seguridad/Usuarios/CambiarContrasena/*'], 'active')}}">
                <i class="fa fa-lock nav-icon"></i>
                <p>Activos</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('PortalParticipantes/Seguridad/Usuarios/Inactivos')}}" class="nav-link {{ routeIs('PortalParticipantes/Seguridad/Usuarios/Inactivos', 'active') }}">
                <i class="fa fa-lock nav-icon"></i>
                <p>Inactivos</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('PortalParticipantes/Seguridad/Usuarios/Eliminados')}}" class="nav-link {{ routeIs('PortalParticipantes/Seguridad/Usuarios/Eliminados', 'active') }}">
                <i class="fa fa-lock nav-icon"></i>
                <p>Eliminados</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="{{url('PortalParticipantes/Seguridad/Role')}}" class="nav-link {{ routeIs(['PortalParticipantes/Seguridad/Role', 'PortalParticipantes/Seguridad/Role/*'], 'active') }}">
            <i class="fa fa-lock nav-icon"></i>
            <p>Roles</p>
          </a>
        </li>
        <li class="nav-item has-treeview {{ routeIs('PortalParticipantes/Seguridad/Reportes/*') }}">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-lock"></i>
            <p>Reportes
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{url('PortalParticipantes/Seguridad/Reportes/Listado')}}" class="nav-link {{ routeIs(['PortalParticipantes/Seguridad/Reportes/Listado', 'PortalParticipantes/Seguridad/Reportes/Listado/*'], 'active') }}">
                <i class="fa fa-lock nav-icon"></i>
                <p>Listado</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</li>
@endif
