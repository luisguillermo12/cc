@if (auth()->user()->hasPermissionModule('monitoreo'))
<!-- Monitoreo -->
  <li class="{{ $monitoreo }} treeview"><a href="#"><i class='fa fa-desktop'></i><span>Monitoreo</span><i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">

      @permissions(['R0100-01-01'])
        <!-- Alarmas -->
        @if(Request::is('Monitoreo/ResumenCompensacion') )
          <li class="active">
        @else
          <li>
        @endif
          <a href="{{url('Monitoreo/ResumenCompensacion')}}"><i class="fa fa-desktop"></i>Resumen de compensación</a>
          </li>
        <!-- /.Alarmas -->
      @endauth

      @permissions(['R0100-02-01','R0100-02-02'])
        <!-- Alarmas -->
        @if(Request::is('Monitoreo/Alarmas') || Request::is('Monitoreo/Alarmas/*'))
          <li class="active">
        @else
          <li>
        @endif
          <a href="{{url('Monitoreo/Alarmas')}}"><i class="fa fa-desktop"></i>Alarmas</a>
          </li>
        <!-- /.Alarmas -->
      @endauth

      @permissions(['R0100-03-01','R0100-03-02'])
        <!-- Eventos -->
        @if(Request::is('Monitoreo/Eventos') || Request::is('Monitoreo/Eventos/*'))
          <li class="active">
        @else
          <li>
        @endif
          <a href="{{url('Monitoreo/Eventos')}}"><i class="fa fa-desktop"></i>Eventos</a>
          </li>
        <!-- /.Notificaciones -->
      @endauth

      @permissions(['R0100-04-01-01' , 'R0100-04-02-01'])
        <!-- Estado Participantes -->
        <li class="{{ $estado_participantes }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Estado de participantes</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">

            @permissions(['R0100-04-01-01'])
              <!-- General -->
              @if(Request::is('Monitoreo/EstadoParticipantes/General'))
                <li class="active">
              @else
                <li>
              @endif
                <a href="{{url('Monitoreo/EstadoParticipantes/General')}}"><i class="fa fa-desktop"></i>General</a>
                </li>
              <!-- /.General -->
            @endauth

            @permissions(['R0100-04-02-01'])
              <!-- Detallado -->
              @if(Request::is('Monitoreo/EstadoParticipantes/Detallado'))
                <li class="active">
              @else
                <li>
              @endif
                <a href="{{url('Monitoreo/EstadoParticipantes/Detallado')}}"><i class="fa fa-desktop"></i>Detallado</a>
                </li>
              <!-- /.Detallado -->
            @endauth

          </ul>
        </li>
        <!-- /.Estado Participantes -->
      @endauth

      @permissions(['R0100-05-01-01-01','R0100-05-01-01-02','R0100-05-01-01-03','R0100-05-01-02-01','R0100-05-01-02-02','R0100-05-01-02-03','R0100-05-02-01', 'R0100-05-02-02' , 'R0100-05-02-03','R0100-05-03-01'])
        <!-- Archivos -->
        <li class="{{ $archivosm }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Archivos</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">

            @permissions(['R0100-05-01-01-01','R0100-05-01-01-02','R0100-05-01-01-03','R0100-05-01-02-01','R0100-05-01-02-02','R0100-05-01-02-03'])
              <!-- Archivos recibidos  -->
              <li class="{{ $operacioesrecibidos }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Recibidos</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  @permissions(['R0100-05-01-01-01','R0100-05-01-01-02','R0100-05-01-01-03'])
                      <!-- Presentadas -->
                      @if(Request::is('Monitoreo/Archivos/Recibidos/Presentados') || Request::is('Monitoreo/Archivos/Recibidos/Presentados/Lotes/*') || Request::is('Monitoreo/Archivos/Recibidos/Presentados/Transacciones/*'))
                        <li class="active">
                      @else
                        <li>
                      @endif
                        <a href="{{url('Monitoreo/Archivos/Recibidos/Presentados')}}"><i class="fa fa-desktop"></i>Presentados</a>
                        </li>
                      <!-- /.Presentadas -->
                  @endauth

                  @permissions(['R0100-05-01-02-01','R0100-05-01-02-02','R0100-05-01-02-03'])
                    <!-- Devuelta -->
                    @if(Request::is('Monitoreo/Archivos/Recibidos/Devueltos') || Request::is('Monitoreo/Archivos/Recibidos/Devueltos/Lotes/*') || Request::is('Monitoreo/Archivos/Recibidos/Devueltos/Transacciones/*'))
                      <li class="active">
                    @else
                      <li>
                    @endif
                      <a href="{{url('Monitoreo/Archivos/Recibidos/Devueltos')}}"><i class="fa fa-desktop"></i>Devueltos</a>
                      </li>
                    <!-- /.Devuelta -->
                  @endauth

                </ul>
              </li>
              <!-- FIN DEL archivos recibidos-->
            @endauth

            @permissions(['R0100-05-02-01', 'R0100-05-02-02' , 'R0100-05-02-03'])
              <!-- Archivos Distribuidos -->
              @if(Request::is('Monitoreo/Archivos/Distribuidos') || Request::is('Monitoreo/Archivos/Distribuidos/Lotes/*') || Request::is('Monitoreo/Archivos/Distribuidos/Transacciones/*'))
                <li class="active">
              @else
                <li>
              @endif
                <a href="{{url('Monitoreo/Archivos/Distribuidos')}}"><i class="fa fa-desktop"></i>Distribuidos</a>
                </li>
              <!-- /.Archivos Distribuidos -->
            @endauth

            @permissions(['R0100-05-03-01'])
              <!-- Archivos Rechazados-->
              @if(Request::is('Monitoreo/Archivos/Rechazados'))
                <li class="active">
              @else
                <li>
              @endif
                <a href="{{url('Monitoreo/Archivos/Rechazados')}}"><i class="fa fa-desktop"></i>Rechazados</a>
                </li>
              <!-- /.Archivos Recibidos -->
            @endauth
          </ul>
        </li>
        <!-- /.Archivos -->
      @endauth

      @permissions(['R0100-06-01-01' , 'R0100-06-02-01'])
        <!-- ARCHIVOS DE RESUMEN -->
        <li class="{{ $horarios }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Archivos de resumen</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">

            @permissions(['R0100-06-01-01'])
                <!-- Cortes de Archivos -->
                @if(Request::is('Monitoreo/ArchivoResumen/Cortes'))
                  <li class="active">
                @else
                  <li>
                @endif
                  <a href="{{url('Monitoreo/ArchivoResumen/Cortes')}}"><i class="fa fa-desktop"></i>Cortes</a>
                  </li>
                <!-- /.Cortes de Archivos -->
            @endauth
            @permissions(['R0100-06-02-01'])
                <!-- Liquidaciónes de Archivos -->
                @if(Request::is('Monitoreo/ArchivoResumen/Liquidaciones'))
                  <li class="active">
                @else
                  <li>
                @endif
                  <a href="{{url('Monitoreo/ArchivoResumen/Liquidaciones')}}"><i class="fa fa-desktop"></i>Liquidaciones</a>
                  </li>
                <!-- /.Liquidaciónes de Archivos -->
            @endauth
          </ul>
        </li>
        <!-- /.ARCHIVOS DE RESUMEN -->
      @endauth

      @permissions(['R0100-07-01-01-01' , 'R0100-07-01-02-01','R0100-07-02-01-01' , 'R0100-07-02-02-01'])
        <!-- LIQUIDACION -->
        <!-- Bilateral -->
        <li class="{{ $liquidacion }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Posición</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">

            @permissions(['R0100-07-01-01-01' , 'R0100-07-01-02-01'])
              <li class="{{ $bilateral }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Bilateral</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  @permissions(['R0100-07-01-01-01'])
                    <!-- Pre-liquidacion producto -->
                    @if(Request::is('Monitoreo/Posicion/Bilateral/Preliquidado'))
                      <li class="active">
                    @else
                      <li>
                    @endif
                      <a href="{{url('Monitoreo/Posicion/Bilateral/Preliquidado')}}"><i class="fa fa-desktop"></i>Pre-liquidado</a>
                      </li>
                    <!-- /.Preli-quidacion producto -->
                  @endauth
                  @permissions(['R0100-07-01-02-01'])
                    <!-- Pre-liquidacion producto -->
                    @if(Request::is('Monitoreo/Posicion/Bilateral/Liquidado'))
                      <li class="active">
                    @else
                      <li>
                    @endif
                      <a href="{{url('Monitoreo/Posicion/Bilateral/Liquidado')}}"><i class="fa fa-desktop"></i>Liquidado</a>
                      </li>
                    <!-- /.Preli-quidacion producto -->
                  @endauth
                </ul>
              </li>
              <!-- ./Bilateral -->
            @endauth

            @permissions(['R0100-07-02-01-01' , 'R0100-07-02-02-01'])
              <!-- Multilateral -->
              <li class="{{ $multilateral }}"><a href="#"><i class="fa fa-desktop"></i><span class="pull-right-container">Multilateral</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  @permissions(['R0100-07-02-01-01'])
                    <!-- pre-liquidacion multilateral -->
                    @if(Request::is('Monitoreo/Posicion/Multilateral/Preliquidado'))
                      <li class="active">
                    @else
                      <li>
                    @endif
                      <a href="{{url('Monitoreo/Posicion/Multilateral/Preliquidado')}}"><i class="fa fa-desktop"></i>Pre-liquidado</a>
                      </li>
                    <!-- pre-liquidacion multilateral -->
                  @endauth
                  @permissions(['R0100-07-02-02-01'])
                    <!-- liquidacion multilateral -->
                    @if(Request::is('Monitoreo/Posicion/Multilateral/Liquidado'))
                      <li class="active">
                    @else
                      <li>
                    @endif
                      <a href="{{url('Monitoreo/Posicion/Multilateral/Liquidado')}}"><i class="fa fa-desktop"></i>Liquidado</a>
                      </li>
                    <!-- liquidacion multilateral -->
                  @endauth
                </ul>
              </li>
              <!-- ./Multilateral -->
            @endauth
          </ul>
        </li>
        <!-- /.FIN LIQUIDACION -->
      @endauth
    </ul>
  </li>
<!-- /.Monitoreo -->
@endif
