@if (auth()->user()->hasPermissionModule('ejecucion_administrativa'))
  <li class="nav-item has-treeview {{ routeIs('EjecucionAdministrativa/*') }}">
    <a href="#" class="nav-link">
      <i class="nav-icon fa fa-cogs"></i>
      <p>
        Ejecución administrativa
        <i class="right fa fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="{{ url('EjecucionAdministrativa/MonitoreoProcesos') }}" class="nav-link {{ routeIs('EjecucionAdministrativa/MonitoreoProcesos', 'active') }}">
          <i class="fa fa-cogs nav-icon"></i>
          <p>Monitoreo de procesos</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('EjecucionAdministrativa/InicioProcesos') }}" class="nav-link {{ routeIs('EjecucionAdministrativa/InicioProcesos', 'active') }}">
          <i class="fa fa-cogs nav-icon"></i>
          <p>Inicio de procesos</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('EjecucionAdministrativa/ModificacionFranja') }}" class="nav-link {{ routeIs('EjecucionAdministrativa/ModificacionFranja', 'active') }}">
          <i class="fa fa-cogs nav-icon"></i>
          <p>Modificación de franjas</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('EjecucionAdministrativa/CierreProcesos') }}" class="nav-link {{ routeIs('EjecucionAdministrativa/CierreProcesos', 'active') }}">
          <i class="fa fa-cogs nav-icon"></i>
          <p>Cierre de procesos</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('EjecucionAdministrativa/StandBy') }}" class="nav-link {{ routeIs('EjecucionAdministrativa/StandBy', 'active') }}">
          <i class="fa fa-cogs nav-icon"></i>
          <p>Stand by</p>
        </a>
      </li>
    </ul>
  </li>
@endif
