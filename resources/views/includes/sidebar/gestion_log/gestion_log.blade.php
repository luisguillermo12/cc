@if (auth()->user()->hasPermissionModule('gestion_logs'))
<!-- Gestión de Logs -->
<li class="{{ $gestion_logs }} treeview"><a href="#"><i class="fa fa-dashboard"></i>
  <span>Gestión de logs</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">

    @permissions(['R0400-01-01-01','R0400-01-02-01','R0400-01-03-01','R0400-01-04-01'])
    <li class="@if(Request::is('GestionLogs/GUI/*')) active @endif"><a href="#"><i class="fa fa-dashboard"></i>GUI<i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

        <!-- Resumen -->
        @if(Request::is('GestionLogs/GUI/Resumen'))
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['R0400-01-01-01'])
          <a href="{{ route('log-viewer::dashboard') }}"><i class="fa fa-dashboard"></i>Resumen</a>
        @endauth
          </li>
        <!--/. Resumen -->

        <!-- Logs Desarollo -->
        @if(Request::is('GestionLogs/GUI/Desarrollo')|| Request::is('GestionLogs/GUI/Desarrollo/*'))
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['R0400-01-02-01'])
          <a href="{{ route('log-viewer::logs.list') }}"><i class="fa fa-dashboard"></i>Desarrollo</a>
        @endauth
          </li>
        <!--/. Logs Desarollo -->

        <!-- Logs Inicio -->
        @if(Request::is('GestionLogs/GUI/Inicio'))
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['R0400-01-03-01'])
          <a href="{{ url('GestionLogs/GUI/Inicio') }}"><i class="fa fa-dashboard"></i>Inicio</a>
        @endauth
          </li>
        <!--/. Logs Inicio -->

        <!-- Logs Cierre -->
        @if(Request::is('GestionLogs/GUI/Cierre'))
          <li class="active">
        @else
          <li>
        @endif
        @permissions(['R0400-01-04-01'])
          <a href="{{ url('GestionLogs/GUI/Cierre') }}"><i class="fa fa-dashboard"></i>Cierre</a>
        @endauth
          </li>
        <!--/. Logs Cierre -->

      </ul>
    </li>
  @endauth

  @permissions(['R0400-02-01-01'])
    <li class="@if(Request::is('GestionLogs/CoreEPS/*')) active @endif"><a href="#"><i class="fa fa-dashboard"></i><span class="pull-right-container">Core EPS</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

        <!-- Core EPS -->
        @if(Request::is('GestionLogs/CoreEPS/*'))
          <li class="active">
        @else
          <li>
        @endif

        @permissions(['R0400-02-01-01'])
          <a href="{{url('GestionLogs/CoreEPS/Ejecucion')}}"><i class="fa fa-dashboard"></i>Ejecución</a>
        @endauth
          </li>
        <!--/. Core EPS --> 
      </ul>
    </li>
  @endauth


  </ul>
</li>
<!--/. Gestión de Logs -->
@endif