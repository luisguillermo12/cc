@if (auth()->user()->hasPermissionModule('imagenes'))
<!-- Conciliador -->
<li class="{{ $conciliador }} treeview"><a href="#"><i class='fa fa-archive'></i><span>Imágenes</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">

     @permissions(['R0150-01-01','R0150-01-02','R0150-01-03'])
    <!-- Imagenes Distribuidas -->
    @if(Request::is('Imagenes/Conciliadas') || Request::is('Imagenes/Conciliadas/*') || Request::is('detalle/*'))
      <li class="active">
    @else
      <li>
    @endif
     @permissions(['R0150-01-01','R0150-01-02','R0150-01-03'])             
        <a href="{{url('Imagenes/Conciliadas')}}"><i class="fa fa-archive"></i>Conciliadas</a>
      </li>
    @endauth
    <!-- /.Imagenes Distribuidas -->
    @endauth


    @permissions(['R0150-02-01-01','R0150-02-02-01','R0150-02-03-01'])
    <!-- Archivos -->
    <li class="{{ $conciliador_archivos }}"><a href="#"><i class="fa fa-archive"></i><span class="pull-right-container">Archivos</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

      <!-- Recibidas -->
      @if(Request::is('Imagenes/Archivos/Recibidos'))
        <li class="active">
        @else
        <li>
          @endif
        @permissions(['R0150-02-01-01'])
          <a href="{{url('Imagenes/Archivos/Recibidos')}}"><i class="fa fa-archive"></i>Recibidos</a>
        @endauth
        </li>
        <!-- /. Recibidas -->


        <!-- Distribuidos -->
        @if(Request::is('Imagenes/Archivos/Distribuidos'))
          <li class="active">
          @else
          <li>
            @endif
            @permissions(['R0150-02-02-01'])
            <a href="{{url('Imagenes/Archivos/Distribuidos')}}"><i class="fa fa-archive"></i>Distribuidos</a>
            @endauth
          </li>
          <!-- /. Distribuidos -->


          <!-- Rechazados -->
          @if(Request::is('Imagenes/Archivos/Rechazados'))
            <li class="active">
            @else
            <li>
              @endif
            @permissions(['R0150-02-03-01'])
              <a href="{{url('Imagenes/Archivos/Rechazados')}}"><i class="fa fa-archive"></i>Rechazados</a>
            @endauth
            </li>
            <!-- /. Rechazados -->
            </ul>
            </li>
            <!-- FIN DE ARCHIVOS -->
            @endauth   



          @permissions(['R0150-03-01','R0150-03-02'])
              <!-- Imagenes Conciliadas -->
              @if(Request::is('Imagenes/DistribuidasParticipante') || Request::is('Imagenes/DistribuidasParticipante/*') || Request::is('cheque/*'))
                <li class="active">
        
              @else
                <li>
              @endif
              @permissions(['R0150-03-01','R0150-03-02'])
                  <a href="{{url('Imagenes/DistribuidasParticipante')}}"><i class="fa fa-archive"></i>Distribuidas por participante</a>
              @endauth
                </li>
              <!--Imagenes Conciliadas -->
          @endauth

             
              </ul>
            </li>
            <!-- /.Conciliador -->
            @endif
