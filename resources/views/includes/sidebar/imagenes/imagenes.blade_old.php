@if (auth()->user()->hasPermissionModule('imagenes'))
<!-- Conciliador -->
<li class="{{ $conciliador }} treeview"><a href="#"><i class='fa fa-archive'></i><span>Imágenes</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">

    @permissions(['ver-img-mon-img-dis', 'det-img-mon-img-dis' , 'det-det--img-mon-img-dis'])
    <!-- MONITOREO CONCILIADOR -->
    <li class="{{ $conciliador_monitoreo }}"><a href="#"><i class="fa fa-archive"></i><span class="pull-right-container">Monitoreo</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

        <!-- Monitoreo Archivo
        @if(Request::is('ConciliadorMonitoreoArchivos'))
        <li class="active">
        @else
        <li>
        @endif
        <a href="{{url('ConciliadorMonitoreoArchivos')}}"><i class="fa fa-archive"></i>Archivos</a>
      </li>
      /.Monitoreo Archivos -->

      @permissions(['ver-img-mon-img-dis', 'det-img-mon-img-dis' , 'det-det--img-mon-img-dis'])
      <!-- Imagenes Enviadas por IB -->
      @if(Request::is('Imagenes/Monitoreo/ImagenesDistribuidas') || Request::is('Imagenes/Monitoreo/ImagenesDistribuidas/*') || Request::is('detalle/*'))
      <li class="active">
        @else
        <li>
          @endif
          <a href="{{url('Imagenes/Monitoreo/ImagenesDistribuidas')}}"><i class="fa fa-archive"></i>Imágenes distribuidas</a>
        </li>
        <!-- /.Imagenes Enviadas por IB -->
        @endauth
      </ul>
    </li>
    <!-- FIN DEL MONITOREO DEL CONCILIADOR-->
    @endauth

    @permissions(['ver-img-por-con', 'ver-img-arc-con-par','det-img-arc-con-par'])
    <!-- Archivos -->
    <li class="{{ $conciliador_archivos }}"><a href="#"><i class="fa fa-archive"></i><span class="pull-right-container">Archivos</span><i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">

        <!-- Archivos Por Tratar
        @if(Request::is('ConciliadorArchPorTratar'))
        <li class="active">
        @else
        <li>
        @endif
        <a href="{{url('ConciliadorArchPorTratar')}}"><i class="fa fa-archive"></i>Pendientes</a>
      </li>
      /.Archivos Por Tratar -->

      @permissions(['ver-arc-img-rec'])
      <!-- Recibidas -->
      <li class="active">
        @else
        <li>
          @endif
          <a href="{{url('Imagenes/Archivos/Recibidos')}}"><i class="fa fa-archive"></i>Recibidos</a>
        </li>
        <!-- /. Recibidas -->


        <!-- Distribuidos -->
        @if(Request::is('Imagenes/Archivos/Distribuidos'))
        <li class="active">
          @else
          <li>
            @endif
            <a href="{{url('Imagenes/Archivos/Distribuidos')}}"><i class="fa fa-archive"></i>Distribuidos</a>
          </li>
          <!-- /. Distribuidos -->


          <!-- Rechazados -->
          @if(Request::is('Imagenes/Archivos/Rechazados'))
          <li class="active">
            @else
            <li>
              @endif
              <a href="{{url('Imagenes/Archivos/Rechazados')}}"><i class="fa fa-archive"></i>Rechazados</a>
            </li>
            <!-- /. Rechazados -->

            @permissions(['ver-img-por-con'])
            <!-- POR CONCILIAR -->
            @if(Request::is('Imagenes/Archivos/PorConciliar'))
            <li class="active">
              @else
              <li>
                @endif
                <a href="{{url('Imagenes/Archivos/PorConciliar')}}"><i class="fa fa-archive"></i>Por conciliar</a>
              </li>
              <!-- /.POR CONCILIAR -->
              @endauth

              @permissions(['ver-img-arc-con-par','det-img-arc-con-par'])
              <!-- CONCILIADOS POR PARTICIPANTES-->
              @if(Request::is('Imagenes/Archivos/Conciliados') || Request::is('Imagenes/Archivos/Conciliados/*'))
              <li class="active">
                @else
                <li>
                  @endif
                  <a href="{{url('Imagenes/Archivos/Conciliados')}}"><i class="fa fa-archive"></i>Conciliados</a>
                </li>
                <!--/.CONCILIADOS POR PARTICIPANTES -->
                @endauth
              </ul>
            </li>
            <!-- FIN DE ARCHIVOS -->
            @endauth



            <!-- Archivos tratados por IBP
            @if(Request::is('ConciliadorArchivosTratadosIBP'))
            <li class="active">
            @else
            <li>
            @endif
            <a href="{{url('ConciliadorArchivosTratadosIBP')}}"><i class="fa fa-archive"></i> Archivos Tratados por IBP</a>
          </li>
          <!-- /.Archivos tratados por IBP -->


          @permissions(['ver-ope-nop-con', 'ver-ope-con','det-ope-con'])
          <!--OPERACIONES -->
          <li class="{{ $conciliador_operaciones }}"><a href="#"><i class="fa fa-archive"></i><span class="pull-right-container">Operaciones</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">

              @permissions(['ver-ope-nop-con', 'ver-ope-con','det-ope-con'])
              <!-- Imagenes de Alto Valor -->
              @if(Request::is('Imagenes/Operaciones/Conciliadas') || Request::is('Imagenes/Operaciones/Conciliadas/*') || Request::is('cheque/*'))
              <li class="active">
                @else
                <li>
                  @endif
                  <a href="{{url('Imagenes/Operaciones/Conciliadas')}}"><i class="fa fa-archive"></i>Conciliadas</a>
                </li>
                <!-- /.Imagenes de Alto Valor -->
                @endauth

                @permissions(['ver-ope-nop-con'])
                <!-- Registros Desafados-->
                @if(Request::is('Imagenes/Operaciones/NoConciliadas') || Request::is('no_conciliadas/*'))
                <li class="active">
                  @else
                  <li>
                    @endif
                    <a href="{{url('Imagenes/Operaciones/NoConciliadas')}}"><i class="fa fa-archive"></i>No conciliadas</a>
                  </li>
                  <!-- /.Registros Desafados -->
                  @endauth
                </ul>
              </li>
              <!--OPERACIONES -->
              @endauth

              @permissions(['ver-est-arc-con-hor'])
              <!--REPORTES -->
              <li class="{{ $conciliador_estadisticas}}"><a href="#"><i class="fa fa-archive"></i><span class="pull-right-container">Estadísticas por archivos</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                  <!--ESTADÍSTICAS FINALES
                  @if(Request::is('EstadisticasPorArchivoPorTratarHora'))
                  <li class="active">
                  @else
                  <li>
                  @endif
                  <a href="{{url('EstadisticasPorArchivoPorTratarHora')}}"><i class="fa fa-archive"></i>Por Tratar por Hora</a>
                </li>
                ESTADÍSTICAS FINALES-->

                @permissions(['ver-est-arc-con-hor'])
                <!--ESTADÍSTICAS FINALES-->
                @if(Request::is('Imagenes/EstadisticasporArchivo/PorConciliarHora'))
                <li class="active">
                  @else
                  <li>
                    @endif
                    <a href="{{url('Imagenes/EstadisticasporArchivo/PorConciliarHora')}}"><i class="fa fa-archive"></i>Por conciliar por hora</a></li>
                    <!-- ESTADÍSTICAS FINALES -->
                    @endauth

                    <!-- ESTADÍSTICAS FINALEs
                    @if(Request::is('EstadisticasPorArchivoHora'))
                    <li class="active">
                    @else
                    <li>
                    @endif
                    <a href="{{url('EstadisticasPorArchivoHora')}}"><i class="fa fa-archive"></i>Conciliados por Hora</a></li>
                    ESTADÍSTICAS FINALES -->

                  </ul>
                </li>
                <!--REPORTES -->
                @endauth
                <!-- CONCILIADOR POSICIONES
                if(Request::is('ConciliadorPosiciones'))
                <li class="active">
                else
                <li>
                endif
                <a href="{{url('ConciliadorPosiciones')}}"><i class="fa fa-archive"></i>Posiciones</a></li>
                CONCILIADOR POSICIONES -->
              </ul>
            </li>
            <!-- /.Conciliador -->
            @endif
