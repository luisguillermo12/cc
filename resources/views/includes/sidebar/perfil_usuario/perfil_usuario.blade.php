<li class="nav-item has-treeview {{routeIs('PerfilUsuario/MiPerfil')}}">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-lock"></i>
          <p>
           Perfil de usuarios
            <i class="right fa fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview ">
                <li class="nav-item  {{routeIs('PerfilUsuario/MiPerfil')}}">
                        <a href="{{url('PerfilUsuario/MiPerfil')}}" class="nav-link">
                            <i class="fa fa-lock nav-icon"></i>
                            <p>Mi perfil</p>
                          </a>
                </li>
        </ul>
</li>


