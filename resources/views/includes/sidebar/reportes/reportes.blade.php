@if (auth()->user()->hasPermissionModule('reportes'))
  <li class="nav-item has-treeview {{ routeIs('Reportes/*') }}">
    <a href="#" class="nav-link">
      <i class="nav-icon fa fa-line-chart"></i>
      <p>
        Reportes
        <i class="right fa fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="{{ url('Reportes/Transaccional/ListadoGeneral') }}" class="nav-link {{ routeIs('Reportes/Transaccional/*', 'active') }}">
          <i class="fa fa-line-chart nav-icon"></i>
          <p>Transaccional</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('Reportes/Historico/ListadoGeneral') }}" class="nav-link {{ routeIs('Reportes/Historico/*', 'active') }}">
          <i class="fa fa-line-chart nav-icon"></i>
          <p>Histórico</p>
        </a>
      </li>
    </ul>
  </li>
@endif
