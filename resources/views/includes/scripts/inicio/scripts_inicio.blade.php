<script>
(function($) {

    var methods = {
        init: function(obj, options) {

            var $this = $(this),
                data = $this.data('daterangeBar');

            if (!data) {
                $(this).data('daterangeBar', {
                    target : obj
                });
            }
            methods.renderBar(obj, options);

            return this;
        },

        renderBar: function(obj, options) {

            var startDateDayArr, startDateTimeArr, startDate, endDateDayArr, endDate, endDateTimeArr;

            var startDateArr = options.startDate.split(' ');
            if (startDateArr[1] != undefined){
                startDateDayArr = startDateArr[0].split('-');
                startDateTimeArr = startDateArr[1].split(':');
                startDate = new Date(startDateDayArr[2], startDateDayArr[1]-1, startDateDayArr[0], startDateTimeArr[0], startDateTimeArr[1], startDateTimeArr[2]);
            }
            else{
                startDateDayArr = startDateArr[0].split('-');
                startDate = new Date(startDateDayArr[2], startDateDayArr[1]-1, startDateDayArr[0]);
            }



            var endDateArr = options.endDate.split(' ');
            if (endDateArr[1] != undefined){
                endDateDayArr = endDateArr[0].split('-');
                endDateTimeArr = endDateArr[1].split(':');
                endDate = new Date(endDateDayArr[2], endDateDayArr[1]-1, endDateDayArr[0], endDateTimeArr[0], endDateTimeArr[1], endDateTimeArr[2]);

            }
            else{
                endDateDayArr = endDateArr[0].split('-');
                endDate = new Date(endDateDayArr[2], endDateDayArr[1]-1, endDateDayArr[0]);
            }

            var today = new Date("{{fechaBd()}}");
            //var today = new Date();
            //console.log(today);
            //console.log(hora_bd());
            var fullRange = (startDateArr[1]!= undefined ? Math.round((endDate - startDate) / 1000) : Math.round((endDate - startDate) / (1000 * 60 * 60 * 24)));
            var todayRange = (startDateArr[1]!= undefined ? Math.round((today - startDate) / 1000) : Math.round((today - startDate) / (1000 * 60 * 60 * 24)));

            //console.log(fullRange);
            //console.log(todayRange);

            var dateDiff = Math.round((todayRange * (parseInt(options.maxValue) - parseInt(options.minValue)) / fullRange)*100)/100;
            if (dateDiff < options.minValue){
                dateDiff = options.minValue;
            }
            if (dateDiff > options.maxValue){
                dateDiff = options.maxValue;
            }

            //console.log(dateDiff);

            var progress = $('<div/>')
                .addClass('progress');

            if (options.privateColors){
                progress
                    .css('border', '1px solid ' + options.barColor)
                    .css('background-color', options.bgColor);
            }
            else{
                progress
                    .attr('role', 'progressbar')
                    .attr('aria-valuenow', dateDiff)
                    .attr('aria-valuemin', options.minValue)
                    .attr('aria-valuemax', options.maxValue);
            }

            var progressBar = $('<div/>')
                .addClass('progress-bar progress-bar-striped progress-bar-animated')
                .css('width', dateDiff + '%');

            if (options.privateColors){
                progressBar
                    .css('background-color', options.barColor);
            }

            if (options.barClass != undefined){
                progressBar.addClass(options.barClass);
            }
            progressBar.text(dateDiff + '% ' + options.msg);

            $(progress).append(progressBar);
            obj.append(progress);
        }
    };


    $.fn.daterangeBar = function(options) {
        var settings = $.extend({
            'msg': 'of the year',
            'startDate': '01-01-2018',
            'endDate': '31-12-2018',
            'barClass': undefined,
            'bootstrap': false,
            'privateColors': true,
            'barColor': '#7BA7B5',
            'bgColor': '#9CD3E6',
            'minValue': 0,
            'maxValue': 100
        }, options);

        return methods.init(this, settings);
    }
})(jQuery);
</script>

<script>
	function creditod() {
    var disk = $.ajax({
      url: "{{ url('Inicio/creditod') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);

          //console.log(data.responseJSON[0],data.responseJSON[1])

          $('.daterangeBarHourCreditod').html("");
            $('.daterangeBarHourCreditod').daterangeBar({
                'startDate': data.responseJSON[1],
                'endDate': data.responseJSON[0],
                'barClass': 'bg-success progress-bar-striped active',
                'bootstrap': true,
                'privateColors': false,
                'msg': 'Completado'
        	});
            
          var timeout = setTimeout(creditod, 1 * 1000 * 60);
        },
        error:function(data){
          //console.log(data);
        }
    });
  }
  creditod();

  function creditodd() {
    var disk = $.ajax({
      url: "{{ url('Inicio/creditodevuelta') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);

          //console.log(data.responseJSON[0],data.responseJSON[1])

          $('.daterangeBarHourCreditodevuelta').html("");
            $('.daterangeBarHourCreditodevuelta').daterangeBar({
                'startDate': data.responseJSON[1],
                'endDate': data.responseJSON[0],
                'barClass': 'bg-success progress-bar-striped active',
                'bootstrap': true,
                'privateColors': false,
                'msg': 'Completado'
            });
            
          var timeout = setTimeout(creditod, 1 * 1000 * 60);
        },
        error:function(data){
          //console.log(data);
        }
    });
  }
  creditodd();


  function cheques() {
    var disk = $.ajax({
      url: "{{ url('Inicio/cheques') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);
          //console.log(data.responseJSON[0],data.responseJSON[1])
          $('.daterangeBarHourCheques').html("");

            $('.daterangeBarHourCheques').daterangeBar({
                'startDate': data.responseJSON[1],
                'endDate': data.responseJSON[0],
                'barClass': 'bg-success progress-bar-striped active',
                'bootstrap': true,
                'privateColors': false,
                'msg': 'Completado'
        	});
            
          var timeout = setTimeout(cheques, 1 * 1000 * 60);
        },
        error:function(data){
          //console.log(data);
        }
    });
  }
  cheques();

  function chequesd() {
    var disk = $.ajax({
      url: "{{ url('Inicio/chequesdevuelta') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);
          //console.log(data.responseJSON[0],data.responseJSON[1])
          $('.daterangeBarHourChequesdevuelta').html("");

            $('.daterangeBarHourChequesdevuelta').daterangeBar({
                'startDate': data.responseJSON[1],
                'endDate': data.responseJSON[0],
                'barClass': 'bg-success progress-bar-striped active',
                'bootstrap': true,
                'privateColors': false,
                'msg': 'Completado'
            });
            
          var timeout = setTimeout(cheques, 1 * 1000 * 60);
        },
        error:function(data){
          //console.log(data);
        }
    });
  }
  chequesd();



  function domiciliaciones() {
    var disk = $.ajax({
      url: "{{ url('Inicio/domiciliaciones') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);
          //console.log(data.responseJSON[0],data.responseJSON[1])
          $('.daterangeBarHourDomiciliaciones').html("");
          
            $('.daterangeBarHourDomiciliaciones').daterangeBar({
                'startDate': data.responseJSON[1],
                'endDate': data.responseJSON[0],
                'barClass': 'bg-success progress-bar-striped active',
                'bootstrap': true,
                'privateColors': false,
                'msg': 'Completado'
        	});
            
          var timeout = setTimeout(domiciliaciones, 1 * 1000 * 60);
        },
        error:function(data){
          //console.log(data);
        }
    });
  }
  domiciliaciones();

  function domiciliacionesd() {
    var disk = $.ajax({
      url: "{{ url('Inicio/domiciliacionesdevueltas') }}",
      type: 'GET',
      complete: function (data){
        clearTimeout(timeout);
        //console.log(data.responseJSON[0],data.responseJSON[1])
        $('.daterangeBarHourDomiciliacionesdevu').html("");
        
          $('.daterangeBarHourDomiciliacionesdevu').daterangeBar({
              'startDate': data.responseJSON[1],
              'endDate': data.responseJSON[0],
              'barClass': 'bg-success progress-bar-striped active',
              'bootstrap': true,
              'privateColors': false,
              'msg': 'Completado'
          });
            
          var timeout = setTimeout(domiciliaciones, 1 * 1000 * 60);
        },
      error:function(data){
        //console.log(data);
      }
    });
  }
  domiciliacionesd();
</script>