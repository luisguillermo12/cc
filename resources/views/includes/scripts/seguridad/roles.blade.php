<script type="text/javascript">
var Eliminar = function(id){
 swal({
  title: 'Esta seguro de eliminar el rol del sistema?',
  text: "El mismo no podra recuperarse!",
  icon: 'warning',
  buttons: [true, "Aceptar"]
}).then((result) => {

  if (result) {
      var route = "{{url('Seguridad/Roles/Borrar')}}/"+id+"";
      console.log(route);
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
            var url='{{ url('Seguridad/Roles')}}';
            location.href = url;
            //Alerta de Confirmación
            swal({
              title: 'Rol eliminado satisfactoriamente',
              text: "",
              icon: 'success'
            });
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};
</script>
