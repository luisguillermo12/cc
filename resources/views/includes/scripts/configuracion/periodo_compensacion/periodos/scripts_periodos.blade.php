<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/timepicker/bootstrap-timepicker.min.css') }}">
<script type="text/javascript" src="{{ asset('js/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {

  // $('#Periodos-table').DataTable({
  //   "scrollCollapse": true,
  //   "aoColumnDefs": [{
  //     "bSortable": false,
  //     "aTargets": [8]
  //   }],
  //     "searching": true,
  //     "lengthChange": true,
  //     "lengthMenu": [[30, 50, 100], [30, 50, 100]],
  //     "order": [[ 0, 'asc' ]],
  //     "language": {
  //     "sProcessing":    "Procesando...",
  //     "sLengthMenu":    "Mostrar _MENU_ registros",
  //     "sZeroRecords":   "No se encontraron resultados",
  //     "sEmptyTable":    "Ningún dato disponible en esta tabla",
  //     "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  //     "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
  //     "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
  //     "sInfoPostFix":   "",
  //     "sSearch":        "Buscar:",
  //     "sUrl":           "",
  //     "sInfoThousands":  ",",
  //     "sLoadingRecords": "Cargando...",
  //     "oPaginate": {
  //       "sFirst":    "Primero",
  //       "sLast":    "Último",
  //       "sNext":    "Siguiente",
  //       "sPrevious": "Anterior"
  //     },
  //     "oAria": {
  //       "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
  //       "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  //     }
  //   }
  // });

  $("#nro_sesion").keypress(soloNumeros);

  $("#producto_id,#dia_inicio,#dia_fin,input[name=estatus]").on('change', function() {
    $('#actualizarPeriodo').attr("disabled", false);
  });

  $("#hora_inicio,#hora_fin,#num_liquidacion").on('keypress', function() {
    $('#actualizarPeriodo').attr("disabled", false);
  });

  $("#dia_inicio").on('change', function() {
    if($(this).val() == 'D') {
      $('#dia_fin').val('D');
      $('#dia_fin').attr("disabled", true);
    } else {
      $('#dia_fin').val('0');
      $('#dia_fin').attr("disabled", false);
    }
  });

  $('.hora_inicio').timepicker({
    showMeridian: false,
    template: false,
    explicitMode: true
  });

  $('.hora_fin').timepicker({
    showMeridian: false,
    template: false,
    explicitMode: true
  });

});

function soloNumeros(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
  return false;

  return true;
}

$( "#form-create" ).on("submit", function( event ) {
  event.preventDefault(event);
  swal({
    title: '¿Está seguro de que quiere agregar el período?',
    text: " ",
    icon: "warning",
    buttons: ['Cancelar', 'Aceptar'],
    closeModal: true
  }).then((confirmed) => {
    if (confirmed) {
      $("#form-create").off("submit").submit();
    }
  });
});

$( "#form-edit" ).on("submit", function( event ) {
  event.preventDefault(event);
  swal({
    title: '¿Está seguro de que quiere actualizar el período?',
    text: " ",
    icon: "warning",
    buttons: ['Cancelar', 'Aceptar'],
    closeModal: true
  }).then((confirmed) => {
    if (confirmed) {
      $("#form-edit").off("submit").submit();
    }
  });
});

//FUNCIÓN PARA ELIMINAR UN MONTO
var Eliminar = function(id, nombre)
{
  // ALERT JQUERY
  swal({
    title: '¿Está seguro de que quiere eliminar el período?',
    text: 'Usted no podrá recuperar el registro: ' + nombre.replace(/&nbsp;/g, " "),
    icon: "warning",
    buttons: ['Cancelar', 'Aceptar'],
    closeModal: true
  }).then((confirmed) => {
    if (confirmed) {
      var route = "{{url('Configuracion/PeriodoCompensacion/Periodos')}}/"+id+"";
      var token = $("#token").val();
      $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        success: function(data) {
          if (data.success == 'true')
          {
              var url="{{ url('Configuracion/PeriodoCompensacion/Periodos')}}";
              location.href = url;
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 419:
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

</script>
