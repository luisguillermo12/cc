<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/timepicker/bootstrap-timepicker.min.css') }}">
<script type="text/javascript" src="{{ asset('js/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script type="text/javascript">
  function copiar_intervalo(num) {
    var valor = $("#num_liquidacion" + num).val();

    if (valor != null) {
      $("#num_liquidacion" + (num + 1)).val(valor);
    }
  };
</script>
<script type="text/javascript">
 $(function () {

    $(document).ready(function(){ //picker de horas
       $('.fechahora_inicio').timepicker({
         showMeridian: false,
         minuteStep: 1,
       });

       $('.fechahora_fin').timepicker({
         showMeridian: false,
         minuteStep: 1,
       });

      //para lanzar el evento de cambio al dar click en incrementar/decrementar el valor del campo, y vue pueda detectar el mismo
      $('div.bootstrap-timepicker div.dropdown-menu table tbody tr td a').on('mouseup', function(){
        setTimeout(function(){
          document.querySelector('.fechahora_inicio').dispatchEvent(new Event('change'));
        }, 100);
      });
      $('div.bootstrap-timepicker div.dropdown-menu table tbody tr td input').on('blur', function(){
        setTimeout(function(){
          document.querySelector('.fechahora_inicio').dispatchEvent(new Event('change'));
        }, 100);
      });
    });

    $( "#form-edit" ).on("submit", function( event ) {
      event.preventDefault(event);
      swal({
        title: '¿Está seguro de que quiere actualizar el intervalo de liquidación?',
        text: " ",
        icon: "warning",
        buttons: ['Cancelar', 'Aceptar'],
        closeModal: true
      }).then((confirmed) => {
        if (confirmed) {
          $("#form-edit").off("submit").submit();
        }
      });
    });

    var horas = [
      <?php for ($i=0; $i < count($rangos_hora); $i++) {
        if($i != (count($rangos_hora)-1)){
          echo '"'.$rangos_hora[$i].'",';
        }else {
          echo '"'.$rangos_hora[$i].'"';
        }
      } ?>
    ];

    var nombre_archivo = 'Reporte_Intervalo_Liquidacion';

    var chartLiq = Highcharts.setOptions({
      lang: {

        loading: 'Cargando...',
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        exportButtonTitle: "Exportar",
        printButtonTitle: "Importar",
        rangeSelectorFrom: "Desde",
        rangeSelectorTo: "Hasta",
        rangeSelectorZoom: "Período",
        downloadPNG: 'Descargar imagen PNG',
        downloadJPEG: 'Descargar imagen JPEG',
        downloadPDF: 'Descargar imagen PDF',
        downloadSVG: 'Descargar imagen SVG',
        printChart: 'Imprimir',
        resetZoom: 'Reiniciar zoom',
        resetZoomTitle: 'Reiniciar zoom',
        thousandsSep: ".",
        decimalPoint: ',',
        numericSymbols: [' mil', ' millones']
      }
    });

    //Gráfico de Xrange
    <?php $i = 0; ?>
    <?php foreach ($operaciones as $operaciones_det) { ?>

      var colores = [
        <?php
          if(substr($operaciones_det->descripcion,0,3)=='010'){
            echo "'#79c447', '#79c447'";
          }elseif(substr($operaciones_det->descripcion,0,3)=='020'){
            echo "'#fabb3d', '#fabb3d'";
          }else{
            echo "'#67c2ef', '#67c2ef'";
          }
        ?>
      ];

    chartLiq = Highcharts.chart('container<?= $i ?>', {
        chart: {
            type: 'xrange',
            spacingTop: 50,
            spacingBottom: 30,
            height: 300
        },
        title: {
            text: 'Distribución del intervalo de liquidación'
        },
        subtitle: {
            text: 'Operación: <?=substr($operaciones_det->descripcion,6,strlen(utf8_decode($operaciones_det->descripcion)))?>'
        },
        xAxis: {
           categories: horas,
           type: 'category',
           tickInterval: 30,
           gridLineWidth: 1,
           gridLineColor: '#ccd6eb',
           labels: {
            	//step: 1,
              rotation: -90
            }
        },
        yAxis: {
            categories: ['Período de liquidación','Franja de presentadas', 'Franja de devueltas'],
            reversed: true,
            gridLineWidth: 0,
            title: {
                text: ''
            },
        },
        tooltip: {
            backgroundColor: '#ffffff',
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"><?=substr($operaciones_det->descripcion,6,strlen(utf8_decode($operaciones_det->descripcion)))?></td>' +
                '</tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        series: [{
            borderColor: '#ffffff',
            colorByPoint: true,
            colors: colores,
            borderRadius: 0,
            pointWidth: 20,
            showInLegend: false,
            data: <?=$operaciones_det->datos?>,
            dataLabels: {
                enabled: true,
                formatter: function() {
                    var percents = this.point.partialFill
                    return percents ? '' : ''
                }
            }
        }],
        exporting: {
            filename: nombre_archivo,
            sourceWidth: 1280,
            sourceHeight: 968
        }
    });
    <?php $i++; ?>
    <?php } ?>

var operaciones_det = <?php foreach ($operaciones as $operaciones_det) { echo $operaciones_det->datos; }?>;

new Vue({
 el: '#app',
 data: {
   id_periodo: [],
   hora_inicio: [],
   hora_fin: []
 },
 created: function() {

   this.handleGraph();

 },
 methods: {
   handleGraph: function() {
     var rangos_hora = horas; //permite generar el rango de horas en js
     var inicio_franjas = 2; //posicion donde inicia la data de franjas en el json

     this.obtainInfo(); //para obtener la informacion al momento de cambio desde la pagina

     for(var i=0; i < this.id_periodo.length; i++){ //para buscar la posicion de la hora inicio en el rango, segun lo colocado en el campo
       operaciones_det[i+inicio_franjas].x = this.findHora(this.hora_inicio[i].substr(0,5),rangos_hora);
       operaciones_det[i+inicio_franjas].x2 = this.findHora(this.hora_fin[i].substr(0,5),rangos_hora);
     }

     //luego de actualizar el objeto base con la nueva data modificada, se actualiza el grafico
     chartLiq.update({
        series: {
         data: operaciones_det
        }
      });

   },
   obtainInfo: function(){
     var i = 0;
     var id_periodo = new Array();
     var horas_inicio = new Array();
     var horas_fin = new Array();
     $( ".intervalo_liquidacion" ).each(function( index ) {
        id_periodo[i] = $( this ).val() ;
        i++;
     });
     this.id_periodo = id_periodo;
     i = 0;
     $( ".hora_inicio" ).each(function( index ) {
        horas_inicio[i] = $( this ).val() ;
        i++;
     });
     this.hora_inicio = horas_inicio;
     i = 0;
     $( ".hora_fin" ).each(function( index ) {
        horas_fin[i] = $( this ).val() ;
        i++;
     });
     this.hora_fin = horas_fin;
   },
   findHora: function(hora,rangos_hora){
     var hora_pos = null;
     for (var i=0; i < rangos_hora.length; i++) {
       if(rangos_hora[i] == hora){
         hora_pos = i;
         break;
       }
     }
     return hora_pos == null ? 0 : hora_pos;
   }
 }
});

 });

</script>
