<!-- datepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker3.min.css') }}">
<script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/locales/bootstrap-datepicker.es.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {

    var validacion = {!! $semana_compensacion !!};

    var nolaboral = {!! $no_laboral !!};
    var desde = new Date( "{!! $fechac !!}" );

    // console.log(desde);
    // console.log(nolaboral);
    // console.log(validacion);

		  $("#fechaAux").datepicker({
			datesDisabled: nolaboral,
			autoclose: true,
			daysOfWeekDisabled: validacion,
      //daysOfWeekHighlighted: validacion,
			format: 'yyyy-mm-dd',
			language: 'es',
			startDate: desde,
      orientation: "left bottom"
		  });

      $(".group-date").datepicker({
  			datesDisabled: nolaboral,
  			autoclose: true,
  			daysOfWeekDisabled: validacion,
        //daysOfWeekHighlighted: validacion,
  			format: 'yyyy-mm-dd',
  			language: 'es',
  			startDate: desde,
        orientation: "left bottom"
		  });

		});
  $(document).ready(function(){

    $('#Calendario-table').DataTable({
      "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [3]
      }],
      "searching": false,
      "lengthChange": false,
      "lengthMenu": [[50], [50]],
      "order": [[ 0, 'asc' ]],
      "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      }
    });
  });

 //Resetear al momento de guardar
  $("#fecha,input[name=estatus]").change(function() {
    if ('form-create' != '')
    {
        $('#guardarCalendario').attr("disabled", false);
    }
  });

  $( "#form-create" ).on("submit", function( event ) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro de realizar los cambios en bancarios y feriados?',
      text: " ",
      icon: "warning",
      buttons: ['Cancelar', 'Aceptar'],
      closeModal: true
    }).then((confirmed) => {
      if (confirmed) {
        $("#form-create").off("submit").submit();
      }
    });
  });

  //FUNCIÓN PARA ELIMINAR UN BANCO
  var Eliminar = function(id, fecha, estatus)
  {
    // ALERT JQUERY
    swal({
      title: '¿Está seguro que quiere eliminar el día no laborable?',
      text: fecha.replace(/&nbsp;/g, " "),
      icon: "warning",
      buttons: ['Cancelar', 'Aceptar'],
      closeModal: true
    }).then((confirmed) => {
      if (confirmed) {
        var route = "{{url('Configuracion/Calendario/DiasExcepcionales/NoLaborables')}}/"+id+"";
        var token = $("#token").val();
        $.ajax({
          url: route,
          headers: {'X-CSRF-TOKEN': token},
          type: 'DELETE',
          dataType: 'json',
          success: function(data) {
            if (data.success == 'true')
            {
              //Alerta de Confirmación
              swal({
                title: 'Día no laborable eliminado correctamente',
                text: " ",
                icon: 'success',
                timer: 3000,
                buttons: false,
                closeModal: true
              });
              setTimeout(function(){
                var url="{{ url('Configuracion/Calendario/DiasExcepcionales/NoLaborables')}}";
                location.href = url;
              }, 3200);
            }
          },
          error:function(data)
          {
            switch (data.status) {
              case 400:
                $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
                break;
              case 419:
              case 422:
                var errors = '<ul>';
                for (datos in data.responseJSON)
                {
                  errors += '<li>' +data.responseJSON[datos] + '</li>';
                }
                errors += '</ul>';
                $("#message-error-delete").show().html(errors);
                break;
              case 401:
                $("#message-error-delete").show().html("Acceso no autorizado.");
                break;
              case 403:
                $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
                break;
              case 405:
                $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
                break;
              case 500:
                $("#message-error-delete").show().html("Error Interno del Servidor.");
                break;
              case 503:
                $("#message-error-delete").show().html("Servicio no disponible.");
                break;
            }
          }
        });
      }
    });
  };
</script>
