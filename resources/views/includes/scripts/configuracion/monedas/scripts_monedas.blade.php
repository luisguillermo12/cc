<script type="text/javascript">

  $(document).ready(function() {

    /*$('#TiposMonedas-table').DataTable({
      "scrollCollapse": true,
      "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [4]
      }],
      "searching": true,
      "lengthChange": true,
      "lengthMenu": [[30, 50, 100], [30, 50, 100]],
      "order": [[ 0, 'asc' ]],
      "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      }
    });*/

    $("#num_decimales").keypress(soloNumeros);
    $("#limite_financiero").keypress(soloNumeros);
    $("#monto_max_bv").keypress(soloNumeros);

    $("input[name=estatus]").on('change', function() {
      $('#actualizarTipoMoneda').attr("disabled", false);
    });

    $("#nombre,#num_decimales").on('keypress', function() {
      $('#actualizarTipoMoneda').attr("disabled", false);
    });
  });

  function soloNumeros(evt) {
    $('#actualizarTipoMoneda').attr("disabled", false);
		var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

    return true;
	}

  $( "#form-create" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere crear este código ISO?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-create").off("submit").submit();
      }
    });
  });

  $( "#form-edit" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar este participante?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-edit").off("submit").submit();
      }
    });
  });

  //FUNCIÓN PARA ELIMINAR MONEDA
  var Eliminar = function(id, nombre){
   swal({
    title: '¿Está seguro que quiere eliminar el código ISO?',
    text: 'Usted no podrá recuperar el registro para el código ISO ' + nombre,
    icon: 'warning',
    buttons: [true, "Aceptar"]
  }).then((result) => {

    if (result) {
      $.ajax({
        url: "{{ url('Configuracion/Monedas/CodigoISO') }}/" + id,
        type: 'POST',
        dataType: 'json',
        data: {
          "id": id,
          "_method": 'DELETE',
          "_token": "8K4stuPHOlVHXPmH46tLC6JZnYv0Li4Z7lAFOjt2",
        },
        success: function(data) {
          if (data.success == 'true')
          {
            location.href = "{{ url('Configuracion/Monedas/CodigoISO') }}/";
            //Alerta de Confirmación
            swal({
              title: 'Sub-producto eliminado satisfactoriamente',
              text: "",
              icon: 'success'
            });
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};


</script>
