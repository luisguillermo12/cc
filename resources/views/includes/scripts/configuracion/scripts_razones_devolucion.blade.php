<script type="text/javascript">
  $(document).ready(function() {
    $( "#nombre").autocomplete({
      source: "autocomplete",
      minLength: 3,
      select: function(event, ui) {
        $('#nombre').val(ui.item.value);
      }
    });
    $( "#cod_tipdev").autocomplete({
      source: "autocompleteCodigo",
      minLength: 2,
      select: function(event, ui) {
        $('#cod_tipdev').val(ui.item.value);
      }
    });
  });

  $(document).ready(function(){

  /*$('#RazonesDevolucion-table').DataTable({
    "scrollCollapse": true,
    "aoColumnDefs": [{
      "bSortable": false,
      "aTargets": false,
    }],
    "searching": true,
    "lengthChange": true,
    "lengthMenu": [[30, 50, 100], [30, 50, 100]],
    "order": [[ 0, 'asc' ]],
    "language": {
      "sProcessing":    "Procesando...",
      "sLengthMenu":    "Mostrar _MENU_ registros",
      "sZeroRecords":   "No se encontraron resultados",
      "sEmptyTable":    "Ningún dato disponible en esta tabla",
      "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":   "",
      "sSearch":        "Buscar:",
      "sUrl":           "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":    "Último",
        "sNext":    "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    }
  });*/

  $("#codigo").keypress(soloNumeros);
  $("#tipo_operacion_id,input[name=estatus]").on('change', function() {
    $('#actualizarTipoDevolucion').attr("disabled", false);
  });

  $("#nombre").on('keypress', function() {
    $('#actualizarTipoDevolucion').attr("disabled", false);
  });
});

  function soloNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }

  $( "#form-create" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar este participante?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-create").off("submit").submit();
      }
    });
  });

  $( "#form-edit" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar este participante?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-edit").off("submit").submit();
      }
    });
  });

//FUNCIÓN PARA ELIMINAR UN RAZONES DE DEVOLUCIONES
  var Eliminar = function(id, nombre){
   swal({
    title: '¿Está seguro que quiere eliminar la razón de devolución?',
    text: 'Usted no podrá recuperar el registro para para la razón de devolución ' + nombre,
    icon: 'warning',
    buttons: [true, "Aceptar"]
  }).then((result) => {

    if (result) {
      $.ajax({
        url: "{{ url('Configuracion/MediosPagos/RazonesDevolucion') }}/" + id,
        type: 'POST',
        dataType: 'json',
        data: {
          "id": id,
          "_method": 'DELETE',
          "_token": "{{ csrf_token() }}",
        },
        success: function(data) {
          if (data.success == 'true')
          {
            location.href = "{{ url('Configuracion/MediosPagos/RazonesDevolucion') }}";
            //Alerta de Confirmación
            swal({
              title: 'Razón de devolución eliminada satisfactoriamente',
              text: "",
              icon: 'success'
            });
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};

</script>
