<script type="text/javascript">

$( "#form-edit" ).submit(function( event ) {
  event.preventDefault(event);
  swal({
    title: '¿Está seguro de que quiere actualizar la Validación de la solución?',
    icon: "warning",
    buttons:['Cancelar','Aceptar']
  }).then((result)=> {
    if (result) {
      $("#form-edit").off("submit").submit();
    }
  });
});


function soloNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }

    $("#rifselecemp").keypress(soloNumeros);
    $("#codigo_interno_error").keypress(soloNumeros);
    $("#nivel").keypress(soloNumeros);
    $("#respuesta_codigo").keypress(soloNumeros);

    $("#codit").keypress(soloNumeros);
    $("#codrt").keypress(soloNumeros);

</script>
