<script type="text/javascript">
function habilitarBoton() {
  $("#actualizar").prop("disabled", false);
}

function actualizarOperacion() {

  var direccion = $('input[name=factura]:checked').val();

  if (direccion != null) {

       $.ajax({
          url: '/Configuracion/Cobranza/RecuperacionCostos/Multiple/'+direccion,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          type: 'POST',
          dataType: 'json',
          success: function(data) {
          location.reload();
          swal({
              title: 'Recuperacion de costos actualizada satisfactoriamente',
              text: "",
              icon: 'success'
      });
       }
    });
  }
}


  $( "#form-edit" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar este producto?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-edit").off("submit").submit();
      }
    });
  });
</script>
