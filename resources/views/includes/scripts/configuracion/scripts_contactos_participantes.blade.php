<script type="text/javascript">

  $(document).ready(function(){
    $("[data-mask]").inputmask();
    $("#telefono_celular").keypress(soloNumeros);
    $("#telefono_local").keypress(soloNumeros);
    $("#cedula").keypress(soloNumeros);
    $("#banco_id,#tipo_contacto_id,#turno_id,#posicion_escalamiento_id,input[name=estatus]").on('change', function() {
      $('#actualizarContactoBancario').attr("disabled", false);
    });

    $("#cedula,#nombre,#cargo,#email").on('keypress', function() {
      $('#actualizarContactoBancario').attr("disabled", false);
    });
  });

  function soloNumeros(evt) {
    $('#actualizarContactoBancario').attr("disabled", false);
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
  }

  $('#cod_banco').on('change', function(e){
    $(this).closest('form').submit();
});

$('#cod_nivel').on('change', function(e){
    $(this).closest('form').submit();
});

  $( "#form-create" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere agregar el contacto participante?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-create").off("submit").submit();
      }
    });
  });

  $( "#form-edit" ).submit(function( event ) {
  event.preventDefault(event);
  swal({
    title: '¿Está seguro que quiere actualizar el contacto participante?',
    icon: "warning",
    buttons:['Cancelar','Aceptar']
  }).then((result)=> {
    if (result) {
      $("#form-edit").off("submit").submit();
    }
  });
});

 
    //BOTON ELIMINAR
  var Eliminar = function(id, nombre)
  {
    // ALERT JQUERY
    swal({
      title: '¿Está seguro que quiere eliminar el tipo de contacto?',
      text: ' Usted no podrá recuperar el registro ' + nombre,
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        

        var route = "{{ url('Configuracion/Contactos/Participantes')}}/"+id+"";
        var token = $("#token").val();
        $.ajax({
          url: route,
          headers: {'X-CSRF-TOKEN': token},
          type: 'DELETE',
          dataType: 'json',
          success: function(data) {
            if (data.success == 'true')
            {
              var url='{{ url('Configuracion/Contactos/Participantes')}}';
              location.href = url;
              //Alerta de Confirmación
              swal({
                title: "Tipo de Contacto Eliminado!",
                text: "!",
                icon: "success",
              });
            }
          },
          error:function(data)
          {
            switch (data.status) {
              case 400:
                $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
                break;
              case 422:
                var errors = '<ul>';
                for (datos in data.responseJSON)
                {
                  errors += '<li>' +data.responseJSON[datos] + '</li>';
                }
                errors += '</ul>';
                $("#message-error-delete").show().html(errors);
                break;
              case 401:
                $("#message-error-delete").show().html("Acceso no autorizado.");
                break;
              case 403:
                $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
                break;
              case 405:
                $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
                break;
              case 500:
                $("#message-error-delete").show().html("Error Interno del Servidor.");
                break;
              case 503:
                $("#message-error-delete").show().html("Servicio no disponible.");
                break;
            }
          }
        });
      }
    });
  };

  </script>
