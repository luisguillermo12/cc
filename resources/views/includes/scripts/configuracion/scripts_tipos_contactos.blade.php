<script type="text/javascript">

  $(document).ready(function(){

    $("input[name=estatus]").on('change', function() {
      $('#actualizarTipoContacto').attr("disabled", false);
    });

    $("#nombre").on('keypress', function() {
      $('#actualizarTipoContacto').attr("disabled", false);
    });

  });

  $( "#form-create" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere agregar el tipo de contacto?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-create").off("submit").submit();
      }
    });
  });

  $( "#form-edit" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar el tipo de contacto?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-edit").off("submit").submit();
      }
    });
  });

  //BOTON ELIMINAR
  var Eliminar = function(id, nombre)
  {
    // ALERT JQUERY
    swal({
      title: '¿Está seguro que quiere eliminar el tipo de contacto?',
      text: ' Usted no podrá recuperar el registro ' + nombre,
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        

        var route = "{{ url('Configuracion/Contactos/Tipos')}}/"+id+"";
        var token = $("#token").val();
        $.ajax({
          url: route,
          headers: {'X-CSRF-TOKEN': token},
          type: 'DELETE',
          dataType: 'json',
          success: function(data) {
            if (data.success == 'true')
            {
              var url='{{ url('Configuracion/Contactos/Tipos')}}';
              location.href = url;
              //Alerta de Confirmación
              swal({
                title: "Tipo de Contacto Eliminado!",
                text: "!",
                icon: "success",
              });
            }
          },
          error:function(data)
          {
            switch (data.status) {
              case 400:
                $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
                break;
              case 422:
                var errors = '<ul>';
                for (datos in data.responseJSON)
                {
                  errors += '<li>' +data.responseJSON[datos] + '</li>';
                }
                errors += '</ul>';
                $("#message-error-delete").show().html(errors);
                break;
              case 401:
                $("#message-error-delete").show().html("Acceso no autorizado.");
                break;
              case 403:
                $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
                break;
              case 405:
                $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
                break;
              case 500:
                $("#message-error-delete").show().html("Error Interno del Servidor.");
                break;
              case 503:
                $("#message-error-delete").show().html("Servicio no disponible.");
                break;
            }
          }
        });
      }
    });
  };

</script>
