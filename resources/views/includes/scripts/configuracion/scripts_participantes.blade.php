<script type="text/javascript">

  /*$('#BancosInactivos-table').DataTable({
    "scrollCollapse": true,
    "aoColumnDefs": [{
      "bSortable": false,
      "aTargets": [5]
    }],
    "searching": true,
    "lengthChange": true,
    "lengthMenu": [[30, 50, 100], [30, 50, 100]],
    "order": [[ 0, 'asc' ]],
    "language": {
        "sProcessing":    "Procesando...",
        "sLengthMenu":    "Mostrar _MENU_ registros",
        "sZeroRecords":   "No se encontraron resultados",
        "sEmptyTable":    "Ningún dato disponible en esta tabla",
        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":   "",
        "sSearch":        "Buscar:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":    "Último",
            "sNext":    "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
  });*/

  //Resetear Select Banco Representante al seleccionar radio con la opción DIRECTO - MODAL CREAR BANCO
  $('input[type=radio][name=modalidad_participacionAdd]').change(function() {
    if (this.value == '1') {
      $("#banco_representante_idAdd").val('');
    }
  });

  //Resetear Select Banco Representante al seleccionar radio con la opción DIRECTO - MODAL EDITAR BANCO
  $('input[type=radio][name=modalidad_participacion]').change(function() {
    $('#actualizarBanco').attr("disabled", false);
    if (this.value == '1') {
      $("#banco_representante_id").val('');
    }
  });

  $("#codigo").keypress(soloNumeros);

  $("#banco_representante_id,#fecha,input[name=estatus]").on('change', function() {
    $('#actualizarBanco').attr("disabled", false);
  });

  $("#nombre").on('keypress', function() {
    $('#actualizarBanco').attr("disabled", false);
  });

  function soloNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

    return true;
  }

  $( "#form-create" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar este participante?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-create").off("submit").submit();
      }
    });
  });

  $( "#form-edit" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar este participante?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-edit").off("submit").submit();
      }
    });
  });

  $( "#form-fusionar" ).submit(function( event ) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere fusionar el participante?',
      type: "warning",
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar',
      closeOnConfirm: true
    }, function(confirmed) {
      if (confirmed) {
        $("#form-fusionar").off("submit").submit();
      }
    });
  });

//FUNCIÓN PARA ELIMINAR UN BANCO
var Eliminar = function(id, nombre){
 swal({
  title: '¿Está seguro que quiere eliminar el participante?',
  text: 'Usted no podrá recuperar el registro para el participante ' + nombre,
  icon: 'warning',
  buttons: [true, "Aceptar"]
}).then((result) => {

  if (result) {
    console.log(id, nombre, "{{ csrf_token() }}");
      $.ajax({
        url: "{{ url('Configuracion/Participantes') }}/" + id,
        type: 'POST',
        dataType: 'json',
        data: {
          "id": id,
          "_method": 'DELETE',
          "_token": "{{ csrf_token() }}",
        },
        success: function(data) {
          if (data.success == 'true')
          {
            location.href = "{{ url('Configuracion/Participantes/Inactivos') }}";
            //Alerta de Confirmación
            swal({
              title: 'Usuario eliminado satisfactoriamente',
              text: "",
              icon: 'success'
            });
          }
        },
        error:function(data)
        {
          switch (data.status) {
            case 400:
            $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
            break;
            case 422:
            var errors = '<ul>';
            for (datos in data.responseJSON)
            {
              errors += '<li>' +data.responseJSON[datos] + '</li>';
            }
            errors += '</ul>';
            $("#message-error-delete").show().html(errors);
            break;
            case 401:
            $("#message-error-delete").show().html("Acceso no autorizado.");
            break;
            case 403:
            $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
            break;
            case 405:
            $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
            break;
            case 500:
            $("#message-error-delete").show().html("Error Interno del Servidor.");
            break;
            case 503:
            $("#message-error-delete").show().html("Servicio no disponible.");
            break;
          }
        }
      });
    }
  });
};



// Deshabilitar listados
function disableRepresentantes(estado) {
  $('#banco_representante_id').prop('disabled', estado);
  $("#banco_representante_id").prop('required', !estado);
  if (estado == true) {
    $("#banco_representante_id").val('');
    $('#estatus_contenedor').show();
  } else {
    $('#estatus_contenedor').hide();
  }

}

if ($('#modalidad').is(':checked')) {
  disableRepresentantes(true);
}

function disablePrincipales(estado) {
  $('#banco_principal_id').prop('disabled', estado);
  $("#banco_principal_id").prop('required', !estado);
  if (estado == true) {
    $("#banco_principal_id").val('');
    $('#modalidad_contenedor').show();
  } else {
    $('#modalidad_contenedor').hide();
  }
}

if ($("#FUSIONADO").is(":checked")) {
  disablePrincipales(false);
} else {
  disablePrincipales(true);
}

</script>
