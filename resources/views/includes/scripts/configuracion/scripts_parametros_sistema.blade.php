<script type="text/javascript">
  /*
  $(document).ready(function(){

    $('#EmpresasOrdenantes-table').DataTable({
      "searching": true,
      "lengthChange": true,
      "lengthMenu": [[30, 50, 100], [30, 50, 100]],
    "order": [[ 0, 'asc' ]],
      "language": {
          "sProcessing":    "Procesando...",
          "sLengthMenu":    "Mostrar _MENU_ registros",
          "sZeroRecords":   "No se encontraron resultados",
          "sEmptyTable":    "Ningún dato disponible en esta tabla",
          "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":   "",
          "sSearch":        "Buscar:",
          "sUrl":           "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      }
    });
  });
  */

  $( "#form-edit" ).submit(function (event) {
    event.preventDefault(event);
    swal({
      title: '¿Está seguro que quiere actualizar el parámetro de la solución?',
      icon: 'warning',
      buttons: ['Cancelar', "Aceptar"]
    }).then((result) => {
      if (result) {
        $("#form-edit").off("submit").submit();
      }
    });
  });

</script>