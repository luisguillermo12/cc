<script type="text/javascript">

$(document).ready(function() {

  function getDisk_C() {
    var disk = $.ajax({
      url: "{{ url('EjecucionAdministrativa/MonitoreoProcesos/diskc') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);

          if ((data.responseJSON[0]) == 0) {
            $("#C").addClass('bg-danger-disabled text-center');
            $("#total").html('Particion no encontrada !');
            $("#libre").html('Particion no encontrada !');
            $("#ocupado").html('Particion no encontrada !');
            $("#unidad").html(data.responseJSON[3]);


          }else{
            $("#C").addClass('bg-success-disabled text-center');
            $("#total").html(data.responseJSON[0]);
            $("#libre").html(data.responseJSON[1]);
            $("#ocupado").html(data.responseJSON[2]);
            $("#unidad").html(data.responseJSON[3]);
          }
          var timeout = setTimeout(getDisk_C, 1 * 1000 * 60);
        },
        error:function(data){
          console.log(data);
        }
    });
  }
  getDisk_C();

  function getDisk_X() {
    var disk = $.ajax({
      url: "{{ url('EjecucionAdministrativa/MonitoreoProcesos/diskx') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);

          if ((data.responseJSON[0]) == 0) {
            $("#X").addClass('bg-danger-disabled text-center');
            $("#totalX").html('Particion no encontrada !');
            $("#libreX").html('Particion no encontrada !');
            $("#ocupadoX").html('Particion no encontrada !');
            $("#unidadX").html(data.responseJSON[3]);

          }else{
            $("#X").addClass('bg-success-disabled text-center');
            $("#totalX").html(data.responseJSON[0]);
            $("#libreX").html(data.responseJSON[1]);
            $("#ocupadoX").html(data.responseJSON[2]);
            $("#unidadX").html(data.responseJSON[3]);

          }
          var timeout = setTimeout(getDisk_X, 1 * 1000 * 60);
        },
        error:function(data){
          console.log(data);
        }
    });
  }
  getDisk_X();

  function getDisk_Y() {
    var disk = $.ajax({
      url: "{{ url('EjecucionAdministrativa/MonitoreoProcesos/disky') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);

          if ((data.responseJSON[0]) == 0) {

            $("#Y").addClass('bg-danger-disabled text-center');
            $("#totalY").html('N/A');
            $("#libreY").html('N/A');
            $("#ocupadoY").html('N/A');
            $("#unidadY").html(data.responseJSON[3]);

          }else{
            $("#Y").addClass('bg-success-disabled text-center');
            $("#totalY").html(data.responseJSON[0]);
            $("#libreY").html(data.responseJSON[1]);
            $("#ocupadoY").html(data.responseJSON[2]);
            $("#unidadY").html(data.responseJSON[3]);
          }
          var timeout = setTimeout(getDisk_Y, 1 * 1000 * 60);
        },
        error:function(data){
          console.log(data);
        }
    });
  }
  getDisk_Y();

  function getDisk_F() {
    var disk = $.ajax({
      url: "{{ url('EjecucionAdministrativa/MonitoreoProcesos/diskf') }}",
      type: 'GET',
        complete: function (data){
          clearTimeout(timeout);

          if ((data.responseJSON[0]) == 0) {

            $("#F").addClass('bg-danger-disabled text-center');
            $("#totalF").html('Particion no encontrada !');
            $("#libreF").html('Particion no encontrada !');
            $("#ocupadoF").html('Particion no encontrada !');
            $("#unidadF").html(data.responseJSON[3]);

          }else{
            $("#F").addClass('bg-success-disabled text-center');
            $("#totalF").html(data.responseJSON[0]);
            $("#libreF").html(data.responseJSON[1]);
            $("#ocupadoF").html(data.responseJSON[2]);
            $("#unidadF").html(data.responseJSON[3]);

          }
          var timeout = setTimeout(getDisk_F, 1 * 1000 * 60);
        },
        error:function(data){
          console.log(data);
        }
    });
  }
  getDisk_F();
});

function orden(columna) {
  $('#columna').val(columna);

  if($('#3'+columna).hasClass('fa-sort-amount-asc') && $('#orden').val() == 'ASC'){
    $('#3'+columna).removeClass( "fa-sort-amount-asc" ).addClass("fa-sort-amount-desc");
  }else{
    $('#3'+columna).removeClass( "fa-sort-amount-desc" ).addClass("fa-sort-amount-asc");
  }
  if($('#5'+columna).hasClass('fa-sort-amount-asc') && $('#orden').val() == 'ASC'){
    $('#5'+columna).removeClass( "fa-sort-amount-asc" ).addClass("fa-sort-amount-desc");
  }else{
    $('#5'+columna).removeClass( "fa-sort-amount-desc" ).addClass("fa-sort-amount-asc");
  }

  if($('#3'+columna).hasClass('fa-long-arrow-down')){
    $('#3'+columna).removeClass( "fa-long-arrow-down" ).addClass("fa-sort-amount-asc");
    $('#4'+columna).removeClass( "fa-long-arrow-up" );
  }
  if($('#5'+columna).hasClass('fa-long-arrow-down')){
    $('#5'+columna).removeClass( "fa-long-arrow-down" ).addClass("fa-sort-amount-asc");
    $('#6'+columna).removeClass( "fa-long-arrow-up" );
  }

  if(columna=='nombre_job'){
    $('#5clasificacion_job').removeClass( "fa-sort-amount-asc" ).removeClass("fa-sort-amount-desc");
  }else{
    $('#3nombre_job').removeClass( "fa-sort-amount-asc" ).removeClass("fa-sort-amount-desc");
  }

  if($('#3nombre_job').hasClass('fa-sort-amount-asc')||$('#3nombre_job').hasClass('fa-sort-amount-desc')){
    $('#5clasificacion_job').removeClass( "fa-sort-amount-asc" ).removeClass( "fa-sort-amount-desc" ).addClass("fa-long-arrow-down");
    $('#6clasificacion_job').addClass( "fa-long-arrow-up" );
  }else if($('#5clasificacion_job').hasClass('fa-sort-amount-asc')||$('#5clasificacion_job').hasClass('fa-sort-amount-desc')){
    $('#3nombre_job').removeClass( "fa-sort-amount-asc" ).removeClass( "fa-sort-amount-desc" ).addClass("fa-long-arrow-down");
    $('#4nombre_job').addClass( "fa-long-arrow-up" );
  }

  if ($('#orden').val() == 'ASC') {
    $('#orden').val('DESC');
  } else {
    $('#orden').val('ASC');
  }
}

</script>

<script type="text/javascript">

  var intervalo;

  new Vue({
   el: '#app',
   data: {
     items: [],
     interval: null,
     orden: "",
     columna: "",
     busqueda:""
   },
   created: function() {

     this.ajax();

     intervalo = setInterval(function () {
       this.ajax();
     }.bind(this), 5000);

   },
   methods: {
     ajax: function() {
       this.obtenerOrden();
       this.$http.get("/EjecucionAdministrativa/MonitoreoProcesos/Consultar?columna="+this.columna+"&orden="+this.orden+"&busqueda="+this.busqueda).then(function(response) {
         //console.log(response.body);
         this.items = response.body;
       }, function(){
         console.log(this.items);
       });
     } ,
     obtenerOrden: function() {
       this.columna = $('#columna').val();
       this.orden = $('#orden').val();
     }
   }
  });

  function compilarBD() {
   $("#compilar").addClass('disabled');
   $("#compilar").html('Compilando objetos de BD');

   $.ajax({
     url: "{{ url('EjecucionAdministrativa/MonitoreoProcesos/compilarBD') }}",
     success: function(result){
       location.reload();
   }});
  }
</script>

<style>
  .fa-long-arrow-up,
  .fa-long-arrow-down{
    color: #9fbecf;
  }
  .fa-sort-amount-asc,
  .fa-sort-amount-desc{
    color: #6d828e;
  }
</style>
