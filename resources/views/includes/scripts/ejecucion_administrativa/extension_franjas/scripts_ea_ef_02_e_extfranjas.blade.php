<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/timepicker/bootstrap-timepicker.min.css') }}">
<script type="text/javascript" src="{{ asset('js/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script type="text/javascript">
 $(function () {

    $(document).ready(function(){ //picker de horas

       $('.fechahora_fin').timepicker({
         showMeridian: false,
         minuteStep: 1,
       });

      //para lanzar el evento de cambio al dar click en incrementar/decrementar el valor del campo, y vue pueda detectar el mismo
      $('div.bootstrap-timepicker div.dropdown-menu table tbody tr td a').on('mouseup', function(){
        setTimeout(function(){
          document.querySelector('.fechahora_fin').dispatchEvent(new Event('change'));
        }, 100);
      });
      $('div.bootstrap-timepicker div.dropdown-menu table tbody tr td input').on('blur', function(){
        setTimeout(function(){
          document.querySelector('.fechahora_fin').dispatchEvent(new Event('change'));
        }, 100);
      });

    });

    //elementos de alertas
    $( "#form-edit" ).on("submit", function( event ) {
      event.preventDefault(event);
      swal({
        title: '¿Está seguro de que quiere actualizar la hora final de la sesión de la franja?',
        text: " ",
        icon: "warning",
        buttons: ['Cancelar', 'Aceptar'],
        closeModal: true
      }).then((confirmed) => {
        if (confirmed) {
          $("#form-edit").off("submit").submit();
        }
      });
    });

    //elementos de los graficos
    var horas = [
      <?php for ($i=0; $i < count($rangos_hora); $i++) {
        if($i != (count($rangos_hora)-1)){
          echo '"'.$rangos_hora[$i].'",';
        }else {
          echo '"'.$rangos_hora[$i].'"';
        }
      } ?>
    ];

    var nombre_archivo = 'Reporte_Extension_Franjas';

    var chartLiq = Highcharts.setOptions({
      lang: {

        loading: 'Cargando...',
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        exportButtonTitle: "Exportar",
        printButtonTitle: "Importar",
        rangeSelectorFrom: "Desde",
        rangeSelectorTo: "Hasta",
        rangeSelectorZoom: "Período",
        downloadPNG: 'Descargar imagen PNG',
        downloadJPEG: 'Descargar imagen JPEG',
        downloadPDF: 'Descargar imagen PDF',
        downloadSVG: 'Descargar imagen SVG',
        printChart: 'Imprimir',
        resetZoom: 'Reiniciar zoom',
        resetZoomTitle: 'Reiniciar zoom',
        thousandsSep: ".",
        decimalPoint: ',',
        numericSymbols: [' mil', ' millones']
      }
    });

    //Gráfico de xrange
    <?php $i = 0; ?>
    <?php foreach ($operaciones as $operaciones_det) { ?>

      var colores = [
        <?php
          if(substr($operaciones_det->descripcion,0,3)=='010'){
            echo "'#79c447', '#79c447'";
          }elseif(substr($operaciones_det->descripcion,0,3)=='020'){
            echo "'#fabb3d', '#fabb3d'";
          }else{
            echo "'#67c2ef', '#67c2ef'";
          }
        ?>
      ];

    chartLiq = Highcharts.chart('container<?= $i ?>', {
        chart: {
            type: 'xrange',
            spacingTop: 50,
            spacingBottom: 30,
            height: 300
        },
        title: {
            text: 'Distribución del intervalo de liquidación'
        },
        subtitle: {
            text: 'Operación: <?=substr($operaciones_det->descripcion,6,strlen(utf8_decode($operaciones_det->descripcion)))?>'
        },
        xAxis: {
           categories: horas,
           type: 'category',
           tickInterval: 30,
           gridLineWidth: 1,
           gridLineColor: '#ccd6eb',
           labels: {
            	//step: 1,
              rotation: -90
            }
        },
        yAxis: {
            categories: ['Período de liquidación','Franja de presentadas', 'Franja de devueltas'],
            reversed: true,
            gridLineWidth: 0,
            title: {
                text: ''
            },
        },
        tooltip: {
            backgroundColor: '#ffffff',
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"><?=substr($operaciones_det->descripcion,6,strlen(utf8_decode($operaciones_det->descripcion)))?></td>' +
                '</tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        series: [{
            borderColor: '#ffffff',
            colorByPoint: true,
            colors: colores,
            borderRadius: 0,
            pointWidth: 20,
            showInLegend: false,
            data: <?=$operaciones_det->datos?>,
            dataLabels: {
                enabled: true,
                formatter: function() {
                    var percents = this.point.partialFill
                    return percents ? '' : ''
                }
            }
        }],
        exporting: {
            filename: nombre_archivo,
            sourceWidth: 1280,
            sourceHeight: 968
        }
    });
    <?php $i++; ?>
    <?php } ?>

var operaciones_det = <?php foreach ($operaciones as $operaciones_det) { echo $operaciones_det->datos; }?>;

new Vue({
 el: '#app',
 data: {
   hora_fin: [],
   num_liquidacion: [],
   num_franja: []
 },
 created: function() {

   this.handleGraph();

 },
 methods: {
   handleGraph: function() {
     var rangos_hora = horas; //permite generar el rango de horas en js
     var inicio_franjas = 2; //posicion donde inicia la data de franjas en el json

     this.obtainInfo(); //para obtener la informacion al momento de cambio desde la pagina

     var j = 0;
     for(var i=0; i < this.hora_fin.length; i++){ //para buscar la posicion de la hora fin en el rango, segun lo colocado en el campo
       if(operaciones_det[j+inicio_franjas].y == this.num_franja && operaciones_det[j+inicio_franjas].nl == this.num_liquidacion){ //se valida que sea la misma franja de la misma liquidacion
         operaciones_det[j+inicio_franjas].x2 = this.findHora(this.hora_fin[i].substr(0,5),rangos_hora);
       }else {
         j++;
         i--;
       }
     }

     //luego de actualizar el objeto base con la nueva data modificada, se actualiza el grafico
     chartLiq.update({
        series: {
         data: operaciones_det
        }
      });

   },
   obtainInfo: function(){
     var i = 0;
     var horas_fin = new Array();
     var num_liquidacion = new Array();
     var num_franja = new Array();
     i = 0;
     $( ".hora_fin" ).each(function( index ) {
        horas_fin[i] = $( this ).val() ;
        i++;
     });
     this.hora_fin = horas_fin;
     i = 0;
     $( ".num_liquidacion" ).each(function( index ) {
        num_liquidacion[i] = $( this ).val() ;
        i++;
     });
     this.num_liquidacion = num_liquidacion;
     i = 0;
     $( ".num_franja" ).each(function( index ) {
        num_franja[i] = $( this ).val() ;
        i++;
     });
     this.num_franja = num_franja;
   },
   findHora: function(hora,rangos_hora){
     var hora_pos = null;
     for (var i=0; i < rangos_hora.length; i++) {
       if(rangos_hora[i] == hora){
         hora_pos = i;
         break;
       }
     }
     return hora_pos == null ? 0 : hora_pos;
   }
 }
});

 });

</script>
