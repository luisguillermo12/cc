<script type="text/javascript">
$(function () {

    var horas = [
      <?php for ($i=0; $i < count($rangos_hora); $i++) {
        if($i != (count($rangos_hora)-1)){
          echo '"'.$rangos_hora[$i].'",';
        }else {
          echo '"'.$rangos_hora[$i].'"';
        }
      } ?>
    ];

    var nombre_archivo = 'Reporte_Extension_Franjas';

    Highcharts.setOptions({
      lang: {

        loading: 'Cargando...',
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        exportButtonTitle: "Exportar",
        printButtonTitle: "Importar",
        rangeSelectorFrom: "Desde",
        rangeSelectorTo: "Hasta",
        rangeSelectorZoom: "Período",
        downloadPNG: 'Descargar imagen PNG',
        downloadJPEG: 'Descargar imagen JPEG',
        downloadPDF: 'Descargar imagen PDF',
        downloadSVG: 'Descargar imagen SVG',
        printChart: 'Imprimir',
        resetZoom: 'Reiniciar zoom',
        resetZoomTitle: 'Reiniciar zoom',
        thousandsSep: ".",
        decimalPoint: ',',
        numericSymbols: [' mil', ' millones']
      }
    });

    //Gráfico de Columnas
    <?php $i = 0; ?>
    <?php foreach ($operaciones as $operaciones_det) { ?>

      var colores = [
        <?php
          if(substr($operaciones_det->descripcion,0,3)=='010'){
            echo "'#79c447', '#79c447'";
          }elseif(substr($operaciones_det->descripcion,0,3)=='020'){
            echo "'#fabb3d', '#fabb3d'";
          }else{
            echo "'#67c2ef', '#67c2ef'";
          }
        ?>
      ];

    Highcharts.chart('container<?= $i ?>', {
        chart: {
            type: 'xrange',
            spacingTop: 50,
            spacingBottom: 30,
            height: 300
        },
        title: {
            text: 'Distribución del intervalo de liquidación'
        },
        subtitle: {
            text: 'Operación: <?=substr($operaciones_det->descripcion,6,strlen(utf8_decode($operaciones_det->descripcion)))?>'
        },
        xAxis: {
           categories: horas,
           type: 'category',
           tickInterval: 30,
           gridLineWidth: 1,
           gridLineColor: '#ccd6eb',
           labels: {
            	//step: 1,
              rotation: -90
            }
        },
        yAxis: {
            categories: ['Período de liquidación','Franja de presentadas', 'Franja de devueltas'],
            reversed: true,
            gridLineWidth: 0,
            title: {
                text: ''
            },
        },
        tooltip: {
            backgroundColor: '#ffffff',
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"><?=substr($operaciones_det->descripcion,6,strlen(utf8_decode($operaciones_det->descripcion)))?></td>' +
                '</tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        series: [{
            borderColor: '#ffffff',
            colorByPoint: true,
            colors: colores,
            borderRadius: 0,
            pointWidth: 20,
            showInLegend: false,
            data: <?=$operaciones_det->datos?>,
            dataLabels: {
                enabled: true,
                formatter: function() {
                    var percents = this.point.partialFill
                    return percents ? '' : ''
                }
            }
        }],
        exporting: {
            filename: nombre_archivo,
            sourceWidth: 1280,
            sourceHeight: 968
        }
    });
    <?php $i++; ?>
    <?php } ?>

});
</script>
