<script>
  @if ($procesos[23]->valor == 1 && $procesos[24]->valor != 1)
  function verificarServidor() {
    $.ajax({
      url: "{{ url('EjecucionAdministrativa/CierreProcesos/VerificarServidor') }}",
      success: function(data) {
        console.log('Verificando servidor');
        if (data.resultado) {
          console.log(data.resultado);
          $('#estatusJobs').html('<span class="col-sm-12 badge badge-danger" style="font-size: 18px">No</span>');
          $('#iniciarJobs').removeClass('disabled');
        } else {
          console.log(data.resultado);
          $('#estatusJobs').html('<span class="col-sm-12 badge badge-warning" style="font-size: 18px">Verificando servidor</span>');
          var timeout = setTimeout(verificarServidor, 1000);
        }
      }
    });
  }

  $(function(){
    verificarServidor();
  });

  @endif
</script>
