<script type="text/javascript" src="{{ asset('js/highcharts/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/highcharts/highcharts-3d.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/highcharts/modules/exporting.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/highcharts/modules/xrange.js') }}"></script>
