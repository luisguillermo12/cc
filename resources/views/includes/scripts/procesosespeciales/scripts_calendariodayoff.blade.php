<script type="text/javascript">
  $(document).ready(function() {

    var fin = Date('<?php echo substr($hasta, 0, 4) . ',' .  substr($hasta, 4, 2) . ',' .  substr($hasta, 6, 2); ?>');
    var inicio = new Date('<?php echo substr($desde, 0, 4) . ',' .  substr($desde, 4, 2) . ',' .  substr($desde, 6, 2); ?>');

    var nolaboral = {!! json_encode($feriados) !!};

    // Se convierte en formato fecha cada una de las fechas que se trae desde la BD
    nolaboral.forEach(function (item, key) {
      nolaboral[key] = new Date(item.substring(0,4) + ', ' + item.substring(5,7) + ', ' + item.substring(8,10));
    });

    $("#fecha").datepicker({
      datesDisabled: nolaboral,
      daysOfWeekHighlighted: '',
      autoclose: true,
      format: 'yyyy-mm-dd',
      language: 'es',
      startDate: inicio,
      //maxDate: '0',
      endDate: fin
    });
  }); 

 //Resetear al momento de guardar
 $("#fecha,input[name=estatus]").change(function() {
  if ('form-create' != '')
  {
    $('#guardarCalendario').attr("disabled", false);
  }
});

 $( "#form-create" ).submit(function( event, estaus ) {
  event.preventDefault(event);
  swal({
    title: 'Estas seguro de añadir los dias como feriado o bancario  day off?',
    type: "warning",
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Aceptar',
    closeOnConfirm: true
  }, function(confirmed) {
    if (confirmed) {
      $("#form-create").off("submit").submit();
    }
  });
});

  //FUNCIÓN PARA ELIMINAR UN BANCO
  var Eliminar = function(id, fecha, estatus)
  {
    // ALERT JQUERY
    swal({
      title: '¿Está seguro que quiere eliminar el día del day off?',
      //text: ' Usted no podrá recuperar el registro ' + fecha,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar',
      closeOnConfirm: true
    }, function(confirmed) {
      if (confirmed) {
        var route = "{{url('feriado')}}/"+id+"";
        var token = $("#token").val();
        $.ajax({
          url: route,
          headers: {'X-CSRF-TOKEN': token},
          type: 'DELETE',
          dataType: 'json',
          success: function(data) {
            if (data.success == 'true')
            {
              var url='{{ url('ProcesosEspeciales/DayOff')}}';
              location.href = url;
              //Alerta de Confirmación
              swal({
                title: 'Día del day off eliminado',
                text: "",
                type: 'success'
              });
            }
          },
          error:function(data)
          {
            switch (data.status) {
              case 400:
              $("#message-error-delete").show().html("Servidor ha entendido la solicitud, pero el contenido de solicitud no es válida.");
              break;
              case 422:
              var errors = '<ul>';
              for (datos in data.responseJSON)
              {
                errors += '<li>' +data.responseJSON[datos] + '</li>';
              }
              errors += '</ul>';
              $("#message-error-delete").show().html(errors);
              break;
              case 401:
              $("#message-error-delete").show().html("Acceso no autorizado.");
              break;
              case 403:
              $("#message-error-delete").show().html("Prohibido recurso no se puede acceder.");
              break;
              case 405:
              $("#message-error-delete").show().html("Ha ocurrido un error en la aplicación.");
              break;
              case 500:
              $("#message-error-delete").show().html("Error Interno del Servidor.");
              break;
              case 503:
              $("#message-error-delete").show().html("Servicio no disponible.");
              break;
            }
          }
        });
      }
    });
  };
</script>
