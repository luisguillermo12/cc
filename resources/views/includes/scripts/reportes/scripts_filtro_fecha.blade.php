<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/timepicker/bootstrap-timepicker.min.css') }}">
<script type="text/javascript" src="{{ asset('js/timepicker/bootstrap-timepicker.min.js') }}"></script>

<!-- datepicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker3.min.css') }}">
<script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datepicker/locales/bootstrap-datepicker.es.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function() {
    var date = new Date();
    date.setDate(date.getDate()+1);

    var inicio = "<?php echo $fechas[1]; ?>";
    var fin = "<?php echo $fechas[0]; ?>";
    var feriados = <?php echo json_encode($feriados); ?>;

  $("#fechaAux").datepicker({
        format: "yyyymmdd",
        startDate: inicio,
        endDate: fin,
        language: "es",
        datesDisabled: feriados,
        daysOfWeekDisabled: [0,6],
        autoclose: true,
        orientation: "left bottom"
    });

  $(".group-date").datepicker({
        format: "yyyymmdd",
        startDate: inicio,
        endDate: fin,
        language: "es",
        datesDisabled: feriados,
        daysOfWeekDisabled: [0,6],
        autoclose: true,
        orientation: "left bottom"
    });

});
</script>
