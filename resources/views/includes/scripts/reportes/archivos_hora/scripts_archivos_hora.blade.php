<script type="text/javascript">
$(function () {

    var data_icom1 = <?php echo $ICOM1; ?>;
    var data_icom2 = <?php echo $ICOM2; ?>;

    var horas = [
      <?php for ($i=0; $i < count($rangos_hora); $i++) {
        if($i != (count($rangos_hora)-1)){
          echo '"'.$rangos_hora[$i].'",';
        }else {
          echo '"'.$rangos_hora[$i].'"';
        }
      } ?>
    ];

    var nombre_archivo = 'Reportes_Archivos_Intervalo';

    Highcharts.setOptions({
      lang: {

        loading: 'Cargando...',
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        exportButtonTitle: "Exportar",
        printButtonTitle: "Importar",
        rangeSelectorFrom: "Desde",
        rangeSelectorTo: "Hasta",
        rangeSelectorZoom: "Período",
        downloadPNG: 'Descargar imagen PNG',
        downloadJPEG: 'Descargar imagen JPEG',
        downloadPDF: 'Descargar imagen PDF',
        downloadSVG: 'Descargar imagen SVG',
        printChart: 'Imprimir',
        resetZoom: 'Reiniciar zoom',
        resetZoomTitle: 'Reiniciar zoom',
        thousandsSep: ".",
        decimalPoint: ',',
        numericSymbols: [' mil', ' millones']
      }
    });

    if(data_icom1 != null){
    //Gráfico de Columnas
    Highcharts.chart('container1', {
        chart: {
            type: 'column',
            spacingTop: 50,
            spacingBottom: 30,
            height: 500
            //height: 400
        },
        title: {
            text: 'Archivos presentados al período del <?=$rangoF_selec?>, en período de horas: <?=$rango_seleccionado?>'
        },
        subtitle: {
            //text: 'Fuente: Electronic Payment Suite (EPS)-ACH'
        },
        xAxis: {
          categories: horas,
            title: {
                      text: 'Intervalos'
                    }
            //crosshair: true
        },
        yAxis: {
            min: 0,
            allowDecimals:false,
            //tickInterval: 20000,
            gridLineColor: '#ccd6eb',
            title: {
                text: 'Cantidad de archivos'
            }
        },
        tooltip: {
            backgroundColor: '#ffffff',
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    inside: true,
                    rotation: 270
                },
            }
        },
        series: [{
                    name: 'Presentados',
                    color: '#1b5e20',
                    data: data_icom1
                  }],
        exporting: {
            filename: nombre_archivo,
            sourceWidth: 1280,
            sourceHeight: 968
        }
    });

    }

    if(data_icom2 != null){

    Highcharts.chart('container2', {
        chart: {
            type: 'column',
            spacingTop: 50,
            spacingBottom: 30,
            height: 500
            //height: 400
        },
        title: {
            text: 'Archivos devueltos al período del <?=$rangoF_selec?>, en período de horas: <?=$rango_seleccionado?>'
        },
        subtitle: {
            //text: 'Fuente: Electronic Payment Suite (EPS)-ACH'
        },
        xAxis: {
          categories: horas,
            title: {
                      text: 'Intervalos'
                    }
            //crosshair: true
        },
        yAxis: {
            min: 0,
            allowDecimals:false,
            //tickInterval: 20000,
            gridLineColor: '#ccd6eb',
            title: {
                text: 'Cantidad de archivos'
            }
        },
        tooltip: {
            backgroundColor: '#ffffff',
            headerFormat: '<span style="font-size:12px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    inside: true,
                    rotation: 270
                },
            }
        },
        series: [{
                    name: 'Devueltos',
                    color: '#79c447',
                    data: data_icom2
                  }],
        exporting: {
            filename: nombre_archivo,
            sourceWidth: 1280,
            sourceHeight: 968
        }
    });

    }

});
</script>
