<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/adminlte.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vue-resource.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/input-mask/jquery.inputmask.js') }}"></script>

@if(notify()->ready())
  <script type="text/javascript">
      swal({
        title: "{!! notify()->message() !!}",
        text: "{!! notify()->option('text') !!}",
        icon: "{!! notify()->type() !!}",
        closeModal: true,
        @if(notify()->option('timer'))
          timer: {{ notify()->option('timer') }},
          buttons: false
        @endif
      });
  </script>
@endif

<script type="text/javascript">
// Cambiar iconos de menos y más al ocultar secciones
// Se debe corregir para cuando hay varias secciones
  $(".card-tools button i.fa").click(function (event) {
    event.preventDefault();

    var icono = $(this);


    icono = setTimeout(function () {
  
      if (icono.closest('.card').hasClass("collapsed-card")) {
        icono.removeClass("fa-minus");
        icono.addClass("fa-plus");
      } else {
        icono.removeClass("fa-plus");
        icono.addClass("fa-minus");
      }

    }, 400);
  })
</script>

<script>
  $('.slimScroll').slimScroll({
    height: '100%',
    color: '#ffffff',
    alwaysVisible: false
  });
</script>

<script>
function hora_bd (){
  return new Date( "{{ fechaBd() }}" );
}
</script>

<script type="text/javascript">
  function completarCero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  function actualizarHora(fecha) {

    clearInterval(intervalo);

    hora_actual = new Date(fecha);

    horas = completarCero(hora_actual.getHours());
    minutos = completarCero(hora_actual.getMinutes());
    segundos = completarCero(hora_actual.getSeconds());

    $("#reloj").html("<font size='5' face='Arial' ><b>"+horas+":"+minutos+":"+segundos+"</b></font>");

    var intervalo = setTimeout(actualizarHora, 1000, hora_actual.setSeconds(hora_actual.getSeconds() + 1));
  }

  var hora_servidor = hora_bd();
  actualizarHora(hora_servidor);
</script>

<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>

@include('sweet::alert')

@yield('before-scripts-end')

@yield('after-scripts-end')
