{{-- @Nombre del programa:  Vista Principal ProEs->Exclusión de participante--}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}

@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-book"></i> Exclusión de participante</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-book"></i> Procesos especiales</a></li>
        <li class="breadcrumb-item active">Exclusión de participante</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{{-- Inicio de la vista --}}

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Exclusión</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body" style="margin: auto; ">
         
          {!! Form::open(['route' => 'exclusion.excluir', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) !!}

            <h4>Jornada actual de compensación: {{ $fecha }} </h4><br>
            <div class="form-group">
              <div class="col-sm-10">
                <p>Los campos marcados con un asterisco (*) son obligatorios, puede realizar la exclusión con un máximo de 5 participante.</p>
                <p></p>
                {!! Form::label('Participante (s) a excluir(*)', 'Participante a excluir(*)', array('class' => 'col-sm-4 control-label')) !!}
                <div class="col-sm-10">
                  {{ Form::select('banco[]', $bancos, null, ['class' => 'chosen-select', 'multiple','col-sm-10', 'data-placeholder' => 'Seleccione uno o más participantes','style'=>'width:nowrap; !important']) }}
                </div><br>
                {!!Form::label(' Descripción:', null, array('class' => 'col-sm-5 control-label'))!!}
                <div class="col-sm-12">
                  {!! Form::textarea('causa', null, ['class' => 'form-control col-sm-12', 'maxlength' => '199', 'rows' => "3"]) !!}
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body-->
          <div class="card-footer">
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Registrar</button>
            </div>      
          </div>
          {!! Form::close() !!}          
          
          <div>
           <h4><center>  No puede ejecutar exclusiones mientras el sistema se encuentre abierto.</center></h4>
          </div>
          
      </div>
      <!-- /.card -->
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <tr style="background-color: #c2e7fc;">
              <th><center>Código de operación</center></th>
              <th><center>Proceso especial</center></th>
              <th><center>Estado</center></th>
              <th><center>Fecha</center></th>
              <th><center>Participante</center></th>
            </tr>
            <tbody>
              @forelse ($reprocesos as $reproceso) 
                <tr>
                  <td ><center> {{ $reproceso->cod_operacion }} </center></td>
                  <td><center> {{ $reproceso->nombre_proceso }} </center></td>
                  @if($reproceso->estatus=='APROBADO')
                    <td style="background-color: #c4ffcb;border-top: 1px solid #ffffff;" ><center> {{ $reproceso->estatus }} </center></td>
                  @else 
                  @if($reproceso->estatus=='DENEGADO')
                    <td style="background-color: #ffb9b0;border-top: 1px solid #ffffff;" ><center>{{ $reproceso->estatus }} </center></td>
                  @else 
                    <td><center>{{ $reproceso->estatus }} </center></td>
                  @endif
                  @endif
                   <td><center> {{ Date::parse( $reproceso->fecha )  }} </center></td>
                   <td><center>{{ $reproceso->cod_banco }}-{{ $reproceso->nombre }}</center></td>
                </tr>
                @empty   
                <tr>
                  <td colspan="8"><center>No existen registros de exclusion pendientes.</center></td>
                </tr>
              @endforelse    
            </tbody>
          </table>
          @if($registrados>0)
            <div class="col-sm-12">
              <p class="text-center">
                <a href="{{url('ProcesosEspeciales/ExclusionParticipante/Aprobar')}}" class="btn btn-primary btn-success margin " role="button">Aprobar</a>
                <a href="{{url('ProcesosEspeciales/ExclusionParticipante/Denegar')}}" class="btn btn-primary btn-danger margin " role="button">Denegar</a>
              </p>
            </div>
          @endif
        </div>
        <!-- /.card-body-->
      </div>
      <!-- /.card -->
    </div>
  </div>

    <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado registros histórico</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <tr style="background-color: #c2e7fc;">
              <th><center>Código de operación</center></th>
              <th><center>Proceso especial</center></th>
              <th><center>Estado</center></th>
              <th><center>Fecha</center></th>
              <th><center>Participante</center></th>
            </tr>
            <tbody>
              @forelse ($reprocesosh as $reproceso) 
                <tr>
                  <td ><center> {{ $reproceso->cod_operacion }} </center></td>
                  <td><center> {{ $reproceso->nombre_proceso }} </center></td>

                  @if($reproceso->estatus=='APROBADO')
                    <td style="background-color:  #e8f9ea" ><center> {{ $reproceso->estatus }} </center></td>
                  @else 
                  @if($reproceso->estatus=='DENEGADO')
                    <td style="background-color:  #ffb9b0" ><center>{{ $reproceso->estatus }} </center></td>
                    @else 
                      <td><center>{{ $reproceso->estatus }} </center></td>
                    @endif
                  @endif
                  <td><center> {{ Date::parse( $reproceso->fecha )  }} </center></td>
                  <td><center>{{ $reproceso->cod_banco }}-{{ $reproceso->nombre }}</center></td>
                </tr>
                @empty   
                  <tr>
                    <td colspan="8">No existen registros de exclusion anteriores.</td>
                  </tr>
                @endforelse    
            </tbody>
          </table>
        </div>
        <!-- /.card-body-->
      </div>
      <!-- /.card -->
    </div>
  </div>


{{-- Fin de la vista --}}
@stop
@section('after-styles-end')
 <link rel="stylesheet" href="{{asset('css/chosen/chosen.css')}}">
@stop

@section('after-scripts-end')
  <script type="text/javascript" src="{{asset('js/chosen/chosen.jquery.js')}}"></script>

  <script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      max_selected_options: 5,
      no_results_text: "No se encontraron resultados para "
    }); 
  </script>
@stop
