{{-- @Nombre del programa: Vista Principal co->Participantes->Inactivo --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}

@section('page-header')
  <h1><i class="fa fa fa-cog"></i> Exclusión</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa fa-cog"></i> Procesos Especiales</a></li>
    <li><a href="#"> Exclusión</a></li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-warning">
      <div class="box-header with-border">
        <!--codigo gui MOD-CONF-1.1-->
        <input type="hidden" name="codigo_gui" value="MOD-CONF-1.1.1" id="codigo_gui">
        <h3 class="box-title">Listado</h3>
      </div>
      <div class="box-body">

        <div class="col-sm-12">
          <div class="table-responsive">
            <table id="BancosInactivos-table" class="table table-striped">
              <thead class="thead-default">
                <tr>
                  <th class="col-sm-1"><center>Código</center></th>
                  <th class="col-sm-5"><center>Participante</center></th>
                  <th class="col-sm-2"><center>Fecha ingreso</center></th>
                  <th class="col-sm-1"><center>Modalidad</center></th>
                  <th class="col-sm-1"><center>Estado</center></th>
                  <th class="col-sm-2"><center>Acciones</center></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($bancos as $banco)
                  <tr>
                    <td><right>{{ $banco->codigo }}</right></td>
                    <td><right>{{ $banco->nombre }}</right></td>
                    <td><center>{{Date::parse($banco->fecha_ingreso)->format('d-m-Y')}}<center></td>
                    <td><center>{{ $banco->modalidad_participacion == 1 ? 'DIRECTO' : 'INDIRECTO' }}</center></td>
                    <td><center>{{ $banco->estatus }}</center></td>
                    <td><center>
                      @if ($cerrado)
                        @if ($banco->proceso_exclusion == null || ($banco->proceso_exclusion->estatus != 'REGISTRADO' && $banco->proceso_exclusion->estatus != 'APROBADO'))
                        <a class="btn btn-xs btn-warning" href="{{ route('exclusion.excluir', [$banco->id]) }}" data-toggle="tooltip" data-placement="top" data-original-title="Solicitar exlusión de participante"><i class="fa fa-trash"></i></a>
                        @elseif (isset($banco->proceso_exclusion) && $banco->proceso_exclusion->estatus == 'REGISTRADO')
                        <a class="btn btn-xs btn-success" href="{{ route('exclusion.aprobar', [$banco->id]) }}" data-toggle="tooltip" data-placement="top" data-original-title="Aprobar exclusión"><i class="fa fa-check"></i></a>
                        <a class="btn btn-xs btn-danger" href="{{ route('exclusion.denegar', [$banco->id]) }}" data-toggle="tooltip" data-placement="top" data-original-title="Denegar exclusión"><i class="fa fa-ban"></i></a>
                        @endif
                      @endif
                    </td></center>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div><!--table-responsive-->
        </div>
      </div>
    </div>
  </div>
</div>
  @stop

  {{--Fin--}}

  @section('after-scripts-end')
    @include('includes.partials.configuracion.bancos.scripts_bancos_inactivos')
  @stop
