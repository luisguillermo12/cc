{{-- @Nombre del programa: Vista Principal Pro->Day off --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/12/2018 --}}
{{-- @Modificado por:    ivan --}}

@extends ('layouts.master')
{{--Inicio--}}
@section('after-styles-end')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker3.min.css') }}">
@endsection
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-book"></i> Day off</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-book"></i> Procesos especiales</a></li>
        <li class="breadcrumb-item active">Day off</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Calendario</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        @if (($dayoff==null || $dayoff->estatus != 'REGISTRADO'))
          <div class="card-body" style="margin: auto; ">
            <table class="table" style="border-bottom: 1px solid #dee2e6;">
              <tbody>
                <tr>
                  <td><h6>Último día compensado / DayOff ejecutado:</h6></td>
                  <td class="text-center"><h6>{{ $ultimo->fecha->format('d-m-Y') }}</h6></td>
                  <td class="text-center"><h6>{{ $ultimo->estatus }}</h6></td>
                </tr>
              </tbody>
            </table><br>
            {!! Form::open() !!}
            @if(auth()->user()->hasPermission(['50-01-XX-XX-02']))
              <div class="form-group">
                <div class="col-sm-12">
                  <span>Los campos marcados con un asterisco (*) son obligatorios.</span>
                  <p></p>
                  {!!Form::label('fecha', 'Fecha hasta la cual se quiere marcar los day off: (*)', array('class' => 'control-label', 'style' => 'text-transform: initial;'))!!}
                  <div class="col-sm-7">
                    <div class="input-group date">
                      <input type="text" class="form-control" id="fecha" name="fecha" readonly="readonly"/>
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div><br>
                  {!!Form::label(' Descripción:', null, array('class' => 'control-label'))!!}
                  <div class="col-sm-12">
                    {!! Form::textarea('causa', null, ['class' => 'form-control col-sm-12', 'maxlength' => '199', 'rows' => "3"]) !!}
                  </div>
              </div></div>
            </div>
            <!-- /.card-body-->
            <div class="card-footer">
              <div class="text-center">
                {!! Form::submit('Registrar', ['class' => 'btn btn-primary btn-sm', 'title' => 'Guardar', 'id' => 'guardarCalendario', 'disabled']) !!}
              </div>      
            </div>
          @endif
          {!! Form::close() !!}
        @else 

        <div class="card-body">
          <div class="text-center">
            <h4>Registro de Day Off pendiente:</h4>
            <p>Fecha desde : {{ Date::parse($desde)->format('d-m-Y') }} - Fecha hasta : {{ Date::parse($dayoff->descripcion)->format('d-m-Y') }}</p>
            <p></p>
            @if(auth()->user()->hasPermission(['50-01-XX-XX-03']))
              <a class="btn btn-success" href="{{ route('dayoff.aprobar', $dayoff->id) }}">Aprobar</a>
              <a class="btn btn-danger" href="{{ route('dayoff.denegar', $dayoff->id) }}">Denegar</a>
            @endif
          </div>
        </div>

        @endif
      </div>
      <!-- /.card -->
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <tr style="background-color: #c2e7fc;">
              <th><center>Código de operación</center></th>
              <th><center>Fecha day off</center></th>
              <th><center>Fecha (detalle) day off</center></th>
              <th><center>Estado</center></th>
              <th><center>Observaciones</center></th>
              <th><center>Acción</center></th>
            </tr>
            @forelse ($calendarios as $calendario)
              <tr>
                <td><center>{{ $calendario->cod_operacion }}</center></td>
                <td><center>{{ Date::parse($calendario->fecha)->format('d/m/Y') }}</center></td>
                <td><center>{{ ucfirst(Date::parse($calendario->fecha)->format('l j \d\e F \d\e Y')) }}</center></td>
                <td><center>{{ $calendario->estatus }}</center></td>
                <td style="width: 20%"><justify>{{ $calendario->observaciones }}</justify></td>
                
                <td><center><a data-toggle="tooltip" data-placement="top" title="Ver detalles" class="btn accion btn-info" href="{{route('DayOffDetalle',$calendario->id)}}"><i class="fa fa-eye"></i></a></center></td>
              </tr>
            @empty
              <tr>
                <td colspan="6">No se encuentran registros para day off.</td>
              </tr>
            @endforelse
            
          </table>
        </div>
        <!-- /.card-body-->
        <div class="card-footer">
          @if($calendarios->count() > 15)
            <div class="pull-right">{{ $calendarios->links() }}</div>
          @endif
        </div>
      </div>
      <!-- /.card -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-success card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado day off histórico</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <tr style="background-color: #c2e7fc;">
              <th><center>Código de operación</center></th>
              <th><center>Fecha day off</center></th>
              <th><center>Fecha (detalle) day off</center></th>
              <th><center>Estado</center></th>
              <th style="width: 40%"><center>Observaciones</center></th>
            </tr>
            @forelse ($calendarios_historico as $calendario)
            <tr>
              <td><center>{{ $calendario->cod_operacion }}</center></td>
              <td><center>{{ Date::parse($calendario->fecha)->format('d/m/Y') }}</center></td>
              <td><center>{{ ucfirst(Date::parse($calendario->fecha)->format('l j \d\e F \d\e Y')) }}</center></td>
              <td><center>{{ $calendario->estatus }}</center></td>
              <td><justify>{{ $calendario->observaciones }}</justify></td>
            
            </tr>
            @empty
            <tr>
              <td colspan="6">No se encuentran registros para day off.</td>
            </tr>
            @endforelse
            
          </table>
        </div>
        <!-- /.card-body-->
        <div class="card-footer">
          <div class="pull-right">{{ $calendarios_historico->links() }}</div>
        </div>
      </div>
      <!-- /.card -->
    </div>
  </div>
@stop
{{--Fin--}}

@section('after-scripts-end')

  <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datepicker.min.js') }}"></script>

  <script type="text/javascript" src="{{asset('js/datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

  <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

  @include('includes.scripts.procesosespeciales.scripts_calendariodayoff')

@stop
