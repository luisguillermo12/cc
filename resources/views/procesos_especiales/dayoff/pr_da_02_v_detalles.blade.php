{{-- @Nombre del programa: Vista Principal co->Calendario->Feriados --}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 23/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 23/04/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')

{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-book"></i> Detalles</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('/ProcesosEspeciales/DayOff') }}"><i class="fa fa fa-book"></i> Procesos especiales</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/ProcesosEspeciales/DayOff') }}">Day off</a></li>
        <li class="breadcrumb-item active">Detalles</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr style="background-color: #c2e7fc;">
                  <th><center>Código operación</center></th>
                  <th><center>Fecha</center></th>
                  <th><center>Fecha (detalle)</center></th>
                  <th><center>Estado</center></th>
                </tr>
              </thead>
              <tr>
                @foreach($calendario as $calendar)
                  <td><center>{{ $calendar->cod_operacion }}</center></td>
                  <td><center>{{ Date::parse($calendar->fecha)->format('d/m/Y') }}</center></td>
                  <td><center>{{ ucfirst(Date::parse($calendar->fecha)->format('l j \d\e F \d\e Y')) }}</center></td>
                  <td><center>{{ $calendar->estatus }}</center></td>
                @endforeach
              </tr>
            </table>
          </div>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr style="background-color: #c2e7fc;">
                  <th><center>Registrado por</center></th>
                  <th><center>Fecha de registro </center></th>
                  <th><center>Aprobado por </center></th>
                  <th><center>Fecha de aprobación </center></th>
                </tr>
              </thead>
              <tr>
                <td><center>{{ $registrado->nombre_usuario }}</center></td>
                <td ><center>{{ Date::parse($registrado->fecha)  }} </center></td>
                <td ><center>{{ $aprobado->nombre_usuario }} </center></td>
                <td ><center>{{ Date::parse($aprobado->fecha) }} </center></td>
              </tr>
            </table>
          </div>
          <div class="card col-sm-10" style="margin: auto; ">
            <div class="card-header" style="border-color: #9ca2a5;background-color: #c1e7fc;">Descripción</div>
            <div class="card-body">
              @foreach($calendario as $calendar)
                <p class="card-text">{{ $calendar->observaciones }}</p>
              @endforeach
            </div>
          </div>
        </div>
        <!-- /.card-body-->
      </div>
      <!-- /.card -->
    </div>
  </div>

@stop
{{--Fin--}}

@section('after-scripts-end')

@stop
