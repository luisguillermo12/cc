{{-- @Nombre del programa: Vista Principal ProEs->Close dayoff--}}
{{-- @Funcion: Close Day Off --}}
{{-- @Autor: Deivi Peña --}}
{{-- @Fecha Creacion: 09/08/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion:  --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="fa fa fa-book"></i> Close day off</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa fa-book"></i> Procesos especiales</a></li>
        <li class="breadcrumb-item active">Close day off</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('after-styles-end')
  <style type="text/css">
    .box-icon-eps {
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
      float: left;
      height: 50px;
      width: 50px;
      text-align: center;
      font-size: 0px;
      line-height: 65px;
      color: white;

    }
    .icon-box-eps {
      font-size: 20px !important;
    }
    .number-box-eps {
      display: block;
      font-weight: bold;
      bottom: auto;
      text-align: center;
    }
    .card-total {
      width: 19rem;
    }
    .card-total .card-body{
      margin-bottom: -8px;
    }
  </style>
@endsection

@section('content')
{{-- Inicio de la vista --}}

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Ejecución</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="col-sm-12">
            <h4 class="text-center">Período de compensación: {{ date('d/m/Y  h:i:s') }}</h4>
          </div><br>
          <table class="table table-bordered">
            <thead>
              <tr class="text-center" style="background-color: #c2e7fc;">
                <th>Estado</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><center><h5>
                  @if ($closedayoff != null && $closedayoff->estatus == 'APROBADO')
                  Close day off en ejecución
                  @elseif ($closedayoff != null && $closedayoff->estatus == 'REGISTRADO')
                  Close day off por aprobar
                  @else
                  Close day off no ejecutado
                  @endif
                </h5></center></td>
                <td class="text-center">
                  
                  @if ($validar && $closedayoff != null && $closedayoff->estatus == 'REGISTRADO')
                    @if(auth()->user()->hasPermission(['50-02-XX-XX-03']))
                  
                      {!! link_to_route('closedayoff.aprobar', 'Aprobar', [$closedayoff->id], ['class' => 'btn btn-success btn-sm','title' => 'Cancelar']) !!}
                      
                      {!! link_to_route('closedayoff.denegar', 'Denegar', [$closedayoff->id], ['class' => 'btn btn-danger btn-sm','title' => 'Cancelar']) !!}
                    @endif

                  @elseif ($validar && ($closedayoff == null || $closedayoff->estatus != 'APROBADO'))
                    @if(auth()->user()->hasPermission(['50-02-XX-XX-02']))

                      {!! Form::open(['route' => 'closedayoff.registrar', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'form-create']) !!}
                      <div class="form-group col-sm-12">
                        {!!Form::label(' Descripción:', null, array('class' => 'col-sm-4'))!!}
                        <div class="col-sm-12">
                          {!! Form::textarea('causa', null, ['class' => 'form-control col-sm-12', 'maxlength' => '199', 'rows' => "3"]) !!}
                        </div>
                      </div>
                      <div class="text-center">
                        {!! Form::submit('Registrar', ['class' => 'btn btn-primary btn-sm','title' => 'Guardar']) !!}    
                      </div>
                    @endif
                  @else
                  <h4>No permitido</h4>
                  @endif
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.card-body-->
      </div>
      <!-- /.card -->
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Listado close day off histórico</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped">
            <tr style="background-color: #c2e7fc;">
              <th><center>Código de operación</center></th>
              <th><center>Fecha day off</center></th>
              <th><center>Fecha (detalle) day off</center></th>
              <th><center>Estado</center></th>
              <th><center>Observaciones</center></th>
            </tr>
            @forelse ($calendarios_historico as $calendario)
            <tr>
              <td><center>{{ $calendario->cod_operacion }}</center></td>
              <td><center>{{ Date::parse($calendario->fecha)->format('d/m/Y') }}</center></td>
              <td><center>{{ ucfirst(Date::parse($calendario->fecha)->format('l j \d\e F \d\e Y')) }}</center></td>
              <td><center>{{ $calendario->estatus }}</center></td>
              <td><justify>{{ $calendario->observaciones }}</justify></td>
            
            </tr>
            @empty
            <tr>
              <td colspan="6">No se encuentran registros para day off.</td>
            </tr>
            @endforelse
          </table>
        </div>
        <!-- /.card-body-->
        <div class="card-footer">
          <div class="pull-right">{{ $calendarios_historico->links() }}</div>
        </div>
      <!-- /.card -->
    </div>

    @if ($closedayoff != null && $closedayoff->estatus == 'APROBADO') 
      <div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="card card-success card-outline">
            <div class="card-header">
              <h3 class="card-title">Total de operaciones</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4">
                    <div class="card card-total">
                      <div class="card-body">
                        <div class="row">
                          <span class="box-icon-eps" style="background: #67c2ef;"><i class="fa fa-file-o icon-box-eps"></i></span>
                          <div class="row col-md-7" style="margin-left: auto;">
                            <span class="card-text" style="font-size: 15px">Total<br>
                            <p class="number-box-eps">{{ number_format($totales->total_operaciones, '0', ',' , '.') }}</p></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card card-total">
                      <div class="card-body">
                        <div class="row">
                          <span class="box-icon-eps" style="background: #79c447;"><i class="fa fa-plus icon-box-eps"></i></span>
                          <div class="row col-md-9" style="margin-left: auto;">
                            <span class="card-text" style="font-size: 15px">De crédito por liquidar<br>
                            <p class="number-box-eps">{{ number_format($totales->credito_por_liquidar, '0', ',' , '.') }}</p></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card card-total">
                      <div class="card-body">
                        <div class="row">
                          <span class="box-icon-eps" style="background: #79c447;"><i class="fa fa-plus icon-box-eps"></i></span>
                          <div class="row col-md-9" style="margin-left: auto;">
                            <span class="card-text" style="font-size: 15px">De crédito liquidadas<br>
                            <p class="number-box-eps">{{ number_format($totales->credito_liquidado, '0', ',' , '.') }}</p></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card card-total">
                      <div class="card-body">
                        <div class="row">
                          <span class="box-icon-eps" style="background: #ff2323;"><i class="fa fa-minus icon-box-eps"></i></span>
                          <div class="row col-md-9" style="margin-left: auto;">
                            <span class="card-text" style="font-size: 15px">De dédito por liquidar<br>
                            <p class="number-box-eps">{{ number_format($totales->debito_por_liquidar, '0', ',' , '.') }}</p></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card card-total">
                      <div class="card-body">
                        <div class="row">
                          <span class="box-icon-eps" style="background: #ff2323;"><i class="fa fa-minus icon-box-eps"></i></span>
                          <div class="row col-md-9" style="margin-left: auto;">
                            <span class="card-text" style="font-size: 15px">De dédito liquidadas<br>
                            <p class="number-box-eps">{{ number_format($totales->debito_liquidado, '0', ',' , '.') }}</p></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {{-- si el anterior no gusta 
                  <table border="0" align="center" width="100%">
                    <tbody>
                      <tr>
                        <td><center>
                          <div class="col-md-12">
                            <div class="info-box-eps">
                              <span class="box-icon-eps" style="background: #67c2ef ;"><i class="fa fa-file-o icon-box-eps"></i></span>
                              <div class="">
                                <span class="info-box-text-eps" style="margin: 20px;">Total operaciones</span><br>
                                <span class="number-box-eps">
                                  {{ number_format($totales->total_operaciones, '0', ',' , '.') }}
                                </span>
                              </div>
                            </div>
                          </div>
                          <!-- /.col -->
                        </center></td>
                        <td><center>
                          <div class="col-md-12">
                            <div class="info-box-eps">
                              <span class="box-icon-eps" style="background: #79c447 ;"><i class="fa fa-plus icon-box-eps"></i></span>
                              <div class="">
                                <span class="info-box-text-eps" style="margin: 20px;">Operaciones de crédito por liquidar</span><br>
                                <span class="number-box-eps">

                                  {{ number_format($totales->credito_por_liquidar, '0', ',' , '.') }}

                                </span>
                              </div>
                            </div>
                          </div>
                          <!-- /.col -->
                        </center></td>
                        <td><center>
                          <div class="col-md-12">
                            <div class="info-box-eps">
                              <span class="box-icon-eps" style="background:#ff2323;"><i class="fa fa-minus icon-box-eps"></i></span>
                              <div class="">
                                <span class="info-box-text-eps" style="margin: 20px;">Operaciones de crédito liquidadas</span><br>
                                <span class="number-box-eps">

                                  {{ number_format($totales->credito_liquidado, '0', ',' , '.') }}

                                </span>
                              </div>
                            </div>
                          </div>
                          <!-- /.col -->
                        </center></td>
                      </tr>

                      <tr>
                        <td></td>
                        <td><center>
                          <div class="col-md-12">
                            <div class="info-box-eps">
                              <span class="box-icon-eps" style="background: #79c447;"><i class="fa fa-plus icon-box-eps"></i></span>
                              <div class="">
                                <span class="info-box-text-eps" style="margin: 20px;">Operaciones de dédito por liquidar</span><br>
                                <span class="number-box-eps">

                                  {{ number_format($totales->debito_por_liquidar, '0', ',' , '.') }}

                                </span>
                              </div>
                            </div>
                          </div>
                          <!-- /.col -->
                        </center></td>
                        <td><center>
                          <div class="col-md-12">
                            <div class="info-box-eps">
                              <span class="box-icon-eps" style="background: #ff2323;"><i class="fa fa-minus icon-box-eps"></i></span>
                              <div class="">
                                <span class="info-box-text-eps" style="margin: 20px;">Operaciones de dédito liquidadas</span><br>
                                <span class="number-box-eps">

                                  {{ number_format($totales->debito_liquidado, '0', ',' , '.') }}

                                </span>
                              </div>
                            </div>
                          </div>
                          <!-- /.col -->
                        </center></td>

                      </tr>
                    </tbody>
                  </table>
                  --}}
                </div>
              </div>
            </div>
            <!-- /.card-body-->
          </div>
          <!-- /.card -->
        </div>
      </div>
    @endif
    @if ($closedayoff != null && $closedayoff->estatus == 'APROBADO')
      <div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="card card-warning card-outline">
            <div class="card-header">
              <h3 class="card-title">Listado</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table id="EmpresasOrdenantes-table" class="table table-striped">
                  <thead class="thead-default">
                    <tr style="background-color: #c2e7fc;">
                      <th><center>Participante</center></th>
                      <th><center>Total </center></th>
                      <th><center>Credito por liquidar </center></th>
                      <th><center>Debito por liquidar</center></th>
                      <th><center>Credito liquidado </center></th>
                      <th><center>Debito liquidado</center></th>
                    </tr>
                  </thead>
                  @foreach ($totales_por_banco as $total_banco)
                    <tr>
                     <td><center>{{ $total_banco->participante }} </center></td>
                     <td><center>{{ number_format($total_banco->total_operaciones, '0', ',' , '.')  }} </center></td>
                     <td><center>{{number_format($total_banco->credito_por_liquidar, '0', ',' , '.')  }}</center></td>
                     <td><center>{{number_format($total_banco->debito_por_liquidar, '0', ',' , '.')   }}</center></td>
                     <td><center>{{number_format($total_banco->credito_liquidado, '0', ',' , '.')  }}</center></td>
                     <td><center>{{number_format($total_banco->debido_liquidado, '0', ',' , '.')   }}</center></td>
                   </tr>
                  @endforeach
                </table>
              </div>
            </div>
            <!-- /.card-body-->
            <div class="card-footer">
              <div class="pull-right">
                {{ $totales_por_banco->links() }}
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div>
    @endif

  </div>
</div>
{{-- Fin de la vista --}}
@stop

@section('after-scripts-end')
  
@stop
