{{-- @Nombre del programa: Vista Principal Pro->reproceso->consulta--}}
{{-- @Funcion: --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 16/04/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 26/06/2018 --}}
{{-- @Modificado por: Ing.VL   --}}
@extends ('layouts.master')
{{--Inicio--}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="nav-icon fa fa-book"></i> Consulta</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="nav-icon fa fa-book"></i> Procesos especiales</a></li>
        <li class="breadcrumb-item active">Reproceso</li>
        <li class="breadcrumb-item active">consulta</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">Listado</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped table-sm">
          <tr style="background-color: #c2e7fc;">
            <th><center>Código de operación</center></th>
            <th><center>Proceso especial</center></th>
            <th><center>Estados</center></th>
            <th><center>Fecha de reproceso</center></th>
            <th><center>Participante </center></th>
          </tr>
          <tbody>
            @forelse ($reprocesos as $reproceso) 
              <tr>
                <td ><center> {{ $reproceso->cod_operacion }} </center></td>
                 <td><center> {{ $reproceso->nombre_proceso }} </center></td>

              @if($reproceso->estatus=='APROBADO')
                <td style="background-color:  #e8f9ea" ><center> {{ $reproceso->estatus }} </center></td>
                @else 
                @if($reproceso->estatus=='DENEGADO')
                <td style="background-color:  #ffb9b0" ><center>{{ $reproceso->estatus }} </center></td>
                @else 
                <td><center>{{ $reproceso->estatus }} </center></td>
                @endif
              @endif
                 <td><center> {{  Date::parse( $reproceso->fecha ) }} </center></td>
                 <td>{{ $reproceso->cod_banco }}-{{ $reproceso->nombre }}</td>
              </tr>
            @empty   
              <tr>
                <td colspan="8">No existen registros de reprocesos pendientes.</td>
              </tr>
            @endforelse    
          </tbody>
           <tfoot style="background-color: #CBCACA; font-weight: bold;">
            @if($registrados>0)
              <div class="col-sm-12">
                <p class="text-center">
                  <a href="{{url('ProcesosEspeciales/Reproceso/Consulta/Aprobar')}}" class="btn btn-primary btn-success margin " role="button">Aprobar</a>
                  <a href="{{url('ProcesosEspeciales/Reproceso/Consulta/Denegar')}}" class="btn btn-primary btn-danger margin " role="button">Denegar</a>
                </p>
              </div>
            @endif
           </tfoot>
        </table>
      </div>
      <!-- /.card-body-->
    </div>
    <!-- /.card -->
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">Listado histórico</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped table-sm">
          <tr style="background-color: #c2e7fc;">
              <th><center>Código de operación</center></th>
              <th><center>Proceso especial</center></th>
              <th><center>Estados</center></th>
              <th><center>Fecha de reproceso</center></th>
              <th><center>Participante</center></th>
          </tr>
          <tbody>
            @forelse ($reprocesos_historico as $reproceso) 
              <tr>
                <td ><center> {{ $reproceso->cod_operacion }} </center></td>
                <td><center> {{ $reproceso->nombre_proceso }} </center></td>
                @if($reproceso->estatus=='APROBADO')
                <td style="background-color:  #e8f9ea" ><center> {{ $reproceso->estatus }} </center></td>
                @else 
                @if($reproceso->estatus=='DENEGADO')
                <td style="background-color:  #ffb9b0" ><center>{{ $reproceso->estatus }} </center></td>
                @else 
                <td><center>{{ $reproceso->estatus }} </center></td>
                @endif
            @endif
                <td><center> {{  Date::parse( $reproceso->fecha ) }} </center></td>
                <td>{{ $reproceso->cod_banco }}-{{ $reproceso->nombre }}</td>
              </tr>
             @empty   
                <tr>
                  <td colspan="8">No existen registros de reprocesos pendientes.</td>
                </tr>
            @endforelse   
          </tbody>
          <tfoot style="background-color: #CBCACA; font-weight: bold;">
            @if($registrados>0)
              <div class="col-sm-12">
                  <p class="text-center">
                    <a href="{{url('ProcesosEspeciales/Reproceso/Consulta/Aprobar')}}" class="btn btn-primary btn-success margin " role="button">Aprobar</a>
                    <a href="{{url('ProcesosEspeciales/Reproceso/Consulta/Denegar')}}" class="btn btn-primary btn-danger margin " role="button">Denegar</a>
                  </p>
              </div>
            @endif
          </tfoot>
        </table>
      </div>
      <!-- /.card-body-->
    </div>
    <!-- /.card -->
  </div>
</div>
@stop
{{--Fin--}}

@section('after-scripts-end')

@stop
