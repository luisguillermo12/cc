{{-- @Nombre del programa: Vista Principal ProEs->reproceso->Simulación--}}
{{-- @Funcion: Operaciones conciliadas --}}
{{-- @Autor: Laser Computacion --}}
{{-- @Fecha Creacion: 04/05/2018 --}}
{{-- @Requerimiento: --}}
{{-- @Fecha Modificacion: 04/05/2018 --}}
{{-- @Modificado por:    --}}

@extends ('layouts.master')
{{-- Inicio Operaciones Conciliadas --}}
@section('page-header')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1><i class="nav-icon fa fa-book"></i> Simulación</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#"><i class="nav-icon fa fa-book"></i> Procesos especiales</a></li>
        <li class="breadcrumb-item active">Reproceso</li>
        <li class="breadcrumb-item active">Simulación</li>
      </ol>
    </div>
  </div>
</div>
@endsection

@section('content')
{{-- Inicio de la vista --}}
 <div class="row">
    <div class="col-md-12">
      <!-- Line chart -->
      <div class="card card-warning card-outline">
        <div class="card-header">
          <h3 class="card-title">Simular reproceso</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
          <div class="card-body" style="margin: auto; ">
          <form role="form" class="form-horizontal">
            <table class="table" style="border-bottom: 1px solid #dee2e6;">
              <tbody>
                <tr>
                  <td><h6>Jornada actual de compensación: {{ $fecha }}</h6></td>
                </tr>
              </tbody>
            </table><br>
              <div class="form-group">
                <div class="col-sm-12">
                  <span>Los campos marcados con un asterisco (*) son obligatorios, puede realizar la simulación con un máximo de 5 participantes.</span>
                  <p></p>
                 <div class="form-group">
                  <div class="col-sm-8">
                    {!! Form::label('Participante(*)', null, array('class' => 'col-sm-3 control-label')) !!}
                    <div class="col-sm-9">
                      {{ Form::select('banco[]', $bancos, null, ['class' => 'chosen-select', 'multiple', 'data-placeholder' => 'Seleccione uno o más participantes']) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <!-- /.card-body-->
            <div class="card-footer">
              <div class="text-center">
               <button type="submit" class="btn btn-primary btn-sm pull-right"><i class="fa fa-search" aria-hidden="true"></i> Simular reproceso</button>
              </div>      
            </div>
          </form>
      </div>
    </div>
</div>

@if($hacer)
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">Listado</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped table-sm">
          <tr style="background-color: #c2e7fc;">
           <th><center>Código de participante</center></th>
            <th><center>Participante emisor</center></th>
            <th nowrap><center>Saldo deudor</center></th>
            <th nowrap><center>Saldo acreedor</center></th>
          </tr>
           <tbody>
            @if(isset($posiciones))
              @foreach ($posiciones as $posicion)
                <tr>
                  <td><center>{{ $posicion->cod_banco_emisor }}</center></td>
                  <td nowrap>{{ $posicion->nombre }}</td>
                  @if ($posicion->posicion>0)
                  <td style="text-align: right">
                    0
                  </td>    
                  <td style="text-align: right">
                    {{ number_format($posicion->posicion/100, '2', ',' , '.') }}
                  </td> 

                  @else
                  <td style="text-align: right">
                  {{ number_format($posicion->posicion/100, '2', ',' , '.') }}
                  </td>    
                  <td style="text-align: right">
                   0
                  </td>
                  @endif                        
                </tr>
              @endforeach
            @endif
           </tbody>

           <tfoot style="background-color: #CBCACA; font-weight: bold;">
            <tr>
              <td></td>
              <td>Totales</td>
              <td style="text-align: right">{{ number_format($total_negativos/100, '2', ',' , '.') }}</td>
              <td style="text-align: right">{{ number_format($total_positivos/100, '2', ',' , '.') }}</td>
            </tr>
           </tfoot>
        </table>
      </div>
      <!-- /.card-body-->
    </div>
    <!-- /.card -->
  </div>
</div>
@endif
{{-- Fin de la vista --}}
@stop
@section('after-styles-end')
  {!! Html::style('css/chosen/chosen.min.css') !!}
@stop

@section('after-scripts-end')
  {!! Html::script('js/chosen/chosen.jquery.min.js') !!}

  <script type="text/javascript">
    $(".chosen-select").chosen({
      width: "100%",
      max_selected_options: 5,
      no_results_text: "No se encontraron resultados para "
    }); 
  </script>
@stop
